# JMash

#### 介绍
JMash旨在遵从Jakarta EE 10 , MicroProfile 6.1 规范,构建Service Mash(服务网格)架构实践。

[![License](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg)](https://gitee.com/jmash/jmash/blob/master/LICENSE)

以K8S为代表的云原生Cloud Native技术将提供更加完善的基础设施,信息化建设将进入工业化发展时代，为解决MicroService微服务架构的服务治理难题，依托Jakarta RPC(GRPC)、K8S+Docker 、Eclipse MicroProfile、Istio(Service Mash)等技术,构建高性能、开源、企业级的应用架构实践，满足金融、医疗、政务、制造、交通、教育、电力、通信等领域信息化长期发展的需求。

## Requirements

JMash requires Java 17 or later.

#### 软件架构
软件架构说明

01. 遵循Jakarta RPC(GRPC)、Protobuf 3.0 、Jakarta Restful 3.1 、Jakarta MVC 2.1 、Jakarta Websocket 2.1 标准规范.
02. OpenID Connect 身份认证及授权，标准的RBAC用户角色权限系统.
03. 前端采用Element UI Plus 、Vue3+TypeScript.
04. Jakarta CDI 4.0 (Weld Replace Spring)、Jakarta Bean Validation 3.0 (数据校验).
05. 关系型数据库采用JPA规范，支持分库分表、实现跨数据库(默认MariaDB).
06. 非关系型数据库：Redis Session共享、JPA二级缓存.
07. RabbitMQ 消息队列支持 Jakarta Messaging 3.1 、MQTT5.0.
08. 工作流系统集成Flowable7将另增项目 jmash-flow.
09. 开源组件：ProtoBeanUtils(MapStruct+BeanUtils)、FreeMarker Java Template Engine、POI等.
10. 基础设施 K8S + docker + Nginx Ingress
11. 附：移动端 Android/鸿蒙/IOS架构、采用ArkUI+ArkTS跨平台开发框架(进行中).
12. 附：C/S架构采用QT、C++、GRPC技术构建. 
13. 附：原生小程序(进行中)
14. 代码审查工具：CheckStyle Spotbugs.

#### 业务功能

1. 面向Saas服务场景，提升Saas开发效率和稳定性
2. 支持跨平台异构开发环境，兼容国产麒麟、统信等操作系统。
如：生产系统  EMS \ WMS \ ERP  等各类专业软件。上位机控制系统.
3. 弹性分布式技术，确保数据一致性。
4. 实现标准RBAC，包含角色互斥和角色继承,更细颗粒的权限控制体系。
5. 严谨的安全体系和智能反馈机制。
6. 完整的操作日志记录及追溯。
7. 适用于专业领域软件系统等应用场景，避免发生数据篡改、丢失、覆盖等问题。


#### 安装教程

1.  安装开发环境
eclipse protobuf-dt、checkstyle、spotbugs、FreeMarker

Preferences->Java->Code Style->Formatter import eclipse-profile.xml

JDK17

https://adoptium.net/zh-CN/temurin/releases/?version=17

2.  idea 
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

交流
微信：digithuman_004

#### 相关项目

Jmash架构底座(Jakarata EE 10 + MicroProfile 6.1 + Grpc)
https://gitee.com/jmash/jmash

用户角色权限管理系统(RBAC)
https://gitee.com/jmash/rbac

前端Git(Vue3+Typescript)
https://gitee.com/jmash/qiankun

工作流Git(Flowable7)
https://gitee.com/jmash/flow

模块代码生成工具Git
https://gitee.com/jmash/generate

标准OpenID Connect实现 
https://gitee.com/jmash/oidc
