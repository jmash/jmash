DROP TABLE IF EXISTS job_define;
CREATE TABLE job_define(
    `job_name` binary(16) NOT NULL  COMMENT '计划名称' ,
    `classify_` VARCHAR(50) NOT NULL  COMMENT '分类' ,
    `job_cn_name` VARCHAR(60) NOT NULL  COMMENT '计划任务中文名称' ,
    `job_describe` VARCHAR(240)   COMMENT '计划任务描述' ,
    `weight_` INT NOT NULL  COMMENT '任务运行权重' ,
    `event_name` VARCHAR(90) NOT NULL  COMMENT '任务触发事件名称' ,
    `event_body` VARCHAR(1000)   COMMENT '任务触发事件JOSN' ,
    `instance_name` VARCHAR(60)   COMMENT '运行实例' ,
    `enable_` bool NOT NULL  COMMENT '是否启用' ,
    `version_` INT NOT NULL  COMMENT '乐观锁' ,
    `create_by` binary(16) NOT NULL  COMMENT '创建人' ,
    `create_time` DATE NOT NULL  COMMENT '创建时间' ,
    `update_by` binary(16)   COMMENT '更新人' ,
    `update_time` DATE   COMMENT '更新时间' ,
    PRIMARY KEY (job_name)
)  COMMENT = '计划任务定义;';



DROP TABLE IF EXISTS job_instance;
CREATE TABLE job_instance(
    `instance_name` VARCHAR(60) NOT NULL  COMMENT '运行实例' ,
    `classify_` VARCHAR(60) NOT NULL  COMMENT '分类' ,
    `heartbeat_` datetime   COMMENT '心跳时间' ,
    `due_date` datetime   COMMENT '过期时间' ,
    `state_` VARCHAR(50)   COMMENT '状态$启动、运行、停止' ,
    PRIMARY KEY (instance_name,classify_)
)  COMMENT = '运行实例;';



DROP TABLE IF EXISTS job_record;
CREATE TABLE job_record(
    `record_id` binary(16) NOT NULL  COMMENT '计划执行ID' ,
    `job_name` binary(16) NOT NULL  COMMENT '计划名称' ,
    `start_date` datetime NOT NULL  COMMENT '开始时间' ,
    `end_date` datetime   COMMENT '结束时间' ,
    `error_msg` TEXT   COMMENT '错误信息' ,
    `state_` bool NOT NULL  COMMENT '状态$成功 失败' ,
    PRIMARY KEY (record_id)
)  COMMENT = '计划任务记录;';


ALTER TABLE job_record ADD CONSTRAINT FK_job_record___job_define__job_name FOREIGN KEY (job_name) REFERENCES job_define (job_name) ON DELETE RESTRICT ON UPDATE RESTRICT;

DROP TABLE IF EXISTS job_trigger;
CREATE TABLE job_trigger(
    `trigger_id` binary(16) NOT NULL  COMMENT '触发ID' ,
    `job_name` binary(16) NOT NULL  COMMENT '计划名称' ,
    `start_now` bool NOT NULL  COMMENT '是否当前日期开始' ,
    `start_date` datetime   COMMENT '启动时间' ,
    `cron_` VARCHAR(60)   COMMENT 'cron表达式' ,
    `cron_name` VARCHAR(240)   COMMENT 'cron描述' ,
    `is_custom` bool   COMMENT '是否自定义$默认false' ,
    PRIMARY KEY (trigger_id)
)  COMMENT = '任务触发器;';


ALTER TABLE job_trigger ADD CONSTRAINT FK_job_trigger___job_define__job_name FOREIGN KEY (job_name) REFERENCES job_define (job_name) ON DELETE RESTRICT ON UPDATE RESTRICT;

