DROP TABLE IF EXISTS sms_app_config;
CREATE TABLE sms_app_config(
    `config_code` VARCHAR(30) NOT NULL  COMMENT '应用配置Code' ,
    `config_name` VARCHAR(30) NOT NULL  COMMENT '配置名称' ,
    `config_type` VARCHAR(50) NOT NULL  COMMENT '配置类型$短信、邮件等' ,
    `provider_` VARCHAR(50) NOT NULL  COMMENT '服务厂商' ,
    `is_default` bool NOT NULL  COMMENT '是否默认' ,
    `access_key_id` VARCHAR(120)   COMMENT '访问key' ,
    `access_key_secret` VARCHAR(500)   COMMENT '访问密钥$加密存储' ,
    `create_by` binary(16)   COMMENT '创建人' ,
    `create_time` DATE NOT NULL  COMMENT '创建时间' ,
    PRIMARY KEY (config_code)
)  COMMENT = '消息应用配置';

DROP TABLE IF EXISTS sms_sign;
CREATE TABLE sms_sign(
    `sign_id` binary(16) NOT NULL  COMMENT '签名ID' ,
    `sign_name` VARCHAR(15) NOT NULL  COMMENT '签名名称' ,
    `config_code` VARCHAR(30) NOT NULL  COMMENT '应用配置code' ,
    `order_id` VARCHAR(30)   COMMENT '工单号' ,
    `business_type` VARCHAR(50) NOT NULL  COMMENT '验证码类型' ,
    `use_scene` VARCHAR(60)   COMMENT '适用场景' ,
    `audit_status` VARCHAR(50) NOT NULL  COMMENT '审核状态' ,
    `create_by` binary(16)   COMMENT '创建人' ,
    `create_time` DATE NOT NULL  COMMENT '创建时间' ,
    PRIMARY KEY (sign_id)
)  COMMENT = '短信签名';

DROP TABLE IF EXISTS sms_template;
CREATE TABLE sms_template(
    `template_id` binary(16) NOT NULL  COMMENT '模板ID' ,
    `template_code` VARCHAR(30) NOT NULL  COMMENT '模板CODE' ,
    `template_name` VARCHAR(60) NOT NULL  COMMENT '模板名称' ,
    `config_code` VARCHAR(30) NOT NULL  COMMENT '应用配置code' ,
    `order_id` VARCHAR(30)   COMMENT '工单号' ,
    `template_content` VARCHAR(500)   COMMENT '模板内容' ,
    `template_type` INT NOT NULL  COMMENT '模板类型' ,
    `audit_status` VARCHAR(50) NOT NULL  COMMENT '审核状态' ,
    `create_by` binary(16)   COMMENT '创建人' ,
    `create_time` DATE NOT NULL  COMMENT '创建时间' ,
    PRIMARY KEY (template_id)
)  COMMENT = '短消息模板';

