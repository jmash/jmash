:: 1.创建namespace
kubectl create -f ns-jmash.yaml


call kubectl delete --force -f nfspvc-file.yaml
call kubectl delete --force -f nfspvc-redis.yaml
call kubectl delete --force -f nfspv-file.yaml
call kubectl delete --force -f nfspv-redis.yaml

:: 2.创建PV
call kubectl apply -f nfspv-file.yaml
call kubectl apply -f nfspv-redis.yaml

:: 3.创建文件存储PVC
call kubectl apply -f nfspvc-file.yaml

:: 4.创建Redis存储PVC
call kubectl apply -f nfspvc-redis.yaml