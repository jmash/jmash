# 1.创建namespace
kubectl create -f ns-jmash.yaml
#
kubectl delete --force -f nfspvc-file.yaml
kubectl delete --force -f nfspvc-redis.yaml
kubectl delete --force -f nfspv-file.yaml
kubectl delete --force -f nfspv-redis.yaml
# 2.创建PV
kubectl apply -f nfspv-file.yaml
kubectl apply -f nfspv-redis.yaml
# 3.创建文件存储PVC
kubectl apply -f nfspvc-file.yaml
# 4.创建Redis存储PVC
kubectl apply -f nfspvc-redis.yaml