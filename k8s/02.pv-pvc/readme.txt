1.配置NFS服务

1.1 Centos Linux配置NFS服务
yum install -y nfs-common nfs-utils  rpcbind

mkdir /data/nfsdata/file
chmod 666 /data/nfsdata/file
chown nfsnobody /data/nfsdata/file

mkdir /data/nfsdata/redis
chmod 666 /data/nfsdata/redis
chown nfsnobody /data/nfsdata/redis


cat /etc/exports
    /data/nfsdata/file  *(rw,no_root_squash,no_all_squash,sync)
	/data/nfsdata/redis *(rw,no_root_squash,no_all_squash,sync)
	
systemctl start rpcbind
systemctl start nfs
systemctl enable rpcbind.service
systemctl enable nfs.service


1.2 Debian Linux配置NFS服务
sudo apt-get install nfs-common nfs-kernel-server portmap -y

mkdir /data/nfsdata/file
chown nobody:nogroup /data/nfsdata/file
chmod 755 /data/nfsdata/file

mkdir /data/nfsdata/redis
chown nobody:nogroup /data/nfsdata/redis
chmod 755 /data/nfsdata/redis

cat /etc/exports
    /data/nfsdata/file  *(rw,no_root_squash,no_all_squash,sync,no_subtree_check)
	/data/nfsdata/redis *(rw,no_root_squash,no_all_squash,sync,no_subtree_check)
	
systemctl restart nfs-server
systemctl enable nfs-server

1.3 Window配置NFS服务
下载WinNFSd.exe 
https://sourceforge.net/projects/winnfsd/
https://github.com/winnfsd/winnfsd
启动NFS
WinNFSd.exe e:\nfsdata

1.3 Linux 测试NFS服务
安装NFS客户端
yum -y intall nfs-utils 
showmount -e IP地址


1.4 k8s 创建PV

call kubectl apply -f pv.yaml
call kubectl get pv

1.5 k8s 创建PVC 

call kubectl apply -f pvc.yaml
call kubectl get pvc
