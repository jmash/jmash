# 1.创建RSA私钥
# 生成2048位私钥，server_private.key文件中
openssl genrsa -out server_private.key 2048
# 查看私钥
openssl rsa -in server_private.key -text

# 2.使用RSA私钥生成公钥
# 如果是加密私钥，需要交互输入私钥密码
openssl rsa -in server_private.key -pubout -out server_public.key
# 查看公钥
openssl rsa  -RSAPublicKey_in -in server_public.key -text

# 3.生成csr
openssl req -new -config csr.cnf -key server_private.key -out server.csr
# 查看csr
openssl req -in server.csr -text -noout

# 4.使用csr生成crt证书
openssl x509 -req -days 3650 -in server.csr -signkey server_private.key -out server.crt -extfile server.ext
# 查看crt
openssl x509 -text -in server.crt -noout

# 5. Test
# openssl s_server -cert server.crt -key server_private.key -www

mv  server_private.key tls.key
mv  server.crt tls.crt