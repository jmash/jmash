0. minikube 按照ingress-nginx

minikube addons enable ingress

1.helm 安装  helm-install.bat

2. 或 yaml 安装  yaml-install.bat

3. 创建tls 
cert/readme.txt

4. 创建Grpc Secret 
kubectl create secret tls grpc-secret  --cert tls.crt --key tls.key -n jmash

5.Example :

  apiVersion: networking.k8s.io/v1
  kind: Ingress
  metadata:
    name: example
    namespace: foo
  spec:
    ingressClassName: nginx
    rules:
      - http:
          paths:
            - pathType: Prefix
              backend:
                service:
                  name: exampleService
                  port:
                    number: 80
              path: /
    # This section is only required if TLS is to be enabled for the Ingress
    tls:
      - hosts:
        - dev.jmash.crenjoy.com
        - jmash.crenjoy.com
        secretName: grpc-secret

If TLS is enabled for the Ingress, a Secret containing the certificate and key must also be provided:

  apiVersion: v1
  kind: Secret
  metadata:
    name: example-tls
    namespace: foo
  data:
    tls.crt: <base64 encoded cert>
    tls.key: <base64 encoded key>
  type: kubernetes.io/tls
