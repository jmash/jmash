---

## Test 系统入口

[RBAC 管理系统](/rbac/login?tenant=test)

[工会管理系统](/union/login?tenant=test)

[公车管理系统](/mscar/login?tenant=test)

[CMS管理系统](/cms/login?tenant=test)
---

## JMash OpenApi Index

## JMash RBAC 服务

[Swagger](/openapi-ui/swagger/index.html?url=/openapi/jmash/rbac/rbac_rpc.swagger.json)
[Proto3](/openapi/jmash/rbac/index.html)

## JMash Captcha 验证码服务

[Swagger](/openapi-ui/swagger/index.html?url=/openapi/jmash/captcha/service.swagger.json)
[Proto3](/openapi/jmash/captcha/index.html)

## JMash Sms 短信服务

[Swagger](/openapi-ui/swagger/index.html?url=/openapi/jmash/sms/service.swagger.json)
[Proto3](/openapi/jmash/sms/index.html)

## JMash File 文件服务

[Swagger](/openapi-ui/swagger/index.html?url=/openapi/jmash/file/file_rpc.swagger.json)
[Proto3](/openapi/jmash/file/index.html)

[Swagger](/openapi-ui/swagger/index.html?url=/openapi/jmash/basic/file_basic_rpc.swagger.json)
[Proto3](/openapi/jmash/basic/index.html)

## JMash Dict 数据字典服务

[Swagger](/openapi-ui/swagger/index.html?url=/openapi/jmash/dict/dict_rpc.swagger.json)
[Proto3](/openapi/jmash/dict/index.html)

## JMash Jobs 定时任务服务

[Swagger](/openapi-ui/swagger/index.html?url=/openapi/jmash/jobs/jobs_rpc.swagger.json)
[Proto3](/openapi/jmash/jobs/index.html)

## JMash Flow 工作流

[Swagger](/openapi-ui/swagger/index.html?url=/openapi/jmash/flow/flow_rpc.swagger.json)
[Proto3](/openapi/jmash/flow/index.html)

## JMash OpenID Connect 认证授权服务

[Swagger](/openapi-ui/swagger/index.html?url=/openapi/jmash/oidc/service.swagger.json)
[Proto3](/openapi/jmash/oidc/index.html)

## 工会会员模块

[Swagger](/openapi-ui/swagger/index.html?url=/openapi/xyvcard/union/union_rpc.swagger.json)
[Proto3](/openapi/xyvcard/union/index.html)

## 工会班组模块

[Swagger](/openapi-ui/swagger/index.html?url=/openapi/xyvcard/teams/teams_rpc.swagger.json)
[Proto3](/openapi/xyvcard/teams/index.html)

## 福利慰问模块

[Swagger](/openapi-ui/swagger/index.html?url=/openapi/xyvcard/benefits/benefits_rpc.swagger.json)
[Proto3](/openapi/xyvcard/benefits/index.html)

## 活动模块

[Swagger](/openapi-ui/swagger/index.html?url=/openapi/xyvcard/activity/activity_rpc.swagger.json)
[Proto3](/openapi/xyvcard/activity/index.html)

## 资产模块

[Swagger](/openapi-ui/swagger/index.html?url=/openapi/xyvcard/asset/asset_rpc.swagger.json)
[Proto3](/openapi/xyvcard/asset/index.html)

## 履职模块

[Swagger](/openapi-ui/swagger/index.html?url=/openapi/xyvcard/duties/duties_rpc.swagger.json)
[Proto3](/openapi/xyvcard/duties/index.html)

## 五小创新模块

[Swagger](/openapi-ui/swagger/index.html?url=/openapi/xyvcard/innovate/innovate_rpc.swagger.json)
[Proto3](/openapi/xyvcard/innovate/index.html)

## 申报模块

[Swagger](/openapi-ui/swagger/index.html?url=/openapi/xyvcard/apply/apply_rpc.swagger.json)
[Proto3](/openapi/xyvcard/apply/index.html)

## 财务模块

[Swagger](/openapi-ui/swagger/index.html?url=/openapi/xyvcard/finance/finance_rpc.swagger.json)
[Proto3](/openapi/xyvcard/finance/index.html)

## CMS模块

[Swagger](/openapi-ui/swagger/index.html?url=/openapi/xyvcard/cms/cms_rpc.swagger.json)
[Proto3](/openapi/xyvcard/cms/index.html)

## CMS前端模块

[Swagger](/openapi-ui/swagger/index.html?url=/openapi/xyvcard/front/front_rpc.swagger.json)
[Proto3](/openapi/xyvcard/front/index.html)

## 商圈安全模块

[Swagger](/openapi-ui/swagger/index.html?url=/openapi/xyvcard/safety/safety_rpc.swagger.json)
[Proto3](/openapi/xyvcard/safety/index.html)

## 会展管理模块

[Swagger](/openapi-ui/swagger/index.html?url=/openapi/xyvcard/mice/mice_rpc.swagger.json)
[Proto3](/openapi/xyvcard/mice/index.html)

## 公车管理模块

[Swagger](/openapi-ui/swagger/index.html?url=/openapi/xyvcard/mscar/mscar_rpc.swagger.json)
[Proto3](/openapi/xyvcard/mscar/index.html)

## 唐僧威卡core

[Swagger](/openapi-ui/swagger/index.html?url=/openapi/xyvcard/core/core_rpc.swagger.json)
[Proto3](/openapi/xyvcard/core/index.html)

## 唐僧威卡diy

[Swagger](/openapi-ui/swagger/index.html?url=/openapi/xyvcard/diy/diy_rpc.swagger.json)
[Proto3](/openapi/xyvcard/diy/index.html)

## 唐僧威卡名片

[Swagger](/openapi-ui/swagger/index.html?url=/openapi/xyvcard/vcard/vcard_rpc.swagger.json)
[Proto3](/openapi/xyvcard/vcard/index.html)

## 企业数据模块

[Swagger](/openapi-ui/swagger/index.html?url=/openapi/xyvcard/entdata/entdata_rpc.swagger.json)
[Proto3](/openapi/xyvcard/entdata/index.html)

## 电商平台模块

[Swagger](/openapi-ui/swagger/index.html?url=/openapi/xyvcard/emall/emall_rpc.swagger.json)
[Proto3](/openapi/xyvcard/emall/index.html)

## 电商店铺模块

[Swagger](/openapi-ui/swagger/index.html?url=/openapi/xyvcard/shop/shop_rpc.swagger.json)
[Proto3](/openapi/xyvcard/shop/index.html)

## 电商商品模块

[Swagger](/openapi-ui/swagger/index.html?url=/openapi/xyvcard/goods/goods_rpc.swagger.json)
[Proto3](/openapi/xyvcard/goods/index.html)

## 卡劵模块

[Swagger](/openapi-ui/swagger/index.html?url=/openapi/xyvcard/coupons/coupons_rpc.swagger.json)
[Proto3](/openapi/xyvcard/coupons/index.html)

## 落地机构模块

[Swagger](/openapi-ui/swagger/index.html?url=/openapi/xyvcard/insts/insts_rpc.swagger.json)
[Proto3](/openapi/xyvcard/insts/index.html)

## 代理商模块

[Swagger](/openapi-ui/swagger/index.html?url=/openapi/xyvcard/agent/agent_rpc.swagger.json)
[Proto3](/openapi/xyvcard/agent/index.html)

## 码牌设备模块

[Swagger](/openapi-ui/swagger/index.html?url=/openapi/xyvcard/qrcode/qrcode_rpc.swagger.json)
[Proto3](/openapi/xyvcard/qrcode/index.html)
## 支付模块

[Swagger](/openapi-ui/swagger/index.html?url=/openapi/xyvcard/payment/payment_rpc.swagger.json)
[Proto3](/openapi/xyvcard/payment/index.html)

## 商户模块

[Swagger](/openapi-ui/swagger/index.html?url=/openapi/xyvcard/merchant/merchant_rpc.swagger.json)
[Proto3](/openapi/xyvcard/merchant/index.html)