kubectl edit configmap coredns -n kube-system

kubectl get configmap -n kube-system -o wide

kubectl scale deployment coredns -n kube-system --replicas=0

kubectl scale deployment coredns -n kube-system --replicas=2
