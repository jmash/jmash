1.配置一,直接将,443,6479 提供给客户端.

stream {
    server {
       listen 443;
       proxy_connect_timeout 1s;
       proxy_timeout 3s;
       proxy_pass 192.168.49.2:30443;
    }
    server {
       listen 8080;
       proxy_connect_timeout 1s;
       proxy_timeout 3s;
       proxy_pass 192.168.49.2:30080;
    }
    server {
       listen 6379;
       proxy_connect_timeout 1s;
       proxy_timeout 3s;
       proxy_pass 192.168.49.2:30379;
    }
    server {
       listen 8443;
       proxy_connect_timeout 1s;
       proxy_timeout 3s;
       proxy_pass 192.168.49.2:8443;
    }
}

2.配置2 
//K8S, redis 
nginx.conf
//grpcs,https
ssl.jmash.crenjoy.com.conf
