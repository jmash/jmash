docker run -d -p 8081-8089:8081-8089 --name nexus -v /Users/zhangxy/nexus-data:/sonatype-work harbor.crenjoy.com/sonatype/nexus3




# x86-64
docker pull sonatype/nexus3:3.37.0
# arm64v8
docker pull klo2k/nexus3:3.35.0-02

# x86-64
mkdir -p /opt/server/nexus3/sonatype-work
chown 200.200 /opt/server/nexus3/sonatype-work
docker run -itd --name=nexus3 \
--net=host \
--privileged=true --restart always \
-e TZ=Asia/Shanghai \
-v /opt/server/nexus3/sonatype-work:/opt/sonatype/sonatype-work \
sonatype/nexus3:3.37.0


# arm64v8
mkdir -p /data/nexus3/sonatype-work
chown 200.200 /data/nexus3/sonatype-work
docker run -itd --name=nexus3 \
--net=host \
--privileged=true --restart always \
-e TZ=Asia/Shanghai \
-v /data/nexus3/sonatype-work:/opt/sonatype/sonatype-work \
klo2k/nexus3:3.35.0-02
