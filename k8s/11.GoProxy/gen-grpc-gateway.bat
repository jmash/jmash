
set cur_cd=%cd%

cd %~dp0

:: 初始化go mod 

go mod init jmash-go-proxy


:: 更新mod 

go mod tidy


:: build 

SET GOOS=windows
SET GOARCH=amd64

go build -o ./bin/main.exe ./src/main.go

SET CGO_ENABLED=0
SET GOOS=linux
SET GOARCH=amd64

go build -o ./bin/main_amd64 ./src/main.go

SET GOARCH=arm64
go build -o ./bin/main_arm64 ./src/main.go

cd %cur_cd%
