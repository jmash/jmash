
export CURRENT_DR=$(pwd)

cd $(dirname $0)

#  初始化go mod 
go mod init jmash-go-proxy

# 更新mod 
go mod tidy

#  build 

export GOOS=windows
export GOARCH=amd64

go build -o ./bin/main.exe ./src/main.go

export CGO_ENABLED=0
export GOOS=linux
export GOARCH=amd64

go build -o ./bin/main_amd64 ./src/main.go

export GOARCH=arm64
go build -o ./bin/main_arm64 ./src/main.go

cd $CURRENT_DIR
