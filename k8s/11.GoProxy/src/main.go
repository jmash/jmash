package main

import (
	"net/http"
	"log"

	"github.com/goproxy/goproxy"
)

func main() { 
    
	log.Println("Goproxy 0.0.0.0:8095! Start...")   
	http.ListenAndServe(":8095", &goproxy.Goproxy{})
	
}