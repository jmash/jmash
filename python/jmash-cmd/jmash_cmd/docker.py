import subprocess
import argparse
import pathlib 
import sys
# export PYTHONPATH="/data/work/jmash/python/jmash-cmd"
import jmash_cmd.mvn

def docker_vue_build_push(namespace,name,arch,version,mirror="docker.dev.crenjoy.com",username="jmash",password="Jmash123"):
    docker_base_build_push(f"{namespace}-{name}-main-{arch}:{version}",arch,"1.0.0",mirror,username,password)

def docker_build_push(namespace,name,arch,version,mirror="docker.dev.crenjoy.com",username="jmash",password="Jmash123"):
    mvn_version=jmash_cmd.mvn.mvn_pom_version(f"../{namespace}-{name}-service/pom.xml")
    print(f"\n{mvn_version}")
    docker_base_build_push(f"{namespace}-{name}-service-{arch}:{version}",arch,mvn_version,mirror,username,password)
    
def docker_sample_build_push(namespace,name,arch,version,mirror="docker.dev.crenjoy.com",username="jmash",password="Jmash123"):
    mvn_version=jmash_cmd.mvn.mvn_pom_version(f"../{namespace}-{name}/pom.xml")
    print(f"\n{mvn_version}")
    docker_base_build_push(f"{namespace}-{name}-service-{arch}:{version}",arch,mvn_version,mirror,username,password)

def docker_base_build_push(server_name,arch,mvn_version,mirror,username,password):
    """
  docker build 和 push 到docker 仓库
    """
    command1 = f"docker build --platform linux/{arch} --build-arg GOARCH={arch} --build-arg MVN_VERSION={mvn_version}  -t {server_name}  -f ./Dockerfile  ../"
    command2 = f"docker login {mirror} --username {username} --password {password}"
    command3 = f"docker tag   {server_name}  {mirror}/library/{server_name}"    
    command4 = f"docker push  {mirror}/library/{server_name}"
    try:
        print(f"\n{command1}")
        subprocess.run(command1, shell=True, check=True)
        print(f"\n{command2}")
        subprocess.run(command2, shell=True, check=True)
        print(f"\n{command3}")
        subprocess.run(command3, shell=True, check=True)
        print(f"\n{command4}")
        subprocess.run(command4, shell=True, check=True)
        print(f"应用程序{server_name}已push docker 仓库.")
    except subprocess.CalledProcessError as e:
        print(f"应用程序{server_name} 出错: {e}")

def docker_tag(server_name,oldmirror,newmirror):
    """
    标记docker应用
    """
    command1 = f"docker  pull {oldmirror}/library/{server_name}"
    command2 = f"docker  tag  {oldmirror}/library/{server_name}  {newmirror}/library/{server_name}"    
    command3 = f"docker  push {newmirror}/library/{server_name}"
    try:
        subprocess.run(command1, shell=True, check=True)
        subprocess.run(command2, shell=True, check=True)
        subprocess.run(command3, shell=True, check=True)
        print(f"应用程序{name}-service-{arch}:{version}已push docker 仓库.")
    except subprocess.CalledProcessError as e:
        print(f"应用程序{name}-service-{arch}:{version} 出错: {e}")


   
