import os
import subprocess
import shutil
import platform

def gw_go_work_dir(work_dir):
    """
    获取当前工作目录并保存
    """
    script_dir = os.path.abspath(work_dir)
    os.chdir(script_dir)
    print(f"工作路径：{script_dir}");
    return script_dir
    
def gw_go_init(work_dir,module_name):
    """
    初始化 Go 模块
    :param module_name: 模块名称
    """
    command = f"go mod init {module_name}"
    subprocess.run(command,cwd=work_dir, shell=True, check=True)

def gw_go_generate(work_dir,namespace,name):
    """
    生成 Go gRPC 网关
    :param proto_files: proto 文件列表
    """
    base_proto_dir = f"../{namespace}-{name}-lib/src/main/proto"
    output_dir = f"./src"
    openapi_dir = f"./openapi"
    if not os.path.exists(f"{openapi_dir}"):
       os.mkdir(f"{openapi_dir}")
       
    for root,dirs,files in os.walk(base_proto_dir):
       for file in files :
           if file.endswith('.proto'):
              relation_path=os.path.relpath(root,base_proto_dir)
              proto_pattern = f"{relation_path}/*.proto"
              doc_dir = f"{openapi_dir}/{relation_path}"
              # GRPC
              gw_go_generate_grpc(work_dir,base_proto_dir,output_dir,openapi_dir,proto_pattern)
              # html 
              gw_go_generate_docs(work_dir,base_proto_dir,output_dir,doc_dir,proto_pattern)
              break
    

        
def gw_go_generate_grpc(work_dir,base_proto_dir,output_dir,openapi_dir,proto_pattern):
    """
    生成 Go gRPC 网关
    :param proto_files: proto 文件列表
    """
    command = (
        f"protoc -I {base_proto_dir} --go_out {output_dir} "
        f"--go-grpc_out {output_dir} --grpc-gateway_out={output_dir} "
        f"--openapiv2_out={openapi_dir} {base_proto_dir}/{proto_pattern}"
    )
    print(f"{command}")
    subprocess.run(command,cwd=work_dir, shell=True, check=True)
    
def gw_go_generate_docs(work_dir,base_proto_dir,output_dir,doc_dir,proto_pattern):
    """
    生成 OpenAPI 文档
    :param proto_pattern: proto 文件匹配模式
    """
    html_command = (
        f"protoc -I {base_proto_dir} --doc_out={doc_dir} "
        f"--doc_opt=html,index.html {base_proto_dir}/{proto_pattern}"
    )
    markdown_command = (
        f"protoc -I {base_proto_dir} --doc_out={doc_dir} "
        f"--doc_opt=markdown,readme.md {base_proto_dir}/{proto_pattern}"
    )
    print(f"{html_command}")
    print(f"{markdown_command}")
    subprocess.run(html_command, cwd=work_dir,shell=True, check=True)
    subprocess.run(markdown_command, cwd=work_dir,shell=True, check=True)


def gw_go_update(work_dir):
    """
    更新 Go 模块
    """
    command = "go mod tidy"
    print(f"{command}")
    subprocess.run(command, cwd=work_dir,shell=True, check=True)

def gw_copy_file(work_dir):
    """
    创建 bin 目录并复制 openapi 目录
    """
    bin_dir = os.path.join(work_dir, 'bin')
    openapi_dir = os.path.join(work_dir, 'openapi')
    if not os.path.exists(f"{bin_dir}"):
        os.mkdir(f"{bin_dir}")
    shutil.copytree(openapi_dir, os.path.join(bin_dir, 'openapi'), dirs_exist_ok=True)

def gw_go_build_app(work_dir,arch):
    """
    构建 Go 应用程序
    """           
    # 构建本地可执行文件
    gw_go_build_command(f"{work_dir}/bin/main",f"{work_dir}/src/main.go","windows","amd64")

    # 构建 Linux Arch 可执行文件
    gw_go_build_command(f"{work_dir}/bin/main",f"{work_dir}/src/main.go","linux",f"{arch}")
    
def gw_go_build_command(dist,src,sysos,arch):
    """
    构建 Go 应用程序
    """
    # 构建可执行文件
    os.environ["CGO_ENABLED"] = "0"    
    os.environ["GOOS"] = f"{sysos}"
    os.environ["GOARCH"] = f"{arch}"
    name = f"{dist}_{arch}"
    if sysos == "windows":
       name = f"{dist}_{arch}.exe"
    
    build_command = f"go build -o {name} {src}"
    print(f"{build_command}")
    subprocess.run(build_command, shell=True, check=True)

def gw_go_get(work_dir,project,version):
    """
    更新 Go get 
    """
    command = f"go get {project}@{version}"
    print(f"{command}")
    subprocess.run(command, cwd=work_dir,shell=True, check=True)
    
def gw_go_copy_openapi(work_dir,go_gateway):    
    # 执行 go list -m 命令
    try:
        result = subprocess.run(['go', 'list', '-m', f"{go_gateway}"], capture_output=True, text=True, check=True)
        output = result.stdout.strip()
        parts = output.split(' ')

        # 构建源目录和目标目录路径
        go_mod_path = os.path.expanduser(f'~/go/pkg/mod/{parts[0]}@{parts[1]}')
        source_dir = os.path.join(go_mod_path, 'openapi')
        target_dir = './openapi/'

        # 创建目标目录（如果不存在）
        if not os.path.exists(target_dir):
            os.makedirs(target_dir,exist_ok=True)

        # 复制目录
        shutil.copytree(source_dir, target_dir, dirs_exist_ok=True)
        subprocess.run(f"chmod -R u+rw {target_dir}", cwd=work_dir,shell=True, check=True)
                        
        print(f"{source_dir} 目录复制成功！")
    except subprocess.CalledProcessError as e:
        print(f"执行 go list 命令时出错: {e.stderr.strip()}")
    except FileNotFoundError as e:
        print(f"源目录或目标目录不存在: {e}")
    except Exception as e:
        print(f"发生未知错误: {e}")

def gw_main(work_dir,arch,namespace,name):
    current_path = os.getcwd()
    # 保存当前目录并切换到脚本所在目录
    work_dir=gw_go_work_dir(work_dir)

    # 生成 Generate
    gw_go_generate(work_dir,namespace,name)

    # 复制文件
    gw_copy_file(work_dir)

    # 更新 Go 模块
    gw_go_update(work_dir)

    # 构建 Go 应用程序
    gw_go_build_app(work_dir,arch)

    # 切换回原来的目录
    os.chdir(current_path)
    print(f"工作路径：{current_path}");

if __name__ == "__main__":
    gw_main(".","amd64","jmash","captcha")
    gw_go_get(".","gitee.com/jmash/jmash/trunk/jmash-core-gateway","master")
    
    
