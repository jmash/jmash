import subprocess
import argparse
import pathlib 
import sys

def k8s_text_replace(file_path, replacements, new_file_path):
    """
    替换多个文本并保存新文件
    """
    with open(file_path, 'r') as file:
        content = file.read()

    for old_text, new_text in replacements:
        content = content.replace(old_text, new_text)

    with open(new_file_path, 'w') as file:
        file.write(content)


def k8s_deploy(namespace,name,arch,version,mirror="docker.dev.crenjoy.com"):
    """
    部署K8S应用程序
    """      
    k8s_deploy_main(".",namespace,name,arch,version,mirror)
    k8s_deploy_subapp(".")
    
        
def k8s_deploy_main(work_dir,namespace,name,arch,version,mirror="docker.dev.crenjoy.com"):
    """
    部署K8S应用程序
    """        
    if mirror=="harbor.crenjoy.com:9443":
       mirror="harbor.crenjoy.com"
    replacements = [('{mirror}', mirror), ('{arch}', arch), ('{version}', version), ]
    
    k8s_text_replace("k8s-deployment-template.yaml",replacements,f"k8s-deployment-{arch}.yaml")
    
    if version=="1.0.0":
       k8s_restart_main(namespace,name,arch)

    command1 = f"kubectl apply -f k8s-configmap.yaml"    
    command2 = f"kubectl apply -f k8s-deployment-{arch}.yaml"
    try:
         subprocess.run(command1,cwd=work_dir, shell=True, check=True)
         subprocess.run(command2,cwd=work_dir, shell=True, check=True)
         print(f"应用程序{name}已成功部署到 {arch} 架构的节点上。")
    except subprocess.CalledProcessError as e:
        print(f"部署应用程序{name}时出错: {e}")
        
def k8s_deploy_subapp(work_dir):
    """
    部署K8S应用程序
    """        
    #print(work_dir)
    command1 = f"kubectl apply -f k8s-service.yaml"
    command2 = f"kubectl apply -f k8s-ingress.yaml"
    command3 = f"kubectl apply -f k8s-ingress-gateway.yaml"
    try:
         subprocess.run(command1,cwd=work_dir, shell=True, check=True)
         subprocess.run(command2,cwd=work_dir, shell=True, check=True)
         subprocess.run(command3,cwd=work_dir, shell=True, check=True)
         print(f"应用程序{work_dir}已成功部署到 节点上。")
    except subprocess.CalledProcessError as e:
        print(f"部署应用程序{work_dir}时出错: {e}")

def k8s_deploy_rm_subapp(namespace,name):
    k8s_deploy_subapp(f"./{namespace}-{name}-k8s")
    k8s_scale(namespace,name,0)
        
def k8s_restart(namespace,name,arch):
    """
    重启K8S应用程序
    """
    k8s_restart_main(namespace,name,arch)   
    k8s_deploy_subapp(".")

def k8s_restart_main(namespace,name,arch):
    """
    重启K8S应用程序
    """
    command1 = f"kubectl apply -f k8s-configmap.yaml" 
    command2 = f"kubectl rollout restart deploy {namespace}-{name}-service-deploy -n {namespace} "    
    try:
        subprocess.run(command1, shell=True, check=True)
        subprocess.run(command2, shell=True, check=True)
        print(f"应用程序{name}已成功重启。")
    except subprocess.CalledProcessError as e:
        print(f"启动应用程序{name}时出错: {e}")

def k8s_delete(namespace,name,arch):
    """
    重启K8S应用程序
    """
    k8s_delete_main(namespace,name,arch)   
    k8s_delete_subapp(".")

def k8s_delete_main(namespace,name,arch):
    """
    删除K8S应用程序
    """
    command1 = f"kubectl delete -f k8s-configmap.yaml" 
    command2 = f"kubectl delete -f k8s-deployment-{arch}.yaml"    
    try:
        subprocess.run(command1, shell=True, check=True)
        subprocess.run(command2, shell=True, check=True)
        print(f"应用程序{name}已成功在 {arch} 架构的节点上停止。")
    except subprocess.CalledProcessError as e:
        print(f"停止应用程序{name}时出错: {e}")
        
def k8s_delete_subapp(work_dir):
    """
    删除K8S应用程序
    """   
    command1 = f"kubectl delete -f k8s-service.yaml"
    command2 = f"kubectl delete -f k8s-ingress.yaml"
    command3 = f"kubectl delete -f k8s-ingress-gateway.yaml"
    try:
        subprocess.run(command1,cwd=work_dir, shell=True, check=True)
        subprocess.run(command2,cwd=work_dir, shell=True, check=True)
        subprocess.run(command3,cwd=work_dir, shell=True, check=True)
        print(f"应用程序{name}已成功在 {arch} 架构的节点上停止。")
    except subprocess.CalledProcessError as e:
        print(f"停止应用程序{name}时出错: {e}")        
        
def k8s_scale(namespace,name,replicas):
    """
    设置副本数量K8S应用程序
    """
    command1 = f"kubectl scale deployment {namespace}-{name}-service-deploy  --replicas={replicas} -n {namespace} "  
    try:
        subprocess.run(command1, shell=True, check=True) 
        print(f"应用程序{name}-service-deploy 设置副本数量{replicas}。")
    except subprocess.CalledProcessError as e:
        print(f"设置副本数量应用程序{name}时出错: {e}")

def k8s_test():
    """
    本地测试服务
    """
    command1 = f"kubectl apply -f k8s-test-ingress.yaml"  
    command2 = f"kubectl apply -f k8s-test-ingress-gateway.yaml"  
    try:
        subprocess.run(command1, shell=True, check=True) 
        subprocess.run(command2, shell=True, check=True) 
        print(f"应用程序 开启本地测试服务。")
    except subprocess.CalledProcessError as e:
        print(f"应用程序 开启本地测试服务时出错: {e}")
        
def k8s_notest():
    """
    停止本地测试服务
    """
    command1 = f"kubectl apply -f k8s-ingress-gateway.yaml"  
    try:
        subprocess.run(command1, shell=True, check=True) 
        print(f"应用程序 停止测试服务。")
    except subprocess.CalledProcessError as e:
        print(f"应用程序 停止测试服务时出错: {e}")

def k8s_vue_deploy(name ,arch,version,mirror="docker.dev.crenjoy.com"):
    """
    部署K8S应用程序
    """      
    if mirror=="harbor.crenjoy.com:9443":
       mirror="harbor.crenjoy.com"
    replacements = [('{mirror}', mirror), ('{arch}', arch), ('{version}', version), ]
    
    k8s_text_replace("k8s-deployment-template.yaml",replacements,f"k8s-deployment-{arch}.yaml")

    command1 = f"kubectl apply -f k8s-deployment-{arch}.yaml"
    command2 = f"kubectl apply -f k8s-service.yaml"
    command3 = f"kubectl apply -f k8s-ingress.yaml"
    try:
         subprocess.run(command1, shell=True, check=True)
         subprocess.run(command2, shell=True, check=True)
         subprocess.run(command3, shell=True, check=True)
         print(f"应用程序{name}已成功部署到 {arch} 架构的节点上。")
    except subprocess.CalledProcessError as e:
        print(f"部署应用程序{name}时出错: {e}")
        
def k8s_vue_restart(namespace,name , arch):
    """
    重启K8S应用程序
    """
    command1 = f"kubectl rollout restart deploy {namespace}-{name}-main-deploy -n {namespace} "    
    command2 = f"kubectl apply -f k8s-service.yaml"
    command3 = f"kubectl apply -f k8s-ingress.yaml"
    try:
        subprocess.run(command1, shell=True, check=True)
        subprocess.run(command2, shell=True, check=True)
        subprocess.run(command3, shell=True, check=True)
        print(f"应用程序{name}已成功重启。")
    except subprocess.CalledProcessError as e:
        print(f"启动应用程序{name}时出错: {e}")
        
def k8s_vue_delete(name ,arch):
    """
    删除K8S应用程序
    """
    command1 = f"kubectl delete -f k8s-deployment-{arch}.yaml"    
    command2 = f"kubectl delete -f k8s-service.yaml"
    command3 = f"kubectl delete -f k8s-ingress.yaml"
    
    try:
        subprocess.run(command1, shell=True, check=True)
        subprocess.run(command2, shell=True, check=True)
        subprocess.run(command3, shell=True, check=True)
        print(f"应用程序{name}已成功在 {arch} 架构的节点上停止。")
    except subprocess.CalledProcessError as e:
        print(f"停止应用程序{name}时出错: {e}")
    
def k8s_vue_scale(namespace,name,replicas):
    """
    设置副本数量K8S应用程序
    """
    command1 = f"kubectl scale deployment {namespace}-{name}-main-deploy  --replicas={replicas} -n {namespace} "  
    try:
        subprocess.run(command1, shell=True, check=True) 
        print(f"应用程序{name}-main-deploy 设置副本数量{replicas}。")
    except subprocess.CalledProcessError as e:
        print(f"设置副本数量应用程序{name}时出错: {e}")
