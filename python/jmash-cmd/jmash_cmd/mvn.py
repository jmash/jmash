import xml.etree.ElementTree as ET
import subprocess
import argparse
import pathlib 
import sys

def mvn_pom_version(path):
    """
    获取Maven POM 文件中定义的版本.
    """
    try:
        # 解析pom.xml文件
        tree = ET.parse(path)
        root = tree.getroot()
        # 定义命名空间
        namespace = {'m': 'http://maven.apache.org/POM/4.0.0'}
        # 查找版本元素
        version_element = root.find('m:version', namespace)
        if version_element is not None:
            return version_element.text
        # 如果根元素中没有版本信息，查找父元素中的版本信息
        parent = root.find('m:parent', namespace)
        if parent is not None:
            parent_version_element = parent.find('m:version', namespace)
            if parent_version_element is not None:
                return parent_version_element.text
        return None
    except Exception as e:
        print(f"Error reading pom.xml: {e}")
        return None


def mvn_exec_command(path,cmd):
    command = f"mvn -f {path} {cmd}"
    try:
        result = subprocess.run(command, shell=True, check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        print(f"{command}  执行成功！")
        #print("标准输出：", result.stdout.decode())
        return True
    except subprocess.CalledProcessError as e:
        print("{command}  命令执行失败！")
        print("错误码：", e.returncode)
        print("标准输出：", e.stdout.decode() if e.stdout else "")
        print("标准错误：", e.stderr.decode())
        return False

def mvn_build(path):
    # 获取版本号
    version = mvn_pom_version(path)
    if version is not None:
        result = False
        if '-SNAPSHOT' in version:
            # 快照版本执行的命令
            result = mvn_exec_command(path,"clean source:jar deploy")
        else:
            # 正式版本执行的命令
            result = mvn_exec_command(path,"clean package")
        return result
    else:
        print(f"Could not determine the version from {path}.")
        return False
        
def mvn_main(namespace,name):
    """
    Project Maven打包
    """
    mvn_build(f"../{namespace}-{name}-lib/pom.xml")

    mvn_build(f"../{namespace}-{name}-service/pom.xml")

def mvn_main_sample(namespace,name):
    """
    Project Maven打包
    """
    mvn_build(f"../{namespace}-{name}/pom.xml")


   
