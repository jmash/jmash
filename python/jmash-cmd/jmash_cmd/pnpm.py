import xml.etree.ElementTree as ET
import subprocess
import argparse
import pathlib 
import sys


def pnpm_build(work_dir,command):
    try:
        print(f"{work_dir}/{command}")
        result = subprocess.run(command,cwd=work_dir, shell=True, check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        print(f"{command}  执行成功！")
        #print("标准输出：", result.stdout.decode())
        return True
    except subprocess.CalledProcessError as e:
        print("{command}  命令执行失败！")
        print("错误码：", e.returncode)
        print("标准输出：", e.stdout.decode() if e.stdout else "")
        print("标准错误：", e.stderr.decode())
        return False

        
def pnpm_main(namespace,name):
    """
    Project NPM打包
    """
    pnpm_build(f"../{namespace}-{name}","pnpm install")

    pnpm_build(f"../{namespace}-{name}","pnpm run build")


   
