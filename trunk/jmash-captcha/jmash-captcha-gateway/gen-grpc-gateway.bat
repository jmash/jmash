
set cur_cd=%cd%

cd %~dp0

:: 初始化go mod 
go mod init gitee.com/jmash/jmash/trunk/jmash-captcha/jmash-captcha-gateway

:: 生成Go Grpc网关
protoc -I ../jmash-captcha-lib/src/main/proto  --go_out ./src  --go-grpc_out ./src   --grpc-gateway_out=./src  --openapiv2_out=./openapi  ../jmash-captcha-lib/src/main/proto/jmash/captcha/resources.proto
protoc -I ../jmash-captcha-lib/src/main/proto  --go_out ./src  --go-grpc_out ./src   --grpc-gateway_out=./src  --openapiv2_out=./openapi  ../jmash-captcha-lib/src/main/proto/jmash/captcha/service.proto

:: 生成OpenApi文档
protoc -I ../jmash-captcha-lib/src/main/proto --doc_out=./openapi/jmash/captcha --doc_opt=html,index.html  ../jmash-captcha-lib/src/main/proto/jmash/captcha/*.proto
protoc -I ../jmash-captcha-lib/src/main/proto --doc_out=./openapi/jmash/captcha --doc_opt=markdown,readme.md  ../jmash-captcha-lib/src/main/proto/jmash/captcha/*.proto

 

:: 更新mod 
go mod tidy

:: build 

go build -o ./bin/main.exe ./src/main.go

SET CGO_ENABLED=0
SET GOOS=linux
SET GOARCH=amd64
go build -o ./bin/main_x86 ./src/main.go

SET GOARCH=arm64
go build -o ./bin/main_arm ./src/main.go


cd %cur_cd%
