export CURRENT_DR=$(pwd)

cd $(dirname $0)

# 初始化go mod 
go mod init gitee.com/jmash/jmash/trunk/jmash-captcha/jmash-captcha-gateway

# 生成Go Grpc网关
protoc -I ../jmash-captcha-lib/src/main/proto  --go_out ./src  --go-grpc_out ./src   --grpc-gateway_out=./src  --openapiv2_out=./openapi  ../jmash-captcha-lib/src/main/proto/jmash/captcha/resources.proto
protoc -I ../jmash-captcha-lib/src/main/proto  --go_out ./src  --go-grpc_out ./src   --grpc-gateway_out=./src  --openapiv2_out=./openapi  ../jmash-captcha-lib/src/main/proto/jmash/captcha/service.proto

# 生成OpenApi文档
protoc -I ../jmash-captcha-lib/src/main/proto --doc_out=./openapi/jmash/captcha --doc_opt=html,index.html  ../jmash-captcha-lib/src/main/proto/jmash/captcha/*.proto
protoc -I ../jmash-captcha-lib/src/main/proto --doc_out=./openapi/jmash/captcha --doc_opt=markdown,readme.md  ../jmash-captcha-lib/src/main/proto/jmash/captcha/*.proto

#  copy 
mkdir ./bin
cp -rf ./openapi ./bin/

# 更新mod 
go mod tidy

# build 

go build -o ./bin/main.exe ./src/main.go

export CGO_ENABLED=0
export GOOS=linux
export GOARCH=amd64
go build -o ./bin/main_amd64 ./src/main.go

export GOARCH=arm64
go build -o ./bin/main_arm64 ./src/main.go


cd $CURRENT_DIR
