// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.2.0
// - protoc             v4.25.1
// source: jmash/captcha/service.proto

package captcha

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	emptypb "google.golang.org/protobuf/types/known/emptypb"
	wrapperspb "google.golang.org/protobuf/types/known/wrapperspb"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// CaptchaClient is the client API for Captcha service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type CaptchaClient interface {
	// 检查验证码服务是否正常
	Ping(ctx context.Context, in *emptypb.Empty, opts ...grpc.CallOption) (*wrapperspb.StringValue, error)
	// 获取一个验证码图形挑战
	Create(ctx context.Context, in *emptypb.Empty, opts ...grpc.CallOption) (*Challenge, error)
	// 验证用户输入验证码文本是否正确
	Validate(ctx context.Context, in *CaptchaReq, opts ...grpc.CallOption) (*wrapperspb.BoolValue, error)
}

type captchaClient struct {
	cc grpc.ClientConnInterface
}

func NewCaptchaClient(cc grpc.ClientConnInterface) CaptchaClient {
	return &captchaClient{cc}
}

func (c *captchaClient) Ping(ctx context.Context, in *emptypb.Empty, opts ...grpc.CallOption) (*wrapperspb.StringValue, error) {
	out := new(wrapperspb.StringValue)
	err := c.cc.Invoke(ctx, "/jmash.captcha.Captcha/Ping", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *captchaClient) Create(ctx context.Context, in *emptypb.Empty, opts ...grpc.CallOption) (*Challenge, error) {
	out := new(Challenge)
	err := c.cc.Invoke(ctx, "/jmash.captcha.Captcha/Create", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *captchaClient) Validate(ctx context.Context, in *CaptchaReq, opts ...grpc.CallOption) (*wrapperspb.BoolValue, error) {
	out := new(wrapperspb.BoolValue)
	err := c.cc.Invoke(ctx, "/jmash.captcha.Captcha/Validate", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// CaptchaServer is the server API for Captcha service.
// All implementations must embed UnimplementedCaptchaServer
// for forward compatibility
type CaptchaServer interface {
	// 检查验证码服务是否正常
	Ping(context.Context, *emptypb.Empty) (*wrapperspb.StringValue, error)
	// 获取一个验证码图形挑战
	Create(context.Context, *emptypb.Empty) (*Challenge, error)
	// 验证用户输入验证码文本是否正确
	Validate(context.Context, *CaptchaReq) (*wrapperspb.BoolValue, error)
	mustEmbedUnimplementedCaptchaServer()
}

// UnimplementedCaptchaServer must be embedded to have forward compatible implementations.
type UnimplementedCaptchaServer struct {
}

func (UnimplementedCaptchaServer) Ping(context.Context, *emptypb.Empty) (*wrapperspb.StringValue, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Ping not implemented")
}
func (UnimplementedCaptchaServer) Create(context.Context, *emptypb.Empty) (*Challenge, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Create not implemented")
}
func (UnimplementedCaptchaServer) Validate(context.Context, *CaptchaReq) (*wrapperspb.BoolValue, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Validate not implemented")
}
func (UnimplementedCaptchaServer) mustEmbedUnimplementedCaptchaServer() {}

// UnsafeCaptchaServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to CaptchaServer will
// result in compilation errors.
type UnsafeCaptchaServer interface {
	mustEmbedUnimplementedCaptchaServer()
}

func RegisterCaptchaServer(s grpc.ServiceRegistrar, srv CaptchaServer) {
	s.RegisterService(&Captcha_ServiceDesc, srv)
}

func _Captcha_Ping_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(emptypb.Empty)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CaptchaServer).Ping(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/jmash.captcha.Captcha/Ping",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CaptchaServer).Ping(ctx, req.(*emptypb.Empty))
	}
	return interceptor(ctx, in, info, handler)
}

func _Captcha_Create_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(emptypb.Empty)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CaptchaServer).Create(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/jmash.captcha.Captcha/Create",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CaptchaServer).Create(ctx, req.(*emptypb.Empty))
	}
	return interceptor(ctx, in, info, handler)
}

func _Captcha_Validate_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CaptchaReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CaptchaServer).Validate(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/jmash.captcha.Captcha/Validate",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CaptchaServer).Validate(ctx, req.(*CaptchaReq))
	}
	return interceptor(ctx, in, info, handler)
}

// Captcha_ServiceDesc is the grpc.ServiceDesc for Captcha service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var Captcha_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "jmash.captcha.Captcha",
	HandlerType: (*CaptchaServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Ping",
			Handler:    _Captcha_Ping_Handler,
		},
		{
			MethodName: "Create",
			Handler:    _Captcha_Create_Handler,
		},
		{
			MethodName: "Validate",
			Handler:    _Captcha_Validate_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "jmash/captcha/service.proto",
}
