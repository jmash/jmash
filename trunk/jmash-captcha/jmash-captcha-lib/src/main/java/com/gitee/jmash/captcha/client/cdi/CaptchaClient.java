
package com.gitee.jmash.captcha.client.cdi;

import com.gitee.jmash.common.grpc.client.GrpcChannel;
import jmash.captcha.service.CaptchaGrpc;

/** Captcha Client . */
public class CaptchaClient {

  /** CaptchaBlocking. */
  public static CaptchaGrpc.CaptchaBlockingStub getCaptchaBlockingStub() {
    return CaptchaGrpc.newBlockingStub(CaptchaClientConfig.getManagedChannel())
        .withCallCredentials(GrpcChannel.getCallCredentials());
  }

  /** CaptchaFuture. */
  public static CaptchaGrpc.CaptchaFutureStub getCaptchaFutureStub() {
    return CaptchaGrpc.newFutureStub(CaptchaClientConfig.getManagedChannel())
        .withCallCredentials(GrpcChannel.getCallCredentials());
  }

  /** Captcha. */
  public static CaptchaGrpc.CaptchaStub getCaptchaStub() {
    return CaptchaGrpc.newStub(CaptchaClientConfig.getManagedChannel())
        .withCallCredentials(GrpcChannel.getCallCredentials());
  }

}
