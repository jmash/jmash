
package com.gitee.jmash.captcha.client.cdi;

import com.gitee.jmash.common.grpc.client.GrpcChannel;
import io.grpc.ManagedChannel;

/** Captcha Client Config . */
public class CaptchaClientConfig {

  protected static ManagedChannel channel = null;

  /** ManagedChannel. */
  public static synchronized ManagedChannel getManagedChannel() {
    if (null != channel && !channel.isShutdown() && !channel.isTerminated()) {
      return channel;
    }
    // k8s环境获取后端服务,本地环境获取测试服务.
    channel = GrpcChannel.getServiceChannel("jmash-captcha-service.jmash.svc.cluster.local");
    return channel;
  }

}
