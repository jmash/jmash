
package com.gitee.jmash.captcha.cdi;

import com.gitee.jmash.core.grpc.DefaultGrpcServer;
import jakarta.enterprise.inject.se.SeContainer;
import jakarta.enterprise.inject.se.SeContainerInitializer;

/** Captcha Application. */
public class CaptchaApp {

  /**
   * 应用入口.
   */
  public static void main(String[] args) throws Exception {
    try (SeContainer container = SeContainerInitializer.newInstance().initialize()) {
      final DefaultGrpcServer server = container.select(DefaultGrpcServer.class).get();
      server.setReflection(true);
      server.start(false);
      server.blockUntilShutdown();
    }
  }

}
