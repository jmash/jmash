
package com.gitee.jmash.captcha.engine;

import com.jhlabs.image.PinchFilter;
import com.jhlabs.math.ImageFunction2D;
import com.octo.captcha.component.image.backgroundgenerator.BackgroundGenerator;
import com.octo.captcha.component.image.backgroundgenerator.UniColorBackgroundGenerator;
import com.octo.captcha.component.image.color.RandomListColorGenerator;
import com.octo.captcha.component.image.deformation.ImageDeformation;
import com.octo.captcha.component.image.deformation.ImageDeformationByBufferedImageOp;
import com.octo.captcha.component.image.fontgenerator.FontGenerator;
import com.octo.captcha.component.image.fontgenerator.RandomFontGenerator;
import com.octo.captcha.component.image.textpaster.GlyphsPaster;
import com.octo.captcha.component.image.textpaster.TextPaster;
import com.octo.captcha.component.image.textpaster.glyphsvisitor.GlyphsVisitors;
import com.octo.captcha.component.image.textpaster.glyphsvisitor.OverlapGlyphsUsingShapeVisitor;
import com.octo.captcha.component.image.textpaster.glyphsvisitor.TranslateAllToRandomPointVisitor;
import com.octo.captcha.component.image.textpaster.glyphsvisitor.TranslateGlyphsVerticalRandomVisitor;
import com.octo.captcha.component.image.wordtoimage.DeformedComposedWordToImage;
import com.octo.captcha.component.image.wordtoimage.WordToImage;
import com.octo.captcha.component.word.FileDictionary;
import com.octo.captcha.component.word.wordgenerator.ComposeDictionaryWordGenerator;
import com.octo.captcha.component.word.wordgenerator.WordGenerator;
import com.octo.captcha.engine.image.ListImageCaptchaEngine;
import com.octo.captcha.image.gimpy.GimpyFactory;
import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;

/**
 * Gmail Engine Captcha.
 *
 * @author CGD
 *
 */
public class JcaptchaEngine extends ListImageCaptchaEngine {

  /**
   * this method should be implemented as folow :.
   * <ul>
   * <li>First construct all the factories you want to initialize the gimpy
   * with</li>
   * <li>then call the this.addFactoriy method for each factory</li>
   * </ul>
   */
  protected void buildInitialFactories() {

    PinchFilter pinch = new PinchFilter();

    pinch.setAmount(-.5f);
    pinch.setRadius(30);
    pinch.setAngle((float) (Math.PI / 16));
    pinch.setCentreX(0.5f);
    pinch.setCentreY(-0.01f);
    pinch.setEdgeAction(ImageFunction2D.CLAMP);

    PinchFilter pinch2 = new PinchFilter();
    pinch2.setAmount(-.6f);
    pinch2.setRadius(70);
    pinch2.setAngle((float) (Math.PI / 16));
    pinch2.setCentreX(0.3f);
    pinch2.setCentreY(1.01f);
    pinch2.setEdgeAction(ImageFunction2D.CLAMP);

    PinchFilter pinch3 = new PinchFilter();
    pinch3.setAmount(-.6f);
    pinch3.setRadius(70);
    pinch3.setAngle((float) (Math.PI / 16));
    pinch3.setCentreX(0.8f);
    pinch3.setCentreY(-0.01f);
    pinch3.setEdgeAction(ImageFunction2D.CLAMP);

    List<ImageDeformation> textDef = new ArrayList<ImageDeformation>();
    textDef.add(new ImageDeformationByBufferedImageOp(pinch));
    textDef.add(new ImageDeformationByBufferedImageOp(pinch2));
    textDef.add(new ImageDeformationByBufferedImageOp(pinch3));

    // word generator
    WordGenerator dictionnaryWords = new ComposeDictionaryWordGenerator(
        new FileDictionary("toddlist"));
    // wordtoimage components
    TextPaster randomPaster = new GlyphsPaster(4, 5, new RandomListColorGenerator(

        new Color[] { //
            new Color(23, 170, 27), //
            new Color(220, 34, 11), //
            new Color(23, 67, 172) //
        }),

        new GlyphsVisitors[] { //
            new TranslateGlyphsVerticalRandomVisitor(0.1), //
            new OverlapGlyphsUsingShapeVisitor(3), //
            new TranslateAllToRandomPointVisitor() //
        });

    BackgroundGenerator back = new UniColorBackgroundGenerator(130, 50, new Color(238, 238, 238));
    FontGenerator shearedFont = new RandomFontGenerator(50, 50, //
        new Font[] { new Font("nyala", Font.BOLD, 50), //
            new Font("Bell MT", Font.PLAIN, 50), //
            new Font("Credit valley", Font.BOLD, 50) //
        }, false);
    // word2image 1
    WordToImage word2image;
    word2image = new DeformedComposedWordToImage(false, shearedFont, back, randomPaster,
        new ArrayList<ImageDeformation>(), new ArrayList<ImageDeformation>(), textDef

    );

    this.addFactory(new GimpyFactory(dictionnaryWords, word2image, false));

  }

}