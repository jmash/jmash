
package com.gitee.jmash.captcha.grpc;

import com.gitee.jmash.captcha.engine.JcaptchaEngine;
import com.gitee.jmash.captcha.store.CachedCaptchaStore;
import com.gitee.jmash.common.utils.ImageUtil;
import com.gitee.jmash.core.grpc.cdi.GrpcService;
import com.google.protobuf.BoolValue;
import com.google.protobuf.Empty;
import com.google.protobuf.StringValue;
import com.octo.captcha.service.CaptchaServiceException;
import com.octo.captcha.service.image.DefaultManageableImageCaptchaService;
import com.octo.captcha.service.image.ImageCaptchaService;
import io.grpc.Status;
import io.grpc.stub.StreamObserver;
import jakarta.inject.Inject;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validator;
import java.awt.image.BufferedImage;
import java.util.Set;
import java.util.UUID;
import jmash.captcha.protobuf.CaptchaReq;
import jmash.captcha.protobuf.Challenge;
import jmash.captcha.service.CaptchaGrpc;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Captcha Grpc.
 *
 * @author CGD
 *
 */
@GrpcService
public class CaptchaGrpcImpl extends CaptchaGrpc.CaptchaImplBase {

  private static Log log = LogFactory.getLog(CaptchaGrpcImpl.class);

  @Inject
  private Validator validator;

  // 经典验证码
  private static ImageCaptchaService instance = new DefaultManageableImageCaptchaService(
      new CachedCaptchaStore(), new JcaptchaEngine(), 180, 100000, 75000);

  @Override
  public void ping(Empty request, StreamObserver<StringValue> responseObserver) {
    responseObserver.onNext(StringValue.of("PONG"));
    responseObserver.onCompleted();
  }

  @Override
  public void create(Empty request, StreamObserver<Challenge> responseObserver) {
    try {
      Challenge.Builder builder = Challenge.newBuilder().setCaptchaId(UUID.randomUUID().toString());
      // 验证码问题
      String question = instance.getQuestionForID(builder.getCaptchaId());
      builder.setQuestion(question);
      // 验证码图形
      BufferedImage image = instance.getImageChallengeForID(builder.getCaptchaId());
      String base64Image = ImageUtil.imageToBase64Image(image);
      builder.setBase64Image(base64Image);

      responseObserver.onNext(builder.build());
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  public void validate(CaptchaReq request, StreamObserver<BoolValue> responseObserver) {
    try {

      Set<ConstraintViolation<CaptchaReq>> validResult = validator.validate(request);
      if (!validResult.isEmpty()) {
        ConstraintViolation<CaptchaReq> violation = validResult.iterator().next();
        responseObserver.onError(Status.INVALID_ARGUMENT
            .withDescription(violation.getPropertyPath() + " " + violation.getMessage())
            .asException());
        return;
      }

      Boolean r = instance.validateResponseForID(request.getCaptchaId(), request.getCaptchaCode());
      responseObserver.onNext(BoolValue.of(r));
      responseObserver.onCompleted();
    } catch (CaptchaServiceException ex) {
      log.error(ex.getMessage());
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

}
