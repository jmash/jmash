
package com.gitee.jmash.captcha.store;

import com.gitee.jmash.common.cache.SerialCache;
import com.octo.captcha.Captcha;
import com.octo.captcha.service.CaptchaServiceException;
import com.octo.captcha.service.captchastore.CaptchaAndLocale;
import com.octo.captcha.service.captchastore.CaptchaStore;
import jakarta.enterprise.inject.spi.CDI;
import java.util.Collection;
import java.util.Locale;

/**
 * Captcha验证码缓存.
 *
 * @author CGD
 *
 */
public class CachedCaptchaStore implements CaptchaStore {
  
  private static final String KEY="captcha:";

  private static SerialCache getCache() {
    return CDI.current().select(SerialCache.class).get();
  }

  public boolean hasCaptcha(String id) {
    return getCache().containsKey(KEY+id);
  }

  public void storeCaptcha(String id, Captcha captcha) throws CaptchaServiceException {
    // 10分钟
    getCache().put(KEY+id, new CaptchaAndLocale(captcha), 600);
  }

  public void storeCaptcha(String id, Captcha captcha, Locale locale)
      throws CaptchaServiceException {
    // 10分钟
    getCache().put(KEY+id, new CaptchaAndLocale(captcha, locale), 600);
  }

  public Captcha getCaptcha(String id) throws CaptchaServiceException {
    Object captchaAndLocale = getCache().get(KEY+id);
    return (captchaAndLocale != null) ? ((CaptchaAndLocale) captchaAndLocale).getCaptcha() : null;
  }

  public Locale getLocale(String id) throws CaptchaServiceException {
    Object captchaAndLocale = getCache().get(KEY+id);
    return (captchaAndLocale != null) ? ((CaptchaAndLocale) captchaAndLocale).getLocale() : null;
  }

  /**
   * 移除缓存.
   */
  public boolean removeCaptcha(String id) {
    if (getCache().get(KEY+id) != null) {
      getCache().remove(KEY+id);
      return true;
    }
    return false;
  }

  public int getSize() {
    return getCache().keySet().size();
  }

  public Collection<?> getKeys() {
    return getCache().keySet();
  }

  public void empty() {
    getCache().clear();
  }

  public void initAndStart() {
  }

  public void cleanAndShutdown() {
    getCache().clear();
  }

}
