
package com.gitee.jmash.captcha.test;

import com.gitee.jmash.captcha.client.cdi.CaptchaClient;
import jakarta.enterprise.inject.se.SeContainer;
import jakarta.enterprise.inject.se.SeContainerInitializer;
import jmash.captcha.service.CaptchaGrpc.CaptchaBlockingStub;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;

/**
 * Captcha Test.
 *
 * @author CGD
 *
 */
@Disabled
public class CaptchaClientTest extends CaptchaTest {

  protected static SeContainer container;

  /**
   * Start.
   */
  @BeforeAll
  public static void setup() throws Exception {
    container = SeContainerInitializer.newInstance().initialize();
  }

  @Override
  public CaptchaBlockingStub getCaptchaStub() {
    return CaptchaClient.getCaptchaBlockingStub();
  }

  /**
   * Close.
   */
  @AfterAll
  public static void stop() throws InterruptedException {
    container.close();
  }
}
