
package com.gitee.jmash.captcha.test;

import com.gitee.jmash.common.grpc.client.GrpcChannel;
import com.gitee.jmash.core.grpc.DefaultGrpcServer;
import io.grpc.ManagedChannel;
import jakarta.enterprise.inject.se.SeContainer;
import jakarta.enterprise.inject.se.SeContainerInitializer;
import jmash.captcha.service.CaptchaGrpc;
import jmash.captcha.service.CaptchaGrpc.CaptchaBlockingStub;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;

/**
 * Captcha Test.
 *
 * @author CGD
 *
 */
public class CaptchaInProcessTest extends CaptchaTest {

  protected static DefaultGrpcServer server;

  protected static ManagedChannel channel = null;

  protected static SeContainer container;

  /**
   * Start.
   */
  @BeforeAll
  public static void setup() throws Exception {
    container = SeContainerInitializer.newInstance().initialize();
    server = container.select(DefaultGrpcServer.class).get();
    server.start(true);
    channel = GrpcChannel.getInProcessChannel();
  }

  @Override
  public CaptchaBlockingStub getCaptchaStub() {
    return CaptchaGrpc.newBlockingStub(channel);
  }

  /**
   * Close.
   */
  @AfterAll
  public static void stop() throws InterruptedException {
    server.stop();
    container.close();
  }

}
