
package com.gitee.jmash.captcha.test;

import com.gitee.jmash.common.config.AppProps;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import jakarta.enterprise.inject.se.SeContainer;
import jakarta.enterprise.inject.se.SeContainerInitializer;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jmash.captcha.protobuf.CaptchaReq;
import jmash.captcha.protobuf.Challenge;
import org.eclipse.microprofile.config.inject.ConfigProperties;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

/**
 * 标准Restful Client.
 *
 * @author CGD
 *
 */
@Disabled
public class CaptchaRestClientTest {

  protected static SeContainer container;

  static Client client;

  static WebTarget target;

  /**
   * Start.
   */
  @BeforeAll
  public static void setup() throws Exception {
    container = SeContainerInitializer.newInstance().initialize();
    AppProps appProps = container.select(AppProps.class, ConfigProperties.Literal.NO_PREFIX).get();
    client = ClientBuilder.newClient();
    target = client.target("https://" + appProps.getTestHost());
  }

  @Test
  @Order(1)
  public void pingTest() {
    Response res = target.path("/v1/captcha/ping").request(MediaType.APPLICATION_JSON).get();
    Assertions.assertEquals(res.getStatus(), 200);
    Assertions.assertEquals(res.readEntity(String.class), "\"PONG\"");
  }

  @Test
  @Order(2)
  public void createTest() throws InvalidProtocolBufferException {
    Response res = target.path("/v1/captcha/classic").request(MediaType.APPLICATION_JSON).get();
    Assertions.assertEquals(res.getStatus(), 200);
    String result = res.readEntity(String.class);
    Challenge.Builder builder = Challenge.newBuilder();
    JsonFormat.parser().merge(result, builder);
    Assertions.assertNotEquals(builder.build(), Challenge.getDefaultInstance());
  }

  @Test
  @Order(3)
  public void validateTest() throws InvalidProtocolBufferException {
    String req = JsonFormat.printer()
        .print(CaptchaReq.newBuilder().setCaptchaCode("test").setCaptchaId("test").build());
    Entity<String> reqEntity = Entity.entity(req, MediaType.APPLICATION_JSON);
    Response res = target.path("/v1/captcha/classic:validate").request(MediaType.APPLICATION_JSON)
        .post(reqEntity);
    Assertions.assertNotEquals(res.getStatus(), 200);
    String result = res.readEntity(String.class);
    System.out.println(result);
  }

  /**
   * Close.
   */
  @AfterAll
  public static void stop() throws InterruptedException {
    client.close();
    container.close();
  }

}
