
package com.gitee.jmash.captcha.test;

import com.gitee.jmash.common.utils.ImageUtil;
import com.google.protobuf.BoolValue;
import com.google.protobuf.Empty;
import com.google.protobuf.StringValue;
import io.grpc.StatusRuntimeException;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import jmash.captcha.protobuf.CaptchaReq;
import jmash.captcha.protobuf.Challenge;
import jmash.captcha.service.CaptchaGrpc;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

/**
 * Captcha Grpc 测试.
 *
 * @author CGD
 *
 */
@TestMethodOrder(OrderAnnotation.class)
public abstract class CaptchaTest {

  public abstract CaptchaGrpc.CaptchaBlockingStub getCaptchaStub();

  @Test
  @Order(1)
  public void pingTest() {
    StringValue value = getCaptchaStub().ping(Empty.getDefaultInstance());
    Assertions.assertEquals(value.getValue(), "PONG");
  }

  @Test
  @Order(2)
  public void createTest() throws IOException {
    Challenge value = getCaptchaStub().create(Empty.getDefaultInstance());
    Assertions.assertNotEquals("", value.getCaptchaId());
    Assertions.assertNotEquals("", value.getBase64Image());
    Assertions.assertNotEquals("", value.getQuestion());
    BufferedImage image = ImageUtil.imageFromBase64Image(value.getBase64Image());
    Assertions.assertNotNull(image);
    ImageIO.write(image, "webp", new File("target/default.webp"));

    BoolValue r = getCaptchaStub().validate(
        CaptchaReq.newBuilder().setCaptchaId(value.getCaptchaId()).setCaptchaCode("test").build());
    Assertions.assertFalse(r.getValue());
  }

  @Test
  @Order(3)
  public void validateTest() {
    Assertions.assertThrows(StatusRuntimeException.class, () -> {
      getCaptchaStub()
          .validate(CaptchaReq.newBuilder().setCaptchaId("test").setCaptchaCode("test").build());
    });
    Assertions.assertThrows(StatusRuntimeException.class, () -> {
      getCaptchaStub().validate(CaptchaReq.newBuilder().setCaptchaCode("test").build());
    });
    Assertions.assertThrows(StatusRuntimeException.class, () -> {
      getCaptchaStub().validate(CaptchaReq.newBuilder().setCaptchaId("test").build());
    });
  }

}
