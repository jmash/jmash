
package com.gitee.jmash.common.algorithm;

import com.gitee.jmash.common.utils.Md5Util;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;
import org.apache.commons.logging.LogFactory;

/**
 * CRC32 Provider 实现 java.security.Provider.
 *
 * @author CGD
 * 
 */
public class Crc32Provider extends Provider {

  private static final long serialVersionUID = 1L;

  public Crc32Provider() {
    super("CRC32Provider", "1.0", "crenjoy.com CRC32 algorithm");
    put("CRC32", "CRC32");
  }

  /** 测试CRC32. */
  public static void main(String[] args) throws NoSuchAlgorithmException, NoSuchProviderException {
    // 不能生效
    // Security.addProvider(new CRC32Provider());

    MessageDigest md = new Crc32MessageDigest();
    byte[] result = md.digest("0123456789".getBytes());

    String resultString = Md5Util.byteArrayToHexString(result);
    LogFactory.getLog(Crc32Provider.class).info(resultString);
  }

}
