/**
 * from com.alisoft.xplatform.asf.cache.ICache
 */

package com.gitee.jmash.common.cache;

import java.time.Instant;
import java.util.Collection;
import java.util.Set;

/**
 * Cache统一接口.
 *
 * @author wenchu.cenwc
 *
 */
public interface Cache<K, V> {
  
  /**
   * 保存数据.
   */
  public V put(K key, V value);

  /**
   * 保存有有效期的数据.
   *
   * @param expiry 有效期
   */
  public V put(K key, V value, Instant expiry);

  /**
   * 保存有有效期的数据.
   *
   * @param ttl 数据超时的秒数
   */
  public V put(K key, V value, int ttl);

  /**
   * 获取缓存数据.
   */
  public V get(K key);

  /**
   * 移出缓存数据.
   */
  public V remove(K key);

  /**
   * 删除所有缓存内的数据.
   */
  public boolean clear();

  /**
   * 缓存数据数量.
   */
  public int size();

  /**
   * 缓存所有的key的集合.
   */
  public Set<K> keySet();

  /**
   * 缓存的所有value的集合.
   */
  public Collection<V> values();

  /**
   * 是否包含了指定key的数据.
   */
  public boolean containsKey(K key);

  /**
   * 释放Cache占用的资源.
   */
  public void destroy();

}
