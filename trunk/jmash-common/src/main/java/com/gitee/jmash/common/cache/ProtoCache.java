package com.gitee.jmash.common.cache;

import com.google.protobuf.Message;

/**
 * Protobuf Cache.
 *
 * @author CGD
 *
 */
public interface ProtoCache extends Cache<String, Message> {

}
