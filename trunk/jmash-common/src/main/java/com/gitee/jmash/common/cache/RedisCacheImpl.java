
package com.gitee.jmash.common.cache;

import com.gitee.jmash.common.redis.RedisProps;
import com.gitee.jmash.common.redis.RedisUtil;
import com.gitee.jmash.common.redis.callback.RadisTransCallback;
import com.gitee.jmash.common.redis.callback.RedisTemplate;
import com.gitee.jmash.common.utils.SerializationUtil;
import jakarta.enterprise.context.Dependent;
import jakarta.enterprise.inject.Typed;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import java.io.Serializable;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.Transaction;
import redis.clients.jedis.params.ScanParams;
import redis.clients.jedis.resps.ScanResult;

/**
 * Redis Cache Impl.
 *
 * @author CGD
 *
 */
@Typed(SerialCache.class)
@Dependent
public class RedisCacheImpl implements SerialCache {

  @Inject
  @Named
  private RedisTemplate redisTemplate;
  // 默认100.
  private Integer scanCount = 100;

  public RedisCacheImpl() {
    super();
  }

  /** Not Use CDI. */
  public RedisCacheImpl(RedisProps redisProps) {
    super();
    JedisPool jedisPool = RedisUtil.getJedisPool(redisProps);
    this.redisTemplate = new RedisTemplate(jedisPool);
  }

  public RedisTemplate getRedisTemplate() {
    return redisTemplate;
  }

  public void setRedisTemplate(RedisTemplate redisTemplate) {
    this.redisTemplate = redisTemplate;
  }

  @Override
  public Serializable put(String key, Serializable value) {
    return getRedisTemplate().transactionMulti(new RadisTransCallback<Serializable>() {
      public Serializable call(Transaction transaction) {
        transaction.set(key.getBytes(), SerializationUtil.serialize(value));
        return value;
      }
    });
  }

  @Override
  public Serializable put(String key, Serializable value, Instant expiry) {
    Long second = expiry.getEpochSecond() - Instant.now().getEpochSecond();
    return put(key, value, second.intValue());
  }

  @Override
  public Serializable put(String key, Serializable value, int ttl) {
    return getRedisTemplate().transactionMulti(new RadisTransCallback<Serializable>() {
      public Serializable call(Transaction transaction) {
        transaction.setex(key.getBytes(), ttl, SerializationUtil.serialize(value));
        return value;
      }
    });
  }

  @Override
  public Serializable get(String key) {
    try (Jedis jedis = getRedisTemplate().getResource()) {
      return getObject(key, jedis);
    }
  }

  @Override
  public Serializable remove(String key) {
    Serializable obj = get(key);
    getRedisTemplate().transactionMulti(new RadisTransCallback<Boolean>() {
      public Boolean call(Transaction transaction) {
        transaction.del(key.getBytes());
        return true;
      }
    });
    return obj;
  }

  @Override
  public boolean clear() {
    return clear("");
  }

  @Override
  public int size() {
    return keySet().size();
  }

  @Override
  public Set<String> keySet() {
    return scanKeySet("");
  }

  @Override
  public Collection<Serializable> values() {
    final Set<String> keys = keySet();
    try (Jedis jedis = getRedisTemplate().getResource()) {
      Collection<Serializable> list = new ArrayList<>();
      for (String key : keys) {
        Serializable obj = getObject(key, jedis);
        list.add(obj);
      }
      return list;
    }
  }

  @Override
  public boolean containsKey(String key) {
    try (Jedis jedis = getRedisTemplate().getResource()) {
      return jedis.exists(key.getBytes());
    }
  }



  public ScanResult<String> scan(String cursor, ScanParams scanParams) {
    try (Jedis jedis = getRedisTemplate().getResource()) {
      ScanResult<String> scanResult = jedis.scan(cursor, scanParams);
      return scanResult;
    }
  }

  @Override
  public Set<String> scanKeySet(String match) {
    ScanParams scanParams = new ScanParams();
    scanParams.match(match);
    scanParams.count(scanCount);
    String cursor = "0";
    Set<String> list = new HashSet<>();
    do {
      ScanResult<String> scanResult = scan(cursor, scanParams);
      cursor = scanResult.getCursor();
      list.addAll(scanResult.getResult());
    } while (!"0".equals(cursor));
    return list;
  }



  @Override
  public boolean clearScan(String match) {
    ScanParams scanParams = new ScanParams();
    scanParams.match(match);
    scanParams.count(scanCount);
    String cursor = "0";
    do {
      ScanResult<String> scanResult = scan(cursor, scanParams);
      cursor = scanResult.getCursor();
      getRedisTemplate().transactionMulti(new RadisTransCallback<Boolean>() {
        public Boolean call(Transaction transaction) {
          for (String key : scanResult.getResult()) {
            transaction.del(key.getBytes());
          }
          return true;
        }
      });
    } while (!"0".equals(cursor));
    return true;
  }

  @Override
  @Deprecated
  public Set<String> keySet(String prefix) {
    try (Jedis jedis = getRedisTemplate().getResource()) {
      Set<byte[]> keys = jedis.keys((prefix + "*").getBytes());
      Set<String> list = new HashSet<>();
      for (byte[] key : keys) {
        list.add(new String(key));
      }
      return list;
    }
  }

  @Override
  @Deprecated
  public boolean clear(String prefix) {
    final Set<String> keys = keySet(prefix);
    return getRedisTemplate().transactionMulti(new RadisTransCallback<Boolean>() {
      public Boolean call(Transaction transaction) {
        for (String key : keys) {
          transaction.del(key.getBytes());
        }
        return true;
      }
    });
  }

  @Override
  public void destroy() {
    redisTemplate = null;
  }

  /**
   * Redis 获取key 反序列化对象.
   */
  private Serializable getObject(String key, Jedis jedis) {
    byte[] value = jedis.get(key.getBytes());
    return (Serializable) SerializationUtil.deserialize(value);
  }



}
