
package com.gitee.jmash.common.cache;

import com.gitee.jmash.common.redis.RedisProps;
import com.gitee.jmash.common.redis.RedisUtil;
import com.gitee.jmash.common.redis.callback.RadisTransCallback;
import com.gitee.jmash.common.redis.callback.RedisTemplate;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Message;
import com.google.protobuf.StringValue;
import jakarta.enterprise.inject.Typed;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import java.lang.reflect.Method;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import org.apache.commons.logging.LogFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.Transaction;

/**
 * Redis Cache Impl.
 *
 * @author CGD
 *
 */
@Typed(ProtoCache.class)
public class RedisProtoCacheImpl implements ProtoCache {

  @Inject
  @Named
  private RedisTemplate redisTemplate;

  public RedisProtoCacheImpl() {
    super();
  }

  /** Not Use CDI. */
  public RedisProtoCacheImpl(RedisProps redisProps) {
    super();
    JedisPool jedisPool = RedisUtil.getJedisPool(redisProps);
    this.redisTemplate = new RedisTemplate(jedisPool);
  }

  public RedisTemplate getRedisTemplate() {
    return redisTemplate;
  }

  public void setRedisTemplate(RedisTemplate redisTemplate) {
    this.redisTemplate = redisTemplate;
  }

  @Override
  public Message put(String key, Message value) {
    return redisTemplate.transactionMulti(new RadisTransCallback<Message>() {
      public Message call(Transaction transaction) {
        transaction.set(StringValue.of(key).toByteArray(), value.toByteArray());
        transaction.set(key + ".class", value.getClass().getName());
        return value;
      }
    });
  }

  @Override
  public Message put(String key, Message value, Instant expiry) {
    Long second = expiry.getEpochSecond() - Instant.now().getEpochSecond();
    return put(key, value, second.intValue());
  }

  @Override
  public Message put(String key, Message value, int ttl) {
    return redisTemplate.transactionMulti(new RadisTransCallback<Message>() {
      public Message call(Transaction transaction) {
        transaction.setex(StringValue.of(key).toByteArray(), ttl, value.toByteArray());
        transaction.setex(key + ".class", ttl, value.getClass().getName());
        return value;
      }
    });
  }

  @Override
  public Message get(String key) {
    try (Jedis jedis = redisTemplate.getResource()) {
      return getProto(key, jedis);
    }
  }

  @Override
  public Message remove(String key) {
    Message obj = get(key);
    redisTemplate.transactionMulti(new RadisTransCallback<Boolean>() {
      public Boolean call(Transaction transaction) {
        transaction.del(StringValue.of(key).toByteArray());
        transaction.del(key + ".class");
        return true;
      }
    });
    return obj;
  }

  @Override
  public boolean clear() {
    final Set<String> keys = keySet();
    return redisTemplate.transactionMulti(new RadisTransCallback<Boolean>() {
      public Boolean call(Transaction transaction) {
        for (String key : keys) {
          transaction.del(key);
          transaction.del(key + ".class");
        }
        return true;
      }
    });
  }

  @Override
  public int size() {
    return keySet().size();
  }

  @Override
  public Set<String> keySet() {
    try (Jedis jedis = redisTemplate.getResource()) {
      Set<byte[]> keys = jedis.keys("*".getBytes());
      Set<String> list = new HashSet<>();
      for (byte[] key : keys) {
        try {
          list.add(StringValue.parseFrom(key).getValue());
        } catch (InvalidProtocolBufferException ex) {
          // list.add(new String(key));
        }
      }
      return list;
    }
  }

  @Override
  public Collection<Message> values() {
    final Set<String> keys = keySet();
    try (Jedis jedis = redisTemplate.getResource()) {
      Collection<Message> list = new ArrayList<>();
      for (String key : keys) {
        Message obj = getProto(key, jedis);
        list.add(obj);
      }
      return list;
    }
  }

  @Override
  public boolean containsKey(String key) {
    try (Jedis jedis = redisTemplate.getResource()) {
      return jedis.exists(StringValue.of(key).toByteArray());
    }
  }

  @Override
  public void destroy() {
    redisTemplate = null;
  }

  /**
   * Redis 获取key Protobuf 对象.
   */
  private Message getProto(String key, Jedis jedis) {
    String className = jedis.get(key + ".class");
    if (null == className) {
      return null;
    }
    byte[] value = jedis.get(StringValue.of(key).toByteArray());
    return toProto(className, value);
  }

  private Message toProto(String className, byte[] value) {
    try {
      if (null == className) {
        return null;
      }
      Class<?> clazz = Class.forName(className);
      Method method = clazz.getDeclaredMethod("parseFrom", byte[].class);
      return (Message) method.invoke(method, value);
    } catch (Exception ex) {
      LogFactory.getLog(RedisProtoCacheImpl.class).error("Class " + className + " Error:", ex);
      return null;
    }
  }

}
