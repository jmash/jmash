package com.gitee.jmash.common.cache;

import java.io.Serializable;
import java.util.Set;

/**
 * Serializable Cache.
 *
 * @author CGD
 *
 */
public interface SerialCache extends Cache<String, Serializable> {


  /**
   * 查询某前缀的缓存key集合.
   */
  @Deprecated
  public Set<String> keySet(String prefix);

  /**
   * 删除某前缀的缓存的数据.
   */
  @Deprecated
  public boolean clear(String prefix);

  /**
   * Scan匹配key集合.
   */
  public Set<String> scanKeySet(String match);

  /**
   * 删除Scan匹配缓存的数据.
   */
  public boolean clearScan(String match);

}
