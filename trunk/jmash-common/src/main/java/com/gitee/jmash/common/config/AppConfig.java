
package com.gitee.jmash.common.config;

import com.gitee.jmash.common.i18n.MessageSource;
import com.gitee.jmash.common.tracing.ZipkinProps;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Produces;
import jakarta.enterprise.inject.spi.InjectionPoint;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.microprofile.config.inject.ConfigProperties;

/**
 * 应用配置接口.
 *
 * @author CGD
 *
 */
@ApplicationScoped
public class AppConfig {

  @Inject
  @ConfigProperties
  AppProps appProps;

  @Inject
  @ConfigProperties
  FileProps fileProps;

  @Inject
  @ConfigProperties
  ZipkinProps zipkinProps;

  /** Apache Log. */
  @Produces
  public Log createLog(InjectionPoint injectionPoint) {
    return LogFactory.getLog(injectionPoint.getMember().getDeclaringClass());
  }

  /** 国际化. */
  @Produces
  @Singleton
  public MessageSource getMessageSource() {
    String msg = String.format("jmash.%s.messages", appProps.getName().toLowerCase());
    String validator = "org.hibernate.validator.ValidationMessages";
    return MessageSource.setBaseNames("jmash.messages", validator, msg);
  }

}
