
package com.gitee.jmash.common.config;

import com.gitee.jmash.common.enums.ProtocolType;
import jakarta.enterprise.context.ApplicationScoped;
import java.util.Optional;
import org.eclipse.microprofile.config.inject.ConfigProperties;

/**
 * 微服务配置.
 *
 * @author CGD
 *
 */
@ConfigProperties(prefix = "jmash.server")
@ApplicationScoped
public class AppProps {

  /** Name. */
  private String name;

  /** 本地服务端口 . */
  private Integer port = 0;

  /** 测试服务Host . */
  private String testHost = "dev.jmash.crenjoy.com";

  /** 测试服务Port . */
  private Integer testPort = 443;

  /** 测试服务是否SSL . */
  private Boolean testSsl = true;

  /** 是否跳过安全验证 . */
  private Boolean testInsecure = false;
  
  /** 证书位置. */
  private String testCert = "jmash.cert";

  /** 是否注册. */
  private Boolean register = false;

  /** 服务类型. */
  private ProtocolType type = ProtocolType.GRPC;

  /** 是否开发环境. */
  private Boolean devMode = false;

  /** 服务标签. */
  private String tag = "1.0";

  /** Http协议内容目录. */
  private Optional<String> context = Optional.empty();

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getPort() {
    return port;
  }

  public void setPort(Integer port) {
    this.port = port;
  }

  public String getTestHost() {
    return testHost;
  }

  public void setTestHost(String testHost) {
    this.testHost = testHost;
  }

  public Integer getTestPort() {
    return testPort;
  }

  public void setTestPort(Integer testPort) {
    this.testPort = testPort;
  }

  public Boolean getTestSsl() {
    return testSsl;
  }

  public void setTestSsl(Boolean testSsl) {
    this.testSsl = testSsl;
  }

  public Boolean getTestInsecure() {
    return testInsecure;
  }

  public void setTestInsecure(Boolean testInsecure) {
    this.testInsecure = testInsecure;
  }

  public String getTestCert() {
    return testCert;
  }

  public void setTestCert(String testCert) {
    this.testCert = testCert;
  }

  public Boolean getRegister() {
    return register;
  }

  public void setRegister(Boolean register) {
    this.register = register;
  }

  public ProtocolType getType() {
    return type;
  }

  public void setType(ProtocolType type) {
    this.type = type;
  }

  public Boolean getDevMode() {
    return devMode;
  }

  public void setDevMode(Boolean devMode) {
    this.devMode = devMode;
  }

  public String getTag() {
    return tag;
  }

  public void setTag(String tag) {
    this.tag = tag;
  }

  public Optional<String> getContext() {
    return context;
  }

  public void setContext(Optional<String> context) {
    this.context = context;
  }

  @Override
  public String toString() {
    return "AppProps [name=" + name + ", port=" + port + ", testHost=" + testHost + ", testPort="
        + testPort + ", testSsl=" + testSsl + ", testCert=" + testCert + ", register=" + register
        + ", type=" + type + ", devMode=" + devMode + ", tag=" + tag + ", context=" + context + "]";
  }

}
