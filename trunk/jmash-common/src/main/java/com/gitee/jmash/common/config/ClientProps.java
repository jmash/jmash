
package com.gitee.jmash.common.config;

import jakarta.enterprise.context.Dependent;
import java.io.Serializable;
import org.eclipse.microprofile.config.inject.ConfigProperties;

/**
 * 微服务客户端配置.
 *
 * @author CGD
 *
 */
@ConfigProperties(prefix = "jmash.client")
@Dependent
public class ClientProps implements Serializable {

  private static final long serialVersionUID = 1L;

  /** IP Address. */
  private String address = "127.0.0.1";

  /** Port. */
  private Integer port = 0;

  /** 测试服务是否SSL . */
  private Boolean ssl = true;

  /** 是否跳过安全验证 . */
  private Boolean insecure = false;

  /** 证书位置. */
  private String cert = "jmash.cert";

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public Integer getPort() {
    return port;
  }

  public void setPort(Integer port) {
    this.port = port;
  }

  public Boolean getSsl() {
    return ssl;
  }

  public void setSsl(Boolean ssl) {
    this.ssl = ssl;
  }

  public Boolean getInsecure() {
    return insecure;
  }

  public void setInsecure(Boolean insecure) {
    this.insecure = insecure;
  }

  public String getCert() {
    return cert;
  }

  public void setCert(String cert) {
    this.cert = cert;
  }

  @Override
  public String toString() {
    return "ClientProps [address=" + address + ", port=" + port + ", ssl=" + ssl + ", insecure="
        + insecure + ", cert=" + cert + "]";
  }
  
}
