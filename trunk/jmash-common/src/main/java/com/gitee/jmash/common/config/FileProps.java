
package com.gitee.jmash.common.config;

import jakarta.enterprise.context.Dependent;
import java.io.Serializable;
import org.apache.commons.io.FileUtils;
import org.eclipse.microprofile.config.inject.ConfigProperties;

/**
 * File Config .
 *
 * @author CGD
 *
 */
@ConfigProperties(prefix = "jmash.file")
@Dependent
public class FileProps implements Serializable {

  private static final long serialVersionUID = 1L;

  // 文件路径
  private String path = "/data/jmash/file";
  // 临时文件路径，如：临时生成的报表文件等
  private String tempPath = "/data/jmash/_tmp";
  // 缩略图路径，如：缩略图
  private String thumbPath = "/data/jmash/_thumb";
  // 允许最大文件尺寸(默认10G)
  private Long maxSize = 10 * 1024 * 1024 * 1024L;
  // 允许最大宽度
  private Integer maxWidth = 1000;

  // 文件大小显示
  public String maxSizeDisplay() {
    return FileUtils.byteCountToDisplaySize(maxSize);
  }

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public String getTempPath() {
    return tempPath;
  }

  public void setTempPath(String tempPath) {
    this.tempPath = tempPath;
  }

  public String getThumbPath() {
    return thumbPath;
  }

  public void setThumbPath(String thumbPath) {
    this.thumbPath = thumbPath;
  }

  public Long getMaxSize() {
    return maxSize;
  }

  public void setMaxSize(Long maxSize) {
    this.maxSize = maxSize;
  }

  public Integer getMaxWidth() {
    return maxWidth;
  }

  public void setMaxWidth(Integer maxWidth) {
    this.maxWidth = maxWidth;
  }

}
