
package com.gitee.jmash.common.config;

import io.smallrye.config.common.MapBackedConfigSource;
import io.smallrye.config.common.utils.ConfigSourceUtil;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.logging.LogFactory;

/**
 * k8s micro config.
 *
 * @author CGD
 *
 */
public class K8sConfigSource extends MapBackedConfigSource {

  private static final long serialVersionUID = 1L;

  public K8sConfigSource() throws MalformedURLException, IOException {
    super("k8sConfig", K8sConfigSource.fileToMap(new File("/usr/app/config/jmash.conf")), 120);
  }

  /**
   * Load Config.
   */
  public static Map<String, String> fileToMap(File file) throws MalformedURLException, IOException {
    if (file.exists()) {
      return ConfigSourceUtil.urlToMap(file.toURI().toURL());
    }
    file = new File("jmash.conf");
    if (file.exists()) {
      return ConfigSourceUtil.urlToMap(file.toURI().toURL());
    } else {
      LogFactory.getLog(K8sConfigSource.class).warn("NOT Found " + file.getPath());
      return new HashMap<>();
    }
  }

}
