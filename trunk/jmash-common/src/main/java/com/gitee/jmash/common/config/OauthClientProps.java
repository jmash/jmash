
package com.gitee.jmash.common.config;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import java.io.Serializable;
import org.eclipse.microprofile.config.inject.ConfigProperty;

/**
 * OAuth Properties .
 *
 * @author CGD
 *
 */
@ApplicationScoped
public class OauthClientProps implements Serializable {

  private static final long serialVersionUID = 1L;

  /** Sign Verify Public Key. */
  @Inject
  @ConfigProperty(name = "mp.jwt.verify.publickey")
  private String publicKey = "Config OIDC mp.jwt.verify.publickey Public Key.";

  /** decrypt Private Key. */
  @Inject
  @ConfigProperty(name = "smallrye.jwt.decrypt.key")
  private String decryptKey = "Config OIDC smallrye.jwt.decrypt.key Private Key.";

  public String getPublicKey() {
    return publicKey;
  }

  public void setPublicKey(String publicKey) {
    this.publicKey = publicKey;
  }

  public String getDecryptKey() {
    return decryptKey;
  }

  public void setDecryptKey(String decryptKey) {
    this.decryptKey = decryptKey;
  }

}
