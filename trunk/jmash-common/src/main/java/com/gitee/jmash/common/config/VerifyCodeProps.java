
package com.gitee.jmash.common.config;

import jakarta.enterprise.context.Dependent;
import java.io.Serializable;
import org.eclipse.microprofile.config.inject.ConfigProperties;

/**
 * 验证码配置.
 *
 * @author CGD
 *
 */
@ConfigProperties(prefix = "jmash.verifycode")
@Dependent
public class VerifyCodeProps implements Serializable {

  private static final long serialVersionUID = 1L;

  /** 是否启用. */
  private Boolean enable = true;

  /** 验证码类型. */
  private Integer type = 0;

  public Boolean getEnable() {
    return enable;
  }

  public void setEnable(Boolean enable) {
    this.enable = enable;
  }

  public Integer getType() {
    return type;
  }

  public void setType(Integer type) {
    this.type = type;
  }
}
