
package com.gitee.jmash.common.enums;

/**
 * 文档和回收站类型.
 */
public enum FileType {

  /** 文档. */
  text("文档"),
  /** 视频. */
  video("视频"),
  /** 图片. */
  image("图片"),
  /** 音频. */
  audio("音频"),
  /** 压缩文件. */
  zip("压缩文件"),

  file("其他文件");

  private String chName;

  private FileType(String chName) {
    this.chName = chName;
  }

  public String getChName() {
    return chName;
  }

  /**
   * 服务器支持的上传文件类型.
   */
  public static FileType[] acceptFileTypes() {
    return new FileType[] { FileType.text, FileType.video, FileType.image, FileType.audio,
        FileType.zip };
  }

}
