
package com.gitee.jmash.common.enums;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * MIME Type Mappings 类型 - 替换为 _ M_开头类型.
 *
 * @author CGD
 * 
 */
public enum MimeType {

  /****************** 文档. *********************/
  // C、C++语言
  M_c("text/x-c", FileType.text),

  M_cc("text/x-c", FileType.text),

  M_cpp("text/x-c", FileType.text),

  M_cxx("text/x-c", FileType.text),

  M_dic("text/x-c", FileType.text),

  M_h("text/x-c", FileType.text),

  M_hh("text/x-c", FileType.text),

  M_conf("text/plain", FileType.text),

  M_def("text/plain", FileType.text),

  // html+CSS
  M_htm("text/html", FileType.text),

  M_html("text/html", FileType.text),

  M_body("text/html", FileType.text),

  M_css("text/css", FileType.text),

  // 汇编代码
  M_asm("text/x-asm", FileType.text),

  M_s("text/x-asm", FileType.text),

  // fortran 语言
  M_f("text/x-fortran", FileType.text),

  M_f77("text/x-fortran", FileType.text),

  M_f90("text/x-fortran", FileType.text),

  M_for("text/x-fortran", FileType.text),

  // <!-- WML Source -->
  M_wml("text/vnd.wap.wml", FileType.text),

  // <!-- WML Script Source -->
  M_wmls("text/vnd.wap.wmlscript", FileType.text),

  M_etx("text/x-setext", FileType.text),

  M_dsc("text/prs.lines.tag", FileType.text),

  M_roff("text/troff", FileType.text),

  M_rtx("text/richtext", FileType.text),

  M_csv("text/csv", FileType.text),

  M_curl("text/vnd.curl", FileType.text),

  M_dcurl("text/vnd.curl.dcurl", FileType.text),

  M_flx("text/vnd.fmi.flexstor", FileType.text),

  M_fly("text/vnd.fly", FileType.text),

  M_gv("text/vnd.graphviz", FileType.text),

  M_ics("text/calendar", FileType.text),

  M_ifb("text/calendar", FileType.text),

  M_htc("text/x-component", FileType.text),

  M_sgm("text/sgml", FileType.text),

  M_js("application/javascript", FileType.text),

  M_jsf("text/plain", FileType.text),

  M_json("application/json", FileType.text),

  M_jspf("text/plain", FileType.text),

  M_sgml("text/sgml", FileType.text),

  M_pas("text/x-pascal", FileType.text),

  M_p("text/x-pascal", FileType.text),

  M_java("text/x-java-source", FileType.text),

  M_pdf("application/pdf", FileType.text),

  M_shtml("text/x-server-parsed-html", FileType.text),

  M_spot("text/vnd.in3d.spot", FileType.text),

  M_t("text/troff", FileType.text),

  M_text("text/plain", FileType.text),

  M_txt("text/plain", FileType.text),

  M_tr("text/troff", FileType.text),

  M_tsv("text/tab-separated-values", FileType.text),

  M_ttl("text/turtle", FileType.text),

  M_uri("text/uri-list", FileType.text),

  M_uris("text/uri-list", FileType.text),

  M_rtf("application/rtf", FileType.text),

  M_urls("text/uri-list", FileType.text),

  M_uu("text/x-uuencode", FileType.text),

  M_vcf("text/x-vcard", FileType.text),

  M_vcs("text/x-vcalendar", FileType.text),

  M_n3("text/n3", FileType.text),

  M_man("text/troff", FileType.text),

  M_me("text/troff", FileType.text),

  M_mcurl("text/vnd.curl.mcurl", FileType.text),

  M_list("text/plain", FileType.text),

  M_log("text/plain", FileType.text),

  M_in("text/plain", FileType.text),

  M_jad("text/vnd.sun.j2me.app-descriptor", FileType.text),

  M_3dml("text/vnd.in3d.3dml", FileType.text),

  M_doc("application/msword", FileType.text),

  M_docm("application/vnd.ms-word.document.macroenabled.12", FileType.text),

  M_docx("application/vnd.openxmlformats-officedocument.wordprocessingml.document", FileType.text),

  M_dot("application/msword", FileType.text),

  M_dotm("application/vnd.ms-word.template.macroenabled.12", FileType.text),

  M_dotx("application/vnd.openxmlformats-officedocument.wordprocessingml.template", FileType.text),

  M_xla("application/vnd.ms-excel", FileType.text),

  M_xlam("application/vnd.ms-excel.addin.macroenabled.12", FileType.text),

  M_xlc("application/vnd.ms-excel", FileType.text),

  M_xlm("application/vnd.ms-excel", FileType.text),

  M_xls("application/vnd.ms-excel", FileType.text),

  M_xlsb("application/vnd.ms-excel.sheet.binary.macroenabled.12", FileType.text),

  M_xlsm("application/vnd.ms-excel.sheet.macroenabled.12", FileType.text),

  M_xlsx("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", FileType.text),

  M_xlt("application/vnd.ms-excel", FileType.text),

  M_xltm("application/vnd.ms-excel.template.macroenabled.12", FileType.text),

  M_xltx("application/vnd.openxmlformats-officedocument.spreadsheetml.template", FileType.text),

  M_xlw("application/vnd.ms-excel", FileType.text),

  M_xml("application/xml", FileType.text),

  M_xsl("application/xml", FileType.text),

  M_bpmn("application/xml", FileType.text),

  M_xslt("application/xslt+xml", FileType.text),

  M_vxml("application/voicexml+xml", FileType.text),

  M_ttc("application/x-font-ttf", FileType.text),

  M_ttf("application/x-font-ttf", FileType.text),

  M_sxc("application/vnd.sun.xml.calc", FileType.text),

  M_sxd("application/vnd.sun.xml.draw", FileType.text),

  M_sxg("application/vnd.sun.xml.writer.global", FileType.text),

  M_sxi("application/vnd.sun.xml.impress", FileType.text),

  M_sxm("application/vnd.sun.xml.math", FileType.text),

  M_sxw("application/vnd.sun.xml.writer", FileType.text),

  M_pps("application/vnd.ms-powerpoint", FileType.text),

  M_ms("text/troff", FileType.text),

  M_scurl("text/vnd.curl.scurl", FileType.text),

  M_stc("application/vnd.sun.xml.calc.template", FileType.text),

  M_std("application/vnd.sun.xml.draw.template", FileType.text),

  M_sti("application/vnd.sun.xml.impress.template", FileType.text),

  M_ppsm("application/vnd.ms-powerpoint.slideshow.macroenabled.12", FileType.text),

  M_ppsx("application/vnd.openxmlformats-officedocument.presentationml.slideshow", FileType.text),

  M_ppt("application/vnd.ms-powerpoint", FileType.text),

  M_pptm("application/vnd.ms-powerpoint.presentation.macroenabled.12", FileType.text),

  M_pptx("application/vnd.openxmlformats-officedocument.presentationml.presentation",
      FileType.text),

  M_pot("application/vnd.ms-powerpoint", FileType.text),

  M_potm("application/vnd.ms-powerpoint.template.macroenabled.12", FileType.text),

  M_potx("application/vnd.openxmlformats-officedocument.presentationml.template", FileType.text),

  M_ppam("application/vnd.ms-powerpoint.addin.macroenabled.12", FileType.text),

  M_otg("application/vnd.oasis.opendocument.graphics-template", FileType.text),

  M_oth("application/vnd.oasis.opendocument.text-web", FileType.text),

  M_oti("application/vnd.oasis.opendocument.image-template", FileType.text),

  M_otp("application/vnd.oasis.opendocument.presentation-template", FileType.text),

  M_ots("application/vnd.oasis.opendocument.spreadsheet-template", FileType.text),

  M_ott("application/vnd.oasis.opendocument.text-template", FileType.text),

  M_oxt("application/vnd.openofficeorg.extension", FileType.text),

  M_otc("application/vnd.oasis.opendocument.chart-template", FileType.text),

  M_odb("application/vnd.oasis.opendocument.database", FileType.text),

  M_odc("application/vnd.oasis.opendocument.chart", FileType.text),

  M_odf("application/vnd.oasis.opendocument.formula", FileType.text),

  M_odft("application/vnd.oasis.opendocument.formula-template", FileType.text),

  M_odg("application/vnd.oasis.opendocument.graphics", FileType.text),

  M_odi("application/vnd.oasis.opendocument.image", FileType.text),

  M_odm("application/vnd.oasis.opendocument.text-master", FileType.text),

  M_odp("application/vnd.oasis.opendocument.presentation", FileType.text),

  M_ods("application/vnd.oasis.opendocument.spreadsheet", FileType.text),

  M_odt("application/vnd.oasis.opendocument.text", FileType.text),

  /****************** 视频. *********************/

  M_3gp("video/3gpp", FileType.video),

  M_wmv("video/x-ms-wmv", FileType.video),

  M_flv("video/x-flv", FileType.video),

  M_mp4("video/mp4", FileType.video),

  M_mov("video/quicktime", FileType.video),

  M_rm("application/vnd.rn-realmedia", FileType.video),

  M_mpv2("video/mpeg2", FileType.video),

  M_3g2("video/3gpp2", FileType.video),

  M_pyv("video/vnd.ms-playready.media.pyv", FileType.video),

  M_asf("video/x-ms-asf", FileType.video),

  M_asx("video/x-ms-asf", FileType.video),

  M_avi("video/x-msvideo", FileType.video),

  M_avx("video/x-rad-screenplay", FileType.video),

  M_axv("video/annodex", FileType.video),

  M_dv("video/x-dv", FileType.video),

  M_f4v("video/x-f4v", FileType.video),

  M_wmx("video/x-ms-wmx", FileType.video),

  M_wvx("video/x-ms-wvx", FileType.video),

  M_wm("video/x-ms-wm", FileType.video),

  M_webm("video/webm", FileType.video),

  M_fli("video/x-fli", FileType.video),

  M_fvt("video/vnd.fvt", FileType.video),

  M_h261("video/h261", FileType.video),

  M_h263("video/h263", FileType.video),

  M_h264("video/h264", FileType.video),

  M_mxu("video/vnd.mpegurl", FileType.audio),

  M_uvh("video/vnd.dece.hd", FileType.audio),

  M_qt("video/quicktime", FileType.audio),

  M_m1v("video/mpeg", FileType.audio),

  M_m4u("video/vnd.mpegurl", FileType.audio),

  M_m4v("video/mp4", FileType.audio),

  M_m2v("video/mpeg", FileType.audio),

  M_uvm("video/vnd.dece.mobile", FileType.audio),

  M_uvp("video/vnd.dece.pd", FileType.audio),

  M_uvs("video/vnd.dece.sd", FileType.audio),

  M_uvu("video/vnd.uvvu.mp4", FileType.audio),

  M_uvv("video/vnd.dece.video", FileType.audio),

  M_uvvh("video/vnd.dece.hd", FileType.audio),

  M_uvvm("video/vnd.dece.mobile", FileType.audio),

  M_uvvp("video/vnd.dece.pd", FileType.audio),

  M_mj2("video/mj2", FileType.audio),

  M_mjp2("video/mj2", FileType.audio),

  M_mpe("video/mpeg", FileType.audio),

  M_mpeg("video/mpeg", FileType.audio),

  M_mpg("video/mpeg", FileType.audio),

  M_jpgm("video/jpm", FileType.audio),

  M_jpgv("video/jpeg", FileType.audio),

  M_jpm("video/jpm", FileType.audio),

  M_movie("video/x-sgi-movie", FileType.audio),

  M_mpg4("video/mp4", FileType.audio),

  M_mp4v("video/mp4", FileType.audio),

  M_uvvs("video/vnd.dece.sd", FileType.audio),

  M_uvvu("video/vnd.uvvu.mp4", FileType.audio),

  M_uvvv("video/vnd.dece.video", FileType.audio),

  M_viv("video/vnd.vivo", FileType.audio),

  M_ogv("video/ogg", FileType.audio),

  /****************** 图片. *********************/

  M_gif("image/gif", FileType.image),

  M_png("image/png", FileType.image),

  M_jpg("image/jpeg", FileType.image),

  M_jpeg("image/jpeg", FileType.image),

  M_bmp("image/bmp", FileType.image),

  M_ico("image/x-icon", FileType.image),

  M_tif("image/tiff", FileType.image),

  M_art("image/x-jg", FileType.image),

  M_dwg("image/vnd.dwg", FileType.image),

  M_psd("image/vnd.adobe.photoshop", FileType.image),

  M_rlc("image/vnd.fujixerox.edmics-rlc", FileType.image),

  M_dxf("image/vnd.dxf", FileType.image),

  M_fbs("image/vnd.fastbidsheet", FileType.image),

  M_xif("image/vnd.xiff", FileType.image),

  M_webp("image/webp", FileType.image),

  // <!-- Wireless Bitmap -->
  M_wbmp("image/vnd.wap.wbmp", FileType.image),

  M_btif("image/prs.btif", FileType.image),

  M_cgm("image/cgm", FileType.image),

  M_cmx("image/x-cmx", FileType.image),

  M_dib("image/bmp", FileType.image),

  M_djv("image/vnd.djvu", FileType.image),

  M_djvu("image/vnd.djvu", FileType.image),

  M_fpx("image/vnd.fpx", FileType.image),

  M_fst("image/vnd.fst", FileType.image),

  M_g3("image/g3fax", FileType.image),

  M_ief("image/ief", FileType.image),

  M_fh("image/x-freehand", FileType.image),

  M_fh4("image/x-freehand", FileType.image),

  M_fh5("image/x-freehand", FileType.image),

  M_fh7("image/x-freehand", FileType.image),

  M_fhc("image/x-freehand", FileType.image),

  M_sub("image/vnd.dvb.subtitle", FileType.image),

  M_rgb("image/x-rgb", FileType.image),

  M_svg("image/svg+xml", FileType.image),

  M_svgz("image/svg+xml", FileType.image),

  M_tiff("image/tiff", FileType.image),

  M_uvg("image/vnd.dece.graphic", FileType.image),

  M_uvi("image/vnd.dece.graphic", FileType.image),

  M_uvvg("image/vnd.dece.graphic", FileType.image),

  M_uvvi("image/vnd.dece.graphic", FileType.image),

  M_xbm("image/x-xbitmap", FileType.image),

  M_jpe("image/jpeg", FileType.image),

  M_npx("image/vnd.net-fpx", FileType.image),

  M_xpm("image/x-xpixmap", FileType.image),

  M_xwd("image/x-xwindowdump", FileType.image),

  M_qti("image/x-quicktime", FileType.image),

  M_qtif("image/x-quicktime", FileType.image),

  M_ras("image/x-cmu-raster", FileType.image),

  M_pnm("image/x-portable-anymap", FileType.image),

  M_pnt("image/x-macpaint", FileType.image),

  M_ppm("image/x-portable-pixmap", FileType.image),

  M_pic("image/pict", FileType.image),

  M_pict("image/pict", FileType.image),

  M_pct("image/pict", FileType.image),

  M_pcx("image/x-pcx", FileType.image),

  M_mdi("image/vnd.ms-modi", FileType.image),

  M_mac("image/x-macpaint", FileType.image),

  M_mmr("image/vnd.fujixerox.edmics-mmr", FileType.image),

  M_pgm("image/x-portable-graymap", FileType.image),

  M_pbm("image/x-portable-bitmap", FileType.image),

  M_ktx("image/ktx", FileType.image),

  /****************** 音频. *********************/

  M_aac("audio/x-aac", FileType.audio),

  M_abs("audio/x-mpeg", FileType.audio),

  M_adp("audio/adpcm", FileType.audio),

  M_aif("audio/x-aiff", FileType.audio),

  M_pya("audio/vnd.ms-playready.media.pya", FileType.audio),

  M_aifc("audio/x-aiff", FileType.audio),

  M_aiff("audio/x-aiff", FileType.audio),

  M_au("audio/basic", FileType.audio),

  M_axa("audio/annodex", FileType.audio),

  M_dra("audio/vnd.dra", FileType.audio),

  M_dts("audio/vnd.dts", FileType.audio),

  M_dtshd("audio/vnd.dts.hd", FileType.audio),

  M_ecelp4800("audio/vnd.nuera.ecelp4800", FileType.audio),

  M_ecelp7470("audio/vnd.nuera.ecelp7470", FileType.audio),

  M_ecelp9600("audio/vnd.nuera.ecelp9600", FileType.audio),

  M_weba("audio/webm", FileType.audio),

  M_wma("audio/x-ms-wma", FileType.audio),

  M_wav("audio/x-wav", FileType.audio),

  M_wax("audio/x-ms-wax", FileType.audio),

  M_flac("audio/flac", FileType.audio),

  M_rip("audio/vnd.rip", FileType.audio),

  M_rmi("audio/midi", FileType.audio),

  M_rmp("audio/x-pn-realaudio-plugin", FileType.audio),

  M_spx("audio/ogg", FileType.audio),

  M_snd("audio/basic", FileType.audio),

  M_ulw("audio/basic", FileType.audio),

  M_uva("audio/vnd.dece.audio", FileType.audio),

  M_uvva("audio/vnd.dece.audio", FileType.audio),

  M_ra("audio/x-pn-realaudio", FileType.audio),

  M_ram("audio/x-pn-realaudio", FileType.audio),

  M_pls("audio/x-scpls", FileType.audio),

  M_kar("audio/midi", FileType.audio),

  M_oga("audio/ogg", FileType.audio),

  M_ogg("audio/ogg", FileType.audio),

  M_mid("audio/midi", FileType.audio),

  M_midi("audio/midi", FileType.audio),

  M_mpga("audio/mpeg", FileType.audio),

  M_mpega("audio/x-mpeg", FileType.audio),

  M_mp4a("audio/mp4", FileType.audio),

  M_mp1("audio/mpeg", FileType.audio),

  M_mp2("audio/mpeg", FileType.audio),

  M_mp2a("audio/mpeg", FileType.audio),

  M_mp3("audio/mpeg", FileType.audio),

  M_mpa("audio/mpeg", FileType.audio),

  M_m4a("audio/mp4", FileType.audio),

  M_m4b("audio/mp4", FileType.audio),

  M_m3u("audio/x-mpegurl", FileType.audio),

  M_m4r("audio/mp4", FileType.audio),

  M_m2a("audio/mpeg", FileType.audio),

  M_m3a("audio/mpeg", FileType.audio),

  /****************** 压缩文件. *********************/

  M_z("application/x-compress", FileType.zip),

  M_Z("application/x-compress", FileType.zip),

  M_zip("application/zip", FileType.zip),

  M_xzip("application/x-zip-compressed", FileType.zip),

  M_7z("application/x-7z-compressed", FileType.zip),

  M_ace("application/x-ace-compressed", FileType.zip),

  M_boz("application/x-bzip2", FileType.zip),

  M_bz("application/x-bzip", FileType.zip),

  M_bz2("application/x-bzip2", FileType.zip),

  M_gz("application/x-gzip", FileType.zip),

  M_tar_gz("application/gzip", FileType.zip),

  M_rar("application/x-rar-compressed", FileType.zip),

  M_apk("application/vnd.android.package-archive", FileType.zip),

  M_tar("application/x-tar", FileType.zip),

  /****************** 可执行程序. *********************/

  M_aab("application/x-authorware-bin", FileType.file),

  M_aam("application/x-authorware-map", FileType.file),

  M_aas("application/x-authorware-seg", FileType.file),

  M_123("application/vnd.lotus-1-2-3", FileType.file),

  M_abw("application/x-abiword", FileType.file),

  M_ac("application/pkix-attr-cert", FileType.file),

  M_acc("application/vnd.americandynamics.acc", FileType.file),

  M_acu("application/vnd.acucobol", FileType.file),

  M_acutc("application/vnd.acucorp", FileType.file),

  M_aep("application/vnd.audiograph", FileType.file),

  M_afm("application/x-font-type1", FileType.file),

  M_afp("application/vnd.ibm.modcap", FileType.file),

  M_ahead("application/vnd.ahead.space", FileType.file),

  M_ai("application/postscript", FileType.file),

  M_aim("application/x-aim", FileType.file),

  M_air("application/vnd.adobe.air-application-installer-package+zip", FileType.file),

  M_ait("application/vnd.dvb.ait", FileType.file),

  M_ami("application/vnd.amiga.ami", FileType.file),

  M_anx("application/annodex", FileType.file),

  M_application("application/x-ms-application", FileType.file),

  M_apr("application/vnd.lotus-approach", FileType.file),

  M_asc("application/pgp-signature", FileType.file),

  M_aso("application/vnd.accpac.simply.aso", FileType.file),

  M_atc("application/vnd.acucorp", FileType.file),

  M_atom("application/atom+xml", FileType.file),

  M_atomcat("application/atomcat+xml", FileType.file),

  M_atomsvc("application/atomsvc+xml", FileType.file),

  M_atx("application/vnd.antix.game-component", FileType.file),

  M_aw("application/applixware", FileType.file),

  M_azf("application/vnd.airzip.filesecure.azf", FileType.file),

  M_azs("application/vnd.airzip.filesecure.azs", FileType.file),

  M_azw("application/vnd.amazon.ebook", FileType.file),

  M_bat("application/x-msdownload", FileType.file),

  M_bcpio("application/x-bcpio", FileType.file),

  M_bdf("application/x-font-bdf", FileType.file),

  M_bdm("application/vnd.syncml.dm+wbxml", FileType.file),

  M_bed("application/vnd.realvnc.bed", FileType.file),

  M_bh2("application/vnd.fujitsu.oasysprs", FileType.file),

  M_bin("application/octet-stream", FileType.file),

  M_bmi("application/vnd.bmi", FileType.file),

  M_book("application/vnd.framemaker", FileType.file),

  M_box("application/vnd.previewsystems.box", FileType.file),

  M_bpk("application/octet-stream", FileType.file),

  M_c11amc("application/vnd.cluetrust.cartomobile-config", FileType.file),

  M_c11amz("application/vnd.cluetrust.cartomobile-config-pkg", FileType.file),

  M_c4d("application/vnd.clonk.c4group", FileType.file),

  M_c4f("application/vnd.clonk.c4group", FileType.file),

  M_c4g("application/vnd.clonk.c4group", FileType.file),

  M_c4p("application/vnd.clonk.c4group", FileType.file),

  M_c4u("application/vnd.clonk.c4group", FileType.file),

  M_cab("application/vnd.ms-cab-compressed", FileType.file),

  M_car("application/vnd.curl.car", FileType.file),

  M_cat("application/vnd.ms-pki.seccat", FileType.file),

  M_cct("application/x-director", FileType.file),

  M_ccxml("application/ccxml+xml", FileType.file),

  M_cdbcmsg("application/vnd.contact.cmsg", FileType.file),

  M_cdf("application/x-cdf", FileType.file),

  M_cdkey("application/vnd.mediastation.cdkey", FileType.file),

  M_cdmia("application/cdmi-capability", FileType.file),

  M_cdmic("application/cdmi-container", FileType.file),

  M_cdmid("application/cdmi-domain", FileType.file),

  M_cdmio("application/cdmi-object", FileType.file),

  M_cdmiq("application/cdmi-queue", FileType.file),

  M_cdx("chemical/x-cdx", FileType.file),

  M_cdxml("application/vnd.chemdraw+xml", FileType.file),

  M_cdy("application/vnd.cinderella", FileType.file),

  M_cer("application/pkix-cert", FileType.file),

  M_chat("application/x-chat", FileType.file),

  M_chm("application/vnd.ms-htmlhelp", FileType.file),

  M_chrt("application/vnd.kde.kchart", FileType.file),

  M_cif("chemical/x-cif", FileType.file),

  M_cii("application/vnd.anser-web-certificate-issue-initiation", FileType.file),

  M_cil("application/vnd.ms-artgalry", FileType.file),

  M_cla("application/vnd.claymore", FileType.file),

  M_clkk("application/vnd.crick.clicker.keyboard", FileType.file),

  M_clkp("application/vnd.crick.clicker.palette", FileType.file),

  M_clkt("application/vnd.crick.clicker.template", FileType.file),

  M_clkw("application/vnd.crick.clicker.wordbank", FileType.file),

  M_clkx("application/vnd.crick.clicker", FileType.file),

  M_clp("application/x-msclip", FileType.file),

  M_cmc("application/vnd.cosmocaller", FileType.file),

  M_cmdf("chemical/x-cmdf", FileType.file),

  M_cml("chemical/x-cml", FileType.file),

  M_cmp("application/vnd.yellowriver-custom-menu", FileType.file),

  M_cod("application/vnd.rim.cod", FileType.file),

  M_com("application/x-msdownload", FileType.file),

  M_cpio("application/x-cpio", FileType.file),

  M_cpt("application/mac-compactpro", FileType.file),

  M_crd("application/x-mscardfile", FileType.file),

  M_crl("application/pkix-crl", FileType.file),

  M_class("application/java", FileType.file),

  M_crt("application/x-x509-ca-cert", FileType.file),

  M_cryptonote("application/vnd.rig.cryptonote", FileType.file),

  M_csh("application/x-csh", FileType.file),

  M_csml("chemical/x-csml", FileType.file),

  M_csp("application/vnd.commonspace", FileType.file),

  M_cst("application/x-director", FileType.file),

  M_cu("application/cu-seeme", FileType.file),

  M_cww("application/prs.cww", FileType.file),

  M_cxt("application/x-director", FileType.file),

  M_dae("model/vnd.collada+xml", FileType.file),

  M_daf("application/vnd.mobius.daf", FileType.file),

  M_dataless("application/vnd.fdsn.seed", FileType.file),

  M_davmount("application/davmount+xml", FileType.file),

  M_dcr("application/x-director", FileType.file),

  M_dd2("application/vnd.oma.dd2+xml", FileType.file),

  M_ddd("application/vnd.fujixerox.ddd", FileType.file),

  M_deb("application/x-debian-package", FileType.file),

  M_deploy("application/octet-stream", FileType.file),

  M_der("application/x-x509-ca-cert", FileType.file),

  M_dfac("application/vnd.dreamfactory", FileType.file),

  M_dir("application/x-director", FileType.file),

  M_dis("application/vnd.mobius.dis", FileType.file),

  M_dist("application/octet-stream", FileType.file),

  M_distz("application/octet-stream", FileType.file),

  M_dll("application/x-msdownload", FileType.file),

  M_dmg("application/octet-stream", FileType.file),

  M_dms("application/octet-stream", FileType.file),

  M_dna("application/vnd.dna", FileType.file),

  M_dp("application/vnd.osgi.dp", FileType.file),

  M_dpg("application/vnd.dpgraph", FileType.file),

  M_dssc("application/dssc+der", FileType.file),

  M_dtb("application/x-dtbook+xml", FileType.file),

  M_dtd("application/xml-dtd", FileType.file),

  M_dump("application/octet-stream", FileType.file),

  M_dvi("application/x-dvi", FileType.file),

  M_dwf("model/vnd.dwf", FileType.file),

  M_dxp("application/vnd.spotfire.dxp", FileType.file),

  M_dxr("application/x-director", FileType.file),

  M_ecma("application/ecmascript", FileType.file),

  M_edm("application/vnd.novadigm.edm", FileType.file),

  M_edx("application/vnd.novadigm.edx", FileType.file),

  M_efif("application/vnd.picsel", FileType.file),

  M_ei6("application/vnd.pg.osasli", FileType.file),

  M_elc("application/octet-stream", FileType.file),

  M_eml("message/rfc822", FileType.file),

  M_emma("application/emma+xml", FileType.file),

  M_eot("application/vnd.ms-fontobject", FileType.file),

  M_eps("application/postscript", FileType.file),

  M_epub("application/epub+zip", FileType.file),

  M_es3("application/vnd.eszigno3+xml", FileType.file),

  M_esf("application/vnd.epson.esf", FileType.file),

  M_et3("application/vnd.eszigno3+xml", FileType.file),

  M_exe("application/octet-stream", FileType.file),

  M_exi("application/exi", FileType.file),

  M_ext("application/vnd.novadigm.ext", FileType.file),

  M_ez("application/andrew-inset", FileType.file),

  M_ez2("application/vnd.ezpix-album", FileType.file),

  M_ez3("application/vnd.ezpix-package", FileType.file),

  M_fcs("application/vnd.isac.fcs", FileType.file),

  M_fdf("application/vnd.fdf", FileType.file),

  M_fe_launch("application/vnd.denovo.fcselayout-link", FileType.file),

  M_fg5("application/vnd.fujitsu.oasysgp", FileType.file),

  M_fgd("application/x-director", FileType.file),

  M_fig("application/x-xfig", FileType.file),

  M_flo("application/vnd.micrografx.flo", FileType.file),

  M_flw("application/vnd.kde.kivio", FileType.file),

  M_fm("application/vnd.framemaker", FileType.file),

  M_fnc("application/vnd.frogans.fnc", FileType.file),

  M_frame("application/vnd.framemaker", FileType.file),

  M_fsc("application/vnd.fsc.weblaunch", FileType.file),

  M_ftc("application/vnd.fluxtime.clip", FileType.file),

  M_fti("application/vnd.anser-web-funds-transfer-initiation", FileType.file),

  M_fxp("application/vnd.adobe.fxp", FileType.file),

  M_fxpl("application/vnd.adobe.fxp", FileType.file),

  M_fzs("application/vnd.fuzzysheet", FileType.file),

  M_g2w("application/vnd.geoplan", FileType.file),

  M_g3w("application/vnd.geospace", FileType.file),

  M_gac("application/vnd.groove-account", FileType.file),

  M_gdl("model/vnd.gdl", FileType.file),

  M_geo("application/vnd.dynageo", FileType.file),

  M_gex("application/vnd.geometry-explorer", FileType.file),

  M_ggb("application/vnd.geogebra.file", FileType.file),

  M_ggt("application/vnd.geogebra.tool", FileType.file),

  M_ghf("application/vnd.groove-help", FileType.file),

  M_gim("application/vnd.groove-identity-message", FileType.file),

  M_gmx("application/vnd.gmx", FileType.file),

  M_gnumeric("application/x-gnumeric", FileType.file),

  M_gph("application/vnd.flographit", FileType.file),

  M_gqf("application/vnd.grafeq", FileType.file),

  M_gqs("application/vnd.grafeq", FileType.file),

  M_gram("application/srgs", FileType.file),

  M_gre("application/vnd.geometry-explorer", FileType.file),

  M_grv("application/vnd.groove-injector", FileType.file),

  M_grxml("application/srgs+xml", FileType.file),

  M_gsf("application/x-font-ghostscript", FileType.file),

  M_gtar("application/x-gtar", FileType.file),

  M_gtm("application/vnd.groove-tool-message", FileType.file),

  M_gtw("model/vnd.gtw", FileType.file),

  M_gxt("application/vnd.geonext", FileType.file),

  M_hal("application/vnd.hal+xml", FileType.file),

  M_hbci("application/vnd.hbci", FileType.file),

  M_hdf("application/x-hdf", FileType.file),

  M_hlp("application/winhlp", FileType.file),

  M_hpgl("application/vnd.hp-hpgl", FileType.file),

  M_hpid("application/vnd.hp-hpid", FileType.file),

  M_hps("application/vnd.hp-hps", FileType.file),

  M_hqx("application/mac-binhex40", FileType.file),

  M_htke("application/vnd.kenameaapp", FileType.file),

  M_hvd("application/vnd.yamaha.hv-dic", FileType.file),

  M_hvp("application/vnd.yamaha.hv-voice", FileType.file),

  M_hvs("application/vnd.yamaha.hv-script", FileType.file),

  M_i2g("application/vnd.intergeo", FileType.file),

  M_icc("application/vnd.iccprofile", FileType.file),

  M_ice("x-conference/x-cooltalk", FileType.file),

  M_icm("application/vnd.iccprofile", FileType.file),

  M_ifm("application/vnd.shana.informed.formdata", FileType.file),

  M_iges("model/iges", FileType.file),

  M_igl("application/vnd.igloader", FileType.file),

  M_igm("application/vnd.insors.igm", FileType.file),

  M_igs("model/iges", FileType.file),

  M_igx("application/vnd.micrografx.igx", FileType.file),

  M_iif("application/vnd.shana.informed.interchange", FileType.file),

  M_imp("application/vnd.accpac.simply.imp", FileType.file),

  M_ims("application/vnd.ms-ims", FileType.file),

  M_ipfix("application/ipfix", FileType.file),

  M_ipk("application/vnd.shana.informed.package", FileType.file),

  M_irm("application/vnd.ibm.rights-management", FileType.file),

  M_irp("application/vnd.irepository.package+xml", FileType.file),

  M_iso("application/octet-stream", FileType.file),

  M_itp("application/vnd.shana.informed.formtemplate", FileType.file),

  M_ivp("application/vnd.immervision-ivp", FileType.file),

  M_ivu("application/vnd.immervision-ivu", FileType.file),

  M_jam("application/vnd.jam", FileType.file),

  M_jar("application/java-archive", FileType.file),

  M_jisp("application/vnd.jisp", FileType.file),

  M_jlt("application/vnd.hp-jlyt", FileType.file),

  M_jnlp("application/x-java-jnlp-file", FileType.file),

  M_joda("application/vnd.joost.joda-archive", FileType.file),

  M_karbon("application/vnd.kde.karbon", FileType.file),

  M_kfo("application/vnd.kde.kformula", FileType.file),

  M_kia("application/vnd.kidspiration", FileType.file),

  M_kml("application/vnd.google-earth.kml+xml", FileType.file),

  M_kmz("application/vnd.google-earth.kmz", FileType.file),

  M_kne("application/vnd.kinar", FileType.file),

  M_knp("application/vnd.kinar", FileType.file),

  M_kon("application/vnd.kde.kontour", FileType.file),

  M_kpr("application/vnd.kde.kpresenter", FileType.file),

  M_kpt("application/vnd.kde.kpresenter", FileType.file),

  M_ksp("application/vnd.kde.kspread", FileType.file),

  M_ktr("application/vnd.kahootz", FileType.file),

  M_ktz("application/vnd.kahootz", FileType.file),

  M_kwd("application/vnd.kde.kword", FileType.file),

  M_kwt("application/vnd.kde.kword", FileType.file),

  M_lasxml("application/vnd.las.las+xml", FileType.file),

  M_latex("application/x-latex", FileType.file),

  M_lbd("application/vnd.llamagraphics.life-balance.desktop", FileType.file),

  M_lbe("application/vnd.llamagraphics.life-balance.exchange+xml", FileType.file),

  M_les("application/vnd.hhe.lesson-player", FileType.file),

  M_lha("application/octet-stream", FileType.file),

  M_link66("application/vnd.route66.link66+xml", FileType.file),

  M_list3820("application/vnd.ibm.modcap", FileType.file),

  M_listafp("application/vnd.ibm.modcap", FileType.file),

  M_lostxml("application/lost+xml", FileType.file),

  M_lrf("application/octet-stream", FileType.file),

  M_lrm("application/vnd.ms-lrm", FileType.file),

  M_ltf("application/vnd.frogans.ltf", FileType.file),

  M_lwp("application/vnd.lotus-wordpro", FileType.file),

  M_lzh("application/octet-stream", FileType.file),

  M_m13("application/x-msmediaview", FileType.file),

  M_m14("application/x-msmediaview", FileType.file),

  M_m21("application/mp21", FileType.file),

  M_m3u8("application/vnd.apple.mpegurl", FileType.file),

  M_ma("application/mathematica", FileType.file),

  M_mads("application/mads+xml", FileType.file),

  M_mag("application/vnd.ecowin.chart", FileType.file),

  M_maker("application/vnd.framemaker", FileType.file),

  M_mathml("application/mathml+xml", FileType.file),

  M_mb("application/mathematica", FileType.file),

  M_mbk("application/vnd.mobius.mbk", FileType.file),

  M_mbox("application/mbox", FileType.file),

  M_mc1("application/vnd.medcalcdata", FileType.file),

  M_mcd("application/vnd.mcd", FileType.file),

  M_mdb("application/x-msaccess", FileType.file),

  M_mesh("model/mesh", FileType.file),

  M_meta4("application/metalink4+xml", FileType.file),

  M_mets("application/mets+xml", FileType.file),

  M_mfm("application/vnd.mfmp", FileType.file),

  M_mgp("application/vnd.osgeo.mapguide.package", FileType.file),

  M_mgz("application/vnd.proteus.magazine", FileType.file),

  M_mif("application/x-mif", FileType.file),

  M_mime("message/rfc822", FileType.file),

  M_mlp("application/vnd.dolby.mlp", FileType.file),

  M_mmd("application/vnd.chipnuts.karaoke-mmd", FileType.file),

  M_mmf("application/vnd.smaf", FileType.file),

  M_mny("application/x-msmoney", FileType.file),

  M_mobi("application/x-mobipocket-ebook", FileType.file),

  M_mods("application/mods+xml", FileType.file),

  M_mp21("application/mp21", FileType.file),

  M_mpc("application/vnd.mophun.certificate", FileType.file),

  M_mp4s("application/mp4", FileType.file),

  M_mpkg("application/vnd.apple.installer+xml", FileType.file),

  M_mpm("application/vnd.blueice.multipass", FileType.file),

  M_mpn("application/vnd.mophun.application", FileType.file),

  M_mpp("application/vnd.ms-project", FileType.file),

  M_mpt("application/vnd.ms-project", FileType.file),

  M_mpy("application/vnd.ibm.minipay", FileType.file),

  M_mqy("application/vnd.mobius.mqy", FileType.file),

  M_mrc("application/marc", FileType.file),

  M_mrcx("application/marcxml+xml", FileType.file),

  M_mscml("application/mediaservercontrol+xml", FileType.file),

  M_mseed("application/vnd.fdsn.mseed", FileType.file),

  M_mseq("application/vnd.mseq", FileType.file),

  M_msf("application/vnd.epson.msf", FileType.file),

  M_msh("model/mesh", FileType.file),

  M_msi("application/x-msdownload", FileType.file),

  M_msl("application/vnd.mobius.msl", FileType.file),

  M_msty("application/vnd.muvee.style", FileType.file),

  M_mts("model/vnd.mts", FileType.file),

  M_mus("application/vnd.musician", FileType.file),

  M_musicxml("application/vnd.recordare.musicxml+xml", FileType.file),

  M_mvb("application/x-msmediaview", FileType.file),

  M_mwf("application/vnd.mfer", FileType.file),

  M_mxf("application/mxf", FileType.file),

  M_mxl("application/vnd.recordare.musicxml", FileType.file),

  M_mxml("application/xv+xml", FileType.file),

  M_mxs("application/vnd.triscape.mxs", FileType.file),

  M_n_gage("application/vnd.nokia.n-gage.symbian.install", FileType.file),

  M_nb("application/mathematica", FileType.file),

  M_nbp("application/vnd.wolfram.player", FileType.file),

  M_nc("application/x-netcdf", FileType.file),

  M_ncx("application/x-dtbncx+xml", FileType.file),

  M_ngdat("application/vnd.nokia.n-gage.data", FileType.file),

  M_nlu("application/vnd.neurolanguage.nlu", FileType.file),

  M_nml("application/vnd.enliven", FileType.file),

  M_nnd("application/vnd.noblenet-directory", FileType.file),

  M_nns("application/vnd.noblenet-sealer", FileType.file),

  M_nnw("application/vnd.noblenet-web", FileType.file),

  M_nsf("application/vnd.lotus-notes", FileType.file),

  M_oa2("application/vnd.fujitsu.oasys2", FileType.file),

  M_oa3("application/vnd.fujitsu.oasys3", FileType.file),

  M_oas("application/vnd.fujitsu.oasys", FileType.file),

  M_obd("application/x-msbinder", FileType.file),

  M_oda("application/oda", FileType.file),

  M_ogx("application/ogg", FileType.file),

  M_onepkg("application/onenote", FileType.file),

  M_onetmp("application/onenote", FileType.file),

  M_onetoc("application/onenote", FileType.file),

  M_onetoc2("application/onenote", FileType.file),

  M_opf("application/oebps-package+xml", FileType.file),

  M_oprc("application/vnd.palm", FileType.file),

  M_org("application/vnd.lotus-organizer", FileType.file),

  M_osf("application/vnd.yamaha.openscoreformat", FileType.file),

  M_osfpvg("application/vnd.yamaha.openscoreformat.osfpvg+xml", FileType.file),

  M_otf("application/x-font-otf", FileType.file),

  M_p10("application/pkcs10", FileType.file),

  M_p12("application/x-pkcs12", FileType.file),

  M_p7b("application/x-pkcs7-certificates", FileType.file),

  M_p7c("application/pkcs7-mime", FileType.file),

  M_p7m("application/pkcs7-mime", FileType.file),

  M_p7r("application/x-pkcs7-certreqresp", FileType.file),

  M_p7s("application/pkcs7-signature", FileType.file),

  M_p8("application/pkcs8", FileType.file),

  M_paw("application/vnd.pawaafile", FileType.file),

  M_pbd("application/vnd.powerbuilder6", FileType.file),

  M_pcf("application/x-font-pcf", FileType.file),

  M_pcl("application/vnd.hp-pcl", FileType.file),

  M_pclxl("application/vnd.hp-pclxl", FileType.file),

  M_pcurl("application/vnd.curl.pcurl", FileType.file),

  M_pdb("application/vnd.palm", FileType.file),

  M_pfa("application/x-font-type1", FileType.file),

  M_pfb("application/x-font-type1", FileType.file),

  M_pfm("application/x-font-type1", FileType.file),

  M_pfr("application/font-tdpfr", FileType.file),

  M_pfx("application/x-pkcs12", FileType.file),

  M_pgn("application/x-chess-pgn", FileType.file),

  M_pgp("application/pgp-encrypted", FileType.file),

  M_pkg("application/octet-stream", FileType.file),

  M_pki("application/pkixcmp", FileType.file),

  M_pkipath("application/pkix-pkipath", FileType.file),

  M_plb("application/vnd.3gpp.pic-bw-large", FileType.file),

  M_plc("application/vnd.mobius.plc", FileType.file),

  M_plf("application/vnd.pocketlearn", FileType.file),

  M_pml("application/vnd.ctc-posml", FileType.file),

  M_portpkg("application/vnd.macports.portpkg", FileType.file),

  M_ppd("application/vnd.cups-ppd", FileType.file),

  M_pqa("application/vnd.palm", FileType.file),

  M_prc("application/x-mobipocket-ebook", FileType.file),

  M_pre("application/vnd.lotus-freelance", FileType.file),

  M_prf("application/pics-rules", FileType.file),

  M_ps("application/postscript", FileType.file),

  M_psb("application/vnd.3gpp.pic-bw-small", FileType.file),

  M_psf("application/x-font-linux-psf", FileType.file),

  M_pskcxml("application/pskc+xml", FileType.file),

  M_ptid("application/vnd.pvi.ptid1", FileType.file),

  M_pub("application/x-mspublisher", FileType.file),

  M_pvb("application/vnd.3gpp.pic-bw-var", FileType.file),

  M_pwn("application/vnd.3m.post-it-notes", FileType.file),

  M_qam("application/vnd.epson.quickanime", FileType.file),

  M_qbo("application/vnd.intu.qbo", FileType.file),

  M_qfx("application/vnd.intu.qfx", FileType.file),

  M_qps("application/vnd.publishare-delta-tree", FileType.file),

  M_qwd("application/vnd.quark.quarkxpress", FileType.file),

  M_qwt("application/vnd.quark.quarkxpress", FileType.file),

  M_qxb("application/vnd.quark.quarkxpress", FileType.file),

  M_qxd("application/vnd.quark.quarkxpress", FileType.file),

  M_qxl("application/vnd.quark.quarkxpress", FileType.file),

  M_qxt("application/vnd.quark.quarkxpress", FileType.file),

  M_rcprofile("application/vnd.ipunplugged.rcprofile", FileType.file),

  M_rdf("application/rdf+xml", FileType.file),

  M_rdz("application/vnd.data-vision.rdz", FileType.file),

  M_rep("application/vnd.businessobjects", FileType.file),

  M_res("application/x-dtbresource+xml", FileType.file),

  M_rif("application/reginfo+xml", FileType.file),

  M_rl("application/resource-lists+xml", FileType.file),

  M_rld("application/resource-lists-diff+xml", FileType.file),

  M_rms("application/vnd.jcp.javame.midlet-rms", FileType.file),

  M_rnc("application/relax-ng-compact-syntax", FileType.file),

  M_rp9("application/vnd.cloanto.rp9", FileType.file),

  M_rpss("application/vnd.nokia.radio-presets", FileType.file),

  M_rpst("application/vnd.nokia.radio-preset", FileType.file),

  M_rq("application/sparql-query", FileType.file),

  M_rs("application/rls-services+xml", FileType.file),

  M_rsd("application/rsd+xml", FileType.file),

  M_rss("application/rss+xml", FileType.file),

  M_saf("application/vnd.yamaha.smaf-audio", FileType.file),

  M_sbml("application/sbml+xml", FileType.file),

  M_sc("application/vnd.ibm.secure-container", FileType.file),

  M_scd("application/x-msschedule", FileType.file),

  M_scm("application/vnd.lotus-screencam", FileType.file),

  M_scq("application/scvp-cv-request", FileType.file),

  M_scs("application/scvp-cv-response", FileType.file),

  M_sda("application/vnd.stardivision.draw", FileType.file),

  M_sdc("application/vnd.stardivision.calc", FileType.file),

  M_sdd("application/vnd.stardivision.impress", FileType.file),

  M_sdkd("application/vnd.solent.sdkm+xml", FileType.file),

  M_sdkm("application/vnd.solent.sdkm+xml", FileType.file),

  M_sdp("application/sdp", FileType.file),

  M_sdw("application/vnd.stardivision.writer", FileType.file),

  M_see("application/vnd.seemail", FileType.file),

  M_seed("application/vnd.fdsn.seed", FileType.file),

  M_sema("application/vnd.sema", FileType.file),

  M_semd("application/vnd.semd", FileType.file),

  M_semf("application/vnd.semf", FileType.file),

  M_ser("application/java-serialized-object", FileType.file),

  M_setpay("application/set-payment-initiation", FileType.file),

  M_setreg("application/set-registration-initiation", FileType.file),

  M_sfd_hdstx("application/vnd.hydrostatix.sof-data", FileType.file),

  M_sfs("application/vnd.spotfire.sfs", FileType.file),

  M_sgl("application/vnd.stardivision.writer-global", FileType.file),

  M_sh("application/x-sh", FileType.file),

  M_shar("application/x-shar", FileType.file),

  M_shf("application/shf+xml", FileType.file),

  M_sig("application/pgp-signature", FileType.file),

  M_silo("model/mesh", FileType.file),

  M_sis("application/vnd.symbian.install", FileType.file),

  M_sisx("application/vnd.symbian.install", FileType.file),

  M_sit("application/x-stuffit", FileType.file),

  M_sitx("application/x-stuffitx", FileType.file),

  M_skd("application/vnd.koan", FileType.file),

  M_skm("application/vnd.koan", FileType.file),

  M_skp("application/vnd.koan", FileType.file),

  M_skt("application/vnd.koan", FileType.file),

  M_sldm("application/vnd.ms-powerpoint.slide.macroenabled.12", FileType.file),

  M_sldx("application/vnd.openxmlformats-officedocument.presentationml.slide", FileType.file),

  M_slt("application/vnd.epson.salt", FileType.file),

  M_sm("application/vnd.stepmania.stepchart", FileType.file),

  M_smf("application/vnd.stardivision.math", FileType.file),

  M_smi("application/smil+xml", FileType.file),

  M_smil("application/smil+xml", FileType.file),

  M_snf("application/x-font-snf", FileType.file),

  M_so("application/octet-stream", FileType.file),

  M_spc("application/x-pkcs7-certificates", FileType.file),

  M_spf("application/vnd.yamaha.smaf-phrase", FileType.file),

  M_spl("application/x-futuresplash", FileType.file),

  M_spp("application/scvp-vp-response", FileType.file),

  M_spq("application/scvp-vp-request", FileType.file),

  M_src("application/x-wais-source", FileType.file),

  M_sru("application/sru+xml", FileType.file),

  M_srx("application/sparql-results+xml", FileType.file),

  M_sse("application/vnd.kodak-descriptor", FileType.file),

  M_ssf("application/vnd.epson.ssf", FileType.file),

  M_ssml("application/ssml+xml", FileType.file),

  M_st("application/vnd.sailingtracker.track", FileType.file),

  M_stf("application/vnd.wt.stf", FileType.file),

  M_stk("application/hyperstudio", FileType.file),

  M_stl("application/vnd.ms-pki.stl", FileType.file),

  M_str("application/vnd.pg.format", FileType.file),

  M_stw("application/vnd.sun.xml.writer.template", FileType.file),

  M_sus("application/vnd.sus-calendar", FileType.file),

  M_susp("application/vnd.sus-calendar", FileType.file),

  M_sv4cpio("application/x-sv4cpio", FileType.file),

  M_sv4crc("application/x-sv4crc", FileType.file),

  M_svc("application/vnd.dvb.service", FileType.file),

  M_svd("application/vnd.svd", FileType.file),

  M_swa("application/x-director", FileType.file),

  M_swf("application/x-shockwave-flash", FileType.file),

  M_swi("application/vnd.aristanetworks.swi", FileType.file),

  M_tao("application/vnd.tao.intent-module-archive", FileType.file),

  M_tcap("application/vnd.3gpp2.tcap", FileType.file),

  M_tcl("application/x-tcl", FileType.file),

  M_teacher("application/vnd.smart.teacher", FileType.file),

  M_tei("application/tei+xml", FileType.file),

  M_teicorpus("application/tei+xml", FileType.file),

  M_tex("application/x-tex", FileType.file),

  M_texi("application/x-texinfo", FileType.file),

  M_texinfo("application/x-texinfo", FileType.file),

  M_tfi("application/thraud+xml", FileType.file),

  M_tfm("application/x-tex-tfm", FileType.file),

  M_thmx("application/vnd.ms-officetheme", FileType.file),

  M_tmo("application/vnd.tmobile-livetv", FileType.file),

  M_torrent("application/x-bittorrent", FileType.file),

  M_tpl("application/vnd.groove-tool-template", FileType.file),

  M_tpt("application/vnd.trid.tpt", FileType.file),

  M_tra("application/vnd.trueapp", FileType.file),

  M_trm("application/x-msterminal", FileType.file),

  M_tsd("application/timestamped-data", FileType.file),

  M_twd("application/vnd.simtech-mindmapper", FileType.file),

  M_twds("application/vnd.simtech-mindmapper", FileType.file),

  M_txd("application/vnd.genomatix.tuxedo", FileType.file),

  M_txf("application/vnd.mobius.txf", FileType.file),

  M_u32("application/x-authorware-bin", FileType.file),

  M_udeb("application/x-debian-package", FileType.file),

  M_ufd("application/vnd.ufdl", FileType.file),

  M_ufdl("application/vnd.ufdl", FileType.file),

  M_umj("application/vnd.umajin", FileType.file),

  M_unityweb("application/vnd.unity", FileType.file),

  M_uoml("application/vnd.uoml+xml", FileType.file),

  M_ustar("application/x-ustar", FileType.file),

  M_utz("application/vnd.uiq.theme", FileType.file),

  M_uvd("application/vnd.dece.data", FileType.file),

  M_uvf("application/vnd.dece.data", FileType.file),

  M_uvt("application/vnd.dece.ttml+xml", FileType.file),

  M_uvvd("application/vnd.dece.data", FileType.file),

  M_uvvf("application/vnd.dece.data", FileType.file),

  M_uvvt("application/vnd.dece.ttml+xml", FileType.file),

  M_uvvx("application/vnd.dece.unspecified", FileType.file),

  M_uvx("application/vnd.dece.unspecified", FileType.file),

  M_vcd("application/x-cdlink", FileType.file),

  M_vcg("application/vnd.groove-vcard", FileType.file),

  M_vcx("application/vnd.vcx", FileType.file),

  M_vis("application/vnd.visionary", FileType.file),

  M_vor("application/vnd.stardivision.writer", FileType.file),

  M_vox("application/x-authorware-bin", FileType.file),

  M_vrml("model/vrml", FileType.file),

  M_vsd("application/vnd.visio", FileType.file),

  M_vsf("application/vnd.vsf", FileType.file),

  M_vss("application/vnd.visio", FileType.file),

  M_vst("application/vnd.visio", FileType.file),

  M_vsw("application/vnd.visio", FileType.file),

  M_vtu("model/vnd.vtu", FileType.file),

  M_w3d("application/x-director", FileType.file),

  M_wad("application/x-doom", FileType.file),

  M_wbs("application/vnd.criticaltools.wbs+xml", FileType.file),

  M_wbxml("application/vnd.wap.wbxml", FileType.file),

  M_wcm("application/vnd.ms-works", FileType.file),

  M_wdb("application/vnd.ms-works", FileType.file),

  M_wg("application/vnd.pmi.widget", FileType.file),

  M_wgt("application/widget", FileType.file),

  M_wks("application/vnd.ms-works", FileType.file),

  M_wmd("application/x-ms-wmd", FileType.file),

  M_wmf("application/x-msmetafile", FileType.file),

  // <!-- Compiled WML -->
  M_wmlc("application/vnd.wap.wmlc", FileType.file),

  // <!-- Compiled WML Script -->
  M_wmlsc("application/vnd.wap.wmlscriptc", FileType.file),

  M_wmz("application/x-ms-wmz", FileType.file),

  M_woff("application/x-font-woff", FileType.file),

  M_wpd("application/vnd.wordperfect", FileType.file),

  M_wpl("application/vnd.ms-wpl", FileType.file),

  M_wps("application/vnd.ms-works", FileType.file),

  M_wqd("application/vnd.wqd", FileType.file),

  M_wri("application/x-mswrite", FileType.file),

  M_wrl("model/vrml", FileType.file),

  M_wsdl("application/wsdl+xml", FileType.file),

  M_wspolicy("application/wspolicy+xml", FileType.file),

  M_wtb("application/vnd.webturbo", FileType.file),

  M_x32("application/x-authorware-bin", FileType.file),

  M_x3d("application/vnd.hzn-3d-crossword", FileType.file),

  M_xap("application/x-silverlight-app", FileType.file),

  M_xar("application/vnd.xara", FileType.file),

  M_xbap("application/x-ms-xbap", FileType.file),

  M_xbd("application/vnd.fujixerox.docuworks.binder", FileType.file),

  M_xdf("application/xcap-diff+xml", FileType.file),

  M_xdm("application/vnd.syncml.dm+xml", FileType.file),

  M_xdp("application/vnd.adobe.xdp+xml", FileType.file),

  M_xdssc("application/dssc+xml", FileType.file),

  M_xdw("application/vnd.fujixerox.docuworks", FileType.file),

  M_xenc("application/xenc+xml", FileType.file),

  M_xer("application/patch-ops-error+xml", FileType.file),

  M_xfdf("application/vnd.adobe.xfdf", FileType.file),

  M_xfdl("application/vnd.xfdl", FileType.file),

  M_xht("application/xhtml+xml", FileType.file),

  M_xhtml("application/xhtml+xml", FileType.file),

  M_xhvml("application/xv+xml", FileType.file),

  M_xo("application/vnd.olpc-sugar", FileType.file),

  M_xop("application/xop+xml", FileType.file),

  M_xpi("application/x-xpinstall", FileType.file),

  M_xpr("application/vnd.is-xpr", FileType.file),

  M_xps("application/vnd.ms-xpsdocument", FileType.file),

  M_xpw("application/vnd.intercon.formnet", FileType.file),

  M_xpx("application/vnd.intercon.formnet", FileType.file),

  M_xyz("chemical/x-xyz", FileType.file),

  M_yang("application/yang", FileType.file),

  M_xsm("application/vnd.syncml+xml", FileType.file),

  M_xspf("application/xspf+xml", FileType.file),

  M_xul("application/vnd.mozilla.xul+xml", FileType.file),

  M_xvm("application/xv+xml", FileType.file),

  M_xvml("application/xv+xml", FileType.file),

  M_yin("application/yin+xml", FileType.file),

  M_zaz("application/vnd.zzazz.deck+xml", FileType.file),

  M_zir("application/vnd.zul", FileType.file),

  M_zirz("application/vnd.zul", FileType.file),

  M_zmm("application/vnd.handheld-entertainment+xml", FileType.file);

  private String mimeType;

  // 对应文件分类
  private FileType fileType;

  public String getMimeType() {
    return mimeType;
  }

  public FileType getFileType() {
    return fileType;
  }

  /**
   * 获取文件扩展名称.
   */
  public String getFileExt() {
    if (MimeType.M_tar_gz.equals(this)) {
      return this.name().substring(2).replace("_", ".");
    }
    // M_n_gage/M_fe_launch
    return this.name().substring(2).replace("_", "-");
  }

  /**
   * 构造.
   *
   * @param mimeType MimeType
   * @param fileType FileType
   */
  private MimeType(String mimeType, FileType fileType) {
    this.mimeType = mimeType;
    this.fileType = fileType;
  }

  /**
   * 通过文件名称获取MimeType.
   */
  public static MimeType fileMimeType(final String fileName) {
    String fileExt = getExtension(fileName).toLowerCase();
    return convert(fileExt);
  }

  /**
   * 通过扩展名称获取MimeType.
   */
  public static MimeType convert(final String fileExt) {
    String name = "M_" + fileExt.toLowerCase().replace("-", "_").replace(".", "_");
    try {
      return MimeType.valueOf(name);
    } catch (Exception ex) {
      LogFactory.getLog(MimeType.class).debug(ex);
    }
    return null;
  }

  /** 获取扩展名. */
  public static String getExtension(final String fileName) throws IllegalArgumentException {
    if (fileName.endsWith("tar.gz")) {
      return "tar.gz";
    }
    return FilenameUtils.getExtension(fileName);
  }

  /**
   * MimeType 对应的 扩展名称数组.
   */
  public static List<MimeType> mimeType(String mimeType) {
    mimeType = mimeType.toLowerCase();
    List<MimeType> list = new ArrayList<MimeType>();
    for (MimeType type : MimeType.values()) {
      if (type.getMimeType().startsWith(mimeType)) {
        list.add(type);
      }
    }
    return list;
  }

  /**
   * 接受的文件类型.
   */
  public static List<MimeType> acceptMimeType() {
    List<MimeType> list = new ArrayList<MimeType>();
    for (FileType type : FileType.values()) {
      list.addAll(acceptMimeType(type));
    }
    return list;
  }

  /**
   * 接受的文件类型.
   */
  public static List<MimeType> acceptMimeType(FileType fileType) {
    List<MimeType> list = new ArrayList<MimeType>();
    switch (fileType) {
      case text:
        list.add(MimeType.M_doc);
        list.add(MimeType.M_docx);
        list.add(MimeType.M_ppt);
        list.add(MimeType.M_pptx);
        list.add(MimeType.M_xls);
        list.add(MimeType.M_xlsx);
        list.add(MimeType.M_txt);
        list.add(MimeType.M_pdf);
        list.add(MimeType.M_rtf);
        list.add(MimeType.M_xml);
        list.add(MimeType.M_bpmn);
        list.add(MimeType.M_ttf);
        break;
      case video:
        list.add(MimeType.M_swf);
        list.add(MimeType.M_flv);
        list.add(MimeType.M_rm);
        list.add(MimeType.M_3gp);
        list.add(MimeType.M_wmv);
        list.add(MimeType.M_mov);
        list.add(MimeType.M_avi);
        list.add(MimeType.M_mp4);
        break;
      case image:
        list.add(MimeType.M_jpg);
        list.add(MimeType.M_jpeg);
        list.add(MimeType.M_bmp);
        list.add(MimeType.M_gif);
        list.add(MimeType.M_png);
        list.add(MimeType.M_tif);
        list.add(MimeType.M_webp);
        break;
      case audio:
        list.add(MimeType.M_mp3);
        list.add(MimeType.M_wav);
        list.add(MimeType.M_wma);
        break;
      case zip:
        list.add(MimeType.M_zip);
        list.add(MimeType.M_xzip);
        list.add(MimeType.M_rar);
        list.add(MimeType.M_7z);
        list.add(MimeType.M_gz);
        list.add(MimeType.M_tar_gz);
        list.add(MimeType.M_tar);
        list.add(MimeType.M_bz);
        list.add(MimeType.M_bz2);
        list.add(MimeType.M_apk);
        break;
      case file:
        list.add(MimeType.M_otf);
        list.add(MimeType.M_woff);
        break;
      default:
        break;
    }
    return list;
  }

  /** TEST . */
  public static void main(String[] args) {
    Log log = LogFactory.getLog(MimeType.class);

    MimeType mimeType = MimeType.convert("js");
    log.info("js " + ((null == mimeType) ? "" : mimeType.getMimeType()));

    mimeType = MimeType.convert("css");
    log.info("css " + ((null == mimeType) ? "" : mimeType.getMimeType()));

    mimeType = MimeType.convert("png");
    log.info("png " + ((null == mimeType) ? "" : mimeType.getMimeType()));

    log.info("image include: ");
    List<MimeType> mimeTypes = mimeType("image");
    for (MimeType type : mimeTypes) {
      log.info(type.getFileExt());
    }

    List<MimeType> allowMimeType = MimeType.acceptMimeType();
    log.info("Allow  include: ");
    for (MimeType type : allowMimeType) {
      log.info(String.format("MimeTypeList['%s']='%s';", type.getFileExt(), type.getMimeType()));
      // log.info(type.getMimeType());
    }

  }

}
