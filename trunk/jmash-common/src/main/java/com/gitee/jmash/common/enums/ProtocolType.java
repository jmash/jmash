package com.gitee.jmash.common.enums;

/**
 * 通讯协议.
 *
 * @author CGD
 *
 */
public enum ProtocolType {

  HTTP,
  
  TCP,

  GRPC,
  
  HTTPS;

}
