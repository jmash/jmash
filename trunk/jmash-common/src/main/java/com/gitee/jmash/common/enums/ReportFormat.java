
package com.gitee.jmash.common.enums;

/**
 * 报表支持格式.
 *
 * @author CGD
 *
 */
public enum ReportFormat {

  PDF(".pdf", true),

  HTML(".html", true),

  XLS(".xls", false),

  XML(".xml", true);

  /** 文件后缀. */
  private String ext;
  /** 是否在线打开. */
  private Boolean isOnline;

  public Boolean getIsOnline() {
    return this.isOnline;
  }

  public String getExt() {
    return this.ext;
  }

  private ReportFormat(String ext, Boolean isOnline) {
    this.ext = ext;
    this.isOnline = isOnline;
  }
}
