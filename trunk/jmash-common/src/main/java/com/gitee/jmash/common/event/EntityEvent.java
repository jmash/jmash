
package com.gitee.jmash.common.event;

import com.gitee.jmash.common.grpc.GrpcContext;
import com.gitee.jmash.common.jaxrs.WebContext;
import jakarta.json.bind.Jsonb;
import jakarta.json.bind.JsonbBuilder;
import java.io.Serializable;
import java.security.Principal;
import java.util.Arrays;
import org.apache.commons.lang3.StringUtils;

/**
 * Entity Event.
 *
 * @author CGD
 */
public class EntityEvent implements Serializable {

  private static final long serialVersionUID = 1L;

  public static EntityEvent create(Object entity, Object id, Object[] state, String[] propertyNames,
      Object[] types) {
    return new EntityEvent(EntityEventType.Create, entity, id, state, propertyNames, types);
  }

  public static EntityEvent delete(Object entity, Object id, Object[] state, String[] propertyNames,
      Object[] types) {
    return new EntityEvent(EntityEventType.Delete, entity, id, state, propertyNames, types);
  }

  public static EntityEvent update(Object entity, Object id, Object[] currentState,
      Object[] previousState, String[] propertyNames, Object[] types) {
    return new EntityEvent(entity, id, currentState, previousState, propertyNames, types);
  }

  private EntityEventType eventType;

  /** 租户. */
  String tenant = "";
  /** 实体. */
  Object entity;
  /** 主键. */
  Object id;
  /** 新增/删除数据. */
  Object[] state;
  /** 变更数据变化后. */
  Object[] currentState;
  /** 变更数据变化前. */
  Object[] previousState;
  /** 字段名称数组. */
  String[] propertyNames;
  /** 字段类型数组. */
  Object[] types;

  private String userIp;

  private String proxyIp;

  private Principal principal;

  /**
   * 构造函数.
   */
  protected EntityEvent(EntityEventType eventType, Object entity, Object id, Object[] state,
      String[] propertyNames, Object[] types) {
    super();
    this.eventType = eventType;
    this.entity = entity;
    this.id = id;
    this.state = state;
    this.propertyNames = propertyNames;
    this.types = types;
    this.userIp = WebContext.USER_IP.get();
    if (StringUtils.isBlank(this.userIp)) {
      this.userIp = GrpcContext.USER_IP.get();
    }
    this.proxyIp = WebContext.PROXY_IP.get();
    if (StringUtils.isBlank(this.proxyIp)) {
      this.proxyIp = GrpcContext.USER_PROXY_IP.get();
    }
    this.principal = WebContext.USER_TOKEN.get();
    if (null == this.principal) {
      this.principal = GrpcContext.USER_TOKEN.get();
    }
  }

  protected EntityEvent(Object entity, Object id, Object[] currentState, Object[] previousState,
      String[] propertyNames, Object[] types) {
    super();
    if (WebContext.TENANT.get() != null) {
      this.tenant = WebContext.TENANT.get();
    }
    this.eventType = EntityEventType.Update;
    this.entity = entity;
    this.id = id;
    this.currentState = currentState;
    this.previousState = previousState;
    this.propertyNames = propertyNames;
    this.types = types;
    this.userIp = WebContext.USER_IP.get();
    if (null == this.userIp) {
      this.userIp = GrpcContext.USER_IP.get();
    }
    this.proxyIp = WebContext.PROXY_IP.get();
    if (null == this.proxyIp) {
      this.userIp = GrpcContext.USER_PROXY_IP.get();
    }
    this.principal = WebContext.USER_TOKEN.get();
    if (null == this.principal) {
      this.principal = GrpcContext.USER_TOKEN.get();
    }
  }

  public String getTenant() {
    return tenant;
  }

  public EntityEventType getEventType() {
    return eventType;
  }

  public Object getEntity() {
    return entity;
  }

  public Object getId() {
    return id;
  }

  public Object[] getState() {
    return state;
  }

  public Object[] getCurrentState() {
    return currentState;
  }

  public Object[] getPreviousState() {
    return previousState;
  }

  public String[] getPropertyNames() {
    return propertyNames;
  }

  public Object[] getTypes() {
    return types;
  }

  public String getUserIp() {
    return userIp;
  }

  public String getProxyIp() {
    return proxyIp;
  }

  public Principal getPrincipal() {
    return principal;
  }

  @Override
  public String toString() {
    try {
      try (Jsonb jsonb = JsonbBuilder.create()) {
        return "EntityEvent [eventType=" + eventType + ", entity=" + jsonb.toJson(entity) + ", id="
            + jsonb.toJson(id) + ", state=" + Arrays.toString(state) + ", currentState="
            + Arrays.toString(currentState) + ", previousState=" + Arrays.toString(previousState)
            + ", propertyNames=" + Arrays.toString(propertyNames) + ", types="
            + Arrays.toString(types) + ", userIp=" + userIp + ", proxyIp=" + proxyIp
            + ", principal=" + jsonb.toJson(principal) + "]";
      }
    } catch (Exception ex) {
      return "";
    }
  }

}
