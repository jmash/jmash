
package com.gitee.jmash.common.event;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.event.Observes;
import jakarta.enterprise.event.ObservesAsync;
import jakarta.enterprise.inject.spi.EventMetadata;
import jakarta.json.bind.Jsonb;
import jakarta.json.bind.JsonbBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Event Log .
 */
@ApplicationScoped
public class EntityEventLogHandle {

  private static Log log = LogFactory.getLog(EntityEventLogHandle.class);

  Jsonb jsonb = JsonbBuilder.create();

  /** 同步实体日志. */
  public void handleEvent(@Observes EntityEvent event, EventMetadata metadata) {
    if (log.isDebugEnabled()) {
      log.info(log(event));
      log.debug(metadata.toString());
      log.debug(event.toString());
    }
  }

  /** 异步实体日志. */
  public void handleAsyncEvent(@ObservesAsync EntityEvent event, EventMetadata metadata) {
    if (log.isDebugEnabled()) {
      log.debug(metadata.toString());
      log.debug("Async:" + event.toString());
      log.info("Async:" + log(event));
    }
  }

  /** 记录日志. */
  public String log(EntityEvent event) {
    StringBuffer buf = new StringBuffer(event.getEventType().name() + ",");
    buf.append(event.getEntity().getClass().getSimpleName() + ";");
    buf.append(jsonb.toJson(event.getEntity()));
    return buf.toString();
  }

}
