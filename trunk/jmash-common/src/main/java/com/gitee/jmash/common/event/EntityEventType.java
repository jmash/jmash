package com.gitee.jmash.common.event;


/**
 * Event Type .
 *
 * @author CGD
 *
 */
public enum EntityEventType {

  Create,
  
  Update,
  
  Delete;
}
