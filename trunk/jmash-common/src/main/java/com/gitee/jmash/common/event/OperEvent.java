package com.gitee.jmash.common.event;

import com.gitee.jmash.common.grpc.GrpcContext;
import com.gitee.jmash.common.jaxrs.WebContext;
import java.security.Principal;
import org.apache.commons.lang3.StringUtils;

/** 操作事件. */
public class OperEvent {

  public static OperEvent create(String tenant, String logName, OperLogLevel logLevel,
      String logMsg, String logContent) {
    return new OperEvent(tenant, logName, logLevel, logMsg, logContent);
  }

  public static OperEvent createNormal(String tenant, String logName, String logMsg,
      String logContent) {
    return new OperEvent(tenant, logName, OperLogLevel.Normal, logMsg, logContent);
  }

  public static OperEvent createNormal(String tenant, String logName, String logMsg) {
    return new OperEvent(tenant, logName, OperLogLevel.Normal, logMsg, "");
  }

  public static OperEvent createNormal(String logName, String logMsg) {
    return new OperEvent(null, logName, OperLogLevel.Normal, logMsg, "");
  }

  public static OperEvent createCritical(String tenant, String logName, String logMsg,
      String logContent) {
    return new OperEvent(tenant, logName, OperLogLevel.Critical, logMsg, logContent);
  }

  public static OperEvent createCritical(String tenant, String logName, String logMsg) {
    return new OperEvent(tenant, logName, OperLogLevel.Critical, logMsg, "");
  }

  public static OperEvent createCritical(String logName, String logMsg) {
    return new OperEvent(null, logName, OperLogLevel.Critical, logMsg, "");
  }

  // 租户.
  private String tenant;
  // 日志名称
  private String logName;
  // 操作日志级别
  private OperLogLevel logLevel;
  // 日志信息
  private String logMsg;
  // 日志内容
  private String logContent;
  // 事件IP地址
  private String userIp;
  // 事件代理IP
  private String proxyIp;
  // 事件用户
  private Principal principal;


  protected OperEvent(String tenant, String logName, OperLogLevel logLevel, String logMsg,
      String logContent) {
    super();
    this.tenant = tenant;
    this.logName = logName;
    this.logLevel = logLevel;
    this.logMsg = logMsg;
    this.logContent = logContent;
    this.userIp = WebContext.USER_IP.get();
    if (StringUtils.isBlank(this.userIp)) {
      this.userIp = GrpcContext.USER_IP.get();
    }
    this.proxyIp = WebContext.PROXY_IP.get();
    if (StringUtils.isBlank(this.proxyIp)) {
      this.proxyIp = GrpcContext.USER_PROXY_IP.get();
    }
    this.principal = WebContext.USER_TOKEN.get();
    if (null == this.principal) {
      this.principal = GrpcContext.USER_TOKEN.get();
    }
  }

  public String getTenant() {
    return tenant;
  }

  public String getLogName() {
    return logName;
  }

  public OperLogLevel getLogLevel() {
    return logLevel;
  }

  public String getLogMsg() {
    return logMsg;
  }

  public String getLogContent() {
    return logContent;
  }

  public String getUserIp() {
    return userIp;
  }

  public String getProxyIp() {
    return proxyIp;
  }

  public Principal getPrincipal() {
    return principal;
  }

  public String getPrincipalName() {
    return (null != this.principal) ? principal.getName() : "";
  }

  @Override
  public String toString() {
    return "OperEvent [tenant=" + tenant + ", logName=" + logName + ", logLevel=" + logLevel
        + ", logMsg=" + logMsg + ", logContent=" + logContent + ", userIp=" + userIp + ", proxyIp="
        + proxyIp + ", principal=" + getPrincipalName() + "]";
  }
}
