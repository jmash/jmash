
package com.gitee.jmash.common.event;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.event.Observes;
import jakarta.enterprise.event.ObservesAsync;
import jakarta.enterprise.inject.spi.EventMetadata;
import jakarta.json.bind.Jsonb;
import jakarta.json.bind.JsonbBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Event Log .
 */
@ApplicationScoped
public class OperEventLogHandle {

  private static Log log = LogFactory.getLog(OperEventLogHandle.class);

  Jsonb jsonb = JsonbBuilder.create();

  /** 同步实体日志. */
  public void handleEvent(@Observes OperEvent event, EventMetadata metadata) {
    if (log.isDebugEnabled()) {
      log.info(event.toString());
      log.debug(metadata.toString());
      log.debug(event.toString());
    }
  }

  /** 异步实体日志. */
  public void handleAsyncEvent(@ObservesAsync OperEvent event, EventMetadata metadata) {
    if (log.isDebugEnabled()) {
      log.debug(metadata.toString());
      log.debug("Async:" + event.toString());
      log.info("Async:" + event.toString());
    }
  }

}
