package com.gitee.jmash.common.event;

/** 操作日志类型. */
public enum OperLogLevel {
  
  // 常规日志
  Normal,
  
  // 关键日志
  Critical;
}
