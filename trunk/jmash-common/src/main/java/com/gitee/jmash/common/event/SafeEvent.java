
package com.gitee.jmash.common.event;

import com.gitee.jmash.common.grpc.GrpcContext;
import com.gitee.jmash.common.jaxrs.WebContext;
import java.io.Serializable;
import java.security.Principal;
import org.apache.commons.lang3.StringUtils;

/**
 * 安全事件.
 *
 * @author CGD
 */
public class SafeEvent implements Serializable {

  private static final long serialVersionUID = 1L;

  /** 登录. */
  public static SafeEvent login(String loginName, boolean status) {
    return login(loginName, "", status);
  }

  /** 多租户登录. */
  public static SafeEvent login(String loginName, String tenant, boolean status) {
    return new SafeEvent(SafeEventType.Login, tenant, status,
        loginName + (status ? "登录成功" : "登录失败"));
  }

  /** 多租户登录失败. */
  public static SafeEvent loginFail(String loginName, String tenant, String message) {
    return new SafeEvent(SafeEventType.Login, tenant, false, loginName + "登录失败:" + message);
  }

  /** 授权. */
  public static SafeEvent update(String message) {
    return update(message, null);
  }

  /** 多租户授权. */
  public static SafeEvent update(String message, String tenant) {
    return new SafeEvent(SafeEventType.Grant, tenant, true, message);
  }

  /** 多租户授权. */
  public static SafeEvent runAs(String message, String tenant) {
    return new SafeEvent(SafeEventType.RunAs, tenant, true, message);
  }

  /** 登出. */
  public static SafeEvent logout(String loginName) {
    return logout(loginName, null);
  }

  /** 登出. */
  public static SafeEvent logout(String loginName, String tenant) {
    return new SafeEvent(SafeEventType.Logout, tenant, true, loginName + "登出");
  }

  // 租户.
  private String tenant;
  // 安全事件类型.
  private SafeEventType eventType;
  // 事件状态
  private boolean status;
  // 事件消息
  private String message;
  // 事件IP地址
  private String userIp;
  // 事件代理IP
  private String proxyIp;
  // 事件用户
  private Principal principal;

  /**
   * 构造函数.
   */
  protected SafeEvent(SafeEventType eventType, String tenant, boolean status, String message) {
    super();
    this.eventType = eventType;
    this.tenant = tenant;
    this.status = status;
    this.message = message;
    this.userIp = WebContext.USER_IP.get();
    if (StringUtils.isBlank(this.userIp)) {
      this.userIp = GrpcContext.USER_IP.get();
    }
    this.proxyIp = WebContext.PROXY_IP.get();
    if (StringUtils.isBlank(this.proxyIp)) {
      this.proxyIp = GrpcContext.USER_PROXY_IP.get();
    }
    this.principal = WebContext.USER_TOKEN.get();
    if (null == this.principal) {
      this.principal = GrpcContext.USER_TOKEN.get();
    }
  }

  protected SafeEvent(SafeEventType eventType, boolean status, String message) {
    this(eventType, null, status, message);
  }

  /**
   * 构造函数.
   */
  protected SafeEvent(SafeEventType eventType, boolean status) {
    this(eventType, status, null);
  }

  public SafeEventType getEventType() {
    return eventType;
  }

  public String getTenant() {
    return tenant;
  }

  public boolean isStatus() {
    return status;
  }

  public String getMessage() {
    return message;
  }

  public String getUserIp() {
    return userIp;
  }

  public String getProxyIp() {
    return proxyIp;
  }

  public Principal getPrincipal() {
    return principal;
  }

  public String getPrincipalName() {
    return (null != this.principal) ? principal.getName() : "";
  }

  @Override
  public String toString() {
    return "SafeEvent [eventType=" + eventType + ", tenant=" + tenant + ", status=" + status
        + ", message=" + message + ", userIp=" + userIp + ", proxyIp=" + proxyIp + ", principal="
        + getPrincipalName() + "]";
  }

}
