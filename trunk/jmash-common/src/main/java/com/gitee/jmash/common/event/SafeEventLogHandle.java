
package com.gitee.jmash.common.event;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.event.Observes;
import jakarta.enterprise.event.ObservesAsync;
import jakarta.enterprise.inject.spi.EventMetadata;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 安全日志日志输出 .
 */
@ApplicationScoped
public class SafeEventLogHandle {

  private static Log log = LogFactory.getLog(SafeEventLogHandle.class);

  /** 同步安全日志. */
  public void handleEvent(@Observes SafeEvent event, EventMetadata metadata) {
    if (log.isDebugEnabled()) {
      log.debug(metadata.toString());
      log.debug(event.toString());
    }
    if (log.isInfoEnabled()) {
      log.info(log(event));
    }
  }

  /** 异步安全日志. */
  public void handleAsyncEvent(@ObservesAsync SafeEvent event, EventMetadata metadata) {
    if (log.isDebugEnabled()) {
      log.debug(metadata.toString());
      log.debug("Async:" + event.toString());
    }
    if (log.isInfoEnabled()) {
      log.info("Async:" + log(event));
    }
  }

  /** 记录日志. */
  public String log(SafeEvent event) {
    StringBuffer buf = new StringBuffer(event.getEventType().name() + ",");
    buf.append(event.getMessage());
    return buf.toString();
  }

}
