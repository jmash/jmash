
package com.gitee.jmash.common.event;

/**
 * 安全日志.
 *
 * @author CGD
 *
 */
public enum SafeEventType {

  Login,

  Grant,
  
  RunAs,

  Logout;

}
