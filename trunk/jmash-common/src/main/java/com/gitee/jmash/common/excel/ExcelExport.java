
package com.gitee.jmash.common.excel;

import com.gitee.jmash.common.excel.write.CellDictFormat;
import com.gitee.jmash.common.excel.write.HeaderExport;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.DataValidationConstraint;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Name;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.xssf.usermodel.XSSFDataValidation;
import org.apache.poi.xssf.usermodel.XSSFDataValidationConstraint;
import org.apache.poi.xssf.usermodel.XSSFDataValidationHelper;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * Excel Export.
 *
 * @author CGD
 *
 */
public class ExcelExport {

  private static final Log log = LogFactory.getLog(ExcelExport.class);

  XSSFWorkbook wb = null;

  XSSFSheet sheet = null;
  //Excel Head Row 数量.
  int headRowNum = 0;
  //数据行数量
  int rowNum = 0;

  /**
   * 表头和字段对应关系.
   */
  protected List<HeaderExport> headers = new ArrayList<HeaderExport>();

  public ExcelExport() {
    super();
    wb = new XSSFWorkbook();
  }

  public ExcelExport(XSSFWorkbook wb) {
    super();
    this.wb = wb;
  }

  /**
   * Create Excel Sheet.
   */
  public XSSFSheet createSheet(String sheetname) {
    this.sheet =
        StringUtils.isBlank(sheetname) ? this.wb.createSheet() : this.wb.createSheet(sheetname);
    //初始化Head数量
    this.headRowNum = 0;
    //初始化数据数量
    this.rowNum = 0;
    //清理Headers.
    this.headers.clear();
    return sheet;
  }

  /**
   * 创建保护和隐藏Sheet.
   */
  public XSSFSheet createSheet(String sheetname, String pwd, boolean hidden) {
    // 创建一个专门用来存放地区信息的隐藏sheet页
    XSSFSheet hideSheet = wb.createSheet(sheetname);
    hideSheet.setDefaultRowHeightInPoints(20);
    if (StringUtils.isNotBlank(pwd)) {
      hideSheet.protectSheet(pwd);
    }
    if (hidden) {
      // 这一行作用是将此sheet隐藏，功能未完成时注释此行,可以查看隐藏sheet中信息是否正确
      wb.setSheetHidden(wb.getSheetIndex(hideSheet), true);
    }
    return hideSheet;
  }

  public HeaderExport addHeader(HeaderExport header) {
    headers.add(header);
    return header;
  }

  public Collection<HeaderExport> addHeaderAll(Collection<HeaderExport> lists) {
    headers.addAll(lists);
    return headers;
  }

  /**
   * Write Excel Title.
   */
  public void writeTitle(String title) {
    int count = HeaderExport.totalColumn(headers);
    Row row1 = sheet.createRow(headRowNum++);
    row1.setHeightInPoints(30);
    if (count > 2) {
      // (起始行，结束行，起始列，结束列)。然后这个区域将被合并
      CellRangeAddress region0 = new CellRangeAddress(headRowNum - 1, headRowNum - 1, 0, count - 1);
      sheet.addMergedRegion(region0);
    }
    Cell row1Cell0 = row1.createCell(0);
    row1Cell0.setCellValue(title);
    row1Cell0.setCellStyle(getTitleStyle());
  }

  public void writeHeader() {
    writeHeader(headers, true);
  }

  public void writeHeader(List<HeaderExport> hs) {
    writeHeader(hs, false);
  }

  /**
   * Write Excel Header .
   *
   * @param hs Header List
   * @param rowRange Row合并
   */
  public void writeHeader(List<HeaderExport> hs, boolean rowRange) {
    Row row2 = sheet.createRow(headRowNum++);
    row2.setHeightInPoints(30);

    CellStyle headerStyle = getHeaderStyle();
    int column = 0;
    for (int i = 0; i < hs.size(); i++) {
      HeaderExport header = hs.get(i);

      // 列设定为默认单元格格式
      if (StringUtils.isNotBlank(header.getFormat())) {
        CellStyle cellStyle = getCellStyleFormat(header.getFormat());
        sheet.setDefaultColumnStyle(column, cellStyle);
        header.setCellStyle(cellStyle);
      }
      sheet.setColumnWidth(column, header.getWidth());

      if (header.getColumn() > 1 || (rowRange && header.getRow() > 1)) {
        try {
          // 合并单元格
          CellRangeAddress region0 = new CellRangeAddress(headRowNum - header.getRow(),
              headRowNum - 1, column, column + header.getColumn() - 1);
          sheet.addMergedRegion(region0);
        } catch (Exception ex) {
          log.error(ex);
        }
      }

      Cell cell = row2.createCell((short) column);
      cell.setCellStyle(headerStyle);
      RichTextString richString = new XSSFRichTextString(header.getHeader());
      cell.setCellValue(richString);

      column = column + header.getColumn();
    }
  }

  /**
   * 写数据.
   */
  public <E> void writeListData(List<E> datas) {
    if (null == datas) {
      return;
    }
    for (E data : datas) {
      writeData(data);
    }
  }

  /**
   * 设置单元格数据,返回是否已设置.
   *
   * @param header 导出列Header
   * @param rowData 行数据
   * @param cell 当前单元格
   */
  public boolean setCellValue(Cell cell, HeaderExport header, Object rowData) {
    return false;
  }

  /**
   * 写数据.
   */
  public <E> void writeData(E data) {
    int i = 0;
    Row row = this.sheet.createRow(headRowNum + rowNum++);
    for (HeaderExport header : headers) {

      Cell cell = row.createCell(i);
      boolean process = false;

      // 行定义处理优先.
      if (!process && null != header.getCellValueFormat()) {
        process = header.getCellValueFormat().setCellValue(cell, header, rowNum, data);
        
      }

      // 类重定义处理.
      if (!process) {
        process = setCellValue(cell, header, data);
      }

      i = i + header.getColumn();
    }
  }

  /**
   * 创建名称管理器.
   *
   * @param sheet XSSFSheet
   * @param sheetName SheetName
   * @param rowIndex 行索引
   * @param keyName 名称管理器名称
   * @param options 可选项
   */
  public Name createNameRange(XSSFSheet sheet, String sheetName, int rowIndex, String keyName,
      String[] options) {
    // 创建行
    Row row = sheet.createRow(rowIndex);
    row.createCell(0).setCellValue(keyName);

    // 添加选项
    for (int i = 0; i < options.length; i++) {
      Cell cell = row.createCell(i + 1);
      cell.setCellValue(options[i]);
    }

    // 创建名称管理器
    String range = getRange(1, rowIndex + 1, options.length);
    Name name = this.wb.createName();
    // key不可重复
    name.setNameName(keyName);
    String formula = sheetName + "!" + range;
    name.setRefersToFormula(formula);
    return name;
  }

  /**
   * 字典数据校验.
   *
   * @param dataRow 数据行数
   * @param initRowNum 首次初始化行数
   */
  public void dictDataValidation(int dataRow, int initRowNum) {
    int maxRow = (dataRow > initRowNum) ? dataRow + 100 : initRowNum;
    int startRow = getHeadRowNum();
    int j = 0;
    for (HeaderExport header : getHeaders()) {
      if (null != header.getCellValueFormat()
          && header.getCellValueFormat() instanceof CellDictFormat) {
        Map<String, ?> map = ((CellDictFormat) header.getCellValueFormat()).getDictMap();
        // 添加数据校验
        createDataValidation(getSheet(), startRow, maxRow, j, map.values().toArray(new String[0]),
            "请选择");
      }
      j++;
    }
  }

  /**
   * 联动校验配置.
   *
   * @param sheet XSSFSheet
   * @param offset 主影响单元格所在列，即此单元格由哪个单元格影响联动
   * @param rowIndex 行
   * @param colIndex 列
   * @param errorTip 错误提示
   */
  public void createDataValidation(XSSFSheet sheet, String offset, int rowIndex, int colIndex,
      String errorTip) {
    XSSFDataValidationHelper dvHelper = new XSSFDataValidationHelper(sheet);
    DataValidation dataValidation = getDataValidationByFormula(
        "INDIRECT($" + offset + (rowIndex) + ")", rowIndex, colIndex, dvHelper, errorTip);
    sheet.addValidationData(dataValidation);
  }

  /**
   * 下拉校验规则.
   */
  public void createDataValidation(XSSFSheet sheet, int startRow, int endRow, int col,
      String[] options, String errorTip) {
    XSSFDataValidationHelper dvHelper = new XSSFDataValidationHelper(sheet);
    // 校验规则
    DataValidationConstraint provConstraint = dvHelper.createExplicitListConstraint(options);
    // 四个参数分别是：起始行、终止行、起始列、终止列
    CellRangeAddressList provRangeAddressList =
        new CellRangeAddressList(startRow, endRow, col, col);
    DataValidation provinceDataValidation =
        dvHelper.createValidation(provConstraint, provRangeAddressList);
    // 验证
    provinceDataValidation.createErrorBox("错误提示", errorTip);
    provinceDataValidation.setShowErrorBox(true);
    provinceDataValidation.setSuppressDropDownArrow(true);
    sheet.addValidationData(provinceDataValidation);
  }

  /**
   * 下拉校验规则.
   */
  public void createDataValidation(XSSFSheet sheet, int startRow, int endRow, int col,
      String keyName, String errorTip) {
    XSSFDataValidationHelper dvHelper = new XSSFDataValidationHelper(sheet);
    // 校验规则
    DataValidationConstraint provConstraint = (XSSFDataValidationConstraint) dvHelper
        .createFormulaListConstraint(String.valueOf(keyName));
    // 四个参数分别是：起始行、终止行、起始列、终止列
    CellRangeAddressList provRangeAddressList =
        new CellRangeAddressList(startRow, endRow, col, col);
    DataValidation provinceDataValidation =
        dvHelper.createValidation(provConstraint, provRangeAddressList);
    // 验证
    provinceDataValidation.createErrorBox("错误提示", errorTip);
    provinceDataValidation.setShowErrorBox(true);
    provinceDataValidation.setSuppressDropDownArrow(true);
    sheet.addValidationData(provinceDataValidation);
  }

  /**
   * 加载下拉列表内容.
   */
  private DataValidation getDataValidationByFormula(String formulaString, int naturalRowIndex,
      int naturalColumnIndex, XSSFDataValidationHelper dvHelper, String errorTip) {
    // 加载下拉列表内容
    // 举例：若formulaString = "INDIRECT($A$2)" 表示规则数据会从名称管理器中获取key与单元格 A2 值相同的数据，
    XSSFDataValidationConstraint dvConstraint =
        (XSSFDataValidationConstraint) dvHelper.createFormulaListConstraint(formulaString);
    // 设置数据有效性加载在哪个单元格上。
    // 四个参数分别是：起始行、终止行、起始列、终止列
    int firstRow = naturalRowIndex - 1;
    int lastRow = naturalRowIndex - 1;
    int firstCol = naturalColumnIndex - 1;
    int lastCol = naturalColumnIndex - 1;
    CellRangeAddressList regions = new CellRangeAddressList(firstRow, lastRow, firstCol, lastCol);
    // 数据有效性对象
    // 绑定
    XSSFDataValidation dataValidation =
        (XSSFDataValidation) dvHelper.createValidation(dvConstraint, regions);
    dataValidation.setEmptyCellAllowed(false);
    if (dataValidation instanceof XSSFDataValidation) {
      dataValidation.setSuppressDropDownArrow(true);
      dataValidation.setShowErrorBox(true);
    } else {
      dataValidation.setSuppressDropDownArrow(false);
    }
    // 设置输入信息提示信息
    dataValidation.createPromptBox("下拉选择提示", errorTip);
    // 设置输入错误提示信息
    dataValidation.createErrorBox("选择错误提示", errorTip);
    return dataValidation;
  }

  /**
   * 计算formula.
   *
   * @param offset 偏移量，如果给0，表示从A列开始，1，就是从B列
   * @param rowId 第几行
   * @param colCount 一共多少列
   * @return 如果给入参 1,1,10. 表示从B1-K1。最终返回 $B$1:$K$1
   *
   */
  public String getRange(int offset, int rowId, int colCount) {
    char start = (char) ('A' + offset);
    if (colCount <= 25) {
      char end = (char) (start + colCount - 1);
      return "$" + start + "$" + rowId + ":$" + end + "$" + rowId;
    } else {
      char endPrefix = 'A';
      char endSuffix = 'A';
      if ((colCount - 25) / 26 == 0 || colCount == 51) { // 26-51之间，包括边界（仅两次字母表计算）
        if ((colCount - 25) % 26 == 0) { // 边界值
          endSuffix = (char) ('A' + 25);
        } else {
          endSuffix = (char) ('A' + (colCount - 25) % 26 - 1);
        }
      } else { // 51以上
        if ((colCount - 25) % 26 == 0) {
          endSuffix = (char) ('A' + 25);
          endPrefix = (char) (endPrefix + (colCount - 25) / 26 - 1);
        } else {
          endSuffix = (char) ('A' + (colCount - 25) % 26 - 1);
          endPrefix = (char) (endPrefix + (colCount - 25) / 26);
        }
      }
      return "$" + start + "$" + rowId + ":$" + endPrefix + endSuffix + "$" + rowId;
    }
  }

  public CellStyle getTitleStyle() {
    Font bigFont = createBigFont();
    return getFontCellStyle(bigFont);
  }

  public CellStyle getHeaderStyle() {
    Font bigFont = createFont();
    return getFontCellStyle(bigFont);
  }

  /** 获取单元格格式. */
  public CellStyle getCellStyleFormat(String format) {
    CellStyle cellStyle = this.wb.createCellStyle();
    DataFormat dataFormat = this.wb.createDataFormat();
    cellStyle.setDataFormat(dataFormat.getFormat(format));
    return cellStyle;
  }

  /**
   * 大字体.
   */
  public Font createBigFont() {
    Font font = this.wb.createFont();
    font.setBold(true); // 加粗
    font.setFontName("楷体"); // 字体名称
    font.setFontHeightInPoints((short) 16); // 字体大小
    return font;
  }

  /**
   * 创建字体.
   */
  public Font createFont() {
    Font font = this.wb.createFont();
    font.setFontName("楷体"); // 字体名称
    font.setFontHeightInPoints((short) 13); // 字体大小
    return font;
  }

  /** 字体风格. */
  public CellStyle getFontCellStyle(Font font) {
    // 表头样式
    CellStyle headStyle = this.wb.createCellStyle();
    headStyle.setFont(font);
    headStyle.setWrapText(true);

    headStyle.setVerticalAlignment(VerticalAlignment.CENTER); // 竖向居中
    headStyle.setAlignment(HorizontalAlignment.CENTER); // 横向居中
    // 边框
    headStyle.setBorderBottom(BorderStyle.NONE);
    headStyle.setBorderLeft(BorderStyle.NONE);
    headStyle.setBorderRight(BorderStyle.NONE);
    headStyle.setBorderTop(BorderStyle.NONE);
    return headStyle;
  }

  public XSSFWorkbook getWb() {
    return wb;
  }

  public XSSFSheet getSheet() {
    return sheet;
  }

  public int getHeadRowNum() {
    return headRowNum;
  }

  public int getRowNum() {
    return rowNum;
  }

  public List<HeaderExport> getHeaders() {
    return headers;
  }


}
