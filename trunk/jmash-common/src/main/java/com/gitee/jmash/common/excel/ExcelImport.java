
package com.gitee.jmash.common.excel;

import com.gitee.jmash.common.excel.read.CellStringReader;
import com.gitee.jmash.common.excel.read.HeaderField;
import com.gitee.jmash.common.excel.read.ReaderError;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellRangeAddress;

/**
 * Excel 导入.
 *
 * @author CGD
 *
 */
public class ExcelImport {

  private static final Log log = LogFactory.getLog(ExcelImport.class);

  Workbook wb = null;

  public void openExcel(InputStream is) throws IOException, InvalidFormatException {
    wb = WorkbookFactory.create(is);
  }

  public void openExcel(File file) throws IOException, InvalidFormatException {
    wb = WorkbookFactory.create(file);
  }

  /**
   * 表头和字段对应关系.
   */
  protected List<HeaderField> headerFields = new ArrayList<HeaderField>();

  /** 必填字段. */
  protected Set<String> requiredFields = new HashSet<String>();
  /** 错误信息. */
  List<ReaderError> errors = new ArrayList<ReaderError>();

  protected int startHeader = 1;

  protected int headerCount = 1;
  // 匹配表头数
  protected int matchHeaderCount = 0;
  // 匹配的头部字段
  protected Map<Integer, HeaderField> fields = null;

  public int getStartContent() {
    return startHeader + headerCount;
  }
 
  public Workbook getWb() {
    return wb;
  }

  public Row filterRow(int rowIndex, Row row) {
    return row;
  }

  public Object filterCell(int rowIndex, int colIndex, Cell cell) {
    return null;
  }

  /**
   * Excel 导入数据.
   */
  public List<Object[]> importObject(Sheet sheet, int startRowNum, int lastRowNum) {
    List<Object[]> objs = new ArrayList<Object[]>();
    CellStringReader reader = new CellStringReader(true);
    for (int i = startRowNum; i <= lastRowNum; i++) {
      Row row = sheet.getRow(i);
      row = filterRow(i, row);
      if (null == row) {
        continue;
      }
      int col = row.getLastCellNum() - row.getFirstCellNum();
      Object[] objArray = new Object[col];
      for (int j = row.getFirstCellNum(); j < row.getLastCellNum(); j++) {
        Cell cell = row.getCell(j);
        Object value = filterCell(i, j, cell);
        if (value != null) {
          objArray[j - row.getFirstCellNum()] = value;
        } else {
          objArray[j - row.getFirstCellNum()] =
              reader.cellValueRead("object", cell, getFormulaEval(), errors);;
        }
      }
      objs.add(objArray);

    }
    return objs;
  }

  /**
   * Excel 导入数据.
   */
  public List<Object[]> importObject(int sheetIndex) {
    Sheet sheet = wb.getSheetAt(sheetIndex);
    return importObject(sheet, sheet.getFirstRowNum(), sheet.getLastRowNum());
  }

  /**
   * 读表头.
   */
  public List<String> readerHeader(Sheet sheet, int startHeader, int headerCount) {
    List<String> header = new ArrayList<String>();
    CellStringReader reader = new CellStringReader(true);
    for (int i = startHeader; i < startHeader + headerCount; i++) {
      Row row = sheet.getRow(i);
      row = filterRow(i, row);
      if (null == row) {
        continue;
      }
      // 必须从0开始读
      for (int j = 0; j < row.getLastCellNum(); j++) {
        Object value = null;
        // 有数据开始读
        if (j >= row.getFirstCellNum()) {
          Cell cell = row.getCell(j);
          value = filterCell(i, j, cell);
          if (value == null) {
            if (this.isMergedRegion(sheet, i, j)) {
              value = getMergedRegionValue(sheet, i, j);
            } else {
              value = reader.cellValueRead("header", cell, getFormulaEval(), errors);
            }
          }
        }
        if (value == null) {
          value = StringUtils.EMPTY;
        }

        // 表头序号
        if (header.size() > j) {
          // 表头自动加
          String v1 = header.get(j);
          String v2 = String.valueOf(value);
          if (!v1.contains(v2)) {
            header.set(j, v1 + v2);
          }
        } else {
          header.add(j, String.valueOf(value));
        }
      }
    }
    return header;
  }

  /**
   * 读表头.
   */
  public List<Map<String, Object>> readerHeader(int sheetIndex) {
    Sheet sheet = wb.getSheetAt(sheetIndex);
    return readerHeader(sheet, fields, this.getStartHeader(), this.getHeaderCount());
  }

  /**
   * 读表头.
   */
  public List<Map<String, Object>> readerHeader(Sheet sheet, Map<Integer, HeaderField> fields,
      int start, int count) {
    List<Map<String, Object>> datas = new ArrayList<Map<String, Object>>();
    for (int i = start; i < start + count; i++) {
      Map<String, Object> map = new LinkedHashMap<String, Object>();
      List<String> header = readerHeader(sheet, i, 1);
      for (int j = 0; j < header.size(); j++) {
        HeaderField field = HeaderField.field("index" + j, "index" + j, false);
        if (fields.containsKey(j)) {
          field = fields.get(j);
        }
        map.put(field.getField(), header.get(j));
      }
      datas.add(map);
    }
    return datas;
  }

  /**
   * 读取数据.
   */
  public List<Map<String, Object>> readerData(int sheetIndex, Map<Integer, HeaderField> fields,
      int start, int count) {
    Sheet sheet = wb.getSheetAt(sheetIndex);
    return readerData(sheet, fields, start, count);
  }

  /**
   * 读数据.
   */
  public List<Map<String, Object>> readerData(Sheet sheet, Map<Integer, HeaderField> fields,
      int start, int count) {
    List<Map<String, Object>> datas = new ArrayList<Map<String, Object>>();
    for (int i = start; i < start + count; i++) {
      Row row = sheet.getRow(i);
      row = filterRow(i, row);
      if (null == row) {
        continue;
      }
      Map<String, Object> map = new LinkedHashMap<String, Object>();
      for (int j = row.getFirstCellNum(); j < row.getLastCellNum(); j++) {
        HeaderField field = null;
        if (fields.containsKey(j)) {
          field = fields.get(j);
        }
        if (null == field) {
          field = HeaderField.field("column" + j, "column" + j, false);
        }

        Cell cell = row.getCell(j);
        if (null == cell) {
          map.put(field.getField(), null);
          continue;
        }

        Object value =
            field.getReader().cellValueRead(field.getHeader(), cell, getFormulaEval(), errors);
        map.put(field.getField(), value);
      }
      datas.add(map);
    }
    return datas;
  }

  /**
   * 导入Map.
   */
  public List<Map<String, Object>> importMap(int sheetIndex) {
    Sheet sheet = wb.getSheetAt(sheetIndex);
    // 读表头
    List<String> headers = readerHeader(sheet, startHeader, headerCount);
    Map<Integer, HeaderField> fields = matchHeaders(headers);

    int startData = startHeader + headerCount;
    int count = sheet.getLastRowNum() - sheet.getFirstRowNum() - startHeader - headerCount + 1;
    // 读取数据
    List<Map<String, Object>> datas = readerData(sheet, fields, startData, count);
    return datas;
  }

  /**
   * Reader Entity.
   */
  public <T> List<T> importEntity(int sheetIndex, T pojo) throws NoSuchMethodException,
      IllegalAccessException, InvocationTargetException, InstantiationException {
    Sheet sheet = wb.getSheetAt(sheetIndex);

    // 读表头
    List<String> headers = readerHeader(sheet, startHeader, headerCount);
    Map<Integer, HeaderField> fields = matchHeaders(headers);

    int startData = startHeader + headerCount;
    int count = sheet.getLastRowNum() - sheet.getFirstRowNum() - startData + 1;
    // 读取数据
    PojoBuild<T> build = PojoBuild.create(pojo);
    List<T> datas = readerEntity(sheet, build, fields, startData, count);
    return datas;
  }

  /** Reader Entity. */
  public <T> List<T> importEntity(int sheetIndex, int dataStartRow, Map<String, Integer> propColumn,
      T pojo) throws IllegalAccessException, InvocationTargetException, InstantiationException,
      IllegalArgumentException, NoSuchMethodException, SecurityException {
    Sheet sheet = wb.getSheetAt(sheetIndex);

    int count = sheet.getLastRowNum() - sheet.getFirstRowNum() - dataStartRow + 1;
    // 读取数据
    PojoBuild<T> build = PojoBuild.create(pojo);
    List<T> datas = readerEntityByIndex(sheet, build, propColumn, dataStartRow, count);
    return datas;
  }

  /**
   * Reader Entity.
   */
  public <T> List<T> readerEntity(Sheet sheet, PojoBuild<T> build, Map<Integer, HeaderField> fields,
      int start, int count) throws NoSuchMethodException, IllegalAccessException,
      InvocationTargetException, InstantiationException {
    List<T> datas = new ArrayList<T>();
    for (int i = start; i < start + count; i++) {
      Row row = sheet.getRow(i);
      row = filterRow(i, row);
      if (null == row) {
        continue;
      }
      Object entity = build.builder();
      // 从有数据列读取
      for (int j = row.getFirstCellNum(); j <= row.getLastCellNum(); j++) {
        HeaderField field = null;
        if (fields.containsKey(j)) {
          field = fields.get(j);
        }
        if (null == field) {
          field = HeaderField.field("column" + j, "column" + j, false);
        }
        Cell cell = row.getCell(j);
        if (null != cell) {
          setEntityFieldValue(build, cell, entity, field);
        }
      }
      datas.add(build.build(entity));
    }
    return datas;
  }



  private void setEntityFieldValue(PojoBuild<?> build, Cell cell, Object entity, HeaderField field)
      throws IllegalAccessException, InvocationTargetException {
    Object value =
        field.getReader().cellValueRead(field.getHeader(), cell, getFormulaEval(), errors);
    if (value == null) {
      return;
    }
    // 多字段
    if (field.getFields().length > 0) {
      String[] values = ((String[]) value);
      for (int k = 0; k < field.getFields().length; k++) {
        if (values.length > k && StringUtils.isNotBlank(field.getFields()[k])) {
          build.setProperty(entity, field.getFields()[k], values[k]);
        }
      }
    } else {
      build.setProperty(entity, field.getField(), value);
    }
  }

  /** Reader Entity. */
  public <T> List<T> readerEntityByIndex(Sheet sheet, PojoBuild<T> build,
      Map<String, Integer> propColumn, int start, int count)
      throws IllegalAccessException, InvocationTargetException, InstantiationException,
      IllegalArgumentException, NoSuchMethodException, SecurityException {
    List<T> datas = new ArrayList<T>();
    for (int i = start; i < start + count; i++) {
      Row row = sheet.getRow(i);
      row = filterRow(i, row);
      if (null == row) {
        continue;
      }
      Object entity = build.builder();

      for (HeaderField field : this.headerFields) {
        if (!propColumn.containsKey(field.getField())) {
          log.error(String.format("找不到列 %s ", field.getField()));
          continue;
        }
        Integer j = propColumn.get(field.getField());
        if (j < row.getFirstCellNum() || j > row.getLastCellNum()) {
          log.error(String.format("该列无数据 %d ", j));
          continue;
        }
        Cell cell = row.getCell(j);
        setEntityFieldValue(build, cell, entity, field);
      }
      datas.add(build.build(entity));
    }
    return datas;
  }

  /**
   * 智能匹配表头.
   */
  public boolean autoMatchHeader(int sheetIndex, int startRow, int endRow, int headerCount,
      int matchCount) {
    Sheet sheet = wb.getSheetAt(sheetIndex);
    for (int i = startRow; i <= endRow; i++) {
      startHeader = i;
      List<String> headers = readerHeader(sheet, startHeader, headerCount);
      fields = matchHeaders(headers);
      if (matchHeaderCount >= matchCount) {
        return true;
      }
    }
    startHeader = startRow;
    return false;
  }

  /**
   * 匹配表头.
   */
  public Map<Integer, HeaderField> matchHeaders(List<String> headers) {
    // 表头对应关系
    fields = new HashMap<Integer, HeaderField>();
    int i = 0;
    matchHeaderCount = 0;
    List<HeaderField> allField = new ArrayList<HeaderField>();
    allField.addAll(this.headerFields);
    for (String header : headers) {

      boolean find = false;

      for (HeaderField field : allField) {
        if (field.getContain() && header.indexOf(field.getHeader()) > -1) {
          fields.put(i, field);
          allField.remove(field);
          matchHeaderCount++;
          find = true;
          break;
        }
        if (!field.getContain() && StringUtils.equals(field.getHeader().trim(), header)) {
          fields.put(i, field);
          allField.remove(field);
          matchHeaderCount++;
          find = true;
          break;
        }
      }
      if (!find) {
        fields.put(i, HeaderField.field(header, String.format("Column%d", i), false));
      }
      i++;
    }
    return fields;
  }

  /**
   * 获取合并单元格的值.
   */
  public Object getMergedRegionValue(Sheet sheet, int row, int column) {
    int sheetMergeCount = sheet.getNumMergedRegions();
    CellStringReader reader = new CellStringReader(true);
    for (int i = 0; i < sheetMergeCount; i++) {
      CellRangeAddress ca = sheet.getMergedRegion(i);
      int firstColumn = ca.getFirstColumn();
      int lastColumn = ca.getLastColumn();
      int firstRow = ca.getFirstRow();
      int lastRow = ca.getLastRow();

      if (row >= firstRow && row <= lastRow) {
        if (column >= firstColumn && column <= lastColumn) {
          Row frow = sheet.getRow(firstRow);
          Cell fcell = frow.getCell(firstColumn);
          return reader.cellValueRead("mergedValue", fcell, getFormulaEval(), errors);
        }
      }
    }
    return null;
  }

  /**
   * 判断指定的单元格是否是合并单元格.
   *
   * @param sheet Sheet
   * @param row 行下标
   * @param column 列下标
   */
  private boolean isMergedRegion(Sheet sheet, int row, int column) {
    int sheetMergeCount = sheet.getNumMergedRegions();
    for (int i = 0; i < sheetMergeCount; i++) {
      CellRangeAddress range = sheet.getMergedRegion(i);
      int firstColumn = range.getFirstColumn();
      int lastColumn = range.getLastColumn();
      int firstRow = range.getFirstRow();
      int lastRow = range.getLastRow();
      if (row >= firstRow && row <= lastRow) {
        if (column >= firstColumn && column <= lastColumn) {
          return true;
        }
      }
    }
    return false;
  }

  public int getStartHeader() {
    return this.startHeader;
  }

  public void setStartHeader(int startHeader) {
    this.startHeader = startHeader;
  }

  public int getHeaderCount() {
    return this.headerCount;
  }

  public void setHeaderCount(int headerCount) {
    this.headerCount = headerCount;
  }

  public Map<Integer, HeaderField> getFields() {
    return this.fields;
  }

  public List<ReaderError> getErrors() {
    return this.errors;
  }

  public FormulaEvaluator getFormulaEval() {
    return this.wb.getCreationHelper().createFormulaEvaluator();
  }

  public HeaderField addHeader(HeaderField headerField) {
    this.headerFields.add(headerField);
    return headerField;
  }

  public boolean addHeaders(List<HeaderField> headerFields) {
    return this.headerFields.addAll(headerFields);
  }
  
  public void setHeaders(List<HeaderField> headerFields) {
    this.headerFields.clear();
    this.headerFields.addAll(headerFields);
  }

  /**
   * 增加必填字段.
   */
  public void addRequiredField(String requiredField) {
    this.requiredFields.add(requiredField);
  }

  /** 获取错误信息. */
  public String getErrorMsg() {
    StringBuilder builder = new StringBuilder();
    for (ReaderError error : getErrors()) {
      builder.append(error.toString() + "<br/>");
    }
    return builder.toString();
  }

}
