package com.gitee.jmash.common.excel;

import com.gitee.jmash.common.excel.read.CellStringReader;
import com.gitee.jmash.common.excel.read.HeaderField;
import com.gitee.jmash.common.excel.read.ReaderError;
import com.monitorjbl.xlsx.StreamingReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

/**
 * Excel 大文件读取.
 *
 * @author CGD-WIN
 */
public class ExcelStreamImport {

  private static final Log log = LogFactory.getLog(ExcelStreamImport.class);

  Workbook wb = null;

  public void openExcel(InputStream is) throws IOException, InvalidFormatException {
    wb = StreamingReader.builder().rowCacheSize(200).bufferSize(4096).open(is);
  }

  public void openExcel(File file) throws IOException, InvalidFormatException {
    wb = StreamingReader.builder().rowCacheSize(200).bufferSize(4096).open(file);
  }

  /**
   * 表头和字段对应关系.
   */
  protected List<HeaderField> headerFields = new ArrayList<HeaderField>();
  /** 错误信息. */
  List<ReaderError> errors = new ArrayList<ReaderError>();

  protected int startHeader = 1;

  protected int headerCount = 1;
  // 匹配表头数
  protected int matchHeaderCount = 0;
  // 匹配的头部字段
  protected Map<Integer, HeaderField> fields = null;

  public Row filterRow(int rowIndex, Row row) {
    return row;
  }

  public Object filterCell(int rowIndex, int colIndex, Cell cell) {
    return null;
  }

  public void setHeaders(List<HeaderField> headerFields) {
    this.headerFields.clear();
    this.headerFields.addAll(headerFields);
  }

  public interface Callback<T> {
    void process(List<T> batchResult);
  }

  /**
   * Reader Entity.
   * 
   * @param batchSize 每次读取的量
   * @param callback 回调
   */
  public <T> int importEntity(int sheetIndex, T pojo, int batchSize, Callback<T> callback)
      throws NoSuchMethodException, IllegalAccessException, InvocationTargetException,
      InstantiationException {
    Sheet sheet = wb.getSheetAt(sheetIndex);

    // 读表头
    List<String> headers = readerHeader(sheet, startHeader, headerCount);
    Map<Integer, HeaderField> fields = matchHeaders(headers);

    int startData = startHeader + headerCount;
    int count = sheet.getLastRowNum() - startData + 1;
    // 读取数据
    PojoBuild<T> build = PojoBuild.create(pojo);

    for (int i = 0; i < count; i += batchSize) {
      int batch = Math.min(batchSize, count - i);
      List<T> datas = readerEntity(sheet, build, fields, 0, batch);
      callback.process(datas);
    }
    return count;
  }

  /**
   * Reader Entity.
   */
  public <T> List<T> readerEntity(Sheet sheet, PojoBuild<T> build, Map<Integer, HeaderField> fields,
      int start, int count) throws NoSuchMethodException, IllegalAccessException,
      InvocationTargetException, InstantiationException {
    List<T> datas = new ArrayList<T>();
    int i = -1;
    for (Row row : sheet) {
      i++;
      if (i < start) {
        continue;
      }
      row = filterRow(i, row);
      if (null == row) {
        continue;
      }
      Object entity = build.builder();
      // 从有数据列读取
      for (int j = row.getFirstCellNum(); j <= row.getLastCellNum(); j++) {
        HeaderField field = null;
        if (fields.containsKey(j)) {
          field = fields.get(j);
        }
        if (null == field) {
          field = HeaderField.field("column" + j, "column" + j, false);
        }
        Cell cell = row.getCell(j);
        if (null != cell) {
          setEntityFieldValue(build, cell, entity, field);
        }
      }
      datas.add(build.build(entity));
      if (i + 1 >= start + count) {
        break;
      }
    }
    return datas;
  }

  /**
   * 读表头.
   */
  public List<String> readerHeader(Sheet sheet, int startHeader, int headerCount) {
    List<String> header = new ArrayList<String>();
    CellStringReader reader = new CellStringReader(true);
    int i = -1;
    for (Row row : sheet) {
      i++;
      if (i < startHeader) {
        continue;
      }
      row = filterRow(i, row);
      if (null == row) {
        continue;
      }
      // 必须从0开始读
      for (int j = 0; j < row.getLastCellNum(); j++) {
        Object value = null;
        // 有数据开始读
        if (j >= row.getFirstCellNum()) {
          Cell cell = row.getCell(j);
          value = filterCell(i, j, cell);
          if (value == null) {
            value = reader.cellValueRead("header", cell, null, errors);
          }
        }
        if (value == null) {
          value = StringUtils.EMPTY;
        }
        // 表头序号
        if (header.size() > j) {
          // 表头自动加
          String v1 = header.get(j);
          String v2 = String.valueOf(value);
          if (!v1.contains(v2)) {
            header.set(j, v1 + v2);
          }
        } else {
          header.add(j, String.valueOf(value));
        }
      }
      if (i + 1 >= startHeader + headerCount) {
        break;
      }
    }
    return header;
  }

  /**
   * 匹配表头.
   */
  public Map<Integer, HeaderField> matchHeaders(List<String> headers) {
    // 表头对应关系
    fields = new HashMap<Integer, HeaderField>();
    int i = 0;
    matchHeaderCount = 0;
    List<HeaderField> allField = new ArrayList<HeaderField>();
    allField.addAll(this.headerFields);
    for (String header : headers) {

      boolean find = false;

      for (HeaderField field : allField) {
        if (field.getContain() && header.indexOf(field.getHeader()) > -1) {
          fields.put(i, field);
          allField.remove(field);
          matchHeaderCount++;
          find = true;
          break;
        }
        if (!field.getContain() && StringUtils.equals(field.getHeader().trim(), header)) {
          fields.put(i, field);
          allField.remove(field);
          matchHeaderCount++;
          find = true;
          break;
        }
      }
      if (!find) {
        fields.put(i, HeaderField.field(header, String.format("Column%d", i), false));
      }
      i++;
    }
    return fields;
  }

  private void setEntityFieldValue(PojoBuild<?> build, Cell cell, Object entity, HeaderField field)
      throws IllegalAccessException, InvocationTargetException {
    Object value = field.getReader().cellValueRead(field.getHeader(), cell, null, errors);
    if (value == null) {
      return;
    }
    // 多字段
    if (field.getFields().length > 0) {
      String[] values = ((String[]) value);
      for (int k = 0; k < field.getFields().length; k++) {
        if (values.length > k && StringUtils.isNotBlank(field.getFields()[k])) {
          build.setProperty(entity, field.getFields()[k], values[k]);
        }
      }
    } else {
      build.setProperty(entity, field.getField(), value);
    }
  }

  public int getStartHeader() {
    return this.startHeader;
  }

  public void setStartHeader(int startHeader) {
    this.startHeader = startHeader;
  }

  public int getHeaderCount() {
    return this.headerCount;
  }

  public void setHeaderCount(int headerCount) {
    this.headerCount = headerCount;
  }

  public Map<Integer, HeaderField> getFields() {
    return this.fields;
  }

  public List<ReaderError> getErrors() {
    return this.errors;
  }

  /** 获取错误信息. */
  public String getErrorMsg() {
    StringBuilder builder = new StringBuilder();
    for (ReaderError error : getErrors()) {
      builder.append(error.toString() + "<br/>");
    }
    return builder.toString();
  }

}
