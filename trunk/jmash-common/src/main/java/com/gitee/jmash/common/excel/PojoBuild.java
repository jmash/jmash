package com.gitee.jmash.common.excel;

import com.google.protobuf.MessageLite;

/**
 * POJO Build.
 *
 * @author cgd
 */
public interface PojoBuild<T> {

  /** 创建. */
  @SuppressWarnings({"unchecked", "rawtypes"})
  public static <T> PojoBuild<T> create(T obj) {
    if (obj instanceof MessageLite) {
      return new PojoMessageBuild((MessageLite) obj);
    }
    return new PojoEntityBuild(obj.getClass());
  }

  /** 创建实体构建. */
  public Object builder();

  /** 设置属性. */
  public Object setProperty(final Object obj, final String name, final Object value);

  /** 构建实体. */
  public T build(Object obj);

}
