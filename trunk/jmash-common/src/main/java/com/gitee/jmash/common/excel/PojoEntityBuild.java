
package com.gitee.jmash.common.excel;

import com.crenjoy.proto.beanutils.ProtoBeanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Entity POJO Build.
 *
 * @author cgd
 */
public class PojoEntityBuild<T> implements PojoBuild<T> {

  private static final Log log = LogFactory.getLog(PojoEntityBuild.class);

  Class<T> clazz;

  public PojoEntityBuild(Class<T> clazz) {
    super();
    this.clazz = clazz;
  }

  @Override
  public Object builder() {
    try {
      return clazz.getDeclaredConstructor().newInstance();
    } catch (Exception ex) {
      log.error("", ex);
      return null;
    }
  }

  @Override
  public Object setProperty(Object obj, String name, Object value) {
    try {
      ProtoBeanUtils.setProperty(obj, name, value);
      return obj;
    } catch (Exception ex) {
      log.error("", ex);
      return obj;
    }
  }

  @Override
  @SuppressWarnings("unchecked")
  public T build(Object obj) {
    return (T) obj;
  }

}
