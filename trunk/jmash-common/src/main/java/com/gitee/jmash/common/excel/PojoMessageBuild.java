
package com.gitee.jmash.common.excel;

import com.crenjoy.proto.beanutils.ProtoConvertUtils;
import com.gitee.jmash.common.text.StringUtil;
import com.google.protobuf.Descriptors;
import com.google.protobuf.Descriptors.FieldDescriptor.JavaType;
import com.google.protobuf.Message;
import com.google.protobuf.MessageLite;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Message POJO Build.
 *
 * @author cgd
 */
public class PojoMessageBuild<T extends MessageLite> implements PojoBuild<T> {

  private static final Log log = LogFactory.getLog(PojoMessageBuild.class);

  T message;

  public PojoMessageBuild(T message) {
    super();
    this.message = message;
  }

  @Override
  public Object builder() {
    try {
      return message.toBuilder();
    } catch (Exception ex) {
      log.error("", ex);
      return null;
    }
  }

  @Override
  public Object setProperty(Object obj, String name, Object value) {
    try {
      Message.Builder builder = ((Message.Builder) obj);
      Descriptors.Descriptor descriptor = builder.getDescriptorForType();
      Descriptors.FieldDescriptor field =
          descriptor.findFieldByName(StringUtil.camelToUnderline(name));
      if (null == field) {
        field = descriptor.findFieldByName(name);
      }
      if (null == field) {
        field = descriptor.findFieldByName(StringUtil.camelToUnderline(name) + "_");
      }
      if (null == field) {
        return obj;
      }

      if (JavaType.ENUM.equals(field.getJavaType())) {
        Object fieldValue =
            StringUtils.isBlank((String) value) ? field.getEnumType().findValueByNumber(0)
                : field.getEnumType().findValueByName((String) value);
        builder.setField(field, fieldValue);
      } else if (JavaType.INT.equals(field.getJavaType())) {
        setField(builder, field, value, Integer.class);
      } else if (JavaType.LONG.equals(field.getJavaType())) {
        setField(builder, field, value, Long.class);
      } else if (JavaType.FLOAT.equals(field.getJavaType())) {
        setField(builder, field, value, Float.class);
      } else if (JavaType.DOUBLE.equals(field.getJavaType())) {
        setField(builder, field, value, Double.class);
      } else if (JavaType.BOOLEAN.equals(field.getJavaType())) {
        setField(builder, field, value, Boolean.class);
      } else if (JavaType.STRING.equals(field.getJavaType())) {
        setField(builder, field, value, String.class);
      } else if (JavaType.MESSAGE.equals(field.getJavaType())) {
        // MESSAGE
        builder.setField(field, value);
      }
      return obj;
    } catch (Exception ex) {
      log.error("", ex);
      return obj;
    }
  }

  /** 设置字段值. */
  public void setField(Message.Builder builder, Descriptors.FieldDescriptor field, Object value,
      Class<?> targetType) {
    if (field.isRepeated()) {
      for (Object obj : (List<?>) value) {
        Object fieldValue = ProtoConvertUtils.convert(obj, targetType);
        builder.addRepeatedField(field, fieldValue);
      }
    } else {
      Object fieldValue = ProtoConvertUtils.convert(value, targetType);
      builder.setField(field, fieldValue);
    }
  }

  @Override
  @SuppressWarnings("unchecked")
  public T build(Object obj) {
    return (T) ((MessageLite.Builder) obj).build();
  }

}
