package com.gitee.jmash.common.excel.read;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.FormulaEvaluator;

/**
 * Execl BigDecimal 读取.
 *
 * @author cgd
 */
public class CellBigDecimalReader implements CellValueReader {

  /** BigDecimal Reader. */
  public CellBigDecimalReader(Integer newScale, RoundingMode roundingMode) {
    super();
    this.newScale = newScale;
    this.roundingMode = roundingMode;
  }

  /** 精度. */
  private Integer newScale;
  /** 精度取舍类型. */
  private RoundingMode roundingMode;

  @Override
  public Object cellValueRead(String header, Cell cell, FormulaEvaluator eval,
      List<ReaderError> errors) {
    return cellReadBigDecimal(header, cell, eval, errors);
  }

  /** 读取BigDecimal. */
  public BigDecimal cellReadBigDecimal(String header, Cell cell, FormulaEvaluator eval,
      List<ReaderError> errors) {
    if (CellType.BLANK.equals(cell.getCellType())) {
      return null;
    } else if (CellType.STRING.equals(cell.getCellType())) {
      return handleToBigDecimal(header, cell, cell.getStringCellValue(), errors);
    } else if (CellType.BOOLEAN.equals(cell.getCellType())) {
      return cell.getBooleanCellValue() ? BigDecimal.ONE : BigDecimal.ZERO;
    } else if (CellType.ERROR.equals(cell.getCellType())) {
      errors.add(ReaderError.create(cell, header, "Cell Error"));
      return null;
    } else if (CellType.NUMERIC.equals(cell.getCellType())) {
      return BigDecimal.valueOf(cell.getNumericCellValue()).setScale(newScale, roundingMode);
    } else if (CellType.FORMULA.equals(cell.getCellType())) {
      CellValue cellValue = eval.evaluate(cell);
      return cellReadBigDecimal(header, cell, cellValue, errors);
    }
    return null;
  }

  /** 读取BigDecimal. */
  public BigDecimal cellReadBigDecimal(String header, Cell formulaCell, CellValue cellValue,
      List<ReaderError> errors) {
    if (CellType.STRING.equals(cellValue.getCellType())) {
      return handleToBigDecimal(header, formulaCell, cellValue.getStringValue(), errors);
    } else if (CellType.BOOLEAN.equals(cellValue.getCellType())) {
      return cellValue.getBooleanValue() ? BigDecimal.ONE : BigDecimal.ZERO;
    } else if (CellType.ERROR.equals(cellValue.getCellType())) {
      errors.add(ReaderError.create(formulaCell, header, "Cell Error"));
      return null;
    } else if (CellType.NUMERIC.equals(cellValue.getCellType())) {
      return BigDecimal.valueOf(cellValue.getNumberValue()).setScale(newScale, roundingMode);
    }
    return null;
  }

  /** 空格/换行符等处理. */
  public BigDecimal handleToBigDecimal(String header, Cell cell, String temp,
      List<ReaderError> errors) {
    try {
      String parse = handleStr(temp);
      return new BigDecimal(parse).setScale(newScale, roundingMode);
    } catch (Exception ex) {
      String message = String.format("%s Convert To BigDecimal ", temp);
      errors.add(ReaderError.create(cell, header, message, ex));
      return null;
    }
  }

  public Integer getNewScale() {
    return newScale;
  }

  public void setNewScale(Integer newScale) {
    this.newScale = newScale;
  }

  public RoundingMode getRoundingMode() {
    return roundingMode;
  }

  public void setRoundingMode(RoundingMode roundingMode) {
    this.roundingMode = roundingMode;
  }

}
