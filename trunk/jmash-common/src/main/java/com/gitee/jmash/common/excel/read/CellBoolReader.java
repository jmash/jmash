package com.gitee.jmash.common.excel.read;

import com.crenjoy.proto.beanutils.ProtoConvertUtils;
import java.util.List;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.FormulaEvaluator;


/**
 * Execl Bool 读取.
 *
 * @author cgd
 */
public class CellBoolReader implements CellValueReader {

  public CellBoolReader() {
    super();
  }

  @Override
  public Object cellValueRead(String header, Cell cell, FormulaEvaluator eval,
      List<ReaderError> errors) {
    return cellReadBool(header, cell, eval, errors);
  }

  /** 读取Bool. */
  public Boolean cellReadBool(String header, Cell cell, FormulaEvaluator eval,
      List<ReaderError> errors) {
    if (CellType.BLANK.equals(cell.getCellType())) {
      return null;
    } else if (CellType.STRING.equals(cell.getCellType())) {
      return handleToBool(header, cell, cell.getStringCellValue(), errors);
    } else if (CellType.BOOLEAN.equals(cell.getCellType())) {
      return cell.getBooleanCellValue();
    } else if (CellType.ERROR.equals(cell.getCellType())) {
      errors.add(ReaderError.create(cell, header, "Cell Error"));
      return null;
    } else if (CellType.NUMERIC.equals(cell.getCellType())) {
      Double value = cell.getNumericCellValue();
      return value.intValue() == 0 ? false : true;
    } else if (CellType.FORMULA.equals(cell.getCellType())) {
      CellValue cellValue = eval.evaluate(cell);
      return cellReadBool(header, cell, cellValue, errors);
    }
    return null;
  }

  /** 读取Bool. */
  public Boolean cellReadBool(String header, Cell formulaCell, CellValue cellValue,
      List<ReaderError> errors) {
    if (CellType.STRING.equals(cellValue.getCellType())) {
      return handleToBool(header, formulaCell, cellValue.getStringValue(), errors);
    } else if (CellType.BOOLEAN.equals(cellValue.getCellType())) {
      return cellValue.getBooleanValue();
    } else if (CellType.ERROR.equals(cellValue.getCellType())) {
      errors.add(ReaderError.create(formulaCell, header, "Cell Error"));
      return null;
    } else if (CellType.NUMERIC.equals(cellValue.getCellType())) {
      Double value = cellValue.getNumberValue();
      return value.intValue() == 0 ? false : true;
    }
    return null;
  }

  /** 空格/换行符等处理. */
  public Boolean handleToBool(String header, Cell cell, String temp, List<ReaderError> errors) {
    try {
      String parse = handleStr(temp);
      return ProtoConvertUtils.convert(parse, Boolean.class);
    } catch (Exception ex) {
      String message = String.format("%s Convert To Bool  ", temp);
      errors.add(ReaderError.create(cell, header, message, ex));
      return null;
    }
  }
}
