package com.gitee.jmash.common.excel.read;

import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;

/**
 * 数据字典值读取.
 *
 * @author CGD
 */
public class CellDictReader extends CellStringReader {

  private Map<String, String> dictMap;

  public CellDictReader(Map<String, String> dictMap) {
    super(true);
    this.dictMap = dictMap;
  }

  @Override
  public Object cellValueRead(String header, Cell cell, FormulaEvaluator eval,
      List<ReaderError> errors) {
    String name = cellReadString(header, cell, errors);
    if (StringUtils.isBlank(name)) {
      return StringUtils.EMPTY;
    }
    if (dictMap.containsKey(name)) {
      return dictMap.get(name);
    } else {
      errors.add(ReaderError.create(cell, header, "Dict Error: " + name));
      return StringUtils.EMPTY;
    }
  }

}
