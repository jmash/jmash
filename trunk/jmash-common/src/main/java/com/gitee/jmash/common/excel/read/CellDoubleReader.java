package com.gitee.jmash.common.excel.read;

import java.util.List;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.FormulaEvaluator;

/**
 * Execl Double 读取.
 *
 * @author cgd
 */
public class CellDoubleReader implements CellValueReader {

  /** Double Reader. */
  public CellDoubleReader() {
    super();
  }

  @Override
  public Object cellValueRead(String header, Cell cell, FormulaEvaluator eval,
      List<ReaderError> errors) {
    return cellReadDouble(header, cell, eval, errors);
  }

  /** 读取Double. */
  public Double cellReadDouble(String header, Cell cell, FormulaEvaluator eval,
      List<ReaderError> errors) {
    if (CellType.BLANK.equals(cell.getCellType())) {
      return null;
    } else if (CellType.STRING.equals(cell.getCellType())) {
      return handleToDouble(header, cell, cell.getStringCellValue(), errors);
    } else if (CellType.BOOLEAN.equals(cell.getCellType())) {
      return cell.getBooleanCellValue() ? 1.0d : 0.0d;
    } else if (CellType.ERROR.equals(cell.getCellType())) {
      errors.add(ReaderError.create(cell, header, "Cell Error"));
      return null;
    } else if (CellType.NUMERIC.equals(cell.getCellType())) {
      return cell.getNumericCellValue();
    } else if (CellType.FORMULA.equals(cell.getCellType())) {
      CellValue cellValue = eval.evaluate(cell);
      return cellReadDouble(header, cell, cellValue, errors);
    }
    return null;
  }

  /** 读取Double. */
  public Double cellReadDouble(String header, Cell formulaCell, CellValue cellValue,
      List<ReaderError> errors) {
    if (CellType.STRING.equals(cellValue.getCellType())) {
      return handleToDouble(header, formulaCell, cellValue.getStringValue(), errors);
    } else if (CellType.BOOLEAN.equals(cellValue.getCellType())) {
      return cellValue.getBooleanValue() ? 1.0d : 0.0d;
    } else if (CellType.ERROR.equals(cellValue.getCellType())) {
      errors.add(ReaderError.create(formulaCell, header, "Cell Error"));
      return null;
    } else if (CellType.NUMERIC.equals(cellValue.getCellType())) {
      return cellValue.getNumberValue();
    }
    return null;
  }

  /** 空格/换行符等处理. */
  public Double handleToDouble(String header, Cell cell, String temp,
      List<ReaderError> errors) {
    try {
      String parse = handleStr(temp);
      return Double.valueOf(parse);
    } catch (Exception ex) {
      String message = String.format("%s Convert To Double ", temp);
      errors.add(ReaderError.create(cell, header, message, ex));
      return null;
    }
  }

}
