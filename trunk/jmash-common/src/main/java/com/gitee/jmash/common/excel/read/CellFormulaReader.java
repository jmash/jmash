package com.gitee.jmash.common.excel.read;

import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.FormulaEvaluator;

/**
 * Execl Formula 读取.
 *
 * @author cgd
 */
public class CellFormulaReader implements CellValueReader {

  public CellFormulaReader() {
    super();
  }

  @Override
  public Object cellValueRead(String header, Cell cell, FormulaEvaluator eval,
      List<ReaderError> errors) {
    return cellReadFormula(header, cell, errors);
  }

  /** 读取Formula. */
  public String cellReadFormula(String header, Cell cell, List<ReaderError> errors) {
    if (CellType.FORMULA.equals(cell.getCellType())) {
      return cell.getCellFormula();
    } else {
      errors.add(ReaderError.create(cell, header, "Cell Not Formula Error"));
      return StringUtils.EMPTY;
    }
  }

}
