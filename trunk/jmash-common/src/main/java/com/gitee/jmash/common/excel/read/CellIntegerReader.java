package com.gitee.jmash.common.excel.read;

import java.util.List;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.FormulaEvaluator;


/**
 * Execl Integer 读取.
 *
 * @author cgd
 */
public class CellIntegerReader implements CellValueReader {

  public CellIntegerReader() {
    super();
  }

  @Override
  public Object cellValueRead(String header, Cell cell, FormulaEvaluator eval,
      List<ReaderError> errors) {
    return cellReadInteger(header, cell, eval, errors);
  }

  /** 读取Integer. */
  public Integer cellReadInteger(String header, Cell cell, FormulaEvaluator eval,
      List<ReaderError> errors) {
    if (CellType.BLANK.equals(cell.getCellType())) {
      return null;
    } else if (CellType.STRING.equals(cell.getCellType())) {
      return handleToInt(header, cell, cell.getStringCellValue(), errors);
    } else if (CellType.BOOLEAN.equals(cell.getCellType())) {
      return cell.getBooleanCellValue() ? 1 : 0;
    } else if (CellType.ERROR.equals(cell.getCellType())) {
      errors.add(ReaderError.create(cell, header, "Cell Error"));
      return null;
    } else if (CellType.NUMERIC.equals(cell.getCellType())) {
      Double value = cell.getNumericCellValue();
      return value.intValue();
    } else if (CellType.FORMULA.equals(cell.getCellType())) {
      CellValue cellValue = eval.evaluate(cell);
      return cellReadInteger(header, cell, cellValue, errors);
    }
    return null;
  }

  /** 读取Integer. */
  public Integer cellReadInteger(String header, Cell formulaCell, CellValue cellValue,
      List<ReaderError> errors) {
    if (CellType.STRING.equals(cellValue.getCellType())) {
      return handleToInt(header, formulaCell, cellValue.getStringValue(), errors);
    } else if (CellType.BOOLEAN.equals(cellValue.getCellType())) {
      return cellValue.getBooleanValue() ? 1 : 0;
    } else if (CellType.ERROR.equals(cellValue.getCellType())) {
      errors.add(ReaderError.create(formulaCell, header, "Cell Error"));
      return null;
    } else if (CellType.NUMERIC.equals(cellValue.getCellType())) {
      Double value = cellValue.getNumberValue();
      return value.intValue();
    }
    return null;
  }

  /** 空格/换行符等处理. */
  public Integer handleToInt(String header, Cell cell, String temp, List<ReaderError> errors) {
    try {
      String parse = handleStr(temp);
      return Integer.valueOf(parse);
    } catch (Exception ex) {
      String message = String.format("%s Convert To Integer ", temp);
      errors.add(ReaderError.create(cell, header, message, ex));
      return null;
    }
  }



}
