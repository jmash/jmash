package com.gitee.jmash.common.excel.read;

import java.time.LocalDate;

/**
 * LocalDate.
 *
 * @author cgd
 */
public class CellLocalDateReader extends CellLocalDateTimeReader {
 
  @Override
  protected Class<?> getDefaultType() {
    return LocalDate.class;
  }
  
}
