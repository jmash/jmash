package com.gitee.jmash.common.excel.read;

import com.crenjoy.proto.beanutils.ProtoConvertUtils;
import java.time.LocalDateTime;
import java.util.List;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.FormulaEvaluator;

/**
 * Execl Date 读取.
 *
 * @author cgd
 */
public class CellLocalDateTimeReader implements CellValueReader {

  public CellLocalDateTimeReader() {
    super();
  }

  protected Class<?> getDefaultType() {
    return LocalDateTime.class;
  }

  @Override
  public Object cellValueRead(String header, Cell cell, FormulaEvaluator eval,
      List<ReaderError> errors) {
    LocalDateTime datetime = cellReadDate(header, cell, errors);
    return ProtoConvertUtils.convert(datetime, getDefaultType());
  }

  /** 读取Date. */
  public LocalDateTime cellReadDate(String header, Cell cell, List<ReaderError> errors) {
    if (CellType.BLANK.equals(cell.getCellType())) {
      return null;
    } else if (CellType.STRING.equals(cell.getCellType())) {
      return handleToDate(header, cell, cell.getStringCellValue(), errors);
    } else if (CellType.BOOLEAN.equals(cell.getCellType())) {
      errors.add(ReaderError.create(cell, header, "Cell Bool Not Date Error"));
      return null;
    } else if (CellType.ERROR.equals(cell.getCellType())) {
      errors.add(ReaderError.create(cell, header, "Cell Error"));
      return null;
    } else if (CellType.NUMERIC.equals(cell.getCellType())) {
      try {
        return cell.getLocalDateTimeCellValue();
      } catch (Exception ex) {
        String message = String.format("%f Convert To LocalDateTime ", cell.getNumericCellValue());
        errors.add(ReaderError.create(cell, header, message, ex));
        return null;
      }
    } else if (CellType.FORMULA.equals(cell.getCellType())) {
      errors.add(ReaderError.create(cell, header, "Cell formula Not Date Error"));
      return null;
    }
    return null;
  }

  /** 空格/换行符等转换处理. */
  public LocalDateTime handleToDate(String header, Cell cell, String temp,
      List<ReaderError> errors) {
    try {
      String parse = handleStr(temp);
      return ProtoConvertUtils.convert(parse, LocalDateTime.class);
    } catch (Exception ex) {
      String message = String.format("%s Convert To LocalDateTime ", temp);
      errors.add(ReaderError.create(cell, header, message, ex));
      return null;
    }
  }

}
