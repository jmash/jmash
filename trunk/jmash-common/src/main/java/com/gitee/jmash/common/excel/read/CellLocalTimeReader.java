package com.gitee.jmash.common.excel.read;

import java.time.LocalTime;

/**
 * LocalTime.
 *
 * @author cgd
 */
public class CellLocalTimeReader extends CellLocalDateTimeReader {
 
  @Override
  protected Class<?> getDefaultType() {
    return LocalTime.class;
  }
  
}
