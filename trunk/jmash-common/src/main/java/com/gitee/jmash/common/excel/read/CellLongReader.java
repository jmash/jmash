package com.gitee.jmash.common.excel.read;

import java.util.List;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.FormulaEvaluator;


/**
 * Execl Long 读取.
 *
 * @author cgd
 */
public class CellLongReader implements CellValueReader {

  public CellLongReader() {
    super();
  }

  @Override
  public Object cellValueRead(String header, Cell cell, FormulaEvaluator eval,
      List<ReaderError> errors) {
    return cellReadLong(header, cell, eval, errors);
  }

  /** 读取Long. */
  public Long cellReadLong(String header, Cell cell, FormulaEvaluator eval,
      List<ReaderError> errors) {
    if (CellType.BLANK.equals(cell.getCellType())) {
      return null;
    } else if (CellType.STRING.equals(cell.getCellType())) {
      return handleToLong(header, cell, cell.getStringCellValue(), errors);
    } else if (CellType.BOOLEAN.equals(cell.getCellType())) {
      return cell.getBooleanCellValue() ? 1L : 0L;
    } else if (CellType.ERROR.equals(cell.getCellType())) {
      errors.add(ReaderError.create(cell, header, "Cell Error"));
      return null;
    } else if (CellType.NUMERIC.equals(cell.getCellType())) {
      Double value = cell.getNumericCellValue();
      return value.longValue();
    } else if (CellType.FORMULA.equals(cell.getCellType())) {
      CellValue cellValue = eval.evaluate(cell);
      return cellReadLong(header, cell, cellValue, errors);
    }
    return null;
  }

  /** 读取Long. */
  public Long cellReadLong(String header, Cell formulaCell, CellValue cellValue,
      List<ReaderError> errors) {
    if (CellType.STRING.equals(cellValue.getCellType())) {
      return handleToLong(header, formulaCell, cellValue.getStringValue(), errors);
    } else if (CellType.BOOLEAN.equals(cellValue.getCellType())) {
      return cellValue.getBooleanValue() ? 1L : 0L;
    } else if (CellType.ERROR.equals(cellValue.getCellType())) {
      errors.add(ReaderError.create(formulaCell, header, "Cell Error"));
      return null;
    } else if (CellType.NUMERIC.equals(cellValue.getCellType())) {
      Double value = cellValue.getNumberValue();
      return value.longValue();
    }
    return null;
  }

  /** 空格/换行符等处理. */
  public Long handleToLong(String header, Cell cell, String temp, List<ReaderError> errors) {
    try {
      String parse = handleStr(temp);
      return Long.valueOf(parse);
    } catch (Exception ex) {
      String message = String.format("%s Convert To Long ", temp);
      errors.add(ReaderError.create(cell, header, message, ex));
      return null;
    }
  }



}
