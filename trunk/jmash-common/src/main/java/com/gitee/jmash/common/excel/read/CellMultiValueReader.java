package com.gitee.jmash.common.excel.read;

import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFDataFormatter;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.FormulaEvaluator;

/**
 * Execl String 读取.
 *
 * @author cgd
 */
public class CellMultiValueReader implements CellValueReader {

  // 分隔符
  private String separatorChars;
  // 长度.
  private int len;

  /** 分隔,长度. */
  public CellMultiValueReader(String separatorChars, int len) {
    super();
    this.separatorChars = separatorChars;
    this.len = len;
  }

  @Override
  public Object cellValueRead(String header, Cell cell, FormulaEvaluator eval,
      List<ReaderError> errors) {
    return cellReadMultiValue(header, cell, errors);
  }

  /** 读取[]. */
  public String[] cellReadMultiValue(String header, Cell cell, List<ReaderError> errors) {
    if (CellType.BLANK.equals(cell.getCellType())) {
      return new String[len];
    } else if (CellType.STRING.equals(cell.getCellType())) {
      return handleToMultiValue(cell.getStringCellValue());
    } else if (CellType.BOOLEAN.equals(cell.getCellType())) {
      errors.add(ReaderError.create(cell, header, "Cell MultiValue is Bool Error"));
      return new String[len];
    } else if (CellType.ERROR.equals(cell.getCellType())) {
      errors.add(ReaderError.create(cell, header, "Cell Error"));
      return new String[len];
    } else {
      // NUMERIC/FORMULA
      HSSFDataFormatter f1 = new HSSFDataFormatter();
      return handleToMultiValue(f1.formatCellValue(cell));
    }
  }

  /** 空格/换行符等处理. */
  public String[] handleToMultiValue(String temp) {
    String r = handleStr(temp);
    return StringUtils.split(r, separatorChars);
  }



}
