package com.gitee.jmash.common.excel.read;

import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.FormulaEvaluator;

/**
 * Execl String 读取.
 *
 * @author cgd
 */
public class CellStringReader implements CellValueReader {

  /** 字符串去掉空格换行. */
  private boolean strHandle = true;

  public CellStringReader(boolean strHandle) {
    super();
    this.strHandle = strHandle;
  }

  @Override
  public Object cellValueRead(String header, Cell cell, FormulaEvaluator eval,
      List<ReaderError> errors) {
    return cellReadString(header, cell, errors);
  }

  /** 读取String. */
  public String cellReadString(String header, Cell cell, List<ReaderError> errors) {
    if (CellType.BLANK.equals(cell.getCellType())) {
      return StringUtils.EMPTY;
    } else if (CellType.STRING.equals(cell.getCellType())) {
      return handleToStr(cell.getStringCellValue());
    } else if (CellType.BOOLEAN.equals(cell.getCellType())) {
      return String.valueOf(cell.getBooleanCellValue());
    } else if (CellType.ERROR.equals(cell.getCellType())) {
      errors.add(ReaderError.create(cell, header, "Cell Error"));
      return StringUtils.EMPTY;
    } else {
      try {
        // NUMERIC/FORMULA
        DataFormatter f1 = new DataFormatter();
        return handleToStr(f1.formatCellValue(cell));
      } catch (Exception ex) {
        return handleToStr(cell.getStringCellValue());
      }
    }
  }

  /** 空格/换行符等处理. */
  public String handleToStr(String temp) {
    if (!strHandle) {
      return temp;
    }
    return handleStr(temp);
  }


  public boolean isStrHandle() {
    return strHandle;
  }

  public void setStrHandle(boolean strHandle) {
    this.strHandle = strHandle;
  }


}
