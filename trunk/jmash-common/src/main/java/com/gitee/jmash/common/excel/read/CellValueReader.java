package com.gitee.jmash.common.excel.read;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;

/**
 * Cell Value Reader.
 *
 * @author cgd
 */
public interface CellValueReader {

  static final Pattern PATTERN = Pattern.compile("\\s*");

  /** 读取Cell值. */
  public Object cellValueRead(String  header, Cell cell, FormulaEvaluator eval,
      List<ReaderError> errors);

  /** 空格/换行符等处理. */
  default String handleStr(String temp) {
    Matcher m = PATTERN.matcher(temp.trim());
    return m.replaceAll("");
  }
  
}
