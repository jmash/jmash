
package com.gitee.jmash.common.excel.read;

import java.util.Map;

/**
 * Header Field.
 *
 * @author CGD
 *
 */
public class HeaderField {

  /** 表头. */
  private String header;
  /** 字段. */
  private String field;
  /** 多字段. */
  private String[] fields = new String[0];
  /** 是否包含Header. */
  private boolean contain = false;
  /** 值读取. */
  private CellValueReader reader;

  public static HeaderField field(String header, String field, boolean contain) {
    return new HeaderField(header, field, contain, new CellStringReader(true));
  }

  public static HeaderField field(String header, String field, boolean contain,
      CellValueReader reader) {
    return new HeaderField(header, field, contain, reader);
  }

  public static HeaderField dict(String header, String field, boolean contain,
      Map<String, String> dictMap) {
    return new HeaderField(header, field, contain, new CellDictReader(dictMap));
  }

  /** 多字段. */
  public static HeaderField multiField(String header, String[] fields, boolean contain) {
    return new HeaderField(header, fields, contain, new CellMultiValueReader(":", 2));
  }

  public HeaderField() {
    super();
  }

  /**
   * Header Field.
   *
   * @param header Header Name.
   * @param field Field Name.
   * @param contain 包含/等于
   */
  public HeaderField(String header, String field, boolean contain, CellValueReader reader) {
    super();
    this.header = header;
    this.field = field;
    this.contain = contain;
    this.reader = reader;
  }

  /**
   * Header Field.
   *
   * @param header Header Name.
   * @param fields 多字段.
   * @param contain 包含/等于
   */
  public HeaderField(String header, String[] fields, boolean contain, CellValueReader reader) {
    super();
    this.header = header;
    this.fields = fields;
    this.contain = contain;
    this.reader = reader;
  }

  public String getHeader() {
    return header;
  }

  public void setHeader(String header) {
    this.header = header;
  }

  public String getField() {
    return field;
  }

  public void setField(String field) {
    this.field = field;
  }

  public boolean getContain() {
    return contain;
  }

  public void setContain(boolean contain) {
    this.contain = contain;
  }

  public String[] getFields() {
    return fields;
  }

  public void setFields(String[] fields) {
    this.fields = fields;
  }

  public CellValueReader getReader() {
    return reader;
  }

  public void setReader(CellValueReader reader) {
    this.reader = reader;
  }


}
