package com.gitee.jmash.common.excel.read;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Cell;

/** 错误. */
public class ReaderError {

  private static Log log = LogFactory.getLog(ReaderError.class);

  String header;

  int rowIndex;

  int columnIndex;

  String message;

  Exception exception;

  /** 异常构造. */
  public static ReaderError create(Cell cell, String header, String message, Exception exception) {
    ReaderError error =
        new ReaderError(header, cell.getRowIndex(), cell.getColumnIndex(), message, exception);
    log.warn(error, exception);
    return error;
  }

  /** 消息构造. */
  public static ReaderError create(Cell cell, String header, String message) {
    ReaderError error =
        new ReaderError(header, cell.getRowIndex(), cell.getColumnIndex(), message, null);
    log.warn(error);
    return error;
  }

  /**
   * 错误信息构造.
   *
   * @param header 表头
   * @param rowIndex 行索引
   * @param columnIndex 列索引
   * @param message 信息
   * @param exception 异常
   */
  public ReaderError(String header, int rowIndex, int columnIndex, String message,
      Exception exception) {
    super();
    this.header = header;
    this.rowIndex = rowIndex;
    this.columnIndex = columnIndex;
    this.message = message;
    this.exception = exception;
  }

  @Override
  public String toString() {
    return String.format("%s 行:%d,列:%d,Msg:%s", this.header, rowIndex + 1, columnIndex + 1,
        message);
  }



}
