package com.gitee.jmash.common.excel.write;

import com.crenjoy.proto.beanutils.ProtoBeanUtilsBean;
import org.apache.commons.beanutils.PropertyUtilsBean;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Cell;

/**
 * 默认字段中获取数据设置CellValue.
 *
 * @author cgd
 */
public class CellDefaultFormat implements CellValueFormat {

  private static final Log log = LogFactory.getLog(CellDefaultFormat.class);

  private static PropertyUtilsBean propertyUtilsBean =
      ProtoBeanUtilsBean.getInstance().getPropertyUtils();

  @Override
  public boolean setCellValue(Cell cell, HeaderExport header, int rowNum, Object rowData) {
    if (StringUtils.isNotBlank(header.getField())) {
      try {
        Object value = propertyUtilsBean.getProperty(rowData, header.getField());
        setCellObject(cell, value, header);
        return true;
      } catch (Exception ex) {
        log.error("", ex);
      }
    }
    return false;
  }

}
