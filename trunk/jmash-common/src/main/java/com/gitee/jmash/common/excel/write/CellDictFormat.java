package com.gitee.jmash.common.excel.write;

import com.crenjoy.proto.beanutils.ProtoBeanUtils;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Cell;

/**
 * 默认字段中获取数据设置CellValue.
 *
 * @author cgd
 */
public class CellDictFormat implements CellValueFormat {

  private static final Log log = LogFactory.getLog(CellDictFormat.class);

  private Map<String, ?> dictMap;

  /** 字典Map,字段名称. */
  public CellDictFormat(Map<String, ?> dictMap) {
    super();
    this.dictMap = dictMap;
  }

  @Override
  public boolean setCellValue(Cell cell, HeaderExport header, int rowNum, Object rowData) {
    try {
      String dictValue = ProtoBeanUtils.getSimpleProperty(rowData, header.getField());
      if (dictMap.containsKey(dictValue)) {
        Object value = dictMap.get(dictValue);
        // 根据数据类型处理.
        setCellObject(cell, value, header);
        return true;
      }
    } catch (Exception ex) {
      log.error("", ex);

    }
    return false;
  }

  public Map<String, ?> getDictMap() {
    return dictMap;
  }



}
