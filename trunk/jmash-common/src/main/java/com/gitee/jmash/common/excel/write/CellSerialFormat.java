package com.gitee.jmash.common.excel.write;

import org.apache.poi.ss.usermodel.Cell;

/**
 * 序号列处理.
 *
 * @author cgd
 */
public class CellSerialFormat implements CellValueFormat {

  @Override
  public boolean setCellValue(Cell cell, HeaderExport header, int rowNum, Object rowData) {
    cell.setCellValue(rowNum);
    return true;
  }

}
