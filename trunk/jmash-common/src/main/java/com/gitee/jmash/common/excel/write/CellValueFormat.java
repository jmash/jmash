package com.gitee.jmash.common.excel.write;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

/**
 * 字段格式化.
 *
 * @author cgd
 */
public interface CellValueFormat {

  /**
   * 设置单元格数据,返回是否已设置.
   *
   * @param header 导出列Header
   * @param rowNum 当前行数
   * @param rowData 行数据
   * @param cell 当前单元格
   */
  public boolean setCellValue(Cell cell, HeaderExport header, int rowNum, Object rowData);

  /** Cell Set Object. */
  default void setCellObject(Cell cell, Object obj, HeaderExport header) {
    if (null == obj) {
      cell.setBlank();
    } else if (obj instanceof String) {
      cell.setCellValue((String) obj);
      if (null == header.getCellStyle()) {
        setCellStyleFormat(cell, header, "@");
      }
    } else if (obj instanceof Boolean) {
      cell.setCellValue((Boolean) obj);
    } else if (obj instanceof Date) {
      cell.setCellValue((Date) obj);
      if (null == header.getCellStyle()) {
        setCellStyleFormat(cell, header, "yyyy-m-d");
      }
    } else if (obj instanceof LocalDateTime) {
      cell.setCellValue((LocalDateTime) obj);
      if (null == header.getCellStyle()) {
        setCellStyleFormat(cell, header, "yyyy-m-d h:mm");
      }
    } else if (obj instanceof LocalDate) {
      cell.setCellValue((LocalDate) obj);
      if (null == header.getCellStyle()) {
        setCellStyleFormat(cell, header, "yyyy-m-d");
      }
    } else if (obj instanceof Calendar) {
      cell.setCellValue((Calendar) obj);
      if (null == header.getCellStyle()) {
        setCellStyleFormat(cell, header, "yyyy-m-d h:mm");
      }
    } else if (obj instanceof RichTextString) {
      cell.setCellValue((RichTextString) obj);
    } else if (obj instanceof Double) {
      cell.setCellValue((Double) obj);
    } else if (obj instanceof Integer) {
      cell.setCellValue((Integer) obj);
    } else if (obj instanceof Long) {
      cell.setCellValue((Long) obj);
    } else if (obj instanceof Float) {
      cell.setCellValue((Float) obj);
    } else {
      cell.setCellValue(obj.toString());
      if (null == header.getCellStyle()) {
        setCellStyleFormat(cell, header, "@");
      }
    }
  }

  /** 设置单元格格式. */
  default void setCellStyleFormat(Cell cell, HeaderExport header, String format) {
    Sheet sheet = cell.getSheet();
    Workbook wb = sheet.getWorkbook();
    CellStyle cellStyle = wb.createCellStyle();
    DataFormat dataFormat = wb.createDataFormat();
    cellStyle.setDataFormat(dataFormat.getFormat(format));
    sheet.setDefaultColumnStyle(cell.getColumnIndex(), cellStyle);
    header.setCellStyle(cellStyle);
  }

}
