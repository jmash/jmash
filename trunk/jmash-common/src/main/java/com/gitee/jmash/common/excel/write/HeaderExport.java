
package com.gitee.jmash.common.excel.write;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.apache.poi.ss.usermodel.CellStyle;

/**
 * Excel Export Header 定义.
 *
 * @author CGD
 *
 */
public class HeaderExport {

  /** 表头. */
  private String header;
  /** 对应字段. */
  private String field;
  /** 占用宽度. */
  private int width;
  /** 占用行数. */
  private int row = 1;
  /** 占用列数. */
  private int column = 1;
  /** 字段处理. */
  private CellValueFormat cellValueFormat;
  /** 单元格格式 参考(org.apache.poi.ss.usermodel.BuiltinFormats). */
  private String format;
  /** 列风格. */
  private CellStyle cellStyle;



  /** 占用总列数. */
  public static int totalColumn(List<HeaderExport> headers) {
    return headers.stream().mapToInt(HeaderExport::getColumn).sum();
  }

  /** Map HeaderExport. */
  public static Map<String, HeaderExport> getHeaderExportMap(List<HeaderExport> headers) {
    Map<String, HeaderExport> map =
        headers.stream().collect(Collectors.toMap(HeaderExport::getField, Function.identity()));
    return map;
  }

  /** 序号. */
  public static HeaderExport serial(String header) {
    return new HeaderExport(header, "NO", 2 * 1000, 1, 1, new CellSerialFormat(), null);
  }

  /** 字段. */
  public static HeaderExport field(String header, String field, int width) {
    return new HeaderExport(header, field, width, 1, 1, new CellDefaultFormat(), null);
  }

  /** 字段. */
  public static HeaderExport field(String header, String field, int width, String format) {
    return new HeaderExport(header, field, width, 1, 1, new CellDefaultFormat(), format);
  }

  /** 字典. */
  public static HeaderExport dict(String header, String field, int width, Map<String, ?> dictMap) {
    return new HeaderExport(header, field, width, 1, 1, new CellDictFormat(dictMap), null);
  }

  /** 自定义. */
  public static HeaderExport custom(String header, String field, int width,
      CellValueFormat cellValueFormat) {
    return new HeaderExport(header, field, width, 1, 1, cellValueFormat, null);
  }


  /**
   * 创建列.
   *
   * @param header 列名称
   * @param field 对应字段
   * @param width 列占用宽度
   * @param cellValueFormat 单元格格式处理
   * @param row 占用行数
   * @param column 占用列数
   */
  public HeaderExport(String header, String field, int width, int row, int column,
      CellValueFormat cellValueFormat, String format) {
    super();
    this.header = header;
    this.field = field;
    this.width = width;
    this.cellValueFormat = cellValueFormat;
    this.row = row;
    this.column = column;
    this.format = format;
  }

  public String getHeader() {
    return header;
  }

  public void setHeader(String header) {
    this.header = header;
  }

  public String getField() {
    return field;
  }

  public void setField(String field) {
    this.field = field;
  }

  public int getWidth() {
    return width;
  }

  public void setWidth(int width) {
    this.width = width;
  }

  public CellValueFormat getCellValueFormat() {
    return cellValueFormat;
  }

  public void setCellValueFormat(CellValueFormat cellValueFormat) {
    this.cellValueFormat = cellValueFormat;
  }

  public int getRow() {
    return row;
  }

  public void setRow(int row) {
    this.row = row;
  }

  public int getColumn() {
    return column;
  }

  public void setColumn(int column) {
    this.column = column;
  }

  public String getFormat() {
    return format;
  }

  public CellStyle getCellStyle() {
    return cellStyle;
  }

  public void setCellStyle(CellStyle cellStyle) {
    this.cellStyle = cellStyle;
  }

  @Override
  public int hashCode() {
    return Objects.hash(header);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    } else if (obj == null) {
      return false;
    } else if (getClass() != obj.getClass()) {
      return false;
    }
    HeaderExport other = (HeaderExport) obj;
    return Objects.equals(header, other.header);
  }

}
