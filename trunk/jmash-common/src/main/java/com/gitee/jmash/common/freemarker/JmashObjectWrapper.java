package com.gitee.jmash.common.freemarker;

import freemarker.template.DefaultObjectWrapper;
import freemarker.template.SimpleDate;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.Version;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;


/**
 * Jmash FreeMarker Object Wrapper.
 *
 * @author cgd
 */
public class JmashObjectWrapper extends DefaultObjectWrapper {

  public JmashObjectWrapper(Version incompatibleImprovements) {
    super(incompatibleImprovements);
  }

  @Override
  public TemplateModel wrap(Object object) throws TemplateModelException {
    if (object instanceof LocalDateTime localDateTime) {
      return new SimpleDate(Timestamp.valueOf(localDateTime));
    } else if (object instanceof LocalDate localDate) {
      return new SimpleDate(Date.valueOf(localDate));
    } else if (object instanceof LocalTime localTime) {
      return new SimpleDate(Time.valueOf(localTime));
    }
    return super.wrap(object);
  }
}
