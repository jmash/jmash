
package com.gitee.jmash.common.grpc;

import io.grpc.Metadata;

/**
 * 网关调用GRPC 传递Metadata .
 *
 * @author CGD
 *
 */
public class GateWayMetadata {
  
  // 用户请求HOST.
  public static final Metadata.Key<String> USER_HOST_KEY = Metadata.Key.of("x-forwarded-host",
      Metadata.ASCII_STRING_MARSHALLER);

  // 用户网关接受语言 zh-CN,zh;q=0.9,
  public static final Metadata.Key<String> ACCEPT_LANGUAGE_KEY = Metadata.Key
      .of("grpcgateway-accept-language", Metadata.ASCII_STRING_MARSHALLER);

  // 缓存 max-age=0,
  public static final Metadata.Key<String> CACHE_CONTROL_KEY = Metadata.Key
      .of("grpcgateway-cache-control", Metadata.ASCII_STRING_MARSHALLER);

  // 用户浏览器
  public static final Metadata.Key<String> BROWSER_USER_AGENT_KEY = Metadata.Key
      .of("grpcgateway-user-agent", Metadata.ASCII_STRING_MARSHALLER);
  
  // Http Referer.
  public static final Metadata.Key<String> REFERER_KEY = Metadata.Key.of("grpcgateway-referer",
      Metadata.ASCII_STRING_MARSHALLER);
  
  // Http cookie.
  public static final Metadata.Key<String> COOKIE_KEY = Metadata.Key.of("grpcgateway-cookie",
      Metadata.ASCII_STRING_MARSHALLER);

}
