
package com.gitee.jmash.common.grpc;


import com.gitee.jmash.common.security.DefaultJmashPrincipal;
import com.gitee.jmash.common.security.JmashPrincipal;
import io.grpc.Context;
import java.util.Locale;
import org.eclipse.microprofile.jwt.JsonWebToken;

/**
 * GRPC Context.
 *
 * @author CGD
 *
 */
public class GrpcContext {

  // 登录用户Subject.
  public static final Context.Key<?> USER_SUBJECT = Context.key("UserSubject");
  // 登录用户Token
  public static final Context.Key<JsonWebToken> USER_TOKEN = Context.key("UserToken");
  // 用户认证信息Token字符(不包含 Bearer 的 Authorization).
  public static final Context.Key<String> USER_AUTH = Context.key("UserAuth");
  // HTTP Header Authorization For Other (其他认证体系使用)
  public static final Context.Key<String> OTHER_AUTH = Context.key("OtherAuth");
  // 用户请求HOST
  public static final Context.Key<String> GRPC_HOST = Context.key("UserHost");
  // 用户请求IP
  public static final Context.Key<String> USER_IP = Context.key("UserIP");
  // 用户请求Proxy
  public static final Context.Key<String> USER_PROXY_IP = Context.key("UserProxy");
  // 用户浏览器
  public static final Context.Key<String> BROWSER_USER_AGENT = Context.key("BrowserUserAgent");
  // 客户端
  public static final Context.Key<String> USER_AGENT = Context.key("UserAgent");
  // 用户请求Locale
  public static final Context.Key<Locale> GRPC_LOCALE = Context.key("UserLocale");
  // 请求Tenant(随Header传递)
  public static final Context.Key<String> TENANT = Context.key("Tenant");
  // 请求Referer
  public static final Context.Key<String> REFERER = Context.key("HttpReferer");
  // 请求cookie
  public static final Context.Key<String> COOKIE = Context.key("HttpCookie");

  // 获取当前用户凭证
  public static JmashPrincipal getPrincipal() {
    JsonWebToken webToken = GrpcContext.USER_TOKEN.get();
    return DefaultJmashPrincipal.create(webToken);
  }
}
