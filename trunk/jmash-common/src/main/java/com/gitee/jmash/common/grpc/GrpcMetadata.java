
package com.gitee.jmash.common.grpc;

import io.grpc.Metadata;

/**
 * Grpc Request Metadata,Grpc 服务间调用传递.
 *
 * @author CGD
 *
 */
public class GrpcMetadata {

  /** Auth Type Bearer. */
  public  static final String BEARER = "Bearer";
  
  /** Auth Type Basic. */
  public  static final String BASIC = "Basic";
  
  // Bearer Authorization.
  public static final Metadata.Key<String> AUTH_KEY =
      Metadata.Key.of("Authorization", Metadata.ASCII_STRING_MARSHALLER);
  
  // tenant.
  public static final Metadata.Key<String> TENANT_KEY =
      Metadata.Key.of("tenant", Metadata.ASCII_STRING_MARSHALLER);

  // 客户端(默认传递)
  public static final Metadata.Key<String> USER_AGENT_KEY =
      Metadata.Key.of("user-agent", Metadata.ASCII_STRING_MARSHALLER);

  // 用户请求,必须设置为'x-host', host ingress-nginx会解析错误.
  public static final Metadata.Key<String> GRPC_HOST_KEY =
      Metadata.Key.of("x-host", Metadata.ASCII_STRING_MARSHALLER);

  // 用户请求IP
  public static final Metadata.Key<String> USER_IP_KEY =
      Metadata.Key.of("x-real-ip", Metadata.ASCII_STRING_MARSHALLER);

  // 用户请求Proxy
  public static final Metadata.Key<String> USER_PROXY_IP_KEY =
      Metadata.Key.of("x-forwarded-for", Metadata.ASCII_STRING_MARSHALLER);

  // 用户浏览器
  public static final Metadata.Key<String> BROWSER_USER_AGENT_KEY =
      Metadata.Key.of("x-user-agent", Metadata.ASCII_STRING_MARSHALLER);

  // 用户Locale
  public static final Metadata.Key<String> GRPC_LOCALE_KEY =
      Metadata.Key.of("x-locale", Metadata.ASCII_STRING_MARSHALLER);
  
  // HTTP Referer
  public static final Metadata.Key<String> HTTP_REFERER_KEY =
      Metadata.Key.of("referer", Metadata.ASCII_STRING_MARSHALLER);
  
}
