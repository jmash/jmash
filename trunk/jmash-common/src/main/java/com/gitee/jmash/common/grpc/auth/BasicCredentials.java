package com.gitee.jmash.common.grpc.auth;

import com.gitee.jmash.common.grpc.GrpcMetadata;
import io.grpc.CallCredentials;
import io.grpc.Metadata;
import io.grpc.Status;
import java.util.Base64;
import java.util.concurrent.Executor;

/** Basic Token Credentials . */
public class BasicCredentials extends CallCredentials {

  private String clientId;

  private String clientSecret;

  public BasicCredentials(String clientId, String clientSecret) {
    this.clientId = clientId;
    this.clientSecret = clientSecret;
  }

  @Override
  public void applyRequestMetadata(final RequestInfo requestInfo, final Executor executor,
      final MetadataApplier metadataApplier) {
    executor.execute(new Runnable() {
      @Override
      public void run() {
        try {
          Metadata headers = new Metadata();
          String auth = String.format("%s:%s", clientId, clientSecret);
          // Base64编码
          String encodeAuth = Base64.getEncoder().encodeToString(auth.getBytes());
          headers.put(GrpcMetadata.AUTH_KEY,
              String.format("%s %s", GrpcMetadata.BASIC, encodeAuth));
          metadataApplier.apply(headers);
        } catch (Throwable e) {
          metadataApplier.fail(Status.UNAUTHENTICATED.withCause(e));
        }
      }
    });
  }

  @Override
  public void thisUsesUnstableApi() {}
}
