package com.gitee.jmash.common.grpc.auth;

import com.gitee.jmash.common.grpc.GrpcMetadata;
import io.grpc.CallCredentials;
import io.grpc.Metadata;
import io.grpc.Status;
import java.util.concurrent.Executor;
import org.apache.commons.lang3.StringUtils;

/** OAuth2 Token Credentials . */
public class OAuth2Credentials extends CallCredentials {

  private String accessToken;

  private String token;

  public OAuth2Credentials(String accessToken) {
    check(accessToken);
    this.accessToken = accessToken;
  }

  public OAuth2Credentials(String accessToken, String token) {
    check(accessToken);
    this.accessToken = accessToken;
    this.token = token;
  }

  public void check(String accessToken) {
    if (StringUtils.isBlank(accessToken)) {
      return;
    }
    if (accessToken.startsWith(GrpcMetadata.BEARER)) {
      throw new RuntimeException("AccessToken 不应该包含 Bearer 前缀. ");
    }
  }

  @Override
  public void applyRequestMetadata(final RequestInfo requestInfo, final Executor executor,
      final MetadataApplier metadataApplier) {
    executor.execute(new Runnable() {
      @Override
      public void run() {
        try {
          Metadata headers = new Metadata();
          if (StringUtils.isNotBlank(accessToken)) {
            headers.put(GrpcMetadata.AUTH_KEY,
                String.format("%s %s", GrpcMetadata.BEARER, accessToken));
          }
          if (StringUtils.isNotBlank(token)) {
            headers.put(GrpcMetadata.TENANT_KEY, token);
          }
          metadataApplier.apply(headers);
        } catch (Throwable e) {
          metadataApplier.fail(Status.UNAUTHENTICATED.withCause(e));
        }
      }
    });
  }

  @Override
  public void thisUsesUnstableApi() {}
}
