
package com.gitee.jmash.common.grpc.client;

import com.ecwid.consul.v1.health.model.HealthService;
import com.gitee.jmash.common.config.AppProps;
import com.gitee.jmash.common.config.ClientProps;
import com.gitee.jmash.common.registry.consul.ConsulDiscoverClient;
import com.gitee.jmash.common.tracing.TracingFactory;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.inprocess.InProcessChannelBuilder;

/**
 * Consul Grpc Channel.
 *
 * @author CGD
 *
 */
public class ConsulGrpcChannel {

  /**
   * 客户端服务管道.
   *
   * @param client      注册客户端
   * @param serviceName 服务名称
   * @param tag         服务标记
   * @return 管道
   */
  public static ManagedChannel getServiceChannel(ConsulDiscoverClient client, String serviceName,
      String tag) {
    HealthService hs = client.getHealthService(serviceName, tag);
    String target = String.format("%s:%s", hs.getService().getAddress(), hs.getService().getPort());
    ManagedChannel channel = ManagedChannelBuilder.forTarget(target)
        .intercept(TracingFactory.getTracing().clientInterceptor(),
            SystemClientInterceptor.getInstance())
        .usePlaintext().build();
    return channel;
  }

  /**
   * 客户端服务管道.
   *
   * @param client      注册客户端
   * @param appProps    App属性.
   * @param clientProps 客户端配置属性
   * @param serviceName 服务名称
   * @param tag         服务标记
   * @return 管道
   */
  public static ManagedChannel getManagedChannel(ConsulDiscoverClient client, AppProps appProps,
      ClientProps clientProps, String serviceName, String tag) {
    ManagedChannel channel = null;
    if (appProps.getRegister()) {
      channel = ConsulGrpcChannel.getServiceChannel(client, serviceName, tag);
    } else if (0 == clientProps.getPort()) {
      channel = InProcessChannelBuilder.forName(clientProps.getAddress()).directExecutor()
          .intercept(TracingFactory.getTracing().clientInterceptor(),
              SystemClientInterceptor.getInstance())
          .usePlaintext().build();
    } else {
      channel = ManagedChannelBuilder.forAddress(clientProps.getAddress(), clientProps.getPort())
          .intercept(TracingFactory.getTracing().clientInterceptor(),
              SystemClientInterceptor.getInstance())
          .usePlaintext().build();
    }

    return channel;
  }
}
