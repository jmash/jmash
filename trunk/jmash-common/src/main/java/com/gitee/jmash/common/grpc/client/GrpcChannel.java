
package com.gitee.jmash.common.grpc.client;

import com.gitee.jmash.common.clazz.ClassLoaderUtils;
import com.gitee.jmash.common.config.AppProps;
import com.gitee.jmash.common.grpc.GrpcContext;
import com.gitee.jmash.common.grpc.auth.OAuth2Credentials;
import com.gitee.jmash.common.tracing.TracingFactory;
import io.grpc.CallCredentials;
import io.grpc.ChannelCredentials;
import io.grpc.Grpc;
import io.grpc.InsecureChannelCredentials;
import io.grpc.ManagedChannel;
import io.grpc.TlsChannelCredentials;
import io.grpc.inprocess.InProcessChannelBuilder;
import jakarta.enterprise.inject.spi.CDI;
import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.LogFactory;
import org.eclipse.microprofile.config.inject.ConfigProperties;

/**
 * 获取客户端服务管道.
 *
 * @author CGD
 *
 */
public class GrpcChannel {

  /**
   * 获取内存客户端管道.
   */
  public static ManagedChannel getInProcessChannel() {
    AppProps appProps =
        CDI.current().select(AppProps.class, ConfigProperties.Literal.NO_PREFIX).get();
    return getInProcessChannel(appProps.getName());
  }

  /**
   * 获取内存客户端管道.
   *
   * @param name 服务名称
   */
  public static ManagedChannel getInProcessChannel(String name) {
    return InProcessChannelBuilder.forName(name).directExecutor()
        .intercept(TracingFactory.getTracing().clientInterceptor(),
            SystemClientInterceptor.getInstance())
        .usePlaintext().build();
  }

  /**
   * 客户端服务管道.
   *
   * @param serviceName 服务名称
   * @return 管道
   */
  public static ManagedChannel getServiceChannel(String serviceName) {
    AppProps appProps =
        CDI.current().select(AppProps.class, ConfigProperties.Literal.NO_PREFIX).get();
    if (appProps.getRegister()) {
      String newServiceName = serviceName.replaceAll("\\.svc\\.cluster\\.local", "");
      return getServiceChannel(newServiceName, appProps.getPort());
    } else {
      // 本地测试服务
      String host = appProps.getTestHost();
      Integer port = appProps.getTestPort();
      Boolean ssl = appProps.getTestSsl();
      Boolean insecure = appProps.getTestInsecure();
      if (ssl != null && !ssl) {
        return getServiceChannel(host, port);
      } else if (StringUtils.isNotBlank(appProps.getTestCert())) {
        InputStream input =
            ClassLoaderUtils.getResourceAsStream(appProps.getTestCert(), GrpcChannel.class);
        return getSslServiceChannel(host, port, insecure, input);
      }
      return getSslServiceChannel(host, port, insecure, null);
    }
  }

  /**
   * 客户端服务管道.
   *
   * @param host 服务名称
   * @param port 服务端口
   * @return 管道
   */
  public static ManagedChannel getServiceChannel(String host, int port) {
    return getServiceChannel(host, port, InsecureChannelCredentials.create());
  }

  /**
   * 客户端服务管道.
   *
   * @param host 服务名称
   * @param port 服务端口
   * @return 管道
   */
  public static ManagedChannel getServiceChannel(String host, int port, ChannelCredentials creds) {
    return Grpc.newChannelBuilderForAddress(host, port, creds)
        .intercept(TracingFactory.getTracing().clientInterceptor(),
            SystemClientInterceptor.getInstance())
        .build();
  }

  /**
   * 客户端服务管道.
   *
   * @param host 服务名称
   * @param port 服务端口
   * @param rootCerts TLS Root Certs.
   * @return 管道
   */
  public static ManagedChannel getSslServiceChannel(String host, int port, Boolean insecure,
      InputStream rootCerts) {
    try {
      // 跳过安全检查.
      if (null != insecure && insecure) {
        return getServiceChannel(host, port, InsecureChannelCredentials.create());
      }
      // With server authentication SSL/TLS;
      TlsChannelCredentials.Builder credsBuilder = TlsChannelCredentials.newBuilder();
      if (null != rootCerts) {
        credsBuilder.trustManager(rootCerts);
      }
      return getServiceChannel(host, port, credsBuilder.build());
    } catch (IOException e) {
      LogFactory.getLog(GrpcChannel.class).error(e);
      throw new RuntimeException(e);
    }
  }

  /**
   * 获取认证凭证.
   *
   * @return 认证凭证
   */
  public static CallCredentials getCallCredentials() {
    String accessToken = GrpcContext.USER_AUTH.get();
    return new OAuth2Credentials(accessToken);
  }

  /**
   * 获取认证凭证.
   *
   * @return 认证凭证
   */
  public static CallCredentials getCallCredentials(String tenant) {
    String accessToken = GrpcContext.USER_AUTH.get();
    return new OAuth2Credentials(accessToken, tenant);
  }
}
