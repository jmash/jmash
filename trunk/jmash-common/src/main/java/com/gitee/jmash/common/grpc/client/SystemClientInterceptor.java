
package com.gitee.jmash.common.grpc.client;

import com.gitee.jmash.common.grpc.GrpcContext;
import com.gitee.jmash.common.grpc.GrpcMetadata;
import com.gitee.jmash.common.utils.LocalIpUtil;
import io.grpc.CallOptions;
import io.grpc.Channel;
import io.grpc.ClientCall;
import io.grpc.ClientInterceptor;
import io.grpc.ForwardingClientCall.SimpleForwardingClientCall;
import io.grpc.Metadata;
import io.grpc.MethodDescriptor;
import org.apache.commons.lang3.StringUtils;

/**
 * Grpc客户端拦截器. 传递服务间共享值.
 *
 * @author CGD
 *
 */
public class SystemClientInterceptor implements ClientInterceptor {

  private static SystemClientInterceptor instance;

  /**
   * 默认客户端拦截器实例.
   *
   * @return 客户端拦截器实例
   */
  public static SystemClientInterceptor getInstance() {
    if (null == instance) {
      instance = new SystemClientInterceptor();
    }
    return instance;
  }

  @Override
  public <ReqT, RespT> ClientCall<ReqT, RespT> interceptCall(MethodDescriptor<ReqT, RespT> method,
      CallOptions callOptions, Channel next) {

    return new SimpleForwardingClientCall<ReqT, RespT>(next.newCall(method, callOptions)) {

      @Override
      public void start(Listener<RespT> responseListener, Metadata headers) {
        if (null == headers) {
          headers = new Metadata();
        }
        // GRPC 服务调用持续传递.
        if (StringUtils.isNotBlank(GrpcContext.GRPC_HOST.get())) {
          headers.put(GrpcMetadata.GRPC_HOST_KEY, GrpcContext.GRPC_HOST.get());
        } else {
          headers.put(GrpcMetadata.GRPC_HOST_KEY, LocalIpUtil.getHostName());
        }
        if (StringUtils.isNotBlank(GrpcContext.USER_IP.get())) {
          headers.put(GrpcMetadata.USER_IP_KEY, GrpcContext.USER_IP.get());
        }
        if (StringUtils.isNotBlank(GrpcContext.USER_PROXY_IP.get())) {
          headers.put(GrpcMetadata.USER_PROXY_IP_KEY, GrpcContext.USER_PROXY_IP.get());
        }
        if (StringUtils.isNotBlank(GrpcContext.BROWSER_USER_AGENT.get())) {
          headers.put(GrpcMetadata.BROWSER_USER_AGENT_KEY, GrpcContext.BROWSER_USER_AGENT.get());
        }
        if (null != GrpcContext.GRPC_LOCALE.get()) {
          headers.put(GrpcMetadata.GRPC_LOCALE_KEY, GrpcContext.GRPC_LOCALE.get().toLanguageTag());
        }
        if (null != GrpcContext.REFERER.get()) {
          headers.put(GrpcMetadata.HTTP_REFERER_KEY, GrpcContext.REFERER.get());
        }
        super.start(responseListener, headers);
      }

    };
  }

}
