
package com.gitee.jmash.common.i18n;

import com.gitee.jmash.common.cache.Cache;
import com.gitee.jmash.common.cache.DefaultCacheImpl;
import java.io.Serializable;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import org.apache.commons.logging.LogFactory;

/**
 * 国际化 i18n.
 *
 * @author CGD
 *
 */
public class MessageSource implements Serializable {

  private static Cache<String, Object> cache = new DefaultCacheImpl();

  private static final long serialVersionUID = 1L;

  private String[] baseNames;

  private MessageSource(String... baseNames) {
    this.baseNames = baseNames;
  }

  /** Cache MessageResourceBundle. */
  public MessageResourceBundle setLocale(Locale locale) {
    String key = String.format("%s:%s", String.join(",", this.baseNames), locale);
    if (cache.containsKey(key)) {
      return (MessageResourceBundle) cache.get(key);
    }
    MessageResourceBundle bundle = new MessageResourceBundle(this.baseNames, locale);
    cache.put(key, bundle);
    return bundle;
  }

  public static MessageSource setBaseNames(String... baseNames) {
    return new MessageSource(baseNames);
  }

  /** 国际化Message Format. */
  public String format(Locale locale, String key, Object... arguments) {
    return setLocale(locale).format(key, arguments);
  }

  /** i18n. */
  public String getString(Locale locale, String key) {
    return setLocale(locale).getString(key);
  }

  /** 格式化数字 Format Number. */
  public String formatNumber(Locale locale, long number) {
    return setLocale(locale).formatNumber(number);
  }

  /** 格式化数字 Format Number. */
  public String formatNumber(Locale locale, double number) {
    return setLocale(locale).formatNumber(number);
  }

  /** 格式化货币 Format Currency. */
  public String formatCurrency(Locale locale, long currency) {
    return setLocale(locale).formatCurrency(currency);
  }

  /** 格式化货币 Format Currency. */
  public String formatCurrency(Locale locale, double currency) {
    return setLocale(locale).formatCurrency(currency);
  }

  /** 日期格式化. */
  public DateTimeFormatter ofPattern(Locale locale, String pattern) {
    return DateTimeFormatter.ofPattern(pattern, locale);
  }

  /** 通过 MessageSource Builder. */
  public static class MessageResourceBundle implements Serializable {
    private static final long serialVersionUID = 1L;
    // Request Locale
    private Locale locale;
    // ResourceBundle baseNames
    private String[] baseNames;
    // 国际化i18n
    private List<ResourceBundle> bundles;
    // 数字格式化
    private NumberFormat numberFormat;
    // 货币格式化
    private NumberFormat currencyFormat;

    /**
     * 通过 MessageSource 构造.
     */
    private MessageResourceBundle(String[] baseNames, Locale locale) {
      this.baseNames = baseNames;
      if (null == locale) {
        this.locale = Locale.getDefault();
      } else {
        this.locale = locale;
      }
    }

    public Locale getLocale() {
      return locale;
    }

    public String[] getBaseNames() {
      return baseNames;
    }

    /** ResourceBundle i18n. */
    public List<ResourceBundle> getBundles() {
      if (null == bundles) {
        bundles = new ArrayList<>();
        for (int i = 0; i < this.baseNames.length; i++) {
          try {
            ResourceBundle bundle = ResourceBundle.getBundle(baseNames[i], locale);
            bundles.add(bundle);
          } catch (Exception ex) {
            LogFactory.getLog(getClass()).warn("Not Fund " + baseNames[i]);
          }
        }
      }
      return bundles;
    }

    /** Number Format. */
    public NumberFormat getNumberFormat() {
      if (null == numberFormat) {
        numberFormat = NumberFormat.getNumberInstance(locale);
      }
      return numberFormat;
    }

    /** Currency Format. */
    public NumberFormat getCurrencyFormat() {
      if (null == currencyFormat) {
        currencyFormat = NumberFormat.getCurrencyInstance(locale);
      }
      return currencyFormat;
    }

    /** 国际化Message Format. */
    public String format(String key, Object... arguments) {
      String pattern = getString(key);
      if (pattern.length() == 0) {
        return pattern;
      }
      MessageFormat mf = new MessageFormat(pattern, locale);
      return mf.format(arguments);
    }

    /** i18n. */
    public String getString(String key) {
      for (ResourceBundle bundle : getBundles()) {
        if (bundle.containsKey(key)) {
          return bundle.getString(key);
        }
      }
      LogFactory.getLog(MessageSource.class).warn("Can't find resource for bundle , key = " + key);
      return "";
    }

    /** 格式化数字 Format Number. */
    public String formatNumber(long number) {
      return getNumberFormat().format(number);
    }

    /** 格式化数字 Format Number. */
    public String formatNumber(double number) {
      return getNumberFormat().format(number);
    }

    /** 格式化货币 Format Currency. */
    public String formatCurrency(long currency) {
      return getCurrencyFormat().format(currency);
    }

    /** 格式化货币 Format Currency. */
    public String formatCurrency(double currency) {
      return getCurrencyFormat().format(currency);
    }

    /** 日期格式化. */
    public DateTimeFormatter ofPattern(String pattern) {
      return DateTimeFormatter.ofPattern(pattern, locale);
    }
  }
}
