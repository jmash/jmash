
package com.gitee.jmash.common.image;

import java.awt.Color;
import java.awt.image.RGBImageFilter;

/**
 * 图片颜色过滤.
 *
 * @author CGD
 *
 */
public class ColorRgbImageFilter extends RGBImageFilter {

  // the color we are looking for... Alpha bits are set to opaque
  public int markerRgb = 0;

  public ColorRgbImageFilter(Color color) {
    this.markerRgb = color.getRGB() | 0xFF000000;
  }

  @Override
  public final int filterRGB(int x, int y, int rgb) {
    if ((rgb | 0xFF000000) == markerRgb) {
      // Mark the alpha bits as zero - transparent
      return 0x00FFFFFF & rgb;
    } else {
      // nothing to do
      return rgb;
    }
  }
}
