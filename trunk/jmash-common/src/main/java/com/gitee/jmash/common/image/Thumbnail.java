
package com.gitee.jmash.common.image;

import com.gitee.jmash.common.enums.FileType;
import com.gitee.jmash.common.enums.MimeType;
import com.gitee.jmash.common.image.ThumbnailFilter.ThumbnailType;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.CropImageFilter;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.awt.image.ImageProducer;
import java.awt.image.RGBImageFilter;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import org.apache.commons.io.FilenameUtils;

/**
 * Title: Thumbnail.<br/>
 * Description: Picture Thumbnail <br/>
 * Copyright: Copyright (c) 54powerman@163.com 2005 <br/>
 * Company: http://blog.sina.com.cn/u1055000490
 *
 * @author 54powerman
 * @version 1.0
 */
public class Thumbnail {

  private String destFile;

  private MimeType mimeType;

  private Image img;

  private String srcFile;

  /** Main. */
  public static void main(String[] args) throws Exception {
    Thumbnail t = new Thumbnail("d:\\ic_login_bg.jpg");
    t.setDestFile("d:\\Test_1.png");
    t.resizeClip(240, 240);
    // makeWhiteTransparent(new File("d:\\Test_2.jpg"), new File("d:\\Test_3.png"));
  }

  public MimeType getMimeType() {
    return mimeType;
  }

  public void setMimeType(MimeType mimeType) {
    this.mimeType = mimeType;
  }

  /**
   * 构造函数.
   */
  public Thumbnail(String fileName) throws IOException {

    this.srcFile = fileName;

    String fileExt = MimeType.getExtension(fileName).toLowerCase();
    mimeType = MimeType.convert(fileExt);

    this.destFile = FilenameUtils.getFullPath(this.srcFile)
        + FilenameUtils.getBaseName(this.srcFile) + "_s." + fileExt;
    // 读入文件
    File file = new File(this.srcFile);
    // 构造Image对象
    img = javax.imageio.ImageIO.read(file);
  }

  /**
   * 按照固定的比例缩放图片.
   *
   * @param t double 比例
   */
  public void resize(double t) throws IOException {
    ThumbnailFilter filter = new ThumbnailFilter(t, ThumbnailType.scale, mimeType, null);
    BufferedImage dst = filter.filter(img);
    ImageIO.write(dst, mimeType.getFileExt(), new File(destFile));
  }

  /**
   * 缩略图（等比例缩放，保存全图内容完整）.
   */
  public void resize(int w, int h, ThumbnailType thumbnailType, Color color) throws IOException {
    ThumbnailFilter filter = new ThumbnailFilter(w, h, thumbnailType, mimeType, color);
    BufferedImage dst = filter.filter(img);
    ImageIO.write(dst, mimeType.getFileExt(), new File(destFile));
  }

  /**
   * 以宽度为基准，等比例放缩图片.
   *
   * @param w int 新宽度
   */
  public void resizeByWidth(int w) throws IOException {
    ThumbnailFilter filter = new ThumbnailFilter(w, 0, ThumbnailType.Width, mimeType, null);
    BufferedImage dst = filter.filter(img);
    ImageIO.write(dst, mimeType.getFileExt(), new File(destFile));
  }

  /**
   * 以高度为基准，等比例缩放图片.
   *
   * @param h int 新高度
   */
  public void resizeByHeight(int h) throws IOException {
    ThumbnailFilter filter = new ThumbnailFilter(0, h, ThumbnailType.Height, mimeType, null);
    BufferedImage dst = filter.filter(img);
    ImageIO.write(dst, mimeType.getFileExt(), new File(destFile));
  }

  /**
   * 按照最小高度/宽度限制，生成最大的等比例缩略图.
   */
  public void resizeMaxFix(int w, int h) throws IOException {
    ThumbnailFilter filter = new ThumbnailFilter(w, h, ThumbnailType.MaxFix, mimeType, null);
    BufferedImage dst = filter.filter(img);
    ImageIO.write(dst, mimeType.getFileExt(), new File(destFile));
  }

  /**
   * 按照最大高度/宽度限制，生成最小的等比例缩略图.
   */
  public void resizeMinFix(int w, int h) throws IOException {
    ThumbnailFilter filter = new ThumbnailFilter(w, h, ThumbnailType.MinFix, mimeType, null);
    BufferedImage dst = filter.filter(img);
    ImageIO.write(dst, mimeType.getFileExt(), new File(destFile));
  }

  /**
   * 按照高度、宽度截取.
   */
  public void resizeClip(int w, int h) throws IOException {
    ThumbnailFilter filter = new ThumbnailFilter(w, h, ThumbnailType.Clip, mimeType, null);
    BufferedImage dst = filter.filter(img);
    ImageIO.write(dst, mimeType.getFileExt(), new File(destFile));
  }

  /**
   * Web端图片处理.
   */
  public void resizeWeb(Integer width, Integer height, WebThumbType thumbType) throws IOException {
    if (thumbType == null) {
      thumbType = WebThumbType.resize;
    }
    if (width == null || width <= 0) {
      this.resizeByHeight(height);
    } else if (height == null || height <= 0) {
      this.resizeByWidth(width);
    } else if (WebThumbType.resize.equals(thumbType)) {
      this.resizeMaxFix(width, height);
    } else if (WebThumbType.white.equals(thumbType)) {
      this.resize(width, height, ThumbnailType.FillWhiteColor, Color.WHITE);
    } else if (WebThumbType.trans.equals(thumbType)) {
      this.resize(width, height, ThumbnailType.FillTranslucentColor, null);
    } else if (WebThumbType.clip.equals(thumbType)) {
      this.resize(width, height, ThumbnailType.Clip, null);
    }
  }

  /**
   * 设置目标文件名 setDestFile.
   *
   * @param fileName String 文件名字符串
   */
  public void setDestFile(String fileName) throws Exception {
    String fileExt = MimeType.getExtension(fileName).toLowerCase();
    MimeType mimeType2 = MimeType.convert(fileExt);
    if (FileType.image.equals(mimeType2.getFileType())) {
      destFile = fileName;
    } else {
      throw new Exception("Crenjoy:Dest File Support JPG PNG BMP  ");
    }
  }

  /**
   * 获取目标文件名 getDestFile.
   */
  public String getDestFile() {
    return destFile;
  }

  /**
   * 高质量图片截取.
   *
   * @param src 源文件路径
   * @param w 缩放后宽度
   * @param h 缩放后高度
   * @param width 选择的宽度
   * @param height 选择的高度
   * @param selectorX 选择的X座标
   * @param selectorY 选择的Y座标
   * @param imageX 图片的X座标
   * @param imageY 图片的Y座标
   */
  public static BufferedImage stretchImage(File src, int w, int h, int width, int height,
      int selectorX, int selectorY, int imageX, int imageY) throws IOException {
    BufferedImage srcImage = ImageIO.read(src);
    Image stretchImage = srcImage.getScaledInstance(w, h, Image.SCALE_SMOOTH);
    BufferedImage cutImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
    Graphics2D graphics = cutImage.createGraphics();
    graphics.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
    graphics.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING,
        RenderingHints.VALUE_COLOR_RENDER_QUALITY);
    graphics.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
        RenderingHints.VALUE_INTERPOLATION_BICUBIC);
    ImageFilter cropFilter =
        new CropImageFilter(selectorX - imageX, selectorY - imageY, width, height);
    graphics.drawImage(Toolkit.getDefaultToolkit()
        .createImage(new FilteredImageSource(stretchImage.getSource(), cropFilter)), 0, 0, null);
    graphics.dispose();
    return cutImage;
  }

  /**
   * 过滤图片颜色为透明色.
   */
  public static Image makeColorTransparent(Image im, final Color color) {
    RGBImageFilter filter = new ColorRgbImageFilter(color);
    ImageProducer ip = new FilteredImageSource(im.getSource(), filter);
    return Toolkit.getDefaultToolkit().createImage(ip);
  }

  /**
   * 生成白色为透明文件.
   */
  public static void makeWhiteTransparent(File src, File desc) throws IOException {
    BufferedImage srcImage = ImageIO.read(src);
    Image descImage = makeColorTransparent(srcImage, Color.WHITE);
    // 转透明色 采用TYPE_INT_ARGB,保存PNG,否则为黑色
    BufferedImage image =
        new BufferedImage(srcImage.getWidth(), srcImage.getHeight(), BufferedImage.TYPE_INT_ARGB);
    // 绘制缩小后的图
    image.getGraphics().drawImage(descImage, 0, 0, srcImage.getWidth(), srcImage.getHeight(), null);
    ImageIO.write(image, "PNG", desc);
  }

  /** Thumb Type. */
  public enum WebThumbType {
    resize("jpg"),
    /** 补白. */
    white("jpg"),
    /** 补透明. */
    trans("png"),
    /** 裁剪. */
    clip("jpg");

    private String fileExt;

    public String getFileExt() {
      return fileExt;
    }

    private WebThumbType(String fileExt) {
      this.fileExt = fileExt;
    }
  }
}
