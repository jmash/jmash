
package com.gitee.jmash.common.image;

import com.gitee.jmash.common.enums.MimeType;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import javax.swing.ImageIcon;
import org.apache.commons.lang3.StringUtils;

/**
 * 缩放过滤器.
 *
 * @author CGD
 * 
 */
public class ThumbnailFilter {

  /** 缩略图宽度. */
  private int width;
  /** 缩略图高度. */
  private int height;
  /** 缩略图比例. */
  private double scale;
  /** 缩略图类型. */
  private ThumbnailType thumbnailType;
  /** 图像类型. */
  private MimeType destImageType;
  /** 填充颜色. */
  private Color color;

  /**
   * 构造.
   *
   * @param width         缩略图宽度
   * @param height        缩略图高度
   * @param thumbnailType 缩略图类型
   * @param destImageType 目标图片类型jpg,png,gif
   * @param color         缩略图类型为FillColor时有效，设置FillWhiteColor类型默认白色、
   *                      设置FillTranslucentColor类型默认透明
   */
  public ThumbnailFilter(int width, int height, ThumbnailType thumbnailType,
      MimeType destImageType, Color color) {
    this.width = width;
    this.height = height;
    this.thumbnailType = thumbnailType;
    this.destImageType = destImageType;
    this.color = color;
  }

  /**
   * 构造.
   *
   * @param scale         缩略图比例
   * @param thumbnailType 缩略图类型
   * @param destImageType 目标图片类型jpg,png,gif
   * @param color         缩略图类型为FillColor时有效，设置FillWhiteColor类型默认白色、
   *                      设置FillTranslucentColor类型默认透明
   */
  public ThumbnailFilter(double scale, ThumbnailType thumbnailType, MimeType destImageType,
      Color color) {
    this.scale = scale;
    this.thumbnailType = thumbnailType;
    this.destImageType = destImageType;
    this.color = color;
  }

  /** 缩略图. */
  public BufferedImage filter(Image src) {
    // 图片宽度
    int w = src.getWidth(null);
    // 图片高度
    int h = src.getHeight(null);

    // 原图片裁剪宽度
    int clipW = 0;
    // 原图片裁剪高度
    int clipH = 0;

    switch (thumbnailType) {
      case scale: // 缩放比例
        width = (int) (w * scale);
        height = (int) (h * scale);
        break;
      case Width: // 按宽度
        height = (int) (h * width / w);
        break;
      case Height: // 按高度
        width = (int) (w * height / h);
        break;
      case Clip: // 截取图片
        if (w * 1.0f / h < width * 1.0f / height) {
          clipH = (int) (h * width / w);
        } else {
          clipW = (int) (w * height / h);
        }
        break;
      case MaxFix:
        // 最大的等比例缩略图
        if (w * 1.0f / h < width * 1.0f / height) {
          height = (int) (h * width / w);
        } else {
          width = (int) (w * height / h);
        }
        break;
      case MinFix:
        // 最小的等比例缩略图
        if (w * 1.0f / h >= width * 1.0f / height) {
          height = (int) (h * width / w);
        } else {
          width = (int) (w * height / h);
        }
        break;
      case FillColor: // 填充颜色
        if (w * 1.0f / h >= width * 1.0f / height) {
          clipH = (int) (h * width / w);
        } else {
          clipW = (int) (w * height / h);
        }
        break;
      case FillWhiteColor: // 填充白色
        color = Color.WHITE;
        if (w * 1.0f / h >= width * 1.0f / height) {
          clipH = (int) (h * width / w);
        } else {
          clipW = (int) (w * height / h);
        }
        break;
      case FillTranslucentColor: // 填充透明
        color = null;
        if (w * 1.0f / h >= width * 1.0f / height) {
          clipH = (int) (h * width / w);
        } else {
          clipW = (int) (w * height / h);
        }
        break;
      default:
        break;
    }

    if (0 == clipW) {
      clipW = width;
    }

    if (0 == clipH) {
      clipH = height;
    }

    Image resizedImage = src.getScaledInstance(clipW, clipH, Image.SCALE_SMOOTH);
    // This code ensures that all the pixels in the image are loaded.
    Image temp = new ImageIcon(resizedImage).getImage();

    int imageType = MimeType.M_jpg.equals(destImageType) || MimeType.M_jpeg.equals(destImageType) ? BufferedImage.TYPE_INT_RGB
        : BufferedImage.TYPE_INT_ARGB;
    // Create the buffered image.
    BufferedImage bufferedImage = new BufferedImage(width, height, imageType);

    // Copy image to buffered image.
    Graphics g = bufferedImage.createGraphics();

    // Clear background and paint the image.
    if (null != color) {
      g.setColor(color);
      g.fillRect(0, 0, width, height);
    }

    g.drawImage(temp, (width - clipW) / 2, (height - clipH) / 2, null);

    g.dispose();

    return bufferedImage;
  }

  /**
   * 缩略图类型.
   *
   * @author CGD
   * 
   */
  public static enum ThumbnailType {
    /** 按比例缩放. */
    scale,
    /** 按宽度截取. */
    Width,
    /** 按高度截取. */
    Height,
    /** 按照最小宽度或高度限制，生成最大的等比例缩略图. */
    MaxFix,
    /** 按照最大宽度或高度限制，生成最小的等比例缩略图. */
    MinFix,
    /** 按宽度、高度、裁剪图片. */
    Clip,
    /** 按宽度、高度缩略，填充背景色 白色、透明. */
    FillColor,
    /** 按宽度、高度缩略，填充背景色 白色. */
    FillWhiteColor,
    /** 按宽度、高度缩略，填充背景色 透明 png,gif. */
    FillTranslucentColor;
  }

}
