
package com.gitee.jmash.common.jaxrs;

import com.gitee.jmash.common.security.DefaultJmashPrincipal;
import com.gitee.jmash.common.security.JmashPrincipal;
import org.eclipse.microprofile.jwt.JsonWebToken;

/**
 * Web Context.
 *
 * @author CGD
 *
 */
public class WebContext {

  // 租户.
  public static final ThreadLocal<String> TENANT = new ThreadLocal<>();
  // 用户IP.
  public static final ThreadLocal<String> USER_IP = new ThreadLocal<>();
  // 代理IP.
  public static final ThreadLocal<String> PROXY_IP = new ThreadLocal<>();
  // 登录用户
  public static final ThreadLocal<JsonWebToken> USER_TOKEN = new ThreadLocal<>();

  // 获取当前用户凭证
  public static JmashPrincipal getPrincipal() {
    JsonWebToken webToken = WebContext.USER_TOKEN.get();
    return DefaultJmashPrincipal.create(webToken);
  }

}
