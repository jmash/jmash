
package com.gitee.jmash.common.lock;

import com.gitee.jmash.common.redis.RedisProps;
import com.gitee.jmash.common.redis.RedisUtil;
import com.gitee.jmash.common.redis.callback.RedisTemplate;
import com.gitee.jmash.common.utils.UUIDUtil;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import java.util.List;
import java.util.UUID;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.Transaction;
import redis.clients.jedis.exceptions.JedisException;

/**
 * 分布式锁简单实现.
 *
 * @author bruceliu
 */
@ApplicationScoped
public class DistributedLock {

  private static Log log = LogFactory.getLog(DistributedLock.class);

  @Inject
  @Named
  private RedisTemplate redisTemplate;

  public DistributedLock() {
    super();
  }

  /** Not Use CDI. */
  public DistributedLock(RedisProps redisProps) {
    super();
    JedisPool jedisPool = RedisUtil.getJedisPool(redisProps);
    this.redisTemplate = new RedisTemplate(jedisPool);
  }

  /**
   * 加锁(超时自动解锁).
   *
   * @param lockName 锁的key
   * @param seconds  自动解锁时间(秒)
   */
  public boolean lock(String lockName, long seconds) {
    // 空锁
    if (UUIDUtil.emptyUUIDStr().equals(lockName)) {
      return true;
    }
    String lockKey = "lock:" + lockName;

    try (Jedis conn = redisTemplate.getResource()) {
      if (conn.incr(lockKey) == 1) {
        conn.expire(lockKey, seconds);
        return true;
      } else {
        return false;
      }
    }
  }

  /**
   * 加锁.
   *
   * @param lockName       锁的key
   * @param acquireTimeout 获取超时时间(毫秒)
   * @param timeout        锁的超时时间(毫秒)
   * @return 锁标识
   */
  public String lockWithTimeout(String lockName, long acquireTimeout, long timeout) {
    Jedis conn = null;
    String retIdentifier = null;
    try {
      // 获取连接
      conn = redisTemplate.getResource();
      // 随机生成一个value
      String identifier = UUID.randomUUID().toString();
      // 锁名，即key值
      String lockKey = "lock:" + lockName;

      // 超时时间，上锁后超过此时间则自动释放锁
      int lockExpire = (int) (timeout / 1000);

      // 获取锁的超时时间，超过这个时间则放弃获取锁
      long end = System.currentTimeMillis() + acquireTimeout;
      while (System.currentTimeMillis() < end) {
        if (conn.setnx(lockKey, identifier) == 1) {
          conn.expire(lockKey, lockExpire);
          // 返回value值，用于释放锁时间确认
          retIdentifier = identifier;
          return retIdentifier;
        }
        // 返回-1代表key没有设置超时时间，为key设置一个超时时间
        if (conn.ttl(lockKey) == -1) {
          conn.expire(lockKey, lockExpire);
        }
        try {
          Thread.sleep(10);
        } catch (InterruptedException e) {
          Thread.currentThread().interrupt();
        }
      }
    } catch (JedisException e) {
      log.error("", e);
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
    return retIdentifier;
  }

  /**
   * 释放锁.
   *
   * @param lockName   锁的key
   * @param identifier 释放锁的标识
   */
  public boolean releaseLock(String lockName, String identifier) {
    Jedis conn = null;
    String lockKey = "lock:" + lockName;
    boolean retFlag = false;
    try {
      conn = redisTemplate.getResource();
      while (true) {
        // 监视lock，准备开始事务
        conn.watch(lockKey);
        // 通过前面返回的value值判断是不是该锁，若是该锁，则删除，释放锁
        if (identifier.equals(conn.get(lockKey))) {
          Transaction transaction = conn.multi();
          transaction.del(lockKey);
          List<Object> results = transaction.exec();
          if (results == null) {
            continue;
          }
          retFlag = true;
        }
        conn.unwatch();
        break;
      }
    } catch (JedisException e) {
      log.error("", e);
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
    return retFlag;
  }
}
