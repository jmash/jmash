
package com.gitee.jmash.common.redis;

import com.gitee.jmash.common.redis.callback.RedisTemplate;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Produces;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import jakarta.inject.Singleton;
import org.eclipse.microprofile.config.inject.ConfigProperties;
import redis.clients.jedis.JedisPool;

/**
 * Redis CDI Config.
 *
 * @author CGD
 *
 */
@ApplicationScoped
public class RedisConfig {

  @Inject
  @ConfigProperties
  RedisProps redisProps;

  /** 默认Redis . */
  @Produces
  @Named
  @Singleton
  public RedisTemplate getRedisTemplate() {
    redisProps.setDatabase(2);
    JedisPool jedisPool = RedisUtil.getJedisPool(redisProps);
    return new RedisTemplate(jedisPool);
  }

  /** Shiro Redis Cache Use Shiro. */
  @Produces
  @Named("redisShiroCache")
  @Singleton
  public RedisTemplate getRedisShiroCache() {
    redisProps.setDatabase(1);
    JedisPool jedisPool = RedisUtil.getJedisPool(redisProps);
    return new RedisTemplate(jedisPool);
  }

}
