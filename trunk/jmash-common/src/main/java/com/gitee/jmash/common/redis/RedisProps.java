
package com.gitee.jmash.common.redis;

import com.gitee.jmash.common.utils.ConfigSecretUtil;
import jakarta.enterprise.context.Dependent;
import org.eclipse.microprofile.config.inject.ConfigProperties;

/**
 * Redis Config .
 *
 * @author CGD
 *
 */
@ConfigProperties(prefix = "jedis")
@Dependent
public class RedisProps {

  // IP 地址
  String host = "127.0.0.1";
  // 端口
  Integer port = 6379;
  // 连接超时2000
  Integer connectionTimeout = 2000;
  // 超时
  Integer soTimeout = 2000;
  // 密码
  String password = "C8D2F3F882310488A23930A01154BBD1";
  // 是否解码
  Boolean isdecode = false;
  // 数据库 Use;Default 2;Shiro 1;
  Integer database = 2;
  // 客户端名称
  String clientName = "Jedis";
  // 控制一个pool可分配多少个jedis实例，通过pool.getResource()来获取；
  // 如果赋值为-1，则表示不限制；如果pool已经分配了maxActive个jedis实例，则此时pool的状态为exhausted(耗尽)。
  Integer maxTotal = 100;
  // 控制一个pool最多有多少个状态为idle(空闲的)的jedis实例。
  Integer maxIdle = 10;
  // 表示当borrow(引入)一个jedis实例时，最大的等待时间，如果超过等待时间，则直接抛出JedisConnectionException；
  Long maxWaitMillis = 10000L;

  public RedisProps() {}

  public String getHost() {
    return host;
  }

  public void setHost(String host) {
    this.host = host;
  }

  public Integer getPort() {
    return port;
  }

  public void setPort(Integer port) {
    this.port = port;
  }

  public Integer getConnectionTimeout() {
    return connectionTimeout;
  }

  public void setConnectionTimeout(Integer connectionTimeout) {
    this.connectionTimeout = connectionTimeout;
  }

  public Integer getSoTimeout() {
    return soTimeout;
  }

  public void setSoTimeout(Integer soTimeout) {
    this.soTimeout = soTimeout;
  }

  /** 获取密钥. */
  public String getPassword() {
    if (isdecode) {
      return password;
    } else {
      password = ConfigSecretUtil.decrypt(password);
      isdecode = true;
      return password;
    }
  }

  public void setPassword(String password) {
    this.password = password;
    this.isdecode = false;
  }

  public Integer getDatabase() {
    return database;
  }

  public void setDatabase(Integer database) {
    this.database = database;
  }

  public String getClientName() {
    return clientName;
  }

  public void setClientName(String clientName) {
    this.clientName = clientName;
  }

  public Integer getMaxTotal() {
    return maxTotal;
  }

  public void setMaxTotal(Integer maxTotal) {
    this.maxTotal = maxTotal;
  }

  public Integer getMaxIdle() {
    return maxIdle;
  }

  public void setMaxIdle(Integer maxIdle) {
    this.maxIdle = maxIdle;
  }

  public Long getMaxWaitMillis() {
    return maxWaitMillis;
  }

  public void setMaxWaitMillis(Long maxWaitMillis) {
    this.maxWaitMillis = maxWaitMillis;
  }

  @Override
  public String toString() {
    return "RedisProps [host=" + host + ", port=" + port + ", connectionTimeout="
        + connectionTimeout + ", soTimeout=" + soTimeout + ", password=" + password + ", database="
        + database + ", clientName=" + clientName + ", maxTotal=" + maxTotal + ", maxIdle="
        + maxIdle + ", maxWaitMillis=" + maxWaitMillis + "]";
  }

}
