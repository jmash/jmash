
package com.gitee.jmash.common.redis;

import com.gitee.jmash.common.redis.proxy.RadisTransactionProxy;
import com.gitee.jmash.common.redis.proxy.RedisTransaction;
import java.lang.reflect.Proxy;
import java.time.Duration;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * Redis 工具类.
 *
 * @author CGD
 *
 */
public class RedisUtil {

  /**
   * Redis 连接池.
   */
  public static JedisPool getJedisPool(RedisProps redisProps) {
    JedisPoolConfig config = new JedisPoolConfig();
    config.setMaxTotal(redisProps.getMaxTotal());
    config.setMaxIdle(redisProps.getMaxIdle());
    final Duration maxWaitDuration = Duration.ofMillis(redisProps.getMaxWaitMillis());
    config.setMaxWait(maxWaitDuration);
    config.setTestOnBorrow(true);
    return new JedisPool(config, redisProps.getHost(), redisProps.getPort(),
        redisProps.getConnectionTimeout(), redisProps.getSoTimeout(), redisProps.getPassword(),
        redisProps.getDatabase(), redisProps.getClientName());
  }

  /**
   * 创建Redis代理.
   */
  @SuppressWarnings("unchecked")
  public static <T extends RedisTransaction> T proxy(JedisPool jedisPool, T targetObject) {
    RadisTransactionProxy<T> proxy = new RadisTransactionProxy<T>(jedisPool, targetObject);
    // 生成代理对象
    return (T) Proxy.newProxyInstance(targetObject.getClass().getClassLoader(),
        targetObject.getClass().getInterfaces(), proxy);
  }
}
