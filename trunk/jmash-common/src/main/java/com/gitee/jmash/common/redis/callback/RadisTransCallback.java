
package com.gitee.jmash.common.redis.callback;

import redis.clients.jedis.Transaction;

/**
 * Redis Transaction Callback.
 *
 * @author CGD
 */
public interface RadisTransCallback<T> {

  T call(Transaction transaction);

}
