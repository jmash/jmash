
package com.gitee.jmash.common.redis.callback;

import redis.clients.jedis.Jedis;

/**
 * Redis 回调.
 *
 * @author CGD
 */
public interface RedisCallback<T> {

  T call(Jedis jedis);

}
