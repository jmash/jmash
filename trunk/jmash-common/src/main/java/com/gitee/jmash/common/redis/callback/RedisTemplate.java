
package com.gitee.jmash.common.redis.callback;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.Transaction;

/**
 * 回调方式不支持乐观锁.
 *
 * @author CGD
 *
 */
public class RedisTemplate {

  private static Log log = LogFactory.getLog(RedisTemplate.class);

  private JedisPool jedisPool;

  public RedisTemplate(JedisPool jedisPool) {
    super();
    this.jedisPool = jedisPool;
  }

  public JedisPool getJedisPool() {
    return jedisPool;
  }
  
  public Jedis getResource() {
    return jedisPool.getResource();
  }

  /**
   * 无事务，连接池中获取连接.
   */
  public <T> T transactionNone(RedisCallback<T> callback) {
    try (Jedis jedis = jedisPool.getResource()) {
      return callback.call(jedis);
    } catch (Exception e) {
      log.error("Redis Error:", e);
      throw new RuntimeException(e);
    }
  }

  /**
   * 事务，保证原子操作.
   */
  public <T> T transactionMulti(RadisTransCallback<T> callback) {
    Transaction transaction = null;
    try (Jedis jedis = jedisPool.getResource()) {
      transaction = jedis.multi();
      T result = callback.call(transaction);
      if (null != transaction) {
        transaction.exec();
      }
      return result;
    } catch (Exception e) {
      log.error("Redis Transaction Error:", e);
      if (null != transaction) {
        transaction.discard();
      }      
      throw new RuntimeException(e);
    }
  }

}
