
package com.gitee.jmash.common.redis.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.Transaction;

/**
 * 事务代理.
 *
 * @author CGD
 *
 */
public class RadisTransactionProxy<T extends RedisTransaction> implements InvocationHandler {

  private Log log = LogFactory.getLog(RadisTransactionProxy.class);

  private JedisPool jedisPool;

  private T targetObject;

  /**
   * 构造代理.
   */
  public RadisTransactionProxy(JedisPool jedisPool, T targetObject) {
    super();
    this.jedisPool = jedisPool;
    this.targetObject = targetObject;
  }

  /**
   * 拦截器.
   */
  public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
    log.debug(method.getName());
    Method ml = targetObject.getClass().getMethod(method.getName(), method.getParameterTypes());

    TransactionRadis transactionRadis = ml.getAnnotation(TransactionRadis.class);
    if (null == transactionRadis) {
      return method.invoke(targetObject, args);
    }
    // 无事务，连接池中获取连接
    if (TransactionType.NONE.equals(transactionRadis.value())) {
      return transactionNone(proxy, method, args);
    } else if (TransactionType.MULTI.equals(transactionRadis.value())) {
      // 事务，保证原子操作
      return transactionMulti(proxy, method, args);
    } else if (TransactionType.WATCH.equals(transactionRadis.value())) {
      // 乐观锁 事务，保证原子操作,监视Key变化
      return transactionWatch(proxy, method, args, transactionRadis.WatchKey());
    }

    return null;
  }

  /**
   * 无事务，连接池中获取连接.
   */
  public synchronized Object transactionNone(Object proxy, Method method, Object[] args)
      throws Throwable {
    Jedis jedis = null;

    try {
      if (null == targetObject.getJedis()) {
        jedis = jedisPool.getResource();
        targetObject.setJedis(jedis);
        targetObject.setTransaction(null);
      }
      return method.invoke(targetObject, args);
    } catch (Exception e) {
      log.error("NONE", e);
      throw new RuntimeException(e);
    } finally {
      if (null != jedis) {
        jedis.close();
        targetObject.setJedis(null);
        targetObject.setTransaction(null);
      }
    }
  }

  /**
   * 事务，保证原子操作.
   */
  public synchronized Object transactionMulti(Object proxy, Method method, Object[] args)
      throws Throwable {
    Jedis jedis = null;
    Transaction transaction = null;
    try {
      if (null == targetObject.getJedis()) {
        jedis = jedisPool.getResource();
        transaction = jedis.multi();
        targetObject.setJedis(jedis);
        targetObject.setTransaction(transaction);
      }
      Object result = method.invoke(targetObject, args);
      if (null != transaction) {
        transaction.exec();
      }
      return result;
    } catch (Exception e) {
      if (null != transaction) {
        transaction.discard();
      }
      log.error("Multi", e);
      throw new RuntimeException(e);
    } finally {
      if (null != jedis) {
        jedis.close();
        targetObject.setJedis(null);
        targetObject.setTransaction(null);
      }
    }
  }

  /**
   * 乐观锁， 事务，保证原子操作.
   */
  public synchronized Object transactionWatch(Object proxy, Method method, Object[] args,
      String[] keys) throws Throwable {
    Jedis jedis = null;
    Transaction transaction = null;
    try {
      if (null == targetObject.getJedis()) {
        jedis = jedisPool.getResource();
        jedis.watch(keys);
        transaction = jedis.multi();
        targetObject.setJedis(jedis);
        targetObject.setTransaction(transaction);
      }
      Object result = method.invoke(targetObject, args);
      if (null != jedis) {
        transaction.exec();
        jedis.unwatch();
      }
      return result;
    } catch (Exception e) {
      if (null != transaction) {
        jedis.unwatch();
        transaction.discard();
      }
      log.error("Watch", e);
      throw new RuntimeException(e);
    } finally {
      if (null != jedis) {
        jedis.close();
      }
    }
  }

}
