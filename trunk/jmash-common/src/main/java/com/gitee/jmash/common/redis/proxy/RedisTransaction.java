
package com.gitee.jmash.common.redis.proxy;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.Transaction;

/**
 * Redis Transaction.
 *
 * @author CGD
 *
 */
public interface RedisTransaction {

  public Jedis getJedis();

  public void setJedis(Jedis jedis);

  public Transaction getTransaction();

  public void setTransaction(Transaction transaction);

}
