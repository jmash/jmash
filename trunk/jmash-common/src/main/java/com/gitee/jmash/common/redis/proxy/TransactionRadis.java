
package com.gitee.jmash.common.redis.proxy;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * 事务注解.
 *
 * @author CGD
 *
 */
@Target({ METHOD, TYPE })
@Retention(RUNTIME)
public @interface TransactionRadis {
  /**
   * NONE @TransactionRadis(value=TransactionType.NONE).
   * MULTI @TransactionRadis(value=TransactionType.MULTI).
   * WATCH @TransactionRadis(value=TransactionType.WATCH,WatchKey={"key1","key2"}).
   */
  TransactionType value() default TransactionType.NONE;

  /** Watch Key. */
  String[] WatchKey() default {};
}
