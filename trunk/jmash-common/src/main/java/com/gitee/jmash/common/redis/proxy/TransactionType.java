
package com.gitee.jmash.common.redis.proxy;

/**
 * 事务类型.
 *
 * @author CGD
 *
 */
public enum TransactionType {

  /**
   * 无事务 @TransactionRadis(value=TransactionType.NONE).
   */
  NONE,
  /**
   * 事务属性 @TransactionRadis(value=TransactionType.MULTI).
   */
  MULTI,
  /**
   * 乐观锁
   * 监视Key @TransactionRadis(value=TransactionType.WATCH,WatchKey={"key1","key2"}).
   */
  WATCH;
}
