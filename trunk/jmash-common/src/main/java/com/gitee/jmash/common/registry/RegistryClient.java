
package com.gitee.jmash.common.registry;

/**
 * 抽象服务注册客户端.
 *
 * @author CGD
 *
 */
public interface RegistryClient {

  /**
   * 服务注册.
   */
  void serviceRegister();

  /**
   * 服务检查注册.
   */
  void checkRegister();

  /**
   * 服务注销.
   */
  void serviceDeregister();

  /**
   * 服务检测注销.
   */
  void checkDeregister();

}
