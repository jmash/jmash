
package com.gitee.jmash.common.registry.consul;

import com.ecwid.consul.v1.ConsulClient;
import com.ecwid.consul.v1.QueryParams;
import com.ecwid.consul.v1.Response;
import com.ecwid.consul.v1.health.HealthServicesRequest;
import com.ecwid.consul.v1.health.model.HealthService;
import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;
import java.util.List;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.config.inject.ConfigProperties;

/**
 * Consul 发现客户端.
 *
 * @author CGD
 *
 */
//@ApplicationScoped
public class ConsulDiscoverClient {

  private ConsulClient client;

  @Inject
  @ConfigProperties
  private ConsulProps consulProps;

  public ConsulDiscoverClient() {
    super();
  }

  @PostConstruct
  public void init() {
    client = new ConsulClient(consulProps.getHost(), consulProps.getPort());
  }

  /**
   * 获取健康服务列表.
   *
   * @param serviceName 服务名称
   * @param tag         服务标记
   * @return 健康检查服务列表
   */
  public List<HealthService> getHealthServices(String serviceName, String tag) {
    HealthServicesRequest.Builder builder = HealthServicesRequest.newBuilder().setPassing(true)
        .setQueryParams(QueryParams.DEFAULT);
    if (StringUtils.isNotBlank(tag)) {
      builder.setTag(tag);
    }
    HealthServicesRequest request = builder.build();
    Response<List<HealthService>> healthyServices = client.getHealthServices(serviceName, request);
    return healthyServices.getValue();
  }

  /**
   * 随机获取服务.
   *
   * @param serviceName 服务名称
   * @param tag         服务标记
   * @return 健康检查服务
   */
  public HealthService getHealthService(String serviceName, String tag) {
    List<HealthService> healthServices = getHealthServices(serviceName, tag);
    if (healthServices.isEmpty()) {
      return null;
    }
    int index = RandomUtils.nextInt(0, healthServices.size());
    return healthServices.get(index);
  }

}
