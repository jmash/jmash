
package com.gitee.jmash.common.registry.consul;

import jakarta.enterprise.context.Dependent;
import org.eclipse.microprofile.config.inject.ConfigProperties;

/**
 * Consul 配置属性实体.
 *
 * @author CGD
 *
 */
@ConfigProperties(prefix = "jmash.consul")
@Dependent
public class ConsulProps {

  /** HOST. */
  private String host = "127.0.0.1";
  /** PORT. */
  private Integer port = 8500;
  /** ipAddress. */
  private String ipAddress = "127.0.0.1";
  /** 服务检查间隔. */
  private String interval = "10s";
  /** 服务地址. */
  private String serviceAddress = "127.0.0.1";
  /** 服务端口. */
  private int servicePort = 50051;

  public String getHost() {
    return host;
  }

  public void setHost(String host) {
    this.host = host;
  }

  public Integer getPort() {
    return port;
  }

  public void setPort(Integer port) {
    this.port = port;
  }

  public String getIpAddress() {
    return ipAddress;
  }

  public void setIpAddress(String ipAddress) {
    this.ipAddress = ipAddress;
  }

  public String getInterval() {
    return interval;
  }

  public void setInterval(String interval) {
    this.interval = interval;
  }

  public String getServiceAddress() {
    return serviceAddress;
  }

  public void setServiceAddress(String serviceAddress) {
    this.serviceAddress = serviceAddress;
  }

  public int getServicePort() {
    return servicePort;
  }

  public void setServicePort(int servicePort) {
    this.servicePort = servicePort;
  }

}
