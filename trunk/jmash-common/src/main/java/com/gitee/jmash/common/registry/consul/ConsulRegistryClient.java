
package com.gitee.jmash.common.registry.consul;

import com.ecwid.consul.v1.ConsulClient;
import com.ecwid.consul.v1.agent.model.NewCheck;
import com.ecwid.consul.v1.agent.model.NewService;
import com.gitee.jmash.common.config.AppProps;
import com.gitee.jmash.common.enums.ProtocolType;
import com.gitee.jmash.common.registry.RegistryClient;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.inject.Typed;
import jakarta.inject.Inject;
import java.util.Collections;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.microprofile.config.inject.ConfigProperties;

/**
 * Consul服务注册客户端.
 *
 * @author CGD
 *
 */
@Typed(RegistryClient.class)
//@ApplicationScoped
public class ConsulRegistryClient implements RegistryClient {

  private static Log log = LogFactory.getLog(ConsulRegistryClient.class);

  private ConsulClient client;

  @Inject
  @ConfigProperties
  private AppProps appProps;

  @Inject
  @ConfigProperties
  private ConsulProps consulProps;

  public ConsulRegistryClient() {
    super();
  }

  @PostConstruct
  public void init() {
    client = new ConsulClient(consulProps.getHost(), consulProps.getPort());
  }

  public String getCheckName() {
    return "service:" + appProps.getName();
  }

  /*
   * (non-Javadoc)
   * 
   * @see gcloud.core.RegistryClient#serviceRegister()
   */
  @Override
  public void serviceRegister() {
    try {
      NewService newService = new NewService();
      newService.setId(appProps.getName());
      newService.setName(appProps.getName());
      newService.setAddress(consulProps.getServiceAddress());
      newService.setPort(consulProps.getServicePort());
      newService.setTags(Collections.singletonList(appProps.getTag()));
      if (ProtocolType.GRPC.equals(appProps.getType())) {
        newService.setMeta(Collections.singletonMap("grpc", "true"));
      }
      client.agentServiceRegister(newService);
    } catch (Exception ex) {
      log.error("注册服务失败：", ex);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see gcloud.core.RegistryClient#checkRegister()
   */
  @Override
  public void checkRegister() {
    try {
      NewCheck newCheck = new NewCheck();
      newCheck.setServiceId(appProps.getName());
      newCheck.setId(getCheckName());
      newCheck.setName(getCheckName());

      if (ProtocolType.GRPC.equals(appProps.getType())) {
        String grcp = String.format("%s:%d", consulProps.getServiceAddress(),
            consulProps.getServicePort());
        newCheck.setGrpc(grcp);
      } else if (ProtocolType.HTTP.equals(appProps.getType())) {
        String http = String.format("%s:%d", consulProps.getServiceAddress(),
            consulProps.getServicePort());
        newCheck.setHttp(http);
      } else if (ProtocolType.TCP.equals(appProps.getType())) {
        String tcp = String.format("%s:%d", consulProps.getServiceAddress(),
            consulProps.getServicePort());
        newCheck.setTcp(tcp);
      }

      newCheck.setInterval(consulProps.getInterval());
      client.agentCheckRegister(newCheck);
    } catch (Exception ex) {
      log.error("注册服务检测失败：", ex);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see gcloud.core.RegistryClient#serviceDeregister()
   */
  @Override
  public void serviceDeregister() {
    client.agentServiceDeregister(appProps.getName());
  }

  /*
   * (non-Javadoc)
   * 
   * @see gcloud.core.RegistryClient#checkDeregister()
   */
  @Override
  public void checkDeregister() {
    client.agentCheckDeregister(getCheckName());
  }
}
