package com.gitee.jmash.common.security;

import com.gitee.jmash.common.utils.UUIDUtil;
import java.util.Set;
import java.util.UUID;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.jwt.Claims;
import org.eclipse.microprofile.jwt.JsonWebToken;

/** Default JmashPrincipal Impl. */
public class DefaultJmashPrincipal implements JmashPrincipal {

  JsonWebToken jsonWebToken;

  public static DefaultJmashPrincipal create(JsonWebToken jsonWebToken) {
    if (jsonWebToken == null) {
      return null;
    }
    return new DefaultJmashPrincipal(jsonWebToken);
  }

  protected DefaultJmashPrincipal(JsonWebToken jsonWebToken) {
    super();
    this.jsonWebToken = jsonWebToken;
  }

  @Override
  public String getName() {
    return jsonWebToken.getName();
  }

  @Override
  public UUID getNameUUID() {
    return UUIDUtil.fromString(jsonWebToken.getName());
  }

  @Override
  public String getSubject() {
    return jsonWebToken.getSubject();
  }

  @Override
  public String getTenant() {
    return jsonWebToken.getIssuer();
  }
  
  @Override
  public String getUnifiedId() {
    String unifiedId = jsonWebToken.getClaim(Claims.dest);
    if (StringUtils.isBlank(unifiedId)) {
      return this.getName();
    }
    return unifiedId;
  }

  @Override
  public String getStorage() {
    return jsonWebToken.getClaim(Claims.orig);
  }

  @Override
  public String getClientId() {
    return jsonWebToken.getClaim(Claims.azp);
  }

  @Override
  public Set<String> getScope() {
    return jsonWebToken.getGroups();
  }

}
