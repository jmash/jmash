package com.gitee.jmash.common.security;

import java.security.Principal;
import java.util.Set;
import java.util.UUID;

/**
 * Jmash Principal.
 */
public interface JmashPrincipal extends Principal {

  /** 登录UserID/UnifiedId (当前Session唯一识别用户身份). */
  @Override
  String getName();

  /** 登录UserID (当前Session唯一识别用户身份) For UUID . */
  UUID getNameUUID();
  
  /** 登录用户UnifiedId 生态唯一. */
  String getUnifiedId();

  /** 登录用户名/手机号/电子邮件/OpenId/UnionId等. */
  String getSubject();

  /** 当前用户登录租户. */
  String getTenant();

  /** 当前用户个人信息存储区. */
  String getStorage();

  /** 当前登录ClientID. */
  String getClientId();

  /** 授权范围. */
  Set<String> getScope();

}
