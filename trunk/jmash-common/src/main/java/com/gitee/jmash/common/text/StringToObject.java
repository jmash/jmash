
package com.gitee.jmash.common.text;

import com.crenjoy.proto.beanutils.ProtoConvertUtils;
import java.lang.reflect.Array;

/**
 * String to Object 工具类.
 *
 * @author CGD
 *
 */
public class StringToObject {

  /**
   * String to Object[].
   *
   * @param type Object Class Type
   * @param s    String值
   * @return Object[]
   */
  @SuppressWarnings("unchecked")
  public static <E> E[] toArray(Class<E> type, String[] s) {
    Object value = Array.newInstance(type, s.length);
    for (int i = 0, icount = s.length; i < icount; i++) {
      Array.set(value, i, to(type, s[i]));
    }
    return (E[]) value;
  }

  /**
   * String to Object.
   *
   * @param type Object Class Type
   * @param s    String值
   * @return Object
   */
  public static <E> E to(Class<E> type, String s) {
    if (s == null || type == null) {
      return null;
    }
    return ProtoConvertUtils.convert(s, type);
  }
}
