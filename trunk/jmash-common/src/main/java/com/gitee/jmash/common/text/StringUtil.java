
package com.gitee.jmash.common.text;

import java.io.File;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * StringUtil提供命名规则的转化.
 *
 * @author cgd email:cgd321@sxinfo.net
 * @version V1.0 2009-2-12
 */
public class StringUtil {

  private static Log log = LogFactory.getLog(StringUtil.class);
  
  /** 转化为驼峰式(oper_name 转 operName). */
  public static String underlineToLowerCaseFirstCamel(String oldString) {
    String newString = underlineToCamel(oldString);
    return lowerCaseFirst(newString);
  }

  /**
   * 转化为驼峰式(oper_name 转 OperName). 
   */
  public static String underlineToCamel(String oldString) {
    if (null == oldString || "".equals(oldString.trim())) {
      return "";
    }
    String[] subs = oldString.split("_");
    if (1 == subs.length) {
      oldString = oldString.replaceAll("_", "");
      return String.valueOf(oldString.charAt(0)).toUpperCase() + oldString.substring(1);
    }
    StringBuffer newString = new StringBuffer();
    for (String sub : subs) {
      if (sub.equals("")) {
        continue;
      }
      try {
        newString
            .append(String.valueOf(sub.charAt(0)).toUpperCase() + sub.substring(1).toLowerCase());
      } catch (Exception e) {
        log.info(oldString);

      }
    }
    return newString.toString();
  }

  /**
   * 转化为下划线式.
   */
  public static String camelToUnderline(String oldString) {
    if (null == oldString || "".equals(oldString.trim())) {
      return "";
    }
    StringBuffer newString = new StringBuffer();
    for (int i = 0; i < oldString.length(); i++) {
      String charStr = oldString.substring(i, i + 1);
      if (i > 0 && charStr.toUpperCase().equals(charStr)) {
        newString.append("_");
        newString.append(charStr);
      } else {
        newString.append(charStr);
      }
    }
    return newString.toString().toLowerCase();
  }

  /**
   * 首字母小写.
   */
  public static String lowerCaseFirst(String string) {
    String s = String.valueOf(string.charAt(0)).toLowerCase();
    s = s + string.substring(1);
    return s;
  }

  /**
   * 首字母大写.
   */
  public static String upperCaseFirst(String string) {
    String s = String.valueOf(string.charAt(0)).toUpperCase();
    s = s + string.substring(1);
    return s;
  }

  /**
   * 包路径.
   */
  public static String packageDir(String packageName) {
    return packageName.replace('.', File.separatorChar);
  }
}
