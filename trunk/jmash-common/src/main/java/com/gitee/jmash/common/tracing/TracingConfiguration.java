
package com.gitee.jmash.common.tracing;

import brave.Tracing;
import brave.grpc.GrpcTracing;
import brave.propagation.CurrentTraceContext;
import brave.propagation.CurrentTraceContext.ScopeDecorator;
import brave.propagation.ThreadLocalCurrentTraceContext;
import brave.rpc.RpcTracing;
import brave.sampler.Sampler;
import io.grpc.ClientInterceptor;
import io.grpc.ServerInterceptor;
import java.io.IOException;
import java.util.logging.Logger;
import zipkin2.reporter.Sender;
import zipkin2.reporter.brave.AsyncZipkinSpanHandler;
import zipkin2.reporter.urlconnection.URLConnectionSender;

/**
 * Tracing配置.
 *
 * @author CGD
 *
 */
public final class TracingConfiguration {

  final RpcTracing rpcTracing;

  public static TracingConfiguration create(String defaultServiceName, ZipkinProps zipkinProps) {
    Tracing tracing = TracingFactory.tracing(defaultServiceName, zipkinProps);
    return new TracingConfiguration(RpcTracing.create(tracing));
  }

  TracingConfiguration(RpcTracing rpcTracing) {
    this.rpcTracing = rpcTracing;
  }

  public ClientInterceptor clientInterceptor() {
    return GrpcTracing.create(rpcTracing).newClientInterceptor();
  }

  public ServerInterceptor serverInterceptor() {
    return GrpcTracing.create(rpcTracing).newServerInterceptor();
  }

  static final class TracingFactory {

    /**
     * Controls aspects of tracing such as the service name that shows up in the UI.
     */
    static Tracing tracing(String serviceName, ZipkinProps zipkinProps) {
      // 是否启动
      Sampler sampler = Sampler.NEVER_SAMPLE;
      if (zipkinProps.getEnabled()) {
        sampler = Sampler.create(zipkinProps.getPercentage());
      }
      return Tracing.newBuilder().localServiceName(serviceName)
          .addSpanHandler(spanHandler(sender(zipkinProps))).sampler(sampler).build();
    }

    /** Propagates trace context between threads. */
    static CurrentTraceContext currentTraceContext(ScopeDecorator correlationScopeDecorator) {
      return ThreadLocalCurrentTraceContext.newBuilder()
          .addScopeDecorator(correlationScopeDecorator).build();
    }

    /** Configuration for how to send spans to Zipkin. */
    static Sender sender(ZipkinProps zipkinProps) {
      return URLConnectionSender.create(zipkinProps.getBaseUrl() + "/api/v2/spans");
    }

    /** Configuration for how to buffer spans into messages for Zipkin. */
    static AsyncZipkinSpanHandler spanHandler(Sender sender) {
      final AsyncZipkinSpanHandler spanHandler = AsyncZipkinSpanHandler.create(sender);

      Runtime.getRuntime().addShutdownHook(new Thread(() -> {
        spanHandler.close(); // Make sure spans are reported on shutdown
        try {
          sender.close(); // Release any network resources used to send spans
        } catch (IOException e) {
          Logger.getAnonymousLogger().warning("error closing trace sender: " + e.getMessage());
        }
      }));

      return spanHandler;
    }

    private TracingFactory() {
    }
  }
}
