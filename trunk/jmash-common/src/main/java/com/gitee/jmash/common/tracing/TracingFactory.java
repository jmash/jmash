
package com.gitee.jmash.common.tracing;

import com.gitee.jmash.common.config.AppProps;
import jakarta.enterprise.inject.spi.CDI;
import org.eclipse.microprofile.config.inject.ConfigProperties;

/**
 * Tracing工厂类.
 *
 * @author CGD
 *
 */
public class TracingFactory {

  private static TracingConfiguration tracing = null;

  /**
   * 获取默认服务Tracing配置.
   *
   * @return Tracing配置
   */
  public static TracingConfiguration getTracing() {
    if (null == tracing) {
      AppProps appProps = CDI.current().select(AppProps.class, ConfigProperties.Literal.NO_PREFIX)
          .get();
      
      ZipkinProps zipkinProps = CDI.current()
          .select(ZipkinProps.class, ConfigProperties.Literal.NO_PREFIX).get();
      tracing = TracingConfiguration.create(appProps.getName(), zipkinProps);
    }
    return tracing;
  }

  /**
   * 通过服务名称获取Tracing配置.
   *
   * @param serviceName 服务名称
   * @return Tracing配置
   */
  public static TracingConfiguration getTracing(String serviceName, ZipkinProps zipkinProps) {
    if (null == tracing) {
      tracing = TracingConfiguration.create(serviceName, zipkinProps);
    }
    return tracing;
  }

}
