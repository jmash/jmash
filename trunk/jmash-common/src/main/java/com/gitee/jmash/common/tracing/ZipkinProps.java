
package com.gitee.jmash.common.tracing;

import jakarta.enterprise.context.ApplicationScoped;
import org.eclipse.microprofile.config.inject.ConfigProperties;

/**
 * Zipkin日志配置属性.
 *
 * @author CGD
 *
 */
@ConfigProperties(prefix = "jmash.zipkin")
@ApplicationScoped
public class ZipkinProps {

  private String baseUrl = "http://dev.jmash.crenjoy.com";
  // 抽检日志比例
  private Float percentage = 1f;
  // 启用
  private Boolean enabled = true;

  public String getBaseUrl() {
    return baseUrl;
  }

  public void setBaseUrl(String baseUrl) {
    this.baseUrl = baseUrl;
  }

  public Float getPercentage() {
    return percentage;
  }

  public void setPercentage(Float percentage) {
    this.percentage = percentage;
  }

  public Boolean getEnabled() {
    return enabled;
  }

  public void setEnabled(Boolean enabled) {
    this.enabled = enabled;
  }

}
