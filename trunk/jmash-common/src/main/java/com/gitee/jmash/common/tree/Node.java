package com.gitee.jmash.common.tree;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

/**
 * 树节点.
 *
 * @author cgd
 * @version V1.0
 * @param <T> 节点信息
 * @param <F> 节点主键
 */
public class Node<T, F> {

  /** 节点主键. */
  private F key;
  /** 节点信息. */
  private T entity;
  /** 父节点. */
  private Node<T, F> parent;
  /** 排序. */
  private int order;
  /** is ROOT. */
  private boolean isRoot = false;
  /** 节点的孩子. */
  private List<Node<T, F>> childs = new ArrayList<Node<T, F>>();


  protected Node(F key, boolean isRoot) {
    this(key, null, null, 0);
    this.isRoot = isRoot;
  }

  protected Node(F key, T entity, Node<T, F> parent) {
    this(key, entity, parent, 0);
  }

  /** 创建树节点. */
  protected Node(F key, T entity, Node<T, F> parent, int order) {
    super();
    this.key = key;
    this.entity = entity;
    this.parent = parent;
    this.order = order;
    if (this.parent != null) {
      this.parent.addChild(this);
    }
  }

  /**
   * 获取父序列.
   */
  public String getParentSeq() {
    if (this.key == null || this.parent == null || this.parent.key == null) {
      return "";
    }
    return this.parent.getParentSeq() + this.parent.key + ",";
  }

  /**
   * 获取孩子序列.
   */
  public String getChildSeq() {
    if (this.isLeaf()) {
      return "";
    }
    StringBuilder temp = new StringBuilder();
    for (Node<T, F> childNode : this.childs) {
      temp.append(childNode.getKey() + ",");
      temp.append(childNode.getChildSeq());
    }
    return temp.toString();
  }

  /**
   * 返回parentKey.
   */
  public F getParentKey() {
    if (this.parent != null) {
      return this.parent.key;
    }
    return null;
  }

  /**
   * 移出Node.
   */
  public void remove() {
    if (this.key == null || this.parent == null) {
      return;
    }
    this.parent.childs.remove(this);
    if (this.childs != null && this.childs.size() > 0) {
      for (Node<T, F> child : this.childs) {
        child.setParent(this.parent);
        this.parent.addChild(child);
      }
    }
  }

  /** 将该节点移动到父节点下. */
  public void changeNodeParent(Node<T, F> parent) {
    // 该父节点不是该节点的孩子
    if (parent.entity != null) {
      List<T> childlist = this.getChildsListExclude(key, new ArrayList<T>());
      if (childlist.contains(parent.entity)) {
        return;
      }
    }
    // 修改
    this.parent.childs.remove(this);
    this.parent = parent;
    this.parent.addChild(this);
  }

  /** 是否是叶子. */
  public boolean isLeaf() {
    return childs == null || childs.size() == 0;
  }

  /** 是否是根节点. */
  public boolean isRoot() {
    return isRoot;
  }

  /** 返回深度. */
  public int getDepth() {
    if (this.getKey() == null || this.parent == null) {
      return 0;
    }
    if (this.parent.getKey() == null) {
      return 1;
    }
    return this.parent.getDepth() + 1;
  }

  /** 增加孩子. */
  private void addChild(Node<T, F> child) {
    if (childs == null) {
      childs = new ArrayList<Node<T, F>>();
    }
    int i;
    for (i = 0; i < childs.size(); i++) {
      if (childs.get(i).order > child.order) {
        childs.add(i, child);
        break;
      }
    }
    if (i == childs.size()) {
      childs.add(child);
    }
  }

  /** 返回孩子List. */
  public List<Node<T, F>> getAllChildsNodeList(List<Node<T, F>> lists) {
    if (lists == null) {
      lists = new ArrayList<Node<T, F>>();
    }
    if (this.isLeaf()) {
      return lists;
    }
    for (Node<T, F> child : this.getChilds()) {
      lists.add(child);
      child.getAllChildsNodeList(lists);
    }
    return lists;
  }

  /** 返回孩子List. */
  public List<T> getAllChildsList(List<T> lists) {
    if (lists == null) {
      lists = new ArrayList<T>();
    }
    if (this.isLeaf()) {
      return lists;
    }
    for (Node<T, F> child : this.getChilds()) {
      lists.add(child.getEntity());
      child.getAllChildsList(lists);
    }
    return lists;
  }

  /**
   * 返回孩子List,不包括该节点和它的子孙.
   *
   * @param key 节点Key
   * @param lists 列表
   */
  public List<T> getChildsListExclude(F key, List<T> lists) {
    if (lists == null) {
      lists = new ArrayList<T>();
    }
    if (this.isLeaf() || key.equals(this.key)) {
      return lists;
    }
    for (Node<T, F> child : this.getChilds()) {
      if (!key.equals(child.getKey())) {
        lists.add(child.getEntity());
        child.getChildsListExclude(key, lists);
      }
    }
    return lists;
  }

  /**
   * 返回父亲List.
   */
  public List<T> getAllParentsList(List<T> lists) {
    if (lists == null) {
      lists = new ArrayList<T>();
    }
    if (this.isRoot() || this.parent.isRoot()) {
      return lists;
    }
    lists.add(0, this.getParent().getEntity());
    this.getParent().getAllParentsList(lists);
    return lists;
  }

  /**
   * 移动本节点.
   *
   * @param up 向上，或向下
   */
  public boolean move(boolean up) {
    if (!this.isRoot() && this.parent != null) {
      List<Node<T, F>> nodes = this.parent.getChilds();
      int index = nodes.indexOf(this);
      if (index < 0 || (up && index == 0) || (!up && index == nodes.size() - 1)) {
        return false;
      }
      int swap = up ? index - 1 : index + 1;
      // 交换ListOrder
      int temp = nodes.get(swap).order;
      nodes.get(swap).order = this.order;
      this.order = temp;
      // 交换位置
      nodes.set(index, nodes.get(swap));
      nodes.set(swap, this);
      return true;
    }
    return false;
  }

  /**
   * 返回他的listChilds.
   */
  public List<T> getListChilds() {
    if (this.childs != null) {
      List<T> list = new ArrayList<T>();
      for (Node<T, F> node : this.childs) {
        list.add(node.entity);
      }
      return list;
    }
    return Collections.emptyList();
  }

  /**
   * 获取节点下的叶子节点.
   */
  public List<Node<T, F>> getAllLeafNodes(List<Node<T, F>> lists) {
    if (lists == null) {
      lists = new ArrayList<Node<T, F>>();
    }
    if (this.isLeaf()) {
      if (!this.isRoot()) {
        lists.add(this);
      }
      return lists;
    }
    for (Node<T, F> child : this.getChilds()) {
      child.getAllLeafNodes(lists);
    }
    return lists;
  }

  /**
   * 获取节点下的叶子节点.
   */
  public List<T> getAllLeafs(List<T> lists) {
    if (lists == null) {
      lists = new ArrayList<T>();
    }
    if (this.isLeaf()) {
      if (!this.isRoot()) {
        lists.add(this.getEntity());
      }
      return lists;
    }
    for (Node<T, F> child : this.getChilds()) {
      child.getAllLeafs(lists);
    }
    return lists;
  }

  /**
   * 获取孩子深度.
   */
  public int getChildsDepth(int depth) {
    if (this.isLeaf()) {
      return depth;
    }
    // 最大深度
    int maxDepth = depth + 1;
    for (Node<T, F> child : this.getChilds()) {
      // 获取孩子深度
      int childDepth = child.getChildsDepth(depth + 1);
      if (childDepth > maxDepth) {
        maxDepth = childDepth;
      }
    }
    return maxDepth;
  }



  /**
   * 获取父序列Key.
   */
  public String getParentSeqKey() {
    if (this.isRoot()) {
      return "0";
    }
    return this.parent.getParentSeqKey() + "," + this.key;
  }

  /**
   * 获取父序列Key.
   */
  public String getChildsJson() {
    if (this.isLeaf()) {
      return "{}";
    }
    String[] args = new String[this.getChilds().size()];
    for (int i = 0; i < this.getChilds().size(); i++) {
      Node<T, F> node = this.getChilds().get(i);
      args[i] = String.format("%s:'%s'", node.key, node.getEntity().toString());
    }
    return "{" + StringUtils.join(args, ",") + "}";
  }

  /**
   * 获取Json.
   */
  public StringBuilder getLinkageJson(StringBuilder json) {
    if (this.isLeaf() && this.isRoot()) {
      return json;
    }
    if (this.isLeaf()) {
      return json;
    }
    json.append(String.format("'%s':%s", this.getParentSeqKey(), getChildsJson()));

    for (Node<T, F> child : this.getChilds()) {
      if (!child.isLeaf()) {
        json.append(",\n");
        json = child.getLinkageJson(json);
      }
    }
    return json;
  }



  /**
   * 逐级移除叶子.
   */
  public void removeLeaf() {
    if (this.isLeaf()) {
      Node<T, F> selfParent = this.parent;
      this.remove();
      if (!selfParent.isRoot()) {
        selfParent.removeLeaf();
      }
    }
  }

  /**
   * 逐级移除节点.
   */
  public void removeIteration() {
    List<Node<T, F>> childrens = this.getChilds();
    if (null != childrens) {
      for (int i = childrens.size() - 1; i >= 0; i--) {
        Node<T, F> node = childrens.get(i);
        if (null != node) {
          node.removeIteration();
        }
      }
    }
    this.remove();
  }

  /**
   * 输出显示.
   */
  @Override
  public String toString() {
    StringBuilder temp = new StringBuilder();
    for (int i = 0; i < this.getDepth(); i++) {
      temp.append("    ");
    }
    temp.append(String.format("ID:%s parendID:%s Depth:%d Order:%d  \n ", this.getKey(),
        this.getParent() != null ? this.getParent().getKey() : "", this.getDepth(),
        this.getOrder()));
    if (this.isLeaf()) {
      return temp.toString();
    }
    for (Node<T, F> child : this.getChilds()) {
      temp.append(child.toString());
    }
    return temp.toString();
  }

  public List<Node<T, F>> getChilds() {
    return childs;
  }

  public F getKey() {
    return key;
  }

  public void setKey(F key) {
    this.key = key;
  }

  public T getEntity() {
    return entity;
  }

  public void setEntity(T entity) {
    this.entity = entity;
  }

  public Node<T, F> getParent() {
    return parent;
  }

  public void setParent(Node<T, F> parent) {
    this.parent = parent;
  }

  public int getOrder() {
    return order;
  }

  public void setOrder(int order) {
    this.order = order;
  }
}
