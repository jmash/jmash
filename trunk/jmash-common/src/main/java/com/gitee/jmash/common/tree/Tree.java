package com.gitee.jmash.common.tree;

import com.gitee.jmash.common.utils.UUIDUtil;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.apache.commons.logging.LogFactory;

/**
 * 树状模型 (Node,Key).
 *
 * @author cgd
 * @version V1.0
 */
public class Tree<T, F> {

  private F rootKey = null;

  private Node<T, F> rootNode = null;

  private Map<F, Node<T, F>> nodes = new HashMap<F, Node<T, F>>();

  /** 根节点. */
  public Node<T, F> getRoot() {
    if (null == rootNode) {
      rootNode = new Node<T, F>(rootKey, true);
    }
    return rootNode;
  }

  public void setRootKey(F rootKey) {
    this.rootKey = rootKey;
    this.rootNode = null;
  }

  /** 添加节点. */
  @SuppressWarnings("unchecked")
  public void add(F key, T entity, F parentKey, int order) {
    Node<T, F> parent = null;
    if (parentKey == null) {
      parent = getRoot();
    } else if (parentKey.equals(this.rootKey)) {
      parent = getRoot();
    } else if (parentKey instanceof String && UUIDUtil.emptyUUIDStr().equals(parentKey)) {
      setRootKey((F) UUIDUtil.emptyUUIDStr());
      parent = getRoot();
    } else if (parentKey instanceof UUID && UUIDUtil.emptyUUID().equals(parentKey)) {
      setRootKey((F) UUIDUtil.emptyUUID());
      parent = getRoot();
    } else if (parentKey instanceof Integer && 0 == (Integer) parentKey) {
      setRootKey((F) Integer.valueOf(0));
      parent = getRoot();
    } else if (parentKey instanceof Long && 0 == (Long) parentKey) {
      setRootKey((F) Long.valueOf(0L));
      parent = getRoot();
    } else {
      if (nodes.containsKey(parentKey)) {
        parent = nodes.get(parentKey);
      } else {
        LogFactory.getLog(Tree.class).error("没有找到它的父亲 parent" + parentKey);
        parent = getRoot();
      }
    }
    Node<T, F> node = new Node<T, F>(key, entity, parent, order);
    nodes.put(key, node);
  }

  /** 返回孩子. */
  public List<Node<T, F>> getChilds() {
    return getRoot().getChilds();
  }

  /**
   * 返回孩子List.
   *
   * @param key 父ID
   * @return 列表
   */
  public List<T> getChildsList(F key) {
    Node<T, F> curNode = this.getNode(key);
    List<Node<T, F>> list = curNode.getChilds();
    List<T> retList = new ArrayList<T>();
    if (list != null && list.size() > 0) {
      for (Node<T, F> node : list) {
        retList.add(node.getEntity());
      }
    }
    return retList;
  }

  @Override
  public String toString() {
    return getRoot().toString();
  }

  /**
   * 根据key 获取Node.
   *
   * @param key 主键
   * @return 节点
   */
  public Node<T, F> getNode(F key) {
    if (key == null) {
      return getRoot();
    } else if (key instanceof UUID && UUIDUtil.emptyUUID().equals(key)) {
      return getRoot();
    } else if (key instanceof Integer && 0 == (Integer) key) {
      return getRoot();
    } else if (key instanceof Long && 0 == (Long) key) {
      return getRoot();
    }
    return nodes.get(key);
  }

  /**
   * 获取实体.
   *
   * @param key 实体主键
   * @return 实体
   */
  public T getEntity(F key) {
    if (key == null) {
      return null;
    } else if (key instanceof UUID && UUIDUtil.emptyUUID().equals(key)) {
      return null;
    } else if (key instanceof Integer && 0 == (Integer) key) {
      return null;
    } else if (key instanceof Long && 0 == (Long) key) {
      return null;
    }
    if (nodes.get(key) != null) {
      return nodes.get(key).getEntity();
    }
    return null;
  }

  /**
   * 删除节点.
   *
   * @param key 节点主键
   * @return 移除的节点
   */
  public Node<T, F> removeNode(F key) {
    if (!nodes.containsKey(key)) {
      return null;
    }
    Node<T, F> node = nodes.get(key);
    node.remove();
    nodes.remove(key);
    return node;
  }

  /**
   * 移动节点到某个父节点下.
   *
   * @param key 节点主键
   * @param parentKey 父主键
   */
  public void changeNodeParent(F key, F parentKey) {
    if (!nodes.containsKey(key)) {
      return;
    }
    Node<T, F> parent = nodes.containsKey(parentKey) ? nodes.get(parentKey) : getRoot();
    nodes.get(key).changeNodeParent(parent);
  }

  /**
   * 返回排序好的list.
   *
   * @return 全部
   */
  public List<T> getAllList() {
    return getRoot().getAllChildsList(new ArrayList<T>());
  }

  /**
   * 节点列表.
   *
   * @return 节点列表
   */
  public List<Node<T, F>> getAllChildsNodeList() {
    return getRoot().getAllChildsNodeList(new ArrayList<Node<T, F>>());
  }

  /**
   * 获取孩子List.
   *
   * @param key 父主键
   * @return 列表
   */
  public List<T> getAllChildsList(F key) {
    Node<T, F> node = nodes.get(key);
    if (node == null) {
      return Collections.emptyList();
    }
    return node.getAllChildsList(new ArrayList<T>());
  }

  /**
   * 本身以及孩子List.
   *
   * @param key 自己主键
   * @return 列表
   */
  public List<T> getSelfChildsList(F key) {
    Node<T, F> node = nodes.get(key);
    if (node == null) {
      return Collections.emptyList();
    }
    List<T> list = new ArrayList<T>();
    list.add(node.getEntity());
    return node.getAllChildsList(list);
  }


  /**
   * 获取所有孩子的父亲.
   *
   * @param key 主键
   * @return 列表
   */
  public List<T> getAllParentsList(F key) {
    Node<T, F> node = nodes.get(key);
    if (node == null) {
      return Collections.emptyList();
    }
    return node.getAllParentsList(new ArrayList<T>());
  }

  /**
   * 获取包括父亲自己的实体.
   *
   * @param key 主键
   * @return 列表
   */
  public List<T> getSelfParentsList(F key) {
    Node<T, F> node = nodes.get(key);
    if (node == null) {
      return Collections.emptyList();
    }
    List<T> list = node.getAllParentsList(new ArrayList<T>());
    list.add(node.getEntity());
    return list;
  }

  /**
   * 返回所有叶子节点.
   *
   * @return 列表
   */
  public List<T> getAllLeafs() {
    return getRoot().getAllLeafs(new ArrayList<T>());
  }

  /**
   * 获取深度.
   *
   * @return 深度
   */
  public int getChildsDepth() {
    return getRoot().getChildsDepth(0);
  }

  /**
   * 移除叶子 会迭代移除父节点，适合文章栏目树等.
   *
   * @param key 叶子主键
   */
  public void removeLeaf(F key) {
    Node<T, F> node = nodes.get(key);
    if (node != null) {
      node.removeLeaf();
    }
  }

  /**
   * 移除移除.
   *
   * @param key 依次移除
   */
  public void removeIteration(F key) {
    Node<T, F> node = nodes.get(key);
    if (node != null) {
      node.removeIteration();
    }
  }

  /**
   * 返回节点List 不包括该节点和它的子孙.
   *
   * @param key 节点Key
   * @return 列表.
   */
  public List<T> getListExclude(F key) {
    return getRoot().getChildsListExclude(key, new ArrayList<T>());
  }

  /**
   * 移动该节点后，该节点兄弟的重新排序.
   *
   * @param key 主键.
   * @param up 上移
   * @return 列表
   */
  public List<T> moveOrder(F key, boolean up) {
    Node<T, F> node = nodes.get(key);
    if (node == null) {
      return Collections.emptyList();
    }
    if (node.move(up)) {
      return node.getParent().getListChilds();
    }
    return Collections.emptyList();
  }

  /**
   * 获取Json.
   *
   * @return json效果
   */
  public String getLinkageJson() {
    return getRoot().getLinkageJson(new StringBuilder()).toString();
  }

}
