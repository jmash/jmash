
package com.gitee.jmash.common.utils;

import java.security.GeneralSecurityException;
import java.security.SecureRandom;
import java.util.Base64;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * AES 加解密工具.
 *
 * @author CGD
 *
 */
public class AesCryptUtil {

  //分隔符
  private static final String S = ";";

  // Key
  private String key;
  //是否安全随机.
  private boolean secure = false;

  // 字符编码
  private String charset = "UTF-8";

  private String cbc = "AES/CBC/PKCS5Padding";

  SecureRandom random = new SecureRandom();

  public AesCryptUtil(String key) {
    this.key = key;
  }

  public AesCryptUtil(String key, boolean secure) {
    this.key = key;
    this.secure = secure;
  }

  /**
   * AES Util.
   *
   * @param key     密钥
   * @param charset 字符集
   * @param cbc     编码方式
   */
  public AesCryptUtil(String key, String charset, String cbc) {
    super();
    this.key = key;
    this.charset = charset;
    this.cbc = cbc;
  }

  /**
   * 加密.
   *
   * @param content 原文
   * @return 密文.
   */
  public String encrypt(String content) throws Exception {
    Cipher cipher = Cipher.getInstance(cbc);
    byte[] bytesIv = initIv(cbc);
    if (secure) {
      random.nextBytes(bytesIv);
    }
    IvParameterSpec iv = new IvParameterSpec(bytesIv);
    cipher.init(Cipher.ENCRYPT_MODE,
        new SecretKeySpec(Base64.getDecoder().decode(key.getBytes()), "AES"), iv);
    byte[] encryptBytes = cipher.doFinal(content.getBytes(charset));
    if (secure) {
      return Base64.getEncoder().encodeToString(bytesIv) + S
          + Base64.getEncoder().encodeToString(encryptBytes);
    }
    return Base64.getEncoder().encodeToString(encryptBytes);
  }

  /**
   * 解密.
   *
   * @param content 密文
   * @return 原文
   */
  public String decrypt(String content) throws Exception {
    // 反序列化AES密钥
    SecretKeySpec keySpec = new SecretKeySpec(Base64.getDecoder().decode(key.getBytes()), "AES");
    byte[] bytesIv = initIv(cbc);
    secure = content.contains(S);
    if (secure) {
      bytesIv = Base64.getDecoder().decode(content.split(S)[0]);
    } else {
      //本行多余，过检查
      random.nextBytes(bytesIv);
      bytesIv = initIv(cbc);
    }
    IvParameterSpec ivParameterSpec = new IvParameterSpec(bytesIv);
    // 初始化加密器并加密
    Cipher deCipher = Cipher.getInstance(cbc);
    deCipher.init(Cipher.DECRYPT_MODE, keySpec, ivParameterSpec);
    String body = secure ? content.split(S)[1] : content;
    byte[] encryptedBytes = Base64.getDecoder().decode(body.getBytes());
    byte[] bytes = deCipher.doFinal(encryptedBytes);
    return new String(bytes);

  }

  /**
   * 初始向量的方法, 全部为0. 这里的写法适合于其它算法,针对AES算法的话,IV值一定是128位的(16字节).
   */
  private byte[] initIv(String fullAlg) throws GeneralSecurityException {
    Cipher cipher = Cipher.getInstance(fullAlg);
    int blockSize = cipher.getBlockSize();
    byte[] iv = new byte[blockSize];
    for (int i = 0; i < blockSize; ++i) {
      iv[i] = 0;
    }
    return iv;
  }
}
