
package com.gitee.jmash.common.utils;

import java.util.Base64;
import org.apache.commons.lang3.StringUtils;

/**
 * Basic Authentication Util.
 *
 * @author CGD
 *
 */
public class BasicAuthzUtil {

  /**
   * Decodes the Basic Authentication into a username and password.
   *
   * @param authentication {@link String} containing the encoded header value.
   *                       e.g. "Basic dXNlcm5hbWU6cGFzc3dvcmQ="
   * @return if the header could be decoded into a non null username and password
   *         or null.
   */
  public static String[] decodeBasicAuthz(String authentication) {
    if (StringUtils.isBlank(authentication)) {
      return null;
    }
    String[] tokens = authentication.split(" ");
    if (tokens.length != 2) {
      return null;
    }
    String authType = tokens[0];
    if (!"basic".equalsIgnoreCase(authType)) {
      return null;
    }
    String encodedCreds = tokens[1];
    return decodeBase64EncodedCredentials(encodedCreds);
  }

  /**
   * Encode a username and password to Basic Authentication.
   *
   * @return e.g. "Basic dXNlcm5hbWU6cGFzc3dvcmQ="
   */
  public static String encodeBasicAuthz(String username, String password) {
    if (StringUtils.isBlank(username) || StringUtils.isBlank(password)) {
      return null;
    }
    String creds = username + ":" + password;
    String encodedCreds = Base64.getEncoder().encodeToString(creds.getBytes());
    return "basic " + encodedCreds;
  }

  private static String[] decodeBase64EncodedCredentials(String encodedCreds) {
    String decodedCreds = new String(Base64.getDecoder().decode(encodedCreds));
    String[] creds = decodedCreds.split(":", 2);
    if (creds.length != 2) {
      return null;
    }
    if (!StringUtils.isBlank(creds[0]) && !StringUtils.isBlank(creds[1])) {
      return creds;
    }
    return null;
  }

}
