
package com.gitee.jmash.common.utils;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import org.apache.commons.logging.LogFactory;

/**
 * Class Scanner Util.
 * https://gist.github.com/danielemaddaluno/302b6226613d08bb684517e36806f96a
 */
public class ClassScannerUtil {

  /**
   * Scanner Annotated Classes.
   *
   * @param pack            包
   * @param subClass        子类
   * @param annotationClass 注解
   * 
   */
  public static List<Class<?>> scanAnnotatedClasses(Package pack, boolean subClass,
      Class<? extends Annotation> annotationClass) {
    try {
      List<Class<?>> classes = scanClasses(pack, subClass);
      List<Class<?>> annotatedClasses = new ArrayList<Class<?>>();

      for (Class<?> clazz : classes) {
        if (clazz.isAnnotationPresent(annotationClass)) {
          annotatedClasses.add(clazz);
        }
      }
      return annotatedClasses;
    } catch (Exception ex) {
      LogFactory.getLog(ClassScannerUtil.class).warn(ex);
      return Collections.emptyList();
    }
  }

  /**
   * Scanner Package Classes.
   *
   * @param pack     包
   * @param subClass 子类
   */
  public static List<Class<?>> scanClasses(Package pack, boolean subClass) {
    try {
      final ClassLoader cld = Thread.currentThread().getContextClassLoader();
      return scanClasses(pack, subClass, cld);
    } catch (Exception ex) {
      LogFactory.getLog(ClassScannerUtil.class).warn(ex);
      return Collections.emptyList();
    }
  }

  /**
   * Scanner Package Classes.
   *
   * @param pack     包
   * @param subClass 子类
   * @param cld      类加载
   */
  public static List<Class<?>> scanClasses(Package pack, boolean subClass, ClassLoader cld)
      throws ClassNotFoundException, IOException {
    String pckgUrl = pack.getName().replace('.', '/');
    final Enumeration<URL> resources = cld.getResources(pckgUrl);
    final List<Class<?>> classes = new ArrayList<Class<?>>();
    for (URL url = null; resources.hasMoreElements()
        && ((url = resources.nextElement()) != null);) {
      URLConnection connection = url.openConnection();
      if (connection instanceof JarURLConnection) {
        scanJarClass((JarURLConnection) connection, pckgUrl, subClass, classes);
      } else {
        scanDirClass(new File(url.getPath()), pack.getName(), subClass, classes);
      }
    }
    return classes;
  }

  /**
   * 扫描Jar中的类.
   *
   * @param connection jar包
   * @param packageUrl 包路径
   * @param subClass   是否扫描子包类
   * @param classes    扫描结果类列表
   */
  private static void scanJarClass(JarURLConnection connection, String packageUrl, boolean subClass,
      List<Class<?>> classes) throws ClassNotFoundException, IOException {
    final JarFile jarFile = connection.getJarFile();
    final Enumeration<JarEntry> entries = jarFile.entries();

    for (JarEntry jarEntry = null; entries.hasMoreElements()
        && ((jarEntry = entries.nextElement()) != null);) {
      String name = jarEntry.getName();
      if (!name.endsWith(".class")) {
        continue;
      }
      if (!name.startsWith(packageUrl)) {
        continue;
      }
      // 不包含子类
      if (!subClass && name.lastIndexOf('/') != packageUrl.length()) {
        continue;
      }
      name = name.substring(0, name.length() - 6).replace('/', '.');
      classes.add(Class.forName(name));
    }
  }

  /**
   * 扫描文件目录类.
   *
   * @param directory 文件目录
   * @param pckgname  包名称
   * @param subClass  是否扫描子包类
   * @param classes   扫描结果类列表
   */
  private static void scanDirClass(File directory, String pckgname, boolean subClass,
      List<Class<?>> classes) throws ClassNotFoundException {
    if (!directory.exists() || !directory.isDirectory()) {
      return;
    }
    final File[] files = directory.listFiles();
    if (files == null || files.length == 0) {
      return;
    }
    for (final File file : files) {
      if (subClass && file.isDirectory()) {
        scanDirClass(file, pckgname + "." + file.getName(), subClass, classes);
      }
      String fileName = file.getName();
      if (file.isFile() && fileName.endsWith(".class")) {
        classes.add(Class.forName(pckgname + '.' + fileName.substring(0, fileName.length() - 6)));
      }
    }
  }

}