
package com.gitee.jmash.common.utils;

import java.util.Arrays;
import java.util.Comparator;

/**
 * 用于比较的工具.
 *
 * @author CGD
 *
 */
public class ComparatorUtil {

  private static Comparator<Object> comparator = new ComparatorImpl();

  public static Comparator<Object> getComparator() {
    return ComparatorUtil.comparator;
  }

  public static int compare(Object o1, Object o2) {
    return comparator.compare(o1, o2);
  }

  /** 比较实现. */
  static class ComparatorImpl implements Comparator<Object> {
    public int compare(Object o1, Object o2) {
      if (o1 == null && o2 == null) {
        return 0;
      }
      if (o1 == null) {
        return -1;
      }
      if (o2 == null) {
        return 1;
      }
      if (o1.equals(o2)) {
        return 0;
      }
      Object[] a = new Object[] { o1, o2 };
      Arrays.sort(a);
      return a[0].equals(o1) ? -1 : 1;
    }
  }
}
