package com.gitee.jmash.common.utils;

import com.gitee.jmash.crypto.cipher.SM4CbcUtil;
import java.util.Base64;
import java.util.Optional;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.LogFactory;
import org.bouncycastle.util.encoders.Hex;
import org.eclipse.microprofile.config.Config;
import org.eclipse.microprofile.config.ConfigProvider;

/** 配置文件加解密. */
public class ConfigSecretUtil {

  private static String SECRET_KEY = "jmash.config.key";

  private static byte[] secretKey;

  /** 获取密钥. */
  private static byte[] getSecretKey() {
    if (secretKey != null && secretKey.length > 0) {
      return secretKey;
    }
    Config config = ConfigProvider.getConfig();
    Optional<String> keyOptional = config.getOptionalValue(SECRET_KEY, String.class);
    String key = keyOptional.orElse("am1hc2gAAAAAAAAAAAAAAA==");
    secretKey = Base64.getDecoder().decode(key);
    return secretKey;
  }

  /** 加密. */
  public static String encrypt(String attribute) {
    if (StringUtils.isBlank(attribute)) {
      return StringUtils.EMPTY;
    }
    byte[] encrypted;
    try {
      encrypted = SM4CbcUtil.get().encrypt(getSecretKey(), attribute.getBytes());
    } catch (Exception e) {
      LogFactory.getLog(ConfigSecretUtil.class).error(e);
      return attribute;
    }
    return Hex.toHexString(encrypted).toUpperCase();
  }

  /** 解密. */
  public static String decrypt(String dbData) {
    if (StringUtils.isBlank(dbData)) {
      return StringUtils.EMPTY;
    }
    byte[] encrypted;
    try {
      encrypted = SM4CbcUtil.get().decrypt(getSecretKey(), Hex.decode(dbData));
    } catch (Exception e) {
      LogFactory.getLog(ConfigSecretUtil.class).error("", e);
      return dbData;
    }
    return new String(encrypted);
  }

  public static void main(String[] args) {
    System.out.println(ConfigSecretUtil.encrypt("test"));
    System.out.println(ConfigSecretUtil.decrypt("C803448FB6A6170EFA0C8BF449C6064F"));
  }

}
