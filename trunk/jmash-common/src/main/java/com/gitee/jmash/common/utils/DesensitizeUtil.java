package com.gitee.jmash.common.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;

/** 脱敏处理工具类. */
public class DesensitizeUtil {

  /** 姓名脱敏,只显示第一个汉字,比如李某某置换为李**, 李某置换为李*. */
  public static String desensitizedName(final String fullName) {
    if (StringUtils.isBlank(fullName)) {
      return fullName;
    }
    String fullNameTrim = fullName.trim();
    String name = StringUtils.left(fullNameTrim, 1);
    return StringUtils.rightPad(name, StringUtils.length(fullNameTrim), "*");
  }

  /** 手机号脱敏,保留前三后四, 比如15638296218置换为156****6218. */
  public static String desensitizedPhone(final String phoneNumber) {
    if (StringUtils.isBlank(phoneNumber)) {
      return phoneNumber;
    }
    return phoneNumber.replaceAll("(\\w{3})\\w*(\\w{4})", "$1****$2");
  }

  /** 身份证脱敏,保留前六后三, 适用于15位和18位身份证号. */
  public static String desensitizedIdNumber(final String idNumber) {
    if (StringUtils.isBlank(idNumber)) {
      return idNumber;
    }
    if (idNumber.length() == 15) {
      return idNumber.replaceAll("(\\w{6})\\w*(\\w{3})", "$1******$2");
    } else if (idNumber.length() == 18) {
      return idNumber.replaceAll("(\\w{6})\\w*(\\w{3})", "$1*********$2");
    } else {
      return idNumber;
    }
  }

  /** 地址脱敏,从第4位开始隐藏,隐藏8位. */
  public static String desensitizedAddress(final String address) {
    if (StringUtils.isBlank(address)) {
      return address;
    }
    return StringUtils.left(address, 3)
        .concat(StringUtils
            .removeStart(StringUtils.leftPad(StringUtils.right(address, address.length() - 11),
                StringUtils.length(address), "*"), "***"));
  }

  /** 对邮件地址进行脱敏处理. */
  public static String desensitizeEmail(String email) {
    if (email == null || email.isEmpty()) {
      return email;
    }
    Pattern pattern = Pattern.compile("(?<=\\w{1})\\w(?=[\\w.]{0,4}@)");
    Matcher matcher = pattern.matcher(email);

    StringBuffer stringBuffer = new StringBuffer();
    while (matcher.find()) {
      matcher.appendReplacement(stringBuffer, "*");
    }
    matcher.appendTail(stringBuffer);
    return stringBuffer.toString();
  }
  
}
