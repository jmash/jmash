
package com.gitee.jmash.common.utils;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.EnumUtils;

/**
 * 枚举工具类.
 *
 * @author CGD
 * 
 */
public class EnumUtil {

  /**
   * 枚举数组转换为枚举名称串.
   *
   * @param objects 枚举数组
   * @return 枚举String通过‘,’分隔
   */
  public static <E extends Enum<E>> String enumsToStr(E[] objects) {
    if (objects == null) {
      return null;
    }
    StringBuilder str = new StringBuilder();
    for (E object : objects) {
      if (object != null) {
        str.append(str.length() == 0 ? "" : ",");
        str.append(object.name());
      }
    }
    return str.toString();
  }

  /**
   * 枚举串转换为枚举数组.
   *
   * @param value 枚举String通过‘,’分隔
   * @return 枚举数组
   */
  @SuppressWarnings("unchecked")
  public static <E extends Enum<E>> E[] strToEnums(Class<E> enumType, String value) {
    if (value == null || "".equals(value)) {
      return null;
    }
    String[] values = value.split(",");
    List<E> lists = new ArrayList<E>();
    for (String enumName : values) {
      E e = Enum.valueOf(enumType, enumName);
      if (e != null) {
        lists.add(e);
      }
    }

    return lists.toArray((E[]) Array.newInstance(enumType, 0));
  }

  /**
   * 枚举名称获取枚举.
   *
   * @param enumType 枚举类型
   * @param name     枚举名称
   * @return 枚举
   */
  public static <E extends Enum<E>> E valueOf(Class<E> enumType, String name) {
    return Enum.valueOf(enumType, name);
  }

  /**
   * 获取枚举.
   *
   * @param enumType 枚举类型
   * @param value    枚举int值
   * @return 枚举
   */
  public static <E extends Enum<E>> E valueOf(Class<E> enumType, int value) {
    E[] types = getEnumArrays(enumType);
    if (null != types && types.length > value) {
      return types[value];
    }
    return null;
  }

  /**
   * 反射获取枚举数组.
   *
   * @param enumType 枚举类型
   * @return 枚举values
   */
  public static <E extends Enum<E>> E[] getEnumArrays(Class<E> enumType) {
    if (enumType.isEnum()) {
      return enumType.getEnumConstants();
    }
    return null;
  }

  /**
   * enum封装为Map.
   *
   * @param enumType 枚举类型
   * @return name,enum Map
   */
  public static <E extends Enum<E>> Map<String, E> getEnumMap(Class<E> enumType) {
    E[] es = getEnumArrays(enumType);
    return getEnumMap(es);
  }

  /**
   * enum封装为Map.
   *
   * @param enums 枚举数组
   * @return name,enum Map
   */
  public static <E extends Enum<E>> Map<String, E> getEnumMap(E[] enums) {
    Map<String, E> map = new HashMap<String, E>();
    for (E e : enums) {
      map.put(e.name(), e);
    }
    return map;
  }

  /**
   * Enum封装为list.
   *
   * @param enumType 枚举类型
   * @return 枚举List
   */
  public static <E extends Enum<E>> List<E> getEnumList(Class<E> enumType) {
    E[] es = getEnumArrays(enumType);
    return Arrays.asList(es);
  }

  /**
   * Enum To Object.
   *
   * @param enumValue Enum value.
   * @param clazzName Object ClassName
   * @return Enum / String / int
   */
  @SuppressWarnings("rawtypes")
  public static Object enumToObject(Enum<?> enumValue, String clazzName) {
    try {
      Class clazz = Class.forName(clazzName);
      return enumToObject(enumValue, clazz);
    } catch (ClassNotFoundException ex) {
      throw new ClassCastException(clazzName + " ClassNotFoundException");
    }
  }

  /**
   * Enum To Object.
   *
   * @param enumValue Enum value.
   * @param objClazz  Object Class
   * @return Enum / String / int
   */
  @SuppressWarnings({ "unchecked", "rawtypes" })
  public static Object enumToObject(Enum<?> enumValue, Class objClazz) {
    if (null == enumValue) {
      return null;
    }
    if (!enumValue.getClass().isEnum()) {
      throw new ClassCastException(enumValue.getClass() + " is Not Enum");
    }
    if (objClazz.isEnum()) {
      return EnumUtils.getEnum(objClazz, enumValue.name());
    } else if (objClazz.equals(String.class)) {
      return enumValue.name();
    } else if (objClazz.equals(int.class) || objClazz.equals(Integer.class)) {
      return enumValue.ordinal();
    }
    return null;
  }

  /**
   * Object To enum .
   *
   * @param clazzName Enum ClassName
   * @param obj       Object Value
   * @return Enum Value
   */
  @SuppressWarnings({ "rawtypes", "unchecked" })
  public static Enum objectToEnum(String clazzName, Object obj) {
    try {
      Class<Enum> clazz = (Class) Class.forName(clazzName);
      return objectToEnum(clazz, obj);
    } catch (ClassNotFoundException ex) {
      throw new ClassCastException(clazzName + " ClassNotFoundException");
    }
  }

  /**
   * Object To enum .
   *
   * @param clazz Enum Class
   * @param obj   Object Value
   * @return Enum Value
   */
  @SuppressWarnings({ "rawtypes" })
  public static <E extends Enum<E>> E objectToEnum(Class<E> clazz, Object obj) {
    if (null == obj) {
      return null;
    }
    if (!clazz.isEnum()) {
      throw new ClassCastException(clazz + " is Not Enum");
    }
    if (obj.getClass().isEnum()) {
      return EnumUtils.getEnum(clazz, ((Enum) obj).name());
    }
    if (obj instanceof String) {
      return EnumUtils.getEnum(clazz, (String) obj);
    }
    if (obj instanceof Integer) {
      return EnumUtil.valueOf(clazz, (Integer) obj);
    }
    return null;
  }

}
