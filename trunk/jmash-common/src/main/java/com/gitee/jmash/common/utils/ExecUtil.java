
package com.gitee.jmash.common.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Exec执行util.
 *
 * @author cgd email:cgd@crenjoy.com
 * @version V1.0 2011-5-12
 */
public class ExecUtil {

  private static Log log = LogFactory.getLog(ExecUtil.class);

  public static int exec(String... cmd) {
    return new ExecUtil().exeCommand(cmd);
  }

  /**
   * 执行命令.
   *
   * @param errorOut Error输出
   * @param inputOut 输出
   * @param cmd      命令行
   * @return 0成功 1失败
   */
  public static int exec(OutputStream errorOut, OutputStream inputOut, String... cmd) {
    return new ExecUtil().exeCommand(cmd, errorOut, inputOut);
  }

  public int exeCommand(String[] commonds) {
    return exeCommand(commonds, null, null);
  }

  /**
   * 执行命令行.
   *
   * @param commonds 命令行
   * @param errorOut Error输出
   * @param inputOut 输出
   * @return 0成功 1失败
   */
  public int exeCommand(String[] commonds, OutputStream errorOut, OutputStream inputOut) {
    try {
      Runtime rt = Runtime.getRuntime();
      Process proc = null;
      if (SystemUtils.IS_OS_WINDOWS) {
        String[] cmd = new String[2 + commonds.length];
        cmd[0] = "cmd.exe";
        cmd[1] = "/C";
        int i = 2;
        for (String commond : commonds) {
          cmd[i++] = commond;
        }
        proc = rt.exec(cmd);
      } else {
        proc = rt.exec(commonds);
      }

      // any error message?
      StreamGobbler errorGobbler = new StreamGobbler(proc.getErrorStream(), "ERROR", errorOut);
      StreamGobbler inputGobbler = new StreamGobbler(proc.getInputStream(), "INPUT", inputOut);

      // kick them off
      errorGobbler.start();
      inputGobbler.start();

      int exitValue;
      if ((exitValue = proc.waitFor()) != 0) {
        log.info("exitValue:" + exitValue);
      }
      proc.destroy();
      return exitValue;
    } catch (InterruptedException t) {
      log.error(t.getMessage(), t);
      Thread.currentThread().interrupt();
      return 1;
    } catch (IOException t) {
      log.error(t.getMessage(), t);
      return 1;
    }
  }

  static class StreamGobbler extends Thread {
    InputStream is;

    String type; // 输出流的类型ERROR或OUTPUT

    OutputStream errorOut;

    OutputStream inputOut;

    StreamGobbler(InputStream is, String type) {
      this.is = is;
      this.type = type;
    }

    /**
     * Stream 反馈输出定义.
     *
     * @param is       流输入
     * @param type     类型
     * @param errorOut 错误流
     * @param debugOut debug流
     */
    StreamGobbler(InputStream is, String type, OutputStream output) {
      this.is = is;
      this.type = type;
      if (StringUtils.equalsIgnoreCase(type, "ERROR")) {
        this.errorOut = output;
      } else {
        this.inputOut = output;
      }
    }

    public void run() {
      try {
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);
        String line = null;
        while ((line = br.readLine()) != null) {
          if (StringUtils.equalsIgnoreCase(type, "ERROR")) {
            log.info(line);
            if (errorOut != null) {
              errorOut.write(line.getBytes());
              errorOut.write("\r\n".getBytes());
            }
          } else {
            log.debug(line);
            if (inputOut != null) {
              inputOut.write(line.getBytes());
              inputOut.write("\r\n".getBytes());
            }
          }
        }
      } catch (IOException ioe) {
        log.error(ioe.getMessage(), ioe);
      }
    }
  }
}
