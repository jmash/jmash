
package com.gitee.jmash.common.utils;

import com.gitee.jmash.common.algorithm.Crc32MessageDigest;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.LogFactory;

/**
 * 文件技术各种HASH值 支持 MD5 SHA-1 SHA-256 CRC32.
 *
 * @author CGD
 * 
 */
public class FileHashUtil {

  protected static char[] hexDigits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b',
      'c', 'd', 'e', 'f' };

  public static String MD5 = "MD5";

  public static String SHA1 = "SHA-1";

  public static String SHA256 = "SHA-256";

  public static String CRC32 = "CRC32";

  /**
   * 获取算法.
   *
   * @param algorithm 算法名称.
   */
  public static MessageDigest getMessageDigest(String algorithm) throws NoSuchAlgorithmException {
    if (StringUtils.equalsIgnoreCase(algorithm, CRC32)) {
      return new Crc32MessageDigest();
    } else {
      return MessageDigest.getInstance(algorithm);
    }
  }

  /**
   * 计算文件的Hash.
   *
   * @param fileName 文件的绝对路径
   */
  public static String[] getFileHashString(String fileName, String[] algorithms) {
    File f = new File(fileName);
    if (f.exists()) {
      return getFileHashString(f, algorithms);
    }
    LogFactory.getLog(FileHashUtil.class).error("文件不存在");
    return null;
  }

  /**
   * 计算文件的Hash，重载方法.
   *
   * @param file 文件对象
   */
  public static String[] getFileHashString(File file, String[] algorithms) {
    try {
      // 获取算法
      MessageDigest[] mds = new MessageDigest[algorithms.length];
      for (int i = 0; i < algorithms.length; i++) {
        mds[i] = getMessageDigest(algorithms[i]);
      }
      // 获取算法结果
      List<byte[]> byteList = getFileHash(file, mds);

      String[] ret = new String[algorithms.length];
      for (int i = 0; i < algorithms.length; i++) {
        ret[i] = bufferToHex(byteList.get(i));
      }
      return ret;
    } catch (Exception e) {
      LogFactory.getLog(FileHashUtil.class).error("文件计算HASH值：ERROR" + Arrays.toString(algorithms));
    }
    return null;
  }

  /**
   * 计算文件的Hash，重载方法.
   *
   * @param file 文件对象.
   */
  public static List<byte[]> getFileHashByte(File file, String[] algorithms) {
    try {
      // 获取算法
      MessageDigest[] mds = new MessageDigest[algorithms.length];
      for (int i = 0; i < algorithms.length; i++) {
        mds[i] = getMessageDigest(algorithms[i]);
      }
      // 获取算法结果
      List<byte[]> byteList = getFileHash(file, mds);

      return byteList;
    } catch (Exception e) {
      LogFactory.getLog(FileHashUtil.class).error("文件计算HASH值：ERROR" + Arrays.toString(algorithms));
    }
    return null;
  }

  /**
   * 计算字节数组Hash值.
   */
  public static String[] getByteArgHashString(byte[] bytes, String[] algorithms) {
    try {
      // 获取算法
      MessageDigest[] mds = new MessageDigest[algorithms.length];
      for (int i = 0; i < algorithms.length; i++) {
        mds[i] = getMessageDigest(algorithms[i]);
      }
      // 获取算法结果
      List<byte[]> byteList = getByteArgHash(bytes, mds);

      String[] ret = new String[algorithms.length];
      for (int i = 0; i < algorithms.length; i++) {
        ret[i] = bufferToHex(byteList.get(i));
      }
      return ret;
    } catch (Exception e) {
      LogFactory.getLog(FileHashUtil.class).error("文件计算HASH值：ERROR" + Arrays.toString(algorithms));
    }
    return null;
  }

  /**
   * 计算字节数组Hash值.
   *
   * @param bytes 字节数组
   * @param mds   Hash算法
   */
  public static List<byte[]> getByteArgHash(byte[] bytes, MessageDigest[] mds) {
    List<byte[]> ret = new ArrayList<byte[]>();

    for (MessageDigest md : mds) {
      md.update(bytes, 0, bytes.length);
    }

    for (MessageDigest md : mds) {
      ret.add(md.digest());
    }

    return ret;
  }

  /**
   * 计算文件Hash值.
   */
  public static List<byte[]> getFileHash(File file, MessageDigest[] mds) throws IOException {
    List<byte[]> ret = new ArrayList<byte[]>();
    int buff = 16384;

    try (RandomAccessFile accessFile = new RandomAccessFile(file, "r")) {
      byte[] buffer = new byte[buff];
      long read = 0;
      // calculate the hash of the hole file for the test
      long offset = file.length();
      int unitsize;
      while (read < offset) {
        // 单次取字节数
        unitsize = (int) (((offset - read) >= buff) ? buff : (offset - read));
        accessFile.read(buffer, 0, unitsize);
        for (MessageDigest md : mds) {
          md.update(buffer, 0, unitsize);
        }
        read += unitsize;
      }
    }

    for (MessageDigest md : mds) {
      ret.add(md.digest());
    }

    return ret;
  }

  /**
   * 字节转换为串.
   */
  private static String bufferToHex(byte[] bytes) {
    return bufferToHex(bytes, 0, bytes.length);
  }

  private static String bufferToHex(byte[] bytes, int m, int n) {
    StringBuffer stringbuffer = new StringBuffer(2 * n);
    int k = m + n;
    for (int l = m; l < k; l++) {
      appendHexPair(bytes[l], stringbuffer);
    }
    return stringbuffer.toString();
  }

  private static void appendHexPair(byte bt, StringBuffer stringbuffer) {
    char c0 = hexDigits[(bt & 0xf0) >> 4];
    char c1 = hexDigits[bt & 0xf];
    stringbuffer.append(c0);
    stringbuffer.append(c1);
  }

}
