
package com.gitee.jmash.common.utils;

import com.gitee.jmash.common.freemarker.JmashObjectWrapper;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Map;
import javax.annotation.concurrent.ThreadSafe;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 通过类路径模板解析.
 *
 * @author CGD
 *
 */
@ThreadSafe
public class FreeMarkerUtil {

  private static final Log log = LogFactory.getLog(FreeMarkerUtil.class);

  /**
   * 获取模板路径.
   */
  public static Configuration getConfiguration(Class<?> resourceLoaderClass) {
    return getConfiguration(resourceLoaderClass, "/");
  }

  /**
   * 获取模板路径.
   */
  public static Configuration getConfiguration(Class<?> loaderClass, String basePath) {
    Configuration cfg = new Configuration(Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS);
    cfg.setDefaultEncoding("UTF-8");
    cfg.setClassForTemplateLoading(loaderClass, basePath);
    cfg.setObjectWrapper(new JmashObjectWrapper(Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS));
    return cfg;
  }

  /**
   * 生成模板方法.
   *
   * @param propMap 属性
   * @param templateName 模板名
   */
  public static String parse(Class<?> loaderClass, Map<String, ? extends Object> propMap,
      String templateName) {
    try {
      // 获取摸板
      Template t = getConfiguration(loaderClass).getTemplate(templateName);
      // 处理文件
      StringWriter writer = new StringWriter();
      t.process(propMap, writer);
      return writer.toString();
    } catch (TemplateException | IOException ex) {
      log.error(ex);
      return null;
    }
  }

  /**
   * 生成模板方法.
   *
   * @param propMap 属性
   * @param templateName 模板名
   * @param out 输出文件
   */
  public static void writeFile(Class<?> loaderClass, Map<String, ? extends Object> propMap,
      String templateName, File out) {
    try {
      // 获取摸板
      Template t = getConfiguration(loaderClass).getTemplate(templateName);
      // 处理文件
      Writer writer =
          new BufferedWriter(new OutputStreamWriter(new FileOutputStream(out), "UTF-8"));
      t.process(propMap, writer);
      writer.close();
    } catch (TemplateException | IOException ex) {
      log.error(ex);
    }
  }

}
