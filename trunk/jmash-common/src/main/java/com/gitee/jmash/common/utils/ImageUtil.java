
package com.gitee.jmash.common.utils;

import com.gitee.jmash.common.enums.FileType;
import com.gitee.jmash.common.enums.MimeType;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Base64;
import java.util.List;
import javax.imageio.ImageIO;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.LogFactory;

/**
 * Image Util.
 *
 * @author CGD
 *
 */
public class ImageUtil {

  /**
   * Image To Base64 String Default webp.
   */
  public static String imageToBase64Image(BufferedImage image) {
    return imageToBase64Image(image, MimeType.M_jpg);
  }

  /**
   * Image To Base64 String.
   */
  public static String imageToBase64Image(BufferedImage image, MimeType imageType) {
    try {
      ByteArrayOutputStream stream = new ByteArrayOutputStream();
      // 设置图片的格式
      ImageIO.write(image, imageType.getFileExt(), stream);
      if (stream.size() == 0) {
        return StringUtils.EMPTY;
      }
      String base64 = Base64.getEncoder().encodeToString(stream.toByteArray());
      return String.format("data:%s;base64,%s", imageType.getMimeType(), base64);
    } catch (IOException e) {
      LogFactory.getLog(ImageUtil.class).error(e);
      return StringUtils.EMPTY;
    }
  }

  /**
   * Image From Base64 String.
   */
  public static BufferedImage imageFromBase64Image(String base64Image) {
    try {
      String base64 = removeBase64ImagePrefix(base64Image);
      byte[] byteImage = Base64.getDecoder().decode(base64);
      return ImageIO.read(new ByteArrayInputStream(byteImage));
    } catch (IOException e) {
      LogFactory.getLog(ImageUtil.class).error(e);
      return null;
    }
  }

  /**
   * 获取Image Type By Prefix 'data:image/jpeg;base64,' .
   */
  public static MimeType imageMimeType(String base64Image) {
    if (base64Image.indexOf(";base64,") > -1) {
      String mimeTypeStr = StringUtils.substringBetween(base64Image, "data:", ";base64,");
      if (StringUtils.isNotEmpty(mimeTypeStr)) {
        List<MimeType> list = MimeType.mimeType(mimeTypeStr);
        return (list.size() > 0) ? list.get(0) : null;
      }
    }
    return null;
  }

  /**
   * 移除'data:image/jpeg;base64,'前缀.
   */
  private static String removeBase64ImagePrefix(String base64Image) {
    if (base64Image.indexOf(";base64,") > -1) {
      return StringUtils.substringAfterLast(base64Image, ";base64,");
    }
    return base64Image;
  }

  /**
   * 通过文件路径得到文件类型.
   */
  public static FileType getFileType(String filePath) {
    // 确定文件类型
    String anyfileExt = MimeType.getExtension(filePath).toLowerCase();
    MimeType mimeType = MimeType.convert(anyfileExt);
    if (null == mimeType) {
      return null;
    }
    return mimeType.getFileType();
  }

  /**
   * 获取图片的宽和高.
   */
  public static Integer[] getImageW_H(File file, String fileName) throws IOException {
    FileType fileType = getFileType(fileName);
    if (null == fileType || !FileType.image.equals(fileType)) {
      return new Integer[] {null, null};
    }
    // 图片，获取高度和宽度
    BufferedImage img = ImageIO.read(file); // 构造Image对象
    if (null == img) {
      return new Integer[] {0, 0};
    }
    return new Integer[] {img.getWidth(null), img.getHeight(null)};
  }

}
