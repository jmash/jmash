
package com.gitee.jmash.common.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Date;
import org.apache.commons.logging.LogFactory;

/**
 * 非对称密钥.
 *
 * @author CGD
 */
public class JwtRsaUtil {

  /**
   * 公钥构建.
   */
  public static JwtRsaUtil createPublic(String publicKey) throws Exception {
    JwtRsaUtil util = new JwtRsaUtil();
    PublicKey publicK = RsaUtil.getPublicKey(publicKey);
    util.setPublicKey(publicK);
    return util;
  }

  /**
   * 公钥构建.
   */
  public static JwtRsaUtil createPublic(PublicKey publicK) {
    JwtRsaUtil util = new JwtRsaUtil();
    util.setPublicKey(publicK);
    return util;
  }

  /**
   * 私钥构建.
   */
  public static JwtRsaUtil createPrivate(String privateKey) throws Exception {
    PrivateKey privateK = RsaUtil.getPrivateKey(privateKey);
    return createPrivate(privateK);
  }

  /**
   * 私钥构建.
   */
  public static JwtRsaUtil createPrivate(PrivateKey privateK) {
    JwtRsaUtil util = new JwtRsaUtil();
    util.setPrivateKey(privateK);
    return util;
  }

  /** 公钥. */
  private PublicKey publicKey;
  /** 私钥. */
  private PrivateKey privateKey;

  /** 算法. */
  private SignatureAlgorithm algorithm = SignatureAlgorithm.RS256;

  public JwtRsaUtil() {
    super();
  }

  /**
   * 编码.
   *
   * @param claims 数据
   */
  public String encode(Claims claims) {
    return encode(claims, null);
  }

  /**
   * 编码.
   *
   * @param claims 数据
   * @param expire 过期时间毫秒
   */
  public String encode(Claims claims, Long expire) {
    if (claims == null || claims.getId() == null || claims.getSubject() == null) {
      return null;
    }
    if (null == expire && null == claims.getExpiration()) {
      return null;
    }
    JwtBuilder builder = Jwts.builder().setClaims(claims).setIssuedAt(new Date());
    // 计算过期时间
    if (null != expire) {
      builder.setExpiration(new Date(System.currentTimeMillis() + expire));
    }
    String token = builder.signWith(privateKey, algorithm).compact();
    return token;
  }

  /**
   * 解码错误.
   */
  public Claims decode(String token) {
    try {
      final Jws<Claims> jws = Jwts.parserBuilder().setSigningKey(publicKey).build()
          .parseClaimsJws(token);
      return jws.getBody();
    } catch (Exception e) {
      LogFactory.getLog(JwtRsaUtil.class).error(e.getMessage(), e);
      return null;
    }
  }

  public PublicKey getPublicKey() {
    return publicKey;
  }

  public void setPublicKey(PublicKey publicKey) {
    this.publicKey = publicKey;
  }

  public PrivateKey getPrivateKey() {
    return privateKey;
  }

  public void setPrivateKey(PrivateKey privateKey) {
    this.privateKey = privateKey;
  }

  public SignatureAlgorithm getAlgorithm() {
    return algorithm;
  }

  public void setAlgorithm(SignatureAlgorithm algorithm) {
    this.algorithm = algorithm;
  }

}
