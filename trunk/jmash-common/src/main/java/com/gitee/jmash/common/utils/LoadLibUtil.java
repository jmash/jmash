
package com.gitee.jmash.common.utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.lang3.SystemUtils;
import org.apache.commons.logging.LogFactory;

/**
 * 处理JarFile.
 *
 * @author CGD
 */
public class LoadLibUtil {

  /**
   * 将某Jar下的文件拷贝到某文件目录下.
   *
   * @param jarName  Jar文件名称
   * @param fileName 文件目录
   * @return 是否拷贝成功
   */
  @SuppressWarnings("rawtypes")
  public static boolean copyJarFile(Class clazz, String jarName, String fileName) {
    InputStream in = null;

    File extractedLibFile = new File(fileName);
    if (!extractedLibFile.exists() || extractedLibFile.length() <= 0) {
      try {
        in = clazz.getResourceAsStream(jarName);
        if (in == null) {
          return false;
        }
        BufferedInputStream reader = new BufferedInputStream(in);
        try (FileOutputStream writer = new FileOutputStream(extractedLibFile)) {

          byte[] buffer = new byte[1024];

          while (reader.read(buffer) > 0) {
            writer.write(buffer);
            buffer = new byte[1024];
          }
        }
      } catch (IOException e) {
        LogFactory.getLog(LoadLibUtil.class).error(e.getMessage());
        return false;
      } finally {
        try {
          if (in != null) {
            in.close();
          }
        } catch (IOException e) {
          LogFactory.getLog(LoadLibUtil.class).error(e.getMessage());
        }
      }
    }
    LogFactory.getLog(LoadLibUtil.class).info(String.format("%s copy to %s", jarName, fileName));
    return true;
  }

  /**
   * 动态加载JAR下lib 文件.
   *
   * @param libPath Lib相对路径
   * @param libName lib名称
   */
  @SuppressWarnings("rawtypes")
  public static synchronized void loadJarlib(Class loadClass, String libPath, String libName) {
    String systemType = System.getProperty("os.name");
    String libExtension = (systemType.toLowerCase().indexOf("win") != -1) ? ".dll" : ".so";
    if (System.getProperty("os.arch").equals("amd64")) {
      libExtension = "_x64" + libExtension;
    }
    String libFullName = libName + libExtension;

    String fileName = System.getProperty("user.dir") + libPath.replace("/", File.separator)
        + libFullName;

    if (copyJarFile(loadClass, libPath + libFullName, fileName)) {
      // 如果是Linux系统增加可执行权限
      if (SystemUtils.IS_OS_LINUX || SystemUtils.IS_OS_MAC || SystemUtils.IS_OS_MAC_OSX) {
        ExecUtil.exec("chmod", "+x", fileName);
      }
      System.load(fileName);
    }
  }
}
