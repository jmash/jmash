
package com.gitee.jmash.common.utils;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.LogFactory;

/**
 * 获取本地IP地址.
 *
 * @author cgd
 *
 */
public class LocalIpUtil {

  /**
   * 获取主机名称.
   *
   * @return 主机名称
   */
  public static String getHostName() {
    try {
      return InetAddress.getLocalHost().getHostName();
    } catch (Exception ex) {
      LogFactory.getLog(LocalIpUtil.class).error("HOST:", ex);
      return StringUtils.EMPTY;
    }
  }

  /**
   * 获取系统首选IP.
   *
   * @return 首选IP地址
   */
  public static String getLocalIp() {
    try {
      return InetAddress.getLocalHost().getHostAddress();
    } catch (Exception ex) {
      LogFactory.getLog(LocalIpUtil.class).error("IP:", ex);
      return StringUtils.EMPTY;
    }
  }

  /**
   * 获取所有网卡IP，排除回文地址、虚拟地址.
   *
   * @return 所有IP地址
   */
  public static String[] getLocalIps() {
    List<String> list = new ArrayList<>();
    try {
      Enumeration<NetworkInterface> enumeration = NetworkInterface.getNetworkInterfaces();
      while (enumeration.hasMoreElements()) {
        NetworkInterface intf = enumeration.nextElement();
        if (intf.isLoopback() || intf.isVirtual()) { //
          continue;
        }
        Enumeration<InetAddress> inets = intf.getInetAddresses();
        while (inets.hasMoreElements()) {
          InetAddress addr = inets.nextElement();
          if (addr.isLoopbackAddress() || !addr.isSiteLocalAddress() || addr.isAnyLocalAddress()) {
            continue;
          }
          list.add(addr.getHostAddress());
        }
      }
    } catch (Exception ex) {
      LogFactory.getLog(LocalIpUtil.class).error("Network IP:", ex);
    }
    return list.toArray(new String[0]);
  }

  /**
   * 判断操作系统是否是Windows.
   *
   * @return 是否Window系统
   */
  public static boolean isWindowsOs() {
    boolean isWindowsOs = false;
    String osName = System.getProperty("os.name");
    if (osName.toLowerCase().indexOf("windows") > -1) {
      isWindowsOs = true;
    }
    return isWindowsOs;
  }

}
