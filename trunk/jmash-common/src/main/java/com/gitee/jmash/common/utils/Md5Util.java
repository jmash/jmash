
package com.gitee.jmash.common.utils;

import java.security.MessageDigest;
import java.util.Arrays;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * MD5加密.
 *
 * @author CGD
 *
 */
public class Md5Util {

  private static final char[] hexDigits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A',
      'B', 'C', 'D', 'E', 'F' };

  /**
   * Convert hex string to byte[].
   *
   * @param hexString the hex string
   * @return byte[]
   */
  public static byte[] hexStringToByteArray(String hexString) {
    if (hexString == null || hexString.equals("")) {
      return null;
    }
    hexString = hexString.toUpperCase();
    int length = hexString.length() / 2;
    char[] hexChars = hexString.toCharArray();
    byte[] d = new byte[length];
    for (int i = 0; i < length; i++) {
      int pos = i * 2;
      d[i] = (byte) (charToByte(hexChars[pos]) << 4 | charToByte(hexChars[pos + 1]) & 0xff);
    }
    return d;
  }

  /**
   * Convert char to byte.
   *
   * @param c char
   * @return byte
   */
  private static byte charToByte(char c) {
    return (byte) Arrays.binarySearch(hexDigits, c);
  }

  /**
   * 转换字节数组为16进制字串.
   *
   * @param b 字节数组
   * @return 16进制字串
   */
  public static String byteArrayToHexString(byte[] b) {
    StringBuffer resultSb = new StringBuffer();
    if (null == b) {
      return resultSb.toString();
    }
    for (int i = 0; i < b.length; i++) {
      resultSb.append(byteToHexString(b[i]));
    }
    return resultSb.toString();
  }

  /**
   * 单字节转换为16进制串.
   */
  private static String byteToHexString(byte b) {
    int n = b;
    if (n < 0) {
      n = 256 + n;
    }
    int d1 = n / 16;
    int d2 = n % 16;
    char[] bm = new char[] { hexDigits[d1], hexDigits[d2] };
    return String.valueOf(bm);
  }

  /**
   * 获取MD5串.
   */
  public static String encodeMd5(String origin) {
    String resultString = null;
    try {
      resultString = new String(origin);
      MessageDigest md = MessageDigest.getInstance("MD5");
      resultString = byteArrayToHexString(md.digest(resultString.getBytes()));
    } catch (Exception ex) {
      LogFactory.getLog(Md5Util.class).error(ex);
    }
    return resultString;
  }

  /** Main. */
  public static void main(String[] args) {
    Log log = LogFactory.getLog(Md5Util.class);
    log.info(encodeMd5("456.com"));

    byte[] bytes = Md5Util.hexStringToByteArray("0123456789ABCDEF");
    String t = Md5Util.byteArrayToHexString(bytes);
    log.info(t);
  }
}
