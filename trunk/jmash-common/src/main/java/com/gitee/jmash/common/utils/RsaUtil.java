
package com.gitee.jmash.common.utils;

import java.io.ByteArrayOutputStream;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import javax.crypto.Cipher;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * RSA公钥/私钥/签名工具包.<br/>
 * 字符串格式的密钥在未在特殊说明情况下都为BASE64编码格式<br/>
 * 由于非对称加密速度极其缓慢，一般文件不使用它来加密而是使用对称加密，<br/>
 * 非对称加密算法可以用来对对称加密的密钥加密，这样保证密钥的安全也就保证了数据的安全
 */
public class RsaUtil {

  private static Log log = LogFactory.getLog(RsaUtil.class);

  /** 算法供应商. */
  public static final String ALG_PROVIDER = "mockAlgorithm";

  /** 加密算法RSA. */
  public static final String KEY_ALGORITHM = "RSA";

  /** 签名算法. */
  public static final String SIGNATURE_ALGORITHM = "SHA256withRSA";

  private static final int KEY_SIZE = 2048;

  /** RSA最大加密明文大小. */
  private static final int MAX_ENCRYPT_BLOCK = KEY_SIZE / 8 - 11;

  /** RSA最大解密密文大小. */
  private static final int MAX_DECRYPT_BLOCK = KEY_SIZE / 8;

  /**
   * 用私钥对信息生成数字签名.
   *
   * @param data 签名数据
   * @param privateKey 私钥
   * @return 默认算法数字签名
   */
  public static String sign(byte[] data, String privateKey) throws Exception {
    PrivateKey privateK = getPrivateKey(privateKey);
    byte[] sign = sign(data, privateK, SIGNATURE_ALGORITHM);

    return Base64.getEncoder().encodeToString(sign);
  }

  /**
   * 用私钥对信息生成数字签名.
   *
   * @param data 签名数据
   * @param privateK 私钥
   * @return 默认算法数字签名
   */
  public static String sign(byte[] data, PrivateKey privateK) throws Exception {
    byte[] sign = sign(data, privateK, SIGNATURE_ALGORITHM);
    return Base64.getEncoder().encodeToString(sign);
  }

  /**
   * 用私钥对信息生成数字签名.
   *
   * @param data 签名数据
   * @param privateK 私钥
   * @param signAlgorithm 签名算法
   * @return 数字签名
   */
  public static byte[] sign(byte[] data, PrivateKey privateK, String signAlgorithm)
      throws Exception {
    Signature signature = Signature.getInstance(signAlgorithm);
    signature.initSign(privateK);
    signature.update(data);
    return signature.sign();
  }

  /**
   * 校验数字签名.
   */
  public static boolean verify(byte[] data, String publicKey, String sign) throws Exception {
    PublicKey publicK = getPublicKey(publicKey);
    byte[] signByte = Base64.getDecoder().decode(sign);
    return verify(data, publicK, signByte, SIGNATURE_ALGORITHM);
  }

  /**
   * 校验数字签名.
   *
   * @param data 已加密数据
   * @param publicK 公钥
   * @param sign 数字签名
   */
  public static boolean verify(byte[] data, PublicKey publicK, String sign) throws Exception {
    byte[] signByte = Base64.getDecoder().decode(sign);
    return verify(data, publicK, signByte, SIGNATURE_ALGORITHM);
  }

  /**
   * 校验数字签名.
   *
   * @param data 签名数据
   * @param publicK 公钥
   * @param sign 数字签名
   * @param signAlgorithm 签名算法
   * 
   */
  public static boolean verify(byte[] data, PublicKey publicK, byte[] sign, String signAlgorithm)
      throws Exception {
    Signature signature = Signature.getInstance(signAlgorithm);
    signature.initVerify(publicK);
    signature.update(data);
    return signature.verify(sign);
  }

  /**
   * 私钥解密.
   *
   * @param data 已加密数据
   * @param privateKey 私钥(BASE64编码)
   * @return 源数据
   */
  public static byte[] decryptByPrivateKey(byte[] data, String privateKey) throws Exception {
    PrivateKey privateK = getPrivateKey(privateKey);
    return decrypt(data, privateK);
  }

  /**
   * 公钥解密.
   *
   * @param data 已加密数据
   * @param publicKey 公钥(BASE64编码)
   * @return 源数据
   */
  public static byte[] decryptByPublicKey(byte[] data, String publicKey) throws Exception {
    PublicKey publicK = getPublicKey(publicKey);
    return decrypt(data, publicK);
  }

  /**
   * 解密.
   *
   * @param data 加密数据
   * @param key 密钥
   * @return 源数据
   */
  public static byte[] decrypt(byte[] data, Key key) throws Exception {
    Cipher cipher = Cipher.getInstance(key.getAlgorithm());
    cipher.init(Cipher.DECRYPT_MODE, key);
    int inputLen = data.length;
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    int offSet = 0;
    byte[] cache;
    int i = 0;
    // 对数据分段解密
    while (inputLen - offSet > 0) {
      if (inputLen - offSet > MAX_DECRYPT_BLOCK) {
        cache = cipher.doFinal(data, offSet, MAX_DECRYPT_BLOCK);
      } else {
        cache = cipher.doFinal(data, offSet, inputLen - offSet);
      }
      out.write(cache, 0, cache.length);
      i++;
      offSet = i * MAX_DECRYPT_BLOCK;
    }
    byte[] decryptedData = out.toByteArray();
    out.close();
    return decryptedData;
  }

  /**
   * 公钥加密.
   *
   * @param data 源数据
   * @param publicKey 公钥(BASE64编码)
   * @return 加密后数据
   */
  public static byte[] encryptByPublicKey(byte[] data, String publicKey) throws Exception {
    PublicKey publicK = getPublicKey(publicKey);
    return encrypt(data, publicK);
  }

  /**
   * 私钥加密.
   *
   * @param data 源数据
   * @param privateKey 私钥(BASE64编码)
   * @return 加密后数据
   */
  public static byte[] encryptByPrivateKey(byte[] data, String privateKey) throws Exception {
    PrivateKey privateK = getPrivateKey(privateKey);
    return encrypt(data, privateK);
  }

  /**
   * 加密.
   *
   * @param data 源数据
   * @param key 密钥
   * @return 加密数据
   */
  public static byte[] encrypt(byte[] data, Key key) throws Exception {
    // 对数据加密
    Cipher cipher = Cipher.getInstance(key.getAlgorithm());
    cipher.init(Cipher.ENCRYPT_MODE, key);
    int inputLen = data.length;
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    int offSet = 0;
    byte[] cache;
    int i = 0;
    // 对数据分段加密
    while (inputLen - offSet > 0) {
      if (inputLen - offSet > MAX_ENCRYPT_BLOCK) {
        cache = cipher.doFinal(data, offSet, MAX_ENCRYPT_BLOCK);
      } else {
        cache = cipher.doFinal(data, offSet, inputLen - offSet);
      }
      out.write(cache, 0, cache.length);
      i++;
      offSet = i * MAX_ENCRYPT_BLOCK;
    }
    byte[] encryptedData = out.toByteArray();
    out.close();
    return encryptedData;
  }

  /**
   * 生成密钥对(公钥和私钥).
   */
  public static KeyPair genKeyPair() throws Exception {
    KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance(KEY_ALGORITHM);
    keyPairGen.initialize(KEY_SIZE);
    return keyPairGen.generateKeyPair();
  }

  /**
   * 获取Base64 Key.
   */
  public static String getBase64Key(Key key) {
    return Base64.getEncoder().encodeToString(key.getEncoded());
  }

  /**
   * Base64 To PublicKey.
   */
  public static PublicKey getPublicKey(String publicKey) throws Exception {

    byte[] keyBytes = Base64.getDecoder().decode(publicKey);
    X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(keyBytes);
    KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
    return keyFactory.generatePublic(x509KeySpec);
  }

  /**
   * Base64 To PrivateKey.
   */
  public static PrivateKey getPrivateKey(String privateKey) throws Exception {
    byte[] keyBytes = Base64.getDecoder().decode(privateKey);
    PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(keyBytes);
    KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
    return keyFactory.generatePrivate(pkcs8KeySpec);
  }

  /**
   * Main .
   */
  public static void main(String[] args) throws Exception {
    KeyPair keyPair = genKeyPair();
    log.info(getBase64Key(keyPair.getPrivate()));
    log.info(getBase64Key(keyPair.getPublic()));
  }
}
