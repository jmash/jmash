
package com.gitee.jmash.common.utils;

import org.apache.commons.io.FileUtils;

/** 运行环境数据. */
public class RunEnvInfo {

  // 操作系统名称
  private String osName = System.getProperty("os.name");
  // 版本信息
  private String osVersion = System.getProperty("os.version");
  // 操作系统的架构
  private String osArch = System.getProperty("os.arch");
  // Java 虚拟机实现名称
  private String jvmName = System.getProperty("java.vm.name");
  // Java 虚拟机实现版本
  private String jvmVersion = System.getProperty("java.vm.version");
  // 虚拟机实现供应商
  private String jvmVendor = System.getProperty("java.vm.vendor");
  // Java运行时环境版本信息
  private String javaVersion = System.getProperty("java.version");
  // CPU数量
  private int availableProcessors = Runtime.getRuntime().availableProcessors();
  // JVM空闲内存
  private long freeMemory = Runtime.getRuntime().freeMemory();
  // JVM总内存
  private long totalMemory = Runtime.getRuntime().totalMemory();

  public String getOsName() {
    return osName;
  }

  public void setOsName(String osName) {
    this.osName = osName;
  }

  public String getOsVersion() {
    return osVersion;
  }

  public void setOsVersion(String osVersion) {
    this.osVersion = osVersion;
  }

  public String getOsArch() {
    return osArch;
  }

  public void setOsArch(String osArch) {
    this.osArch = osArch;
  }

  public String getJvmName() {
    return jvmName;
  }

  public void setJvmName(String jvmName) {
    this.jvmName = jvmName;
  }

  public String getJvmVersion() {
    return jvmVersion;
  }

  public void setJvmVersion(String jvmVersion) {
    this.jvmVersion = jvmVersion;
  }

  public String getJvmVendor() {
    return jvmVendor;
  }

  public void setJvmVendor(String jvmVendor) {
    this.jvmVendor = jvmVendor;
  }

  public String getJavaVersion() {
    return javaVersion;
  }

  public void setJavaVersion(String javaVersion) {
    this.javaVersion = javaVersion;
  }

  public int getAvailableProcessors() {
    return availableProcessors;
  }

  public void setAvailableProcessors(int availableProcessors) {
    this.availableProcessors = availableProcessors;
  }

  public long getFreeMemory() {
    return freeMemory;
  }

  public String getDisplayFreeMemory() {
    return FileUtils.byteCountToDisplaySize(freeMemory);
  }

  public long getUsedMemory() {
    return this.totalMemory - this.freeMemory;
  }

  public String getDisplayUsedMemory() {
    return FileUtils.byteCountToDisplaySize(getUsedMemory());
  }

  public void setFreeMemory(long freeMemory) {
    this.freeMemory = freeMemory;
  }

  public long getTotalMemory() {
    return totalMemory;
  }

  public String getDisplayTotalMemory() {
    return FileUtils.byteCountToDisplaySize(totalMemory);
  }

  public void setTotalMemory(long totalMemory) {
    this.totalMemory = totalMemory;
  }

}
