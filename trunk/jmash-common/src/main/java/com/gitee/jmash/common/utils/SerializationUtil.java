
package com.gitee.jmash.common.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import org.apache.commons.logging.LogFactory;

/**
 * Java Serialization.
 *
 * @author CGD
 *
 */
public class SerializationUtil {

  /**
   * 序列化.
   */
  public static byte[] serialize(Serializable obj) {
    if (null == obj) {
      return null;
    }
    try {
      ByteArrayOutputStream stream = new ByteArrayOutputStream();
      ObjectOutputStream oos = new ObjectOutputStream(stream);
      oos.writeObject(obj);
      return stream.toByteArray();
    } catch (IOException e) {
      LogFactory.getLog(SerializationUtil.class).error(e);
      return null;
    }
  }

  /**
   * serialize the given object and save it to file.
   */
  public static void serialize(Serializable obj, String fileName) throws IOException {
    try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fileName))) {
      oos.writeObject(obj);
    }
  }

  /**
   * 反序列化.
   */
  public static Object deserialize(byte[] bytes) {
    if (null == bytes) {
      return null;
    }
    try {
      ByteArrayInputStream stream = new ByteArrayInputStream(bytes);
      ObjectInputStream ois = new ObjectInputStream(stream);
      return ois.readObject();
    } catch (Exception e) {
      LogFactory.getLog(SerializationUtil.class).error(e);
      return null;
    }
  }

  /**
   * deserialize to Object from given file.
   */
  public static Object deserialize(String fileName) throws IOException, ClassNotFoundException {
    try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fileName))) {
      return ois.readObject();
    }
  }

}
