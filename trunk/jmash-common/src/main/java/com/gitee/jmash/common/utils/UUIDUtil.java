
package com.gitee.jmash.common.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.apache.commons.lang3.StringUtils;

/**
 * UUID转换 二进制转换.
 *
 * @author CGD
 * 
 */
public class UUIDUtil {

  static char[] hexs =
      {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

  static char[] hex32s = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e',
      'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v'};

  static char[] hex64s = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e',
      'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
      'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q',
      'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '-', '_'};

  /**
   * empty UUID string.
   *
   * @return empty UUID
   */
  public static String emptyUUIDStr() {
    return "00000000-0000-0000-0000-000000000000";
  }
  
  public static String emptyUUID32() {
    return "00000000000000000000000000000000";
  }

  /**
   * 返回UUID 00000000-0000-0000-0000-000000000000.
   *
   * @return empty UUID
   */
  public static UUID emptyUUID() {
    UUID uuid = UUID.fromString(emptyUUIDStr());
    return uuid;
  }

  /**
   * System UUID.
   * 
   */
  public static UUID systemUUID() {
    UUID uuid = UUID.fromString("FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF");
    return uuid;
  }

  /**
   * uuid string.
   *
   * @param uuidStr UUID String
   * @return UUID
   */
  public static UUID fromString(String uuidStr) {
    if (StringUtils.isEmpty(uuidStr)) {
      return null;
    }
    if (uuidStr.length() == 32) {
      uuidStr = formatUUID(uuidStr);
    }
    UUID uuid = UUID.fromString(uuidStr);
    return uuid;
  }

  /** a,b 比较是否UUID一致. */
  public static boolean equals(UUID a, String b) {
    if (a == null && b == null) {
      return true;
    }
    if (a == null || b == null) {
      return false;
    }
    return a.equals(fromString(b));
  }

  /** a,b 比较是否UUID一致. */
  public static boolean equals(String a, String b) {
    if (StringUtils.equals(a, b)) {
      return true;
    }
    if (a == null || b == null) {
      return false;
    }
    return equals(fromString(a), b);
  }

  /**
   * 判断UUID 是否为null或00000000-0000-0000-0000-000000000000.
   *
   * @param uuid UUID
   * @return 是否空
   */
  public static boolean equalsEmpty(UUID uuid) {
    if (null == uuid) {
      return true;
    }
    if (UUIDUtil.emptyUUID().equals(uuid)) {
      return true;
    }
    return false;
  }

  /** uuid 是否为 00000000-0000-0000-0000-000000000000 或 00000000000000000000000000000000 */
  public static boolean equalsEmptyUUID(String uuid) {
    if (StringUtils.isBlank(uuid)) {
      return false;
    }
    if (uuid.length() != 32 && uuid.length() != 36) {
      return false;
    }
    if (StringUtils.equals(emptyUUIDStr(), uuid)) {
      return true;
    }
    return StringUtils.equals(emptyUUIDStr().replace("-", ""), uuid);
  }

  /**
   * List String to List UUID.
   *
   * @param strList String List
   * @return UUID List
   */
  public static List<UUID> fromListString(List<String> strList) {
    List<UUID> uuidList = new ArrayList<UUID>(strList.size());
    for (String uuidStr : strList) {
      uuidList.add(UUID.fromString(uuidStr));
    }
    return uuidList;
  }

  /**
   * List UUID to List String.
   *
   * @param uuidList UUID String
   * @return String List
   */
  public static List<String> toListString(List<UUID> uuidList) {
    List<String> strList = new ArrayList<String>(uuidList.size());
    for (UUID uuid : uuidList) {
      strList.add(uuid.toString());
    }
    return strList;
  }

  /**
   * byte[]转为UUID.
   *
   * @param src byte[]
   * @return String UUID
   */
  public static String byteToUUID(byte[] src) {
    StringBuilder stringBuilder = new StringBuilder("");
    if (src == null || src.length <= 0) {
      return null;
    }
    for (int i = 0; i < src.length; i++) {
      int v = src[i] & 0xFF;
      String hv = Integer.toHexString(v);
      if (hv.length() < 2) {
        stringBuilder.append(0);
      }
      stringBuilder.append(hv);
    }
    return formatUUID(stringBuilder.toString());
  }

  /**
   * uuid 转 byte[].
   *
   * @param src UUID String
   * @return byte[]
   */
  public static byte[] uuidToByte(String src) {
    String hexString = src.replace("-", "");
    if (hexString == null || hexString.equals("")) {
      return null;
    }
    hexString = hexString.toLowerCase();
    int length = hexString.length() / 2;

    char[] hexChars = hexString.toCharArray();

    byte[] d = new byte[length];

    for (int i = 0; i < length; i++) {
      int pos = i * 2;
      d[i] = (byte) (indexOf(hexs, hexChars[pos]) << 4 | indexOf(hexs, hexChars[pos + 1]));
    }
    return d;
  }

  public static String uuid32(UUID uuid) {
    return uuid.toString().replace("-", "");
  }

  /**
   * UUID 转 32进制.
   *
   * @param uuid UUID String
   * @return UUID 32进制
   */
  public static String uuidToUUIDHex32(String uuid) {
    String bin = hexToBinary(uuid.replace("-", ""));
    return binaryToHex32(bin);
  }

  /**
   * 32进制 转 UUID.
   *
   * @param uuid32 UUID 32进制
   * @return UUID String
   */
  public static String uuid32ToUUID(String uuid32) {
    String bin = hex32ToBinary(uuid32);
    String uuid = binaryToHex(bin.substring(2));
    return formatUUID(uuid);
  }

  /**
   * UUID 转 64进制.
   *
   * @param uuid UUID String
   * @return UUID 64进制
   */
  public static String uuidToUUIDHex64(String uuid) {
    String bin = hexToBinary(uuid.replace("-", ""));
    return binaryToHex64(bin);
  }

  /**
   * 64进制 转 UUID.
   *
   * @param uuid64 UUID 64进制
   * @return UUID String
   */
  public static String uuid64ToUUID(String uuid64) {
    String bin = hex64ToBinary(uuid64);
    String uuid = binaryToHex(bin.substring(4));
    return formatUUID(uuid);
  }

  /**
   * 格式化UUID.
   *
   * @param uuid UUID String
   * @return UUID 含-
   */
  public static String formatUUID(String uuid) {
    if (uuid.length() != 32) {
      return uuid;
    }
    return String.format("%s-%s-%s-%s-%s", uuid.substring(0, 8), uuid.substring(8, 12),
        uuid.substring(12, 16), uuid.substring(16, 20), uuid.substring(20));
  }

  /**
   * 2进制转换16进制.
   *
   * @param binary 二进制String
   * @return 16进制String
   */
  public static String binaryToHex(String binary) {
    return binaryTo(binary, hexs, 4);
  }

  /**
   * 2进制转换32进制.
   *
   * @param binary 2进制
   * @return 32进制
   */
  public static String binaryToHex32(String binary) {
    return binaryTo(binary, hex32s, 5);
  }

  /**
   * 2进制转换64进制.
   *
   * @param binary 2进制
   * @return 64进制
   */
  public static String binaryToHex64(String binary) {
    return binaryTo(binary, hex64s, 6);
  }

  /**
   * 2进制转换为多进制(8,16,32,...).
   *
   * @param binary 二进制
   * @param chars 转换数组
   * @param bits 多进制占用2进制位数
   * @return 目的进制
   */
  private static String binaryTo(String binary, char[] chars, int bits) {
    if (StringUtils.isBlank(binary)) {
      return StringUtils.EMPTY;
    }
    String bin = binary;
    if (bin.length() % bits > 0) {
      bin = StringUtils.leftPad(bin, (bin.length() / bits + 1) * bits, "0");
    }
    try {
      StringBuffer sb = new StringBuffer();
      for (int i = 0; i < bin.length() / bits; i++) {
        String binaryChar = bin.substring(i * bits, i * bits + bits);
        Integer index = Integer.valueOf(binaryChar, 2);
        sb.append(chars[index]);
      }
      return sb.toString();
    } catch (Exception e) {
      return binary;
    }
  }

  /**
   * 16进制转换为2进制.
   *
   * @param hex 16进制
   * @return 2进制
   */
  public static String hexToBinary(String hex) {
    return toBinary(hex, hexs, 4);
  }

  /**
   * 32进制转换为2进制.
   *
   * @param hex 32进制
   * @return 2进制
   */
  public static String hex32ToBinary(String hex) {
    return toBinary(hex, hex32s, 5);
  }

  /**
   * 64进制转换为2进制.
   *
   * @param hex 64进制
   * @return 2进制
   */
  public static String hex64ToBinary(String hex) {
    return toBinary(hex, hex64s, 6);
  }

  /**
   * 多进制转换为2进制.
   *
   * @param source 原字符串
   * @param chars 进制char数组
   * @param bits 占用位数
   * @return 目的进制字符串
   */
  private static String toBinary(String source, char[] chars, int bits) {
    if (StringUtils.isBlank(source)) {
      return StringUtils.EMPTY;
    }
    StringBuffer sb = new StringBuffer();
    for (int i = 0; i < source.length(); i++) {
      char key = source.charAt(i);
      int index = indexOf(chars, key);
      if (index < 0) {
        return source;
      }
      sb.append(StringUtils.leftPad(Integer.toBinaryString(index), bits, "0"));
    }
    return sb.toString();
  }

  /**
   * 查询索引.
   *
   * @param chars 数组
   * @param key 键
   * @return 查找不到返回-1
   */
  public static int indexOf(char[] chars, char key) {
    for (int i = 0; i < chars.length; i++) {
      if (chars[i] == key) {
        return i;
      }
    }
    return -1;
  }

}
