package com.gitee.jmash.common.utils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/** 版本工具类. */
public class VersionUtil {
 
  /** 获取类编译时间快照. */
  public static String snapshot(Class<?> clazz) {
    // 获取编译时间作为版本快照，稳定后封装.
    File file = new File(clazz.getProtectionDomain().getCodeSource().getLocation().getPath());
    long timestamp = file.lastModified();
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmm");
    return dateFormat.format(new Date(timestamp));
  }
  
}
