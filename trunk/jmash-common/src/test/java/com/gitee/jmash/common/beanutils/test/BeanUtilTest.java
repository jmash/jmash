
package com.gitee.jmash.common.beanutils.test;

import com.crenjoy.proto.beanutils.ProtoBeanUtils;
import com.gitee.jmash.common.enums.ProtocolType;
import com.gitee.jmash.common.test.TestBean;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Custom BeanUtil 测试.
 *
 * @author CGD
 *
 */
public class BeanUtilTest {

  @Test
  public void testLocalDate() throws IllegalAccessException, InvocationTargetException {

    TestBean test = new TestBean();

    ProtoBeanUtils.setProperty(test, "testLocalDateTime", LocalDateTime.now());

    ProtoBeanUtils.setProperty(test, "testLocalDateTime", new Date());

    ProtoBeanUtils.setProperty(test, "testLocalDateTime", "2019-12-10 23:59:59");

  }

  @Test
  public void test() throws IllegalAccessException, InvocationTargetException {

    TestBean test = new TestBean();

    ProtoBeanUtils.setProperty(test, "testDate", new Date());

    ProtoBeanUtils.setProperty(test, "testBigDecimal", BigDecimal.TEN);

    ProtoBeanUtils.setProperty(test, "testEnum", ProtocolType.GRPC);

    ProtoBeanUtils.setProperty(test, "testDate", "2019-12-10 23:59:59");

    ProtoBeanUtils.setProperty(test, "testBigDecimal", "");

    ProtoBeanUtils.setProperty(test, "testEnum", ProtocolType.GRPC.name());

    ProtoBeanUtils.setProperty(test, "testEnum", (Integer) ProtocolType.GRPC.ordinal());

  }

  @Test
  public void test1()
      throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {

    TestBean test = new TestBean();

    ProtoBeanUtils.setProperty(test, "testDate", new Date());

    ProtoBeanUtils.setProperty(test, "testBigDecimal", BigDecimal.TEN);

    ProtoBeanUtils.setProperty(test, "testEnum", ProtocolType.GRPC);

    ProtoBeanUtils.setProperty(test, "testDate", "2019-12-10 23:59:59");

    ProtoBeanUtils.setProperty(test, "testBigDecimal", "");

    ProtoBeanUtils.setProperty(test, "testEnum", ProtocolType.GRPC.name());

    ProtoBeanUtils.setProperty(test, "testEnum", (Integer) ProtocolType.GRPC.ordinal());

    Assertions.assertEquals(ProtoBeanUtils.getProperty(test, "testBigDecimal"), null);

    Assertions.assertEquals(ProtoBeanUtils.getProperty(test, "testEnum"), ProtocolType.GRPC.name());

  }

}
