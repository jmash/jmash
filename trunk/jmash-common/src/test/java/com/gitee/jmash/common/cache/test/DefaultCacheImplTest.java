
package com.gitee.jmash.common.cache.test;

import com.gitee.jmash.common.cache.Cache;
import com.gitee.jmash.common.cache.DefaultCacheImpl;
import java.time.Instant;
import java.util.Collection;
import java.util.Set;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * 内存缓存测试.
 *
 * @author CGD
 *
 */
public class DefaultCacheImplTest {

  Cache<String, Object> cache;

  @BeforeEach
  public void setUpBeforeClass() throws Exception {
    cache = new DefaultCacheImpl();
  }

  @AfterEach
  public void tearDownAfterClass() throws Exception {
    cache.clear();
  }

  @Test
  public void testGet() {
    cache.put("key1", "value1");

    Assertions.assertEquals("value1", cache.get("key1"));
  }

  @Test
  public void testClear() {
    cache.put("key1", "value1");
    Assertions.assertEquals("value1", cache.get("key1"));

    cache.clear();

    Assertions.assertNull(cache.get("key1"));
  }

  @Test
  public void testContainsKey() {
    cache.put("key1", "value1");
    Assertions.assertTrue(cache.containsKey("key1"));
  }

  @Test
  public void testKeySet() {
    cache.put("key1", "value1");
    cache.put("key2", "value2");
    cache.put("key3", "value3");

    Set<String> keys = cache.keySet();

    Assertions.assertTrue(keys.contains("key1"));
    Assertions.assertTrue(keys.contains("key2"));
    Assertions.assertTrue(keys.contains("key3"));
  }

  @Test
  public void testPutObjectObject() {
    cache.put("key1", "value1");
    Assertions.assertEquals("value1", cache.get("key1"));
  }

  @Test
  public void testRemove() {
    cache.put("key1", "value1");
    Assertions.assertEquals("value1", cache.get("key1"));

    cache.remove("key1");
    Assertions.assertNull(cache.get("key1"));
  }

  @Test
  public void testSize() {
    cache.put("key1", "value1");
    cache.put("key2", "value2");
    cache.put("key3", "value3");
    cache.put("key3", "value4");

    Assertions.assertEquals(3, cache.size());
  }

  @Test
  public void testValues() {
    cache.put("key1", "value1");
    cache.put("key2", "value2");
    cache.put("key3", "value3");
    cache.put("key3", "value4");

    Collection<Object> values = cache.values();

    Assertions.assertTrue(values.contains("value1"));
    Assertions.assertTrue(values.contains("value2"));
    Assertions.assertTrue(values.contains("value4"));
    Assertions.assertFalse(values.contains("value3"));
  }

  @Test
  public void testPutStringObjectDate() {

    cache.put("key1", "value1", Instant.now().plusSeconds(1));
    Assertions.assertEquals("value1", cache.get("key1"));

    cache.put("key2", "value2", 5);
    Assertions.assertEquals("value2", cache.get("key2"));

    try {
      Thread.sleep(2 * 1000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    Assertions.assertNull(cache.get("key1"));

    try {
      Thread.sleep(2 * 1000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    Assertions.assertNotNull(cache.get("key2"));
  }

}
