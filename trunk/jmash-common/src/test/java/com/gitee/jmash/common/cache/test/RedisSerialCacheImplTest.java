
package com.gitee.jmash.common.cache.test;

import com.gitee.jmash.common.cache.RedisCacheImpl;
import com.gitee.jmash.common.cache.SerialCache;
import com.gitee.jmash.common.redis.RedisProps;
import com.google.protobuf.StringValue;
import java.io.Serializable;
import java.time.Instant;
import java.util.Collection;
import java.util.Set;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

/**
 * 测试Redis Impl.
 *
 * @author CGD
 *
 */
@Disabled
public class RedisSerialCacheImplTest {

  static SerialCache cache;

  /**
   * Setup.
   */
  @BeforeAll
  public static void setUpBeforeClass() {
    RedisProps redisProps = new RedisProps();
    redisProps.setClientName("TestClient");
    // kubectl port-forward -n jmash redis 6379:6379
    RedisCacheImpl impl = new RedisCacheImpl(redisProps);
    cache = impl;
  }

  @AfterAll
  public static void tearDownAfterClass() throws Exception {
    cache.clear();
  }

  @Test
  public void testGet() {
    cache.put("key1", StringValue.of("value1"));

    Assertions.assertEquals(StringValue.of("value1"), cache.get("key1"));
  }

  @Test
  public void testClear() {
    cache.put("key1", StringValue.of("value1"));
    Assertions.assertEquals(StringValue.of("value1"), cache.get("key1"));

    cache.clear();

    Assertions.assertNull(cache.get("key1"));
  }

  @Test
  public void testContainsKey() {
    cache.remove("key1");
    Assertions.assertFalse(cache.containsKey("key1"));
    cache.put("key1", StringValue.of("value1"));
    Assertions.assertTrue(cache.containsKey("key1"));
  }

  @Test
  public void testKeySet() {
    cache.put("key1", StringValue.of("value1"));
    cache.put("key2", StringValue.of("value2"));
    cache.put("key3", StringValue.of("value3"));

    Set<String> keys = cache.keySet();

    Assertions.assertTrue(keys.contains("key1"));
    Assertions.assertTrue(keys.contains("key2"));
    Assertions.assertTrue(keys.contains("key3"));
  }

  @Test
  public void testPutObjectObject() {
    cache.put("key1", StringValue.of("value1"));
    Assertions.assertEquals(StringValue.of("value1"), cache.get("key1"));
  }

  @Test
  public void testRemove() {
    cache.put("key1", StringValue.of("value1"));
    Assertions.assertEquals(StringValue.of("value1"), cache.get("key1"));

    cache.remove("key1");
    Assertions.assertNull(cache.get("key1"));
  }

  @Test
  public void testSize() {
    cache.put("key1", StringValue.of("value1"));
    cache.put("key2", StringValue.of("value2"));
    cache.put("key3", StringValue.of("value3"));
    cache.put("key3", StringValue.of("value4"));

    Assertions.assertEquals(3, cache.size());
  }

  @Test
  public void testValues() {
    cache.put("key1", StringValue.of("value1"));
    cache.put("key2", StringValue.of("value2"));
    cache.put("key3", StringValue.of("value3"));
    cache.put("key3", StringValue.of("value4"));

    Collection<Serializable> values = cache.values();

    Assertions.assertTrue(values.contains(StringValue.of("value1")));
    Assertions.assertTrue(values.contains(StringValue.of("value2")));
    Assertions.assertTrue(values.contains(StringValue.of("value4")));
    Assertions.assertFalse(values.contains(StringValue.of("value3")));
  }

  @Test
  public void testPutStringObjectDate() {

    cache.put("key1", StringValue.of("value1"), Instant.now().plusSeconds(1));
    Assertions.assertEquals(StringValue.of("value1"), cache.get("key1"));

    cache.put("key2", StringValue.of("value2"), 5);
    Assertions.assertEquals(StringValue.of("value2"), cache.get("key2"));

    try {
      Thread.sleep(2 * 1000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    Assertions.assertNull(cache.get("key1"));

    try {
      Thread.sleep(2 * 1000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    Assertions.assertNotNull(cache.get("key2"));
  }
}
