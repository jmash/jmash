
package com.gitee.jmash.common.i18n;

import com.gitee.jmash.common.i18n.MessageSource.MessageResourceBundle;
import java.util.Currency;
import java.util.Locale;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * MessageSource Test.
 *
 * @author CGD
 *
 */
public class MessageSourceTest {

  private MessageSource messageSource = MessageSource.setBaseNames("messages", "jmash.messages");

  private MessageResourceBundle chinese = messageSource.setLocale(Locale.CHINESE);
  private MessageResourceBundle us = messageSource.setLocale(Locale.US);

  @Test
  public void testAllGetString() {
    Assertions.assertEquals("JMash", chinese.getString("name"));
    Assertions.assertEquals("JMash", messageSource.getString(Locale.CHINESE, "name"));
    Assertions.assertEquals("JMash", us.getString("name"));
    Assertions.assertEquals("JMash", messageSource.getString(Locale.US, "name"));

    Assertions.assertEquals("JMash", messageSource.setLocale(null).getString("name"));
    Assertions.assertEquals("JMash", messageSource.getString(null, "name"));
  }

  @Test
  public void testNull() {
    Assertions.assertEquals("", chinese.getString("name1"));
    Assertions.assertEquals("", messageSource.getString(Locale.US, "name1"));
    Assertions.assertEquals("", us.format("name1", "--"));
    Assertions.assertEquals("", messageSource.format(Locale.US, "name1", "--"));
  }

  @Test
  public void testGetString() {
    Assertions.assertEquals("语言", chinese.getString("language.select"));
    Assertions.assertEquals("语言", messageSource.getString(Locale.CHINESE, "language.select"));
    Assertions.assertEquals("language", us.getString("language.select"));
    Assertions.assertEquals("language", messageSource.getString(Locale.US, "language.select"));
  }

  @Test
  public void testFormat() {
    Assertions.assertEquals("版权所有©jmash.crenjoy.com",
        chinese.format("copyright", "jmash.crenjoy.com"));
    Assertions.assertEquals("版权所有©jmash.crenjoy.com",
        messageSource.format(Locale.CHINESE, "copyright", "jmash.crenjoy.com"));
    Assertions.assertEquals("copyright©jmash.crenjoy.com",
        us.format("copyright", "jmash.crenjoy.com"));
    Assertions.assertEquals("copyright©jmash.crenjoy.com",
        messageSource.format(Locale.US, "copyright", "jmash.crenjoy.com"));
  }

  @Test
  public void testFormatNumber() {
    Assertions.assertEquals("658,671.25", messageSource.formatNumber(Locale.CHINESE, 658671.23f));
    Assertions.assertEquals("658.671,25", messageSource.formatNumber(Locale.GERMAN, 658671.23f));
    Assertions.assertEquals("658,671.25", messageSource.formatNumber(null, 658671.23f));
  }

  @Test
  public void testFormatCurrency() {
    System.out.println(Currency.getInstance("CNY").getSymbol());
    // Assertions.assertEquals(Currency.getInstance("CNY").getSymbol(), "¥");
    Assertions.assertEquals("$658,671.25", us.formatCurrency(658671.23f));

    Assertions.assertNotNull(messageSource.formatCurrency(null, 658671.23f));
  }

}
