
package com.gitee.jmash.common.redis.test;

import com.gitee.jmash.common.redis.RedisProps;
import com.gitee.jmash.common.redis.RedisUtil;
import com.gitee.jmash.common.redis.test.impl.RadisTest;
import com.gitee.jmash.common.redis.test.impl.RadisTestImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import redis.clients.jedis.JedisPool;

/**
 * 测试Redis .
 *
 * @author CGD
 *
 */
public class RadisTestClass {

  static RadisTest radisTest;

  /**
   * Set Up.
   */
  @BeforeAll
  public static void setUp() {
    RedisProps redisProps = new RedisProps();
    redisProps.setClientName("TestClient");
    JedisPool jdeisPool = RedisUtil.getJedisPool(redisProps);
    // kubectl port-forward -n jmash redis 6379:6379
    radisTest = RedisUtil.proxy(jdeisPool, new RadisTestImpl());
  }

  @Test
  public void testNone() {
    radisTest.testNone();
  }

  @Test
  public void testMulti() {
    radisTest.testMulti();
  }

  @Test
  public void testWatch() {
    radisTest.testWatch();
  }
}
