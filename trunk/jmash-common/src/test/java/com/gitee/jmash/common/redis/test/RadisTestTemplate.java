
package com.gitee.jmash.common.redis.test;

import com.gitee.jmash.common.redis.RedisProps;
import com.gitee.jmash.common.redis.RedisUtil;
import com.gitee.jmash.common.redis.callback.RadisTransCallback;
import com.gitee.jmash.common.redis.callback.RedisCallback;
import com.gitee.jmash.common.redis.callback.RedisTemplate;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.Transaction;

/**
 * Template Test.
 *
 * @author CGD
 *
 */
public class RadisTestTemplate {

  static RedisTemplate redisTemplate;

  /**
   * Set Up.
   */
  @BeforeAll
  public static void setUp() {
    RedisProps redisProps = new RedisProps();
    redisProps.setClientName("TestClient");
    // kubectl port-forward -n jmash redis 6379:6379
    JedisPool jdeisPool = RedisUtil.getJedisPool(redisProps);
    redisTemplate = new RedisTemplate(jdeisPool);
  }

  @Test
  public void testNone() {
    final String key = "key1";
    redisTemplate.transactionNone(new RedisCallback<Boolean>() {
      public Boolean call(Jedis jedis) {
        jedis.set(key, "value1");
        String value = jedis.get(key);
        Assertions.assertEquals(value, "value1");
        jedis.del(key);
        return true;
      }

    });
  }

  @Test
  public void testMulti() {
    final String key = "key1";
    redisTemplate.transactionMulti(new RadisTransCallback<Boolean>() {
      public Boolean call(Transaction transaction) {
        transaction.set(key, "value2");
        transaction.del(key);
        return true;
      }
    });
  }

}
