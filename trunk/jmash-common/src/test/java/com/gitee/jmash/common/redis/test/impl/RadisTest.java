
package com.gitee.jmash.common.redis.test.impl;

/**
 * Redis Test .
 *
 * @author CGD
 *
 */
public interface RadisTest {

  public void testNone();

  public void testMulti();

  public void testWatch();

}
