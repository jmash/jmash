
package com.gitee.jmash.common.redis.test.impl;

import com.gitee.jmash.common.redis.proxy.RedisTransaction;
import com.gitee.jmash.common.redis.proxy.TransactionRadis;
import com.gitee.jmash.common.redis.proxy.TransactionType;
import org.junit.jupiter.api.Assertions;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Transaction;

/**
 * Redis Test Impl.
 *
 * @author CGD
 *
 */
public class RadisTestImpl implements RadisTest, RedisTransaction {

  private Jedis jedis;

  private Transaction transaction;

  public RadisTestImpl() {
    super();
  }

  /** None. */
  @TransactionRadis(TransactionType.NONE)
  public void testNone() {
    getJedis().set("key1", "value1");
    getJedis().set("key2", "value2");
    getJedis().set("key3", "value3");
    Assertions.assertEquals(getJedis().get("key1"), "value1");
    getJedis().del("key1");
    getJedis().del("key2");
    getJedis().del("key3");
  }

  /** MULTI. */
  @TransactionRadis(TransactionType.MULTI)
  public void testMulti() {
    getTransaction().set("key1", "value1");
    getTransaction().set("key2", "value2");
    getTransaction().set("key3", "value3");
    getTransaction().del("key1");
    getTransaction().del("key2");
    getTransaction().del("key3");
  }

  /** WATCH. */
  @TransactionRadis(value = TransactionType.WATCH, WatchKey = { "key1", "key2" })
  public void testWatch() {
    getTransaction().set("key1", "value1");
    getTransaction().set("key2", "value2");
    getTransaction().set("key3", "value3");
    getTransaction().del("key1");
    getTransaction().del("key2");
    getTransaction().del("key3");
  }

  public Jedis getJedis() {
    return jedis;
  }

  public void setJedis(Jedis jedis) {
    this.jedis = jedis;
  }

  public Transaction getTransaction() {
    return transaction;
  }

  public void setTransaction(Transaction transaction) {
    this.transaction = transaction;
  }

}
