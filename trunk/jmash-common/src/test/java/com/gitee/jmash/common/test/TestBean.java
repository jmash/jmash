
package com.gitee.jmash.common.test;

import com.gitee.jmash.common.enums.ProtocolType;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;

/**
 * Test Bean.
 *
 * @author CGD
 *
 */
public class TestBean implements Serializable {

  private static final long serialVersionUID = 1L;

  private LocalDateTime testLocalDateTime;

  private LocalDate testLocalDate;

  private LocalTime testLocalTime;

  private Instant testInstant;

  private Date testDate;

  private BigDecimal testBigDecimal;

  private ProtocolType testEnum;

  private String testString;

  private Integer testInteger;

  private Long testLong;

  private Double testDouble;

  public LocalDateTime getTestLocalDateTime() {
    return testLocalDateTime;
  }

  public void setTestLocalDateTime(LocalDateTime testLocalDateTime) {
    this.testLocalDateTime = testLocalDateTime;
  }

  public LocalDate getTestLocalDate() {
    return testLocalDate;
  }

  public void setTestLocalDate(LocalDate testLocalDate) {
    this.testLocalDate = testLocalDate;
  }

  public LocalTime getTestLocalTime() {
    return testLocalTime;
  }

  public void setTestLocalTime(LocalTime testLocalTime) {
    this.testLocalTime = testLocalTime;
  }

  public Instant getTestInstant() {
    return testInstant;
  }

  public void setTestInstant(Instant testInstant) {
    this.testInstant = testInstant;
  }

  public static long getSerialversionuid() {
    return serialVersionUID;
  }

  public BigDecimal getTestBigDecimal() {
    return testBigDecimal;
  }

  public void setTestBigDecimal(BigDecimal testBigDecimal) {
    this.testBigDecimal = testBigDecimal;
  }

  public ProtocolType getTestEnum() {
    return testEnum;
  }

  public void setTestEnum(ProtocolType testEnum) {
    this.testEnum = testEnum;
  }

  public String getTestString() {
    return testString;
  }

  public void setTestString(String testString) {
    this.testString = testString;
  }

  public Date getTestDate() {
    return testDate;
  }

  public void setTestDate(Date testDate) {
    this.testDate = testDate;
  }

  public Integer getTestInteger() {
    return testInteger;
  }

  public void setTestInteger(Integer testInteger) {
    this.testInteger = testInteger;
  }

  public Long getTestLong() {
    return testLong;
  }

  public void setTestLong(Long testLong) {
    this.testLong = testLong;
  }

  public Double getTestDouble() {
    return testDouble;
  }

  public void setTestDouble(Double testDouble) {
    this.testDouble = testDouble;
  }

}
