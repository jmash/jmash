
package com.gitee.jmash.common.text.test;

import com.gitee.jmash.common.text.StringUtil;
import java.io.File;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * StringUtil测试.
 *
 * @author CGD
 *
 */
public class StringUtilTest {

  @Test
  public void test() {
    String a = StringUtil.camelToUnderline("CatMat");
    Assertions.assertEquals("cat_mat", a);
    a = StringUtil.camelToUnderline("catMat");
    Assertions.assertEquals("cat_mat", a);
  }

  @Test
  public void test1() {
    String a = StringUtil.lowerCaseFirst("CatMat");
    Assertions.assertEquals("catMat", a);
    a = StringUtil.upperCaseFirst("catMat");
    Assertions.assertEquals("CatMat", a);
  }

  @Test
  public void test2() {
    String a = StringUtil.packageDir("com.gitee.jmash.common.text.test");
    if (StringUtils.equals(File.separator, "/")) {
      Assertions.assertEquals("com/gitee/jmash/common/text/test", a);
    } else {
      Assertions.assertEquals("com\\gitee\\jmash\\common\\text\\test", a);
    }

  }

}
