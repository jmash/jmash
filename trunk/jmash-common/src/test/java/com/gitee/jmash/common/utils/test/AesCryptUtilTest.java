
package com.gitee.jmash.common.utils.test;

import com.gitee.jmash.common.utils.AesCryptUtil;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

/**
 * AES 加解密.
 *
 * @author CGD
 *
 */
public class AesCryptUtilTest {

  @Test
  public void test() throws Exception {
    AesCryptUtil aes = new AesCryptUtil("hz1Virh4EX0SiO8ccCZoUQ==");
    String context = "test";
    String result = aes.encrypt(context);
    System.out.println(result);
    String context2 = aes.decrypt(result);
    Assertions.assertEquals(context, context2);
  }
  
  @Test
  public void test1() throws Exception {
    AesCryptUtil aes = new AesCryptUtil("VrGs6iJSlrVzBwZLxj0H0w==");
    String context = "test";
    String result = aes.encrypt(context);
    System.out.println(result);
    String context2 = aes.decrypt(result);
    Assertions.assertEquals(context, context2);
  }

}
