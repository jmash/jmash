
package com.gitee.jmash.common.utils.test;

import com.gitee.jmash.common.utils.BasicAuthzUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Basic Authentication Util Test.
 *
 * @author CGD
 *
 */
public class BasicAuthzUtilTest {

  @Test
  public void test() {
    String userName = "test";
    String password = "pwd123456";
    String authz = BasicAuthzUtil.encodeBasicAuthz(userName, password);

    String[] creds = BasicAuthzUtil.decodeBasicAuthz(authz);

    Assertions.assertEquals(userName, creds[0]);
    Assertions.assertEquals(password, creds[1]);

  }

}
