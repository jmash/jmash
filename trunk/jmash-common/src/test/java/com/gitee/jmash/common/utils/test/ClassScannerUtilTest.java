
package com.gitee.jmash.common.utils.test;

import com.gitee.jmash.common.redis.RedisConfig;
import com.gitee.jmash.common.utils.ClassScannerUtil;
import com.gitee.jmash.common.utils.EnumUtil;
import jakarta.enterprise.context.ApplicationScoped;
import java.util.List;
import org.apache.commons.beanutils.BeanUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * ClassScannerUtil Test.
 *
 * @author CGD
 *
 */
public class ClassScannerUtilTest {

  @Test
  public void testAnnotated() {
    List<Class<?>> list1 = ClassScannerUtil.scanAnnotatedClasses(RedisConfig.class.getPackage(),
        true, ApplicationScoped.class);

    Assertions.assertTrue(list1.size() > 0);

  }

  @Test
  public void testScanDir() {
    List<Class<?>> list1 = ClassScannerUtil.scanClasses(EnumUtil.class.getPackage(), true);
    List<Class<?>> list2 = ClassScannerUtil.scanClasses(EnumUtil.class.getPackage(), false);

    Assertions.assertTrue(list1.size() > 0);
    Assertions.assertTrue(list2.size() > 0);

    Assertions.assertTrue(list1.size() > list2.size());
  }

  @Test
  public void testScanJar() {
    List<Class<?>> list1 = ClassScannerUtil.scanClasses(BeanUtils.class.getPackage(), true);
    List<Class<?>> list2 = ClassScannerUtil.scanClasses(BeanUtils.class.getPackage(), false);

    Assertions.assertTrue(list1.size() > 0);
    Assertions.assertTrue(list2.size() > 0);

    Assertions.assertTrue(list1.size() > list2.size());
  }

}
