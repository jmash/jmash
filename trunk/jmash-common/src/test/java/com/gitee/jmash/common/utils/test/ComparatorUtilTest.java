
package com.gitee.jmash.common.utils.test;

import com.gitee.jmash.common.utils.ComparatorUtil;
import org.junit.Assert;
import org.junit.Test;

/** 比较工具. */
public class ComparatorUtilTest {

  @Test
  public void test() {
    Assert.assertEquals(ComparatorUtil.compare(1, 2), -1);
    Assert.assertEquals(ComparatorUtil.compare(1.0f, 2.0f), -1);
    Assert.assertEquals(ComparatorUtil.compare("1", "2"), -1);

    Assert.assertEquals(ComparatorUtil.compare(1, 1), 0);
    Assert.assertEquals(ComparatorUtil.compare(1f, 1f), 0);
    Assert.assertEquals(ComparatorUtil.compare("1", "1"), 0);

    Assert.assertEquals(ComparatorUtil.compare(2, 1), 1);
    Assert.assertEquals(ComparatorUtil.compare(2f, 1f), 1);
    Assert.assertEquals(ComparatorUtil.compare("2", "1"), 1);

    Assert.assertEquals(ComparatorUtil.compare(null, "1"), -1);
    Assert.assertEquals(ComparatorUtil.compare("2", null), 1);
    Assert.assertEquals(ComparatorUtil.compare(null, null), 0);
  }

}
