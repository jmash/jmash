
package com.gitee.jmash.common.utils.test;

import com.gitee.jmash.common.enums.ProtocolType;
import com.gitee.jmash.common.utils.EnumUtil;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Enum 工具类测试.
 *
 * @author CGD
 *
 */
public class EnumUtilTest {

  @Test
  public void testEnumToString() {
    String str = EnumUtil.enumsToStr(ProtocolType.values());
    ProtocolType[] enums = EnumUtil.strToEnums(ProtocolType.class, str);
    Assertions.assertEquals(enums.length, ProtocolType.values().length);
  }

  @Test
  public void testEnumArrays() {
    ProtocolType[] enums = EnumUtil.getEnumArrays(ProtocolType.class);
    Assertions.assertEquals(enums.length, ProtocolType.values().length);
  }

  @Test
  public void testEnumList() {
    List<ProtocolType> enumList = EnumUtil.getEnumList(ProtocolType.class);
    Assertions.assertEquals(enumList, Arrays.asList(ProtocolType.values()));
  }

  @Test
  public void testEnumMaps() {
    Map<String, ProtocolType> enumMap = EnumUtil.getEnumMap(ProtocolType.class);
    Map<String, ProtocolType> v2 = Arrays.asList(ProtocolType.values()).stream()
        .collect(Collectors.toMap(a -> a.name(), Function.identity(), (a, b) -> a));
    Assertions.assertEquals(enumMap, v2);
  }

  @Test
  public void testEnumByName() {
    ProtocolType value = EnumUtil.valueOf(ProtocolType.class, "HTTP");
    Assertions.assertEquals(value, ProtocolType.HTTP);

    ProtocolType value1 = EnumUtil.valueOf(ProtocolType.class, "TCP");
    Assertions.assertEquals(value1, ProtocolType.TCP);

    ProtocolType value2 = EnumUtil.valueOf(ProtocolType.class, "GRPC");
    Assertions.assertEquals(value2, ProtocolType.GRPC);

  }

  @Test
  public void testEnumByValue() {
    ProtocolType value = EnumUtil.valueOf(ProtocolType.class, 0);
    Assertions.assertEquals(value, ProtocolType.HTTP);

    ProtocolType value1 = EnumUtil.valueOf(ProtocolType.class, 1);
    Assertions.assertEquals(value1, ProtocolType.TCP);

    ProtocolType value2 = EnumUtil.valueOf(ProtocolType.class, 2);
    Assertions.assertEquals(value2, ProtocolType.GRPC);
  }

  @Test
  public void testEnumToObject() {
    Object obj = EnumUtil.enumToObject(ProtocolType.TCP, ProtocolType.class);
    Assertions.assertEquals(obj, ProtocolType.TCP);

    obj = EnumUtil.enumToObject(ProtocolType.TCP, String.class);
    Assertions.assertEquals(obj, "TCP");

    obj = EnumUtil.enumToObject(ProtocolType.TCP, int.class);
    Assertions.assertEquals(obj, 1);

    obj = EnumUtil.enumToObject(ProtocolType.GRPC, Integer.class);
    Assertions.assertEquals(obj, 2);

    obj = EnumUtil.enumToObject(null, Integer.class);
    Assertions.assertEquals(obj, null);

    obj = EnumUtil.enumToObject(null, ProtocolType.class);
    Assertions.assertEquals(obj, null);

    obj = EnumUtil.enumToObject(null, String.class);
    Assertions.assertEquals(obj, null);

    obj = EnumUtil.enumToObject(ProtocolType.GRPC, Integer.class.getName());
    Assertions.assertEquals(obj, 2);
  }

  @Test
  public void testObjectToEnum() {
    Enum<?> obj = EnumUtil.objectToEnum(ProtocolType.class, "TCP");
    Assertions.assertEquals(obj, ProtocolType.TCP);

    obj = EnumUtil.objectToEnum(ProtocolType.class, ProtocolType.GRPC);
    Assertions.assertEquals(obj, ProtocolType.GRPC);

    obj = EnumUtil.objectToEnum(ProtocolType.class, 2);
    Assertions.assertEquals(obj, ProtocolType.GRPC);

    obj = EnumUtil.objectToEnum(ProtocolType.class, 0);
    Assertions.assertEquals(obj, ProtocolType.HTTP);

    obj = EnumUtil.objectToEnum(ProtocolType.class, "HTTP");
    Assertions.assertEquals(obj, ProtocolType.HTTP);

    obj = EnumUtil.objectToEnum(ProtocolType.class, null);
    Assertions.assertEquals(obj, null);

    obj = EnumUtil.objectToEnum(ProtocolType.class.getName(), ProtocolType.GRPC);
    Assertions.assertEquals(obj, ProtocolType.GRPC);

    obj = EnumUtil.objectToEnum(ProtocolType.class.getName(), "HTTP");
    Assertions.assertEquals(obj, ProtocolType.HTTP);
  }

}
