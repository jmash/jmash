
package com.gitee.jmash.common.utils.test;

import com.gitee.jmash.common.utils.ExecUtil;
import java.io.ByteArrayOutputStream;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * exec 工具类测试.
 *
 * @author CGD
 *
 */
public class ExecUtilTest {

  @Test
  public void testExec() {
    int x = ExecUtil.exec("java", "-version");
    Assertions.assertEquals(x, 0);
  }

  // 测试结果输出
  @Test
  public void testExecWithOutputStream() {
    ByteArrayOutputStream errorStream = new ByteArrayOutputStream();
    ByteArrayOutputStream stream = new ByteArrayOutputStream();
    int x = ExecUtil.exec(errorStream, stream, "java", "-version");
    Assertions.assertEquals(x, 0);

    Assertions.assertEquals(StringUtils.contains(errorStream.toString(), "Runtime"), true);
  }
}
