
package com.gitee.jmash.common.utils.test;

import com.gitee.jmash.common.utils.FileHashUtil;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * File Hash Util Test.
 *
 * @author CGD
 *
 */
public class FileHashUtilTest {

  private static Log log = LogFactory.getLog("");

  @Test
  public void test() throws NoSuchAlgorithmException {
    // String fileName="D:\\test\\easypr.zip";
    // testFile(fileName,new
    // String[]{FileHashUtil.MD5,FileHashUtil.SHA1,FileHashUtil.SHA256,FileHashUtil.CRC32});
    String[] hashs = FileHashUtil.getByteArgHashString("0123456".getBytes(), new String[] {
        FileHashUtil.MD5, FileHashUtil.SHA1, FileHashUtil.SHA256, FileHashUtil.CRC32 });
    Assertions.assertEquals(hashs[0], "124bd1296bec0d9d93c7b52a71ad8d5b");
    Assertions.assertEquals(hashs[1], "b7ed088190c204b31cd71484e6a1c538986b5f77");
    Assertions.assertEquals(hashs[2],
        "5f6121bc06e18e209920d57d2f16b17cc82dfc2ade1d375d6951b99c65d1b89d");
    Assertions.assertEquals(hashs[3], "8dbf08ee");
  }

  /**
   * Test File.
   */
  public void testFile(String fileName, String[] algorithms) {
    String[] hashs;
    Date start = new Date();
    hashs = FileHashUtil.getFileHashString(fileName, algorithms);
    Date end = new Date();
    long t = end.getTime() - start.getTime();

    log.info(String.format("time:%d", t));

    for (int i = 0; i < algorithms.length; i++) {
      log.info(String.format(" %s : %s", algorithms[i], hashs[i]));
    }

  }

}
