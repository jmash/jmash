
package com.gitee.jmash.common.utils.test;

import com.gitee.jmash.common.enums.MimeType;
import com.gitee.jmash.common.utils.ImageUtil;
import java.awt.image.BufferedImage;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Image 工具类测试.
 *
 * @author CGD
 *
 */
public class ImageUtilTest {

  @Test
  public void testBase64Png() {
    BufferedImage image = ImageUtil.imageFromBase64Image(jpgBase64);
    Assertions.assertNotNull(image);

    // PNG 无损
    undamagedCheck(image, MimeType.M_png);
  }

  /**
   * 无损压缩检查.
   */
  private void undamagedCheck(BufferedImage image, MimeType mimeType) {
    String base64P1 = ImageUtil.imageToBase64Image(image, mimeType);
    Assertions.assertNotEquals(base64P1, StringUtils.EMPTY);

    BufferedImage image2 = ImageUtil.imageFromBase64Image(base64P1);

    Assertions.assertNotNull(image2);
    String base64P2 = ImageUtil.imageToBase64Image(image2, mimeType);

    // LogFactory.getLog(ImageUtilTest.class).info(base64P1);
    // LogFactory.getLog(ImageUtilTest.class).info(base64P2);
    Assertions.assertEquals(base64P1, base64P2);
  }

  @Test
  public void testImageMimeType() {
    MimeType mimeType = ImageUtil.imageMimeType(jpgBase64);
    Assertions.assertEquals(mimeType.getMimeType(), MimeType.M_jpg.getMimeType());
  }

  @Test
  public void testJpg() {
    BufferedImage image = ImageUtil.imageFromBase64Image(jpgBase64);
    Assertions.assertNotNull(image);
    String jpg = ImageUtil.imageToBase64Image(image);
    Assertions.assertNotEquals(jpg, StringUtils.EMPTY);

    // Assertions.assertEquals(jpg, jpgBase64);
    MimeType mimeType = ImageUtil.imageMimeType(jpg);
    Assertions.assertEquals(mimeType.getMimeType(), MimeType.M_jpg.getMimeType());
  }

  @Test
  public void test() {
    BufferedImage image = ImageUtil.imageFromBase64Image(jpgBase64);
    Assertions.assertNotNull(image);
    String webp = ImageUtil.imageToBase64Image(image, MimeType.M_webp);
    Assertions.assertNotEquals(webp, StringUtils.EMPTY);
    Assertions.assertTrue(webp.length() < jpgBase64.length() * 4 / 5);
    MimeType mimeType = ImageUtil.imageMimeType(webp);
    Assertions.assertEquals(mimeType.getMimeType(), MimeType.M_webp.getMimeType());
  }

  String jpgBase64 = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAoHBwgHBgoICAgLCgoL"
      + "DhgQDg0NDh0VFhEYIx8lJCIfIiEmKzcvJik0KSEiMEExNDk7Pj4+JS5ESUM8SDc9Pjv/2wBDAQoLCw4NDhwQEBw7"
      + "KCIoOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozv/wAARCAAyAOoDASIA"
      + "AhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQR"
      + "BRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZ"
      + "WmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW"
      + "19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgEC"
      + "BAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2"
      + "Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0"
      + "tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwC0xUAhcDn1+vvT"
      + "GcZIz69/r71ECxfBJ/zn3pASCxzzz3+vvXxXMz7lRJHmCggZ6+v196Y0rtk7sDPGPx96Q9Pf/wDX70Djkdee/wBf"
      + "eqKSQ2RyActk/wD6/eoCcZ5/X61JK2Seenv9feo2PX/Pr70mzaKsIev/ANf6+9LwMndk+n50AgNyevv9fehiuT1/"
      + "A/X3qkUN38nJOPr9femtlec5H1+vvTyAc7W/X6007hx/X/69MpCH2wR/+ukPXsv0/GgjGSePx/8Ar0p54zx35q0M"
      + "aWCg88DPf6+9Mz8x+b8aV23HAzgUrIxxhTTKQqJK8wVMk59f/r10ksEcyBZVDY9T06+9Y1leNZgqYAcnls4Pf3rZ"
      + "huFni3j5euQT06+9aRPPxTndO2iM/WSsNj5cahd7dvx96wPeumu7mzlR4ZXyOR9Dz71jNboG2q+VB6+vX3oZ0YWf"
      + "LC0kVd5BO4H3pNxQcjA579aumJdxHXGe/wBaayIoGQOvr9feqsdPtEVVbO7BpkuTEMD/ADzV8hNxxjoabkEnnAA4"
      + "/WqGp67GeS5VuDmqmragNOg3Iyeew+VWPT3/APrVqFlCsSw4zgZ6nmj4eaBbeINVvdU1CJbhbciNFcZXeeScew4r"
      + "rwtJTblLZHl5rjZ0YKFPRy69kcTLqt9MpWS4mTec4U/KT9BxU9rrN4jLFMwljUY2sBkD2PX8692uvDGiy2zi4sLU"
      + "R7eWZFXH415l4k+H4hFzqOhXKSwRL5jW5bJGOu09xjtXqXi1ytaHysZ1IT9pCTuRWflXFsJ4XLpIMg9D9KlEUQXa"
      + "4OR6msLQLt47eaFTjawPTHUVq+fJIjAtnAzmvIqw9nNxR91ga08Th41Xu/8AhieRYyu0D86iKR56D/P41WLsc5Y/"
      + "iaZk56mszvVN9z0doIbaFyigEA8k89/f/P8ALFJ6rnnPXP1966GZ1RTvOBg9fx96o/YDeSSGzQyFFLtg/dHPPWuO"
      + "cG7KJ8xRq2u5feZpwMkHHvn6+9Ru/UA59/z96sMwxxj6g/X3ppPXn9fr71gztUiqVbtn8/r70GNueP1+vvWlaade"
      + "X+/7LA8uz723t196qnPODnBPf6+9VyO17DVVN2QkOnyTjcHUAHnn6+9QyWxjdlZsY/8Ar+9XoL1rcFeNvPf6+9QS"
      + "zNNIWZ8k+/196dlbQUZ1OZ32KxhAz8x/zn3pSgwRk/n9fermoWN1pjIl5GYmkBKgkcj86qNMig8E/j9feqcXF2Zp"
      + "GfOrxd0GF5B5/H6+9KQgBwf88+9NZ2YExxkkHnGT61GftH/PJh/wE01cpK49sDOP89felLHBIGMd/wA/eodtwW5R"
      + "/wDvk+9Nk8yM5k3j0zTSZXL5kxYNnLcjPU/X3qWO+8hHjDYLD16dfes48scf560rtwxHbj+dUmU6SejJHuF3E5JO"
      + "f8fekW5DOAAe/f61XPfn9afGcAk4AHf86aNXBJErTsZGxgcev/16qXOr20DAyT5Xodgziq2pPMJ4baNS7XHKqg3M"
      + "5zgAD8/8itSP4W6vqSLLdvHZqefLLZYD3x3r06GGjKPNLqfP43MZU6jp0lt1KsN3DeJ5kMwkXvgkEfUU4twRu5Gc"
      + "c1g36XOi640TrsaGQI69AU6D9K3HDBmwvasa9JUnpsz0cuxjxMGpKzRC5Yjk8HOK6HwRo91Botw1m8pka8ZgqzmJ"
      + "SNq43EAk9x+FYOG27WjyK6nwTqjW13JYuoVZfmjyf4h2/L+VPDVOWVu4s3w/taHOt4/l1Owmga9tFgmZPMBIDOoY"
      + "Ajvg8GmjT3t7Ty7iUzll2liirkYweAABVeO7OZkk3DyWOVfCk/TuRVwTN5UbT4BPIQnOPavTPkmmjyW/8PRaDDZx"
      + "7dt3LG8k2DyQzHZkduBj8KrSEINo6kc0sUF8GnkvZTLPJMzF3fcQOgGc9ABTmtnJOSPWvMrzUqjaPuMsh7LCwjLR"
      + "/wCZXzRtY+n51MbZu7CnCLAxvrG6PSc13PV7nVNas9J0ldSSAXV3qKW1wgUMpjYtwO3QCtWFJor26RLbTkgEeI9h"
      + "IcnjhxjAH51yyQJLomkx2V4t1Ha6gtx580h+fbuJGcfhWnJNLFdzXKaPpvmXC7Xla7KtIvof3fTpWDxup+fTo9Eu"
      + "/l19Sl4n8y28JtPNa6fFcfaFXNnyuPqQDmsfRtJvtZZfLTy4f4pm+6P15NX9at2l8NS2dtZafYpG/nlYbgsDtGTx"
      + "sHJqU6t4pnhspdM0+1htxGG2EgiTK+mRgc5wKznUjUakzupTqU6TUbXbe72I9R8YW+gTw6VoMSXAhkzdSNyG9QD6"
      + "+/QYp+o6NFrUR1fw9IpL8zWpOMN3x6H26VJ/a/jf/oHaafz/APi6jn1LxzNEUS0sYDkHenJ4OccsRz0/GtJV4yXL"
      + "dW9TOKnBqULKXV817+pgRwrDqKC9hlKIwMsPKtjuOorp9E/4RnUtTEFpot7FJEPN8yZmCDB/3znr0xWPqfibxDaa"
      + "vbvcW1tBNHbkbR8wbceT14+70/xrd0rXNZv9BnuPLt2vCzLCOVQemevvWaqRpq7s7m+JlWnTUtumktChq/iLwtf6"
      + "rI17o+p3M1uTD5qZCEAnphxx17VRudU8KPaypb6DqaTMjeWzE4DYOCf3nrWqt544x839l59g1V9Q1zxhplm11cDT"
      + "fLUgHarE88etavEQm+jZlTjKNoxf/k7/AMifwK1x/Zmp/Zti3O0eX5n3Q+GxnHbOKdY6v4yu9XvNNc6fC9qu4yNC"
      + "xVv7uOc89fwrK0a9uW0DxHcScS3EUkhZDjaSrnj860dWv9Vl8DSTi3CX8tsguHXhtnc/kTx2yaFieT3F3sKvTbrS"
      + "bSd2lr00Qy08QeMrjWl0qWGyt5iu8l4iQq4znhvwqD4hTT+VoP2h42leOTzWj+4WxHkj2znFXY9Wvv8AhEDqos9+"
      + "pLaFFm/iZf73/s2K53xRMZdH8LhgRstCM56/JHTWJc3yv0+4uhT/ANojJRStdad7MobEKnI/X6+9NYRY2jGP/wBf"
      + "vUbNk9f1oADZ5x/n61jzH0SXmOZUXsOaGdcbTjj/AOvSAfKM9jzz9aCMks3GR/jTTYzodB011/4qCGB7iaNfJhhi"
      + "RSVH8TfMRyeB19a6+6vp7fTxcCFmfGSgRmP/AHyOc1zvgm+I0+5tk+Z438zaOpU+n5V0H25lMbKHYv8AKqhMlj/S"
      + "vew8r0kz47G03HETXmef+OdCuNYm0++t4Qs0soiZSpXKnnJB5GMGs6VthYZ7kV2HizVHt3EaAGYg9/uD1+tceeVO"
      + "7HPPX6+9cWMmnJRXQ9rKKLhB1Jdf0IjKPf8AOlSZlkDISrKcgg4x+tRXMtvaqWmmC47Z5rEuvEJJY2/yIvT1b/Cs"
      + "6OFq1dVojsxWY4bD6N3fZHsGj37XdhBM9qsspBG4kDoSM/oa0o7SW9n3tgMeEUdF96xNBtUsPDuhW2X+1raiScsp"
      + "KlXJbk/3gW/x7Vf8a+J7fwx4Oe5sJ1kvLsmC3cHJ3fxN/wABAJ+uBXsxpNaM+NqVoyblFWv07HnGuatp0fizUrC3"
      + "f9xDL5aOTwxAAbn/AHt1M8w8HAPoc9R+dcHJuRxjJ5zuJ/nV+11O4tnBjY7epQ9DWFbAqT5oOzPWwWdSpJQrK679"
      + "f+CdTIxGOlRmQ56/5/Os3+34CuZ0aP8A3ea00aJ0V1kUhgCP85rzp4erB6o+loZhha0bwkvnod/b20D/AA9mDQxs"
      + "Et5nUFQdrYbke/vW7/ZtheQQG6srecpGApliVtowOmRRRXnP4vmz55/F82cl470+ysre0NrZwQFpCCYolXIx7CuZ"
      + "n1G+iUJHe3CKowAsrADr70UVrL4Udi/ho6zxVd3Nv4L02WG4ljkdotzo5BOUJ5IrltS1K/QWmy9uF3W6k4lYZOTz"
      + "1oorKnsctDb5sSwuJrm5DzzSSsOAXYscc+tbOoIo8O6lIFAdLgBWA5Uc9DRRVv4kehL4Ec9pUkj6hbK7symVQQTk"
      + "EZrr/iNI6Lp0SOyxs75QHAOMY4ooqqn8SJz1P49MueDFD2V6jgMpCgg8gj5qvQO7+N72FmLRfYU+QnK/ePb8TRRX"
      + "LL4mcmI/jT/rsWclfEcECkiL7C58sfd++o6VynjMBLXw+FAUCF+Bx2SiiqpfGv66MeE/jR/rozDJO3qf85oBO/qe"
      + "v+NFFWtz6ToTuTzz6/1qCUnd1P8AnNFFWyYmv4RZh4ktwGIyGB568GvQwiiRmCgHHUCiivXwX8L5nzeb/wC8fJfq"
      + "ed+JSf7Ul5P3B/I1j5O7r2/xoorgrfxGe7gv93h6I47WmY6ncgsT+9bvVI/c/wCA0UV9LT+BHwtb+JL1Z9Daj8mh"
      + "2JX5T9n7cfwLXlvj4kW+gqDhTZu5HbcZOT9Tgc+1FFavYzOIn+6PrUqfd/Giip6iGXX3F/3jVxXYKAGOMetFFSxo"
      + "/9k=";

}
