
package com.gitee.jmash.common.utils.test;

import com.gitee.jmash.common.utils.JwtRsaUtil;
import com.gitee.jmash.common.utils.RsaUtil;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.impl.DefaultClaims;
import java.security.KeyPair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

/**
 * JWT Test.
 *
 * @author CGD
 *
 */
@TestInstance(Lifecycle.PER_CLASS)
public class JwtRsaUtilTest {

  private static Log log = LogFactory.getLog(JwtRsaUtilTest.class);

  private KeyPair keyPair;

  /** SetUp. */
  @BeforeAll
  public void setUp() throws Exception {
    keyPair = RsaUtil.genKeyPair();

    log.info(RsaUtil.getBase64Key(keyPair.getPrivate()));
    log.info(RsaUtil.getBase64Key(keyPair.getPublic()));
  }

  @Test
  public void test1() {
    DefaultClaims claims = new DefaultClaims();
    claims.setId("ClientId");
    claims.setIssuer("Crenjoy");
    claims.setSubject("Chenguodong");
    String token = JwtRsaUtil.createPrivate(keyPair.getPrivate()).encode(claims, 3600L);
    log.info(token);
    log.info(token.length());
    Claims claims2 = JwtRsaUtil.createPublic(keyPair.getPublic()).decode(token);
    log.info(claims2.toString());
    Assertions.assertEquals("ClientId", claims2.getId());
  }

  @Test
  public void test2() throws InterruptedException {
    DefaultClaims claims = new DefaultClaims();
    claims.setId("ClientId");
    claims.setIssuer("Crenjoy");
    claims.setSubject("Chenguodong");
    String token = JwtRsaUtil.createPrivate(keyPair.getPrivate()).encode(claims, 1L);
    log.info(token);
    log.info(token.length());
    Thread.sleep(10L);
    Claims claims2 = JwtRsaUtil.createPublic(keyPair.getPublic()).decode(token);
    Assertions.assertNull(claims2);
  }
}
