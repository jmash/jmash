
package com.gitee.jmash.common.utils.test;

import com.gitee.jmash.common.utils.LoadLibUtil;
import java.io.File;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * 加载Jar Lib工具.
 *
 * @author CGD
 *
 */
public class LoadLibUtilTest {

  private static final Log log = LogFactory.getLog(LoadLibUtilTest.class);

  @Test
  public void testCopyJarFile() {
    String userDir = System.getProperty("user.dir");
    String path = userDir + "/target/test-jar/log4j2.xml".replace("/", File.separator);
    log.info(path);
    File file = new File(path);
    file.delete();
    Assertions.assertEquals(file.exists(), false);
    file.getParentFile().mkdirs();
    LoadLibUtil.copyJarFile(LoadLibUtil.class, "/log4j2.xml", path);
    Assertions.assertEquals(file.exists(), true);
  }

}
