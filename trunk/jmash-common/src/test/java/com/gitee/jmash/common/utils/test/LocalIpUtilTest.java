
package com.gitee.jmash.common.utils.test;

import com.gitee.jmash.common.utils.LocalIpUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * LocalIpUtil工具类测试.
 *
 * @author CGD
 *
 */
public class LocalIpUtilTest {

  private static final Log log = LogFactory.getLog(LocalIpUtilTest.class);

  @Test
  public void test() {
    // 主机名称
    Assertions.assertNotNull(LocalIpUtil.getHostName());
    log.info("HOST：" + LocalIpUtil.getHostName());
    // 首选IP地址
    Assertions.assertNotNull(LocalIpUtil.getLocalIp());
    log.info("IP: " + LocalIpUtil.getLocalIp());

    Assertions.assertTrue(LocalIpUtil.getLocalIps().length >= 1);
    log.info("ALL IP:" + StringUtils.join(LocalIpUtil.getLocalIps()));
  }

}
