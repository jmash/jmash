
package com.gitee.jmash.common.utils.test;

import com.gitee.jmash.common.utils.RunEnvInfo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.jupiter.api.Test;

/** 运行环境. */
public class RunEnvInfoTest {
  private static final Log log = LogFactory.getLog(RunEnvInfoTest.class);

  @Test
  public void testEnvInfo() {
    RunEnvInfo env = new RunEnvInfo();
    log.info("操作系统：" + env.getOsName() + " " + env.getOsVersion() + " " + env.getOsArch());
    log.info("JDK：" + env.getJvmVendor() + " " + env.getJvmName() + " " + env.getJvmVersion());
    log.info("CPU：" + env.getAvailableProcessors() + "核");

    log.info("内存：" + env.getDisplayUsedMemory() + "/" + env.getDisplayTotalMemory());
  }

}
