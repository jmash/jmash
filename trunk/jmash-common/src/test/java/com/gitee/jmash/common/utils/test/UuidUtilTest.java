
package com.gitee.jmash.common.utils.test;

import com.gitee.jmash.common.utils.UUIDUtil;
import java.util.UUID;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * UUIDUtil 工具类测试.
 *
 * @author CGD
 *
 */
public class UuidUtilTest {

  private Log log = LogFactory.getLog(UUIDUtil.class);

  @Test
  public void testEmpty() {
    log.info(UUIDUtil.emptyUUID());
    Assertions.assertEquals(UUIDUtil.emptyUUID().toString(), UUIDUtil.emptyUUIDStr());
  }

  @Test
  public void testFormat() {
    // 32UUID
    String olduuid = UUID.randomUUID().toString();

    String uuid = olduuid.replace("-", "");
    //log.info(uuid);

    Assertions.assertEquals(UUIDUtil.formatUUID(uuid), olduuid);
  }

  @Test
  public void testByte() {
    // 32UUID
    String olduuid = UUID.randomUUID().toString();
    //log.info(olduuid);

    byte[] bytes = UUIDUtil.uuidToByte(olduuid);

    Assertions.assertEquals(UUIDUtil.byteToUUID(bytes), olduuid);
  }

  @Test
  public void testUuid32() {
    // 32UUID
    String olduuid = UUID.randomUUID().toString();

    String uuid32 = UUIDUtil.uuidToUUIDHex32(olduuid);

    //log.info(uuid32);

    Assertions.assertEquals(UUIDUtil.uuid32ToUUID(uuid32), olduuid);
  }

  @Test
  public void testUuid64() {
    // 64UUID
    String olduuid = UUID.randomUUID().toString();

    String uuid64 = UUIDUtil.uuidToUUIDHex64(olduuid);
    //log.info(uuid64);

    Assertions.assertEquals(UUIDUtil.uuid64ToUUID(uuid64), olduuid);
  }

  @Test
  public void testUuid32List() {
    for (int i = 0; i < 1000; i++) {
      testFormat();
      testByte();
      testUuid32();
      testUuid64();
    }
  }

}
