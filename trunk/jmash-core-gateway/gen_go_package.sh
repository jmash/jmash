# 初始化go mod 
go mod init gitee.com/jmash/jmash/trunk/jmash-core-gateway


# 生成Go Grpc网关
protoc -I ../jmash-core-lib/src/main/proto  --go_out ./src  --go-grpc_out ./src   --grpc-gateway_out=./src  --openapiv2_out=./openapi  ../jmash-core-lib/src/main/proto/jmash/protobuf/*.proto
protoc -I ../jmash-core-lib/src/main/proto  --go_out ./src  --go-grpc_out ./src   --grpc-gateway_out=./src  --openapiv2_out=./openapi  ../jmash-core-lib/src/main/proto/jmash/basic/*.proto


#  生成OpenApi文档
mkdir ./openapi/jmash/basic
protoc -I ../jmash-core-lib/src/main/proto --doc_out=./openapi/jmash/basic --doc_opt=html,index.html  ../jmash-core-lib/src/main/proto/jmash/protobuf/*.proto
protoc -I ../jmash-core-lib/src/main/proto --doc_out=./openapi/jmash/basic --doc_opt=markdown,readme.md  ../jmash-core-lib/src/main/proto/jmash/basic/*.proto

#  copy 
cp -rf openapi ./bin/

# 更新mod 
go mod tidy

export GOOS=windows
export GOARCH=amd64

go build -o ./bin/main.exe ./src/main.go

export CGO_ENABLED=0
export GOOS=linux
export GOARCH=amd64

go build -o ./bin/main_amd64 ./src/main.go

export GOARCH=arm64
go build -o ./bin/main_arm64 ./src/main.go
