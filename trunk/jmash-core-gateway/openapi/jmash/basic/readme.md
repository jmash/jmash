# Protocol Documentation
<a name="top"></a>

## Table of Contents

- [jmash/basic/file_basic_rpc.proto](#jmash_basic_file_basic_rpc-proto)
    - [FileBasic](#jmash-basic-FileBasic)
  
- [Scalar Value Types](#scalar-value-types)



<a name="jmash_basic_file_basic_rpc-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## jmash/basic/file_basic_rpc.proto


 

 

 


<a name="jmash-basic-FileBasic"></a>

### FileBasic
File Service

| Method Name | Request Type | Response Type | Description |
| ----------- | ------------ | ------------- | ------------|
| version | [.google.protobuf.Empty](#google-protobuf-Empty) | [.google.protobuf.StringValue](#google-protobuf-StringValue) | 版本 |
| uploadFile | [.jmash.protobuf.FileUploadReq](#jmash-protobuf-FileUploadReq) stream | [.jmash.protobuf.FileInfo](#jmash-protobuf-FileInfo) | 上传文件 |
| uploadFileWeb | [.jmash.protobuf.FileWebUploadReq](#jmash-protobuf-FileWebUploadReq) | [.jmash.protobuf.FileInfo](#jmash-protobuf-FileInfo) | 通过Web上传文件 |
| uploadBase64File | [.jmash.protobuf.FileBase64Req](#jmash-protobuf-FileBase64Req) | [.jmash.protobuf.FileInfo](#jmash-protobuf-FileInfo) | 上传Base64文件 3.5M限制 |
| existFile | [.jmash.protobuf.FileHash](#jmash-protobuf-FileHash) | [.jmash.protobuf.FileInfo](#jmash-protobuf-FileInfo) | 文件是否存在 |
| existFileHex | [.jmash.protobuf.FileHashHex](#jmash-protobuf-FileHashHex) | [.jmash.protobuf.FileInfo](#jmash-protobuf-FileInfo) | 文件是否存在 |
| downloadFile | [.jmash.protobuf.DownloadReq](#jmash-protobuf-DownloadReq) | [.google.api.HttpBody](#google-api-HttpBody) stream | 下载文件通过ID |
| downloadFileBySrc | [.jmash.protobuf.DownloadSrcReq](#jmash-protobuf-DownloadSrcReq) | [.google.api.HttpBody](#google-api-HttpBody) stream | 下载文件通过Src |
| downloadThumb | [.jmash.protobuf.ThumbDownloadReq](#jmash-protobuf-ThumbDownloadReq) | [.google.api.HttpBody](#google-api-HttpBody) stream | 下载缩略图通过ID |
| downloadThumbBySrc | [.jmash.protobuf.ThumbSrcDownloadReq](#jmash-protobuf-ThumbSrcDownloadReq) | [.google.api.HttpBody](#google-api-HttpBody) stream | 下载缩略图通过Src |
| removeFile | [.jmash.protobuf.FileSrcReq](#jmash-protobuf-FileSrcReq) | [.google.protobuf.BoolValue](#google-protobuf-BoolValue) | 移除文件 |
| downloadFileRange | [.jmash.protobuf.FileRangeReq](#jmash-protobuf-FileRangeReq) | [.jmash.protobuf.FileRangeResp](#jmash-protobuf-FileRangeResp) stream | 文件Range数据 |

 



## Scalar Value Types

| .proto Type | Notes | C++ | Java | Python | Go | C# | PHP | Ruby |
| ----------- | ----- | --- | ---- | ------ | -- | -- | --- | ---- |
| <a name="double" /> double |  | double | double | float | float64 | double | float | Float |
| <a name="float" /> float |  | float | float | float | float32 | float | float | Float |
| <a name="int32" /> int32 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint32 instead. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="int64" /> int64 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint64 instead. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="uint32" /> uint32 | Uses variable-length encoding. | uint32 | int | int/long | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="uint64" /> uint64 | Uses variable-length encoding. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum or Fixnum (as required) |
| <a name="sint32" /> sint32 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int32s. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sint64" /> sint64 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int64s. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="fixed32" /> fixed32 | Always four bytes. More efficient than uint32 if values are often greater than 2^28. | uint32 | int | int | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="fixed64" /> fixed64 | Always eight bytes. More efficient than uint64 if values are often greater than 2^56. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum |
| <a name="sfixed32" /> sfixed32 | Always four bytes. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sfixed64" /> sfixed64 | Always eight bytes. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="bool" /> bool |  | bool | boolean | boolean | bool | bool | boolean | TrueClass/FalseClass |
| <a name="string" /> string | A string must always contain UTF-8 encoded or 7-bit ASCII text. | string | String | str/unicode | string | string | string | String (UTF-8) |
| <a name="bytes" /> bytes | May contain any arbitrary sequence of bytes. | string | ByteString | str | []byte | ByteString | string | String (ASCII-8BIT) |

