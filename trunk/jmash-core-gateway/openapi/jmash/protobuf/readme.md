# Protocol Documentation
<a name="top"></a>

## Table of Contents

- [jmash/protobuf/basic.proto](#jmash_protobuf_basic-proto)
    - [Chart](#jmash-protobuf-Chart)
    - [ChartSeries](#jmash-protobuf-ChartSeries)
    - [CustomEnumValue](#jmash-protobuf-CustomEnumValue)
    - [CustomEnumValueMap](#jmash-protobuf-CustomEnumValueMap)
    - [CustomEnumValueMap.ValuesEntry](#jmash-protobuf-CustomEnumValueMap-ValuesEntry)
    - [Entry](#jmash-protobuf-Entry)
    - [EntryList](#jmash-protobuf-EntryList)
    - [EntryMap](#jmash-protobuf-EntryMap)
    - [EntryMap.ValuesEntry](#jmash-protobuf-EntryMap-ValuesEntry)
    - [EnumEntryReq](#jmash-protobuf-EnumEntryReq)
    - [EnumValueList](#jmash-protobuf-EnumValueList)
    - [ExcelContentData](#jmash-protobuf-ExcelContentData)
    - [ExcelContentData.RowDataEntry](#jmash-protobuf-ExcelContentData-RowDataEntry)
    - [ExcelHeaderData](#jmash-protobuf-ExcelHeaderData)
    - [ExcelImportSheet](#jmash-protobuf-ExcelImportSheet)
    - [ExcelImportSheet.PropColumnEntry](#jmash-protobuf-ExcelImportSheet-PropColumnEntry)
    - [ExcelPreviewData](#jmash-protobuf-ExcelPreviewData)
    - [ExcelSheet](#jmash-protobuf-ExcelSheet)
    - [Geolocation](#jmash-protobuf-Geolocation)
    - [TableHead](#jmash-protobuf-TableHead)
    - [TenantReq](#jmash-protobuf-TenantReq)
    - [TreeList](#jmash-protobuf-TreeList)
    - [TreeModel](#jmash-protobuf-TreeModel)
    - [UUIDValue](#jmash-protobuf-UUIDValue)
  
    - [Gender](#jmash-protobuf-Gender)
  
    - [File-level Extensions](#jmash_protobuf_basic-proto-extensions)
    - [File-level Extensions](#jmash_protobuf_basic-proto-extensions)
  
- [jmash/protobuf/file_basic.proto](#jmash_protobuf_file_basic-proto)
    - [DownloadReq](#jmash-protobuf-DownloadReq)
    - [DownloadSrcReq](#jmash-protobuf-DownloadSrcReq)
    - [FileBase64Req](#jmash-protobuf-FileBase64Req)
    - [FileDownloadResp](#jmash-protobuf-FileDownloadResp)
    - [FileHash](#jmash-protobuf-FileHash)
    - [FileHashHex](#jmash-protobuf-FileHashHex)
    - [FileInfo](#jmash-protobuf-FileInfo)
    - [FileRangeReq](#jmash-protobuf-FileRangeReq)
    - [FileRangeReq.HeadersEntry](#jmash-protobuf-FileRangeReq-HeadersEntry)
    - [FileRangeResp](#jmash-protobuf-FileRangeResp)
    - [FileRangeResp.HeadersEntry](#jmash-protobuf-FileRangeResp-HeadersEntry)
    - [FileSrcReq](#jmash-protobuf-FileSrcReq)
    - [FileUploadReq](#jmash-protobuf-FileUploadReq)
    - [FileWebUploadReq](#jmash-protobuf-FileWebUploadReq)
    - [ThumbDownloadReq](#jmash-protobuf-ThumbDownloadReq)
    - [ThumbSrcDownloadReq](#jmash-protobuf-ThumbSrcDownloadReq)
  
    - [FileType](#jmash-protobuf-FileType)
    - [ThumbType](#jmash-protobuf-ThumbType)
  
- [Scalar Value Types](#scalar-value-types)



<a name="jmash_protobuf_basic-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## jmash/protobuf/basic.proto



<a name="jmash-protobuf-Chart"></a>

### Chart
图表数据


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| x_axis_data | [string](#string) | repeated | X轴坐标数组 |
| series | [ChartSeries](#jmash-protobuf-ChartSeries) | repeated | 展示数据数组 |






<a name="jmash-protobuf-ChartSeries"></a>

### ChartSeries
图标数据


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| name | [string](#string) |  | 数据名称 |
| data | [double](#double) | repeated | 展示数据数组 |






<a name="jmash-protobuf-CustomEnumValue"></a>

### CustomEnumValue
自定义枚举值


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| ordinal | [int32](#int32) |  | 枚举值 |
| name | [string](#string) |  | 枚举名 |
| value | [int32](#int32) |  | proto配置value值. |
| display_name | [string](#string) |  | 显示名称. |
| type | [int32](#int32) |  | 枚举类型 |






<a name="jmash-protobuf-CustomEnumValueMap"></a>

### CustomEnumValueMap
枚举值枚举


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| values | [CustomEnumValueMap.ValuesEntry](#jmash-protobuf-CustomEnumValueMap-ValuesEntry) | repeated |  |






<a name="jmash-protobuf-CustomEnumValueMap-ValuesEntry"></a>

### CustomEnumValueMap.ValuesEntry



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| key | [int32](#int32) |  |  |
| value | [CustomEnumValue](#jmash-protobuf-CustomEnumValue) |  |  |






<a name="jmash-protobuf-Entry"></a>

### Entry
Entry


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| key | [string](#string) |  |  |
| value | [string](#string) |  |  |






<a name="jmash-protobuf-EntryList"></a>

### EntryList
Entry List


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| values | [Entry](#jmash-protobuf-Entry) | repeated |  |






<a name="jmash-protobuf-EntryMap"></a>

### EntryMap
键值对Map


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| values | [EntryMap.ValuesEntry](#jmash-protobuf-EntryMap-ValuesEntry) | repeated |  |






<a name="jmash-protobuf-EntryMap-ValuesEntry"></a>

### EntryMap.ValuesEntry



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| key | [string](#string) |  |  |
| value | [string](#string) |  |  |






<a name="jmash-protobuf-EnumEntryReq"></a>

### EnumEntryReq
枚举查询请求


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| class_name | [string](#string) |  | 枚举类名称 |
| type | [int32](#int32) |  | 枚举类型(默认0) |






<a name="jmash-protobuf-EnumValueList"></a>

### EnumValueList
枚举值列表


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| values | [google.protobuf.EnumValue](#google-protobuf-EnumValue) | repeated |  |






<a name="jmash-protobuf-ExcelContentData"></a>

### ExcelContentData
Excel 内容数据


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| row_data | [ExcelContentData.RowDataEntry](#jmash-protobuf-ExcelContentData-RowDataEntry) | repeated | 行数据 &lt;prop,value&gt; |






<a name="jmash-protobuf-ExcelContentData-RowDataEntry"></a>

### ExcelContentData.RowDataEntry



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| key | [string](#string) |  |  |
| value | [string](#string) |  |  |






<a name="jmash-protobuf-ExcelHeaderData"></a>

### ExcelHeaderData
Excel 表头数据


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| label | [string](#string) |  | 表头 |
| prop | [string](#string) |  | 字段名 |
| column_index | [int32](#int32) |  | 列Index |
| children | [ExcelHeaderData](#jmash-protobuf-ExcelHeaderData) | repeated | children |






<a name="jmash-protobuf-ExcelImportSheet"></a>

### ExcelImportSheet
Excel 导入Sheet 前端定义位置


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| file_name | [string](#string) |  | Excel 文件 |
| sheet | [int32](#int32) |  | sheet |
| data_start_row | [int32](#int32) |  | 数据开始行 |
| prop_column | [ExcelImportSheet.PropColumnEntry](#jmash-protobuf-ExcelImportSheet-PropColumnEntry) | repeated | 属性，列索引对应 |






<a name="jmash-protobuf-ExcelImportSheet-PropColumnEntry"></a>

### ExcelImportSheet.PropColumnEntry



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| key | [string](#string) |  |  |
| value | [int32](#int32) |  |  |






<a name="jmash-protobuf-ExcelPreviewData"></a>

### ExcelPreviewData
Excel预览数据


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header_list | [ExcelHeaderData](#jmash-protobuf-ExcelHeaderData) | repeated | 数据头信息 |
| content_list | [ExcelContentData](#jmash-protobuf-ExcelContentData) | repeated | 数据信息 |
| header_start_row | [int32](#int32) |  | 表头开始行 |
| header_count | [int32](#int32) |  | 表头数 |






<a name="jmash-protobuf-ExcelSheet"></a>

### ExcelSheet
Excel Sheet 表


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| file_name | [string](#string) |  | Excel 文件 |
| sheet | [int32](#int32) |  | sheet |
| header_start_row | [int32](#int32) |  | 表头开始行 |
| header_count | [int32](#int32) |  | 表头数 |






<a name="jmash-protobuf-Geolocation"></a>

### Geolocation
地理信息位置


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| latitude | [double](#double) |  | The latitude in degrees. It must be in the range [-90.0, &#43;90.0]. |
| longitude | [double](#double) |  | The longitude in degrees. It must be in the range [-180.0, &#43;180.0]. |
| accuracy | [int32](#int32) |  | 精度 |
| altitude | [double](#double) |  | 海拔高度 |
| altitude_accracy | [int32](#int32) |  | 海拔高度精度 |






<a name="jmash-protobuf-TableHead"></a>

### TableHead
表头


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| label | [string](#string) |  | 表头名称 |
| prop | [string](#string) |  | 属性 |
| serial | [bool](#bool) |  | 是否序号 |






<a name="jmash-protobuf-TenantReq"></a>

### TenantReq
租户


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |






<a name="jmash-protobuf-TreeList"></a>

### TreeList
Tree List.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [TreeModel](#jmash-protobuf-TreeModel) | repeated |  |






<a name="jmash-protobuf-TreeModel"></a>

### TreeModel
TreeModel


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| value | [string](#string) |  | Key Value |
| label | [string](#string) |  | Name |
| parentId | [string](#string) |  | Parent Value |
| children | [TreeModel](#jmash-protobuf-TreeModel) | repeated | child Model |






<a name="jmash-protobuf-UUIDValue"></a>

### UUIDValue



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| most_sig_bits | [int64](#int64) |  |  |
| least_sig_bits | [int64](#int64) |  |  |





 


<a name="jmash-protobuf-Gender"></a>

### Gender
性别

| Name | Number | Description |
| ---- | ------ | ----------- |
| unknown | 0 | 未知的性别 |
| male | 1 | 男性 |
| female | 2 | 女性 |
| undeclared | 9 | 未说明的性别 |


 


<a name="jmash_protobuf_basic-proto-extensions"></a>

### File-level Extensions
| Extension | Type | Base | Number | Description |
| --------- | ---- | ---- | ------ | ----------- |
| display_name | string | .google.protobuf.EnumValueOptions | 50001 | 枚举显示名 |
| type | int32 | .google.protobuf.EnumValueOptions | 50002 | 枚举类型 |

 

 



<a name="jmash_protobuf_file_basic-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## jmash/protobuf/file_basic.proto



<a name="jmash-protobuf-DownloadReq"></a>

### DownloadReq
下载请求


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| file_id | [string](#string) |  | 文件下载 |






<a name="jmash-protobuf-DownloadSrcReq"></a>

### DownloadSrcReq
下载请求Src


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| file_src | [string](#string) |  | 文件下载 |






<a name="jmash-protobuf-FileBase64Req"></a>

### FileBase64Req



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| base64 | [string](#string) |  | Base64文件内容 |
| file_name | [string](#string) |  | 文件名称 |
| content_type | [string](#string) |  | 内容类型 |
| file_dir | [string](#string) |  | 保存路径 |






<a name="jmash-protobuf-FileDownloadResp"></a>

### FileDownloadResp



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| body | [bytes](#bytes) |  | 文件内容 |
| file_name | [string](#string) |  | 文件名称 |
| content_type | [string](#string) |  | 内容类型 |
| file_ext | [string](#string) |  | 文件后缀 |
| file_type | [FileType](#jmash-protobuf-FileType) |  | 文件格式类型 |
| file_size | [int64](#int64) |  | 文件大小 |
| file_part | [int32](#int32) |  | 文件部分 |
| file_width | [int32](#int32) |  | 图片/视频宽度 |
| file_height | [int32](#int32) |  | 图片/视频高度 |






<a name="jmash-protobuf-FileHash"></a>

### FileHash



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| hash_sm3 | [bytes](#bytes) |  | File SM3信息摘要 |
| hash_sha256 | [bytes](#bytes) |  | File sha256 |
| file_size | [int64](#int64) |  | 文件大小 |






<a name="jmash-protobuf-FileHashHex"></a>

### FileHashHex



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| hash_sm3 | [string](#string) |  | File SM3信息摘要 |
| hash_sha256 | [string](#string) |  | File sha256 |
| file_size | [int64](#int64) |  | 文件大小 |






<a name="jmash-protobuf-FileInfo"></a>

### FileInfo



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| file_id | [string](#string) |  | 文件ID |
| file_src | [string](#string) |  | 文件存放路径 |
| file_name | [string](#string) |  | 文件名称 |
| content_type | [string](#string) |  | 内容类型 |
| file_ext | [string](#string) |  | 文件后缀 |
| file_type | [FileType](#jmash-protobuf-FileType) |  | 文件格式类型 |
| file_size | [int64](#int64) |  | 文件大小 |
| hash_sm3 | [bytes](#bytes) |  | File SM3信息摘要 |
| hash_sha256 | [bytes](#bytes) |  | File sha256 |
| file_width | [int32](#int32) |  | 图片/视频宽度 |
| file_height | [int32](#int32) |  | 图片/视频高度 |
| file_time | [int64](#int64) |  | 视频/音频时长 |
| file_location | [string](#string) |  | 地理位置 |
| position | [Geolocation](#jmash-protobuf-Geolocation) |  | 文件拍摄位置 |
| create_date | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  | 创建时间 |






<a name="jmash-protobuf-FileRangeReq"></a>

### FileRangeReq
Range 请求.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| file_src | [string](#string) |  | 文件下载 |
| headers | [FileRangeReq.HeadersEntry](#jmash-protobuf-FileRangeReq-HeadersEntry) | repeated | headers数据. |
| method | [string](#string) |  | 请求Method |






<a name="jmash-protobuf-FileRangeReq-HeadersEntry"></a>

### FileRangeReq.HeadersEntry



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| key | [string](#string) |  |  |
| value | [string](#string) |  |  |






<a name="jmash-protobuf-FileRangeResp"></a>

### FileRangeResp
Range 数据回复.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| status_code | [int32](#int32) |  | 状态码. |
| headers | [FileRangeResp.HeadersEntry](#jmash-protobuf-FileRangeResp-HeadersEntry) | repeated | headers数据. |
| content_type | [string](#string) |  | 内容类型. |
| data | [bytes](#bytes) |  | The HTTP request/response body as raw binary. |






<a name="jmash-protobuf-FileRangeResp-HeadersEntry"></a>

### FileRangeResp.HeadersEntry



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| key | [string](#string) |  |  |
| value | [string](#string) |  |  |






<a name="jmash-protobuf-FileSrcReq"></a>

### FileSrcReq
文件Src请求.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| file_src | [string](#string) |  | 文件下载 |






<a name="jmash-protobuf-FileUploadReq"></a>

### FileUploadReq
文件上传


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| body | [bytes](#bytes) |  | 文件内容 |
| file_name | [string](#string) |  | 文件名称 |
| content_type | [string](#string) |  | 内容类型 |
| file_size | [int64](#int64) |  | 文件大小 |
| file_dir | [string](#string) |  | 相对路径 |






<a name="jmash-protobuf-FileWebUploadReq"></a>

### FileWebUploadReq



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| body | [bytes](#bytes) |  | 文件内容 |
| file_name | [string](#string) |  | 文件名称 |
| content_type | [string](#string) |  | 内容类型 |
| file_size | [int64](#int64) |  | 文件大小 |
| file_dir | [string](#string) |  | 保存路径 |
| request_id | [string](#string) |  | 文件唯一ID |
| file_part | [int32](#int32) |  | 文件部分 |






<a name="jmash-protobuf-ThumbDownloadReq"></a>

### ThumbDownloadReq
缩略图下载请求


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| file_id | [string](#string) |  | 文件下载 |
| thumb_type | [ThumbType](#jmash-protobuf-ThumbType) |  | 缩略图类型 |
| width | [int32](#int32) |  | 图片宽度 |
| height | [int32](#int32) |  | 图片高度 |






<a name="jmash-protobuf-ThumbSrcDownloadReq"></a>

### ThumbSrcDownloadReq
缩略图下载请求


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| file_src | [string](#string) |  | 文件Src |
| thumb_type | [ThumbType](#jmash-protobuf-ThumbType) |  | 缩略图类型 |
| width | [int32](#int32) |  | 图片宽度 |
| height | [int32](#int32) |  | 图片高度 |





 


<a name="jmash-protobuf-FileType"></a>

### FileType
文件类型

| Name | Number | Description |
| ---- | ------ | ----------- |
| text | 0 | 文档 |
| video | 1 | 视频 |
| image | 2 | 图片 |
| audio | 3 | 音频 |
| zip | 4 | 压缩文件 |
| file | 5 | 其他文件 |



<a name="jmash-protobuf-ThumbType"></a>

### ThumbType
图片缩放类型

| Name | Number | Description |
| ---- | ------ | ----------- |
| resize | 0 | 等比例缩放 |
| white | 1 | 补白 |
| trans | 2 | 补透明 |
| clip | 3 | 裁剪 |


 

 

 



## Scalar Value Types

| .proto Type | Notes | C++ | Java | Python | Go | C# | PHP | Ruby |
| ----------- | ----- | --- | ---- | ------ | -- | -- | --- | ---- |
| <a name="double" /> double |  | double | double | float | float64 | double | float | Float |
| <a name="float" /> float |  | float | float | float | float32 | float | float | Float |
| <a name="int32" /> int32 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint32 instead. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="int64" /> int64 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint64 instead. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="uint32" /> uint32 | Uses variable-length encoding. | uint32 | int | int/long | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="uint64" /> uint64 | Uses variable-length encoding. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum or Fixnum (as required) |
| <a name="sint32" /> sint32 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int32s. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sint64" /> sint64 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int64s. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="fixed32" /> fixed32 | Always four bytes. More efficient than uint32 if values are often greater than 2^28. | uint32 | int | int | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="fixed64" /> fixed64 | Always eight bytes. More efficient than uint64 if values are often greater than 2^56. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum |
| <a name="sfixed32" /> sfixed32 | Always four bytes. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sfixed64" /> sfixed64 | Always eight bytes. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="bool" /> bool |  | bool | boolean | boolean | bool | bool | boolean | TrueClass/FalseClass |
| <a name="string" /> string | A string must always contain UTF-8 encoded or 7-bit ASCII text. | string | String | str/unicode | string | string | string | String (UTF-8) |
| <a name="bytes" /> bytes | May contain any arbitrary sequence of bytes. | string | ByteString | str | []byte | ByteString | string | String (ASCII-8BIT) |

