# Protocol Documentation
<a name="top"></a>

## Table of Contents

- [protoc-gen-openapiv2/options/annotations.proto](#protoc-gen-openapiv2_options_annotations-proto)
    - [File-level Extensions](#protoc-gen-openapiv2_options_annotations-proto-extensions)
    - [File-level Extensions](#protoc-gen-openapiv2_options_annotations-proto-extensions)
    - [File-level Extensions](#protoc-gen-openapiv2_options_annotations-proto-extensions)
    - [File-level Extensions](#protoc-gen-openapiv2_options_annotations-proto-extensions)
    - [File-level Extensions](#protoc-gen-openapiv2_options_annotations-proto-extensions)
  
- [protoc-gen-openapiv2/options/openapiv2.proto](#protoc-gen-openapiv2_options_openapiv2-proto)
    - [Contact](#grpc-gateway-protoc_gen_openapiv2-options-Contact)
    - [ExternalDocumentation](#grpc-gateway-protoc_gen_openapiv2-options-ExternalDocumentation)
    - [Header](#grpc-gateway-protoc_gen_openapiv2-options-Header)
    - [HeaderParameter](#grpc-gateway-protoc_gen_openapiv2-options-HeaderParameter)
    - [Info](#grpc-gateway-protoc_gen_openapiv2-options-Info)
    - [Info.ExtensionsEntry](#grpc-gateway-protoc_gen_openapiv2-options-Info-ExtensionsEntry)
    - [JSONSchema](#grpc-gateway-protoc_gen_openapiv2-options-JSONSchema)
    - [JSONSchema.ExtensionsEntry](#grpc-gateway-protoc_gen_openapiv2-options-JSONSchema-ExtensionsEntry)
    - [JSONSchema.FieldConfiguration](#grpc-gateway-protoc_gen_openapiv2-options-JSONSchema-FieldConfiguration)
    - [License](#grpc-gateway-protoc_gen_openapiv2-options-License)
    - [Operation](#grpc-gateway-protoc_gen_openapiv2-options-Operation)
    - [Operation.ExtensionsEntry](#grpc-gateway-protoc_gen_openapiv2-options-Operation-ExtensionsEntry)
    - [Operation.ResponsesEntry](#grpc-gateway-protoc_gen_openapiv2-options-Operation-ResponsesEntry)
    - [Parameters](#grpc-gateway-protoc_gen_openapiv2-options-Parameters)
    - [Response](#grpc-gateway-protoc_gen_openapiv2-options-Response)
    - [Response.ExamplesEntry](#grpc-gateway-protoc_gen_openapiv2-options-Response-ExamplesEntry)
    - [Response.ExtensionsEntry](#grpc-gateway-protoc_gen_openapiv2-options-Response-ExtensionsEntry)
    - [Response.HeadersEntry](#grpc-gateway-protoc_gen_openapiv2-options-Response-HeadersEntry)
    - [Schema](#grpc-gateway-protoc_gen_openapiv2-options-Schema)
    - [Scopes](#grpc-gateway-protoc_gen_openapiv2-options-Scopes)
    - [Scopes.ScopeEntry](#grpc-gateway-protoc_gen_openapiv2-options-Scopes-ScopeEntry)
    - [SecurityDefinitions](#grpc-gateway-protoc_gen_openapiv2-options-SecurityDefinitions)
    - [SecurityDefinitions.SecurityEntry](#grpc-gateway-protoc_gen_openapiv2-options-SecurityDefinitions-SecurityEntry)
    - [SecurityRequirement](#grpc-gateway-protoc_gen_openapiv2-options-SecurityRequirement)
    - [SecurityRequirement.SecurityRequirementEntry](#grpc-gateway-protoc_gen_openapiv2-options-SecurityRequirement-SecurityRequirementEntry)
    - [SecurityRequirement.SecurityRequirementValue](#grpc-gateway-protoc_gen_openapiv2-options-SecurityRequirement-SecurityRequirementValue)
    - [SecurityScheme](#grpc-gateway-protoc_gen_openapiv2-options-SecurityScheme)
    - [SecurityScheme.ExtensionsEntry](#grpc-gateway-protoc_gen_openapiv2-options-SecurityScheme-ExtensionsEntry)
    - [Swagger](#grpc-gateway-protoc_gen_openapiv2-options-Swagger)
    - [Swagger.ExtensionsEntry](#grpc-gateway-protoc_gen_openapiv2-options-Swagger-ExtensionsEntry)
    - [Swagger.ResponsesEntry](#grpc-gateway-protoc_gen_openapiv2-options-Swagger-ResponsesEntry)
    - [Tag](#grpc-gateway-protoc_gen_openapiv2-options-Tag)
    - [Tag.ExtensionsEntry](#grpc-gateway-protoc_gen_openapiv2-options-Tag-ExtensionsEntry)
  
    - [HeaderParameter.Type](#grpc-gateway-protoc_gen_openapiv2-options-HeaderParameter-Type)
    - [JSONSchema.JSONSchemaSimpleTypes](#grpc-gateway-protoc_gen_openapiv2-options-JSONSchema-JSONSchemaSimpleTypes)
    - [Scheme](#grpc-gateway-protoc_gen_openapiv2-options-Scheme)
    - [SecurityScheme.Flow](#grpc-gateway-protoc_gen_openapiv2-options-SecurityScheme-Flow)
    - [SecurityScheme.In](#grpc-gateway-protoc_gen_openapiv2-options-SecurityScheme-In)
    - [SecurityScheme.Type](#grpc-gateway-protoc_gen_openapiv2-options-SecurityScheme-Type)
  
- [Scalar Value Types](#scalar-value-types)



<a name="protoc-gen-openapiv2_options_annotations-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## protoc-gen-openapiv2/options/annotations.proto


 

 


<a name="protoc-gen-openapiv2_options_annotations-proto-extensions"></a>

### File-level Extensions
| Extension | Type | Base | Number | Description |
| --------- | ---- | ---- | ------ | ----------- |
| openapiv2_field | JSONSchema | .google.protobuf.FieldOptions | 1042 | ID assigned by protobuf-global-extension-registry@google.com for gRPC-Gateway project.

All IDs are the same, as assigned. It is okay that they are the same, as they extend different descriptor messages. |
| openapiv2_swagger | Swagger | .google.protobuf.FileOptions | 1042 | ID assigned by protobuf-global-extension-registry@google.com for gRPC-Gateway project.

All IDs are the same, as assigned. It is okay that they are the same, as they extend different descriptor messages. |
| openapiv2_schema | Schema | .google.protobuf.MessageOptions | 1042 | ID assigned by protobuf-global-extension-registry@google.com for gRPC-Gateway project.

All IDs are the same, as assigned. It is okay that they are the same, as they extend different descriptor messages. |
| openapiv2_operation | Operation | .google.protobuf.MethodOptions | 1042 | ID assigned by protobuf-global-extension-registry@google.com for gRPC-Gateway project.

All IDs are the same, as assigned. It is okay that they are the same, as they extend different descriptor messages. |
| openapiv2_tag | Tag | .google.protobuf.ServiceOptions | 1042 | ID assigned by protobuf-global-extension-registry@google.com for gRPC-Gateway project.

All IDs are the same, as assigned. It is okay that they are the same, as they extend different descriptor messages. |

 

 



<a name="protoc-gen-openapiv2_options_openapiv2-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## protoc-gen-openapiv2/options/openapiv2.proto



<a name="grpc-gateway-protoc_gen_openapiv2-options-Contact"></a>

### Contact
`Contact` is a representation of OpenAPI v2 specification&#39;s Contact object.

See: https://github.com/OAI/OpenAPI-Specification/blob/3.0.0/versions/2.0.md#contactObject

Example:

 option (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_swagger) = {
   info: {
     ...
     contact: {
       name: &#34;gRPC-Gateway project&#34;;
       url: &#34;https://github.com/grpc-ecosystem/grpc-gateway&#34;;
       email: &#34;none@example.com&#34;;
     };
     ...
   };
   ...
 };


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| name | [string](#string) |  | The identifying name of the contact person/organization. |
| url | [string](#string) |  | The URL pointing to the contact information. MUST be in the format of a URL. |
| email | [string](#string) |  | The email address of the contact person/organization. MUST be in the format of an email address. |






<a name="grpc-gateway-protoc_gen_openapiv2-options-ExternalDocumentation"></a>

### ExternalDocumentation
`ExternalDocumentation` is a representation of OpenAPI v2 specification&#39;s
ExternalDocumentation object.

See: https://github.com/OAI/OpenAPI-Specification/blob/3.0.0/versions/2.0.md#externalDocumentationObject

Example:

 option (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_swagger) = {
   ...
   external_docs: {
     description: &#34;More about gRPC-Gateway&#34;;
     url: &#34;https://github.com/grpc-ecosystem/grpc-gateway&#34;;
   }
   ...
 };


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| description | [string](#string) |  | A short description of the target documentation. GFM syntax can be used for rich text representation. |
| url | [string](#string) |  | The URL for the target documentation. Value MUST be in the format of a URL. |






<a name="grpc-gateway-protoc_gen_openapiv2-options-Header"></a>

### Header
`Header` is a representation of OpenAPI v2 specification&#39;s Header object.

See: https://github.com/OAI/OpenAPI-Specification/blob/3.0.0/versions/2.0.md#headerObject


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| description | [string](#string) |  | `Description` is a short description of the header. |
| type | [string](#string) |  | The type of the object. The value MUST be one of &#34;string&#34;, &#34;number&#34;, &#34;integer&#34;, or &#34;boolean&#34;. The &#34;array&#34; type is not supported. |
| format | [string](#string) |  | `Format` The extending format for the previously mentioned type. |
| default | [string](#string) |  | `Default` Declares the value of the header that the server will use if none is provided. See: https://tools.ietf.org/html/draft-fge-json-schema-validation-00#section-6.2. Unlike JSON Schema this value MUST conform to the defined type for the header. |
| pattern | [string](#string) |  | &#39;Pattern&#39; See https://tools.ietf.org/html/draft-fge-json-schema-validation-00#section-5.2.3. |






<a name="grpc-gateway-protoc_gen_openapiv2-options-HeaderParameter"></a>

### HeaderParameter
`HeaderParameter` a HTTP header parameter.
See: https://swagger.io/specification/v2/#parameter-object


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| name | [string](#string) |  | `Name` is the header name. |
| description | [string](#string) |  | `Description` is a short description of the header. |
| type | [HeaderParameter.Type](#grpc-gateway-protoc_gen_openapiv2-options-HeaderParameter-Type) |  | `Type` is the type of the object. The value MUST be one of &#34;string&#34;, &#34;number&#34;, &#34;integer&#34;, or &#34;boolean&#34;. The &#34;array&#34; type is not supported. See: https://swagger.io/specification/v2/#parameterType. |
| format | [string](#string) |  | `Format` The extending format for the previously mentioned type. |
| required | [bool](#bool) |  | `Required` indicates if the header is optional |






<a name="grpc-gateway-protoc_gen_openapiv2-options-Info"></a>

### Info
`Info` is a representation of OpenAPI v2 specification&#39;s Info object.

See: https://github.com/OAI/OpenAPI-Specification/blob/3.0.0/versions/2.0.md#infoObject

Example:

 option (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_swagger) = {
   info: {
     title: &#34;Echo API&#34;;
     version: &#34;1.0&#34;;
     description: &#34;&#34;;
     contact: {
       name: &#34;gRPC-Gateway project&#34;;
       url: &#34;https://github.com/grpc-ecosystem/grpc-gateway&#34;;
       email: &#34;none@example.com&#34;;
     };
     license: {
       name: &#34;BSD 3-Clause License&#34;;
       url: &#34;https://github.com/grpc-ecosystem/grpc-gateway/blob/main/LICENSE&#34;;
     };
   };
   ...
 };


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| title | [string](#string) |  | The title of the application. |
| description | [string](#string) |  | A short description of the application. GFM syntax can be used for rich text representation. |
| terms_of_service | [string](#string) |  | The Terms of Service for the API. |
| contact | [Contact](#grpc-gateway-protoc_gen_openapiv2-options-Contact) |  | The contact information for the exposed API. |
| license | [License](#grpc-gateway-protoc_gen_openapiv2-options-License) |  | The license information for the exposed API. |
| version | [string](#string) |  | Provides the version of the application API (not to be confused with the specification version). |
| extensions | [Info.ExtensionsEntry](#grpc-gateway-protoc_gen_openapiv2-options-Info-ExtensionsEntry) | repeated | Custom properties that start with &#34;x-&#34; such as &#34;x-foo&#34; used to describe extra functionality that is not covered by the standard OpenAPI Specification. See: https://swagger.io/docs/specification/2-0/swagger-extensions/ |






<a name="grpc-gateway-protoc_gen_openapiv2-options-Info-ExtensionsEntry"></a>

### Info.ExtensionsEntry



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| key | [string](#string) |  |  |
| value | [google.protobuf.Value](#google-protobuf-Value) |  |  |






<a name="grpc-gateway-protoc_gen_openapiv2-options-JSONSchema"></a>

### JSONSchema
`JSONSchema` represents properties from JSON Schema taken, and as used, in
the OpenAPI v2 spec.

This includes changes made by OpenAPI v2.

See: https://github.com/OAI/OpenAPI-Specification/blob/3.0.0/versions/2.0.md#schemaObject

See also: https://cswr.github.io/JsonSchema/spec/basic_types/,
https://github.com/json-schema-org/json-schema-spec/blob/master/schema.json

Example:

 message SimpleMessage {
   option (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_schema) = {
     json_schema: {
       title: &#34;SimpleMessage&#34;
       description: &#34;A simple message.&#34;
       required: [&#34;id&#34;]
     }
   };

   // Id represents the message identifier.
   string id = 1; [
       (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_field) = {
         description: &#34;The unique identifier of the simple message.&#34;
       }];
 }


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| ref | [string](#string) |  | Ref is used to define an external reference to include in the message. This could be a fully qualified proto message reference, and that type must be imported into the protofile. If no message is identified, the Ref will be used verbatim in the output. For example: `ref: &#34;.google.protobuf.Timestamp&#34;`. |
| title | [string](#string) |  | The title of the schema. |
| description | [string](#string) |  | A short description of the schema. |
| default | [string](#string) |  |  |
| read_only | [bool](#bool) |  |  |
| example | [string](#string) |  | A free-form property to include a JSON example of this field. This is copied verbatim to the output swagger.json. Quotes must be escaped. This property is the same for 2.0 and 3.0.0 https://github.com/OAI/OpenAPI-Specification/blob/3.0.0/versions/3.0.0.md#schemaObject https://github.com/OAI/OpenAPI-Specification/blob/3.0.0/versions/2.0.md#schemaObject |
| multiple_of | [double](#double) |  |  |
| maximum | [double](#double) |  | Maximum represents an inclusive upper limit for a numeric instance. The value of MUST be a number, |
| exclusive_maximum | [bool](#bool) |  |  |
| minimum | [double](#double) |  | minimum represents an inclusive lower limit for a numeric instance. The value of MUST be a number, |
| exclusive_minimum | [bool](#bool) |  |  |
| max_length | [uint64](#uint64) |  |  |
| min_length | [uint64](#uint64) |  |  |
| pattern | [string](#string) |  |  |
| max_items | [uint64](#uint64) |  |  |
| min_items | [uint64](#uint64) |  |  |
| unique_items | [bool](#bool) |  |  |
| max_properties | [uint64](#uint64) |  |  |
| min_properties | [uint64](#uint64) |  |  |
| required | [string](#string) | repeated |  |
| array | [string](#string) | repeated | Items in &#39;array&#39; must be unique. |
| type | [JSONSchema.JSONSchemaSimpleTypes](#grpc-gateway-protoc_gen_openapiv2-options-JSONSchema-JSONSchemaSimpleTypes) | repeated |  |
| format | [string](#string) |  | `Format` |
| enum | [string](#string) | repeated | Items in `enum` must be unique https://tools.ietf.org/html/draft-fge-json-schema-validation-00#section-5.5.1 |
| field_configuration | [JSONSchema.FieldConfiguration](#grpc-gateway-protoc_gen_openapiv2-options-JSONSchema-FieldConfiguration) |  | Additional field level properties used when generating the OpenAPI v2 file. |
| extensions | [JSONSchema.ExtensionsEntry](#grpc-gateway-protoc_gen_openapiv2-options-JSONSchema-ExtensionsEntry) | repeated | Custom properties that start with &#34;x-&#34; such as &#34;x-foo&#34; used to describe extra functionality that is not covered by the standard OpenAPI Specification. See: https://swagger.io/docs/specification/2-0/swagger-extensions/ |






<a name="grpc-gateway-protoc_gen_openapiv2-options-JSONSchema-ExtensionsEntry"></a>

### JSONSchema.ExtensionsEntry



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| key | [string](#string) |  |  |
| value | [google.protobuf.Value](#google-protobuf-Value) |  |  |






<a name="grpc-gateway-protoc_gen_openapiv2-options-JSONSchema-FieldConfiguration"></a>

### JSONSchema.FieldConfiguration
&#39;FieldConfiguration&#39; provides additional field level properties used when generating the OpenAPI v2 file.
These properties are not defined by OpenAPIv2, but they are used to control the generation.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| path_param_name | [string](#string) |  | Alternative parameter name when used as path parameter. If set, this will be used as the complete parameter name when this field is used as a path parameter. Use this to avoid having auto generated path parameter names for overlapping paths. |






<a name="grpc-gateway-protoc_gen_openapiv2-options-License"></a>

### License
`License` is a representation of OpenAPI v2 specification&#39;s License object.

See: https://github.com/OAI/OpenAPI-Specification/blob/3.0.0/versions/2.0.md#licenseObject

Example:

 option (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_swagger) = {
   info: {
     ...
     license: {
       name: &#34;BSD 3-Clause License&#34;;
       url: &#34;https://github.com/grpc-ecosystem/grpc-gateway/blob/main/LICENSE&#34;;
     };
     ...
   };
   ...
 };


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| name | [string](#string) |  | The license name used for the API. |
| url | [string](#string) |  | A URL to the license used for the API. MUST be in the format of a URL. |






<a name="grpc-gateway-protoc_gen_openapiv2-options-Operation"></a>

### Operation
`Operation` is a representation of OpenAPI v2 specification&#39;s Operation object.

See: https://github.com/OAI/OpenAPI-Specification/blob/3.0.0/versions/2.0.md#operationObject

Example:

 service EchoService {
   rpc Echo(SimpleMessage) returns (SimpleMessage) {
     option (google.api.http) = {
       get: &#34;/v1/example/echo/{id}&#34;
     };

     option (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_operation) = {
       summary: &#34;Get a message.&#34;;
       operation_id: &#34;getMessage&#34;;
       tags: &#34;echo&#34;;
       responses: {
         key: &#34;200&#34;
           value: {
           description: &#34;OK&#34;;
         }
       }
     };
   }
 }


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tags | [string](#string) | repeated | A list of tags for API documentation control. Tags can be used for logical grouping of operations by resources or any other qualifier. |
| summary | [string](#string) |  | A short summary of what the operation does. For maximum readability in the swagger-ui, this field SHOULD be less than 120 characters. |
| description | [string](#string) |  | A verbose explanation of the operation behavior. GFM syntax can be used for rich text representation. |
| external_docs | [ExternalDocumentation](#grpc-gateway-protoc_gen_openapiv2-options-ExternalDocumentation) |  | Additional external documentation for this operation. |
| operation_id | [string](#string) |  | Unique string used to identify the operation. The id MUST be unique among all operations described in the API. Tools and libraries MAY use the operationId to uniquely identify an operation, therefore, it is recommended to follow common programming naming conventions. |
| consumes | [string](#string) | repeated | A list of MIME types the operation can consume. This overrides the consumes definition at the OpenAPI Object. An empty value MAY be used to clear the global definition. Value MUST be as described under Mime Types. |
| produces | [string](#string) | repeated | A list of MIME types the operation can produce. This overrides the produces definition at the OpenAPI Object. An empty value MAY be used to clear the global definition. Value MUST be as described under Mime Types. |
| responses | [Operation.ResponsesEntry](#grpc-gateway-protoc_gen_openapiv2-options-Operation-ResponsesEntry) | repeated | The list of possible responses as they are returned from executing this operation. |
| schemes | [Scheme](#grpc-gateway-protoc_gen_openapiv2-options-Scheme) | repeated | The transfer protocol for the operation. Values MUST be from the list: &#34;http&#34;, &#34;https&#34;, &#34;ws&#34;, &#34;wss&#34;. The value overrides the OpenAPI Object schemes definition. |
| deprecated | [bool](#bool) |  | Declares this operation to be deprecated. Usage of the declared operation should be refrained. Default value is false. |
| security | [SecurityRequirement](#grpc-gateway-protoc_gen_openapiv2-options-SecurityRequirement) | repeated | A declaration of which security schemes are applied for this operation. The list of values describes alternative security schemes that can be used (that is, there is a logical OR between the security requirements). This definition overrides any declared top-level security. To remove a top-level security declaration, an empty array can be used. |
| extensions | [Operation.ExtensionsEntry](#grpc-gateway-protoc_gen_openapiv2-options-Operation-ExtensionsEntry) | repeated | Custom properties that start with &#34;x-&#34; such as &#34;x-foo&#34; used to describe extra functionality that is not covered by the standard OpenAPI Specification. See: https://swagger.io/docs/specification/2-0/swagger-extensions/ |
| parameters | [Parameters](#grpc-gateway-protoc_gen_openapiv2-options-Parameters) |  | Custom parameters such as HTTP request headers. See: https://swagger.io/docs/specification/2-0/describing-parameters/ and https://swagger.io/specification/v2/#parameter-object. |






<a name="grpc-gateway-protoc_gen_openapiv2-options-Operation-ExtensionsEntry"></a>

### Operation.ExtensionsEntry



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| key | [string](#string) |  |  |
| value | [google.protobuf.Value](#google-protobuf-Value) |  |  |






<a name="grpc-gateway-protoc_gen_openapiv2-options-Operation-ResponsesEntry"></a>

### Operation.ResponsesEntry



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| key | [string](#string) |  |  |
| value | [Response](#grpc-gateway-protoc_gen_openapiv2-options-Response) |  |  |






<a name="grpc-gateway-protoc_gen_openapiv2-options-Parameters"></a>

### Parameters
`Parameters` is a representation of OpenAPI v2 specification&#39;s parameters object.
Note: This technically breaks compatibility with the OpenAPI 2 definition structure as we only
allow header parameters to be set here since we do not want users specifying custom non-header
parameters beyond those inferred from the Protobuf schema.
See: https://swagger.io/specification/v2/#parameter-object


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| headers | [HeaderParameter](#grpc-gateway-protoc_gen_openapiv2-options-HeaderParameter) | repeated | `Headers` is one or more HTTP header parameter. See: https://swagger.io/docs/specification/2-0/describing-parameters/#header-parameters |






<a name="grpc-gateway-protoc_gen_openapiv2-options-Response"></a>

### Response
`Response` is a representation of OpenAPI v2 specification&#39;s Response object.

See: https://github.com/OAI/OpenAPI-Specification/blob/3.0.0/versions/2.0.md#responseObject


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| description | [string](#string) |  | `Description` is a short description of the response. GFM syntax can be used for rich text representation. |
| schema | [Schema](#grpc-gateway-protoc_gen_openapiv2-options-Schema) |  | `Schema` optionally defines the structure of the response. If `Schema` is not provided, it means there is no content to the response. |
| headers | [Response.HeadersEntry](#grpc-gateway-protoc_gen_openapiv2-options-Response-HeadersEntry) | repeated | `Headers` A list of headers that are sent with the response. `Header` name is expected to be a string in the canonical format of the MIME header key See: https://golang.org/pkg/net/textproto/#CanonicalMIMEHeaderKey |
| examples | [Response.ExamplesEntry](#grpc-gateway-protoc_gen_openapiv2-options-Response-ExamplesEntry) | repeated | `Examples` gives per-mimetype response examples. See: https://github.com/OAI/OpenAPI-Specification/blob/3.0.0/versions/2.0.md#example-object |
| extensions | [Response.ExtensionsEntry](#grpc-gateway-protoc_gen_openapiv2-options-Response-ExtensionsEntry) | repeated | Custom properties that start with &#34;x-&#34; such as &#34;x-foo&#34; used to describe extra functionality that is not covered by the standard OpenAPI Specification. See: https://swagger.io/docs/specification/2-0/swagger-extensions/ |






<a name="grpc-gateway-protoc_gen_openapiv2-options-Response-ExamplesEntry"></a>

### Response.ExamplesEntry



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| key | [string](#string) |  |  |
| value | [string](#string) |  |  |






<a name="grpc-gateway-protoc_gen_openapiv2-options-Response-ExtensionsEntry"></a>

### Response.ExtensionsEntry



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| key | [string](#string) |  |  |
| value | [google.protobuf.Value](#google-protobuf-Value) |  |  |






<a name="grpc-gateway-protoc_gen_openapiv2-options-Response-HeadersEntry"></a>

### Response.HeadersEntry



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| key | [string](#string) |  |  |
| value | [Header](#grpc-gateway-protoc_gen_openapiv2-options-Header) |  |  |






<a name="grpc-gateway-protoc_gen_openapiv2-options-Schema"></a>

### Schema
`Schema` is a representation of OpenAPI v2 specification&#39;s Schema object.

See: https://github.com/OAI/OpenAPI-Specification/blob/3.0.0/versions/2.0.md#schemaObject


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| json_schema | [JSONSchema](#grpc-gateway-protoc_gen_openapiv2-options-JSONSchema) |  |  |
| discriminator | [string](#string) |  | Adds support for polymorphism. The discriminator is the schema property name that is used to differentiate between other schema that inherit this schema. The property name used MUST be defined at this schema and it MUST be in the required property list. When used, the value MUST be the name of this schema or any schema that inherits it. |
| read_only | [bool](#bool) |  | Relevant only for Schema &#34;properties&#34; definitions. Declares the property as &#34;read only&#34;. This means that it MAY be sent as part of a response but MUST NOT be sent as part of the request. Properties marked as readOnly being true SHOULD NOT be in the required list of the defined schema. Default value is false. |
| external_docs | [ExternalDocumentation](#grpc-gateway-protoc_gen_openapiv2-options-ExternalDocumentation) |  | Additional external documentation for this schema. |
| example | [string](#string) |  | A free-form property to include an example of an instance for this schema in JSON. This is copied verbatim to the output. |






<a name="grpc-gateway-protoc_gen_openapiv2-options-Scopes"></a>

### Scopes
`Scopes` is a representation of OpenAPI v2 specification&#39;s Scopes object.

See: https://github.com/OAI/OpenAPI-Specification/blob/3.0.0/versions/2.0.md#scopesObject

Lists the available scopes for an OAuth2 security scheme.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| scope | [Scopes.ScopeEntry](#grpc-gateway-protoc_gen_openapiv2-options-Scopes-ScopeEntry) | repeated | Maps between a name of a scope to a short description of it (as the value of the property). |






<a name="grpc-gateway-protoc_gen_openapiv2-options-Scopes-ScopeEntry"></a>

### Scopes.ScopeEntry



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| key | [string](#string) |  |  |
| value | [string](#string) |  |  |






<a name="grpc-gateway-protoc_gen_openapiv2-options-SecurityDefinitions"></a>

### SecurityDefinitions
`SecurityDefinitions` is a representation of OpenAPI v2 specification&#39;s
Security Definitions object.

See: https://github.com/OAI/OpenAPI-Specification/blob/3.0.0/versions/2.0.md#securityDefinitionsObject

A declaration of the security schemes available to be used in the
specification. This does not enforce the security schemes on the operations
and only serves to provide the relevant details for each scheme.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| security | [SecurityDefinitions.SecurityEntry](#grpc-gateway-protoc_gen_openapiv2-options-SecurityDefinitions-SecurityEntry) | repeated | A single security scheme definition, mapping a &#34;name&#34; to the scheme it defines. |






<a name="grpc-gateway-protoc_gen_openapiv2-options-SecurityDefinitions-SecurityEntry"></a>

### SecurityDefinitions.SecurityEntry



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| key | [string](#string) |  |  |
| value | [SecurityScheme](#grpc-gateway-protoc_gen_openapiv2-options-SecurityScheme) |  |  |






<a name="grpc-gateway-protoc_gen_openapiv2-options-SecurityRequirement"></a>

### SecurityRequirement
`SecurityRequirement` is a representation of OpenAPI v2 specification&#39;s
Security Requirement object.

See: https://github.com/OAI/OpenAPI-Specification/blob/3.0.0/versions/2.0.md#securityRequirementObject

Lists the required security schemes to execute this operation. The object can
have multiple security schemes declared in it which are all required (that
is, there is a logical AND between the schemes).

The name used for each property MUST correspond to a security scheme
declared in the Security Definitions.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| security_requirement | [SecurityRequirement.SecurityRequirementEntry](#grpc-gateway-protoc_gen_openapiv2-options-SecurityRequirement-SecurityRequirementEntry) | repeated | Each name must correspond to a security scheme which is declared in the Security Definitions. If the security scheme is of type &#34;oauth2&#34;, then the value is a list of scope names required for the execution. For other security scheme types, the array MUST be empty. |






<a name="grpc-gateway-protoc_gen_openapiv2-options-SecurityRequirement-SecurityRequirementEntry"></a>

### SecurityRequirement.SecurityRequirementEntry



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| key | [string](#string) |  |  |
| value | [SecurityRequirement.SecurityRequirementValue](#grpc-gateway-protoc_gen_openapiv2-options-SecurityRequirement-SecurityRequirementValue) |  |  |






<a name="grpc-gateway-protoc_gen_openapiv2-options-SecurityRequirement-SecurityRequirementValue"></a>

### SecurityRequirement.SecurityRequirementValue
If the security scheme is of type &#34;oauth2&#34;, then the value is a list of
scope names required for the execution. For other security scheme types,
the array MUST be empty.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| scope | [string](#string) | repeated |  |






<a name="grpc-gateway-protoc_gen_openapiv2-options-SecurityScheme"></a>

### SecurityScheme
`SecurityScheme` is a representation of OpenAPI v2 specification&#39;s
Security Scheme object.

See: https://github.com/OAI/OpenAPI-Specification/blob/3.0.0/versions/2.0.md#securitySchemeObject

Allows the definition of a security scheme that can be used by the
operations. Supported schemes are basic authentication, an API key (either as
a header or as a query parameter) and OAuth2&#39;s common flows (implicit,
password, application and access code).


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| type | [SecurityScheme.Type](#grpc-gateway-protoc_gen_openapiv2-options-SecurityScheme-Type) |  | The type of the security scheme. Valid values are &#34;basic&#34;, &#34;apiKey&#34; or &#34;oauth2&#34;. |
| description | [string](#string) |  | A short description for security scheme. |
| name | [string](#string) |  | The name of the header or query parameter to be used. Valid for apiKey. |
| in | [SecurityScheme.In](#grpc-gateway-protoc_gen_openapiv2-options-SecurityScheme-In) |  | The location of the API key. Valid values are &#34;query&#34; or &#34;header&#34;. Valid for apiKey. |
| flow | [SecurityScheme.Flow](#grpc-gateway-protoc_gen_openapiv2-options-SecurityScheme-Flow) |  | The flow used by the OAuth2 security scheme. Valid values are &#34;implicit&#34;, &#34;password&#34;, &#34;application&#34; or &#34;accessCode&#34;. Valid for oauth2. |
| authorization_url | [string](#string) |  | The authorization URL to be used for this flow. This SHOULD be in the form of a URL. Valid for oauth2/implicit and oauth2/accessCode. |
| token_url | [string](#string) |  | The token URL to be used for this flow. This SHOULD be in the form of a URL. Valid for oauth2/password, oauth2/application and oauth2/accessCode. |
| scopes | [Scopes](#grpc-gateway-protoc_gen_openapiv2-options-Scopes) |  | The available scopes for the OAuth2 security scheme. Valid for oauth2. |
| extensions | [SecurityScheme.ExtensionsEntry](#grpc-gateway-protoc_gen_openapiv2-options-SecurityScheme-ExtensionsEntry) | repeated | Custom properties that start with &#34;x-&#34; such as &#34;x-foo&#34; used to describe extra functionality that is not covered by the standard OpenAPI Specification. See: https://swagger.io/docs/specification/2-0/swagger-extensions/ |






<a name="grpc-gateway-protoc_gen_openapiv2-options-SecurityScheme-ExtensionsEntry"></a>

### SecurityScheme.ExtensionsEntry



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| key | [string](#string) |  |  |
| value | [google.protobuf.Value](#google-protobuf-Value) |  |  |






<a name="grpc-gateway-protoc_gen_openapiv2-options-Swagger"></a>

### Swagger
`Swagger` is a representation of OpenAPI v2 specification&#39;s Swagger object.

See: https://github.com/OAI/OpenAPI-Specification/blob/3.0.0/versions/2.0.md#swaggerObject

Example:

 option (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_swagger) = {
   info: {
     title: &#34;Echo API&#34;;
     version: &#34;1.0&#34;;
     description: &#34;&#34;;
     contact: {
       name: &#34;gRPC-Gateway project&#34;;
       url: &#34;https://github.com/grpc-ecosystem/grpc-gateway&#34;;
       email: &#34;none@example.com&#34;;
     };
     license: {
       name: &#34;BSD 3-Clause License&#34;;
       url: &#34;https://github.com/grpc-ecosystem/grpc-gateway/blob/main/LICENSE&#34;;
     };
   };
   schemes: HTTPS;
   consumes: &#34;application/json&#34;;
   produces: &#34;application/json&#34;;
 };


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| swagger | [string](#string) |  | Specifies the OpenAPI Specification version being used. It can be used by the OpenAPI UI and other clients to interpret the API listing. The value MUST be &#34;2.0&#34;. |
| info | [Info](#grpc-gateway-protoc_gen_openapiv2-options-Info) |  | Provides metadata about the API. The metadata can be used by the clients if needed. |
| host | [string](#string) |  | The host (name or ip) serving the API. This MUST be the host only and does not include the scheme nor sub-paths. It MAY include a port. If the host is not included, the host serving the documentation is to be used (including the port). The host does not support path templating. |
| base_path | [string](#string) |  | The base path on which the API is served, which is relative to the host. If it is not included, the API is served directly under the host. The value MUST start with a leading slash (/). The basePath does not support path templating. Note that using `base_path` does not change the endpoint paths that are generated in the resulting OpenAPI file. If you wish to use `base_path` with relatively generated OpenAPI paths, the `base_path` prefix must be manually removed from your `google.api.http` paths and your code changed to serve the API from the `base_path`. |
| schemes | [Scheme](#grpc-gateway-protoc_gen_openapiv2-options-Scheme) | repeated | The transfer protocol of the API. Values MUST be from the list: &#34;http&#34;, &#34;https&#34;, &#34;ws&#34;, &#34;wss&#34;. If the schemes is not included, the default scheme to be used is the one used to access the OpenAPI definition itself. |
| consumes | [string](#string) | repeated | A list of MIME types the APIs can consume. This is global to all APIs but can be overridden on specific API calls. Value MUST be as described under Mime Types. |
| produces | [string](#string) | repeated | A list of MIME types the APIs can produce. This is global to all APIs but can be overridden on specific API calls. Value MUST be as described under Mime Types. |
| responses | [Swagger.ResponsesEntry](#grpc-gateway-protoc_gen_openapiv2-options-Swagger-ResponsesEntry) | repeated | An object to hold responses that can be used across operations. This property does not define global responses for all operations. |
| security_definitions | [SecurityDefinitions](#grpc-gateway-protoc_gen_openapiv2-options-SecurityDefinitions) |  | Security scheme definitions that can be used across the specification. |
| security | [SecurityRequirement](#grpc-gateway-protoc_gen_openapiv2-options-SecurityRequirement) | repeated | A declaration of which security schemes are applied for the API as a whole. The list of values describes alternative security schemes that can be used (that is, there is a logical OR between the security requirements). Individual operations can override this definition. |
| tags | [Tag](#grpc-gateway-protoc_gen_openapiv2-options-Tag) | repeated | A list of tags for API documentation control. Tags can be used for logical grouping of operations by resources or any other qualifier. |
| external_docs | [ExternalDocumentation](#grpc-gateway-protoc_gen_openapiv2-options-ExternalDocumentation) |  | Additional external documentation. |
| extensions | [Swagger.ExtensionsEntry](#grpc-gateway-protoc_gen_openapiv2-options-Swagger-ExtensionsEntry) | repeated | Custom properties that start with &#34;x-&#34; such as &#34;x-foo&#34; used to describe extra functionality that is not covered by the standard OpenAPI Specification. See: https://swagger.io/docs/specification/2-0/swagger-extensions/ |






<a name="grpc-gateway-protoc_gen_openapiv2-options-Swagger-ExtensionsEntry"></a>

### Swagger.ExtensionsEntry



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| key | [string](#string) |  |  |
| value | [google.protobuf.Value](#google-protobuf-Value) |  |  |






<a name="grpc-gateway-protoc_gen_openapiv2-options-Swagger-ResponsesEntry"></a>

### Swagger.ResponsesEntry



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| key | [string](#string) |  |  |
| value | [Response](#grpc-gateway-protoc_gen_openapiv2-options-Response) |  |  |






<a name="grpc-gateway-protoc_gen_openapiv2-options-Tag"></a>

### Tag
`Tag` is a representation of OpenAPI v2 specification&#39;s Tag object.

See: https://github.com/OAI/OpenAPI-Specification/blob/3.0.0/versions/2.0.md#tagObject


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| name | [string](#string) |  | The name of the tag. Use it to allow override of the name of a global Tag object, then use that name to reference the tag throughout the OpenAPI file. |
| description | [string](#string) |  | A short description for the tag. GFM syntax can be used for rich text representation. |
| external_docs | [ExternalDocumentation](#grpc-gateway-protoc_gen_openapiv2-options-ExternalDocumentation) |  | Additional external documentation for this tag. |
| extensions | [Tag.ExtensionsEntry](#grpc-gateway-protoc_gen_openapiv2-options-Tag-ExtensionsEntry) | repeated | Custom properties that start with &#34;x-&#34; such as &#34;x-foo&#34; used to describe extra functionality that is not covered by the standard OpenAPI Specification. See: https://swagger.io/docs/specification/2-0/swagger-extensions/ |






<a name="grpc-gateway-protoc_gen_openapiv2-options-Tag-ExtensionsEntry"></a>

### Tag.ExtensionsEntry



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| key | [string](#string) |  |  |
| value | [google.protobuf.Value](#google-protobuf-Value) |  |  |





 


<a name="grpc-gateway-protoc_gen_openapiv2-options-HeaderParameter-Type"></a>

### HeaderParameter.Type
`Type` is a a supported HTTP header type.
See https://swagger.io/specification/v2/#parameterType.

| Name | Number | Description |
| ---- | ------ | ----------- |
| UNKNOWN | 0 |  |
| STRING | 1 |  |
| NUMBER | 2 |  |
| INTEGER | 3 |  |
| BOOLEAN | 4 |  |



<a name="grpc-gateway-protoc_gen_openapiv2-options-JSONSchema-JSONSchemaSimpleTypes"></a>

### JSONSchema.JSONSchemaSimpleTypes


| Name | Number | Description |
| ---- | ------ | ----------- |
| UNKNOWN | 0 |  |
| ARRAY | 1 |  |
| BOOLEAN | 2 |  |
| INTEGER | 3 |  |
| NULL | 4 |  |
| NUMBER | 5 |  |
| OBJECT | 6 |  |
| STRING | 7 |  |



<a name="grpc-gateway-protoc_gen_openapiv2-options-Scheme"></a>

### Scheme
Scheme describes the schemes supported by the OpenAPI Swagger
and Operation objects.

| Name | Number | Description |
| ---- | ------ | ----------- |
| UNKNOWN | 0 |  |
| HTTP | 1 |  |
| HTTPS | 2 |  |
| WS | 3 |  |
| WSS | 4 |  |



<a name="grpc-gateway-protoc_gen_openapiv2-options-SecurityScheme-Flow"></a>

### SecurityScheme.Flow
The flow used by the OAuth2 security scheme. Valid values are
&#34;implicit&#34;, &#34;password&#34;, &#34;application&#34; or &#34;accessCode&#34;.

| Name | Number | Description |
| ---- | ------ | ----------- |
| FLOW_INVALID | 0 |  |
| FLOW_IMPLICIT | 1 |  |
| FLOW_PASSWORD | 2 |  |
| FLOW_APPLICATION | 3 |  |
| FLOW_ACCESS_CODE | 4 |  |



<a name="grpc-gateway-protoc_gen_openapiv2-options-SecurityScheme-In"></a>

### SecurityScheme.In
The location of the API key. Valid values are &#34;query&#34; or &#34;header&#34;.

| Name | Number | Description |
| ---- | ------ | ----------- |
| IN_INVALID | 0 |  |
| IN_QUERY | 1 |  |
| IN_HEADER | 2 |  |



<a name="grpc-gateway-protoc_gen_openapiv2-options-SecurityScheme-Type"></a>

### SecurityScheme.Type
The type of the security scheme. Valid values are &#34;basic&#34;,
&#34;apiKey&#34; or &#34;oauth2&#34;.

| Name | Number | Description |
| ---- | ------ | ----------- |
| TYPE_INVALID | 0 |  |
| TYPE_BASIC | 1 |  |
| TYPE_API_KEY | 2 |  |
| TYPE_OAUTH2 | 3 |  |


 

 

 



## Scalar Value Types

| .proto Type | Notes | C++ | Java | Python | Go | C# | PHP | Ruby |
| ----------- | ----- | --- | ---- | ------ | -- | -- | --- | ---- |
| <a name="double" /> double |  | double | double | float | float64 | double | float | Float |
| <a name="float" /> float |  | float | float | float | float32 | float | float | Float |
| <a name="int32" /> int32 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint32 instead. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="int64" /> int64 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint64 instead. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="uint32" /> uint32 | Uses variable-length encoding. | uint32 | int | int/long | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="uint64" /> uint64 | Uses variable-length encoding. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum or Fixnum (as required) |
| <a name="sint32" /> sint32 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int32s. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sint64" /> sint64 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int64s. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="fixed32" /> fixed32 | Always four bytes. More efficient than uint32 if values are often greater than 2^28. | uint32 | int | int | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="fixed64" /> fixed64 | Always eight bytes. More efficient than uint64 if values are often greater than 2^56. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum |
| <a name="sfixed32" /> sfixed32 | Always four bytes. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sfixed64" /> sfixed64 | Always eight bytes. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="bool" /> bool |  | bool | boolean | boolean | bool | bool | boolean | TrueClass/FalseClass |
| <a name="string" /> string | A string must always contain UTF-8 encoded or 7-bit ASCII text. | string | String | str/unicode | string | string | string | String (UTF-8) |
| <a name="bytes" /> bytes | May contain any arbitrary sequence of bytes. | string | ByteString | str | []byte | ByteString | string | String (ASCII-8BIT) |

