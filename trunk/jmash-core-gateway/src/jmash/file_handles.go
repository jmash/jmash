package jmash

import (
	"context"
	"log"
	"net/http"
	"fmt"
	"io"
	

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/golang/protobuf/jsonpb"
	"github.com/golang/protobuf/proto"
	
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/encoding/protojson"
	
	servicepb "gitee.com/jmash/jmash/trunk/jmash-core-gateway/src/jmash/basic"
)

var (
	pattern_FileBasic_UploadFile_0 = runtime.MustPattern(runtime.NewPattern(1, []int{2, 0, 2, 1, 2, 2}, []string{"v1", "file", "upload"}, ""))
)

//自定义文件下载解析,去掉换行符.
func FileServeMuxOption() runtime.ServeMuxOption {
	var fileMarshaler = &FileHttpBodyMarshaler {
		Marshaler: &runtime.JSONPb{
			MarshalOptions: protojson.MarshalOptions{
				EmitUnpopulated: true,
			},
			UnmarshalOptions: protojson.UnmarshalOptions{
				DiscardUnknown: true,
			},
		},
	}
	return runtime.WithMarshalerOption("*",fileMarshaler)
}

//注册XML
func XmlServeMuxOption() runtime.ServeMuxOption {
	var xmlMarshaler = &XMLMarshaler {
	    Marshaler: &runtime.JSONPb{
			MarshalOptions: protojson.MarshalOptions{
				EmitUnpopulated: true,
			},
			UnmarshalOptions: protojson.UnmarshalOptions{
				DiscardUnknown: true,
			},
		},
	}
	return runtime.WithMarshalerOption("text/xml",xmlMarshaler)
}

//注册文件上传.
func RegisterFileUploadHandler(ctx context.Context, mux *runtime.ServeMux, conn *grpc.ClientConn) error {
	var client = servicepb.NewFileBasicClient(conn)
	mux.Handle("POST", pattern_FileBasic_UploadFile_0, func(w http.ResponseWriter, req *http.Request, pathParams map[string]string) {
		ctx, cancel := context.WithCancel(req.Context())
		defer cancel()
		
		err := req.ParseForm()
		if err != nil {
			http.Error(w, fmt.Sprintf("failed to parse form: %s", err.Error()), http.StatusBadRequest)
			return 
		}

		//http header 转入 grpc MetaData.
		var annotatedContext context.Context
		annotatedContext, err = runtime.AnnotateContext(ctx, mux, req, "/jmash.basic.FileBasic/UploadFile", runtime.WithHTTPPathPattern("/v1/file/upload"))
		//调用后端服务.
		resp, md, err := request_FileBasic_UploadFile_0(annotatedContext, client, req, pathParams)
		
		if err != nil {
			http.Error(w, fmt.Sprintf("failed to get file 'file': %s", err.Error()), http.StatusBadRequest)
			return 
		}

	    // 初始化一个jsonpb编码器
	    marshaler := jsonpb.Marshaler{}
	 
	    // 将Protobuf消息转换为JSON字符串
	    jsonString, err := marshaler.MarshalToString(resp)
	    if err != nil {
	    	log.Println(md)
	        log.Fatalf("Failed to convert proto message to JSON string: %v", err)
	    }

		http.Error(w, jsonString, http.StatusOK)
		return 

	})
	return nil
}

func request_FileBasic_UploadFile_0(ctx context.Context, client servicepb.FileBasicClient, req *http.Request, pathParams map[string]string) (proto.Message, runtime.ServerMetadata, error) {
	var metadata runtime.ServerMetadata
	
	//文件	
	f, header, err := req.FormFile("file")
	if err != nil {
		log.Println("Failed to start streaming: %v", err)
		return nil, metadata, err
	}
	defer f.Close()
	
	//租户
	var tenant = req.FormValue("tenant")
	
	var protoReq servicepb.FileUploadReq
	protoReq.Tenant = tenant
	protoReq.FileName = header.Filename
	protoReq.FileSize = header.Size
	protoReq.ContentType = header.Header.Get("Content-Type")
	
	PrintlnProtoReq(protoReq)
	
	stream, err := client.UploadFile(ctx)
	if err != nil {
		log.Println("Failed to start streaming: %v", err)
		return nil, metadata, err
	}
	var data = make([]byte,3670016)
	for {
		//log.Println("1")
		n,err := f.Read(data)
		log.Println(fmt.Sprintf("Len:%d, Err:%v ",n, err))
		
		protoReq.Body = data[:n]
		
		if err == io.EOF {
			//log.Println("8")
			break
		}
		
		if err != nil {
			log.Println("Failed to decode request: %v", err)
			return nil, metadata, status.Errorf(codes.InvalidArgument, "%v", err)
		}
		
		if err = stream.Send(&protoReq); err != nil {
			if err == io.EOF {
				//log.Println("9")
				break
			}
			log.Println("Failed to send request: %v", err)
			return nil, metadata, err
		}
	}
	
	//log.Println("10")

	if err := stream.CloseSend(); err != nil {
		log.Println("Failed to terminate client stream: %v", err)
		return nil, metadata, err
	}
	headerMD, err := stream.Header()
	if err != nil {
		log.Println("Failed to get header from client: %v", err)
		return nil, metadata, err
	}
	metadata.HeaderMD = headerMD

	msg, err := stream.CloseAndRecv()
	metadata.TrailerMD = stream.Trailer()
	return msg, metadata, err
	
}

func PrintlnProtoReq(protoReq servicepb.FileUploadReq){
	log.Println(fmt.Sprintf("tenant:%s, fileName:%s, fileSize:%d, contentType:%s", protoReq.Tenant, protoReq.FileName,protoReq.FileSize,protoReq.ContentType))
}
