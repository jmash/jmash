package jmash

import (
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"google.golang.org/genproto/googleapis/api/httpbody"
)

//自定义文件解析
type FileHttpBodyMarshaler struct {
	runtime.Marshaler
}

// ContentType returns its specified content type in case v is a
// google.api.HttpBody message, otherwise it will fall back to the default Marshalers
// content type.
func (h *FileHttpBodyMarshaler) ContentType(v interface{}) string {
	//log.Println("FileHttpBodyMarshaler")
	if httpBody, ok := v.(*httpbody.HttpBody); ok {
		return httpBody.GetContentType()
	}
	return h.Marshaler.ContentType(v)
}

// Marshal marshals "v" by returning the body bytes if v is a
// google.api.HttpBody message, otherwise it falls back to the default Marshaler.
func (h *FileHttpBodyMarshaler) Marshal(v interface{}) ([]byte, error) {
	if httpBody, ok := v.(*httpbody.HttpBody); ok {
		return httpBody.GetData(), nil
	}
	return h.Marshaler.Marshal(v)
}

//分隔符设置未'',默认换行.
func (t *FileHttpBodyMarshaler)Delimiter() []byte {
	return []byte("")
}