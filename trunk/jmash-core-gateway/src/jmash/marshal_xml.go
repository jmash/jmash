package jmash

import (
    "errors"
    "io"
    "io/ioutil"
    "reflect"

    "github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
    "google.golang.org/genproto/googleapis/api/httpbody"
)

type XMLMarshaler struct {
    runtime.Marshaler
}

func (x XMLMarshaler) ContentType(v interface{}) string {
	//log.Println("XMLMarshaler")
    return "application/xml"
}

func (x XMLMarshaler) Marshal(v interface{}) ([]byte, error) {
    if httpBody, ok := v.(*httpbody.HttpBody); ok {
        return httpBody.Data, nil
    }
    return x.Marshaler.Marshal(v)
}

func (x XMLMarshaler) NewDecoder(r io.Reader) runtime.Decoder {

    return runtime.DecoderFunc(func(p interface{}) error {
        buffer, err := ioutil.ReadAll(r)
        if err != nil {
            return err
        }

        pType := reflect.TypeOf(p)
        if pType.Kind() == reflect.Ptr {
            v := reflect.Indirect(reflect.ValueOf(p))
            if v.Kind() == reflect.String {
                v.Set(reflect.ValueOf(string(buffer)))
                return nil
            }
        }
        return errors.New("value type error")
    })
}