package jmash

import (
	"context"
	"log"
	"net/http"
	"fmt"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/grpc-ecosystem/grpc-gateway/v2/utilities"
	
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	
	servicepb "gitee.com/jmash/jmash/trunk/jmash-core-gateway/src/jmash/basic"
)

var (
	pattern_FileBasic_DownloadFileRange_0 = runtime.MustPattern(runtime.NewPattern(1, []int{2, 0, 2, 1, 2, 2, 3, 0, 4, 1, 5, 3}, []string{"v1", "file", "range", "file_src"}, ""))
)

var (
	filter_FileBasic_DownloadFileRange_0 = &utilities.DoubleArray{Encoding: map[string]int{"file_src": 0}, Base: []int{1, 1, 0}, Check: []int{0, 1, 2}}
)


//注册文件Range下载.
func RegisterFileRangeHandler(ctx context.Context, mux *runtime.ServeMux, conn *grpc.ClientConn) error {
	var client = servicepb.NewFileBasicClient(conn)

	mux.Handle("GET", pattern_FileBasic_DownloadFileRange_0, func(w http.ResponseWriter, req *http.Request, pathParams map[string]string) {
		ctx, cancel := context.WithCancel(req.Context())
		defer cancel()
		inboundMarshaler, outboundMarshaler := runtime.MarshalerForRequest(mux, req)
		var err error
		var annotatedContext context.Context
		annotatedContext, err = runtime.AnnotateContext(ctx, mux, req, "/jmash.basic.FileBasic/DownloadFileRange", runtime.WithHTTPPathPattern("/v1/file/range/{file_src=**}"))
		if err != nil {
			runtime.HTTPError(ctx, mux, outboundMarshaler, w, req, err)
			return
		}
		stream, md, err := request_FileBasic_DownloadFileRange_0(annotatedContext, inboundMarshaler, client, req, pathParams)
		annotatedContext = runtime.NewServerMetadataContext(annotatedContext, md)
		if err != nil {
			runtime.HTTPError(annotatedContext, mux, outboundMarshaler, w, req, err)
			return
		}
		
		first := 0

		// 处理流式响应
	    for {
	        resp, err := stream.Recv()
	        if err != nil {
	            if err.Error() == "EOF" { 
	                break
	            }
	            //log.Println("Error receiving data: %v\n", err)
	            return
	        }
	        
	        if (first == 0){		       
		        w.Header().Set("Content-Type", resp.ContentType)
		        // 遍历请求头
			    for key, value := range resp.Headers {
					w.Header().Set(key, value)
					//log.Println("Resp Header %v:%v\n", key,value)
			    }
			    w.WriteHeader(int(resp.StatusCode))
		        first=1
	        }
	        
	        //log.Println("Received %d bytes of data\n", len(resp.Data))
	        // 将接收到的数据写入HTTP响应
	        _, err = w.Write(resp.Data)
	        if err != nil {
	            http.Error(w, fmt.Sprintf("Error writing data to HTTP response: %v", err), http.StatusInternalServerError)
	            return
	        }
	        // 刷新响应，确保数据及时发送给客户端
	        if flusher, ok := w.(http.Flusher); ok {
	            flusher.Flush()
	        }
	    }
	    //log.Println("Download completed")
	})
	return nil
}

func request_FileBasic_DownloadFileRange_0(ctx context.Context, marshaler runtime.Marshaler, client servicepb.FileBasicClient, req *http.Request, pathParams map[string]string) (servicepb.FileBasic_DownloadFileRangeClient, runtime.ServerMetadata, error) {
	var protoReq servicepb.FileRangeReq
	var metadata runtime.ServerMetadata

	var (
		val string
		ok  bool
		err error
		_   = err
	)

	val, ok = pathParams["file_src"]
	if !ok {
		return nil, metadata, status.Errorf(codes.InvalidArgument, "missing parameter %s", "file_src")
	}

	protoReq.FileSrc, err = runtime.String(val)
	if err != nil {
		return nil, metadata, status.Errorf(codes.InvalidArgument, "type mismatch, parameter: %s, error: %v", "file_src", err)
	}
	
	protoReq.Method  = "GET"
	
	protoReq.Headers = make(map[string]string)
	
	// 遍历请求头
    for key, values := range req.Header {
        // 通常取第一个值作为该请求头的值
        if len(values) > 0 {
            protoReq.Headers[key] = values[0]
        }
    }
	
	log.Println(protoReq.FileSrc);
	//log.Println(protoReq.Headers);

	stream, err := client.DownloadFileRange(ctx, &protoReq)
	if err != nil {
		return nil, metadata, err
	}
	header, err := stream.Header()
	if err != nil {
		return nil, metadata, err
	}
	metadata.HeaderMD = header
	return stream, metadata, nil

}

