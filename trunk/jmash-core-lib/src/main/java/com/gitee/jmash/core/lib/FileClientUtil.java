package com.gitee.jmash.core.lib;

import com.gitee.jmash.common.enums.MimeType;
import com.google.api.HttpBody;
import com.google.protobuf.ByteString;
import com.google.protobuf.ByteString.Output;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import javax.imageio.ImageIO;
import io.grpc.stub.StreamObserver;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import jmash.basic.FileBasicGrpc;
import jmash.protobuf.FileInfo;
import jmash.protobuf.FileUploadReq;
import jmash.protobuf.FileWebUploadReq;



/** 文件客戶端工具类. */
public class FileClientUtil {

  private static Log log = LogFactory.getLog(FileClientUtil.class);
  // grpc 默认Stream 4M限制,这里使用3.5M+Stream Head
  public static final int MAX_FILE_SIZE = 4 * 1024 * 1024 - 512 * 1024;

  /**
   * 文件下载HttpBody.
   *
   * @param destFile 下载文件路径.
   * @param latchFinish 阻塞,可以为null.
   */
  public static StreamObserver<HttpBody> createHttpBodyResp(final File destFile,
      CountDownLatch latchFinish) throws IOException {
    final File tempFile =
        File.createTempFile("download_", "." + MimeType.getExtension(destFile.getName()));
    StreamObserver<HttpBody> resp = new StreamObserver<HttpBody>() {
      @Override
      public void onNext(HttpBody value) {
        try (FileOutputStream output = new FileOutputStream(tempFile, true)) {
          output.write(value.getData().toByteArray());
        } catch (Exception e) {
          log.error("Error:", e);
        }
      }

      @Override
      public void onError(Throwable t) {
        log.error("ERROR", t);
        try {
          FileUtils.forceDelete(tempFile);
        } catch (IOException e) {
          log.error("ERROR", e);
        }
        if (null != latchFinish) {
          latchFinish.countDown();
        }
      }

      @Override
      public void onCompleted() {
        try {
          if (destFile.exists()) {
            FileUtils.forceDelete(destFile);
          }
          FileUtils.moveFile(tempFile, destFile);
        } catch (IOException e) {
          log.error("ERROR", e);
        }
        if (null != latchFinish) {
          latchFinish.countDown();
        }
      }
    };
    return resp;
  }

  /**
   * 上传文件接收信息.
   *
   * @param latchFinish 阻塞,可以为null.
   */
  public static StreamObserver<FileInfo> createFileInfoResp(CountDownLatch latchFinish) {
    StreamObserver<FileInfo> responseObserver = new StreamObserver<FileInfo>() {
      @Override
      public void onNext(FileInfo value) {
        log.info(value);
      }

      @Override
      public void onError(Throwable t) {
        log.error("ERROR", t);
        if (null != latchFinish) {
          latchFinish.countDown();
        }
      }

      @Override
      public void onCompleted() {
        log.info("上传完成");
        if (null != latchFinish) {
          latchFinish.countDown();
        }
      }
    };
    return responseObserver;
  }

  /**
   * 上传文件.
   *
   * @param req 请求
   * @param fileName 文件路径
   */
  public static void upload(String tenant, StreamObserver<FileUploadReq> req, String fileName) {
    File file = new File(fileName);
    upload(tenant, req, file, "");
  }

  /**
   * 上传文件.
   *
   * @param req 请求
   * @param fileName 文件路径
   * @param serverDir 服务器路径
   */
  public static void upload(String tenant, StreamObserver<FileUploadReq> req, String fileName,
      String serverDir) {
    File file = new File(fileName);
    upload(tenant, req, file, serverDir);
  }

  /** 上传文件. */
  public static void upload(String tenant, StreamObserver<FileUploadReq> req, File file,
      String dir) {
    MimeType mimeType = MimeType.fileMimeType(file.getName());
    upload(tenant, req, mimeType.getMimeType(), file, dir);
  }

  /** 上传文件. */
  public static void upload(String tenant, StreamObserver<FileUploadReq> req, String contentType,
      File file, String dir) {
    FileUploadReq.Builder builder =
        FileUploadReq.newBuilder().setTenant(tenant).setFileName(file.getName())
            .setFileSize(file.length()).setContentType(contentType).setFileDir(dir);
    try (FileInputStream is = new FileInputStream(file)) {
      int len = 0;
      // grpc 默认Stream 4M限制,这里使用3.5M+Stream Head
      byte[] buffer = new byte[MAX_FILE_SIZE];
      while ((len = is.read(buffer)) != -1) {
        try (Output out = ByteString.newOutput()) {
          out.write(buffer, 0, len);
          FileUploadReq uploadRep = builder.setBody(out.toByteString()).build();
          req.onNext(uploadRep);
        }
      }
    } catch (Exception ex) {
      log.error("ERROR", ex);
    }
    req.onCompleted();
  }

  /** 小文件下载HttpBody. */
  public static void download(Iterator<HttpBody> resps, final File destFile) throws IOException {
    final File tempFile =
        File.createTempFile("download_", "." + MimeType.getExtension(destFile.getName()));
    try (FileOutputStream output = new FileOutputStream(tempFile, true)) {
      while (resps.hasNext()) {
        HttpBody resp = resps.next();
        output.write(resp.getData().toByteArray());
      }
    } catch (Exception e) {
      log.error("Error:", e);
    }

    if (destFile.exists()) {
      FileUtils.forceDelete(destFile);
    }
    FileUtils.moveFile(tempFile, destFile);
  }

  /** 小文件下载HttpBody Image. */
  public static BufferedImage downloadImage(Iterator<HttpBody> resps) throws IOException {
    try (ByteArrayOutputStream output = new ByteArrayOutputStream()) {
      while (resps.hasNext()) {
        HttpBody resp = resps.next();
        output.write(resp.getData().toByteArray());
      }
      ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(output.toByteArray());
      BufferedImage bufferedImage = ImageIO.read(byteArrayInputStream);
      return bufferedImage;
    } catch (Exception e) {
      log.error("Error:", e);
    }
    return null;
  }

  /**
   * 通过Web上传File.
   */
  public static FileInfo uploadWeb(FileBasicGrpc.FileBasicBlockingStub fileBasicStub,
      String fileName) {
    File file = new File(fileName);
    return uploadWeb(fileBasicStub, file, "", null);
  }

  /**
   * 通过Web上传File.
   */
  public static FileInfo uploadWeb(FileBasicGrpc.FileBasicBlockingStub fileBasicStub,
      String fileName, String serverDir) {
    File file = new File(fileName);
    return uploadWeb(fileBasicStub, file, serverDir, null);
  }

  /**
   * 通过Web上传File.
   */
  public static FileInfo uploadWeb(FileBasicGrpc.FileBasicBlockingStub fileBasicStub, File file) {
    return uploadWeb(fileBasicStub, file, "", null);
  }

  /**
   * 通过Web上传File.
   */
  public static FileInfo uploadWeb(FileBasicGrpc.FileBasicBlockingStub fileBasicStub, File file,
      String serverDir, String uploadId) {
    Path path = Paths.get(file.toURI());
    String contentType = "";
    try {
      contentType = Files.probeContentType(path);
    } catch (Exception ex) {
      log.error("ERROR", ex);
    }
    String fileExt = MimeType.getExtension(file.getName()).toLowerCase();
    // 解决Linux probeContentType 识别空问题
    if (StringUtils.isBlank(contentType)) {
      MimeType mimeType = MimeType.convert(fileExt);
      contentType = mimeType.getMimeType();
    }

    if (StringUtils.isBlank(uploadId)) {
      uploadId = UUID.randomUUID().toString() + "." + fileExt;
    }

    FileWebUploadReq.Builder builder =
        FileWebUploadReq.newBuilder().setFileName(file.getName()).setFileSize(file.length())
            .setContentType(contentType).setFileDir(serverDir).setRequestId(uploadId);

    FileInfo fileInfo = null;
    try (FileInputStream is = new FileInputStream(file)) {

      int len = 0;
      int filePart = 1;
      // grpc 默认Stream 4M限制,这里使用3.5M+Stream Head
      byte[] buffer = new byte[MAX_FILE_SIZE];
      while ((len = is.read(buffer)) != -1) {
        try (Output out = ByteString.newOutput()) {
          out.write(buffer, 0, len);
          FileWebUploadReq uploadRep =
              builder.setBody(out.toByteString()).setFilePart(filePart++).build();
          fileInfo = fileBasicStub.uploadFileWeb(uploadRep);
        }
      }
    } catch (Exception ex) {
      log.error("ERROR", ex);
    }
    return fileInfo;
  }

}
