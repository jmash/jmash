
package com.gitee.jmash.core.lib;

import com.google.protobuf.Any;
import com.google.protobuf.Descriptors.FieldDescriptor;
import com.google.protobuf.EnumValue;
import com.google.protobuf.Int32Value;
import com.google.protobuf.Option;
import com.google.protobuf.ProtocolMessageEnum;
import com.google.protobuf.StringValue;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import jmash.protobuf.CustomEnumValue;
import jmash.protobuf.Entry;
import org.apache.commons.lang3.StringUtils;

/**
 * 枚举工具类.
 *
 * @author CGD
 *
 */
public class ProtoEnumUtil {

  /**
   * 枚举中文名.
   *
   * @param enumValue 枚举类
   * @return 中文名
   */
  public static String getEnumDisplayName(Enum<?> enumValue) {
    return (String) getEnumField(enumValue, "display_name");
  }
  
  /**
   * 枚举Value、Name List.
   *
   * @param className 枚举类
   * @param type 枚举类型
   * @return Value、Name List.
   */
  @SuppressWarnings({ "rawtypes", "unchecked" })
  public static List<Entry> getEnumCodeList(String className,int type) throws Exception {
    Class<Enum<?>> clazz = (Class) Class.forName(className);
    if (!clazz.isEnum()) {
      throw new Exception("IS NOT Enum : " + className);
    }
    return getEnumCodeList(clazz,type);
  }
  
  /**
   * 枚举Value、Name List.
   *
   * @param enumClass 枚举类
   * @param type 枚举类型
   * @return key、Value List.
   */
  @SuppressWarnings("unchecked")
  public static List<Entry> getEnumCodeList(final Class<?> enumClass,int type) {
    final List<Entry> list = new ArrayList<>();
    for (final CustomEnumValue value : getEnumCustomList((Class<Enum<?>>) enumClass)) {
      if (type<0 || type==value.getType()) {
        list.add(Entry.newBuilder().setKey(value.getName()).setValue(value.getDisplayName()).build());
      }
    }
    return list;
  }

  /**
   * 枚举Code、Name Map.
   *
   * @param enumClass 枚举类
   * @return Code、Name Map
   */
  @SuppressWarnings("unchecked")
  public static Map<String, String> getEnumNameMap(final Class<?> enumClass,int type) {
    final Map<String, String> map = new LinkedHashMap<>();
    for (final CustomEnumValue value : getEnumCustomList((Class<Enum<?>>) enumClass)) {
      if (type<0 || type==value.getType()) {
        map.put(value.getName(), value.getDisplayName());
      }
    }
    return map;
  }

  /**
   * 枚举Name、Code Map.
   *
   * @param enumClass 枚举类
   * @return Name、Code Map
   */
  @SuppressWarnings("unchecked")
  public static Map<String, String> getEnumCodeMap(final Class<?> enumClass,int type) {
    final Map<String, String> map = new LinkedHashMap<>();
    for (final CustomEnumValue value : getEnumCustomList((Class<Enum<?>>) enumClass)) {
      if ((type<0 || type==value.getType()) && StringUtils.isNotBlank(value.getDisplayName())) {
        map.put(value.getDisplayName(), value.getName());
      }
    }
    return map;
  }

  /**
   * 获取枚举值列表.
   *
   * @param className 枚举类名称
   * @return 枚举值List
   * @throws Exception 不是枚举类
   */
  @SuppressWarnings({ "rawtypes", "unchecked" })
  public static List<EnumValue> getEnumList(String className) throws Exception {
    Class<Enum<?>> clazz = (Class) Class.forName(className);
    if (!clazz.isEnum()) {
      throw new Exception("IS NOT Enum : " + className);
    }
    return getEnumList(clazz);
  }

  /**
   * 枚举列表.
   *
   * @param clazz 枚举类
   * @return 枚举值List
   */
  public static List<EnumValue> getEnumList(Class<Enum<?>> clazz) {
    List<EnumValue> list = new ArrayList<>();
    for (Enum<?> value : clazz.getEnumConstants()) {
      EnumValue.Builder enumValueBuilder = EnumValue.newBuilder().setName(value.name())
          .setNumber(value.ordinal());
      if (value instanceof ProtocolMessageEnum) {
        ProtocolMessageEnum pme = (ProtocolMessageEnum) value;
        if (!StringUtils.equals("UNRECOGNIZED", value.name())) {
          Map<FieldDescriptor, Object> map = pme.getValueDescriptor().getOptions().getAllFields();
          for (Map.Entry<FieldDescriptor, Object> entry : map.entrySet()) {
            Any any = null;
            if (entry.getValue() instanceof String) {
              any = Any.pack(StringValue.of((String) entry.getValue()));
            } else if (entry.getValue() instanceof Integer) {
              any = Any.pack(Int32Value.of((Integer) entry.getValue()));
            }
            enumValueBuilder
                .addOptions(Option.newBuilder().setName(entry.getKey().getName()).setValue(any));
          }
        }
      }
      list.add(enumValueBuilder.build());
    }
    return list;
  }

  /**
   * 枚举自定义List.
   *
   * @param clazz 枚举类
   * @return 枚举自定义List
   */
  public static List<CustomEnumValue> getEnumCustomList(Class<Enum<?>> clazz) {
    List<CustomEnumValue> list = new ArrayList<>();
    for (Enum<?> value : clazz.getEnumConstants()) {
      CustomEnumValue.Builder enumValueBuilder = CustomEnumValue.newBuilder().setName(value.name())
          .setOrdinal(value.ordinal());
      if (value instanceof ProtocolMessageEnum) {
        ProtocolMessageEnum pme = (ProtocolMessageEnum) value;
        if (!StringUtils.equals("UNRECOGNIZED", value.name())) {
          enumValueBuilder.setValue(pme.getNumber());
          Map<FieldDescriptor, Object> fieldMap = pme.getValueDescriptor().getOptions()
              .getAllFields();
          for (Map.Entry<FieldDescriptor, Object> entry : fieldMap.entrySet()) {
            if (entry.getKey().getName().equals("display_name")) {
              enumValueBuilder.setDisplayName((String) entry.getValue());
            }
            if (entry.getKey().getName().equals("type")) {
              enumValueBuilder.setType((Integer) entry.getValue());
            }
          }  
        }else {
          enumValueBuilder.setValue(-1);
          enumValueBuilder.setDisplayName("UNRECOGNIZED");
          enumValueBuilder.setType(-1);
        }
      }
      list.add(enumValueBuilder.build());
    }
    return list;
  }

  /**
   * 枚举Ordinal、Enum Map.
   *
   * @param className 枚举类名称
   * @return Ordinal、Enum Map
   * @throws Exception 不是枚举类
   */
  @SuppressWarnings({ "rawtypes", "unchecked" })
  public static Map<Integer, CustomEnumValue> getEnumMap(String className) throws Exception {
    Class<Enum<?>> clazz = (Class) Class.forName(className);
    if (!clazz.isEnum()) {
      throw new Exception("IS NOT Enum : " + className);
    }
    return getEnumMap(clazz);
  }

  /**
   * 枚举Ordinal、Enum Map.
   *
   * @param clazz 枚举类
   * @return Ordinal、Enum Map
   */
  public static Map<Integer, CustomEnumValue> getEnumMap(Class<Enum<?>> clazz) {
    Map<Integer, CustomEnumValue> map = new LinkedHashMap<>();
    for (Enum<?> value : clazz.getEnumConstants()) {
      CustomEnumValue.Builder enumValueBuilder = CustomEnumValue.newBuilder().setName(value.name())
          .setOrdinal(value.ordinal());
      if (value instanceof ProtocolMessageEnum) {
        ProtocolMessageEnum pme = (ProtocolMessageEnum) value;
        if (!StringUtils.equals("UNRECOGNIZED", value.name())) {
          Map<FieldDescriptor, Object> fieldMap = pme.getValueDescriptor().getOptions()
              .getAllFields();
          for (Map.Entry<FieldDescriptor, Object> entry : fieldMap.entrySet()) {
            if (entry.getKey().getName().equals("display_name")) {
              enumValueBuilder.setDisplayName((String) entry.getValue());
            }
            if (entry.getKey().getName().equals("type")) {
              enumValueBuilder.setType((Integer) entry.getValue());
            }
          }
        }
      }
      map.put(enumValueBuilder.getOrdinal(), enumValueBuilder.build());
    }
    return map;
  }

  /**
   * 枚举扩展字段.
   *
   * @param enumValue 枚举类
   * @param field     字段名
   * @return 扩展字段值
   */
  public static Object getEnumField(Enum<?> enumValue, String field) {
    if (enumValue instanceof ProtocolMessageEnum) {
      ProtocolMessageEnum pme = (ProtocolMessageEnum) enumValue;
      if (!StringUtils.equals("UNRECOGNIZED", enumValue.name())) {
        Map<FieldDescriptor, Object> fieldMap = pme.getValueDescriptor().getOptions()
            .getAllFields();
        for (Map.Entry<FieldDescriptor, Object> entry : fieldMap.entrySet()) {
          if (entry.getKey().getName().equals(field)) {
            return entry.getValue();
          }
        }
      }
    }
    return StringUtils.EMPTY;
  }

}
