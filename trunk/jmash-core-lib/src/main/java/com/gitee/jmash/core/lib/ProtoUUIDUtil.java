
package com.gitee.jmash.core.lib;

import java.util.UUID;
import jmash.protobuf.UUIDValue;

/**
 * UUID Util.
 *
 * @author CGD
 *
 */
public class ProtoUUIDUtil {

  public static UUIDValue fromUUID(UUID value) {
    if (value == null) {
      return UUIDValue.newBuilder().build();
    }
    return UUIDValue.newBuilder().setMostSigBits(value.getMostSignificantBits())
        .setLeastSigBits(value.getLeastSignificantBits()).build();
  }

  public static UUID toUUID(UUIDValue value) {
    if (value == null) {
      return null;
    }
    return new UUID(value.getMostSigBits(), value.getLeastSigBits());
  }
}
