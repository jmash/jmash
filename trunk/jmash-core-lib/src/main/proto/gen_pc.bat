protoc  ./jmash/protobuf/basic.proto --js_out=import_style=commonjs,binary:./  --grpc-web_out=import_style=typescript,mode=grpcweb:./
protoc  ./jmash/protobuf/file_basic.proto --js_out=import_style=commonjs,binary:./  --grpc-web_out=import_style=typescript,mode=grpcweb:./
protoc  ./jmash/rpc/file_basic_download_rpc.proto --js_out=import_style=commonjs,binary:./  --grpc-web_out=import_style=typescript,mode=grpcweb:./
protoc  ./jmash/rpc/file_basic_upload_rpc.proto --js_out=import_style=commonjs,binary:./  --grpc-web_out=import_style=typescript,mode=grpcweb:./
protoc  ./jmash/rpc/file_basic_verify_rpc.proto --js_out=import_style=commonjs,binary:./  --grpc-web_out=import_style=typescript,mode=grpcweb:./