
package com.gitee.jmash.core.lib.test;

import com.gitee.jmash.core.lib.ProtoEnumUtil;
import com.google.protobuf.EnumValue;
import java.util.List;
import java.util.Map;
import jmash.protobuf.CustomEnumValue;
import jmash.protobuf.Entry;
import jmash.protobuf.Gender;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

/**
 * ProtoEnum 工具类测试.
 *
 * @author CGD
 *
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ProtoEnumUtilTest {

  Log log = LogFactory.getLog(ProtoEnumUtilTest.class);
  
  @Test
  public void testEnumCodeList() throws Exception {
    List<Entry> values1 = ProtoEnumUtil.getEnumCodeList(Gender.class,0);
    Assertions.assertEquals(values1.size(), Gender.values().length - 3);
    Assertions.assertEquals(values1.get(0).getKey(), String.valueOf(Gender.male.name()));
    Assertions.assertEquals(values1.get(1).getKey(), String.valueOf(Gender.female.name()));
  }

  @Test
  public void testEnumDisplayName() {
    String chinaName = ProtoEnumUtil.getEnumDisplayName(Gender.female);
    Assertions.assertEquals(chinaName, "女");
    chinaName = ProtoEnumUtil.getEnumDisplayName(Gender.UNRECOGNIZED);
    Assertions.assertEquals(chinaName, "");
    chinaName = ProtoEnumUtil.getEnumDisplayName(Gender.undeclared);
    Assertions.assertEquals(chinaName, "未说明的性别");
  }

  @Test
  public void testEnumNameMap() throws Exception {
    Map<String, String> values1 = ProtoEnumUtil.getEnumNameMap(Gender.class,-1);
    Assertions.assertEquals(values1.size(), Gender.values().length);
    Assertions.assertEquals(values1.get("male"), "男");
    Assertions.assertEquals(values1.get("female"), "女");
  }

  @Test
  public void testEnumCodeMap() throws Exception {
    Map<String, String> values1 = ProtoEnumUtil.getEnumCodeMap(Gender.class,-1);
    Assertions.assertEquals(values1.size(), Gender.values().length);
    Assertions.assertEquals(values1.get("男"), Gender.male.name());
    Assertions.assertEquals(values1.get("女"), Gender.female.name());
  }

  @Test
  public void testEnumList() throws Exception {
    List<EnumValue> values1 = ProtoEnumUtil.getEnumList("jmash.protobuf.Gender");
    Assertions.assertEquals(values1.size(), Gender.values().length);
  }

  @Test
  public void testEnumMap() throws Exception {
    Map<Integer, CustomEnumValue> values = ProtoEnumUtil.getEnumMap("jmash.protobuf.Gender");
    for (CustomEnumValue v : values.values()) {
      log.info(v);
      log.info(v.getDisplayName());
    }
    Assertions.assertEquals(values.size(), Gender.values().length);
    Assertions.assertEquals(values.get(1).getName(), Gender.male.name());
  }

}
