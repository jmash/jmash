
package com.gitee.jmash.core.lib.test;

import com.gitee.jmash.core.lib.ProtoUUIDUtil;
import java.util.UUID;
import jmash.protobuf.UUIDValue;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

/**
 * UUID Util Test.
 *
 * @author CGD
 *
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ProtoUUIDUtilTest {

  Log log = LogFactory.getLog(ProtoUUIDUtilTest.class);

  @Test
  public void test() {
    UUID uuid = ProtoUUIDUtil.toUUID(UUIDValue.newBuilder().build());
    log.info(uuid);
  }
  
  
  @Test
  public void test1() {
    UUID v1 = UUID.randomUUID();
    UUIDValue uuid = ProtoUUIDUtil.fromUUID(v1);
    UUID v2 = ProtoUUIDUtil.toUUID(uuid);
    Assertions.assertEquals(v1, v2);
  }
  
}
