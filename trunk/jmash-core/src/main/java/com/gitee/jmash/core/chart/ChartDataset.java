
package com.gitee.jmash.core.chart;

import java.util.List;

/** Chart DataSet. */
public class ChartDataset {

  private String title;

  private String[] dimensions = new String[] { "xdata", "series" };

  private List<ChartSource> source;

  public ChartDataset() {
    super();
  }

  /** Chart DataSet. */
  public ChartDataset(String title, List<ChartSource> source) {
    super();
    this.title = title;
    this.source = source;
  }

  /** Chart DataSet. */
  public ChartDataset(String title, String[] dimensions, List<ChartSource> source) {
    super();
    this.title = title;
    this.dimensions = dimensions;
    this.source = source;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String[] getDimensions() {
    return dimensions;
  }

  public void setDimensions(String[] dimensions) {
    this.dimensions = dimensions;
  }

  public List<ChartSource> getSource() {
    return source;
  }

  public void setSource(List<ChartSource> source) {
    this.source = source;
  }

}
