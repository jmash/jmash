
package com.gitee.jmash.core.chart;

import java.math.BigDecimal;

/**Chart Data.*/
public class ChartSource {

  private String xdata;

  private String ydata;

  private BigDecimal series;

  public String getXdata() {
    return xdata;
  }

  public void setXdata(String xdata) {
    this.xdata = xdata;
  }

  public String getYdata() {
    return ydata;
  }

  public void setYdata(String ydata) {
    this.ydata = ydata;
  }

  public BigDecimal getSeries() {
    return series;
  }

  public void setSeries(BigDecimal series) {
    this.series = series;
  }

}
