package com.gitee.jmash.core.grpc;

import io.grpc.Codec;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Base64;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Base64编码.
 *
 * @author CGD
 *
 */
public class Base64Codec implements Codec {

  private static Log log = LogFactory.getLog(Base64Codec.class);

  @Override
  public String getMessageEncoding() {
    return "base64";
  }

  @Override
  public OutputStream compress(OutputStream os) throws IOException {
    log.debug("base64 compress");
    return Base64.getEncoder().wrap(os);
  }

  @Override
  public InputStream decompress(InputStream is) throws IOException {
    log.debug("base64 decompress");
    return Base64.getDecoder().wrap(is);
  }

}
