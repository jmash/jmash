
package com.gitee.jmash.core.grpc;

import jakarta.enterprise.context.Dependent;
import java.io.IOException;

/**
 * 默认Grpc Server.
 *
 * @author CGD
 *
 */
@Dependent
public class DefaultGrpcServer extends GrpcServer {

  /** 初始化. */
  public void init(boolean isInProcess) {
    this.setInProcess(isInProcess);
    super.init();
  }

  public void start(boolean isInProcess) throws IOException {
    init(isInProcess);
    super.start();
  }

  @Override
  public void startBefore() {
    // Do nothing .
  }

  @Override
  public void startAfter() {
    // Do nothing .
  }

  @Override
  public void stopAfter() {
    // Do nothing .
  }
}
