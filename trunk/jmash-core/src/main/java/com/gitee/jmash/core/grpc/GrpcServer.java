
package com.gitee.jmash.core.grpc;

import com.gitee.jmash.common.config.AppProps;
import com.gitee.jmash.common.tracing.TracingFactory;
import com.gitee.jmash.core.grpc.intercept.PriorityComparator;
import io.grpc.BindableService;
import io.grpc.CompressorRegistry;
import io.grpc.DecompressorRegistry;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.ServerInterceptor;
import io.grpc.ServerInterceptors;
import io.grpc.ServerServiceDefinition;
import io.grpc.health.v1.HealthCheckResponse.ServingStatus;
import io.grpc.inprocess.InProcessServerBuilder;
import io.grpc.netty.NettyServerBuilder;
import io.grpc.protobuf.services.HealthStatusManager;
import io.grpc.protobuf.services.ProtoReflectionService;
import jakarta.enterprise.inject.Any;
import jakarta.enterprise.inject.spi.Bean;
import jakarta.enterprise.inject.spi.CDI;
import jakarta.inject.Inject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.microprofile.config.inject.ConfigProperties;

/**
 * Grpc Server 基类.
 *
 * @author CGD
 *
 */
public abstract class GrpcServer {

  private static Log log = LogFactory.getLog(GrpcServer.class);

  protected ServerBuilder<?> serverBuilder;

  protected Server server;

  protected HealthStatusManager health = new HealthStatusManager();

  @Inject
  @ConfigProperties
  protected AppProps appProps;

  // 是否运行在内存中
  protected boolean isInProcess = false;
  // 是否开启反射
  protected boolean isReflection = false;

  private boolean init = false;

  /**
   * 初始化.
   */
  public void init() {
    /* The port on which the server should run */
    if (isInProcess) {
      serverBuilder = InProcessServerBuilder.forName(appProps.getName()).directExecutor();
    } else {
      serverBuilder = NettyServerBuilder.forPort(appProps.getPort());
    }
    init = true;
  }

  /**
   * 服务开始前.
   */
  public abstract void startBefore();

  public void addService(ServerServiceDefinition service) {
    serverBuilder.addService(service);
  }

  public void addService(BindableService bindableService) {
    addService(Set.of(bindableService));
  }

  /**
   * 添加服务和拦截器.
   *
   * @param bindableServices 服务
   * @param serverInterceptor 拦截器
   */
  public void addService(Collection<BindableService> bindableServices,
      ServerInterceptor... serverInterceptor) {
    for (BindableService bindableService : bindableServices) {
      log.info("GRPC: " + bindableService.getClass().toString());
      serverBuilder.addService(ServerInterceptors.intercept(bindableService, serverInterceptor));
    }
  }

  /**
   * 服务开始后.
   */
  public abstract void startAfter();

  /**
   * 服务停止后.
   */
  public abstract void stopAfter();

  /**
   * CDI Class Instance Set.
   */
  @SuppressWarnings("unchecked")
  public <T> Set<T> getInstanceSet(Class<T> clazz) {
    Set<Bean<?>> beans = CDI.current().getBeanManager().getBeans(clazz, new Any.Literal());
    Set<T> instanceSet = new HashSet<>();
    if (null != beans) {
      for (Bean<?> bean : beans) {
        Object bs = CDI.current().select(bean.getBeanClass(), new Any.Literal()).get();
        instanceSet.add((T) bs);
      }
    }
    return instanceSet;
  }

  /** 获取拦截器. */
  public ServerInterceptor[] getInterceptors() {
    Set<ServerInterceptor> interceptors = getInstanceSet(ServerInterceptor.class);
    List<ServerInterceptor> list = new ArrayList<>();
    if (interceptors != null) {
      list.addAll(interceptors);
    }
    list.add(TracingFactory.getTracing().serverInterceptor());
    // 按优先级排序.
    Collections.sort(list, new PriorityComparator());
    return list.toArray(new ServerInterceptor[0]);
  }

  /**
   * 启动服务.
   *
   * @throws IOException 启动异常
   */
  public void start() throws IOException {
    if (!init) {
      init();
    }

    Set<BindableService> grpcServices = getInstanceSet(BindableService.class);

    ServerInterceptor[] interceptors = getInterceptors();

    addService(grpcServices, interceptors);

    startBefore();

    // 主动开启反射或测试环境开启反射服务.
    if (this.isReflection || Boolean.TRUE.equals(appProps.getDevMode())) {
      serverBuilder.addService(ProtoReflectionService.newInstance());
      log.info("ProtoReflectionService Start");
    }

    // 注册Base64编码、解密
    // registryBase64();

    server = serverBuilder.addService(health.getHealthService()).build().start();
    log.info("Server started, listening on " + appProps.getPort());

    startAfter();

    Runtime.getRuntime().addShutdownHook(new Thread() {
      @Override
      public void run() {
        // Use stderr here since the logger may have been reset by its JVM shutdown
        // hook.
        log.info("*** shutting down gRPC server since JVM is shutting down");
        try {
          GrpcServer.this.stop();
        } catch (InterruptedException e) {
          log.error("Interrupted", e);
          Thread.currentThread().interrupt();
        }
        log.info("*** server shut down");
      }
    });
  }

  /**
   * 注册Base64编码、解密.
   */
  public void registryBase64() {
    // 编码-解码
    CompressorRegistry creg = CompressorRegistry.getDefaultInstance();
    creg.register(new Base64Codec());
    DecompressorRegistry dreg = DecompressorRegistry.getDefaultInstance();
    dreg = dreg.with(new Base64Codec(), true);

    serverBuilder.compressorRegistry(creg);
    serverBuilder.decompressorRegistry(dreg);
  }

  /**
   * 停止.
   *
   * @throws InterruptedException 停止异常
   */
  public void stop() throws InterruptedException {
    if (server != null) {
      server.shutdown().awaitTermination(30, TimeUnit.SECONDS);
      health.setStatus("", ServingStatus.SERVING);
      stopAfter();
    }
  }

  /**
   * Await termination on the main thread since the grpc library uses daemon threads.
   */
  public void blockUntilShutdown() throws InterruptedException {
    if (server != null) {
      server.awaitTermination();
      health.setStatus("", ServingStatus.SERVING);
      stopAfter();
    }
  }

  public boolean isInProcess() {
    return isInProcess;
  }

  public void setInProcess(boolean isInProcess) {
    this.isInProcess = isInProcess;
  }
  
  public boolean isReflection() {
    return isReflection;
  }

  public void setReflection(boolean isReflection) {
    this.isReflection = isReflection;
  }

  public AppProps getAppProps() {
    return appProps;
  }

  public void setAppProps(AppProps appProps) {
    this.appProps = appProps;
  }

}
