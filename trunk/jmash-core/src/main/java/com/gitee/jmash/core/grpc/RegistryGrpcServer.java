
package com.gitee.jmash.core.grpc;

import com.gitee.jmash.common.registry.RegistryClient;
import jakarta.inject.Inject;

/**
 * 默认Grpc Server.
 *
 * @author CGD
 *
 */
//@ApplicationScoped
public class RegistryGrpcServer extends GrpcServer {

  @Inject
  private RegistryClient registryClient;

  /** 是否注册. */
  private boolean isRegistry = true;
  /** 是否k8s环境,无需取消注册. */
  private boolean k8s = true;

  /** 初始化. */
  public void init(boolean isInProcess, boolean isRegistry) {
    this.setInProcess(isInProcess);
    this.setRegistry(isRegistry);
    super.init();
  }

  /** 初始化. */
  public void init(boolean isInProcess) {
    this.setInProcess(isInProcess);
    super.init();
  }

  @Override
  public void startBefore() {

  }

  @Override
  public void startAfter() {
    if (isRegistry && !isInProcess) {
      registryClient.serviceRegister();
      registryClient.checkRegister();
    }
  }

  @Override
  public void stopAfter() {
    if (isRegistry && !isInProcess && !k8s) {
      registryClient.checkDeregister();
      registryClient.serviceDeregister();
    }
  }

  public boolean isRegistry() {
    return isRegistry;
  }

  public void setRegistry(boolean isRegistry) {
    this.isRegistry = isRegistry;
  }

  public void setRegistryClient(RegistryClient registryClient) {
    this.registryClient = registryClient;
  }

}
