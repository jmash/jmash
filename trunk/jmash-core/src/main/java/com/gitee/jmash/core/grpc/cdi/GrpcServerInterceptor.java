
package com.gitee.jmash.core.grpc.cdi;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import jakarta.enterprise.context.Dependent;
import jakarta.enterprise.inject.Stereotype;
import jakarta.enterprise.util.AnnotationLiteral;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * GRPC Service .
 *
 * @author CGD
 *
 */
@Dependent
@Stereotype
@Target({ TYPE })
@Retention(RUNTIME)
public @interface GrpcServerInterceptor {

  /** Default Impl. */
  @SuppressWarnings("all")
  public static final class Literal extends AnnotationLiteral<GrpcServerInterceptor>
      implements GrpcServerInterceptor {
    public static final Literal INSTANCE = new Literal();
    private static final long serialVersionUID = 1L;
  }

}
