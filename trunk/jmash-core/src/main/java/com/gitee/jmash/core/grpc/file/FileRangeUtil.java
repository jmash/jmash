package com.gitee.jmash.core.grpc.file;

import com.gitee.jmash.core.jaxrs.file.BrowserUtil;
import com.gitee.jmash.core.jaxrs.file.FileDownloadUtil;
import com.gitee.jmash.core.jaxrs.file.FileRange;
import com.gitee.jmash.core.jaxrs.file.FileResourceAttributes;
import com.gitee.jmash.core.utils.FilePathUtil;
import com.google.protobuf.ByteString;
import com.google.protobuf.ByteString.Output;
import io.grpc.stub.StreamObserver;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import jmash.protobuf.FileRangeReq;
import jmash.protobuf.FileRangeResp;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 文件Range工具类.
 */
public class FileRangeUtil {
  /**
   * Full range marker.
   */
  protected static final ArrayList<FileRange> FULL = new ArrayList<FileRange>();

  protected static Log log = LogFactory.getLog(FileDownloadUtil.class);

  /**
   * The output buffer size to use when serving resources.
   */
  protected static int output = 256 * 1024;
  /**
   * The input buffer size to use when serving resources.
   */
  protected static int input = 256 * 1024;

  // grpc 默认Stream 4M限制,这里使用3.5M+Stream Head
  public static final int MAX_FILE_SIZE = 4 * 1024 * 1024 - 512 * 1024;

  /**
   * MIME multipart separation string.
   */
  protected static final String mimeSeparation = "BOUNDARY";

  /**
   * 下载文件.
   */
  public static void downloadFile(FileRangeReq req, StreamObserver<FileRangeResp> respObserver,
      Boolean useAcceptRanges) throws IOException {
    String fileName = FilePathUtil.realPath(req.getFileSrc(), false);
    File file = new File(fileName);
    if (!file.exists()) {
      sendErrorStatus(respObserver, HttpServletResponse.SC_NOT_FOUND, req.getFileSrc());
      return;
    }

    FileResourceAttributes resourceAttributes = new FileResourceAttributes(file);
    if (!checkIfHeaders(req, respObserver, resourceAttributes)) {
      return;
    }

    FileRangeResp.Builder builder = FileRangeResp.newBuilder();
    if (useAcceptRanges) {
      // Accept ranges header
      builder.putHeaders("Accept-Ranges", "bytes");
    }
    // 跨域
    builder.putHeaders("Access-Control-Allow-Origin", "*");

    // Parse range specifier
    ArrayList<FileRange> ranges = parseRange(req, respObserver, resourceAttributes);

    // ETag header
    builder.putHeaders("ETag", resourceAttributes.getEtagWeak());
    // Last-Modified header
    builder.putHeaders("Last-Modified", resourceAttributes.getLastModifiedHttp());
    // Find content type.
    String contentType = resourceAttributes.getMimeType();
    if (null == ranges || ranges.equals(FULL)) {
      downloadFullFile(req, respObserver, builder, file, contentType);
      return;
    }
    downloadRangeFile(req, respObserver, builder, file, contentType, ranges);
  }

  /**
   * Range 下载文件.
   */
  public static void downloadRangeFile(FileRangeReq req, StreamObserver<FileRangeResp> respObserver,
      FileRangeResp.Builder builder, File file, String contentType, ArrayList<FileRange> ranges)
      throws IOException {
    // Partial content response.
    builder.setStatusCode(HttpServletResponse.SC_PARTIAL_CONTENT);

    if (ranges.size() == 1) {
      FileRange range = ranges.get(0);
      String userAgent = builder.getHeadersMap().get("user-agent");
      if (!StringUtils.equals(userAgent, "com.tencent.mm.app.Application")) {
        // 华为手机小程序 Content-Length 不能传输
        builder.putHeaders("Content-Length", Long.toString(range.getRangeLength()));
      }
      builder.putHeaders("Content-Range",
          "bytes " + range.getStart() + "-" + range.getEnd() + "/" + range.getLength());

      if (contentType != null) {
        log.debug("DefaultServlet.serveFile:  contentType='" + contentType + "'");
        builder.setContentType(contentType);
      }
      writeRange(file, respObserver, builder, range);
      respObserver.onCompleted();
    } else {
      builder.setContentType("multipart/byteranges; boundary=" + mimeSeparation);
      writeRange(file, respObserver, builder, ranges.iterator(), contentType);
      respObserver.onCompleted();
    }
  }

  /**
   * Range 写文件.
   */
  public static void writeRange(File file, StreamObserver<FileRangeResp> respObserver,
      FileRangeResp.Builder builder, FileRange range) throws IOException {
    InputStream resourceInputStream = new FileInputStream(file);
    try (InputStream istream = new BufferedInputStream(resourceInputStream, input)) {
      try (Output out = ByteString.newOutput()) {
        writeRange(istream, out, range.getStart(), range.getEnd());
        respObserver.onNext(builder.setData(out.toByteString()).build());
      }
    }
  }

  /**
   * Range 写文件.
   */
  public static void writeRange(File file, StreamObserver<FileRangeResp> respObserver,
      FileRangeResp.Builder builder, Iterator<FileRange> ranges, String contentType)
      throws IOException {
    try (Output out = ByteString.newOutput()) {
      while (ranges.hasNext()) {
        InputStream resourceInputStream = new FileInputStream(file);
        try (InputStream istream = new BufferedInputStream(resourceInputStream, input)) {
          // Writing MIME header.
          out.write(("--" + mimeSeparation + "\n").getBytes());

          if (contentType != null) {
            out.write(("Content-Type: " + contentType + "\n").getBytes());
          }

          FileRange currentRange = ranges.next();
          out.write(("Content-Range: bytes " + currentRange.getStart() + "-" + currentRange.getEnd()
              + "/" + currentRange.getLength() + "\n").getBytes());
          out.write("\n".getBytes());
          // Printing content
          writeRange(istream, out, currentRange.getStart(), currentRange.getEnd());
          out.write("\n".getBytes());
        }
        respObserver.onNext(builder.setData(out.toByteString()).build());
        out.reset();
      }
      out.write(("--" + mimeSeparation + "--\n").getBytes());
      respObserver.onNext(builder.setData(out.toByteString()).build());
    }
  }

  /**
   * Range 写文件.
   */
  public static void writeRange(InputStream istream, Output out, long start, long end)
      throws IOException {
    log.debug("Serving bytes:" + start + "-" + end);
    long skipped = 0;
    try {
      skipped = istream.skip(start);
    } catch (IOException e) {
      throw e;
    }
    if (skipped < start) {
      throw new IOException(
          String.format("skipped %d,start %d ", Long.valueOf(skipped), Long.valueOf(start)));
    }
    long bytesToRead = end - start + 1;

    byte[] buffer = new byte[input];
    int len = buffer.length;
    while ((bytesToRead > 0) && (len >= buffer.length)) {
      try {
        len = istream.read(buffer);
        if (bytesToRead >= len) {
          out.write(buffer, 0, len);
          bytesToRead -= len;
        } else {
          out.write(buffer, 0, (int) bytesToRead);
          bytesToRead = 0;
        }
      } catch (IOException e) {
        len = -1;
        throw e;
      }
      if (len < buffer.length) {
        break;
      }
    }
  }

  /**
   * 通用的文件下载.
   */
  public static void downloadFullFile(FileRangeReq req, StreamObserver<FileRangeResp> respObserver,
      FileRangeResp.Builder builder, File file, String contentType) {
    builder.setStatusCode(HttpServletResponse.SC_OK);
    builder.setContentType(contentType);
    try (BufferedInputStream br = new BufferedInputStream(new FileInputStream(file));) {
      // Grpc 最大尺寸.
      byte[] buf = new byte[MAX_FILE_SIZE];
      int len = 0;
      while ((len = br.read(buf)) > 0) {
        try (Output out = ByteString.newOutput()) {
          out.write(buf, 0, len);
          respObserver.onNext(builder.setData(out.toByteString()).build());
        }
      }      
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
    respObserver.onCompleted();
  }

  /**
   * 转换下载显示的文件名.
   */
  public static String downloadFileName(HttpServletRequest request, String fileName) {
    try {
      if (BrowserUtil.isMsBrowser(request)) {
        // IE/EDGE 浏览器
        fileName = URLEncoder.encode(fileName, "UTF-8");
      } else if (BrowserUtil.isFirefox(request)) {
        // Firefox
        byte[] utf8Bytes = fileName.getBytes(StandardCharsets.UTF_8);
        fileName = Base64.getMimeEncoder().encodeToString(utf8Bytes);
      } else {
        fileName = new String(fileName.getBytes("UTF-8"), "ISO-8859-1");
      }
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    }
    return fileName;
  }

  /**
   * Parse the range header. If-Range与文件属性ETag比较，如果文件发生变化，返回FULL(Range).
   *
   * @return Vector of ranges
   */
  public static ArrayList<FileRange> parseRange(FileRangeReq req,
      StreamObserver<FileRangeResp> respObserver, FileResourceAttributes resourceAttributes)
      throws IOException {
    // Checking If-Range
    String headerValue = req.getHeadersMap().get("If-Range");
    if (StringUtils.isNotBlank(headerValue)) {
      long headerValueTime = (-1L);
      try {
        headerValueTime = getDateHeader(req.getHeadersMap(), "If-Range");
      } catch (IllegalArgumentException e) {
        // Ignore
      }
      long lastModified = resourceAttributes.getLastModified();
      if (headerValueTime == (-1L)) {
        // If the ETag the client gave does not match the entity
        // etag, then the entire entity is returned.
        if (!resourceAttributes.equalEtag(headerValue)) {
          return FULL;
        }
      } else {
        // If the timestamp of the entity the client got is older than
        // the last modification date of the entity, the entire entity
        // is returned.
        if (lastModified > (headerValueTime + 1000)) {
          return FULL;
        }
      }
    }

    long fileLength = resourceAttributes.getContentLength();

    if (fileLength == 0) {
      return null;
    }

    // Retrieving the range header (if any is specified
    String rangeHeader = req.getHeadersMap().get("Range");

    if (StringUtils.isBlank(rangeHeader)) {
      return null;
    }
    // bytes is the only range unit supported (and I don't see the point
    // of adding new ones).
    if (!rangeHeader.startsWith("bytes")) {
      sendRangeError(respObserver, fileLength);
      return null;
    }

    rangeHeader = rangeHeader.substring(6);

    // Vector which will contain all the ranges which are successfully
    // parsed.
    ArrayList<FileRange> result = new ArrayList<FileRange>();
    StringTokenizer commaTokenizer = new StringTokenizer(rangeHeader, ",");

    // Parsing the range list
    while (commaTokenizer.hasMoreTokens()) {
      String rangeDefinition = commaTokenizer.nextToken().trim();

      FileRange currentRange = new FileRange();
      currentRange.setLength(fileLength);

      int dashPos = rangeDefinition.indexOf('-');

      if (dashPos == -1) {
        sendRangeError(respObserver, fileLength);
        return null;
      }

      if (dashPos == 0) {
        try {
          long offset = Long.parseLong(rangeDefinition);
          currentRange.setStart(fileLength + offset);
          currentRange.setEnd(fileLength - 1);
        } catch (NumberFormatException e) {
          sendRangeError(respObserver, fileLength);
          return null;
        }
      } else {
        try {
          currentRange.setStart(Long.parseLong(rangeDefinition.substring(0, dashPos)));
          if (dashPos < rangeDefinition.length() - 1) {
            currentRange.setEnd(
                Long.parseLong(rangeDefinition.substring(dashPos + 1, rangeDefinition.length())));
          } else {
            currentRange.setEnd(fileLength - 1);
          }
        } catch (NumberFormatException e) {
          sendRangeError(respObserver, fileLength);
          return null;
        }
      }

      if (!currentRange.validate()) {
        sendRangeError(respObserver, fileLength);
        return null;
      }

      result.add(currentRange);
    }

    return result;
  }

  /**
   * 发送错误状态.
   */
  public static void sendErrorStatus(StreamObserver<FileRangeResp> respObserver, int statusCode,
      String message) throws UnsupportedEncodingException {
    FileRangeResp.Builder builder = FileRangeResp.newBuilder().setStatusCode(statusCode)
        .setData(ByteString.copyFrom(message, "UTF-8"));
    respObserver.onNext(builder.build());
    respObserver.onCompleted();
  }

  /**
   * 发送错误状态.
   */
  public static void sendRangeError(StreamObserver<FileRangeResp> respObserver, long fileLength)
      throws UnsupportedEncodingException {
    FileRangeResp.Builder builder = FileRangeResp.newBuilder()
        .setStatusCode(HttpServletResponse.SC_REQUESTED_RANGE_NOT_SATISFIABLE)
        .putHeaders("Content-Range", "bytes */" + fileLength);
    respObserver.onNext(builder.build());
    respObserver.onCompleted();
  }

  /**
   * 检查Http附加Header条件.
   *
   * @return 返回true 继续执行，false停止执行
   */
  public static boolean checkIfHeaders(FileRangeReq req, StreamObserver<FileRangeResp> respObserver,
      FileResourceAttributes resourceAttributes) throws IOException {
    return checkIfMatch(req, respObserver, resourceAttributes)
        && checkIfModifiedSince(req, respObserver, resourceAttributes)
        && checkIfNoneMatch(req, respObserver, resourceAttributes)
        && checkIfUnmodifiedSince(req, respObserver, resourceAttributes);

  }

  /**
   * If-Match与文件属性ETag不同,文件发生变化返回false,发送412错误.
   */
  public static boolean checkIfMatch(FileRangeReq req, StreamObserver<FileRangeResp> respObserver,
      FileResourceAttributes resourceAttributes) throws IOException {
    String headerValue = req.getHeadersMap().get("If-Match");
    if (headerValue != null) {
      if (headerValue.indexOf('*') == -1) {
        StringTokenizer commaTokenizer = new StringTokenizer(headerValue, ",");
        boolean conditionSatisfied = false;
        while (!conditionSatisfied && commaTokenizer.hasMoreTokens()) {
          String currentToken = commaTokenizer.nextToken();
          if (resourceAttributes.equalEtag(currentToken)) {
            conditionSatisfied = true;
          }
        }
        // If none of the given ETags match, 412 Precondition failed is
        // sent back
        if (!conditionSatisfied) {
          sendErrorStatus(respObserver, HttpServletResponse.SC_PRECONDITION_FAILED, "");
          return false;
        }
      }
    }
    return true;

  }

  /**
   * 如果If-None-Match指定忽略If-Modified-Since,
   * If-Modified-Since比较文件属性Last-Modified,文件未发生变化返回false,发送304错误.
   */
  public static boolean checkIfModifiedSince(FileRangeReq req,
      StreamObserver<FileRangeResp> respObserver, FileResourceAttributes resourceAttributes) {
    long headerValue = getDateHeader(req.getHeadersMap(), "If-Modified-Since");
    long lastModified = resourceAttributes.getLastModified();
    if (headerValue != -1) {
      // If an If-None-Match header has been specified, if modified
      // since is ignored.
      String noneMatch = req.getHeadersMap().get("If-None-Match");
      if ((StringUtils.isBlank(noneMatch)) && (lastModified < headerValue + 1000)) {
        // The entity has not been modified since the date
        // specified by the client. This is not an error case.
        FileRangeResp.Builder builder =
            FileRangeResp.newBuilder().setStatusCode(HttpServletResponse.SC_NOT_MODIFIED)
                .putHeaders("ETag", resourceAttributes.getEtagWeak());
        respObserver.onNext(builder.build());
        respObserver.onCompleted();
        return false;
      }
    }
    return true;

  }

  /**
   * If-None-Match与文件属性ETag比较,未发生变化返回false,GET\HEAD发送304错误,其他发送412错误.
   */
  public static boolean checkIfNoneMatch(FileRangeReq req,
      StreamObserver<FileRangeResp> respObserver, FileResourceAttributes resourceAttributes)
      throws IOException {
    String headerValue = req.getHeadersMap().get("If-None-Match");
    if (StringUtils.isNotBlank(headerValue)) {
      boolean conditionSatisfied = false;
      if (!headerValue.equals("*")) {
        StringTokenizer commaTokenizer = new StringTokenizer(headerValue, ",");

        while (!conditionSatisfied && commaTokenizer.hasMoreTokens()) {
          String currentToken = commaTokenizer.nextToken();
          if (resourceAttributes.equalEtag(currentToken)) {
            conditionSatisfied = true;
          }
        }
      } else {
        conditionSatisfied = true;
      }
      if (conditionSatisfied) {
        // For GET and HEAD, we should respond with 304 Not Modified.
        // For every other method, 412 Precondition Failed is sent back.
        if (("GET".equalsIgnoreCase(req.getMethod()))
            || ("HEAD".equalsIgnoreCase(req.getMethod()))) {
          FileRangeResp.Builder builder =
              FileRangeResp.newBuilder().setStatusCode(HttpServletResponse.SC_NOT_MODIFIED)
                  .putHeaders("ETag", resourceAttributes.getEtagWeak());
          respObserver.onNext(builder.build());
          respObserver.onCompleted();
          return false;
        }
        sendErrorStatus(respObserver, HttpServletResponse.SC_PRECONDITION_FAILED, "");
        return false;
      }
    }
    return true;

  }

  /**
   * If-Unmodified-Since比较文件属性Last-Modified,文件发生变化返回false,发送412错误.
   */
  public static boolean checkIfUnmodifiedSince(FileRangeReq req,
      StreamObserver<FileRangeResp> respObserver, FileResourceAttributes resourceAttributes)
      throws IOException {
    try {
      long lastModified = resourceAttributes.getLastModified();

      long headerValue = getDateHeader(req.getHeadersMap(), "If-Unmodified-Since");
      if (headerValue != -1) {
        if (lastModified >= (headerValue + 1000)) {
          // The entity has not been modified since the date
          // specified by the client. This is not an error case.
          sendErrorStatus(respObserver, HttpServletResponse.SC_PRECONDITION_FAILED, "");
          return false;
        }
      }
    } catch (IllegalArgumentException illegalArgument) {
      return true;
    }
    return true;
  }

  /** 时间头. */
  public static long getDateHeader(Map<String, String> headers, String name) {
    String headerValue = headers.get(name);
    if (StringUtils.isBlank(headerValue)) {
      return -1;
    }
    // 定义 HTTP 日期格式
    DateTimeFormatter[] formats = new DateTimeFormatter[] {
        DateTimeFormatter.ofPattern("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.US),
        DateTimeFormatter.ofPattern("EEEE, dd-MMM-yy HH:mm:ss zzz", Locale.US),
        DateTimeFormatter.ofPattern("EEE MMM dd HH:mm:ss yyyy", Locale.US)};

    // 尝试解析日期
    for (DateTimeFormatter format : formats) {
      try {
        // 先解析为 LocalDateTime
        LocalDateTime localDateTime = LocalDateTime.parse(headerValue, format);
        // 转换为带时区的 ZonedDateTime，假设为 UTC 时区
        ZonedDateTime zonedDateTime = localDateTime.atZone(ZoneId.of("UTC"));
        return zonedDateTime.toInstant().toEpochMilli();
      } catch (Exception e) {
        // 继续尝试下一个格式
      }
    }
    // 解析失败
    return -1;
  }

}
