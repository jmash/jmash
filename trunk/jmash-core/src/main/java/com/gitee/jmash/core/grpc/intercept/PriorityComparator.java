package com.gitee.jmash.core.grpc.intercept;

import com.gitee.jmash.core.utils.AnnotationUtils;
import io.grpc.ServerInterceptor;
import jakarta.annotation.Priority;
import java.util.Comparator;

/**
 * 拦截器优先级比较.
 *
 * @author CGD
 *
 */
public class PriorityComparator implements Comparator<ServerInterceptor> {

  @Override
  public int compare(ServerInterceptor o1, ServerInterceptor o2) {
    Priority p1 = AnnotationUtils.findAnnotation(o1.getClass(), Priority.class);
    Priority p2 = AnnotationUtils.findAnnotation(o2.getClass(), Priority.class);
    int v1 = (p1 == null) ? 5000 : p1.value();
    int v2 = (p2 == null) ? 5000 : p2.value();
    return Integer.compare(v2, v1);
  }

  
}
