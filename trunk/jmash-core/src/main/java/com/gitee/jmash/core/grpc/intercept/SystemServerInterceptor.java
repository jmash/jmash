
package com.gitee.jmash.core.grpc.intercept;

import com.gitee.jmash.common.grpc.GateWayMetadata;
import com.gitee.jmash.common.grpc.GrpcContext;
import com.gitee.jmash.common.grpc.GrpcMetadata;
import com.gitee.jmash.core.grpc.cdi.GrpcServerInterceptor;
import com.gitee.jmash.core.orm.TotalSqlTime;
import com.google.api.HttpBody;
import com.google.protobuf.Any;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageOrBuilder;
import com.google.protobuf.util.JsonFormat;
import com.google.protobuf.util.JsonFormat.Printer;
import com.google.protobuf.util.JsonFormat.TypeRegistry;
import io.grpc.Context;
import io.grpc.ForwardingServerCall.SimpleForwardingServerCall;
import io.grpc.ForwardingServerCallListener;
import io.grpc.Grpc;
import io.grpc.Metadata;
import io.grpc.ServerCall;
import io.grpc.ServerCall.Listener;
import io.grpc.ServerCallHandler;
import io.grpc.ServerInterceptor;
import jakarta.annotation.Priority;
import java.net.SocketAddress;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import jmash.protobuf.FileDownloadResp;
import jmash.protobuf.FileUploadReq;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * 系统服务端拦截器. SQL执行时间统计、客户端IP等Header属性、请求消息打印.
 *
 * @author cgd
 *
 */
@GrpcServerInterceptor
@Priority(500)
public class SystemServerInterceptor implements ServerInterceptor {

  private static Log log = LogFactory.getLog(SystemServerInterceptor.class);

  public static final Context.Key<TotalSqlTime> TOTAL_SQL_TIME = Context.key("TotalSqlTime");

  private static Printer printer;

  /** 获取Printer. */
  public static Printer getPrinter() {
    if (printer == null) {
      TypeRegistry typeRegistry =
          JsonFormat.TypeRegistry.newBuilder().add(Any.getDescriptor()).build();
      printer = JsonFormat.printer().usingTypeRegistry(typeRegistry);
    }
    return printer;
  }

  @Override
  public <ReqT, RespT> Listener<ReqT> interceptCall(ServerCall<ReqT, RespT> call, Metadata headers,
      ServerCallHandler<ReqT, RespT> next) {

    // 统计时间
    TotalSqlTime totalSqlTime = SystemServerInterceptor.TOTAL_SQL_TIME.get();
    Context context = Context.current();
    if (null == totalSqlTime) {
      context = context.withValue(TOTAL_SQL_TIME,
          new TotalSqlTime(call.getMethodDescriptor().getFullMethodName()));
    }

    if (log.isTraceEnabled()) {
      for (String key : headers.keys()) {
        log.trace(String.format("Header %s:%s", key,
            headers.get(Metadata.Key.of(key, Metadata.ASCII_STRING_MARSHALLER))));
      }
    }

    // 域名
    String host = headers.get(GateWayMetadata.USER_HOST_KEY);
    if (StringUtils.isNotBlank(host)) {
      context = context.withValue(GrpcContext.GRPC_HOST, host);
    }

    // 接受语言
    String languages = headers.get(GateWayMetadata.ACCEPT_LANGUAGE_KEY);
    if (StringUtils.isNotBlank(languages)) {
      try {
        List<Locale> list = Locale.LanguageRange.parse(languages).stream()
            .sorted(Comparator.comparing(Locale.LanguageRange::getWeight).reversed())
            .map(range -> new Locale(range.getRange())).collect(Collectors.toList());
        context = context.withValue(GrpcContext.GRPC_LOCALE, list.get(0));
      } catch (Exception ex) {
        log.warn("Parse Language Error:" + languages, ex);
      }
    }

    // 用户浏览器
    String wgBrowserAgent = headers.get(GateWayMetadata.BROWSER_USER_AGENT_KEY);
    if (StringUtils.isNotBlank(wgBrowserAgent)) {
      context = context.withValue(GrpcContext.BROWSER_USER_AGENT, wgBrowserAgent);
    }

    // referer
    String referer = headers.get(GateWayMetadata.REFERER_KEY);
    if (StringUtils.isNotBlank(referer)) {
      context = context.withValue(GrpcContext.REFERER, referer);
    }

    // cookie
    String cookie = headers.get(GateWayMetadata.COOKIE_KEY);
    if (StringUtils.isNotBlank(cookie)) {
      context = context.withValue(GrpcContext.COOKIE, cookie);
    }

    // ----------------Grpc 间传递.-----------------

    // 程序调用客户端
    String agent = headers.get(GrpcMetadata.USER_AGENT_KEY);
    if (StringUtils.isNotBlank(agent)) {
      context = context.withValue(GrpcContext.USER_AGENT, agent);
    }

    // 域名
    String grpcHost = headers.get(GrpcMetadata.GRPC_HOST_KEY);
    if (StringUtils.isNotBlank(grpcHost)) {
      context = context.withValue(GrpcContext.GRPC_HOST, grpcHost);
    }

    // 代理IP地址
    String proxyIp = headers.get(GrpcMetadata.USER_PROXY_IP_KEY);
    if (StringUtils.isBlank(proxyIp)) {
      // 获取客户端IP地址
      SocketAddress sa = call.getAttributes().get(Grpc.TRANSPORT_ATTR_REMOTE_ADDR);
      proxyIp = sa.toString();
    }
    context = context.withValue(GrpcContext.USER_PROXY_IP, proxyIp);

    // IP地址
    String ip = headers.get(GrpcMetadata.USER_IP_KEY);
    if (StringUtils.isBlank(ip)) {
      ip = proxyIp.split(",")[0];
    }
    context = context.withValue(GrpcContext.USER_IP, ip);

    // 用户浏览器
    String browserAgent = headers.get(GrpcMetadata.BROWSER_USER_AGENT_KEY);
    if (StringUtils.isNotBlank(browserAgent)) {
      context = context.withValue(GrpcContext.BROWSER_USER_AGENT, browserAgent);
    }

    // 用户语言
    String grpcLocale = headers.get(GrpcMetadata.GRPC_LOCALE_KEY);
    if (StringUtils.isNotBlank(grpcLocale)) {
      context = context.withValue(GrpcContext.GRPC_LOCALE, Locale.forLanguageTag(grpcLocale));
    }

    // 传递referer
    String httpReferer = headers.get(GrpcMetadata.HTTP_REFERER_KEY);
    if (StringUtils.isNotBlank(httpReferer)) {
      context = context.withValue(GrpcContext.REFERER, httpReferer);
    }

    // 消息请求打印
    if (log.isDebugEnabled()) {
      call = new MessagePrintServerCall<>(call);
    }

    // 回调输出结果
    Context previous = context.attach();
    try {
      return new ContextualizedServerCallListener<>(next.startCall(call, headers), context);
    } finally {
      context.detach(previous);
    }

  }

  /**
   * 服务端回复消息打印.
   *
   * @author CGD
   */
  private static class MessagePrintServerCall<ReqT, RespT>
      extends SimpleForwardingServerCall<ReqT, RespT> {

    protected MessagePrintServerCall(ServerCall<ReqT, RespT> delegate) {
      super(delegate);
    }

    @Override
    public void sendMessage(RespT message) {
      super.sendMessage(message);
      // 文件下载.
      if (message instanceof HttpBody || message instanceof FileDownloadResp) {
        return;
      }
      if (log.isTraceEnabled()) {
        try {
          String json = getPrinter().print((MessageOrBuilder) message);
          log.trace(this.getMethodDescriptor().getFullMethodName() + " resp json: \n" + json);
        } catch (InvalidProtocolBufferException ex) {
          // 不需要具体错误信息,Grpc Any类型会报错.
          log.warn(ex.getMessage());
          log.debug(
              this.getMethodDescriptor().getFullMethodName() + " resp protobuf: \n" + message);
        }
      }
    }

  }

  /**
   * 客户端请求处理过程各阶段信息打印. Implementation of {@link io.grpc.ForwardingServerCallListener} that attaches
   * a context before dispatching calls to the delegate and detaches them after the call completes.
   */
  private static class ContextualizedServerCallListener<ReqT>
      extends ForwardingServerCallListener.SimpleForwardingServerCallListener<ReqT> {
    private final Context context;

    public ContextualizedServerCallListener(ServerCall.Listener<ReqT> delegate, Context context) {
      super(delegate);
      this.context = context;
    }

    @Override
    public void onMessage(ReqT message) {
      // 打印消息请求
      TotalSqlTime totalSqlTime = TOTAL_SQL_TIME.get(context);
      Context previous = context.attach();
      try {
        super.onMessage(message);
        if (log.isDebugEnabled()) {
          if (!(message instanceof FileUploadReq)) {
            String json = getPrinter().print((MessageOrBuilder) message);
            log.debug(totalSqlTime.getFullMethodName() + " IP:" + GrpcContext.USER_IP.get(context)
                + " req json: \n" + json);
          }
        }
      } catch (InvalidProtocolBufferException ex) {
        // 不需要具体错误信息,Grpc Any类型会报错.
        log.warn(ex.getMessage());
        log.info(totalSqlTime.getFullMethodName() + " req protobuf: \n" + message);
      } finally {
        context.detach(previous);
      }
    }

    @Override
    public void onHalfClose() {
      Context previous = context.attach();
      try {
        super.onHalfClose();
      } finally {
        context.detach(previous);
      }
    }

    @Override
    public void onCancel() {
      Context previous = context.attach();
      try {
        super.onCancel();
        // 打印SQL
        TotalSqlTime totalSqlTime = TOTAL_SQL_TIME.get(context);
        if (!totalSqlTime.getList().isEmpty()) {
          log.info(totalSqlTime.getFullMethodName() + " onCancel " + totalSqlTime.toString());
        }
      } finally {
        context.detach(previous);
      }
    }

    @Override
    public void onComplete() {
      Context previous = context.attach();
      try {
        super.onComplete();
        // 打印SQL
        TotalSqlTime totalSqlTime = TOTAL_SQL_TIME.get(context);
        if (!totalSqlTime.getList().isEmpty()) {
          log.info(totalSqlTime.getFullMethodName() + " " + totalSqlTime.toString());
        }
      } finally {
        context.detach(previous);
      }
    }

    @Override
    public void onReady() {
      Context previous = context.attach();
      try {
        super.onReady();
      } finally {
        context.detach(previous);
      }
    }
  }

}
