package com.gitee.jmash.core.grpc.jpa;

import com.gitee.jmash.core.grpc.intercept.SystemServerInterceptor;
import com.gitee.jmash.core.orm.TotalSqlTime;
import com.gitee.jmash.core.orm.TotalSqlTime.SingleSqlTime;
import com.gitee.jmash.core.orm.jpa.proxy.QueryProxy;
import jakarta.persistence.Query;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.time.Instant;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * JPA Query Proxy 统计SQL执行时间.
 *
 * @author CGD
 *
 */
public class GrpcQueryProxy implements QueryProxy {

  private static Log log = LogFactory.getLog(GrpcQueryProxy.class);

  private Query target;

  /**
   * 创建代理对象.
   * 
   */
  public Query getInstance(Query target) {
    this.target = target;
    if (target == null) {
      return null;
    }
    if (this.target instanceof Proxy
        && Proxy.getInvocationHandler(this.target) instanceof GrpcQueryProxy) {
      return this.target;
    }
    Object obj = Proxy.newProxyInstance(target.getClass().getClassLoader(),
        target.getClass().getInterfaces(), this);
    return (Query) obj;
  }

  /**
   * Proxy invoke.
   */
  public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
    Object result = null;
    boolean isStatis = isStatis(method.getName());
    Instant begin = Instant.now();
    result = method.invoke(target, args);
    if (isStatis) {
      Instant end = Instant.now();
      long time = end.toEpochMilli() - begin.toEpochMilli();
      // 记录线程SQL执行时间
      TotalSqlTime totalSqlTime = SystemServerInterceptor.TOTAL_SQL_TIME.get();
      if (null != totalSqlTime) {
        totalSqlTime.add(new SingleSqlTime(time));
      }
      if (time > TotalSqlTime.LONG_QUERY_TIME) {
        log.info(String.format("%s time:%dms", method.getName(), time));
      } else {
        log.trace(String.format("%s time:%dms", method.getName(), time));
      }
    }
    return result;
  }

  /**
   * 那些方法参与统计.
   *
   * @param methodName 方法名
   * @return 是否统计
   */
  public boolean isStatis(String methodName) {
    if (methodName.equals("getResultList")) {
      return true;
    }
    if (methodName.equals("getSingleResult")) {
      return true;
    }
    if (methodName.equals("executeUpdate")) {
      return true;
    }
    return false;
  }
}
