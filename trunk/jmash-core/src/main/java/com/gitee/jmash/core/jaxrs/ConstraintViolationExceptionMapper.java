
package com.gitee.jmash.core.jaxrs;

import jakarta.validation.ConstraintViolationException;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;
import java.util.HashSet;
import java.util.Set;

/**
 * Custom Jaxrs Validation Exception .
 *
 * @author CGD
 *
 */
@Provider
public class ConstraintViolationExceptionMapper
    implements ExceptionMapper<ConstraintViolationException> {

  @Override
  public Response toResponse(ConstraintViolationException exception) {
    return Response.status(Response.Status.BAD_REQUEST).entity(prepareMessage(exception))
        .type(MediaType.APPLICATION_JSON).build();
  }

  private ValidationError prepareMessage(ConstraintViolationException exception) {
    Set<ValidationParamError> errors = new HashSet<>();
    exception.getConstraintViolations().stream().forEach(
        v -> errors.add(new ValidationParamError(v.getPropertyPath().toString(), v.getMessage())));
    return new ValidationError(errors);
  }
}
