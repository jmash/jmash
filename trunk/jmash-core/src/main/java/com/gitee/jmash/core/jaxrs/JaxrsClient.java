
package com.gitee.jmash.core.jaxrs;

import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.Map;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;

/** Jaxrs Client . */
public class JaxrsClient<E extends Serializable> {

  private WebTarget target;

  public JaxrsClient() {
    super();
  }

  public JaxrsClient(WebTarget target) {
    super();
    this.target = target;
  }

  @SuppressWarnings("unchecked")
  private Class<E> entityClass = (Class<E>) ((ParameterizedType) getClass().getGenericSuperclass())
      .getActualTypeArguments()[0];

  /** Page. */
  public <T> T list(String resourcePath, Map<String, Object> queryParams,
      GenericType<T> genericType) {
    WebTarget t = target.path(resourcePath);
    for (Map.Entry<String, Object> param : queryParams.entrySet()) {
      t = t.queryParam(param.getKey(), param.getValue());
    }
    Response res = t.request(MediaType.APPLICATION_JSON).get();

    if (res.getStatus() != 200) {
      throw new ValidationClientException(res);
    }

    T page = res.readEntity(genericType);
    return page;
  }

  /** findById. */
  public E find(String resourcePath, String id) {
    Response res = target.path(resourcePath + "/" + id).request(MediaType.APPLICATION_JSON).get();
    if (res.getStatus() != 200) {
      throw new ValidationClientException(res);
    }
    E entity = res.readEntity(entityClass);
    return entity;
  }

  /** create. */
  public E create(String resourcePath, Entity<?> reqEntity) {
    Response res = target.path(resourcePath).request(MediaType.APPLICATION_JSON).post(reqEntity);
    if (res.getStatus() != 200) {
      throw new ValidationClientException(res);
    }
    E entity = res.readEntity(entityClass);
    return entity;
  }

  /** update. */
  public E update(String resourcePath, Entity<?> reqEntity) {
    Response res = target.path(resourcePath).request(MediaType.APPLICATION_JSON).put(reqEntity);
    if (res.getStatus() != 200) {
      throw new ValidationClientException(res);
    }
    E entity = res.readEntity(entityClass);
    return entity;
  }

  /** delete. */
  public E delete(String resourcePath, String id) {
    Response res = target.path(resourcePath + "/" + id).request(MediaType.APPLICATION_JSON)
        .delete();
    if (res.getStatus() != 200) {
      throw new ValidationClientException(res);
    }
    E entity = res.readEntity(entityClass);
    return entity;
  }

  /** batch delete. */
  public int batchDelete(String resourcePath, Set<String> ids) {
    Response res = target.path(resourcePath + "/batch/" + StringUtils.join(ids, ","))
        .request(MediaType.APPLICATION_JSON).delete();
    if (res.getStatus() != 200) {
      throw new ValidationClientException(res);
    }
    return res.readEntity(Integer.class);
  }

  public WebTarget getTarget() {
    return target;
  }

  public void setTarget(WebTarget target) {
    this.target = target;
  }

}
