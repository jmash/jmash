
package com.gitee.jmash.core.jaxrs;

import com.gitee.jmash.common.config.AppProps;
import jakarta.enterprise.context.Dependent;
import jakarta.inject.Inject;
import jakarta.ws.rs.SeBootstrap;
import jakarta.ws.rs.core.Application;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;
import org.eclipse.microprofile.config.inject.ConfigProperties;

/**
 * Java SE Restful Server.
 *
 * @author CGD
 *
 */
@Dependent
public class JaxrsServer {

  @Inject
  @ConfigProperties
  protected AppProps appProps;

  SeBootstrap.Instance instance;

  /** start Create Server . */
  public void start(Application application) throws InterruptedException, ExecutionException {
    final SeBootstrap.Configuration config = SeBootstrap.Configuration.builder()
        .host("0.0.0.0").protocol(appProps.getType().name()).port(appProps.getPort())
        .rootPath(appProps.getContext().orElse("")).build();
    start(application, config);
  }

  /** start Create Server . */
  public void start(Application application, SeBootstrap.Configuration config)
      throws InterruptedException, ExecutionException {
    CompletionStage<SeBootstrap.Instance> completionStage = SeBootstrap.start(application, config);
    instance = completionStage.toCompletableFuture().get();
  }

  /**
   * Stop Server.
   */
  public void stop() {
    if (null != instance) {
      instance.stop();
    }
  }

}
