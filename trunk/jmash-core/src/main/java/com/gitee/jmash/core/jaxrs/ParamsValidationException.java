
package com.gitee.jmash.core.jaxrs;

import jakarta.validation.ValidationException;
import java.util.HashSet;
import java.util.Set;

/**
 * 参数验证异常.
 *
 * @author CGD
 *
 */
public class ParamsValidationException extends ValidationException {

  private static final long serialVersionUID = 1L;

  private final Set<ValidationParamError> validationParamErrors;

  /** 参数异常. */
  public ParamsValidationException(String param, String message) {
    super(String.format("参数%s:%s", param, message));
    this.validationParamErrors = new HashSet<>();
    this.validationParamErrors.add(new ValidationParamError(param, message));
  }

  public ParamsValidationException(Set<ValidationParamError> validationParamErrors) {
    super();
    this.validationParamErrors = validationParamErrors;
  }

  public Set<ValidationParamError> getValidationParamErrors() {
    return validationParamErrors;
  }

  public ValidationError getValidationError() {
    return new ValidationError(validationParamErrors);
  }

}
