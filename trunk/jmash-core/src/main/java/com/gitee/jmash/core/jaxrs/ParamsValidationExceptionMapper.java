
package com.gitee.jmash.core.jaxrs;

import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

/**
 * Custom Jaxrs Validation Exception .
 *
 * @author CGD
 *
 */
@Provider
public class ParamsValidationExceptionMapper implements ExceptionMapper<ParamsValidationException> {

  @Override
  public Response toResponse(ParamsValidationException exception) {
    return Response.status(Response.Status.BAD_REQUEST).entity(exception.getValidationError())
        .type(MediaType.APPLICATION_JSON).build();
  }

}
