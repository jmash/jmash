
package com.gitee.jmash.core.jaxrs;

import com.gitee.jmash.core.service.ServiceException;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Custom Jaxrs Jmash ServiceException .
 *
 * @author CGD
 *
 */
@Provider
public class ServiceExceptionMapper implements ExceptionMapper<ServiceException> {

  private static Log log = LogFactory.getLog(ServiceExceptionMapper.class);

  @Override
  public Response toResponse(ServiceException exception) {
    log.error("", exception);
    return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
        .entity(new ValidationError(exception.getMessage())).type(MediaType.APPLICATION_JSON)
        .build();
  }

}
