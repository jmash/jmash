
package com.gitee.jmash.core.jaxrs;

import jakarta.ws.rs.core.Response;
import java.util.Set;

/**
 * Jaxrs Validation Client Exception .
 *
 * @author CGD
 *
 */
public class ValidationClientException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  private int status;

  private String reason;

  private Set<ValidationParamError> errors;

  /** Response Create. */
  public ValidationClientException(Response res) {
    status = res.getStatus();
    reason = res.getStatusInfo().getReasonPhrase();
    if (res.getStatus() == 400) {
      ValidationError error = res.readEntity(ValidationError.class);
      if (null != error) {
        reason = error.getMessage();
        errors = error.getErrors();
      }
    }
  }

  public int getStatus() {
    return status;
  }

  public String getReason() {
    return reason;
  }

  public Set<ValidationParamError> getErrors() {
    return errors;
  }

  @Override
  public String toString() {
    return "ValidationClientException [status=" + status + ", reason=" + reason + ", errors="
        + errors + "]";
  }

}
