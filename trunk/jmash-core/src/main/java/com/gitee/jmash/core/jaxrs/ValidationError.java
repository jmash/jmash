
package com.gitee.jmash.core.jaxrs;

import java.util.Set;

/**
 * 验证错误.
 *
 * @author CGD
 *
 */
public class ValidationError {

  private String message;

  private Set<ValidationParamError> errors;

  public ValidationError() {
    super();
  }

  public ValidationError(String message) {
    super();
    this.message = message;
  }

  /** 构造函数. */
  public ValidationError(Set<ValidationParamError> errors) {
    super();
    this.errors = errors;
    if (!errors.isEmpty()) {
      StringBuilder builder = new StringBuilder();
      int i = 1;
      for (ValidationParamError error : errors) {
        builder.append((i++) + "." + error.getParam() + ":" + error.getMessage() + ";<br/>");
      }
      this.message = builder.toString();
    }
  }
  
  /** 构造函数. */
  public ValidationError(String message, Set<ValidationParamError> errors) {
    super();
    this.message = message;
    this.errors = errors;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public Set<ValidationParamError> getErrors() {
    return errors;
  }

  public void setErrors(Set<ValidationParamError> errors) {
    this.errors = errors;
  }

}
