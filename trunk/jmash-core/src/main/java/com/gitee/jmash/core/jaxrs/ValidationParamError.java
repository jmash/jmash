
package com.gitee.jmash.core.jaxrs;

/**
 * 验证错误.
 *
 * @author CGD
 *
 */
public class ValidationParamError {

  private String param;

  private String message;

  public ValidationParamError() {
    super();
  }

  /** 构造函数. */
  public ValidationParamError(String param, String message) {
    super();
    this.param = param;
    this.message = message;
  }

  public String getParam() {
    return param;
  }

  public void setParam(String param) {
    this.param = param;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  @Override
  public String toString() {
    return "ValidationParamError [param=" + param + ", message=" + message + "]";
  }

  
}
