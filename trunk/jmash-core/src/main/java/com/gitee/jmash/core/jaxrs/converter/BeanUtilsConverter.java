
package com.gitee.jmash.core.jaxrs.converter;

import com.crenjoy.proto.beanutils.ProtoConvertUtils;
import jakarta.ws.rs.ext.ParamConverter;
import org.apache.commons.lang3.StringUtils;

/**
 * BeanUtils(ProtoBeanUtils) 提供Jaxrs Object to String 转换.
 *
 * @author CGD
 */
public class BeanUtilsConverter<T> implements ParamConverter<T> {

  Class<T> rawType;

  public BeanUtilsConverter(Class<T> rawType) {
    this.rawType = rawType;
  }

  @Override
  public T fromString(String value) {
    if (StringUtils.isBlank(value)) {
      return null;
    }
    return ProtoConvertUtils.convert(value, rawType);
  }

  @Override
  public String toString(T value) {
    if (value == null) {
      return null;
    }
    return ProtoConvertUtils.convert(value);
  }

}
