
package com.gitee.jmash.core.jaxrs.converter;

import com.crenjoy.proto.beanutils.ProtoConvertUtils;
import com.google.protobuf.Message;
import jakarta.ws.rs.ext.ParamConverter;
import jakarta.ws.rs.ext.ParamConverterProvider;
import jakarta.ws.rs.ext.Provider;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

/**
 * Jmash Jaxrs Converter Provider.
 *
 * @author CGD
 *
 */
@Provider
public class MvcParamConverterProvider implements ParamConverterProvider {

  @SuppressWarnings("unchecked")
  @Override
  public <T> ParamConverter<T> getConverter(Class<T> rawType, Type genericType,
      Annotation[] annotations) {

    if (rawType.isAssignableFrom(Message.class)) {
      return (ParamConverter<T>) new ProtoJsonConverter((Class<Message>) rawType);
    }

    if (ProtoConvertUtils.lookup(rawType) != null) {
      return new BeanUtilsConverter<T>(rawType);
    }

    return null;
  }

}
