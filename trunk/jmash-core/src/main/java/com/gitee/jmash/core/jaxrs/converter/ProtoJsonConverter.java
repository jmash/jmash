
package com.gitee.jmash.core.jaxrs.converter;

import com.google.protobuf.Message;
import com.google.protobuf.util.JsonFormat;
import jakarta.ws.rs.ext.ParamConverter;
import java.lang.reflect.Method;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.LogFactory;

/**
 * BeanUtils(ProtoBeanUtils) 提供Jaxrs Object to String 转换.
 *
 * @author CGD
 */
public class ProtoJsonConverter implements ParamConverter<Message> {

  Class<Message> rawMessageype;

  public ProtoJsonConverter(Class<Message> rawMessageype) {
    this.rawMessageype = rawMessageype;
  }
  
  @Override
  public Message fromString(String value) {
    if (StringUtils.isBlank(value)) {
      return null;
    }
    try {
      Method method = rawMessageype.getDeclaredMethod("newBuilder");
      Message.Builder builder = (Message.Builder) method.invoke(method);
      JsonFormat.parser().merge(value, builder);
      return (Message) builder.build();
    } catch (Exception ex) {
      LogFactory.getLog(ProtoJsonConverter.class).error(ex);
      return null;
    }
  }

  @Override
  public String toString(Message value) {
    if (value == null) {
      return null;
    }
    try {
      String json = JsonFormat.printer().print(value);
      return json;
    } catch (Exception ex) {
      LogFactory.getLog(ProtoJsonConverter.class).error(ex);
      return null;
    }
  }

}
