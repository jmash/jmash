package com.gitee.jmash.core.jaxrs.file;

import jakarta.servlet.http.HttpServletRequest;

/**
 * 浏览器信息获取工具类.
 */
public class BrowserUtil {

  static final String[] MOBILE_SPECIFIC_SUBSTRING = {"iPhone", "Android", "MIDP", "Opera Mobi",
      "Opera Mini", "BlackBerry", "HP iPAQ", "IEMobile", "MSIEMobile", "Windows Phone", "HTC", "LG",
      "MOT", "Nokia", "Symbian", "Fennec", "Maemo", "Tear", "Midori", "armv", "Windows CE",
      "WindowsCE", "Smartphone", "240x320", "176x220", "320x320", "160x160", "webOS", "Palm",
      "Sagem", "Samsung", "SGH", "SonyEricsson", "MMP", "UCWEB"};

  static final String[] IE_SPECIFIC_SUBSTRING = {"MSIE", "Trident", "Edge"};

  /**
   * 是否IE浏览器.
   */
  public static boolean isMsBrowser(HttpServletRequest request) {
    String userAgent = getUserAgent(request);
    for (String signal : IE_SPECIFIC_SUBSTRING) {
      if (userAgent.contains(signal)) {
        return true;
      }
    }
    return false;
  }

  /**
   * 是否Firefox.
   */
  public static boolean isFirefox(HttpServletRequest request) {
    String userAgent = getUserAgent(request);
    return userAgent.contains("Firefox");
  }

  /**
   * 是否Chrome.
   */
  public static boolean isChrome(HttpServletRequest request) {
    String userAgent = getUserAgent(request);
    return userAgent.contains("Chrome");
  }

  /**
   * 获取用户浏览器.
   */
  public static String getUserAgent(HttpServletRequest request) {
    String userAgent =
        request.getHeader("User-Agent") == null ? "" : request.getHeader("User-Agent");
    return userAgent;
  }

  /**
   * 判断是否移动端浏览器.
   */
  public static boolean checkMobile(HttpServletRequest request) {
    // 取用户操作系统信息
    String userAgent = getUserAgent(request);
    for (String mobile : MOBILE_SPECIFIC_SUBSTRING) {
      if (userAgent.contains(mobile) || userAgent.contains(mobile.toUpperCase())
          || userAgent.contains(mobile.toLowerCase())) {
        return true;
      }
    }
    return false;
  }

  /**
   * 判断是否微信内置浏览器.
   */
  public static boolean checkWeiXin(HttpServletRequest request) {
    // 取用户操作系统信息
    String userAgent = getUserAgent(request);
    String weixin = "MicroMessenger".toLowerCase();
    return userAgent.toLowerCase().contains(weixin);
  }
}
