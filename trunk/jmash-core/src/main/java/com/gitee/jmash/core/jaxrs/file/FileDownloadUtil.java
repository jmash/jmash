package com.gitee.jmash.core.jaxrs.file;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Iterator;
import java.util.StringTokenizer;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Http File Download Util.
 *
 * @author CGD
 * 
 */
public class FileDownloadUtil {

  /**
   * Full range marker.
   */
  protected static final ArrayList<FileRange> FULL = new ArrayList<FileRange>();

  protected static Log log = LogFactory.getLog(FileDownloadUtil.class);

  /**
   * The output buffer size to use when serving resources.
   */
  protected static int output = 256 * 1024;
  /**
   * The input buffer size to use when serving resources.
   */
  protected static int input = 256 * 1024;

  /**
   * MIME multipart separation string.
   */
  protected static final String mimeSeparation = "BOUNDARY";

  /**
   * 下载文件.
   */
  public static void downloadFile(HttpServletRequest request, HttpServletResponse response,
      File file, String fileName, Boolean isOnLine, Boolean useAcceptRanges) throws IOException {
    if (!file.exists()) {
      // Check if we're included so we can return the appropriate
      // missing resource name in the error
      String requestUri = (String) request.getAttribute(RequestDispatcher.INCLUDE_REQUEST_URI);
      if (requestUri == null) {
        requestUri = request.getRequestURI();
      } else {
        // We're included
        // SRV.9.3 says we must throw a FNFE
        throw new FileNotFoundException(requestUri);
      }
      response.sendError(HttpServletResponse.SC_NOT_FOUND, requestUri);
      return;
    }

    FileResourceAttributes resourceAttributes = new FileResourceAttributes(file);
    if (!checkIfHeaders(request, response, resourceAttributes)) {
      return;
    }

    // Find content type.
    String contentType = resourceAttributes.getMimeType();
    if (contentType == null) {
      contentType = request.getServletContext().getMimeType(resourceAttributes.getFileName());
    }

    if (useAcceptRanges) {
      // Accept ranges header
      response.setHeader("Accept-Ranges", "bytes");
    }
    // 跨域
    response.setHeader("Access-Control-Allow-Origin", "*");

    // Parse range specifier
    ArrayList<FileRange> ranges = parseRange(request, response, resourceAttributes);

    // ETag header
    response.setHeader("ETag", resourceAttributes.getEtagWeak());

    // Last-Modified header
    response.setHeader("Last-Modified", resourceAttributes.getLastModifiedHttp());

    if (null == ranges || ranges.equals(FULL)) {
      downloadFullFile(request, response, file, contentType, fileName, isOnLine);
      return;
    }
    downloadRangeFile(request, response, file, contentType, ranges);
  }

  /**
   * Range 下载文件.
   */
  public static void downloadRangeFile(HttpServletRequest request, HttpServletResponse response,
      File file, String contentType, ArrayList<FileRange> ranges) throws IOException {
    // Partial content response.
    response.setStatus(HttpServletResponse.SC_PARTIAL_CONTENT);

    if (ranges.size() == 1) {
      FileRange range = ranges.get(0);

      String userAgent = request.getHeader("user-agent");
      if (!StringUtils.equals(userAgent, "com.tencent.mm.app.Application")) {
        // 华为手机小程序 Content-Length 不能传输,IOS Agent:MicroMessenger Client
        response.setHeader("Content-Length", Long.toString(range.getRangeLength()));
      }
      response.addHeader("Content-Range",
          "bytes " + range.getStart() + "-" + range.getEnd() + "/" + range.getLength());

      if (contentType != null) {
        log.debug("DefaultServlet.serveFile:  contentType='" + contentType + "'");
        response.setContentType(contentType);
      }

      try (ServletOutputStream output = response.getOutputStream()) {
        writeRange(file, output, range);
        output.flush();
      }
    } else {
      response.setContentType("multipart/byteranges; boundary=" + mimeSeparation);
      try (ServletOutputStream output = response.getOutputStream()) {
        writeRange(file, output, ranges.iterator(), contentType);
        output.flush();
      }
    }
  }

  /**
   * Range 写文件.
   */
  public static void writeRange(File file, ServletOutputStream ostream, FileRange range)
      throws IOException {
    InputStream resourceInputStream = new FileInputStream(file);
    try (InputStream istream = new BufferedInputStream(resourceInputStream, input)) {
      writeRange(istream, ostream, range.getStart(), range.getEnd());
    }
  }

  /**
   * Range 写文件.
   */
  public static void writeRange(File file, ServletOutputStream ostream, Iterator<FileRange> ranges,
      String contentType) throws IOException {
    while (ranges.hasNext()) {
      InputStream resourceInputStream = new FileInputStream(file);
      try (InputStream istream = new BufferedInputStream(resourceInputStream, input)) {
        // Writing MIME header.
        ostream.println("--" + mimeSeparation);
        if (contentType != null) {
          ostream.println("Content-Type: " + contentType);
        }

        FileRange currentRange = ranges.next();
        ostream.println("Content-Range: bytes " + currentRange.getStart() + "-"
            + currentRange.getEnd() + "/" + currentRange.getLength());
        ostream.println();
        // Printing content
        writeRange(istream, ostream, currentRange.getStart(), currentRange.getEnd());
        ostream.println();
      }
    }
    ostream.println("--" + mimeSeparation + "--");
  }

  /**
   * Range 写文件.
   */
  public static void writeRange(InputStream istream, ServletOutputStream ostream, long start,
      long end) throws IOException {
    log.debug("Serving bytes:" + start + "-" + end);
    long skipped = 0;
    try {
      skipped = istream.skip(start);
    } catch (IOException e) {
      throw e;
    }
    if (skipped < start) {
      throw new IOException(
          String.format("skipped %d,start %d ", Long.valueOf(skipped), Long.valueOf(start)));
    }
    long bytesToRead = end - start + 1;

    byte[] buffer = new byte[input];
    int len = buffer.length;
    while ((bytesToRead > 0) && (len >= buffer.length)) {
      try {
        len = istream.read(buffer);
        if (bytesToRead >= len) {
          ostream.write(buffer, 0, len);
          bytesToRead -= len;
        } else {
          ostream.write(buffer, 0, (int) bytesToRead);
          bytesToRead = 0;
        }
      } catch (IOException e) {
        len = -1;
        throw e;
      }
      if (len < buffer.length) {
        break;
      }
    }
  }

  /**
   * 通用的文件下载.
   */
  public static void downloadFullFile(HttpServletRequest request, HttpServletResponse response,
      File file, String contentType, String fileName, Boolean isOnLine) {
    response.setStatus(HttpServletResponse.SC_OK);
    fileName = StringUtils.isEmpty(fileName) ? file.getName() : fileName;
    // 转换为下载文件名
    fileName = downloadFileName(request, fileName);

    try (BufferedInputStream br = new BufferedInputStream(new FileInputStream(file));) {
      if (StringUtils.isNotEmpty(contentType)) {
        response.setContentType(contentType);
      }
      response.setHeader("Content-Length", Integer.toString(br.available()));
      // 下载文件
      if (isOnLine) { // 在线打开方式
        response.setHeader("Content-Disposition", "inline; filename=\"" + fileName + "\"");
        // 文件名应该编码成UTF-8
      } else if (file.getName().endsWith(".swf")) {
        response.setHeader("Content-Disposition", "inline; filename=\"" + fileName + "\"");
      } else {
        // 纯下载方式
        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
      }

      try (OutputStream out = response.getOutputStream()) {
        // 默认2048
        byte[] buf = new byte[input];
        int len = 0;
        while ((len = br.read(buf)) > 0) {
          out.write(buf, 0, len);
        }
        out.flush();
      }
    } catch (Exception e) {
      throw new RuntimeException(e.getMessage());
    }
  }

  /**
   * 转换下载显示的文件名.
   */
  public static String downloadFileName(HttpServletRequest request, String fileName) {
    try {
      if (BrowserUtil.isMsBrowser(request)) {
        // IE/EDGE 浏览器
        fileName = URLEncoder.encode(fileName, "UTF-8");
      } else if (BrowserUtil.isFirefox(request)) {
        // Firefox
        byte[] utf8Bytes = fileName.getBytes(StandardCharsets.UTF_8);
        fileName = Base64.getMimeEncoder().encodeToString(utf8Bytes);
      } else {
        fileName = new String(fileName.getBytes("UTF-8"), "ISO-8859-1");
      }
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    }
    return fileName;
  }

  /**
   * Parse the range header. If-Range与文件属性ETag比较，如果文件发生变化，返回FULL(Range).
   *
   * @return Vector of ranges
   */
  public static ArrayList<FileRange> parseRange(HttpServletRequest request,
      HttpServletResponse response, FileResourceAttributes resourceAttributes) throws IOException {
    // Checking If-Range
    String headerValue = request.getHeader("If-Range");
    if (headerValue != null) {
      long headerValueTime = (-1L);
      try {
        headerValueTime = request.getDateHeader("If-Range");
      } catch (IllegalArgumentException e) {
        // Ignore
      }
      long lastModified = resourceAttributes.getLastModified();
      if (headerValueTime == (-1L)) {
        // If the ETag the client gave does not match the entity
        // etag, then the entire entity is returned.
        if (!resourceAttributes.equalEtag(headerValue)) {
          return FULL;
        }
      } else {
        // If the timestamp of the entity the client got is older than
        // the last modification date of the entity, the entire entity
        // is returned.
        if (lastModified > (headerValueTime + 1000)) {
          return FULL;
        }
      }
    }

    long fileLength = resourceAttributes.getContentLength();

    if (fileLength == 0) {
      return null;
    }

    // Retrieving the range header (if any is specified
    String rangeHeader = request.getHeader("Range");

    if (rangeHeader == null) {
      return null;
    }
    // bytes is the only range unit supported (and I don't see the point
    // of adding new ones).
    if (!rangeHeader.startsWith("bytes")) {
      response.addHeader("Content-Range", "bytes */" + fileLength);
      response.sendError(HttpServletResponse.SC_REQUESTED_RANGE_NOT_SATISFIABLE);
      return null;
    }

    rangeHeader = rangeHeader.substring(6);

    // Vector which will contain all the ranges which are successfully
    // parsed.
    ArrayList<FileRange> result = new ArrayList<FileRange>();
    StringTokenizer commaTokenizer = new StringTokenizer(rangeHeader, ",");

    // Parsing the range list
    while (commaTokenizer.hasMoreTokens()) {
      String rangeDefinition = commaTokenizer.nextToken().trim();

      FileRange currentRange = new FileRange();
      currentRange.setLength(fileLength);

      int dashPos = rangeDefinition.indexOf('-');

      if (dashPos == -1) {
        response.addHeader("Content-Range", "bytes */" + fileLength);
        response.sendError(HttpServletResponse.SC_REQUESTED_RANGE_NOT_SATISFIABLE);
        return null;
      }

      if (dashPos == 0) {

        try {
          long offset = Long.parseLong(rangeDefinition);
          currentRange.setStart(fileLength + offset);
          currentRange.setEnd(fileLength - 1);
        } catch (NumberFormatException e) {
          response.addHeader("Content-Range", "bytes */" + fileLength);
          response.sendError(HttpServletResponse.SC_REQUESTED_RANGE_NOT_SATISFIABLE);
          return null;
        }

      } else {

        try {
          currentRange.setStart(Long.parseLong(rangeDefinition.substring(0, dashPos)));
          if (dashPos < rangeDefinition.length() - 1) {
            currentRange.setEnd(
                Long.parseLong(rangeDefinition.substring(dashPos + 1, rangeDefinition.length())));
          } else {
            currentRange.setEnd(fileLength - 1);
          }
        } catch (NumberFormatException e) {
          response.addHeader("Content-Range", "bytes */" + fileLength);
          response.sendError(HttpServletResponse.SC_REQUESTED_RANGE_NOT_SATISFIABLE);
          return null;
        }

      }

      if (!currentRange.validate()) {
        response.addHeader("Content-Range", "bytes */" + fileLength);
        response.sendError(HttpServletResponse.SC_REQUESTED_RANGE_NOT_SATISFIABLE);
        return null;
      }

      result.add(currentRange);
    }

    return result;
  }

  /**
   * 检查Http附加Header条件.
   *
   * @return 返回true 继续执行，false停止执行
   */
  public static boolean checkIfHeaders(HttpServletRequest request, HttpServletResponse response,
      FileResourceAttributes resourceAttributes) throws IOException {
    return checkIfMatch(request, response, resourceAttributes)
        && checkIfModifiedSince(request, response, resourceAttributes)
        && checkIfNoneMatch(request, response, resourceAttributes)
        && checkIfUnmodifiedSince(request, response, resourceAttributes);

  }

  /**
   * If-Match与文件属性ETag不同,文件发生变化返回false,发送412错误.
   */
  public static boolean checkIfMatch(HttpServletRequest request, HttpServletResponse response,
      FileResourceAttributes resourceAttributes) throws IOException {
    String headerValue = request.getHeader("If-Match");
    if (headerValue != null) {
      if (headerValue.indexOf('*') == -1) {

        StringTokenizer commaTokenizer = new StringTokenizer(headerValue, ",");
        boolean conditionSatisfied = false;

        while (!conditionSatisfied && commaTokenizer.hasMoreTokens()) {
          String currentToken = commaTokenizer.nextToken();
          if (resourceAttributes.equalEtag(currentToken)) {
            conditionSatisfied = true;
          }
        }

        // If none of the given ETags match, 412 Precondition failed is
        // sent back
        if (!conditionSatisfied) {
          response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED);
          return false;
        }

      }
    }
    return true;

  }

  /**
   * 如果If-None-Match指定忽略If-Modified-Since,
   * If-Modified-Since比较文件属性Last-Modified,文件未发生变化返回false,发送304错误.
   */
  public static boolean checkIfModifiedSince(HttpServletRequest request,
      HttpServletResponse response, FileResourceAttributes resourceAttributes) {
    try {
      long headerValue = request.getDateHeader("If-Modified-Since");
      long lastModified = resourceAttributes.getLastModified();
      if (headerValue != -1) {

        // If an If-None-Match header has been specified, if modified
        // since is ignored.
        if ((request.getHeader("If-None-Match") == null) && (lastModified < headerValue + 1000)) {
          // The entity has not been modified since the date
          // specified by the client. This is not an error case.
          response.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
          response.setHeader("ETag", resourceAttributes.getEtagWeak());
          try (ServletOutputStream output = response.getOutputStream()) {
            output.flush();
          }
          return false;
        }
      }
    } catch (IOException illegalArgument) {
      return true;
    }
    return true;

  }

  /**
   * If-None-Match与文件属性ETag比较,未发生变化返回false,GET\HEAD发送304错误,其他发送412错误.
   */
  public static boolean checkIfNoneMatch(HttpServletRequest request, HttpServletResponse response,
      FileResourceAttributes resourceAttributes) throws IOException {
    String headerValue = request.getHeader("If-None-Match");
    if (headerValue != null) {

      boolean conditionSatisfied = false;

      if (!headerValue.equals("*")) {

        StringTokenizer commaTokenizer = new StringTokenizer(headerValue, ",");

        while (!conditionSatisfied && commaTokenizer.hasMoreTokens()) {
          String currentToken = commaTokenizer.nextToken();
          if (resourceAttributes.equalEtag(currentToken)) {
            conditionSatisfied = true;
          }
        }

      } else {
        conditionSatisfied = true;
      }

      if (conditionSatisfied) {

        // For GET and HEAD, we should respond with 304 Not Modified.
        // For every other method, 412 Precondition Failed is sent back.
        if (("GET".equals(request.getMethod())) || ("HEAD".equals(request.getMethod()))) {
          response.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
          response.setHeader("ETag", resourceAttributes.getEtagWeak());
          try (ServletOutputStream output = response.getOutputStream()) {
            output.flush();
          }
          return false;
        }
        response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED);
        return false;
      }
    }
    return true;

  }

  /**
   * If-Unmodified-Since比较文件属性Last-Modified,文件发生变化返回false,发送412错误.
   */
  public static boolean checkIfUnmodifiedSince(HttpServletRequest request,
      HttpServletResponse response, FileResourceAttributes resourceAttributes) throws IOException {
    try {
      long lastModified = resourceAttributes.getLastModified();
      long headerValue = request.getDateHeader("If-Unmodified-Since");
      if (headerValue != -1) {
        if (lastModified >= (headerValue + 1000)) {
          // The entity has not been modified since the date
          // specified by the client. This is not an error case.
          response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED);
          return false;
        }
      }
    } catch (IllegalArgumentException illegalArgument) {
      return true;
    }
    return true;
  }

}
