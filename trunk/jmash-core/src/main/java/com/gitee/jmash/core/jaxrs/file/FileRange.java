package com.gitee.jmash.core.jaxrs.file;

/**
 * 文件区域数据段,文件访问Range.
 */
public class FileRange {

  private long start = 0;
  private long end = -1;
  private long length = 0;

  /**
   * Validate range.
   */
  public boolean validate() {
    if (end >= length) {
      end = length - 1;
    }
    return (start >= 0) && (end >= 0) && (start <= end) && (length > 0);
  }

  public long getRangeLength() {
    return end - start + 1;
  }

  public long getStart() {
    return start;
  }

  public void setStart(long start) {
    this.start = start;
  }

  public long getEnd() {
    return end;
  }

  public void setEnd(long end) {
    this.end = end;
  }

  public long getLength() {
    return length;
  }

  public void setLength(long length) {
    this.length = length;
  }

  @Override
  public String toString() {
    return "FileRange [start=" + start + ", end=" + end + ", length=" + length + "] RangeLength= "
        + this.getRangeLength();
  }


}
