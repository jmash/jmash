package com.gitee.jmash.core.jaxrs.file;

import com.gitee.jmash.common.enums.MimeType;
import com.gitee.jmash.common.utils.FileHashUtil;
import com.gitee.jmash.crypto.digests.SM3Util;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * Http File Resource Attributes.
 *
 * @author CGD
 *
 */
public class FileResourceAttributes {

  /**
   * HTTP date format.
   */
  protected  final SimpleDateFormat format =
      new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.US);

  protected File file;

  public FileResourceAttributes(File file) {
    this.file = file;
  }

  /** 文件MimeType. */
  public String getMimeType() {
    String anyfileExt = FilenameUtils.getExtension(file.getName()).toLowerCase();
    MimeType mimeType = MimeType.convert(anyfileExt);
    if (null == mimeType) {
      return null;
    }
    return mimeType.getMimeType();
  }

  public String getFileName() {
    return file.getName();
  }

  public long getContentLength() {
    return file.length();
  }

  public long getLastModified() {
    return file.lastModified();
  }

  /** 文件上次修改时间. */
  public String getLastModifiedHttp() {
    Date modifiedDate = new Date(getLastModified());
    synchronized (format) {
      return format.format(modifiedDate);
    }
  }

  /** 文件ETag. */
  public String getEtagWeak() {
    long contentLength = getContentLength();
    long lastModified = getLastModified();
    if ((contentLength >= 0) || (lastModified >= 0)) {
      return "W/\"" + contentLength + "-" + lastModified + "\"";
    }
    return getEtagStrong();
  }

  /** 文件ETag(文件校验). */
  public String getEtagStrong() {
    return FileHashUtil.getFileHashString(file,
        new String[] {SM3Util.get().getMessageDigest().getAlgorithm()})[0];
  }

  /** 文件校验. */
  public boolean equalEtag(String etag) {
    etag = etag.trim();
    if (StringUtils.startsWith(etag, "W/")) {
      // Weak ETag
      return StringUtils.equals(getEtagWeak(), etag);
    }
    // Strong ETag
    return StringUtils.equals(getEtagStrong(), etag);
  }
}
