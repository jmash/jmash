
package com.gitee.jmash.core.jaxrs.filter;

import com.gitee.jmash.common.jaxrs.WebContext;
import com.gitee.jmash.core.orm.TotalSqlTime;
import com.gitee.jmash.core.utils.WebUtil;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.container.PreMatching;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.UriInfo;
import jakarta.ws.rs.ext.Provider;
import java.io.IOException;
import org.apache.commons.logging.LogFactory;
import org.eclipse.microprofile.jwt.JsonWebToken;

/**
 * 服务端系统Web过滤器(Thread Context,执行时间统计).
 *
 * @author CGD
 *
 */
@Provider
@PreMatching
public class ContainerFilter implements ContainerRequestFilter {

  // 线程SQL统计.
  public static final ThreadLocal<TotalSqlTime> TOTAL_SQL_TIME = new ThreadLocal<>();

  @Context
  HttpServletRequest request;

  @Override
  public void filter(ContainerRequestContext ctx) throws IOException {
    UriInfo uriInfo = ctx.getUriInfo();

    // 统计时间
    TotalSqlTime totalSqlTime = TOTAL_SQL_TIME.get();
    if (null == totalSqlTime) {
      TOTAL_SQL_TIME.set(new TotalSqlTime(uriInfo.getPath()));
    }

    // User IP .
    String userIp = WebUtil.realityIp(request);
    WebContext.USER_IP.set(userIp);
    // Proxy IP.
    String proxyIp = WebUtil.proxyIp(request);
    WebContext.PROXY_IP.set(proxyIp);
    JsonWebToken token = getJsonWebToken();
    if (null != token) {
      WebContext.USER_TOKEN.set(token);
    }

    ctx.getHeaders().putSingle("USER_IP", userIp);
    ctx.getHeaders().putSingle("PROXY_IP", proxyIp);
  }

  /** 获取Login Subject. */
  public JsonWebToken getJsonWebToken() {
    try {
      return (JsonWebToken) request.getUserPrincipal();
    } catch (Exception ex) {
      LogFactory.getLog(ContainerFilter.class).warn("HttpServletRequest.getUserPrincipal Error.");
    }
    return null;
  }

}
