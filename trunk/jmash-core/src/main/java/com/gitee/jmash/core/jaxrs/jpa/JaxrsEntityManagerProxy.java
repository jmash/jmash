package com.gitee.jmash.core.jaxrs.jpa;


import com.gitee.jmash.core.jaxrs.filter.ContainerFilter;
import com.gitee.jmash.core.orm.TotalSqlTime;
import com.gitee.jmash.core.orm.TotalSqlTime.SingleSqlTime;
import com.gitee.jmash.core.orm.jpa.proxy.EntityManagerProxy;
import com.gitee.jmash.core.orm.jpa.proxy.QueryProxy;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.time.Instant;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 基于cglib的proxy实现.
 *
 * @author CGD
 * 
 */
public class JaxrsEntityManagerProxy implements EntityManagerProxy {

  private static Log log = LogFactory.getLog(JaxrsEntityManagerProxy.class);

  private EntityManager target;

  protected Class<? extends QueryProxy> queryProxy;

  /**
   * 创建代理对象.
   */
  @Override
  public EntityManager getInstance(EntityManager target, Class<? extends QueryProxy> queryProxy) {
    this.target = target;
    this.queryProxy = queryProxy;
    if (target == null) {
      return null;
    }
    if (this.target instanceof Proxy
        && Proxy.getInvocationHandler(this.target) instanceof JaxrsEntityManagerProxy) {
      return this.target;
    }
    Object obj = Proxy.newProxyInstance(target.getClass().getClassLoader(),
        target.getClass().getInterfaces(), this);
    return (EntityManager) obj;
  }

  /**
   * Proxy invoke.
   */
  public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
    Object result = null;
    boolean isStatis = isStatis(method.getName());
    // 统计SQL时间
    Instant begin = Instant.now();
    result = method.invoke(target, args);
    if (isStatis) {
      Instant end = Instant.now();
      long time = end.toEpochMilli() - begin.toEpochMilli();
      // 记录线程SQL执行时间
      TotalSqlTime totalSqlTime = ContainerFilter.TOTAL_SQL_TIME.get();
      if (null != totalSqlTime) {
        totalSqlTime.add(new SingleSqlTime(time));
      }
      if (time > TotalSqlTime.LONG_QUERY_TIME) {
        log.info(String.format("%s time:%dms", method.getName(), time));
      } else {
        log.debug(String.format("%s time:%dms", method.getName(), time));
      }
    }
    // 封装结果
    if ((queryProxy != null) && (result instanceof Query)) {
      // log.debug(result.getClass().getName());
      QueryProxy proxyObj = queryProxy.getDeclaredConstructor().newInstance();
      return proxyObj.getInstance((Query) result);
    }
    return result;
  }

  /**
   * 那些方法参与统计.
   *
   * @param methodName 方法名
   * @return 是否统计
   */
  public boolean isStatis(String methodName) {
    if (methodName.equals("persist")) {
      return true;
    }
    if (methodName.equals("merge")) {
      return true;
    }
    if (methodName.equals("remove")) {
      return true;
    }
    if (methodName.equals("find")) {
      return true;
    }
    if (methodName.equals("getReference")) {
      return true;
    }
    return false;
  }

}
