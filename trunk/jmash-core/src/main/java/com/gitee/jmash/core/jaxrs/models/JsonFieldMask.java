
package com.gitee.jmash.core.jaxrs.models;

import java.util.ArrayList;
import java.util.List;

/**
 * 对应 com.google.protobuf.FieldMask.
 *
 * @author CGD
 *
 */
public class JsonFieldMask {

  List<String> paths = new ArrayList<>();

  public void addPaths(String path) {
    paths.add(path);
  }

  public int getPathsCount() {
    return (paths == null) ? 0 : this.paths.size();
  }

  public List<String> getPaths() {
    return paths;
  }

  public void setPaths(List<String> paths) {
    this.paths = paths;
  }

}
