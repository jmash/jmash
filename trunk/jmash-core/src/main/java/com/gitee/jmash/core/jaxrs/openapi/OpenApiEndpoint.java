
package com.gitee.jmash.core.jaxrs.openapi;

import io.smallrye.openapi.runtime.io.Format;
import io.smallrye.openapi.runtime.io.OpenApiSerializer;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import java.nio.charset.StandardCharsets;
import java.util.stream.Stream;
import org.eclipse.microprofile.openapi.models.OpenAPI;

/**
 * 提供应用OpenApi服务接口.
 *
 * @author CGD
 *
 */
@Path("openapi")
public class OpenApiEndpoint {

  @Context
  HttpHeaders httpHeaders;

  OpenAPI openApi = OpenApiProvider.createOpenApi();

  static final MediaType APPLICATION_YAML = new MediaType("application", "yaml");

  /**
   * OpenApi 服务接口.
   */
  @GET
  public Response openApi(@QueryParam("format") final String format) throws Exception {
    final Format formatOpenApi = getOpenApiFormat(httpHeaders, format);
    return Response
        .ok(OpenApiSerializer.serialize(openApi, formatOpenApi).getBytes(StandardCharsets.UTF_8))
        .type(formatOpenApi.getMimeType()).build();
  }

  private Format getOpenApiFormat(final HttpHeaders httpHeaders, final String format) {
    return Stream.of(Format.values())
        .filter(f -> format != null && f.name().compareToIgnoreCase(format) == 0).findFirst()
        .orElse(httpHeaders.getAcceptableMediaTypes().contains(APPLICATION_YAML) ? Format.YAML
            : Format.JSON);
  }
}
