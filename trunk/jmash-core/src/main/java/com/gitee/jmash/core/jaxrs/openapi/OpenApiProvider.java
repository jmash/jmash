
package com.gitee.jmash.core.jaxrs.openapi;

import com.alibaba.druid.support.logging.LogFactory;
import io.smallrye.openapi.api.OpenApiConfig;
import io.smallrye.openapi.api.OpenApiConfigImpl;
import io.smallrye.openapi.api.OpenApiDocument;
import io.smallrye.openapi.runtime.OpenApiProcessor;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import org.eclipse.microprofile.config.Config;
import org.eclipse.microprofile.config.ConfigProvider;
import org.eclipse.microprofile.openapi.models.OpenAPI;
import org.jboss.jandex.CompositeIndex;
import org.jboss.jandex.IndexReader;
import org.jboss.jandex.IndexView;


/**
 * OpenApi Provider.
 *
 * @author CGD
 *
 */
public class OpenApiProvider {

  /** 遍历idx文件创建OpenAPI. */
  public static OpenAPI createOpenApi() {
    try {
      Optional<OpenAPI> staticOpenApi = Stream.of(readerOpenApiFromIdx("META-INF/jandex.idx"))
          .filter(Optional::isPresent).findFirst().flatMap(file -> file);
      return staticOpenApi.orElse(null);
    } catch (Exception e) {
      LogFactory.getLog(OpenApiProvider.class).warn("Init OpenApi:", e);
      return null;
    }
  }
  
  /** 读取idx文件. */
  private static Optional<OpenAPI> readerOpenApiFromIdx(final String location) {
    try {
      final Enumeration<URL> resources =
          Thread.currentThread().getContextClassLoader().getResources(location);
      if (!resources.hasMoreElements()) {
        return Optional.empty();
      }
      List<IndexView> indexs = new ArrayList<>();
      while (resources.hasMoreElements()) {
        URL url = resources.nextElement();
        IndexReader reader = new IndexReader(url.openStream());
        indexs.add(reader.read());
      }

      final OpenApiDocument document = OpenApiDocument.INSTANCE;
      Config config = ConfigProvider.getConfig();
      OpenApiConfig openApiConfig = new OpenApiConfigImpl(config);
      document.reset();
      document.config(openApiConfig);
      document.filter(OpenApiProcessor.getFilter(openApiConfig,
          Thread.currentThread().getContextClassLoader()));

      document.modelFromAnnotations(
          OpenApiProcessor.modelFromAnnotations(openApiConfig, CompositeIndex.create(indexs)));

      document.initialize();
      return Optional.of(document.get());
    } catch (Exception ex) {
      return Optional.of(null);
    }
  }

}
