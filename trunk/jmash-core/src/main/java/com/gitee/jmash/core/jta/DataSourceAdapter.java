package com.gitee.jmash.core.jta;

import jakarta.enterprise.inject.spi.CDI;
import jakarta.transaction.Status;
import jakarta.transaction.SystemException;
import jakarta.transaction.TransactionManager;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.Objects;
import java.util.logging.Logger;
import javax.sql.DataSource;

/**
 * 代理事务.
 *
 * @author cgd.
 */
public class DataSourceAdapter implements DataSource {

  /** 唯一资源名. */
  private String uniqueResourceName;
  /** 数据源. */
  private DataSource dataSource;
  /** 事务管理器. */
  private TransactionManager transactionManager;

  /**
   * 创建代理数据源.
   *
   * @param uniqueResourceName 唯一资源名称.
   * @param dataSource 数据源.
   */
  public DataSourceAdapter(String uniqueResourceName, DataSource dataSource) {
    super();
    this.uniqueResourceName = uniqueResourceName;
    this.dataSource = dataSource;
  }

  /** 获取当前事务. */
  public DataSourceTransaction getTransaction() {
    if (transactionManager == null) {
      transactionManager = CDI.current().select(TransactionManager.class).get();
    }
    if (transactionManager == null) {
      return null;
    }
    try {
      return (DataSourceTransaction) transactionManager.getTransaction();
    } catch (SystemException ex) {
      return null;
    }
  }

  @Override
  public Connection getConnection() throws SQLException {
    DataSourceTransaction transaction = getTransaction();
    // 不在事务管理内.
    if (transaction == null) {
      Connection conn = dataSource.getConnection();
      DataSourceConnection dsc = new DataSourceConnection();
      dsc.getInstance(conn, false);
      return dsc.getProxy();
    }
    // 检查事务状态
    checkStatus(transaction);
    // 事务管理添加数据库连接.
    if (!transaction.getConnMap().containsKey(uniqueResourceName)) {
      Connection conn = dataSource.getConnection();
      conn.setAutoCommit(false);
      DataSourceConnection dsc = new DataSourceConnection();
      dsc.getInstance(conn, true);
      transaction.getConnMap().put(uniqueResourceName, dsc);
    }
    return transaction.getConnMap().get(uniqueResourceName).getProxy();
  }

  @Override
  public Connection getConnection(String arg0, String arg1) throws SQLException {
    DataSourceTransaction transaction = getTransaction();
    // 不在事务管理内.
    if (transaction == null) {
      Connection conn = dataSource.getConnection(arg0, arg1);
      DataSourceConnection dsc = new DataSourceConnection();
      dsc.getInstance(conn, false);
      return dsc.getProxy();
    }
    // 检查事务状态
    checkStatus(transaction);
    // 事务管理添加数据库连接.
    if (!transaction.getConnMap().containsKey(uniqueResourceName)) {
      Connection conn = dataSource.getConnection(arg0, arg1);
      conn.setAutoCommit(false);
      DataSourceConnection dsc = new DataSourceConnection();
      dsc.getInstance(conn, true);
      transaction.getConnMap().put(uniqueResourceName, dsc);
    }
    return transaction.getConnMap().get(uniqueResourceName).getProxy();
  }

  /** 检查事务状态. */
  private void checkStatus(DataSourceTransaction transaction) {
    try {
      if (transaction.getStatus() != Status.STATUS_ACTIVE) {
        throw new RuntimeException("事务状态非激活状态,不能获取数据库连接");
      }
    } catch (SystemException e) {
      throw new RuntimeException("获取事务状态失败!");
    }
  }

  @Override
  public Logger getParentLogger() throws SQLFeatureNotSupportedException {
    return dataSource.getParentLogger();
  }

  @Override
  public boolean isWrapperFor(Class<?> arg0) throws SQLException {
    return dataSource.isWrapperFor(arg0);
  }

  @Override
  public <T> T unwrap(Class<T> arg0) throws SQLException {
    return dataSource.unwrap(arg0);
  }

  @Override
  public PrintWriter getLogWriter() throws SQLException {
    return dataSource.getLogWriter();
  }

  @Override
  public int getLoginTimeout() throws SQLException {
    return dataSource.getLoginTimeout();
  }

  @Override
  public void setLogWriter(PrintWriter arg0) throws SQLException {
    dataSource.setLogWriter(arg0);
  }

  @Override
  public void setLoginTimeout(int arg0) throws SQLException {
    dataSource.setLoginTimeout(arg0);
  }

  public String getUniqueResourceName() {
    return uniqueResourceName;
  }

  public void setUniqueResourceName(String uniqueResourceName) {
    this.uniqueResourceName = uniqueResourceName;
  }

  public DataSource getDataSource() {
    return dataSource;
  }

  public void setDataSource(DataSource dataSource) {
    this.dataSource = dataSource;
  }

  @Override
  public int hashCode() {
    return Objects.hash(uniqueResourceName);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    DataSourceAdapter other = (DataSourceAdapter) obj;
    return Objects.equals(uniqueResourceName, other.uniqueResourceName);
  }
}
