package com.gitee.jmash.core.jta;

import com.gitee.jmash.core.orm.JdbcContext;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.sql.Connection;
import org.apache.commons.lang3.StringUtils;

/**
 * Connection 拦截.
 *
 * @author cgd
 */
public class DataSourceConnection implements InvocationHandler {

  private Connection target;

  private Connection proxy;
  // 是否包含事务.
  private boolean hasTransaction = false;

  /**
   * 创建代理对象.
   * 
   */
  public Connection getInstance(Connection target, boolean hasTransaction) {
    this.target = target;
    this.hasTransaction = hasTransaction;
    if (target == null) {
      return null;
    }
    if (this.target instanceof Proxy
        && Proxy.getInvocationHandler(this.target) instanceof DataSourceConnection) {
      return this.target;
    }
    Object obj = Proxy.newProxyInstance(target.getClass().getClassLoader(),
        target.getClass().getInterfaces(), this);
    proxy = (Connection) obj;
    return proxy;
  }

  /**
   * Proxy invoke.
   */
  public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
    Object result = null;
    // 关闭不做任何事.
    if (hasTransaction && StringUtils.equals("close", method.getName())) {
      return result;
    }
    // 分表SQL处理
    if (StringUtils.equals("prepareStatement", method.getName()) && JdbcContext.isTableSuffux()) {
      args[0] = JdbcContext.replaceSql((String) args[0]);
    }
    result = method.invoke(target, args);
    return result;
  }

  public Connection getTarget() {
    return target;
  }

  public Connection getProxy() {
    return proxy;
  }

}
