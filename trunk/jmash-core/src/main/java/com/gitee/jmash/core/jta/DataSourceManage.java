package com.gitee.jmash.core.jta;

import com.alibaba.druid.pool.DruidDataSource;
import com.crenjoy.proto.beanutils.ProtoBeanUtils;
import com.gitee.jmash.core.orm.cdi.DataSourceConfig;
import com.gitee.jmash.core.orm.jpa.DataSourceProps;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javax.sql.DataSource;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.LogFactory;
import org.eclipse.microprofile.config.Config;
import org.eclipse.microprofile.config.ConfigProvider;

/**
 * 数据源管理器.
 *
 * @author cgd
 */
public class DataSourceManage {

  /** 相同数据库读写数据源. */
  protected static final Map<String, DataSource> dataSourceMap = new ConcurrentHashMap<>();

  /** 创建模块租户数据源. */
  public static synchronized DataSource getDataSource(String database, boolean write) {
    database = (database == null) ? "" : database;
    String uniqueResourceName = (write ? "write" : "read") + database + "DS";
    if (dataSourceMap.containsKey(uniqueResourceName)) {
      return dataSourceMap.get(uniqueResourceName);
    }
    DruidDataSource defaultDataSource = createDataSource(database, write);
    //读写数据源都使用代理，分表查询需要。
    DataSourceAdapter dsProxy = new DataSourceAdapter(uniqueResourceName, defaultDataSource);
    dataSourceMap.put(uniqueResourceName, dsProxy);
    return dataSourceMap.get(uniqueResourceName);
  }

  /** 创建数据源. */
  private static DruidDataSource createDataSource(String database, boolean write) {
    DataSourceProps props = getDataSourceProps(write);
    DruidDataSource defaultDataSource = new DruidDataSource();
    try {
      ProtoBeanUtils.copyProperties(defaultDataSource, props);
    } catch (IllegalAccessException | InvocationTargetException e) {
      LogFactory.getLog(DataSourceConfig.class).error(e);
    }

    if (StringUtils.isNotBlank(database)) {
      defaultDataSource.setUrl(props.getUrl().replaceFirst("/[\\w_]+\\?", "/" + database + "?"));
      defaultDataSource.setMinIdle(5);
    }

    defaultDataSource.setPassword(props.getPassword());
    if (write) {
      defaultDataSource.setDefaultReadOnly(false);
      defaultDataSource.setDefaultAutoCommit(false);
    } else {
      defaultDataSource.setDefaultReadOnly(true);
      defaultDataSource.setDefaultAutoCommit(true);
    }
    return defaultDataSource;
  }

  /** 读写数据源配置. */
  private static DataSourceProps getDataSourceProps(boolean write) {
    if (write) {
      return getDataSourceProps("write.jdbc");
    } else {
      return getDataSourceProps("read.jdbc");
    }
  }

  /** 满足flowable初始化加载需求(此时CDI未完成加载). */
  private static DataSourceProps getDataSourceProps(String prefix) {
    DataSourceProps props = new DataSourceProps();
    props.setUrl(getConfigValue(prefix, "url", String.class));
    props.setUsername(getConfigValue(prefix, "username", String.class));
    props.setPassword(getConfigValue(prefix, "password", String.class));
    props.setValidationQuery(getConfigValue(prefix, "validationQuery", String.class));
    props.setInitialSize(getConfigValue(prefix, "initialSize", Integer.class));
    props.setMinIdle(getConfigValue(prefix, "minIdle", Integer.class));
    props.setMaxActive(getConfigValue(prefix, "maxActive", Integer.class));
    return props;
  }

  /** 获取配置值. */
  private static <T> T getConfigValue(String prefix, String name, Class<T> clazz) {
    Config config = ConfigProvider.getConfig();
    return config.getValue(prefix + "." + name, clazz);
  }

}
