package com.gitee.jmash.core.jta;

import com.gitee.jmash.common.utils.UUIDUtil;
import com.gitee.jmash.core.jta.interceptor.TransactionEntityManager;
import jakarta.persistence.EntityTransaction;
import jakarta.transaction.HeuristicMixedException;
import jakarta.transaction.HeuristicRollbackException;
import jakarta.transaction.RollbackException;
import jakarta.transaction.Status;
import jakarta.transaction.Synchronization;
import jakarta.transaction.SystemException;
import jakarta.transaction.Transaction;
import java.sql.SQLException;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import javax.transaction.xa.XAResource;
import org.apache.commons.logging.LogFactory;

/** 数据源事务. */
public class DataSourceTransaction implements Transaction {

  // 事务暂停.
  public static final int STATUS_SUSPEND = 10;

  /** 事务唯一ID. */
  private String xid;
  /** 事务 TransactionEntityManager. */
  private TransactionEntityManager tem;
  // 是否当前拦截器开启事务.
  boolean beginTransaction = false;
  /** JPA事务. */
  private EntityTransaction transaction;
  /** 事务涉及到的数据库资源连接，唯一资源名. */
  private Map<String, DataSourceConnection> connMap = new ConcurrentHashMap<>();
  /** jakarta.transaction.Status. */
  private int status = Status.STATUS_ACTIVE;
  // 暂停前状态
  private int prestatus = -1;

  /** 事务. */
  public DataSourceTransaction(TransactionEntityManager tem) {
    super();
    this.xid = UUIDUtil.uuid32(UUID.randomUUID());
    this.tem = tem;
  }

  /** 创建后，再加载开始. */
  public void begin() {
    this.transaction = tem.getEntityManager().getTransaction();
    // 开启事务.
    if (!transaction.isActive()) {
      this.transaction.begin();
      this.beginTransaction = true;
    }
  }

  @Override
  public void commit() throws RollbackException, HeuristicMixedException,
      HeuristicRollbackException, SecurityException, IllegalStateException, SystemException {
    if (status == Status.STATUS_MARKED_ROLLBACK) {
      throw new RuntimeException("标记为回滚，不能commit.");
    }
    try {
      status = Status.STATUS_COMMITTING;
      // 提交事务.
      if (beginTransaction) {
        transaction.commit();
      }
      for (DataSourceConnection conn : connMap.values()) {
        conn.getTarget().commit();
      }
      status = Status.STATUS_COMMITTED;
    } catch (SQLException | jakarta.persistence.RollbackException ex) {
      status = Status.STATUS_MARKED_ROLLBACK;
      throw new RuntimeException(ex);
    }
  }

  @Override
  public int getStatus() throws SystemException {
    return status;
  }

  @Override
  public void rollback() throws IllegalStateException, SystemException {
    try {
      status = Status.STATUS_ROLLING_BACK;
      // 回滚.
      if (beginTransaction) {
        transaction.rollback();
      }
      for (DataSourceConnection conn : connMap.values()) {
        conn.getTarget().rollback();
      }
      status = Status.STATUS_ROLLEDBACK;
    } catch (SQLException ex) {
      throw new RuntimeException(ex);
    }
  }

  @Override
  public void setRollbackOnly() throws IllegalStateException, SystemException {
    status = Status.STATUS_MARKED_ROLLBACK;
  }

  /** 暂停事务. */
  public void suspend() {
    if (this.status == STATUS_SUSPEND) {
      throw new RuntimeException("当前事务已处于暂停状态.");
    } else {
      this.prestatus = this.status;
      this.status = STATUS_SUSPEND;
    }
  }

  /** 恢复事务. */
  public void resume() {
    if (this.status != STATUS_SUSPEND) {
      throw new RuntimeException("当前事务未处于暂停状态,不能恢复事务.");
    } else {
      this.status = this.prestatus;
      this.prestatus = -1;
    }
  }

  @Override
  public boolean delistResource(XAResource xaRes, int flag)
      throws IllegalStateException, SystemException {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public boolean enlistResource(XAResource xaRes)
      throws RollbackException, IllegalStateException, SystemException {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public void registerSynchronization(Synchronization sync)
      throws RollbackException, IllegalStateException, SystemException {
    // TODO Auto-generated method stub
  }

  /** 事务关闭清理连接. */
  public void close() {
    if (beginTransaction) {
      // Clear Fist level Cache.
      this.tem.getEntityManager().clear();
    }
    for (DataSourceConnection conn : connMap.values()) {
      try {
        conn.getTarget().close();
      } catch (Exception ex) {
        LogFactory.getLog(getClass()).error("", ex);
      }
    }
  }

  public String getXid() {
    return xid;
  }

  public void setXid(String xid) {
    this.xid = xid;
  }

  public Map<String, DataSourceConnection> getConnMap() {
    return connMap;
  }

  public void setConnMap(Map<String, DataSourceConnection> connMap) {
    this.connMap = connMap;
  }

  public TransactionEntityManager getTem() {
    return tem;
  }

  @Override
  public int hashCode() {
    return Objects.hash(xid);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    DataSourceTransaction other = (DataSourceTransaction) obj;
    return Objects.equals(xid, other.xid);
  }


}
