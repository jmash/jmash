package com.gitee.jmash.core.jta;

import jakarta.transaction.HeuristicMixedException;
import jakarta.transaction.HeuristicRollbackException;
import jakarta.transaction.InvalidTransactionException;
import jakarta.transaction.NotSupportedException;
import jakarta.transaction.RollbackException;
import jakarta.transaction.SystemException;
import jakarta.transaction.Transaction;
import jakarta.transaction.TransactionManager;

/**
 * 数据源事务管理，解决 JdbcTemplate、JPA、MyBatis 本地化事务问题（参考Spring思路，实现JTA）.
 *
 * @author cgd
 */
public class DataSourceTransactionManager implements TransactionManager {

  @Override
  public void begin() throws NotSupportedException, SystemException {
    DataSourceTransaction dst = DataSourceTransactionStruct.createTransaction();
    dst.begin();
  }

  @Override
  public void commit() throws RollbackException, HeuristicMixedException,
      HeuristicRollbackException, SecurityException, IllegalStateException, SystemException {
    DataSourceTransaction transaction = DataSourceTransactionStruct.getTransaction();
    if (transaction == null) {
      throw new RuntimeException("事务不能未空!");
    }
    transaction.commit();
    DataSourceTransactionStruct.clearTransaction();
  }

  @Override
  public int getStatus() throws SystemException {
    DataSourceTransaction transaction = DataSourceTransactionStruct.getTransaction();
    if (transaction == null) {
      throw new RuntimeException("事务不能未空!");
    }
    return transaction.getStatus();
  }

  @Override
  public Transaction getTransaction() throws SystemException {
    return DataSourceTransactionStruct.getTransaction();
  }

  /** 暂停事务. */
  @Override
  public Transaction suspend() throws SystemException {
    DataSourceTransaction transaction = DataSourceTransactionStruct.getTransaction();
    if (transaction == null) {
      throw new RuntimeException("事务suspend不能为空!");
    }
    transaction.suspend();
    DataSourceTransactionStack.push(transaction);
    DataSourceTransactionStruct.setTransaction(null);
    return transaction;
  }

  /** 唤醒事务. */
  @Override
  public void resume(Transaction tobj)
      throws InvalidTransactionException, IllegalStateException, SystemException {
    if (tobj == null) {
      throw new RuntimeException("事务resume不能为空!");
    }
    if (DataSourceTransactionStack.empty()) {
      throw new RuntimeException("resume事务不存在!");
    }
    // 事务检查是否一致。
    DataSourceTransaction transcation = DataSourceTransactionStack.peek();
    if (!tobj.equals(transcation)) {
      throw new RuntimeException("resume事务与当前事务不一致!");
    }
    // 唤醒事务
    transcation.resume();
    // 设置当前事务为该事务.
    transcation = DataSourceTransactionStack.pop();
    DataSourceTransactionStruct.setTransaction(transcation);
  }

  @Override
  public void rollback() throws IllegalStateException, SecurityException, SystemException {
    try {
      DataSourceTransaction transaction = DataSourceTransactionStruct.getTransaction();
      if (transaction == null) {
        throw new RuntimeException("事务不能为空!");
      }
      transaction.rollback();
    } finally {
      DataSourceTransactionStruct.clearTransaction();
    }
  }

  @Override
  public void setRollbackOnly() throws IllegalStateException, SystemException {
    DataSourceTransaction transaction = DataSourceTransactionStruct.getTransaction();
    if (transaction == null) {
      throw new RuntimeException("事务不能为空!");
    }
    transaction.setRollbackOnly();
  }

  @Override
  public void setTransactionTimeout(int seconds) throws SystemException {
    // TODO Auto-generated method stub
  }



  public void close() {

  }


}
