package com.gitee.jmash.core.jta;

import java.util.Stack;

/** 线程事务栈. */
public class DataSourceTransactionStack {
  
  // 线程事务栈.
  private static final ThreadLocal<Stack<DataSourceTransaction>> transStack = new ThreadLocal<>();

  /** 获取事务栈. */
  public static synchronized Stack<DataSourceTransaction> getStack() {
    if (transStack.get() == null) {
      transStack.set(new Stack<DataSourceTransaction>());
    }
    return transStack.get();
  }

  /** 判断栈是否为空. */
  public static synchronized boolean empty() {
    return getStack().empty();
  }

  /** 获取栈顶元素. */
  public static synchronized DataSourceTransaction peek() {
    return getStack().peek();
  }

  /** 移除并返回栈顶元素。如果栈为空，将抛出 EmptyStackException. */
  public static synchronized DataSourceTransaction pop() {
    return getStack().pop();
  }

  /** 将指定元素添加到栈顶. */
  public static synchronized DataSourceTransaction push(DataSourceTransaction target) {
    return getStack().push(target);
  }

}
