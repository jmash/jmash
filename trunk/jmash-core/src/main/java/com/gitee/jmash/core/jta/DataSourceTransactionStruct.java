package com.gitee.jmash.core.jta;

import com.gitee.jmash.core.jta.interceptor.TransactionEntityManager;
import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.core.transaction.JakartaTransaction;

/** 当前事务线程. */
public class DataSourceTransactionStruct {

  // 线程当前事务.
  private static final ThreadLocal<DataSourceTransaction> curTran = new ThreadLocal<>();

  // 当前EntityManager.
  private static final ThreadLocal<TransactionEntityManager> curTem = new ThreadLocal<>();


  /** 获取当前事务. */
  public static synchronized DataSourceTransaction getTransaction() {
    return curTran.get();
  }

  /** 创建新事务. */
  public static synchronized DataSourceTransaction createTransaction() {
    if (curTran.get() != null) {
      throw new RuntimeException("当前存在事务，不能创建事务!");
    }
    DataSourceTransaction transaction = new DataSourceTransaction(curTem.get());
    curTran.set(transaction);
    return transaction;
  }

  /** 清空当前事务. */
  public static synchronized DataSourceTransaction clearTransaction() {
    DataSourceTransaction transaction = curTran.get();
    if (transaction != null) {
      transaction.close();
    }
    curTem.set(null);
    curTran.set(null);
    return transaction;
  }

  /** 设置. */
  public static synchronized void setTransaction(DataSourceTransaction transaction) {
    curTran.set(transaction);
  }

  /** 设置当前线程EntityManager. */
  public static synchronized TransactionEntityManager curEntityManager(JakartaTransaction target) {
    TransactionEntityManager tem = new TransactionEntityManager();
    tem.setEntityManager(target.getEntityManager());
    if (target instanceof TenantService) {
      tem.setTenant(((TenantService) target).getTenant());
    }
    curTem.set(tem);
    return tem;
  }

  /** 加入当前事务. */
  public static synchronized void joinTransaction(JakartaTransaction target) {
    if (curTran.get() == null) {
      throw new RuntimeException("当前事务未空，不能加入当前事务.");
    }
    TransactionEntityManager tem = curTran.get().getTem();
    target.setEntityManager(tem.getEntityManager());
    if (target instanceof TenantService) {
      ((TenantService) target).setTenantOnly(tem.getTenant());
    }
  }


}
