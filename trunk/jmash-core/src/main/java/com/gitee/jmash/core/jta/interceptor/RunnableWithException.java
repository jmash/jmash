package com.gitee.jmash.core.jta.interceptor;

/**
 * Functional interface for 'run' method that throws a checked exception.
 */
@FunctionalInterface
public interface RunnableWithException {
  void run() throws Exception;
}
