package com.gitee.jmash.core.jta.interceptor;

import jakarta.persistence.EntityManager;

/**
 * 事务EntityManager.
 */
public class TransactionEntityManager {

  /** JPA 实体管理. */
  private EntityManager entityManager;
  /** 租户. */
  private String tenant;

  public EntityManager getEntityManager() {
    return entityManager;
  }

  public void setEntityManager(EntityManager entityManager) {
    this.entityManager = entityManager;
  }

  public String getTenant() {
    return tenant;
  }

  public void setTenant(String tenant) {
    this.tenant = tenant;
  }

}
