package com.gitee.jmash.core.jta.interceptor;

import jakarta.transaction.Status;
import jakarta.transaction.SystemException;
import jakarta.transaction.Transaction;
import jakarta.transaction.TransactionManager;
import jakarta.transaction.Transactional;
import org.apache.commons.logging.LogFactory;

/**
 * 事务处理.
 */
public final class TransactionHandler {

  /**
   * For cases that the transaction should be marked for rollback ie. when {@link RuntimeException}
   * is thrown or when {@link Error} is thrown or when the exception si marked in
   * {@link Transactional#rollbackOn()} then {@link Transaction#setRollbackOnly()} is invoked.
   */
  public static void handleExceptionNoThrow(Transactional transactional, Throwable t,
      Transaction tx) throws IllegalStateException, SystemException {

    for (Class<?> dontRollbackOnClass : transactional.dontRollbackOn()) {
      if (dontRollbackOnClass.isAssignableFrom(t.getClass())) {
        return;
      }
    }

    for (Class<?> rollbackOnClass : transactional.rollbackOn()) {
      if (rollbackOnClass.isAssignableFrom(t.getClass())) {
        tx.setRollbackOnly();
        return;
      }
    }

    // RuntimeException and Error are un-checked exceptions and rollback is expected
    if (t instanceof RuntimeException || t instanceof Error) {
      tx.setRollbackOnly();
      return;
    }
  }

  /**
   * <p>
   * It finished the transaction.
   * </p>
   * <p>
   * Call {@link TransactionManager#rollback()} when the transaction si marked for
   * {@link Status#STATUS_MARKED_ROLLBACK}. otherwise the transaction is committed. Either way there
   * is executed the {@link Runnable} 'afterEndTransaction' after the transaction is finished.
   * </p>
   */
  public static void endTransaction(TransactionManager tm, Transaction tx,
      RunnableWithException afterEndTransaction) throws Exception {
    try {
      if (tx != tm.getTransaction()) {
        throw new RuntimeException("错误的事务线程.");
      }
      if (tx.getStatus() == Status.STATUS_MARKED_ROLLBACK) {
        tm.rollback();
      } else {
        tm.commit();
      }
    } catch (Exception ex) {
      // commit exception roll back.
      try {
        tm.rollback();
      } catch (Exception e) {
        LogFactory.getLog(TransactionHandler.class).error(e.getMessage());
      }
      // 抛出事务提交异常.
      throw ex;
    } finally {
      afterEndTransaction.run();
    }
  }
}
