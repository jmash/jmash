
/*
 * Copyright The Narayana Authors SPDX-License-Identifier: Apache-2.0
 */

package com.gitee.jmash.core.jta.interceptor;

import com.gitee.jmash.core.jta.DataSourceTransactionStruct;
import com.gitee.jmash.core.transaction.JakartaTransaction;
import jakarta.inject.Inject;
import jakarta.interceptor.InvocationContext;
import jakarta.transaction.Transaction;
import jakarta.transaction.TransactionManager;
import jakarta.transaction.Transactional;
import java.io.Serializable;
import java.lang.reflect.Method;
import org.apache.commons.lang3.StringUtils;

/**
 * 事务.
 *
 * @author paul.robinson@redhat.com 02/05/2013
 *
 * @author <a href="https://about.me/lairdnelson" target="_parent">Laird Nelson</a>
 */
public abstract class TransactionalInterceptorBase implements Serializable {
  private static final long serialVersionUID = 1L;

  @Inject
  private TransactionManager transactionManager;
  /** UserTransaction 是否可获得，暂不支持. */
  private final boolean userTransactionAvailable;

  protected TransactionalInterceptorBase(boolean userTransactionAvailable) {
    this.userTransactionAvailable = userTransactionAvailable;
  }

  /** 拦截器. */
  public Object intercept(InvocationContext ic) throws Exception {
    // 非事务方法处理
    if (!(ic.getTarget() instanceof JakartaTransaction)) {
      return ic.proceed();
    }
    // 跳过一些方法.
    if (isJumpTransacional(ic.getMethod())) {
      return ic.proceed();
    }
    // 事务注解.
    Transactional transacional = getTransactional(ic);
    if (null == transacional) {
      return ic.proceed();
    }
    // 事务注入.
    DataSourceTransactionStruct.curEntityManager((JakartaTransaction) ic.getTarget());
    final Transaction tx = transactionManager.getTransaction();
    return doIntercept(transactionManager, tx, ic);

  }

  /** 获取注解. */
  private Transactional getTransactional(InvocationContext ic) {
    Transactional transactional = ic.getMethod().getAnnotation(Transactional.class);
    if (transactional != null) {
      return transactional;
    }

    Class<?> targetClass = ic.getTarget().getClass();
    transactional = targetClass.getAnnotation(Transactional.class);
    return transactional;
  }

  /**
   * 该方法是否开启事务?.
   */
  public boolean isJumpTransacional(Method method) {
    // AutoCloseable
    if (StringUtils.equals("close", method.getName())) {
      return true;
    }
    // TenantService
    if (StringUtils.equals("setTenant", method.getName())) {
      return true;
    }
    if (StringUtils.equals("getTenant", method.getName())) {
      return true;
    }
    // JakartaTransaction
    if (StringUtils.equals("getEntityManager", method.getName())) {
      return true;
    }
    if (StringUtils.equals("setEntityManager", method.getName())) {
      return true;
    }
    return false;
  }


  protected abstract Object doIntercept(TransactionManager tm, Transaction tx, InvocationContext ic)
      throws Exception;



  protected Object invokeInOurTx(InvocationContext ic, TransactionManager tm) throws Exception {
    return invokeInOurTx(ic, tm, () -> {
    });
  }

  protected Object invokeInOurTx(InvocationContext ic, TransactionManager tm,
      RunnableWithException afterEndTransaction) throws Exception {
    tm.begin();
    Transaction tx = tm.getTransaction();
    Object ret = null;
    try {
      ret = ic.proceed();
    } catch (Throwable e) {
      handleException(ic, e, tx);
    } finally {
      TransactionHandler.endTransaction(tm, tx, afterEndTransaction);
    }
    return ret;
  }

  protected Object invokeInCallerTx(InvocationContext ic, Transaction tx) throws Exception {
    try {
      return ic.proceed();
    } catch (Throwable t) {
      handleException(ic, t, tx);
    }
    throw new RuntimeException("UNREACHABLE");
  }

  protected Object invokeInNoTx(InvocationContext ic) throws Exception {
    return ic.proceed();
  }

  /**
   * The handleException considers the transaction to be marked for rollback only in case the thrown
   * exception comes with this effect (see
   * {@link TransactionHandler#handleExceptionNoThrow(Transactional, Throwable, Transaction)} and
   * consider the {@link Transactional#dontRollbackOn()}. If so then this method rethrows the
   * {@link Throwable} passed as the parameter 't'.
   */
  protected void handleException(InvocationContext ic, Throwable t, Transaction tx)
      throws Exception {
    TransactionHandler.handleExceptionNoThrow(getTransactional(ic), t, tx);
    if (t instanceof Exception) {
      throw (Exception) t;
    }
    throw new RuntimeException(t);
  }

  public boolean isUserTransactionAvailable() {
    return userTransactionAvailable;
  }



}
