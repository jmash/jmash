package com.gitee.jmash.core.jta.interceptor;

import com.gitee.jmash.core.jta.DataSourceTransactionStruct;
import com.gitee.jmash.core.transaction.JakartaTransaction;
import jakarta.annotation.Priority;
import jakarta.interceptor.AroundInvoke;
import jakarta.interceptor.Interceptor;
import jakarta.interceptor.InvocationContext;
import jakarta.transaction.Transaction;
import jakarta.transaction.TransactionManager;
import jakarta.transaction.TransactionRequiredException;
import jakarta.transaction.Transactional;
import jakarta.transaction.TransactionalException;

/**
 *  如果在事务上下文之外被调用，必须抛出一个嵌套了TransactionRequiredException的TransactionalException.
 *  如果在事务上下文内被调用，托管bean的方法执行将在那个上下文中继续.
 *
 * @author paul.robinson@redhat.com 25/05/2013
 */
@Interceptor
@Transactional(Transactional.TxType.MANDATORY)
@Priority(Interceptor.Priority.PLATFORM_BEFORE + 200)
public class TransactionalInterceptorMandatory extends TransactionalInterceptorBase {

  private static final long serialVersionUID = 1L;

  public TransactionalInterceptorMandatory() {
    super(false);
  }

  @AroundInvoke
  public Object intercept(InvocationContext ic) throws Exception {
    return super.intercept(ic);
  }

  @Override
  protected Object doIntercept(TransactionManager tm, Transaction tx, InvocationContext ic)
      throws Exception {
    if (tx == null) {
      throw new TransactionalException("事务不能未null.", new TransactionRequiredException());
    }
    // 加入已有事务.
    if (ic.getTarget() instanceof JakartaTransaction) {
      DataSourceTransactionStruct.joinTransaction((JakartaTransaction) ic.getTarget());
    }
    return invokeInCallerTx(ic, tx);
  }
}
