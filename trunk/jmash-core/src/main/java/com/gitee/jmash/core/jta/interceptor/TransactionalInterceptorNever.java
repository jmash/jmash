package com.gitee.jmash.core.jta.interceptor;

import jakarta.annotation.Priority;
import jakarta.interceptor.AroundInvoke;
import jakarta.interceptor.Interceptor;
import jakarta.interceptor.InvocationContext;
import jakarta.transaction.InvalidTransactionException;
import jakarta.transaction.Transaction;
import jakarta.transaction.TransactionManager;
import jakarta.transaction.Transactional;
import jakarta.transaction.TransactionalException;

/**
 * 如果在事务上下文之外被调用，托管bean的方法执行必须在事务上下文之外继续.
 * 如果在事务上下文内被调用，必须抛出一个嵌套了InvalidTransactionException的TransactionalException.
 *
 * @author paul.robinson@redhat.com 25/05/2013
 */
@Interceptor
@Transactional(Transactional.TxType.NEVER)
@Priority(Interceptor.Priority.PLATFORM_BEFORE + 200)
public class TransactionalInterceptorNever extends TransactionalInterceptorBase {
  private static final long serialVersionUID = 1L;

  public TransactionalInterceptorNever() {
    super(true);
  }

  @AroundInvoke
  public Object intercept(InvocationContext ic) throws Exception {
    return super.intercept(ic);
  }

  @Override
  protected Object doIntercept(TransactionManager tm, Transaction tx, InvocationContext ic)
      throws Exception {
    if (tx != null) {
      throw new TransactionalException("事务必须为null.", new InvalidTransactionException());
    }
    return invokeInNoTx(ic);
  }
}
