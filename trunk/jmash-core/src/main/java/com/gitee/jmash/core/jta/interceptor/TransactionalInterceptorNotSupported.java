package com.gitee.jmash.core.jta.interceptor;

import jakarta.annotation.Priority;
import jakarta.interceptor.AroundInvoke;
import jakarta.interceptor.Interceptor;
import jakarta.interceptor.InvocationContext;
import jakarta.transaction.Transaction;
import jakarta.transaction.TransactionManager;
import jakarta.transaction.Transactional;

/**
 * 如果在事务上下文之外被调用，托管bean的方法执行必须在事务上下文之外继续.
 * 如果在事务上下文内被调用，当前的事务上下文必须被挂起，托管bean的方法执行必须在事务上下文之外继续,且先前挂起的事务必须由挂起它的拦截器在其方法执行完成后恢复. 
 *
 * @author paul.robinson@redhat.com 25/05/2013
 */
@Interceptor
@Transactional(Transactional.TxType.NOT_SUPPORTED)
@Priority(Interceptor.Priority.PLATFORM_BEFORE + 200)
public class TransactionalInterceptorNotSupported extends TransactionalInterceptorBase {
  
  private static final long serialVersionUID = 1L;

  public TransactionalInterceptorNotSupported() {
    super(true);
  }

  @AroundInvoke
  public Object intercept(InvocationContext ic) throws Exception {
    return super.intercept(ic);
  }

  @Override
  protected Object doIntercept(TransactionManager tm, Transaction tx, InvocationContext ic)
      throws Exception {
    if (tx != null) {
      tm.suspend();
      try {
        return invokeInNoTx(ic);
      } finally {
        tm.resume(tx);
      }
    } else {
      return invokeInNoTx(ic);
    }
  }
}
