package com.gitee.jmash.core.jta.interceptor;

import com.gitee.jmash.core.jta.DataSourceTransactionStruct;
import com.gitee.jmash.core.transaction.JakartaTransaction;
import jakarta.annotation.Priority;
import jakarta.interceptor.AroundInvoke;
import jakarta.interceptor.Interceptor;
import jakarta.interceptor.InvocationContext;
import jakarta.transaction.Transaction;
import jakarta.transaction.TransactionManager;
import jakarta.transaction.Transactional;

/**
 * 如果在事务上下文之外被调用，拦截器必须开始一个新的Jakarta Transactions事务， 然后托管bean的方法执行必须在这个事务上下文中继续，且事务必须由拦截器完成.
 * 如果在事务上下文内被调用，托管bean的方法执行必须在这个事务上下文中继续.
 *
 * @author paul.robinson@redhat.com 25/05/2013
 */
@Interceptor
@Transactional(Transactional.TxType.REQUIRED)
@Priority(Interceptor.Priority.PLATFORM_BEFORE + 200)
public class TransactionalInterceptorRequired extends TransactionalInterceptorBase {

  private static final long serialVersionUID = 1L;

  public TransactionalInterceptorRequired() {
    super(false);
  }

  @AroundInvoke
  public Object intercept(InvocationContext ic) throws Exception {
    return super.intercept(ic);
  }

  @Override
  protected Object doIntercept(TransactionManager tm, Transaction tx, InvocationContext ic)
      throws Exception {
    if (tx == null) {
      return invokeInOurTx(ic, tm);
    } else {
      //加入已有事务.
      if (ic.getTarget() instanceof JakartaTransaction) {
        DataSourceTransactionStruct.joinTransaction((JakartaTransaction) ic.getTarget());
      }
      return invokeInCallerTx(ic, tx);
    }
  }
}
