package com.gitee.jmash.core.jta.interceptor;

import jakarta.annotation.Priority;
import jakarta.interceptor.AroundInvoke;
import jakarta.interceptor.Interceptor;
import jakarta.interceptor.InvocationContext;
import jakarta.transaction.Transaction;
import jakarta.transaction.TransactionManager;
import jakarta.transaction.Transactional;

/**
 * 如果在事务上下文之外被调用，拦截器必须开始一个新的Jakarta Transactions事务，然后托管bean的方法执行必须在这个事务上下文中继续，且事务必须由拦截器完成.
 * 如果在事务上下文内被调用，当前的事务上下文必须被挂起，一个新的Jakarta Transactions事务将开始，托管bean的方法执行必须在这个事务上下文中继续，
 * 事务必须完成，且先前挂起的事务必须由挂起它的拦截器恢复.
 *
 * @author paul.robinson@redhat.com 25/05/2013
 */
@Interceptor
@Transactional(Transactional.TxType.REQUIRES_NEW)
@Priority(Interceptor.Priority.PLATFORM_BEFORE + 200)
public class TransactionalInterceptorRequiresNew extends TransactionalInterceptorBase {
  private static final long serialVersionUID = 1L;

  public TransactionalInterceptorRequiresNew() {
    super(false);
  }

  @AroundInvoke
  public Object intercept(InvocationContext ic) throws Exception {
    return super.intercept(ic);
  }

  @Override
  protected Object doIntercept(TransactionManager tm, Transaction tx, InvocationContext ic)
      throws Exception {
    if (tx != null) {
      tm.suspend();
      return invokeInOurTx(ic, tm, () -> tm.resume(tx));
    } else {
      return invokeInOurTx(ic, tm);
    }
  }
}
