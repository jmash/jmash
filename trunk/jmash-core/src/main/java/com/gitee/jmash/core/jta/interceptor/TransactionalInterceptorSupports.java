package com.gitee.jmash.core.jta.interceptor;

import com.gitee.jmash.core.jta.DataSourceTransactionStruct;
import com.gitee.jmash.core.transaction.JakartaTransaction;
import jakarta.annotation.Priority;
import jakarta.interceptor.AroundInvoke;
import jakarta.interceptor.Interceptor;
import jakarta.interceptor.InvocationContext;
import jakarta.transaction.Transaction;
import jakarta.transaction.TransactionManager;
import jakarta.transaction.Transactional;

/**
 * 如果在事务上下文之外被调用，托管bean的方法执行必须在事务上下文之外继续.
 * 如果在事务上下文内被调用，托管bean的方法执行必须在这个事务上下文中继续.
 *
 * @author paul.robinson@redhat.com 25/05/2013
 */
@Interceptor
@Transactional(Transactional.TxType.SUPPORTS)
@Priority(Interceptor.Priority.PLATFORM_BEFORE + 200)
public class TransactionalInterceptorSupports extends TransactionalInterceptorBase {

  private static final long serialVersionUID = 1L;

  public TransactionalInterceptorSupports() {
    super(false);
  }

  @AroundInvoke
  public Object intercept(InvocationContext ic) throws Exception {
    return super.intercept(ic);
  }

  @Override
  protected Object doIntercept(TransactionManager tm, Transaction tx, InvocationContext ic)
      throws Exception {
    if (tx == null) {
      return invokeInNoTx(ic);
    } else {
      // 加入已有事务.
      if (ic.getTarget() instanceof JakartaTransaction) {
        DataSourceTransactionStruct.joinTransaction((JakartaTransaction) ic.getTarget());
      }
      return invokeInCallerTx(ic, tx);
    }
  }
}
