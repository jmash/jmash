package com.gitee.jmash.core.lucene;

import org.apache.lucene.document.Document;

/**
 * lucene 文档.
 *
 * @author cgd email:cgd@crenjoy.com
 * @version V1.0 2011-5-17
 */
public class LuceneDoc {

  private float score;

  private Document doc;

  public LuceneDoc() {
    super();
  }

  /** 构造. */
  public LuceneDoc(float score, Document doc) {
    super();
    this.score = score;
    this.doc = doc;
  }

  public float getScore() {
    return score;
  }

  public void setScore(float score) {
    this.score = score;
  }

  public Document getDoc() {
    return doc;
  }

  public void setDoc(Document doc) {
    this.doc = doc;
  }

}
