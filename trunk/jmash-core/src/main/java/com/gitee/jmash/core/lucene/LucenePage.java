package com.gitee.jmash.core.lucene;

import java.util.List;

/**
 * 翻页实体(如需支持webservices还需要改进).
 *
 * @author cgd email:cgd@crenjoy.com
 * @version V2.0 2009-4-4
 */
public abstract class LucenePage<E> implements java.io.Serializable {

  private static final long serialVersionUID = 1L;

  /** 当前页内容. */
  protected List<E> results;
  /** 当前页码. */
  protected int curPage;
  /** 页尺寸. */
  protected int pageSize;
  /** 总结果数. */
  protected long totalSize;
  /** 花费时间(毫秒). */
  protected long costTime;

  public LucenePage() {
    super();
  }

  /**
   * 实际构造函数.
   *
   * @param results 查询结果list
   * @param curPage 当前页
   * @param pageSize 页尺寸
   * @param totalSize 结果总数
   * @param costTime 花费时间.
   */
  public LucenePage(List<E> results, int curPage, int pageSize, long totalSize, long costTime) {
    super();
    this.results = results;
    this.curPage = (curPage < 1 ? 1 : curPage);
    this.pageSize = (pageSize < 1 ? 1 : pageSize);
    this.totalSize = totalSize;
    this.costTime = costTime;
  }

  /**
   * 花费时间.
   */
  public long getCostTime() {
    return costTime;
  }

  /**
   * 获取当前页码.
   */
  public int getCurPage() {
    return curPage;
  }

  /**
   * 获取当前页尺寸.
   */
  public int getPageSize() {
    return pageSize;
  }

  /**
   * 当前结果.
   */
  public List<E> getResults() {
    return results;
  }

  /**
   * 返回全部结果数.
   */
  public long getTotalSize() {
    return totalSize;
  }



  /**
   * 下一页码.
   */
  public int getNextPageNumber() {
    return curPage < getPageCount() ? curPage + 1 : Long.valueOf(getPageCount()).intValue();
  }

  /**
   * 前一页码.
   */
  public int getPrePageNumber() {
    return curPage > 1 ? curPage - 1 : 1;
  }

  /**
   * 是否第一页.
   */
  public boolean isFirstPage() {
    return curPage == 1;
  }

  /**
   * 是否最后一页.
   */
  public boolean isLastPage() {
    long pageCount = getPageCount();
    return pageCount == 0 || curPage == pageCount;
  }

  /**
   * 有上一页码.
   */
  public boolean hasPrePage() {
    return curPage > 1;
  }

  /**
   * 是否有下一页.
   */
  public boolean hasNextPage() {
    return curPage < getPageCount();
  }

  /**
   * 返回总页数.
   */
  public long getPageCount() {
    return (totalSize / pageSize) + ((totalSize % pageSize) == 0 ? 0 : 1);
  }

  /**
   * 开始的结果索引.
   */
  public int getStartRecord() {
    return (this.curPage - 1) * this.pageSize + 1;
  }

  /**
   * 结束索引.
   */
  public int getEndRecord() {
    return this.getStartRecord() - 1 + this.getResults().size();
  }
}
