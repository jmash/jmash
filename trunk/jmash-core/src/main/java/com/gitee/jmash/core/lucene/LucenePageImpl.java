package com.gitee.jmash.core.lucene;

import java.util.List;

/**
 * lucene 翻页查询.
 *
 * @author cgd email:cgd@crenjoy.com
 * @version V1.0 2011-5-17
 * @param <E> 实体.
 */
public class LucenePageImpl<E> extends LucenePage<E> {

  private static final long serialVersionUID = 1L;

  /**
   * 查询结果list.
   *
   * @param results 查询结果list
   */
  public void setResults(List<E> results) {
    this.results = results;
  }

  /**
   * 当前页.
   *
   * @param curPage 当前页
   */
  public void setCurPage(int curPage) {
    this.curPage = curPage;
  }

  /**
   * 页尺寸.
   *
   * @param pageSize 页尺寸
   */
  public void setPageSize(int pageSize) {
    this.pageSize = pageSize;
  }

  /**
   * 结果总数.
   *
   * @param totalSize 结果总数
   */
  public void setTotalSize(int totalSize) {
    this.totalSize = totalSize;
  }

  /**
   * 保证可以序列化.
   */
  public LucenePageImpl() {
    super(null, 0, 0, 0, 0);
  }

  /**
   * 构造函数.
   */
  public LucenePageImpl(List<E> results, int curPage, int pageSize, long totalSize, long costTime) {
    super(results, curPage, pageSize, totalSize, costTime);
  }

}
