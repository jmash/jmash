package com.gitee.jmash.core.lucene;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexableField;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.FSDirectory;
import org.eclipse.microprofile.config.ConfigProvider;

/**
 * Lucene 索引工具类.
 */
public class LuceneUtil {

  private static final Log log = LogFactory.getLog(LuceneUtil.class);

  protected Path indexPath;

  protected Analyzer analyzer;

  private static final Object obj = new Object();

  public LuceneUtil() {
    this(Paths.get(ConfigProvider.getConfig().getValue("jmash.index.path", String.class)),
        new StandardAnalyzer());
  }

  /**
   * 初始化.
   *
   * @param indexPath 路径
   * @param analyzer 索引
   */
  public LuceneUtil(Path indexPath, Analyzer analyzer) {
    synchronized (obj) {
      this.indexPath = indexPath;
      this.analyzer = analyzer;
      try {
        IndexWriterConfig config = new IndexWriterConfig(analyzer);
        IndexWriter indexWriter = new IndexWriter(FSDirectory.open(indexPath), config);
        indexWriter.close();
      } catch (Exception ex) {
        log.error("建立索引文件失败，path:" + indexPath.toString(), ex);
      }
    }
  }

  /**
   * 创建索引文档.
   */
  public synchronized void createIndex(Document... docs) {
    if (docs == null || docs.length == 0) {
      return;
    }
    IndexWriter indexWriter = null;
    try {
      IndexWriterConfig config = new IndexWriterConfig(analyzer);
      indexWriter = new IndexWriter(FSDirectory.open(indexPath), config);
      for (Document doc : docs) {
        if (doc != null) {
          indexWriter.addDocument(doc);
        }
      }
      indexWriter.close();
    } catch (Exception ex) {
      log.error("建立索引文件失败，path:" + indexPath, ex);
    } finally {
      try {
        if (indexWriter != null) {
          indexWriter.close();
        }
      } catch (IOException ex) {
        log.error("", ex);
      }
    }
  }

  /**
   * 修改文档.
   *
   * @param fieldName ID字段名
   * @param docs 文档
   */
  public synchronized void modifyIndex(String fieldName, Document... docs) {
    if (docs == null || docs.length == 0) {
      return;
    }
    // 删除
    deleteIndex(fieldName, docs);
    // 增加
    createIndex(docs);
  }

  /**
   * 删除文档.
   *
   * @param fieldName 字段名
   * @param docs 文档
   */
  public synchronized void deleteIndex(String fieldName, Document... docs) {
    if (docs == null || docs.length == 0) {
      return;
    }
    String[] fids = new String[docs.length];
    for (int i = 0; i < docs.length; i++) {
      IndexableField field = docs[i].getField(fieldName);
      fids[i] = field.stringValue();
    }
    // 删除
    deleteIndex(fieldName, fids);
  }

  /**
   * 删除文档.
   *
   * @param fieldName ID字段名
   * @param fids ID.
   */
  public synchronized void deleteIndex(String fieldName, String... fids) {
    if (fids == null || fids.length == 0) {
      return;
    }
    try {
      IndexWriterConfig config = new IndexWriterConfig(analyzer);
      IndexWriter indexWriter = new IndexWriter(FSDirectory.open(indexPath), config);
      for (String fid : fids) {
        Term term = new Term(fieldName, fid);
        indexWriter.deleteDocuments(term);
      }
      indexWriter.commit();
      indexWriter.close();
    } catch (IOException ex) {
      log.error("lucene delete article IndexPath error:" + indexPath, ex);
    }
  }

  /** 全文检索. */
  protected LucenePage<LuceneDoc> searchDoc(int page, int pageSize, Query query)
      throws IOException {
    int hm = page * pageSize;
    Date start = new Date();
    IndexReader reader = DirectoryReader.open(FSDirectory.open(indexPath));
    IndexSearcher searcher = new IndexSearcher(reader);
    TopDocs hits = searcher.search(query, hm);
    Date end = new Date();
    // 调整页码
    int x = (page - 1) * pageSize;
    long y = x + pageSize;
    if (y > hits.totalHits.value) {
      y = hits.totalHits.value;
    }

    log.info("x:" + x + "|" + "y:" + y);

    // 封装文档
    List<LuceneDoc> docs = new ArrayList<LuceneDoc>();
    for (int i = x; i < y; i++) {
      ScoreDoc sdoc = hits.scoreDocs[i];
      Document doc = searcher.doc(sdoc.doc);
      docs.add(new LuceneDoc(sdoc.score, doc));
    }

    reader.close();
    long costTime = end.getTime() - start.getTime();
    return new LucenePageImpl<LuceneDoc>(docs, page, pageSize, hits.totalHits.value,
        costTime);
  }

  public String getFieldValue(Document doc, String name) {
    String value = doc.get(name);
    return (value == null) ? "" : value;
  }

}
