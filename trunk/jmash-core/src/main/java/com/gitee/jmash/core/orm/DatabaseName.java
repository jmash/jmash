package com.gitee.jmash.core.orm;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 数据库类型.
 *
 * @author CGD
 *
 */
public enum DatabaseName {

  MySQL,

  Oracle,

  SQLServer,

  H2,

  HSQLDB;

  private static Log log = LogFactory.getLog(DatabaseName.class);

  /**
   * 转换数据库类型.
   *
   * @param databaseProductName 数据库产品名称
   * @return 数据名称枚举
   */
  public static DatabaseName convertDatabaseName(String databaseProductName) {
    log.trace("Database:" + databaseProductName);
    if (StringUtils.equals(databaseProductName, "MySQL")
        || StringUtils.equals(databaseProductName, "MariaDB")) {
      return DatabaseName.MySQL;
    }
    if (StringUtils.equals(databaseProductName, "Oracle")) {
      return DatabaseName.Oracle;
    }
    if (StringUtils.equals(databaseProductName, "Microsoft SQL Server")) {
      return DatabaseName.SQLServer;
    }
    if (StringUtils.equals(databaseProductName, "H2")) {
      return DatabaseName.H2;
    }
    if (StringUtils.equals(databaseProductName, "HSQL Database Engine")) {
      return DatabaseName.HSQLDB;
    }
    log.info("DatabaseName enum is not find ---" + databaseProductName);
    return null;
  }
}
