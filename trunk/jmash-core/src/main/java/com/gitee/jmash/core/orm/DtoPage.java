
package com.gitee.jmash.core.orm;

import jakarta.json.bind.annotation.JsonbProperty;
import jakarta.json.bind.annotation.JsonbTransient;
import java.util.List;

/**
 * 翻页实体.
 *
 * @author cgd email:cgd@crenjoy.com
 * @version V2.0 2009-4-4
 */
public abstract class DtoPage<E, F extends DtoTotal> implements java.io.Serializable {

  private static final long serialVersionUID = 2L;

  /** 当前页内容. */
  protected List<E> results;
  /** 当前页码. */
  protected int curPage;
  /** 页尺寸. */
  protected int pageSize;
  /** 总记录数. */
  protected int totalSize;
  /** 本页小计. */
  protected F subTotalDto;
  /** 统计信息. */
  protected F totalDto;
  
  public DtoPage() {
    super();
  }

  /**
   * 实际构造函数.
   *
   * @param results 查询结果list
   * @param curPage 当前页
   * @param pageSize 页尺寸
   * @param totalDto 页统计信息
   */
  public DtoPage(List<E> results, int curPage, int pageSize, F totalDto) {
    super();
    this.results = results;
    this.curPage = (curPage < 1 ? 1 : curPage);
    this.pageSize = (pageSize < 1 ? 1 : pageSize);
    if (null == totalDto) {
      this.totalSize = 0;
    } else {
      this.totalSize = totalDto.getTotalSize();
      this.totalDto = totalDto;
    }
  }

  /**
   * 页面函数.
   *
   * @param results 查询结果list
   * @param curPage 当前页
   * @param pageSize 页尺寸
   * @param subTotalDto 本页统计信息
   * @param totalDto 页统计信息
   */
  public DtoPage(List<E> results, int curPage, int pageSize, F subTotalDto, F totalDto) {
    super();
    this.results = results;
    this.curPage = (curPage < 1 ? 1 : curPage);
    this.pageSize = (pageSize < 1 ? 1 : pageSize);
    if (null == totalDto) {
      this.totalSize = 0;
    } else {
      this.totalSize = totalDto.getTotalSize();
      this.totalDto = totalDto;
    }
    this.subTotalDto = subTotalDto;
  }

  /**
   * 获取当前页码.
   */
  @JsonbProperty
  public int getCurPage() {
    return curPage;
  }

  /**
   * 获取当前页尺寸.
   */
  @JsonbProperty
  public int getPageSize() {
    return pageSize;
  }

  /**
   * 当前结果.
   */
  @JsonbProperty
  public List<E> getResults() {
    return results;
  }

  /**
   * 返回全部结果数.
   */
  @JsonbProperty
  public long getTotalSize() {
    return totalSize;
  }

  @JsonbProperty
  public F getSubTotalDto() {
    return subTotalDto;
  }

  /**
   * 统计信息.
   */
  @JsonbProperty
  public F getTotalDto() {
    return totalDto;
  }

  /**
   * 总页数.
   */
  @JsonbProperty
  public int getPageCount() {
    return (totalSize / pageSize) + ((totalSize % pageSize) == 0 ? 0 : 1);
  }

  /**
   * 下一页码.
   *
   * @return int型下一页码
   */
  @JsonbTransient
  public int getNextPageNumber() {
    return curPage < getPageCount() ? curPage + 1 : getPageCount();
  }

  /**
   * 前一页码.
   *
   * @return int型前一页码
   */
  @JsonbTransient
  public int getPrePageNumber() {
    return curPage > 1 ? curPage - 1 : 1;
  }

  /**
   * 是否第一页.
   */
  @JsonbTransient
  public boolean isFirstPage() {
    return curPage == 1;
  }

  /**
   * 是否最后一页.
   */
  @JsonbTransient
  public boolean isLastPage() {
    long pageCount = getPageCount();
    return pageCount == 0 || curPage == pageCount;
  }

  /**
   * 有上一页吗.
   */
  @JsonbTransient
  public boolean hasPrePage() {
    return curPage > 1;
  }

  /**
   * 是否有下一页.
   */
  @JsonbTransient
  public boolean hasNextPage() {
    return curPage < getPageCount();
  }

  /**
   * 开始的结果索引.
   *
   * @return int
   */
  @JsonbTransient
  public int getStartRecord() {
    return (this.curPage - 1) * this.pageSize + 1;
  }

  /**
   * 结束索引.
   *
   * @return int
   */
  @JsonbTransient
  public int getEndRecord() {
    return this.getStartRecord() - 1 + this.getResults().size();
  }

}
