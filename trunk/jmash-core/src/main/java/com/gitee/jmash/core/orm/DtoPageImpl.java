
package com.gitee.jmash.core.orm;

import com.crenjoy.proto.beanutils.ProtoBeanUtils;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.LogFactory;

/**
 * 实际传输的PageImpl，保证这个可以序列化.
 *
 * @author cgd email:cgd@crenjoy.com
 * @version V2.0 2009-4-4
 */
public class DtoPageImpl<E, F extends DtoTotal> extends DtoPage<E, F> {

  private static final String TOTAL_SIZE = "totalSize";
  
  private static final long serialVersionUID = 1L;

  /**
   * 保证可以序列化.
   */
  public DtoPageImpl() {
    super(null, 0, 0, null);
  }

  /**
   * 构造函数.
   *
   * @param results  查询结果列表
   * @param curPage  当前页
   * @param pageSize 页尺寸
   * @param totalDto 统计信息
   */
  @SuppressWarnings("unchecked")
  public DtoPageImpl(List<E> results, int curPage, int pageSize, F totalDto) {
    super(results, curPage, pageSize, totalDto);

    try {
      Method[] methods = totalDto.getClass().getMethods();
      subTotalDto = (F) totalDto.getClass().getDeclaredConstructor().newInstance();

      if (results.size() == 0) {
        ProtoBeanUtils.setProperty(subTotalDto, TOTAL_SIZE, results.size());
        return;
      }

      for (int i = 0; i < methods.length; i++) {
        String name = methods[i].getName();
        try {
          E obj = results.get(0);
          if (!hasGetProperty(obj.getClass(), name)) {
            continue;
          }
          if (name.startsWith("set") && name.length() > 3) {
            String field = StringUtils.uncapitalize(name.substring(3));
            Class<?> typeClass = methods[i].getParameterTypes()[0];
            if (typeClass.equals(Long.class) || typeClass.equals(Integer.class)) {
              Long total = 0L;
              for (int j = 0; j < results.size(); j++) {
                Object value = ProtoBeanUtils.getProperty(results.get(j), field);
                if (null != value) {
                  total += Long.valueOf((String) value);
                }
              }
              ProtoBeanUtils.setProperty(subTotalDto, field, total);
            } else if (typeClass.equals(BigDecimal.class)) {
              BigDecimal total = BigDecimal.ZERO;
              for (int j = 0; j < results.size(); j++) {
                Object value = ProtoBeanUtils.getProperty(results.get(j), field);
                if (null != value) {
                  total = total.add(new BigDecimal((String) value));
                }
              }
              ProtoBeanUtils.setProperty(subTotalDto, field, total);
            }
          }
        } catch (Exception ex) {
          LogFactory.getLog(DtoPageImpl.class).error("subTotalDto:" + name);
        }
      }
      // 小计记录数
      ProtoBeanUtils.setProperty(subTotalDto, TOTAL_SIZE, results.size());
    } catch (Exception ex) {
      LogFactory.getLog(DtoPageImpl.class).error("", ex);
    }

  }

  public DtoPageImpl(List<E> results, int curPage, int pageSize, F subTotalDto, F totalDto) {
    super(results, curPage, pageSize, subTotalDto, totalDto);
  }

  /**
   * 判断类是否有Name的Get属性.
   *
   * @param clazz 类
   * @param name  某属性名
   * @return 是否有
   */
  public boolean hasGetProperty(Class<?> clazz, String name) {
    if (name.startsWith("set") && name.length() > 3) {
      String method = "get" + name.substring(3);
      Method[] ms = clazz.getMethods();
      for (Method mt : ms) {
        if (mt.getName().equals(method)) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * 查询结果list.
   *
   * @param results 查询结果list
   */
  public void setResults(List<E> results) {
    this.results = results;
  }

  /**
   * 当前页.
   *
   * @param curPage 当前页
   */
  public void setCurPage(int curPage) {
    this.curPage = curPage;
  }

  /**
   * 页尺寸.
   *
   * @param pageSize 页尺寸
   */
  public void setPageSize(int pageSize) {
    this.pageSize = pageSize;
  }

  /**
   * 结果总数.
   *
   * @param totalSize 结果总数
   */
  public void setTotalSize(int totalSize) {
    this.totalSize = totalSize;
  }

  /**
   * 统计信息DTO.
   *
   * @param totalDto 统计信息
   */
  public void setTotalDto(F totalDto) {
    this.totalDto = totalDto;
  }

  public void setSubTotalDto(F subTotalDto) {
    this.subTotalDto = subTotalDto;
  }

  public void setPageCount(int pageCount) {

  }

}
