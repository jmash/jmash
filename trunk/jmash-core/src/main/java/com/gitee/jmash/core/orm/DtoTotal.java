
package com.gitee.jmash.core.orm;

import jakarta.persistence.Column;

/**
 * 翻页统计信息.
 *
 * @author cgd
 *
 */
public class DtoTotal implements java.io.Serializable {

  private static final long serialVersionUID = 1L;

  // 总记录数
  protected int totalSize = 0;

  @Column(name = "total_size")
  public int getTotalSize() {
    return totalSize;
  }

  public void setTotalSize(int totalSize) {
    this.totalSize = totalSize;
  }

}
