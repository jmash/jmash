package com.gitee.jmash.core.orm;

import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.commons.lang3.StringUtils;

/**
 * 数据库操作线程内容目录.
 *
 * @author CGD-WIN
 */
public class JdbcContext {

  // 表名后缀.
  protected static final ThreadLocal<Map<String, String>> TABLE_SUFFUX = new ThreadLocal<>();

  /** 设置线程表后缀. */
  public static void setTableSuffux(String nativeTableName, String tableSuffux) {
    if (TABLE_SUFFUX.get() == null) {
      TABLE_SUFFUX.set(new ConcurrentHashMap<>());
    }
    if (StringUtils.isBlank(tableSuffux) && TABLE_SUFFUX.get().containsKey(nativeTableName)) {
      TABLE_SUFFUX.get().remove(nativeTableName);
    } else if (StringUtils.isNotBlank(tableSuffux)) {
      TABLE_SUFFUX.get().put(nativeTableName, tableSuffux);
    }
  }

  /** 清理表后缀. */
  public static void clearTableSuffux(String nativeTableName) {
    Map<String, String> tableMap = TABLE_SUFFUX.get();
    if (tableMap != null && tableMap.containsKey(nativeTableName)) {
      TABLE_SUFFUX.get().remove(nativeTableName);
    }
  }

  /** 是否存在分表配置. */
  public static boolean isTableSuffux() {
    Map<String, String> tableMap = TABLE_SUFFUX.get();
    return tableMap != null && tableMap.size() > 0;
  }


  /** 替换SQL. */
  public static String replaceSql(String sql) {
    Map<String, String> tableMap = TABLE_SUFFUX.get();
    String temp = sql;
    if (tableMap != null) {
      // 空格解决，字段名字包含表名称的情况。
      for (Entry<String, String> table : tableMap.entrySet()) {
        String newTableName = table.getKey() + "_" + table.getValue() + " ";
        temp = temp.replace(table.getKey() + " ", newTableName);
      }
    }
    return temp;
  }

}
