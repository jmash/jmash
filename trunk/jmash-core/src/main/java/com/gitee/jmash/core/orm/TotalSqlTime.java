package com.gitee.jmash.core.orm;

import java.util.ArrayList;
import java.util.List;

/**
 * 用于统计SQL的实体.
 *
 * @author CGD
 *
 */
public class TotalSqlTime {

  // SQL 执行时间阈值(毫秒).
  public static long LONG_QUERY_TIME = 500;

  /** 总执行时间. */
  private long totalTime = 0L;
  /** 完整的方法名称. */
  private String fullMethodName = "";

  /** 单个执行时间记录. */
  private List<SingleSqlTime> list = new ArrayList<SingleSqlTime>();

  public TotalSqlTime() {
    super();
  }

  public TotalSqlTime(String fullMethodName) {
    super();
    this.fullMethodName = fullMethodName;
  }

  /**
   * 增加单个SQLTime.
   */
  public void add(SingleSqlTime singleSqlTime) {
    list.add(singleSqlTime);
    this.totalTime += singleSqlTime.time;
  }

  @Override
  public String toString() {
    if (list.size() == 0) {
      return String.format("执行0条SQL");
    }
    return String.format("执行%d条SQL,花费%dms时间,单条SQL平均%dms", list.size(), totalTime,
        totalTime / list.size());
  }

  public long getTotalTime() {
    return totalTime;
  }

  public void setTotalTime(int totalTime) {
    this.totalTime = totalTime;
  }

  public List<SingleSqlTime> getList() {
    return list;
  }

  public void setList(List<SingleSqlTime> list) {
    this.list = list;
  }

  public String getFullMethodName() {
    return fullMethodName;
  }

  public void setFullMethodName(String fullMethodName) {
    this.fullMethodName = fullMethodName;
  }

  /**
   * 单SQL执行时间统计.
   *
   * @author CGD
   *
   */
  public static class SingleSqlTime {

    public SingleSqlTime(long time) {
      super();
      this.time = time;
    }

    private long time;

    public long getTime() {
      return time;
    }

    public void setTime(int time) {
      this.time = time;
    }

  }
}
