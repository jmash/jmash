package com.gitee.jmash.core.orm.cdi;

import com.gitee.jmash.core.jta.DataSourceManage;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Produces;
import jakarta.inject.Named;
import jakarta.inject.Singleton;
import javax.sql.DataSource;

/** 数据源配置. */
@ApplicationScoped
public class DataSourceConfig {

  /** 满足flowable初始化加载需求(此时CDI未完成加载). */
  public static DataSource getWriteDataSource() {
    return DataSourceManage.getDataSource(null, true);
  }

  /**
   * 写数据源.
   */
  @Produces
  @Named
  @Singleton
  DataSource writeDataSource() {
    return DataSourceManage.getDataSource(null, true);
  }

  /**
   * 读数据源.
   */
  @Produces
  @Named
  @Singleton
  DataSource readDataSource() {
    return DataSourceManage.getDataSource(null, false);
  }

}
