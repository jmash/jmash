
package com.gitee.jmash.core.orm.cdi;

import jakarta.enterprise.inject.spi.Annotated;
import jakarta.enterprise.inject.spi.InjectionPoint;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.PersistenceUnit;
import java.util.Objects;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.engine.spi.SessionImplementor;
import org.jboss.weld.injection.ParameterInjectionPoint;
import org.jboss.weld.injection.spi.ResourceReference;
import org.jboss.weld.injection.spi.ResourceReferenceFactory;

/**
 * Weld JPA Injection Services.
 *
 * @author CGD
 *
 */
public class JakartaInjectionServices implements org.jboss.weld.injection.spi.JpaInjectionServices {

  private static Log log = LogFactory.getLog(JakartaInjectionServices.class);

  @Override
  public ResourceReferenceFactory<EntityManager> registerPersistenceContextInjectionPoint(
      InjectionPoint injectionPoint) {
    final PersistenceContext persistenceContextAnnotation =
        getResourceAnnotated(injectionPoint).getAnnotation(PersistenceContext.class);
    if (persistenceContextAnnotation == null) {
      throw new IllegalArgumentException(
          "injectionPoint.getAnnotated().getAnnotation(PersistenceContext.class) == null");
    }
    final String unitName = persistenceContextAnnotation.unitName();
    log.debug(unitName);
    return () -> new EntityManagerResourceReference(unitName);
  }

  @Override
  public ResourceReferenceFactory<EntityManagerFactory> registerPersistenceUnitInjectionPoint(
      InjectionPoint injectionPoint) {
    Objects.requireNonNull(injectionPoint);
    final PersistenceUnit persistenceUnitAnnotation =
        getResourceAnnotated(injectionPoint).getAnnotation(PersistenceUnit.class);
    if (persistenceUnitAnnotation == null) {
      throw new IllegalArgumentException(
          "injectionPoint.getAnnotated().getAnnotation(PersistenceUnit.class) == null");
    }
    final String name = persistenceUnitAnnotation.unitName();

    return () -> new EntityManagerFactoryResourceReference(name);
  }

  @Override
  public void cleanup() {

  }

  /** 获取注入切入点注解. */
  public static Annotated getResourceAnnotated(InjectionPoint injectionPoint) {
    if (injectionPoint instanceof ParameterInjectionPoint) {
      return ((ParameterInjectionPoint<?, ?>) injectionPoint).getAnnotated().getDeclaringCallable();
    }
    return injectionPoint.getAnnotated();
  }



  private final class EntityManagerResourceReference implements ResourceReference<EntityManager> {

    private String unitName;

    EntityManager entityManager;

    public EntityManagerResourceReference(String unitName) {
      super();
      this.unitName = unitName;
    }

    @Override
    public EntityManager getInstance() {
      if (unitName.startsWith("Write")) {
        log.trace("inject Write EntityManager");
        entityManager = JpaUtil.getEntityManager(unitName.substring(5).toLowerCase(), true);
      } else if (unitName.startsWith("Read")) {
        log.trace("inject Read EntityManager");
        entityManager = JpaUtil.getEntityManager(unitName.substring(4).toLowerCase(), false);
        // ReadOnly Transaction.
        SessionImplementor session = entityManager.unwrap(SessionImplementor.class);
        session.setDefaultReadOnly(true);
      } else {
        log.warn("PersistenceUnit Unit Name Mush Start With  Write/Read. ");
        entityManager = null;
      }
      return entityManager;
    }



    @Override
    public void release() {
      entityManager.close();
      entityManager = null;
    }
  }

  private final class EntityManagerFactoryResourceReference
      implements ResourceReference<EntityManagerFactory> {

    private String unitName;

    public EntityManagerFactoryResourceReference(String unitName) {
      super();
      this.unitName = unitName;
    }

    @Override
    public EntityManagerFactory getInstance() {
      if (unitName.startsWith("Write")) {
        return JpaUtil.getWriteEntityManagerFactory();
      } else if (unitName.startsWith("Read")) {
        return JpaUtil.getReadEntityManagerFactory();
      } else {
        log.warn("PersistenceUnit Unit Name Mush Start With  Write/Read. ");
      }
      return null;
    }

    @Override
    public void release() {

    }

  }

}
