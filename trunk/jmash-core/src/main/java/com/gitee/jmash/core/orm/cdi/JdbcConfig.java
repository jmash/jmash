
package com.gitee.jmash.core.orm.cdi;

import com.gitee.jmash.core.orm.jdbc.JdbcTemplate;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Produces;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import jakarta.inject.Singleton;
import javax.sql.DataSource;

/** JDBC Config. */
@ApplicationScoped
public class JdbcConfig {

  @Inject
  @Named("writeDataSource")
  DataSource writeDataSource;

  @Produces
  @Singleton
  JdbcTemplate getJdbcTemplate() {
    return JpaUtil.getJdbcTemplate(writeDataSource);
  }

}
