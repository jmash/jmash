
package com.gitee.jmash.core.orm.cdi;

import com.gitee.jmash.common.config.AppProps;
import com.gitee.jmash.core.orm.jpa.proxy.EntityManagerFactoryProxy;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Disposes;
import jakarta.enterprise.inject.Produces;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import jakarta.inject.Singleton;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.eclipse.microprofile.config.Config;
import org.eclipse.microprofile.config.ConfigProvider;
import org.eclipse.microprofile.config.inject.ConfigProperties;

/**
 * JPA CDI Config.
 *
 * @author CGD
 *
 */
@ApplicationScoped
public class JpaReadWriteConfig {

  private static String UnitNameKey = "jmash.jpa.unitname";

  @Inject
  @ConfigProperties
  AppProps appProps;

  @Inject
  @Named("writeDataSource")
  DataSource writeDataSource;

  @Inject
  @Named("readDataSource")
  DataSource readDataSource;

  /**
   * Write JPA Factory.
   */
  @Produces
  @Named
  @Singleton
  EntityManagerFactory writeEntityManagerFactory() {
    Config config = ConfigProvider.getConfig();
    String unitName = config.getValue(UnitNameKey, String.class);
    Map<String, Object> map = new HashMap<>();
    map.put("jakarta.persistence.nonJtaDataSource", writeDataSource);
    EntityManagerFactory emf = Persistence.createEntityManagerFactory(unitName, map);

    EntityManagerFactoryProxy proxy = new EntityManagerFactoryProxy();
    return proxy.bind(emf, appProps.getType());
  }

  /**
   * Read JPA Factory.
   */
  @Produces
  @Named
  @Singleton
  EntityManagerFactory readEntityManagerFactory() {
    Config config = ConfigProvider.getConfig();
    String unitName = config.getValue(UnitNameKey, String.class);
    Map<String, Object> map = new HashMap<>();
    map.put("jakarta.persistence.nonJtaDataSource", readDataSource);
    EntityManagerFactory emf = Persistence.createEntityManagerFactory(unitName, map);
    EntityManagerFactoryProxy proxy = new EntityManagerFactoryProxy();
    return proxy.bind(emf, appProps.getType());
  }

  public void close(@Disposes EntityManagerFactory entityManagerFactory) {
    entityManagerFactory.close();
  }

}
