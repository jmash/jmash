
package com.gitee.jmash.core.orm.cdi;

import jakarta.enterprise.context.RequestScoped;
import jakarta.enterprise.inject.Stereotype;
import jakarta.enterprise.util.AnnotationLiteral;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * JPA Service.
 *
 * @author CGD
 *
 */
@RequestScoped
@Stereotype
@Target(java.lang.annotation.ElementType.TYPE)
@Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
public @interface JpaService {

  /** 默认实现. */
  @SuppressWarnings("all")
  public static final class Literal extends AnnotationLiteral<JpaService> implements JpaService {
    public static final Literal INSTANCE = new Literal();
    private static final long serialVersionUID = 1L;
  }
}
