
package com.gitee.jmash.core.orm.cdi;

import jakarta.enterprise.context.Dependent;
import jakarta.enterprise.inject.Stereotype;
import jakarta.enterprise.util.AnnotationLiteral;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * JPA Service.
 *
 * @author CGD
 *
 */
@Dependent
@Stereotype
@Target(java.lang.annotation.ElementType.TYPE)
@Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
public @interface JpaTenantService {

  /** 默认实现. */
  @SuppressWarnings("all")
  public static final class Literal extends AnnotationLiteral<JpaTenantService>
      implements JpaTenantService {
    public static final Literal INSTANCE = new Literal();
    private static final long serialVersionUID = 1L;
  }
}
