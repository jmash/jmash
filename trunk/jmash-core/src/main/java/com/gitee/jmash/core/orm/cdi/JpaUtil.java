package com.gitee.jmash.core.orm.cdi;

import com.gitee.jmash.core.jta.DataSourceAdapter;
import com.gitee.jmash.core.orm.jdbc.DataSourceConnectionAccess;
import com.gitee.jmash.core.orm.jdbc.JdbcTemplate;
import com.gitee.jmash.core.orm.tenant.JpaMultiTenantIdentifier;
import com.gitee.jmash.core.orm.tenant.JpaMultiTenantResolver;
import com.gitee.jmash.core.utils.AnnotationUtils;
import jakarta.enterprise.inject.spi.CDI;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.engine.spi.SessionImplementor;

/** JPA Util. */
public class JpaUtil {

  public static String UNIT_NAME = "jmash.unit_name";

  /** Read EntityManagerFactory. */
  public static EntityManagerFactory getReadEntityManagerFactory() {
    return CDI.current().select(EntityManagerFactory.class,
        AnnotationUtils.getNamedAnnotation("readEntityManagerFactory")).get();
  }

  /** Write EntityManagerFactory. */
  public static EntityManagerFactory getWriteEntityManagerFactory() {
    return CDI.current().select(EntityManagerFactory.class,
        AnnotationUtils.getNamedAnnotation("writeEntityManagerFactory")).get();
  }

  /** Tenant 为 “” . */
  public static EntityManager getEntityManager(String unitName, boolean write) {
    return getTenantEntityManager(unitName, "", write);
  }

  /** (非JTA)获取数据源JdbcTemplate. */
  public static JdbcTemplate getJdbcTemplate(DataSource writeDataSource) {
    DataSource dbDateSource = writeDataSource;
    if (writeDataSource instanceof DataSourceAdapter) {
      dbDateSource = ((DataSourceAdapter) dbDateSource).getDataSource();
    }
    return new JdbcTemplate(new DataSourceConnectionAccess(dbDateSource), false);
  }

  /** 支持Tenant(bug:并发导致获取租户有误！). */
  public static synchronized EntityManager getTenantEntityManager(String unitName, String tenant,
      boolean write) {
    SessionFactoryImplementor sf =
        write ? (SessionFactoryImplementor) getWriteEntityManagerFactory()
            : (SessionFactoryImplementor) getReadEntityManagerFactory();
    CurrentTenantIdentifierResolver<?> resolver = sf.getCurrentTenantIdentifierResolver();
    if (resolver instanceof JpaMultiTenantResolver) {
      JpaMultiTenantIdentifier tenantIdentifier =
          new JpaMultiTenantIdentifier(unitName, tenant, write);
      ((JpaMultiTenantResolver) resolver).setTenantIdentifier(tenantIdentifier);
    }
    EntityManager em = sf.createEntityManager();
    em.setProperty(UNIT_NAME, unitName);
    return em;
  }

  /** 支持Tenant. */
  public static SessionImplementor getTenantSessionImpl(String unitName, String tenant,
      boolean write) {
    return (SessionImplementor) getTenantEntityManager(unitName, tenant, write);
  }
}
