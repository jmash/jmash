package com.gitee.jmash.core.orm.cdi;

import com.gitee.jmash.core.jta.DataSourceTransactionManager;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Disposes;
import jakarta.enterprise.inject.Produces;
import jakarta.inject.Singleton;
import jakarta.transaction.SystemException;
import jakarta.transaction.TransactionManager;

/**
 * 事务管理.
 *
 * @author cgd
 */
@ApplicationScoped
public class TransactionConfig {


  /** 初始化事务管理器. */
  @Produces
  @Singleton
  public TransactionManager getTransactionManager() throws SystemException {
    // 创建本地数据源事务管理器
    DataSourceTransactionManager transactionManager = new DataSourceTransactionManager();
    return transactionManager;
  }

  /**
   * 关闭事务管理器.
   */
  public void close(@Disposes TransactionManager transactionManager) {
    if (transactionManager instanceof DataSourceTransactionManager) {
      // 关闭事务管理器
      ((DataSourceTransactionManager) transactionManager).close();
    }
  }

}
