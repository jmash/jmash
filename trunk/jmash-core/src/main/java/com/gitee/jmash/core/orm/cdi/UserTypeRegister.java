package com.gitee.jmash.core.orm.cdi;

import com.gitee.jmash.core.orm.tenant.TenantIdentifierType;
import org.hibernate.boot.model.TypeContributions;
import org.hibernate.service.ServiceRegistry;


/** UserType SPI注册. */
public class UserTypeRegister implements org.hibernate.boot.model.TypeContributor {

  @Override
  public void contribute(TypeContributions typeContributions, ServiceRegistry serviceRegistry) {
    typeContributions.contributeType(TenantIdentifierType.INSTANCE);
  }

}
