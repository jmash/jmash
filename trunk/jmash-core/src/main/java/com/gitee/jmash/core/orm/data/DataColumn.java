
package com.gitee.jmash.core.orm.data;

import org.apache.commons.lang3.StringUtils;

/** Column 数据. */
public class DataColumn {

  /** dataTable的引用. */
  private DataTable table;
  /** 列名. */
  private String columnName;
  /** 显示名称. */
  private String columnRemark;
  /** 通过tag对象保存公式配置. */
  private Object tag;
  /** 列索引. */
  private int columnIndex;
  /** 列数据类型. */
  private int dataType;
  /** 数据类型名称. */
  private String dataTypeName;
  /** 是否允许为空（null）. */
  private Boolean nullable;
  /** 字段长度. */
  private Integer length;
  /** 小数位数. */
  private Integer digits;
  /** 默认值. */
  private String defaultValue;

  public DataColumn() {
    this("default1");
  }

  public DataColumn(int dataType) {
    this("default1", dataType);
  }

  public DataColumn(String columnName) {
    this(columnName, 0);
  }

  public DataColumn(String columnName, int dataType) {
    this.setDataType(dataType);
    this.columnName = columnName;
  }

  /**
   * 功能描述： 将输入数据转为当前列的数据类型返回.
   *
   * @author James Cheung.
   * @version 2.0
   */
  public Object convertTo(Object value) {
    return value;
  }

  @Override
  public String toString() {
    return this.columnName;
  }

  public String getColumnName() {
    return this.columnName;
  }

  /** 设置列名称和列描述. */
  public void setColumnName(String columnName) {
    this.columnName = columnName;
    if (StringUtils.isEmpty(columnRemark)) {
      columnRemark = columnName;
    }
  }

  public String getColumnRemark() {
    return columnRemark;
  }

  public void setColumnRemark(String columnRemark) {
    this.columnRemark = columnRemark;
  }

  public DataTable getTable() {
    return this.table;
  }

  public void setTable(DataTable table) {
    this.table = table;
  }

  public void setDataType(int dataType) {
    this.dataType = dataType;
  }

  public int getDataType() {
    return dataType;
  }

  public void setColumnIndex(int columnIndex) {
    this.columnIndex = columnIndex;
  }

  public int getColumnIndex() {
    return columnIndex;
  }

  public String getDataTypeName() {
    return this.dataTypeName;
  }

  public void setDataTypeName(String dataTypeName) {
    this.dataTypeName = dataTypeName;
  }

  public void setTag(Object tag) {
    this.tag = tag;
  }

  public Object getTag() {
    return tag;
  }

  public Boolean getNullable() {
    return nullable;
  }

  public void setNullable(Boolean nullable) {
    this.nullable = nullable;
  }

  public Integer getLength() {
    return length;
  }

  public void setLength(Integer length) {
    this.length = length;
  }

  public Integer getDigits() {
    return digits;
  }

  public void setDigits(Integer digits) {
    this.digits = digits;
  }

  public String getDefaultValue() {
    return defaultValue;
  }

  public void setDefaultValue(String defaultValue) {
    this.defaultValue = defaultValue;
  }

}
