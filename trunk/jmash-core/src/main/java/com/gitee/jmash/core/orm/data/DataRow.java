
package com.gitee.jmash.core.orm.data;

import java.util.LinkedHashMap;
import java.util.Map;

/** 数据行. */
public class DataRow {

  /** 定义该行记录在table所处的行数. */
  private int rowIndex = -1;

  /** 一行数据. */
  private DataColumnCollection columns;

  /** table的一个引用. */
  private DataTable table;

  /** 用于存储数据的Map对象，这里保存的对象不包括顺序信息，数据获取的索引通过行信息标识. */
  private Map<String, Object> itemMap = new LinkedHashMap<String, Object>();

  public DataRow() {

  }

  /** 创建表行. */
  public DataRow(DataTable table) {
    this.table = table;
    if (table != null) {
      this.columns = table.getColumns();
    }
  }

  /**
   * 功能描述： 获取当前行的行索引.
   *
   * @author James Cheung
   * @version 2.0
   */
  public int getRowIndex() {
    return rowIndex;
  }

  /**
   * 功能描述： 获取当前行所属数据表对象.
   *
   * @author James Cheung
   * @version 2.0
   */
  public DataTable getTable() {
    return this.table;
  }

  public void setColumns(DataColumnCollection columns) {
    this.columns = columns;
  }

  public DataColumnCollection getColumns() {
    return columns;
  }

  public void setValue(int index, Object value) {
    setValue(this.columns.get(index), value);
  }

  public void setValue(String columnName, Object value) {
    setValue(this.columns.get(columnName), value);
  }

  /** 设置数据列. */
  public void setValue(DataColumn column, Object value) {
    if (column != null) {
      getItemMap().put(column.getColumnName(), column.convertTo(value));
    }
  }

  public Object getValue(int index) {
    String colName = this.columns.get(index).getColumnName();
    return this.getItemMap().get(colName);
  }

  public Object getValue(String columnName) {
    return this.getItemMap().get(columnName);
  }

  public Map<String, Object> getItemMap() {
    return itemMap;
  }

  public void setRowIndex(int rowIndex) {
    this.rowIndex = rowIndex;
  }

  /** 行拷贝. */
  public void copyFrom(DataRow row) {
    this.itemMap.clear();
    for (Object c : this.columns) {
      this.itemMap.put(c.toString(), row.getValue(c.toString()));
    }
  }

  /** 值转换为字符串. */
  public String valueToString() {
    String result = "$$";
    for (DataColumn column : this.getColumns()) {
      result += this.itemMap.get(column.getColumnName());
      result += "$$";
    }
    return result.substring(0, result.length() - 2);
  }

  /** 字符串转换为值. */
  public void parseString(String inputString) {
    String[] values = inputString.split("$$");
    itemMap.clear();
    if (values.length == this.getColumns().size()) {
      int i = 0;
      for (DataColumn dc : columns) {
        itemMap.put(dc.getColumnName(), values[i]);
        i++;
      }
    }
  }

  public String toString() {
    return this.getItemMap().toString();
  }
}
