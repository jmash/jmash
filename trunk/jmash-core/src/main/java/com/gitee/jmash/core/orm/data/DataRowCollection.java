
package com.gitee.jmash.core.orm.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/** 行集合. */
public class DataRowCollection implements List<DataRow> {

  List<DataRow> dataRows = new ArrayList<DataRow>();

  private DataColumnCollection columns;

  DataRowCollection() {
  }

  public int size() {
    return this.dataRows.size();
  }

  public void clear() {
    this.dataRows.clear();
  }

  public boolean add(DataRow row) {
    return this.dataRows.add(row);
  }

  public void add(int index, DataRow element) {
    this.dataRows.add(index, element);
  }

  /** 移除行. */
  public boolean remove(DataRow row) {
    if (this.dataRows.contains(row)) {
      this.dataRows.remove(row);
    }
    return false;
  }

  /** 移除行索引. */
  public DataRow remove(int index) {
    if (this.dataRows.size() >= index) {
      return this.dataRows.remove(index);
    }
    return null;
  }

  public boolean remove(Object o) {
    return this.dataRows.remove(o);
  }

  /** 获取行数据. */
  public DataRow get(int index) {
    if (this.dataRows.size() > index) {
      return this.dataRows.get(index);
    }
    return null;
  }

  public void setColumns(DataColumnCollection columns) {
    this.columns = columns;
  }

  public DataColumnCollection getColumns() {
    return columns;
  }

  public boolean isEmpty() {
    return this.dataRows.isEmpty();
  }

  public boolean contains(Object o) {
    return this.dataRows.contains(o);
  }

  public Iterator<DataRow> iterator() {
    return this.dataRows.iterator();
  }

  public Object[] toArray() {
    return this.dataRows.toArray();
  }

  public <T> T[] toArray(T[] a) {
    return this.dataRows.toArray(a);
  }

  public boolean containsAll(Collection<?> c) {
    return this.dataRows.containsAll(c);
  }

  public boolean addAll(Collection<? extends DataRow> c) {
    return this.dataRows.addAll(c);
  }

  public boolean addAll(int index, Collection<? extends DataRow> c) {
    return this.dataRows.addAll(index, c);
  }

  public boolean removeAll(Collection<?> c) {
    return this.dataRows.removeAll(c);
  }

  public boolean retainAll(Collection<?> c) {
    return this.dataRows.retainAll(c);
  }

  public DataRow set(int index, DataRow element) {
    return this.dataRows.set(index, element);
  }

  public int indexOf(Object o) {
    return this.dataRows.indexOf(o);
  }

  public int lastIndexOf(Object o) {
    return this.dataRows.lastIndexOf(o);
  }

  public ListIterator<DataRow> listIterator() {
    return this.dataRows.listIterator();
  }

  public ListIterator<DataRow> listIterator(int index) {
    return this.dataRows.listIterator(index);
  }

  public List<DataRow> subList(int fromIndex, int toIndex) {
    return this.dataRows.subList(fromIndex, toIndex);
  }

}
