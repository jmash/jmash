
package com.gitee.jmash.core.orm.data;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

/**
 * 此类描述的是： 保存一组DataTable的数据集合对象，用于统一管理这组对象.
 *
 * @author James Cheung
 * @version 2.0
 */
public class DataSet implements Collection<DataTable> {

  private String name;

  /** 数据集合集. */
  private List<DataTable> tables;

  /**
   * 功能描述： 获取当前Dataset对象内包括的数据表集合.
   *
   * @author James Cheung
   * @version 2.0
   */
  public List<DataTable> getDataTables() {
    return this.tables;
  }

  /**
   * 创建一个新的实例 DataSet.
   */
  public DataSet() {
    if (tables == null) {
      tables = new Vector<DataTable>();
    }
  }

  /**
   * 创建一个新的实例 DataSet.
   */
  public DataSet(String name) {
    this();
    this.name = name;
  }

  /**
   * 功能描述： 获取当前Dataset的名称.
   *
   * @author James Cheung
   * @version 2.0
   */
  public String getName() {
    return name;
  }

  /**
   * 功能描述： 设置数据集对象的名称.
   *
   * @author James Cheung
   * @version 2.0
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * 功能描述： 添加一个新的数据表到集合.
   *
   * @author James Cheung
   * @version 2.0
   */
  public void addDataTable(DataTable dtb) {
    this.tables.add(dtb);
  }

  /**
   * 功能描述： 获取一个已经存在的数据表.
   *
   * @author James Cheung
   * @version 2.0
   */
  public DataTable getDataTable(int index) {
    return this.tables.get(index);
  }

  /**
   * 功能描述： 获取指定名称的数据表对象.
   *
   * @author James Cheung
   * @version 2.0
   */
  public DataTable getDataTable(String tableName) {
    for (DataTable table : tables) {
      if (table != null && table.getTableName().equals(tableName)) {
        return table;
      }
    }
    return null;
  }

  /**
   * 功能描述： 删除指定名称的数据表.
   *
   * @author James Cheung
   * @version 2.0
   */
  public void removeDataTable(String tableName) {
    int i = -1;
    for (DataTable table : tables) {
      if (table != null && table.getTableName().equals(tableName)) {
        i++;
      }
    }
    if (i > -1) {
      removeDataTable(i);
    }
  }

  /**
   * 功能描述： 删除指定位置的数据表.
   *
   * @author James Cheung
   * @version 2.0
   */
  public void removeDataTable(int tableIndex) {
    tables.remove(tableIndex);
  }

  /**
   * 功能描述： 获取当前数据集合包括的数据表对象.
   *
   * @author James Cheung
   * @version 2.0
   */
  public int size() {
    return this.tables.size();
  }

  public boolean isEmpty() {
    return tables.isEmpty();
  }

  public boolean contains(Object o) {
    return tables.contains(o);
  }

  public Iterator<DataTable> iterator() {
    return tables.iterator();
  }

  public Object[] toArray() {
    return tables.toArray();
  }

  public <T> T[] toArray(T[] a) {
    return tables.toArray(a);
  }

  public boolean add(DataTable e) {
    return tables.add(e);
  }

  public boolean remove(Object o) {
    return tables.remove(o);
  }

  public boolean containsAll(Collection<?> c) {
    return tables.containsAll(c);
  }

  public boolean addAll(Collection<? extends DataTable> c) {
    return tables.addAll(c);
  }

  public boolean removeAll(Collection<?> c) {
    return tables.removeAll(c);
  }

  public boolean retainAll(Collection<?> c) {
    return tables.retainAll(c);
  }

  public void clear() {
    tables.clear();
  }
}
