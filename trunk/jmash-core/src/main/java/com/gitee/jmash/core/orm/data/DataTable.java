
package com.gitee.jmash.core.orm.data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * 仿.NET DataTable 简化和修改了大部分方法.
 * 
 */
public final class DataTable {

  /** 用于保存DataRow的集合对象. */
  private DataRowCollection rows;
  /** 用于保存DataColumn的对象. */
  private DataColumnCollection columns;
  /** 表名. */
  private String tableName;
  /** 表中文名. */
  private String tableRemark;
  /** 主键. */
  private Set<String> primeKey = new TreeSet<String>();
  /** 索引. */
  private Map<String, Set<String>> indexs = new HashMap<String, Set<String>>();

  /** 存放其他扩展属性. */
  private Map<String, Object> tag = new HashMap<String, Object>();

  /** 表. */
  public DataTable() {
    this.columns = new DataColumnCollection();
    this.rows = new DataRowCollection();
    this.rows.setColumns(columns);
  }

  public DataTable(String dataTableName) {
    this();
    this.tableName = dataTableName;
  }

  /**
   * 数据记录数.
   */
  public int getTotalCount() {
    return rows.size();
  }

  /**
   * 功能描述： 返回表名.
   *
   * @author guojiyong
   * @version 2.0
   */
  public String getTableName() {
    return this.tableName;
  }

  /**
   * 功能描述： 设置表名.
   *
   * @author guojiyong
   * @version 2.0
   */
  public void setTableName(String tableName) {
    this.tableName = tableName;
  }

  /**
   * 功能描述： 返回该表引用的封装类.
   *
   * @author guojiyong
   * @version 2.0
   */
  public DataRowCollection getRows() {
    return this.rows;
  }

  public DataColumnCollection getColumns() {
    return this.columns;
  }

  /**
   * 功能描述： 获取指定行指定列的数据.
   *
   * @author James Cheung
   * @version 2.0
   */

  public Object getValue(int row, String colName) {
    return this.rows.get(row).getValue(colName);
  }

  public Object getValue(int row, int col) {
    return this.rows.get(row).getValue(col);
  }

  /**
   * 功能描述： 为该表数据新建一行.
   *
   * @author guojiyong
   * @version 2.0
   */
  public DataRow newRow() throws Exception {
    DataRow tempRow = new DataRow(this);
    // nextRowIndex = nextRowIndex < this.rows.size() ? this.rows.size()
    // : nextRowIndex;
    int lastRowIndex = 0;
    if (this.rows.size() > 0) {
      lastRowIndex = this.rows.get(this.rows.size() - 1).getRowIndex();
    } else {
      lastRowIndex = 0;
    }

    tempRow.setColumns(this.columns);
    tempRow.setRowIndex(++lastRowIndex);
    return tempRow;
  }

  public void setValue(int row, int col, Object value) {
    this.rows.get(row).setValue(col, value);
  }

  public void setValue(int row, String colName, Object value) {
    this.rows.get(row).setValue(colName, value);
  }

  public void setTag(String name, Object value) {
    this.tag.put(name, value);
  }

  public Object getTag(String name) {
    return tag.get(name);
  }

  /** 增加列. */
  public DataColumn addColumn(String columnName, int dataType) {
    DataColumn column = this.columns.addColumn(columnName, dataType);
    column.setTable(this);
    return column;
  }

  /** 增加列. */
  public DataColumn addColumnIndex(int index, String columnName, int dataType) {
    DataColumn column = this.columns.addColumn(index, columnName, dataType);
    column.setTable(this);
    return column;
  }

  /** 增加行. */
  public boolean addRow(DataRow row) throws Exception {
    if (this.rows.size() > 0) {
      row.setRowIndex(this.rows.get(this.rows.size() - 1).getRowIndex() + 1);
    } else {
      row.setRowIndex(1);
    }
    return this.rows.add(row);

  }

  /**
   * 功能描述: 排序.
   *
   * @author James Cheung
   * @version 2.0
   */
  public DataTable sort(DataTable table, DataRow row, List<SortedDataColumn> sort) {
    if (table == null) {
      return null;
    }
    int tagetIndex = 0;
    for (DataRow r : table.rows) {
      // 循环现有行，针对个排序字段进行判断，判断规则为：当找到第一个返回TURE的记录的索引
      int compareResult = 0; // 默认按相等处理
      tagetIndex++;
      for (SortedDataColumn st : sort) {
        // 如果倒序
        int temp = 0;
        if (st.getSortType() == SortType.DESC) {

          temp = st.getComparator().compare(row.getValue(st.getColumn().getColumnName()),
              r.getValue(st.getColumn().getColumnName()));
        } else {
          // 构造按正序
          temp = st.getComparator().compare(r.getValue(st.getColumn().getColumnName()),
              row.getValue(st.getColumn().getColumnName()));
        }
        if (temp < 0) {
          compareResult = 0;
          // 出现不满足条件记录，继续对比下一条
          break;
        } else {
          compareResult = compareResult + temp;
        }
        // 如果找到了，则直接跳出
        if (compareResult > 0) {
          tagetIndex--;
          break;
        }
      }
      if (compareResult > 0) {
        break;
      }
    }
    table.getRows().add(tagetIndex, row);
    return table;
  }

  public String getTableRemark() {
    return tableRemark;
  }

  public void setTableRemark(String tableRemark) {
    this.tableRemark = tableRemark;
  }

  public Set<String> getPrimeKey() {
    return primeKey;
  }

  public void setPrimeKey(Set<String> primeKey) {
    this.primeKey = primeKey;
  }

  public Map<String, Set<String>> getIndexs() {
    return indexs;
  }

  public void setIndexs(Map<String, Set<String>> indexs) {
    this.indexs = indexs;
  }

}
