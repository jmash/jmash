
package com.gitee.jmash.core.orm.data;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import org.apache.commons.logging.LogFactory;

/** 数据表适配. */
public class DataTableAdapter {

  /**
   * 将ResultSet 转换为 DataTable.
   */
  public static DataTable parse(ResultSet rs) throws SQLException {
    if (rs == null) {
      return null;
    }
    ResultSetMetaData rsmd = rs.getMetaData();
    int count = rsmd.getColumnCount();
    if (count <= 0) {
      return null;
    }
    DataTable dt = new DataTable(rsmd.getTableName(1));
    try {
      // 表元数据
      for (int i = 1; i <= count; i++) {
        DataColumn col = dt.addColumn(rsmd.getColumnName(i), rsmd.getColumnType(i));
        col.setColumnRemark(rsmd.getColumnLabel(i));
        col.setDataTypeName(rsmd.getColumnTypeName(i));
      }
      // 数据
      while (rs.next()) {
        DataRow row = new DataRow(dt);
        for (int j = 0; j < count; j++) {
          row.setValue(j, rs.getObject(j + 1));
        }
        dt.addRow(row);
      }
    } catch (Exception ex) {
      LogFactory.getLog(DataTableAdapter.class).error("", ex);
    }
    return dt;
  }

}
