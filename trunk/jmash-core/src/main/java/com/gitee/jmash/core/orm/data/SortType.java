
package com.gitee.jmash.core.orm.data;

/**
 * 此类描述的是: 排序.
 *
 * @author James Cheung
 * @version 2.0
 */
public enum SortType {
  ASC, DESC;
}
