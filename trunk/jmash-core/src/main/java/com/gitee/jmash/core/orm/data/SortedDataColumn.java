
package com.gitee.jmash.core.orm.data;

import com.gitee.jmash.common.utils.ComparatorUtil;
import java.util.Comparator;

/**
 * 此类描述的是： 用于保存排序字段信息的对象.
 *
 * @author James Cheung
 * @version 2.0
 */
public class SortedDataColumn {

  private Comparator<Object> comparator = ComparatorUtil.getComparator();

  private DataColumn column;

  private SortType sortType;

  public void setColumn(DataColumn column) {
    this.column = column;
  }

  public DataColumn getColumn() {
    return column;
  }

  public void setSortType(SortType sortType) {
    this.sortType = sortType;
  }

  public SortType getSortType() {
    return sortType;
  }

  public Comparator<Object> getComparator() {
    return comparator;
  }

  public void setComparator(Comparator<Object> comparator) {
    this.comparator = comparator;
  }

}
