
package com.gitee.jmash.core.orm.jdbc;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * 参考Spi实现.
 *
 * @see org.hibernate.engine.jdbc.connections.spi.JdbcConnectionAccess
 */
public interface ConnectionAccess extends Serializable {

  Connection obtainConnection() throws SQLException;

  void releaseConnection(Connection connection) throws SQLException;

  boolean supportsAggressiveRelease();
  
}
