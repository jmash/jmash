
package com.gitee.jmash.core.orm.jdbc;

import java.sql.Connection;
import java.sql.SQLException;
import javax.sql.DataSource;

/** 数据源 JDBC 连接访问. */
public class DataSourceConnectionAccess implements ConnectionAccess {

  private static final long serialVersionUID = 1L;
  DataSource dataSource;

  public DataSourceConnectionAccess(DataSource dataSource) {
    super();
    this.dataSource = dataSource;
  }

  @Override
  public Connection obtainConnection() throws SQLException {
    return dataSource.getConnection();
  }

  @Override
  public void releaseConnection(Connection connection) throws SQLException {
    connection.close();
  }

  @Override
  public boolean supportsAggressiveRelease() {
    return false;
  }

}
