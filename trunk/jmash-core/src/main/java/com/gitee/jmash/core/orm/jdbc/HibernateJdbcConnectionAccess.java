
package com.gitee.jmash.core.orm.jdbc;

import java.sql.Connection;
import java.sql.SQLException;
import org.hibernate.engine.jdbc.connections.spi.JdbcConnectionAccess;

/** 数据源 JDBC 连接访问. */
public class HibernateJdbcConnectionAccess implements ConnectionAccess {

  private static final long serialVersionUID = 1L;
  JdbcConnectionAccess jca;

  public HibernateJdbcConnectionAccess(JdbcConnectionAccess jca) {
    super();
    this.jca = jca;
  }

  @Override
  public Connection obtainConnection() throws SQLException {
    return jca.obtainConnection();
  }

  @Override
  public void releaseConnection(Connection connection) throws SQLException {
    jca.releaseConnection(connection);
  }

  @Override
  public boolean supportsAggressiveRelease() {
    return jca.supportsAggressiveRelease();
  }

}
