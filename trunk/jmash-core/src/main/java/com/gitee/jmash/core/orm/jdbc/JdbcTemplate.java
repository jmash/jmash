
package com.gitee.jmash.core.orm.jdbc;

import com.gitee.jmash.core.orm.DatabaseName;
import com.gitee.jmash.core.orm.data.DataColumn;
import com.gitee.jmash.core.orm.data.DataRow;
import com.gitee.jmash.core.orm.data.DataRowCollection;
import com.gitee.jmash.core.orm.data.DataSet;
import com.gitee.jmash.core.orm.data.DataTable;
import com.gitee.jmash.core.orm.data.DataTableAdapter;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * JDBC 操作.
 *
 * @author CGD
 * 
 */
public class JdbcTemplate {

  private static Log log = LogFactory.getLog(JdbcTemplate.class);
  /** table. */
  public static final String TABLE = "TABLE";
  /** tableName. */
  public static final String TABLE_NAME = "TABLE_NAME";
  /** tableRemarks. */
  public static final String REMARKS = "REMARKS";
  /** 试图View. */
  public static final String VIEW = "VIEW";
  /** table column name. */
  private static final String COLUMN_NAME = "COLUMN_NAME";
  private static final String COLUMN_DEF = "COLUMN_DEF";
  private static final String COLUMN_SIZE = "COLUMN_SIZE";
  private static final String TYPE_NAME = "TYPE_NAME";
  private static final String DATA_TYPE = "DATA_TYPE";
  private static final String DECIMAL_DIGITS = "DECIMAL_DIGITS";
  private static final String INDEX_NAME = "INDEX_NAME";

  private ConnectionAccess jca;

  private String tenant;
  /** 是否JTA事务. */
  private boolean jta = false;

  public JdbcTemplate(ConnectionAccess jca, boolean jta) {
    this.jca = jca;
    this.jta = jta;
  }

  /** 租户JdbcTemplate. */
  public JdbcTemplate(ConnectionAccess jca, String tenant, boolean jta) {
    this.jca = jca;
    this.tenant = tenant;
    this.jta = jta;
  }

  /**
   * 关闭连接.
   */
  public void close(Connection conn) {
    try {
      if (conn != null) {
        jca.releaseConnection(conn);
      }
      conn = null;
    } catch (Exception e) {
      LogFactory.getLog(getClass()).error(e);
    }
  }

  public String getTenant() {
    return tenant;
  }

  public boolean getJta() {
    return jta;
  }

  /**
   * 获取数据库产品名称.
   */
  public String getDataBaseName() {
    try (Connection conn = this.jca.obtainConnection()) {
      DatabaseMetaData metaData = conn.getMetaData();
      String databaseProductName = metaData.getDatabaseProductName();
      return DatabaseName.convertDatabaseName(databaseProductName).name();
    } catch (SQLException e) {
      log.error(e.getLocalizedMessage(), e);
    }
    return null;
  }

  public boolean existSchema(String schema) {
    return getSchemas().contains(schema);
  }

  /** 获取数据 Schema . */
  public List<String> getSchemas() {
    try (Connection conn = this.jca.obtainConnection()) {
      try (ResultSet schemasRet = conn
          .prepareStatement("SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA").executeQuery()) {
        List<String> schemas = new ArrayList<>();
        while (schemasRet.next()) {
          schemas.add(schemasRet.getString(1));
        }
        return schemas;
      }
    } catch (SQLException e) {
      log.error(e.getLocalizedMessage(), e);
    }
    return Collections.emptyList();
  }

  /**
   * 判断是否存在表.
   */
  public boolean existTables(String... tableNames) throws SQLException {
    if (tableNames.length == 0) {
      return true;
    }
    DataSet ds = getTables(new String[] {TABLE, VIEW});
    for (int i = 0; i < tableNames.length; i++) {
      DataTable dt = ds.getDataTable(tableNames[i]);
      if (dt == null) {
        return false;
      }
    }
    return true;
  }

  /**
   * 获取表的数据.
   */
  public DataTable getColumns(DataTable dt) throws SQLException {
    Connection conn = null;
    try {
      conn = this.jca.obtainConnection();
      DatabaseMetaData metaData = conn.getMetaData();
      ResultSet colRet = metaData.getColumns(null, metaData.getUserName(), dt.getTableName(), "%");

      // 获取表中项的注释
      Map<String, String> remarks = null;

      DatabaseName dbName = DatabaseName.convertDatabaseName(metaData.getDatabaseProductName());
      if (DatabaseName.SQLServer.equals(dbName)) {
        remarks = this.getSqlServerColumnRemarks(dt.getTableName());
      }

      while (colRet.next()) {

        String columnName = colRet.getString(COLUMN_NAME);
        int columnType = colRet.getInt(DATA_TYPE);

        String columnRemark = colRet.getString(REMARKS);
        if (null != remarks) {
          columnRemark = remarks.get(columnName);
        }

        log.debug(columnName);

        DataColumn col = dt.addColumn(columnName, columnType);
        col.setColumnRemark(columnRemark);
        String columnTypeName = colRet.getString(TYPE_NAME);
        col.setDataTypeName(columnTypeName);
        int nullable = colRet.getInt("NULLABLE");
        col.setNullable(0 == nullable);
        String defaultValue = colRet.getString(COLUMN_DEF);
        col.setDefaultValue(defaultValue);
        Integer length = colRet.getInt(COLUMN_SIZE);
        col.setLength(length);
        // 精度
        Integer digits = colRet.getInt(DECIMAL_DIGITS);
        col.setDigits(digits);

      }
      colRet.close();
      return dt;
    } finally {
      close(conn);
    }
  }

  /**
   * 获取数据库表.
   */
  public DataSet getTables() throws SQLException {
    return this.getTables(new String[] {TABLE});
  }

  /**
   * 查询表通过表类型.
   */
  public DataSet getTables(String[] types) throws SQLException {
    Connection conn = null;
    try {
      conn = this.jca.obtainConnection();
      DatabaseMetaData metaData = conn.getMetaData();
      DataSet dataSet = new DataSet(conn.getCatalog());

      log.debug(metaData.getDatabaseProductName());
      log.debug(metaData.getUserName());

      ResultSet rs = metaData.getTables(conn.getCatalog(), metaData.getUserName(), "%", types);
      while (rs.next()) {
        String tableName = rs.getString(TABLE_NAME);
        String tableRemark = rs.getString(REMARKS);
        // MySQL Oracle Connection 连接属性设置如下 可获取表备注
        // props.setProperty("remarks", "true"); //设置可以获取remarks信息
        // props.setProperty("useInformationSchema", "true");//设置可以获取tables remarks信息

        log.trace(tableName);
        if (tableName.toUpperCase().startsWith("BIN$")) {
          // Oracle 删除表
          continue;
        }
        DataTable dt = new DataTable(tableName);
        dt.setTableRemark(tableRemark);
        // SQLServer 获取表备注
        DatabaseName dbName = DatabaseName.convertDatabaseName(metaData.getDatabaseProductName());
        if (DatabaseName.SQLServer.equals(dbName)) {
          tableRemark = getSqlServerTableRemarks(tableName);
          dt.setTableRemark(tableRemark);
        }
        dataSet.add(dt);
      }
      rs.close();
      return dataSet;
    } finally {
      close(conn);
    }
  }

  /**
   * 表、字段填充.
   */
  public DataSet getFullTables() throws SQLException {
    DataSet ds = getTables();
    List<DataTable> dts = ds.getDataTables();
    for (DataTable dt : dts) {
      dt = getColumns(dt);
      dt = getPrimaryKeys(dt);
      dt = getIndexs(dt);
    }
    return ds;
  }

  /**
   * 查询数据库视图.
   */
  public DataSet getViews() throws SQLException {
    return this.getTables(new String[] {VIEW});
  }

  /**
   * 视图、字段填充.
   */
  public DataSet getFullViews() throws SQLException {
    DataSet ds = this.getViews();
    List<DataTable> dts = ds.getDataTables();
    for (DataTable dt : dts) {
      dt = getColumns(dt);
    }
    return ds;
  }

  /**
   * 处理表的主键.
   */
  public DataTable getPrimaryKeys(DataTable dt) throws SQLException {
    dt.setPrimeKey(getPrimaryKeys(dt.getTableName()));
    return dt;
  }

  /**
   * 获取表的主键.
   */
  public Set<String> getPrimaryKeys(String tableName) throws SQLException {
    Connection conn = null;
    try {
      conn = this.jca.obtainConnection();
      // 主键信息.
      Set<String> primaryKey = new TreeSet<String>();
      DatabaseMetaData metaData = conn.getMetaData();
      ResultSet rs = metaData.getPrimaryKeys(null, null, tableName);
      while (rs.next()) {
        String columnName = rs.getString(COLUMN_NAME);
        log.debug(columnName);
        primaryKey.add(columnName);
      }
      rs.close();
      return primaryKey;
    } finally {
      close(conn);
    }
  }

  /**
   * 处理表的索引.
   */
  public DataTable getIndexs(DataTable dt) throws SQLException {
    dt.setIndexs(getIndexs(dt.getTableName()));
    return dt;
  }

  /**
   * 返回表索引.
   */
  public Map<String, Set<String>> getIndexs(String tableName) throws SQLException {
    Connection conn = null;
    try {
      conn = this.jca.obtainConnection();
      Map<String, Set<String>> indexs = new HashMap<String, Set<String>>();
      DatabaseMetaData metaData = conn.getMetaData();
      ResultSet rs = metaData.getIndexInfo(null, null, tableName, false, true);
      while (rs.next()) {
        String indexName = rs.getString(INDEX_NAME);
        if (StringUtils.isBlank(indexName)) {
          continue;
        }
        log.debug(indexName);
        String columnName = rs.getString(COLUMN_NAME);
        log.debug(columnName);
        if (indexs.containsKey(indexName)) {
          indexs.get(indexName).add(columnName);
        } else {
          Set<String> index = new TreeSet<String>();
          index.add(columnName);
          indexs.put(indexName, index);
        }
      }
      rs.close();
      return indexs;
    } finally {
      close(conn);
    }
  }

  /**
   * 执行SQL.
   */
  public int execSql(String sql) throws SQLException {
    Statement st = null;
    Connection conn = null;
    try {
      conn = this.jca.obtainConnection();
      if (!jta) {
        conn.setAutoCommit(false);
      }
      st = conn.createStatement();
      int num = st.executeUpdate(sql);
      if (!jta) {
        conn.commit();
      }
      return num;
    } catch (SQLException ex) {
      if (!jta) {
        conn.rollback();
      }
      throw ex;
    } finally {
      if (st != null) {
        st.close();
      }
      close(conn);
    }
  }

  /**
   * 带参数执行SQL.
   */
  public int execSqlParam(String sql, Object... params) throws SQLException {
    PreparedStatement st = null;
    Connection conn = null;
    try {
      conn = this.jca.obtainConnection();
      if (!jta) {
        conn.setAutoCommit(false);
      }
      st = conn.prepareStatement(sql);
      for (int i = 0; i < params.length; i++) {
        st.setObject(i + 1, params[i]);
      }
      int num = st.executeUpdate();
      if (!jta) {
        conn.commit();
      }
      return num;
    } catch (SQLException ex) {
      if (!jta) {
        conn.rollback();
      }
      throw ex;
    } finally {
      if (st != null) {
        st.close();
      }
      close(conn);
    }
  }

  /**
   * 查询SQL 返回DataTable.
   */
  public DataTable querySql(String sql) throws SQLException {
    Statement st = null;
    Connection conn = null;
    try {
      conn = this.jca.obtainConnection();
      st = conn.createStatement();
      ResultSet rs = st.executeQuery(sql);
      return DataTableAdapter.parse(rs);
    } finally {
      if (st != null) {
        st.close();
      }
      close(conn);
    }
  }

  /**
   * 查询带参数SQL，返回DataTable.
   */
  public DataTable querySqlParam(String sql, Object... params) throws SQLException {
    PreparedStatement st = null;
    Connection conn = null;
    try {
      conn = this.jca.obtainConnection();
      st = conn.prepareStatement(sql);
      for (int i = 0; i < params.length; i++) {
        st.setObject(i + 1, params[i]);
      }
      ResultSet rs = st.executeQuery();
      return DataTableAdapter.parse(rs);
    } finally {
      if (st != null) {
        st.close();
      }
      close(conn);
    }
  }

  /**
   * 查询SQLServer表的注释.
   */
  public String getSqlServerTableRemarks(String tableName) throws SQLException {
    String sql = "SELECT  cast(isnull([value],'')as   varchar)   as  Description "
        + "FROM   ::fn_listextendedproperty ('MS_Description','user', 'dbo', 'table', '" + tableName
        + "', null, default) ";
    DataTable dt = querySql(sql);
    DataRowCollection ds = dt.getRows();
    if (ds.size() > 0) {
      DataRow dr = ds.get(0);
      return (String) dr.getValue("Description");
    }
    return null;
  }

  /**
   * 获取SQLServer表中项的注释.
   */
  public Map<String, String> getSqlServerColumnRemarks(String tableName) throws SQLException {
    String sql = "SELECT objname , cast(isnull([value],'')as   varchar)   as  Description  "
        + "FROM   ::fn_listextendedproperty ('MS_Description','user', 'dbo', 'table', '" + tableName
        + "', 'column', default) ";
    DataTable dt = querySql(sql);

    Map<String, String> remarks = new HashMap<String, String>();

    DataRowCollection ds = dt.getRows();
    if (ds.size() > 0) {
      DataRow dr = ds.get(0);
      remarks.put((String) dr.getValue("objname"), (String) dr.getValue("Description"));
    }
    return remarks;
  }

}
