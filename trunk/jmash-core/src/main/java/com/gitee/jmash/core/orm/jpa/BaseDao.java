
package com.gitee.jmash.core.orm.jpa;

import com.gitee.jmash.common.jaxrs.WebContext;
import com.gitee.jmash.core.orm.DatabaseName;
import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.orm.DtoPageImpl;
import com.gitee.jmash.core.orm.DtoTotal;
import com.gitee.jmash.core.orm.JdbcContext;
import com.gitee.jmash.core.orm.jpa.entity.Delete;
import com.gitee.jmash.core.orm.tenant.JpaTenantIdentifier;
import com.gitee.jmash.core.orm.tenant.TenantIdentifier;
import com.gitee.jmash.core.utils.TenantUtil;
import jakarta.persistence.Cache;
import jakarta.persistence.Cacheable;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityNotFoundException;
import jakarta.persistence.LockModeType;
import jakarta.persistence.Query;
import jakarta.persistence.Table;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Pattern;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockOptions;


/**
 * 持久层基类（仅支持JPA实现）基于范型的实现.
 *
 * @author cgd email:cgd@crenjoy.com
 * @version V1.0
 * @param <E> 表实体
 * @param <F> 实体的主键
 */
public abstract class BaseDao<E extends Serializable, F extends Serializable> {

  private static Log log = LogFactory.getLog(BaseDao.class);

  /** 多租户实体管理. */
  private TenantEntityManager tem;
  /** 数据库类型. */
  private DatabaseName databaseName;
  /** 实体是否可缓存. */
  private Boolean cacheable = null;

  public BaseDao() {
    super();
  }

  public BaseDao(TenantEntityManager tem) {
    super();
    this.tem = tem;
  }

  // 获取<E>的类型 class[0]表示获取泛型E的类型［1］为泛型F的类型
  @SuppressWarnings("unchecked")
  protected Class<E> entityClass =
      (Class<E>) ((ParameterizedType) getClass().getGenericSuperclass())
          .getActualTypeArguments()[0];

  @SuppressWarnings("unchecked")
  protected Class<F> idClass = (Class<F>) ((ParameterizedType) getClass().getGenericSuperclass())
      .getActualTypeArguments()[1];

  public Class<E> getEntityClass() {
    return entityClass;
  }

  public Class<F> getIdClass() {
    return idClass;
  }

  /**
   * 实体是否可缓存.
   */
  public boolean isCacheable() {
    if (null == cacheable) {
      Cacheable c = entityClass.getAnnotation(Cacheable.class);
      cacheable = (null == c) ? false : c.value();
    }
    return cacheable;
  }

  /** 设置线程表后缀. */
  public void setTableSuffux(String tableSuffux) {
    String nativeTableName = this.getNativeTableName();
    JdbcContext.setTableSuffux(nativeTableName, tableSuffux);
  }

  /** 设置线程表后缀. */
  public void clearTableSuffux() {
    String nativeTableName = this.getNativeTableName();
    JdbcContext.clearTableSuffux(nativeTableName);
  }

  /**
   * 获取JPA实体管理.
   *
   * @return EntityManager
   */
  protected EntityManager getEntityManager() {
    return this.tem.getEntityManager();
  }

  /** 租户. */
  protected String getTenant() {
    return this.tem.getTenant();
  }

  /** 租户标识. */
  public TenantIdentifier getTenantId() {
    return TenantUtil.getTenantIdentifier(this.tem.getTenant());
  }

  /** 当前实体是否包含租户标识. */
  public boolean hasTenantId() {
    return JpaTenantIdentifier.class.isAssignableFrom(entityClass);
  }

  /**
   * 获取数据源数据库类型，Oracle、Mysql、SqlServer等.
   */
  public synchronized DatabaseName getDataBaseName() {
    if (null != databaseName) {
      return databaseName;
    }
    try {
      Connection conn = this.tem.connection();
      if (conn == null) {
        return null;
      }
      String databaseProductName = conn.getMetaData().getDatabaseProductName();
      databaseName = DatabaseName.convertDatabaseName(databaseProductName);
    } catch (Exception ex) {
      log.warn(ex);
      return null;
    }
    return databaseName;
  }

  /**
   * 获取原生数据库表名称.
   */
  public String getNativeTableName() {
    Table t = entityClass.getAnnotation(Table.class);
    return t.name();
  }

  /**
   * 返回JPA-2级缓存API.
   */
  public Cache getCache() {
    return getEntityManager().getEntityManagerFactory().getCache();
  }

  /**
   * 设置查询缓存.
   */
  protected void setCacheable(Query query, boolean cacheable) {
    query.setHint("org.hibernate.cacheable", cacheable);
  }

  /** 业务主键查询. */
  public E findByNaturalId(Object naturalId) {
    if (null == naturalId) {
      return null;
    }
    return this.tem.getSessionImpl().bySimpleNaturalId(this.entityClass).load(naturalId);
  }

  /** 业务主键查询,带锁. */
  public E findByNaturalId(Object naturalId, LockOptions lockOptions) {
    if (null == naturalId) {
      return null;
    }
    return this.tem.getSessionImpl().bySimpleNaturalId(this.entityClass).with(lockOptions)
        .load(naturalId);
  }

  /**
   * 获取实体By Id ,如果不存在返回null.
   *
   * @param primaryKey 主键
   * @return 实体或null
   */
  public E find(F primaryKey) {
    if (null == primaryKey) {
      return null;
    }
    return (E) this.getEntityManager().find(entityClass, primaryKey);
  }

  /**
   * 获取实体By Id ,如果不存在返回null,detach实体游离，否则悲观写锁.
   *
   * @param primaryKey primaryKey 主键
   * @param detach 设置游离状态，否则悲观写锁.
   */
  public E find(F primaryKey, boolean detach) {
    if (detach) {
      E obj = find(primaryKey);
      if (obj != null) {
        this.getEntityManager().detach(obj);
      }
      return obj;
    } else {
      E obj = find(primaryKey, LockModeType.PESSIMISTIC_WRITE);
      return obj;
    }
  }

  /**
   * 获取实体By Id ,如果不存在返回null.
   *
   * @param primaryKey 主键
   * @param lockModeType 锁模式
   * @return 实体或null
   */
  public E find(F primaryKey, LockModeType lockModeType) {
    if (null == primaryKey) {
      return null;
    }
    return (E) this.getEntityManager().find(entityClass, primaryKey, lockModeType);
  }

  /**
   * 获取实体By Id ,如果不存在抛出异常javax.persistence.EntityNotFoundException.
   *
   * @param primaryKey 主键
   * @return 获取的结果
   * @throws EntityNotFoundException 找不到异常
   */
  public E getReference(F primaryKey) throws EntityNotFoundException {
    if (null == primaryKey) {
      return null;
    }
    return (E) this.getEntityManager().getReference(entityClass, primaryKey);
  }

  /**
   * 刷新实体.
   */
  public void refresh(E entity) {
    WebContext.TENANT.set(this.getTenant());
    this.getEntityManager().refresh(entity);
  }

  /**
   * 刷新实体,同时锁该条记录.
   *
   * @param entity 实体
   * @param lockModeType 锁模式
   */
  public void refresh(E entity, LockModeType lockModeType) {
    this.getEntityManager().refresh(entity, lockModeType);
  }

  /**
   * 持久到实体.
   *
   * @param entity 实体
   */
  public void persist(E entity) {
    // 插入数据，租户设置.
    if (this.hasTenantId()) {
      TenantUtil.persistPre(entity, this.getTenantId());
    }
    WebContext.TENANT.set(this.getTenant());
    this.getEntityManager().persist(entity);
  }

  /**
   * 修改实体.
   *
   * @param entity 实体
   */
  public void merge(E entity) {
    // 数据merge前,租户设置/检查.
    if (this.hasTenantId()) {
      TenantUtil.mergePre(entity, this.getTenantId());
    }
    WebContext.TENANT.set(this.getTenant());
    this.getEntityManager().merge(entity);
  }

  /** 实体改变为detach(游离)状态. */
  public void detach(E entity) {
    this.getEntityManager().detach(entity);
  }

  /**
   * 修改或插入实体Collection.
   *
   * @param cols 实体列表
   */
  public void save(Collection<E> cols) {
    TenantIdentifier tenantIdentifier = this.getTenantId();
    for (E entity : cols) {
      // 数据merge前,租户设置/检查.
      if (this.hasTenantId()) {
        TenantUtil.mergePre(entity, tenantIdentifier);
      }
      WebContext.TENANT.set(this.getTenant());
      this.getEntityManager().merge(entity);
    }
  }

  /**
   * 删除实体.
   */
  public void remove(E obj) {
    // 删除数据，租户检查.
    if (this.hasTenantId()) {
      TenantUtil.removePre(obj, this.getTenantId());
    }
    WebContext.TENANT.set(this.getTenant());
    this.getEntityManager().remove(obj);
  }

  /**
   * 逻辑/物理删除实体.
   */
  public void removeByStatus(E obj) {
    if (obj instanceof Delete) {
      // 逻辑删除.
      Delete entity = (Delete) obj;
      entity.setDeleted(true);
      entity.setDeleteBy(null);
      // 数据merge前,租户设置/检查.
      if (this.hasTenantId()) {
        TenantUtil.mergePre(obj, this.getTenantId());
      }
      WebContext.TENANT.set(this.getTenant());
      this.getEntityManager().merge(obj);
    } else {
      // 删除数据，租户检查.
      if (this.hasTenantId()) {
        TenantUtil.removePre(obj, this.getTenantId());
      }
      WebContext.TENANT.set(this.getTenant());
      // 物理删除.
      this.getEntityManager().remove(obj);
    }
  }

  /**
   * 删除Collection实体.
   */
  public int removeAll(Collection<E> cols) {
    TenantIdentifier tenantIdentifier = this.getTenantId();
    int i = 0;
    for (E entity : cols) {
      // 删除数据，租户检查.
      if (this.hasTenantId()) {
        TenantUtil.removePre(entity, tenantIdentifier);
      }
      WebContext.TENANT.set(this.getTenant());
      this.getEntityManager().remove(entity);
      i++;
    }
    return i;
  }

  /**
   * removeAll 方法替代.
   */
  @Deprecated
  public void deleteAll(Collection<E> cols) {
    removeAll(cols);
  }

  /**
   * 根据key删除实体/逻辑删除/Only By Id.
   */
  public E removeById(F primaryKey) {
    if (null == primaryKey) {
      return null;
    }
    E obj = (E) this.getEntityManager().find(entityClass, primaryKey);
    if (null != obj) {
      removeByStatus(obj);
    }
    return obj;
  }

  /**
   * 提交到数据库.
   */
  public void flush() {
    WebContext.TENANT.set(this.getTenant());
    this.getEntityManager().flush();
  }

  /**
   * 执行update delete sql操作.
   *
   * @param queryString 查询串
   * @param values 值
   * @return 影响的行
   */
  protected int executeUpdate(String queryString, Object... values) {
    Query query = this.getEntityManager().createQuery(queryString);
    for (int i = 1; i <= values.length; i++) {
      query.setParameter(i, values[i - 1]);
    }
    return query.executeUpdate();
  }

  /**
   * 执行update delete sql操作.
   *
   * @param queryString sql
   * @param params 参数
   * @return 影响的行
   */
  protected int executeUpdateByParams(String queryString, Map<String, ?> params) {
    Query query = this.getEntityManager().createQuery(queryString);
    if (null != params) {
      for (Entry<String, ?> entry : params.entrySet()) {
        query.setParameter(entry.getKey(), entry.getValue());
      }
    }
    return query.executeUpdate();
  }

  /**
   * 执行原生update delete sql操作.
   *
   * @param sql 原生SQL
   * @param values 值
   * @return 影响的行
   */
  protected int executeNativeUpdate(String sql, Object... values) {
    Query query = this.getEntityManager().createNativeQuery(sql);
    for (int i = 1; i <= values.length; i++) {
      query.setParameter(i, values[i - 1]);
    }
    return query.executeUpdate();
  }

  /**
   * 执行原生update delete sql操作.
   *
   * @param sql 原生SQL
   * @param params 参数
   * @return 影响的行
   */
  protected int executeNativeUpdateByParams(String sql, Map<String, ?> params) {
    Query query = this.getEntityManager().createQuery(sql);
    if (null != params) {
      for (Entry<String, ?> entry : params.entrySet()) {
        query.setParameter(entry.getKey(), entry.getValue());
      }
    }
    return query.executeUpdate();
  }

  /**
   * 判断该主建的值是否存在.
   *
   * @param primaryKey 主键
   */
  public boolean exists(F primaryKey) {
    return find(primaryKey) != null;
  }

  /**
   * 获取总行数.
   */
  public long getRowCount() {
    Query query = this.getEntityManager()
        .createQuery(String.format("select Count(s) from %s as s", entityClass.getSimpleName()));
    this.setCacheable(query, this.isCacheable());
    return (Long) query.getSingleResult();
  }

  /**
   * 获取某个字段的最大值.
   *
   * @param fieldName 字段名
   */
  public Object getMaxField(String fieldName) {
    Query query = this.getEntityManager().createQuery(
        String.format("select Max(s.%s) from %s as s", fieldName, entityClass.getSimpleName()));
    this.setCacheable(query, this.isCacheable());
    return query.getSingleResult();
  }

  /**
   * 获取某个字段的最小值.
   *
   * @param fieldName 字段名
   */
  public Object getMinField(String fieldName) {
    Query query = this.getEntityManager().createQuery(
        String.format("select Min(s.%s) from %s as s", fieldName, entityClass.getSimpleName()));
    this.setCacheable(query, this.isCacheable());
    return query.getSingleResult();
  }

  /**
   * 查询单个实体.
   *
   * @param queryString 查询串
   * @param values 参数值
   * @return 单个实体
   */
  @SuppressWarnings("unchecked")
  protected E findSingle(String queryString, Object... values) {
    Query query = this.getEntityManager().createQuery(queryString);
    for (int i = 1; i <= values.length; i++) {
      query.setParameter(i, values[i - 1]);
    }
    query.setFirstResult(0);
    query.setMaxResults(1);
    this.setCacheable(query, this.isCacheable());
    List<E> results = (List<E>) query.getResultList();
    if (results.size() > 0) {
      return results.get(0);
    }
    return null;
  }

  /**
   * 获取单个结果.
   *
   * @param queryString 查询串
   * @param values 参数
   * @return 查询结果
   */
  protected Object findSingleResult(String queryString, Object... values) {
    Query query = this.getEntityManager().createQuery(queryString);
    for (int i = 1; i <= values.length; i++) {
      query.setParameter(i, values[i - 1]);
    }
    this.setCacheable(query, this.isCacheable());
    return query.getSingleResult();
  }

  /**
   * 获取单个结果.
   *
   * @param queryString 查询串
   * @param params 参数
   * @return 查询结果
   */
  protected Object findSingleResultByParams(String queryString, Map<String, ?> params) {
    Query query = this.getEntityManager().createQuery(queryString);
    if (null != params) {
      for (Entry<String, ?> entry : params.entrySet()) {
        query.setParameter(entry.getKey(), entry.getValue());
      }
    }
    this.setCacheable(query, this.isCacheable());
    return query.getSingleResult();
  }

  /**
   * 查询带参数3.
   *
   * @param queryString 查询串
   * @param values 参数值
   * @return 查询结果
   */
  protected List<E> findList(String queryString, Object... values) {
    Query query = this.getEntityManager().createQuery(queryString);
    return findListBy(query, null, values);
  }

  /**
   * 参数查询5.
   *
   * @param queryString 查询串
   * @param resultClass 结果类型
   * @param values 参数数组
   * @return 查询结果
   */
  protected <T> List<T> findList(String queryString, Class<T> resultClass, Object... values) {
    Query query = this.getEntityManager().createQuery(queryString, resultClass);
    return findListBy(query, null, values);
  }

  /**
   * 查询从第几行开始，查询多少行.
   *
   * @param start 开始行数
   * @param count 查询行数
   * @param queryString 查询条件
   * @param values 参数
   */
  protected List<E> findList(Integer start, Integer count, String queryString, Object... values) {
    start = (start == null || start < 0) ? 0 : start;
    count = (count == null || count < 1) ? 5 : count;
    Query query = this.getEntityManager().createQuery(queryString);

    query.setFirstResult(start);
    query.setMaxResults(count);
    return findListBy(query, null, values);
  }

  @SuppressWarnings("unchecked")
  protected <T> List<T> findList(Query query, Class<T> transformerClass) {
    this.setCacheable(query, this.isCacheable());
    if (null != transformerClass && !transformerClass.equals(this.getEntityClass())) {
      DtoResultTransformer<T> transformer = new DtoResultTransformer<>(transformerClass);
      query.unwrap(org.hibernate.query.Query.class).setTupleTransformer(transformer)
          .setResultListTransformer(transformer);
    }
    return (List<T>) query.getResultList();
  }

  protected <T> List<T> findListBy(Query query, Class<T> transformerClass, Object... values) {
    for (int i = 1; i <= values.length; i++) {
      query.setParameter(i, values[i - 1]);
    }
    return findList(query, transformerClass);
  }

  /**
   * 查询通过命名参数4.
   *
   * @param queryString 查询串
   * @param params 参数Map
   * @return 查询结果
   */
  protected List<E> findListByParams(String queryString, Map<String, ?> params) {
    Query query = this.getEntityManager().createQuery(queryString);
    return findListByParams(query, null, params);
  }

  /**
   * 查询从第几行开始，查询多少行.
   *
   * @param start 开始行数
   * @param count 查询行数
   * @param queryString 查询条件
   * @param params 命名参数
   */
  protected List<E> findListByParams(Integer start, Integer count, String queryString,
      Map<String, ?> params) {
    start = (start == null || start < 0) ? 0 : start;
    count = (count == null || count < 1) ? 5 : count;
    Query query = this.getEntityManager().createQuery(queryString);
    query.setFirstResult(start);
    query.setMaxResults(count);
    return findListByParams(query, null, params);
  }

  protected <T> List<T> findListByParams(Query query, Class<T> transformerClass,
      Map<String, ?> params) {
    if (params != null) {
      for (Entry<String, ?> entry : params.entrySet()) {
        query.setParameter(entry.getKey(), entry.getValue());
      }
    }
    return findList(query, transformerClass);
  }

  /**
   * 参数查询5.
   *
   * @param queryString 查询串
   * @param transformerClass 结果类型
   * @param values 参数数组
   */
  protected <T> List<T> findDtoList(String queryString, Class<T> transformerClass,
      Object... values) {
    Query query = this.getEntityManager().createQuery(queryString);
    return findListBy(query, transformerClass, values);
  }

  /**
   * 查询通过命名参数5.
   *
   * @param queryString 查询串
   * @param transformerClass 结果转换类型
   * @param params 参数Map
   * @return 查询结果
   */
  protected <T> List<T> findDtoListByParams(String queryString, Class<T> transformerClass,
      Map<String, ?> params) {
    Query query = this.getEntityManager().createQuery(queryString);
    return findListByParams(query, transformerClass, params);
  }

  /**
   * 原生Transformer查询.
   */
  protected <T> List<T> findDtoNativeList(String sql, Class<T> transformerClass, Object... values) {
    Query query = this.getEntityManager().createNativeQuery(sql);
    return this.findListBy(query, transformerClass, values);
  }

  /**
   * 原生Transformer查询.
   */
  protected <T> List<T> findDtoNativeListByParams(String sql, Class<T> transformerClass,
      Map<String, ?> params) {
    Query query = this.getEntityManager().createNativeQuery(sql);
    return this.findListByParams(query, transformerClass, params);
  }

  /**
   * 命名查询2.
   *
   * @param nameString 命名
   * @param values 参数值
   * @return 查询结果
   */
  protected List<E> findNamedQuery(String nameString, Object... values) {
    Query query = this.getEntityManager().createNamedQuery(nameString);
    return this.findListBy(query, null, values);
  }

  /**
   * 命名查询3.
   *
   * @param nameString 命名查询
   * @param params 参数Map
   * @return 查询结果
   */
  protected List<E> findNamedQueryByParams(String nameString, Map<String, ?> params) {
    Query query = this.getEntityManager().createNamedQuery(nameString);
    return this.findListByParams(query, null, params);
  }

  /**
   * Dto 查询数据.
   */
  @SuppressWarnings("unchecked")
  protected <T> T findDtoSingle(String queryStr, Class<T> dtoClass, Map<String, ?> params) {
    Query query = this.getEntityManager().createQuery(queryStr);
    if (params != null) {
      for (Entry<String, ?> entry : params.entrySet()) {
        query.setParameter(entry.getKey(), entry.getValue());
      }
    }
    query.setFirstResult(0);
    query.setMaxResults(1);
    this.setCacheable(query, this.isCacheable());
    DtoResultTransformer<T> transformer = new DtoResultTransformer<>(dtoClass);
    query.unwrap(org.hibernate.query.Query.class).setTupleTransformer(transformer)
        .setResultListTransformer(transformer);
    // 查询结果总数
    List<T> results = (List<T>) query.getResultList();
    if (results.size() > 0) {
      return results.get(0);
    }
    return null;
  }

  /**
   * 翻页查询.
   *
   * @param curPage 当前页码
   * @param pageSize 页尺寸
   * @param queryStr 查询条件
   * @param params 参数
   */
  protected DtoPage<E, DtoTotal> findDtoPageByParams(int curPage, int pageSize, String queryStr,
      Map<String, ?> params) {
    String totalQueryStr = createQueryCount(queryStr);
    return findDtoPageByParams(curPage, pageSize, queryStr, totalQueryStr, getEntityClass(),
        DtoTotal.class, params);
  }

  protected <T extends DtoTotal> DtoPage<E, T> findDtoPageByParams(int curPage, int pageSize,
      String queryStr, String totalQueryStr, Class<T> totalClass, Map<String, ?> params) {
    return findDtoPageByParams(curPage, pageSize, queryStr, totalQueryStr, getEntityClass(),
        totalClass, params);
  }

  /**
   * DTO 查询.
   *
   * @param curPage 当前页码
   * @param pageSize 页尺寸
   * @param queryStr 查询SQL
   * @param totalQueryStr 统计SQL
   * @param totalClass 结果Class
   * @param params 查询条件
   * @return DtoPage
   */
  protected <H extends Serializable, T extends DtoTotal> DtoPage<H, T> findDtoPageByParams(
      int curPage, int pageSize, String queryStr, String totalQueryStr, Class<H> clazz,
      Class<T> totalClass, Map<String, ?> params) {
    Query query = this.getEntityManager().createQuery(queryStr);
    Query queryCount = this.getEntityManager().createQuery(totalQueryStr);
    if (params != null) {
      for (Entry<String, ?> entry : params.entrySet()) {
        query.setParameter(entry.getKey(), entry.getValue());
        queryCount.setParameter(entry.getKey(), entry.getValue());
      }
    }

    return findDtoPage(curPage, pageSize, query, queryCount, clazz, totalClass);
  }

  protected <T extends DtoTotal> DtoPage<E, T> findDtoPage(int curPage, int pageSize, Query query,
      Query totalQuery, Class<T> totalClass) {
    return findDtoPage(curPage, pageSize, query, totalQuery, getEntityClass(), totalClass);
  }

  /**
   * 分页查询.
   *
   * @param <T> 统计Class
   * @param curPage 当前页码
   * @param pageSize 页尺寸
   * @param query 查询SQL
   * @param totalQuery 统计SQL
   * @param clazz 实体
   * @param totalClass 统计
   */
  @SuppressWarnings("unchecked")
  protected <H extends Serializable, T extends DtoTotal> DtoPage<H, T> findDtoPage(int curPage,
      int pageSize, Query query, Query totalQuery, Class<H> clazz, Class<T> totalClass) {
    curPage = curPage < 1 ? 1 : curPage;
    pageSize = pageSize < 1 ? 10 : pageSize;
    // 缓存配置
    this.setCacheable(query, this.isCacheable());
    this.setCacheable(totalQuery, this.isCacheable());

    // 查询结果映射
    if (!clazz.equals(getEntityClass())) {
      DtoResultTransformer<H> transformer = new DtoResultTransformer<>(clazz);
      query.unwrap(org.hibernate.query.Query.class).setTupleTransformer(transformer)
          .setResultListTransformer(transformer);
    }

    DtoResultTransformer<T> totalTransformer = new DtoResultTransformer<>(totalClass);
    totalQuery.unwrap(org.hibernate.query.Query.class).setTupleTransformer(totalTransformer)
        .setResultListTransformer(totalTransformer);

    // 查询结果总数
    T obj = (T) totalQuery.getSingleResult();
    int totalSize = obj.getTotalSize();
    // 加入判断
    if (totalSize != 0) {
      // 调整页码
      int pageCount = (totalSize / pageSize) + ((totalSize % pageSize) == 0 ? 0 : 1);
      curPage = curPage > pageCount ? pageCount : curPage;

      query.setFirstResult((curPage - 1) * pageSize);
      query.setMaxResults(pageSize);
    }
    return new DtoPageImpl<>(query.getResultList(), curPage, pageSize, obj);
  }

  /**
   * 原生SQL分页查询.
   *
   * @param <T> 统计Class
   * @param curPage 当前页码
   * @param pageSize 页尺寸
   * @param queryStr 查询SQL
   * @param totalQueryStr 统计SQL
   * @param clazz 实体
   * @param totalClass 统计
   */
  protected <H extends Serializable, T extends DtoTotal> DtoPage<H, T> findNativePageByParams(
      int curPage, int pageSize, String queryStr, String totalQueryStr, Class<H> clazz,
      Class<T> totalClass, Map<String, ?> params) {
    Query query = this.getEntityManager().createNativeQuery(queryStr);
    Query queryCount = this.getEntityManager().createNativeQuery(totalQueryStr);
    if (params != null) {
      for (Entry<String, ?> entry : params.entrySet()) {
        query.setParameter(entry.getKey(), entry.getValue());
        queryCount.setParameter(entry.getKey(), entry.getValue());
      }
    }
    return findDtoPage(curPage, pageSize, query, queryCount, clazz, totalClass);
  }

  /**
   * 获取行数的sql.
   *
   * @param queryString SQL
   * @return SQL 求行数
   */
  protected String createQueryCount(String queryString) {
    // first Form FORM 等替换为 from
    queryString = Pattern.compile("\\bfrom\\b", Pattern.CASE_INSENSITIVE).matcher(queryString)
        .replaceFirst("from");
    String queryCountString;
    if (queryString.trim().startsWith("from")) {
      queryCountString = String.format("select Count(*) as recordCount %s ", queryString);
    } else {
      String from = queryString.substring(queryString.indexOf("from "));
      String pro =
          queryString.substring(queryString.indexOf("select") + 6, queryString.indexOf("from "));
      queryCountString = String.format("select Count(%s) as recordCount %s ", pro.trim(), from);
    }

    return clearSqlOrderBy(queryCountString);
  }

  /**
   * 清楚SQL中的order by.
   *
   * @param queryString 查询SQL
   * @return 去掉Order by
   */
  protected String clearSqlOrderBy(String queryString) {
    // all Order By,Order by 等 替换为order by
    queryString = Pattern.compile("\\border\\s+by\\b", Pattern.CASE_INSENSITIVE)
        .matcher(queryString).replaceAll("order by");
    int index = queryString.toLowerCase().lastIndexOf("order by");
    if (index > 0) {
      queryString = queryString.substring(0, index - 1);
    }
    return queryString;
  }

  /**
   * 获取行数的sql.
   *
   * @param queryString 查询SQL
   * @return 求行数
   */
  protected String createNativeQueryCount(String queryString) {
    return String.format("select count (*) from ( %s ) abc ", queryString);
  }



}
