package com.gitee.jmash.core.orm.jpa;

import com.gitee.jmash.core.utils.DbSecretUtil;
import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

/** JPA 数据库加解密转换. */
@Converter
public class CryptoConverter implements AttributeConverter<String, String> {

  @Override
  public String convertToDatabaseColumn(String attribute) {
    return DbSecretUtil.encrypt(attribute);
  }

  @Override
  public String convertToEntityAttribute(String dbData) {
    return DbSecretUtil.decrypt(dbData);
  }

}
