package com.gitee.jmash.core.orm.jpa;

/**
 * 持久层的异常DaoException.
 *
 * @author cgd email:cgd@crenjoy.com
 * @version V1.0
 */
public class DaoException extends jakarta.persistence.PersistenceException {
  private static final long serialVersionUID = 1L;

  public DaoException() {
    super();
  }

  public DaoException(String msg) {
    super(msg);
  }

  public DaoException(String msg, Throwable cause) {
    super(msg, cause);
  }

  public DaoException(Throwable cause) {
    super(cause);
  }
}
