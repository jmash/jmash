
package com.gitee.jmash.core.orm.jpa;

import com.gitee.jmash.common.utils.ConfigSecretUtil;

/**
 * JDBC DataSource Props (因flowable初始化原因，不采用@ConfigProperties.).
 *
 * @author CGD
 *
 */
public class DataSourceProps {
  
  private String url = "jdbc:mariadb://127.0.0.1:3306/jmash_core?characterEncoding=utf8";

  private String username = "jmash";

  private String password = "*";
  // 是否解码
  private Boolean isdecode = false;

  private String validationQuery = "select 1 ";
  private int initialSize = 10;
  private int minIdle = 10;
  private int maxActive = 100;

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  /** 获取密钥. */
  public String getPassword() {
    if (isdecode) {
      return password;
    } else {
      password = ConfigSecretUtil.decrypt(password);
      isdecode = true;
      return password;
    }
  }

  public void setPassword(String password) {
    this.password = password;
    this.isdecode = false;
  }

  public String getValidationQuery() {
    return validationQuery;
  }

  public void setValidationQuery(String validationQuery) {
    this.validationQuery = validationQuery;
  }

  public int getInitialSize() {
    return initialSize;
  }

  public void setInitialSize(int initialSize) {
    this.initialSize = initialSize;
  }

  public int getMinIdle() {
    return minIdle;
  }

  public void setMinIdle(int minIdle) {
    this.minIdle = minIdle;
  }

  public int getMaxActive() {
    return maxActive;
  }

  public void setMaxActive(int maxActive) {
    this.maxActive = maxActive;
  }

}
