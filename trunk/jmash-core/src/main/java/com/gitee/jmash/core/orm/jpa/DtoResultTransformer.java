
package com.gitee.jmash.core.orm.jpa;

import com.crenjoy.proto.beanutils.ProtoBeanUtils;
import jakarta.persistence.Column;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.query.ResultListTransformer;
import org.hibernate.query.TupleTransformer;

/**
 * 结果转换.
 *
 * @author CGD
 *
 */
public class DtoResultTransformer<T> implements ResultListTransformer<T>, TupleTransformer<T> {

  private final Class<T> resultClass;

  private Map<String, String> fields = new HashMap<String, String>();

  /**
   * JPA Result Transformer.
   *
   * @param resultClass 结果类
   */
  public DtoResultTransformer(final Class<T> resultClass) {
    this.resultClass = resultClass;

    Method[] methods = this.resultClass.getMethods();
    for (int i = 0; i < methods.length; i++) {
      Column column = methods[i].getAnnotation(Column.class);
      String name = methods[i].getName();
      if (null != column) {
        if (name.startsWith("get") && name.length() > 3) {
          fields.put(column.name().toLowerCase(), StringUtils.uncapitalize(name.substring(3)));
        } else if (name.startsWith("is") && name.length() > 2) {
          fields.put(column.name().toLowerCase(), StringUtils.uncapitalize(name.substring(2)));
        }
      }
    }
  }

  @Override
  public T transformTuple(Object[] tuple, String[] aliases) {
    T result = null;
    try {
      result = this.resultClass.getDeclaredConstructor().newInstance();
      for (int i = 0; i < aliases.length; i++) {
        if (null != tuple[i] && null != aliases[i]) {
          try {
            if (fields.containsKey(aliases[i].toLowerCase())) {
              String filedName = fields.get(aliases[i].toLowerCase());
              ProtoBeanUtils.setProperty(result, filedName, tuple[i]);
            } else {
              ProtoBeanUtils.setProperty(result, aliases[i], tuple[i]);
            }
          } catch (Exception e) {
            throw new DaoException(this.resultClass.getName() + " 无字段 " + aliases[i], e);
          }
        }
      }
    } catch (Exception e) {
      throw new DaoException("Could not instantiate resultclass: " + this.resultClass.getName(), e);
    }

    return result;
  }

  @Override
  public List<T> transformList(List<T> list) {
    return list;
  }
}
