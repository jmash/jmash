package com.gitee.jmash.core.orm.jpa;

import com.gitee.jmash.core.orm.jpa.entity.OrderBy;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 排序持久层基类（仅支持JPA实现）基于范型的实现.
 *
 * @param <E> 表实体
 * @param <F> 实体的主键
 */
public class OrderBaseDao<E extends OrderBy<F>, F extends Serializable> extends BaseDao<E, F> {


  public OrderBaseDao() {
    super();
  }

  public OrderBaseDao(TenantEntityManager tem) {
    super(tem);
  }

  /**
   * 获取最大排序值.
   */
  public int getMaxOrderBy(Map<String, ?> params) {
    StringBuilder sql = new StringBuilder(
        String.format(" select MAX(s.orderBy) from %s s where 1=1 ", entityClass.getSimpleName()));
    for (String param : params.keySet()) {
      sql.append(String.format(" and s.%s = :%s ", param, param));
    }
    Object orderBy = this.findSingleResultByParams(sql.toString(), params);
    return (null == orderBy) ? 1 : Integer.parseInt(orderBy.toString()) + 1;
  }

  /**
   * 查询排序列表.
   *
   * @param moveUp 上移/下移
   * @param entity 实体
   * @param params 查询参数
   */
  public List<E> findListByOrderBy(boolean moveUp, E entity, Map<String, ?> params) {
    StringBuilder sql = new StringBuilder(
        String.format("select s from %s s where  1=1 ", entityClass.getSimpleName()));
    for (String param : params.keySet()) {
      sql.append(String.format(" and s.%s = :%s ", param, param));
    }
    if (moveUp) {
      sql.append(" and s.orderBy<=:orderBy order by s.orderBy desc limit 2");

    } else {
      sql.append(" and s.orderBy>=:orderBy order by s.orderBy asc limit 2");
    }
    Map<String, Object> newParams = new HashMap<String, Object>(params);
    newParams.put("orderBy", entity.getOrderBy());
    return this.findListByParams(sql.toString(), newParams);
  }

  /** 排序. */
  public boolean moveOrderBy(boolean moveUp, E entity, Map<String, ?> params) {
    List<E> entityList = findListByOrderBy(moveUp, entity, params);
    if (entityList.size() < 2) {
      String msg = "";
      if (moveUp) {
        msg = "当前已经是第一条，无法向上移动！";
      } else {
        msg = "当前已经是最后一条，无法向下移动！";
      }
      throw new UnsupportedOperationException(msg);
    } else {
      int orderBy1 = entityList.get(1).getOrderBy();
      int orderBy2 = entityList.get(0).getOrderBy();

      if (orderBy1 == orderBy2 && moveUp) {
        orderBy1 = orderBy2 + 1;
      }
      if (orderBy1 == orderBy2 && !moveUp) {
        orderBy2 = orderBy1 + 1;
      }

      E entryEntity1 = entityList.get(1);
      entryEntity1.setOrderBy(orderBy2);
      this.merge(entryEntity1);

      E entryEntity2 = entityList.get(0);
      entryEntity2.setOrderBy(orderBy1);
      this.merge(entryEntity2);
    }
    return true;
  }

}
