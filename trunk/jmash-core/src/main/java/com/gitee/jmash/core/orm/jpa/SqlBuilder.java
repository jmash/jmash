
package com.gitee.jmash.core.orm.jpa;

import java.util.Map;

/** SQL Builder . */
public class SqlBuilder {

  private StringBuilder  sql;

  private Map<String, Object> params;

  private String orderSql;

  public static SqlBuilder build() {
    return new SqlBuilder();
  }

  protected SqlBuilder() {
  }

  public String getQuerySql(String select) {
    return select + sql.toString() + orderSql;
  }

  public String getTotalSql(String select) {
    return select + sql.toString();
  }

  public SqlBuilder setSql(StringBuilder sql) {
    this.sql = sql;
    return this;
  }

  public SqlBuilder setParams(Map<String, Object> params) {
    this.params = params;
    return this;
  }

  public SqlBuilder setOrderSql(String orderSql) {
    this.orderSql = orderSql;
    return this;
  }

  public StringBuilder getSql() {
    return sql;
  }

  public Map<String, Object> getParams() {
    return params;
  }

  public String getOrderSql() {
    return orderSql;
  }

}
