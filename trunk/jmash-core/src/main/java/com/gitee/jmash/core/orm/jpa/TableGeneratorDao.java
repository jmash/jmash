package com.gitee.jmash.core.orm.jpa;

import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;


/**
 * 表序列DAO.
 *
 * @author cgd
 * 
 */
public class TableGeneratorDao {

  /** JPA 实体管理. */
  private TenantEntityManager tem;

  /**
   * 注入JPA实体管理.
   *
   * @param entityManager 实体管理
   */
  public void setEntityManager(TenantEntityManager entityManager) {
    this.tem = entityManager;
  }

  /**
   * 获取JPA实体管理.
   *
   * @return EntityManager
   */
  public EntityManager getEntityManager() {
    return this.tem.getEntityManager();
  }

  public TableGeneratorDao() {
    super();
  }

  public TableGeneratorDao(TenantEntityManager tem) {
    super();
    this.tem = tem;
  }

  static Object lock = new Object();

  // 缓存
  static ConcurrentHashMap<String, Integer[]> caches = new ConcurrentHashMap<String, Integer[]>();

  /**
   * 获取序列值.
   *
   * @param name 序列名称
   * @param initialValue 初始值
   * @param allocationSize 步数
   * @return 序列值
   */
  public synchronized int createValue(String name, int initialValue, int allocationSize) {
    synchronized (lock) {
      if (caches.containsKey(name)) {
        Integer[] value = caches.get(name);
        if (value[0].compareTo(value[1]) < 0) {
          caches.put(name, new Integer[] {value[0] + 1, value[1]});
          return value[0];
        } else {
          caches.remove(name);
        }
      }

      int value = createValue(name, initialValue);
      updateNextValue(name, value + allocationSize);
      if (allocationSize > 1) {
        caches.put(name, new Integer[] {value + 1, value + allocationSize});
      } else {
        caches.remove(name);
      }
      return value;
    }
  }

  /**
   * 获取数据库数据或是初始值.
   */
  protected int createValue(String name, int initialValue) {
    // 悲观锁 select ... for update 锁行 支持高并发 Mysql (InnoDB) Oracle 不支持SQL Server
    String sql =
        "select s.next_val from hibernate_sequences s where s.sequence_name=?1 for update   ";
    Query query = this.getEntityManager().createNativeQuery(sql);
    // 悲观锁，Mysql InnoDB 环境 尝试多线程 无效
    // query.setLockMode(LockModeType.PESSIMISTIC_WRITE);
    query.setParameter(1, name);
    List<?> objs = query.getResultList();
    if (objs.size() == 0) {
      insertValue(name, initialValue);
      return initialValue;
    } else {
      Integer value = Integer.valueOf(objs.get(0).toString());
      return value;
    }
  }

  protected int insertValue(String name, int value) {
    String sql = "insert hibernate_sequences (next_val,sequence_name) values(?1,?2)";
    Query query = this.getEntityManager().createNativeQuery(sql);
    query.setParameter(1, value);
    query.setParameter(2, name);
    return query.executeUpdate();
  }

  /**
   * 更新到下一个序列值.
   */
  protected int updateNextValue(String name, int nextValue) {
    String sql = "update hibernate_sequences set  next_val=?1 where sequence_name=?2 ";
    Query query = this.getEntityManager().createNativeQuery(sql);
    query.setParameter(1, nextValue);
    query.setParameter(2, name);
    return query.executeUpdate();
  }

}
