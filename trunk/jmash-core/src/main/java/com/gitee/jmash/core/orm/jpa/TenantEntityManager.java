package com.gitee.jmash.core.orm.jpa;

import com.gitee.jmash.core.jaxrs.ParamsValidationException;
import com.gitee.jmash.core.orm.cdi.JpaUtil;
import com.gitee.jmash.core.orm.jdbc.ConnectionAccess;
import com.gitee.jmash.core.orm.jdbc.HibernateJdbcConnectionAccess;
import com.gitee.jmash.core.orm.jdbc.JdbcTemplate;
import com.gitee.jmash.core.utils.TenantUtil;
import jakarta.persistence.EntityManager;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.engine.spi.SessionImplementor;

/**
 * 租户 JPA 实体管理,降低使用难度.
 *
 * @author cgd
 */
public class TenantEntityManager {

  private static Log log = LogFactory.getLog(TenantEntityManager.class);

  /** JPA 实体管理. */
  private EntityManager entityManager;
  /** JPA UNIT NAME. */
  private String unitName;
  /** 租户. */
  private String tenant;
  /** 是否写数据源. */
  private boolean write;

  /** 设置EntityManager. */
  public void setEntityManager(EntityManager entityManager, boolean write) {
    this.entityManager = entityManager;
    // JPA注入设置.
    Map<String, Object> props = entityManager.getProperties();
    unitName = (String) props.get(JpaUtil.UNIT_NAME);
    this.write = write;
  }

  /** 事务设置，不能调整unitName. */
  public void setEntityManager(EntityManager entityManager) {
    this.entityManager = entityManager;
    Map<String, Object> props = entityManager.getProperties();
    String tranUnitName = (String) props.get(JpaUtil.UNIT_NAME);
    if (StringUtils.equals(this.unitName, tranUnitName)) {
      throw new RuntimeException("跨模块，不能使用同一个本地事务,事务模块:" + tranUnitName + " 当前模块：" + this.unitName);
    }
  }

  /**
   * 设置多租户数据源.
   *
   * @param tenant 租户名称
   */
  public void setTenant(String tenant) {
    if (StringUtils.isBlank(tenant)) {
      throw new ParamsValidationException("tenant", "租户Tenant,不能为空");
    }
    this.tenant = tenant;
    this.entityManager.close();
    this.entityManager = JpaUtil.getTenantEntityManager(unitName, tenant, this.write);
  }

  /** 获取JDBCTemplate . */
  public JdbcTemplate getJdbcTemplate() {
    SessionImplementor session = getSessionImpl();
    if (null != session) {
      ConnectionAccess ac = new HibernateJdbcConnectionAccess(session.getJdbcConnectionAccess());
      return new JdbcTemplate(ac, true);
    }
    return null;
  }

  /**
   * 获取JPA 数据源,如果获取不到返回null.
   *
   * @throws SQLException 获取连接异常.
   */
  public Connection connection() throws SQLException {
    SessionImplementor session = getSessionImpl();
    if (null != session) {
      return session.getJdbcConnectionAccess().obtainConnection();
    }
    return null;
  }

  /**
   * 获取Hibernate SessionImplementor.
   */
  public SessionImplementor getSessionImpl() {
    if (this.getEntityManager().getDelegate() instanceof SessionImplementor) {
      SessionImplementor session = this.getEntityManager().unwrap(SessionImplementor.class);
      return session;
    }
    log.warn("JPA EntityManager delegate is not hibernate ,not gain  Connection ");
    return null;
  }

  public EntityManager getEntityManager() {
    return entityManager;
  }

  public String getTenant() {
    return tenant;
  }

  /** 租户ID. */
  public String getTenantId() {
    if (TenantUtil.hasTenantIdentifier(tenant)) {
      return TenantUtil.getTenantIdentifierStr(tenant);
    }
    return StringUtils.EMPTY;
  }

  /** 租户Name. */
  public String getTenantName() {
    return TenantUtil.getTenantName(this.tenant);
  }

  public void setTenantOnly(String tenant) {
    this.tenant = tenant;
  }

  public String getUnitName() {
    return unitName;
  }

}
