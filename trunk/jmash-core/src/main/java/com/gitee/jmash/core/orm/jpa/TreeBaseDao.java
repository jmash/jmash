package com.gitee.jmash.core.orm.jpa;

import com.gitee.jmash.common.utils.UUIDUtil;
import com.gitee.jmash.core.orm.jpa.entity.TreeEntity;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;


/**
 * 树型持久层基类（仅支持JPA实现）基于范型的实现.
 *
 * @param <E> 表实体
 * @param <F> 实体的主键
 */
public class TreeBaseDao<E extends TreeEntity<F>, F extends Serializable> extends BaseDao<E, F> {

  public TreeBaseDao() {
    super();
  }

  public TreeBaseDao(TenantEntityManager tem) {
    super(tem);
  }


  /** 获取空ID. **/
  @SuppressWarnings("unchecked")
  public F getEmptyId() {
    if (UUID.class.equals(idClass)) {
      return (F) UUIDUtil.emptyUUID();
    } else if (Long.class.equals(idClass)) {
      return (F) Long.valueOf(0L);
    } else if (Integer.class.equals(idClass)) {
      return (F) Integer.valueOf(0);
    } else {
      return null;
    }
  }

  /** Insert 设置父ID、深度和排序. **/
  public void insertTree(E entity, final F parentId, Map<String, ?> params) {
    // 设置深度
    if (parentId == null || parentId.equals(getEmptyId())) {
      entity.setParentId(getEmptyId());
      entity.setDepth(1);
    } else {
      E parent = find(entity.getParentId());
      entity.setDepth(parent.getDepth() + 1);
    }
    // 设置排序
    Integer orderBy = getMaxOrderBy(entity.getParentId(), params);
    entity.setOrderBy(orderBy);
  }

  /** update 设置父ID、深度和排序. */
  public void updateTree(E entity, F oldParentId, F parentId, Map<String, ?> params) {
    // 设置为原父ID.
    entity.setParentId(oldParentId);
    // 全空情况不处理
    if ((parentId == null || parentId.equals(getEmptyId()))
        && (entity.getParentId() == null || entity.getParentId().equals(getEmptyId()))) {
      return;
    }
    // parentId不空的情况
    if (parentId != null && parentId.equals(entity.getParentId())) {
      return;
    }
    // entity.getParentId() 不空的情况
    if (entity.getParentId() != null && entity.getParentId().equals(parentId)) {
      return;
    }
    // 父ID发生变更，获取最新父ID和深度
    F newParentId = parentId == null ? getEmptyId() : parentId;
    int newDepth = 1;
    if (!(parentId == null || parentId.equals(getEmptyId()))) {
      E parent = find(newParentId);
      newDepth = parent.getDepth() + 1;
    }
    // 调整深度
    if (!entity.getDepth().equals(newDepth)) {
      entity.setDepth(newDepth);
      adjustDepthByParent(entity, params);
    }
    // 修改父ID和排序
    entity.setParentId(newParentId);
    Integer orderBy = getMaxOrderBy(newParentId, params);
    entity.setOrderBy(orderBy);
  }

  /** 调整孩子深度. */
  protected void adjustDepthByParent(E parent, Map<String, ?> params) {
    List<E> childList = findByParent(parent.getEntityPk(), params);
    if (childList.size() == 0) {
      return;
    }
    for (E child : childList) {
      child.setDepth(parent.getDepth() + 1);
      this.merge(child);
      adjustDepthByParent(child, params);
    }
  }

  /** 根据父ID查询子实体. */
  public List<E> findByParent(F parentId, Map<String, ?> params) {
    SqlBuilder sqlBuilder = findByParentSql(parentId, params);
    String query = sqlBuilder.getQuerySql("select s ");
    return this.findListByParams(query, sqlBuilder.getParams());
  }

  /** 根据父ID查询子实体数量. */
  public Long findCountByParent(F parentId, Map<String, ?> params) {
    SqlBuilder sqlBuilder = findByParentSql(parentId, params);
    String query = sqlBuilder.getQuerySql("select Count(s) ");
    Object count = this.findSingleResultByParams(query, sqlBuilder.getParams());
    return Long.parseLong(count.toString());
  }

  /** 根据父ID查询子实体. */
  protected SqlBuilder findByParentSql(F parentId, Map<String, ?> params) {
    StringBuilder sql = new StringBuilder(
        String.format(" from %s s where s.parentId=:parentId ", entityClass.getSimpleName()));
    for (String param : params.keySet()) {
      sql.append(String.format(" and s.%s = :%s ", param, param));
    }
    Map<String, Object> newParams = new HashMap<String, Object>(params);
    newParams.put("parentId", parentId);  
    String orderSql = " order by s.depth,s.orderBy";
    return SqlBuilder.build().setSql(sql).setParams(newParams).setOrderSql(orderSql);
  }

  /**
   * 根据父级获取最大排序值.
   */
  public int getMaxOrderBy(F parentId, Map<String, ?> params) {
    StringBuilder sql = new StringBuilder(
        String.format(" select MAX(s.orderBy) from %s s where s.parentId=:parentId ",
            entityClass.getSimpleName()));
    for (String param : params.keySet()) {
      sql.append(String.format(" and s.%s = :%s ", param, param));
    }
    Map<String, Object> newParams = new HashMap<String, Object>(params);
    newParams.put("parentId", parentId);   
    Object orderBy = this.findSingleResultByParams(sql.toString(), newParams);
    return (null == orderBy) ? 1 : Integer.parseInt(orderBy.toString()) + 1;
  }

  /**
   * 查询排序列表.
   *
   * @param moveUp 上移/下移
   * @param entity 实体
   * @param params 查询参数
   */
  public List<E> findListByOrderBy(boolean moveUp, E entity, Map<String, ?> params) {
    StringBuilder sql = new StringBuilder(
        String.format("select s from %s s where s.parentId=:parentId and s.depth =:depth ",
            entityClass.getSimpleName()));
    for (String param : params.keySet()) {
      sql.append(String.format(" and s.%s = :%s ", param, param));
    }
    Map<String, Object> newParams = new HashMap<String, Object>(params);
    newParams.put("parentId", entity.getParentId());
    newParams.put("depth", entity.getDepth());
    newParams.put("orderBy", entity.getOrderBy());
    if (moveUp) {
      sql.append(" and s.orderBy<=:orderBy order by s.orderBy desc limit 2");

    } else {
      sql.append(" and s.orderBy>=:orderBy order by s.orderBy asc limit 2");
    }
    return this.findListByParams(sql.toString(), newParams);
  }



  /** 排序. */
  public boolean moveOrderBy(boolean moveUp, E entity, Map<String, ?> params) {
    List<E> entityList = findListByOrderBy(moveUp, entity, params);
    if (entityList.size() < 2) {
      String msg = "";
      if (moveUp) {
        msg = "当前已经是第一条，无法向上移动！";
      } else {
        msg = "当前已经是最后一条，无法向下移动！";
      }
      throw new UnsupportedOperationException(msg);
    } else {
      int orderBy1 = entityList.get(1).getOrderBy();
      int orderBy2 = entityList.get(0).getOrderBy();

      if (orderBy1 == orderBy2 && moveUp) {
        orderBy1 = orderBy2 + 1;
      }
      if (orderBy1 == orderBy2 && !moveUp) {
        orderBy2 = orderBy1 + 1;
      }

      E entryEntity1 = entityList.get(1);
      entryEntity1.setOrderBy(orderBy2);
      this.merge(entryEntity1);

      E entryEntity2 = entityList.get(0);
      entryEntity2.setOrderBy(orderBy1);
      this.merge(entryEntity2);
    }
    return true;
  }
}
