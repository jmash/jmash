
package com.gitee.jmash.core.orm.jpa.entity;

import com.gitee.jmash.common.grpc.GrpcContext;
import com.gitee.jmash.common.jaxrs.WebContext;
import com.gitee.jmash.common.utils.UUIDUtil;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import java.time.LocalDateTime;
import java.util.UUID;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.microprofile.jwt.JsonWebToken;

/** 基本实体监听. */
@MappedSuperclass
public abstract class BaiscEntityListener {

  private static Log log = LogFactory.getLog(BaiscEntityListener.class);

  /** 插入前. */
  @PrePersist
  public void onPersist() {
    Object entity = this;
    if (entity instanceof Create) {
      Create create = (Create) entity;
      if (null == create.getCreateBy()) {
        create.setCreateBy(BaiscEntityListener.getPrincipalName());
      }
      create.setCreateTime(LocalDateTime.now());
    }
    if (entity instanceof Update) {
      Update update = (Update) entity;
      update.setUpdateTime(LocalDateTime.now());
    }
  }

  /** 更新前. */
  @PreUpdate
  public void onUpdate() {
    Object entity = this;
    if (entity instanceof Update) {
      // 删除后不记录变更.
      if (!(entity instanceof Delete) || !((Delete) entity).getDeleted()) {
        Update update = (Update) entity;
        update.setUpdateBy(BaiscEntityListener.getPrincipalName());
        update.setUpdateTime(LocalDateTime.now());
      }
    }
    if (entity instanceof Delete) {
      // 逻辑删除.
      Delete delete = (Delete) entity;
      if (delete.getDeleted() && null == delete.getDeleteBy()) {
        delete.setDeleteBy(BaiscEntityListener.getPrincipalName());
        delete.setDeleteTime(LocalDateTime.now());
      }
    }
  }

  /** 获取Login User ID. */
  public static UUID getPrincipalName() {
    JsonWebToken principal = WebContext.USER_TOKEN.get();
    if (null == principal) {
      principal = GrpcContext.USER_TOKEN.get();
    }
    try {
      if (null != principal) {
        return UUIDUtil.fromString(principal.getName());
      }
      log.warn("User Not Login,Insert/Update Data.");
    } catch (Exception ex) {
      if (null != principal) {
        log.warn("Principal Name Not UUID:" + principal.getName());
      }
    }
    return UUIDUtil.emptyUUID();
  }

}
