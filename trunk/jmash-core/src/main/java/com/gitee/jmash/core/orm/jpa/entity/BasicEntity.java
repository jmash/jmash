
package com.gitee.jmash.core.orm.jpa.entity;

import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.Version;
import jakarta.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

/** 通用继承实体 . */
@MappedSuperclass
public abstract class BasicEntity<F> extends BaiscEntityListener
    implements Serializable, Create, Update, PrimaryKey<F> {

  private static final long serialVersionUID = 1L;

  /** 乐观锁. */
  protected Integer version = 0;
  /** 创建人. */
  protected UUID createBy;
  /** 创建时间. */
  protected LocalDateTime createTime;
  /** 更新人. */
  protected UUID updateBy;
  /** 更新时间. */
  protected LocalDateTime updateTime;

  /**
   * 乐观锁(Version).
   */
  @Column(name = "version_")
  @Version
  public Integer getVersion() {
    return version;
  }

  /**
   * 乐观锁(Version).
   */
  public void setVersion(Integer version) {
    this.version = version;
  }

  /**
   * 创建人(CreateBy).
   */
  @Column(name = "create_by", columnDefinition = "BINARY(16)")
  @NotNull
  public UUID getCreateBy() {
    return createBy;
  }

  /**
   * 创建人(CreateBy).
   */
  public void setCreateBy(UUID createBy) {
    this.createBy = createBy;
  }

  /**
   * 创建时间(CreateTime).
   */
  @Column(name = "create_time", nullable = false)
  @NotNull
  public LocalDateTime getCreateTime() {
    return createTime;
  }

  /**
   * 创建时间(CreateTime).
   */
  public void setCreateTime(LocalDateTime createTime) {
    this.createTime = createTime;
  }

  /**
   * 更新人(UpdateBy).
   */
  @Column(name = "update_by", columnDefinition = "BINARY(16)")
  public UUID getUpdateBy() {
    return updateBy;
  }

  /**
   * 更新人(UpdateBy).
   */
  public void setUpdateBy(UUID updateBy) {
    this.updateBy = updateBy;
  }

  /**
   * 更新时间(UpdateTime).
   */
  @Column(name = "update_time", nullable = false)
  public LocalDateTime getUpdateTime() {
    return updateTime;
  }

  /**
   * 更新时间(UpdateTime).
   */
  public void setUpdateTime(LocalDateTime updateTime) {
    this.updateTime = updateTime;
  }

}
