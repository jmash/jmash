
package com.gitee.jmash.core.orm.jpa.entity;

import java.time.LocalDateTime;
import java.util.UUID;

/** 创建实体继承接口. */
public interface Create {
  /**
   * 创建人(CreateBy).
   */
  public UUID getCreateBy();

  /**
   * 创建人(CreateBy).
   */
  public void setCreateBy(UUID createBy);

  /**
   * 创建时间(CreateTime).
   */
  public LocalDateTime getCreateTime();

  /**
   * 创建时间(CreateTime).
   */
  public void setCreateTime(LocalDateTime createTime);

}
