
package com.gitee.jmash.core.orm.jpa.entity;

import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;
import jakarta.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

/** Only Create Not Allow Update Entity . */
@MappedSuperclass
public abstract class CreateEntity<F> extends BaiscEntityListener
       implements Serializable, Create, PrimaryKey<F> {

  private static final long serialVersionUID = 1L;

  /** 创建人. */
  protected UUID createBy;
  /** 创建时间. */
  protected LocalDateTime createTime;

  /**
   * 创建人(CreateBy).
   */
  @Column(name = "create_by", columnDefinition = "BINARY(16)")
  @NotNull
  public UUID getCreateBy() {
    return createBy;
  }

  /**
   * 创建人(CreateBy).
   */
  public void setCreateBy(UUID createBy) {
    this.createBy = createBy;
  }

  /**
   * 创建时间(CreateTime).
   */
  @Column(name = "create_time", nullable = false)
  @NotNull
  public LocalDateTime getCreateTime() {
    return createTime;
  }

  /**
   * 创建时间(CreateTime).
   */
  public void setCreateTime(LocalDateTime createTime) {
    this.createTime = createTime;
  }

}
