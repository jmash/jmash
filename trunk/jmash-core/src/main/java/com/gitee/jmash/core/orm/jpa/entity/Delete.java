
package com.gitee.jmash.core.orm.jpa.entity;

import java.time.LocalDateTime;
import java.util.UUID;

/** 删除实体继承接口. */
public interface Delete {
  
  /**
   * 删除人(deleteBy).
   */
  public UUID getDeleteBy();

  /**
   * 删除人(deleteBy).
   */
  public void setDeleteBy(UUID deleteBy);

  /**
   * 删除状态(deleted).
   */
  public Boolean getDeleted();

  /**
   * 删除状态(deleted).
   */
  public void setDeleted(Boolean deleted);

  /**
   * 删除时间(deleteTime).
   */
  public LocalDateTime getDeleteTime();

  /**
   * 删除时间(deleteTime).
   */
  public void setDeleteTime(LocalDateTime deleteTime);

}
