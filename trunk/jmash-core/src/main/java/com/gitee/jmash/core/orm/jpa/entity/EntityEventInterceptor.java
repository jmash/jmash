
package com.gitee.jmash.core.orm.jpa.entity;

import com.gitee.jmash.common.event.EntityEvent;
import jakarta.enterprise.event.Event;
import jakarta.enterprise.inject.spi.CDI;
import jakarta.enterprise.util.TypeLiteral;
import org.hibernate.CallbackException;
import org.hibernate.Interceptor;
import org.hibernate.Transaction;
import org.hibernate.type.Type;

/** 实体事件拦截器. */
public class EntityEventInterceptor implements Interceptor {

  Event<EntityEvent> event;

  /** CDI 获取Event. */
  public Event<EntityEvent> getEvent() {
    if (event == null) {
      TypeLiteral<Event<EntityEvent>> eventType = new TypeLiteral<Event<EntityEvent>>() {
        private static final long serialVersionUID = 1L;
      };
      event = CDI.current().select(eventType).get();
    }
    return event;
  }

  @Override
  public boolean onSave(Object entity, Object id, Object[] state, String[] propertyNames,
      Type[] types) throws CallbackException {
    getEvent().fireAsync(EntityEvent.create(entity, id, state, propertyNames, types));
    return Interceptor.super.onSave(entity, id, state, propertyNames, types);
  }

  @Override
  public void onDelete(Object entity, Object id, Object[] state, String[] propertyNames,
      Type[] types) throws CallbackException {
    getEvent().fireAsync(EntityEvent.delete(entity, id, state, propertyNames, types));
    Interceptor.super.onDelete(entity, id, state, propertyNames, types);
  }

  @Override
  public boolean onFlushDirty(Object entity, Object id, Object[] currentState,
      Object[] previousState, String[] propertyNames, Type[] types) throws CallbackException {
    getEvent().fireAsync(
        EntityEvent.update(entity, id, currentState, previousState, propertyNames, types));
    return Interceptor.super.onFlushDirty(entity, id, currentState, previousState, propertyNames,
        types);
  }

}
