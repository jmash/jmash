
package com.gitee.jmash.core.orm.jpa.entity;

import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.Version;
import jakarta.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

/** 通用继承实体 . */
@MappedSuperclass
public abstract class FullEntity<F> extends BaiscEntityListener
    implements Serializable, Create, Update, Delete, PrimaryKey<F> {

  private static final long serialVersionUID = 1L;

  /** 乐观锁. */
  protected Integer version = 0;
  /** 创建人. */
  protected UUID createBy;
  /** 创建时间. */
  protected LocalDateTime createTime;
  /** 更新人. */
  protected UUID updateBy;
  /** 更新时间. */
  protected LocalDateTime updateTime;
  /** 删除人. */
  protected UUID deleteBy;
  /** 删除状态. */
  protected Boolean deleted = false;
  /** 删除时间. */
  protected LocalDateTime deleteTime;

  /**
   * 乐观锁(Version).
   */
  @Column(name = "version_")
  @Version
  public Integer getVersion() {
    return version;
  }

  /**
   * 乐观锁(Version).
   */
  public void setVersion(Integer version) {
    this.version = version;
  }

  /**
   * 创建人(CreateBy).
   */
  @Column(name = "create_by", columnDefinition = "BINARY(16)")
  @NotNull
  public UUID getCreateBy() {
    return createBy;
  }

  /**
   * 创建人(CreateBy).
   */
  public void setCreateBy(UUID createBy) {
    this.createBy = createBy;
  }

  /**
   * 创建时间(CreateTime).
   */
  @Column(name = "create_time", nullable = false)
  @NotNull
  public LocalDateTime getCreateTime() {
    return createTime;
  }

  /**
   * 创建时间(CreateTime).
   */
  public void setCreateTime(LocalDateTime createTime) {
    this.createTime = createTime;
  }

  /**
   * 更新人(UpdateBy).
   */
  @Column(name = "update_by", columnDefinition = "BINARY(16)")
  public UUID getUpdateBy() {
    return updateBy;
  }

  /**
   * 更新人(UpdateBy).
   */
  public void setUpdateBy(UUID updateBy) {
    this.updateBy = updateBy;
  }

  /**
   * 更新时间(UpdateTime).
   */
  @Column(name = "update_time", nullable = false)
  public LocalDateTime getUpdateTime() {
    return updateTime;
  }

  /**
   * 更新时间(UpdateTime).
   */
  public void setUpdateTime(LocalDateTime updateTime) {
    this.updateTime = updateTime;
  }

  /**
   * 删除人(deleteBy).
   */
  @Column(name = "delete_by", columnDefinition = "BINARY(16)")
  public UUID getDeleteBy() {
    return deleteBy;
  }

  /**
   * 删除人(deleteBy).
   */
  public void setDeleteBy(UUID deleteBy) {
    this.deleteBy = deleteBy;
  }

  /**
   * 删除状态(deleted).
   */
  @Column(name = "deleted_", nullable = false)
  public Boolean getDeleted() {
    return deleted;
  }

  /**
   * 删除状态(deleted).
   */
  public void setDeleted(Boolean deleted) {
    this.deleted = deleted;
  }

  /**
   * 删除时间(deleteTime).
   */
  @Column(name = "delete_time")
  public LocalDateTime getDeleteTime() {
    return deleteTime;
  }

  /**
   * 删除时间(deleteTime).
   */
  public void setDeleteTime(LocalDateTime deleteTime) {
    this.deleteTime = deleteTime;
  }

}
