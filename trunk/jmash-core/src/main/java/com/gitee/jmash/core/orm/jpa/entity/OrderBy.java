package com.gitee.jmash.core.orm.jpa.entity;

import java.io.Serializable;

/**
 * 排序.
 */
public interface OrderBy<F extends Serializable> extends Serializable, PrimaryKey<F> {

  /** 排序(OrderBy). */
  public Integer getOrderBy();

  /** 排序(OrderBy). */
  public void setOrderBy(Integer orderBy);

}
