
package com.gitee.jmash.core.orm.jpa.entity;

/** 主键实体接口. */
public interface PrimaryKey<F> {

  public abstract F getEntityPk();

  public abstract void setEntityPk(F pk);

}
