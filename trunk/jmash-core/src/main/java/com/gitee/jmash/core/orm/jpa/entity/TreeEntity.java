package com.gitee.jmash.core.orm.jpa.entity;

import java.io.Serializable;

/**
 * 树型实体结构接口.
 *
 * @param <F> 主键
 */
public interface TreeEntity<F extends Serializable> extends Serializable, OrderBy<F> {

  /** 获取父ID(ParentId). */
  public F getParentId();

  /** 设置父ID(ParentId). */
  public void setParentId(F parentId);

  /** 深度(Depth). */
  public Integer getDepth();

  /** 深度(Depth). */
  public void setDepth(Integer depth);

}
