
package com.gitee.jmash.core.orm.jpa.entity;

import java.time.LocalDateTime;
import java.util.UUID;

/** 更新实体继承接口. */
public interface Update {

  /**
   * 更新人(UpdateBy).
   */
  public UUID getUpdateBy();

  /**
   * 更新人(UpdateBy).
   */
  public void setUpdateBy(UUID updateBy);

  /**
   * 更新时间(UpdateTime).
   */
  public LocalDateTime getUpdateTime();

  /**
   * 更新时间(UpdateTime).
   */
  public void setUpdateTime(LocalDateTime updateTime);
  
}
