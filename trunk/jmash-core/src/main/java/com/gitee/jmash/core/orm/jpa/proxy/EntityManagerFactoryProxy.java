package com.gitee.jmash.core.orm.jpa.proxy;


import com.gitee.jmash.common.enums.ProtocolType;
import com.gitee.jmash.core.grpc.jpa.GrpcEntityManagerProxy;
import com.gitee.jmash.core.grpc.jpa.GrpcQueryProxy;
import com.gitee.jmash.core.jaxrs.jpa.JaxrsEntityManagerProxy;
import com.gitee.jmash.core.jaxrs.jpa.JaxrsQueryProxy;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * 基于JDK的动态代理实现.
 *
 * @author CGD
 *
 */
public class EntityManagerFactoryProxy implements InvocationHandler {

  private EntityManagerFactory target;

  protected Class<? extends EntityManagerProxy> entityManagerProxy;

  protected Class<? extends QueryProxy> queryProxy;


  /** GRPC/Jaxrs不同. */
  public EntityManagerFactory bind(EntityManagerFactory target, ProtocolType type) {
    if (ProtocolType.GRPC.equals(type)) {
      return bind(target, GrpcEntityManagerProxy.class, GrpcQueryProxy.class);
    }
    return bind(target, JaxrsEntityManagerProxy.class, JaxrsQueryProxy.class);
  }

  /**
   * 绑定委托对象并返回一个代理类.
   */
  public EntityManagerFactory bind(EntityManagerFactory target,
      Class<? extends EntityManagerProxy> entityManagerProxy,
      Class<? extends QueryProxy> queryProxy) {
    this.target = target;
    this.entityManagerProxy = entityManagerProxy;
    this.queryProxy = queryProxy;
    if (target == null) {
      return null;
    }
    if (this.target instanceof Proxy
        && Proxy.getInvocationHandler(this.target) instanceof EntityManagerFactoryProxy) {
      return this.target;
    }
    // 取得代理对象
    // 要绑定接口(这是一个缺陷，cglib弥补了这一缺陷)
    Object obj = Proxy.newProxyInstance(target.getClass().getClassLoader(),
        target.getClass().getInterfaces(), this);
    return (EntityManagerFactory) obj;
  }

  /**
   * 调用方法.
   */
  public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
    Object result = null;
    result = method.invoke(target, args);
    // 封装结果
    if ((entityManagerProxy != null) && (result instanceof EntityManager)) {
      // log.debug(result.getClass().getName());
      EntityManagerProxy proxyObj = entityManagerProxy.getDeclaredConstructor().newInstance();
      return proxyObj.getInstance((EntityManager) result, queryProxy);
    }
    return result;
  }

}
