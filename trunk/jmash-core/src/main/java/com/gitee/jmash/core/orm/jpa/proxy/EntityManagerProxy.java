package com.gitee.jmash.core.orm.jpa.proxy;

import jakarta.persistence.EntityManager;
import java.lang.reflect.InvocationHandler;

/**
 * JPA EntityManager Proxy.
 *
 * @author CGD
 *
 */
public interface EntityManagerProxy extends InvocationHandler {

  /**
   * 创建代理对象.
   */
  public EntityManager getInstance(EntityManager target, Class<? extends QueryProxy> queryProxy);

}
