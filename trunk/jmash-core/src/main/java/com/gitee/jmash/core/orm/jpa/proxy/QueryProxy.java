package com.gitee.jmash.core.orm.jpa.proxy;

import jakarta.persistence.Query;
import java.lang.reflect.InvocationHandler;

/**
 * JPA Query Proxy.
 *
 * @author CGD
 *
 */
public interface QueryProxy extends InvocationHandler {

  /**
   * 创建代理对象.
   * 
   */
  public Query getInstance(Query target);

}
