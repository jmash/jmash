
package com.gitee.jmash.core.orm.module;

import com.gitee.jmash.core.orm.jdbc.JdbcTemplate;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import java.util.Collection;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.LogFactory;

/**
 * Database Build.
 */
@ApplicationScoped
public class DbBuild {

  @Inject
  private JdbcTemplate jdbcTemplate;

  @Inject
  private ModuleConfig moduleConfig;

  Collection<ModuleModel> models;

  /** Database build. */
  public void build() {
    build(this.jdbcTemplate);
  }

  /** Database build. */
  public void build(String modelName) {
    build(this.jdbcTemplate, modelName);
  }

  /** Database build. */
  public void build(JdbcTemplate jdbc) {
    if (models == null) {
      models = moduleConfig.load();
    }
    for (ModuleModel model : models) {
      ModuleDbBuild dbBuild = new ModuleDbBuild(jdbc, model);
      dbBuild.build();
    }
  }

  /** Database build. */
  public void build(JdbcTemplate jdbc, String modelName) {
    if (models == null) {
      models = moduleConfig.load();
    }
    for (ModuleModel model : models) {
      if (!StringUtils.equals(model.getName(), modelName)) {
        continue;
      }
      // 构建依赖模块
      for (String depend : model.getDependencies()) {
        build(jdbc, depend);
      }
      // 构建自身
      ModuleDbBuild dbBuild = new ModuleDbBuild(jdbc, model);
      dbBuild.build();
    }
  }

  /** 创建数据库. */
  public boolean createDatabase(String schame) {
    try {
      if (StringUtils.isNotBlank(schame) && !jdbcTemplate.existSchema(schame)) {
        String sql = "create database  " + schame + "  character set utf8mb4; ";
        jdbcTemplate.execSql(sql);
      }
    } catch (Exception ex) {
      LogFactory.getLog(getClass()).error(ex);
      return false;
    }
    return true;
  }
  
  /** 创建Schema. */
  public boolean createSchema(String schame) {
    try {
      if (StringUtils.isNotBlank(schame) && !jdbcTemplate.existSchema(schame)) {
        String sql = "create schema  " + schame + ";";
        jdbcTemplate.execSql(sql);
      }
    } catch (Exception ex) {
      LogFactory.getLog(getClass()).error(ex);
      return false;
    }
    return true;
  }
}
