
package com.gitee.jmash.core.orm.module;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.json.bind.Jsonb;
import jakarta.json.bind.JsonbBuilder;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 模块配置加载.
 */
@ApplicationScoped
public class ModuleConfig {

  private static Log log = LogFactory.getLog(ModuleConfig.class);

  public static String CONFIG = "META-INF/module/";

  private Class<?> callingClass;

  private String config = CONFIG + "config.json";

  private Jsonb jsonb = JsonbBuilder.create();

  public ModuleConfig() {
    this.callingClass = this.getClass();
  }

  @SuppressWarnings("rawtypes")
  public ModuleConfig(Class callingClass) {
    this.callingClass = callingClass;
  }

  public ModuleConfig(String config) {
    this.config = config;
    this.callingClass = this.getClass();
  }

  /**
   * Load Module配置文件.
   */
  public Collection<ModuleModel> load() {
    List<ModuleModel> dbModuleList = new ArrayList<ModuleModel>();
    try {
      Enumeration<URL> urls = this.callingClass.getClassLoader().getResources(config);
      while (urls.hasMoreElements()) {
        URL url = urls.nextElement();

        ModuleModel module = jsonb.fromJson(url.openStream(), ModuleModel.class);
        log.debug(url);

        log.debug(module.getName() + " " + StringUtils.join(module.getDependencies(), ","));
        dbModuleList.add(module);

      }
    } catch (Exception e) {
      log.error(e.getMessage());
    }
    return topSort(dbModuleList);
  }

  /**
   * 模块拓扑顺序.
   *
   * @param dbModuleList 模块列表
   * @return 模块集合
   */
  private Collection<ModuleModel> topSort(List<ModuleModel> dbModuleList) {
    Map<String, ModuleModel> map = new LinkedHashMap<String, ModuleModel>();
    int dbSize = dbModuleList.size();
    for (int i = 0; i < dbSize; i++) {
      if (dbModuleList.size() == 0) {
        break;
      }
      // 过滤一遍
      for (int j = dbModuleList.size() - 1; j >= 0; j--) {
        ModuleModel m = dbModuleList.get(j);
        Boolean contain = true;
        for (String s : m.getDependencies()) {
          if (!map.containsKey(s)) {
            contain = false;
          }
        }
        if (contain) {
          map.put(m.getName(), m);
          log.debug("module order : " + m.getName());
          dbModuleList.remove(j);
        }
      }
    }
    return map.values();
  }

  /** 测试.*/
  public static void main(String[] args) {
    Log log = LogFactory.getLog(ModuleConfig.class);
    log.info("test");
    ModuleConfig config = new ModuleConfig();
    Collection<ModuleModel> modules = config.load();
    for (ModuleModel md : modules) {
      log.info(md.getName());
    }
  }

}
