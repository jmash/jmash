
package com.gitee.jmash.core.orm.module;

import com.gitee.jmash.core.orm.jdbc.JdbcTemplate;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Database构建工具.
 *
 * @author CGD
 * 
 */
public class ModuleDbBuild {

  private static Log log = LogFactory.getLog(ModuleDbBuild.class);

  private JdbcTemplate jdbcTemplate;

  private ModuleInterface moduleInterface;

  private ModuleModel moduleModel;

  private Class<?> callingClass;

  /** 模块DBBuild. */
  public ModuleDbBuild(JdbcTemplate jdbcTemplate, ModuleModel moduleModel) {
    this.moduleModel = moduleModel;
    this.jdbcTemplate = jdbcTemplate;
    this.moduleInterface = new ModuleDbImpl(jdbcTemplate);
    this.callingClass = this.getClass();
  }

  /** 模块DBBuild. */
  public ModuleDbBuild(JdbcTemplate jdbcTemplate, ModuleModel moduleModel, Class<?> callingClass) {
    this.moduleModel = moduleModel;
    this.jdbcTemplate = jdbcTemplate;
    this.moduleInterface = new ModuleDbImpl(jdbcTemplate);
    this.callingClass = callingClass;
  }

  /**
   * 获取当前数据库该模块的版本.
   */
  public Integer getCurVersion() {
    return moduleInterface.getCurVersion(moduleModel.getName());
  }

  /**
   * 设置当前数据库模块的版本.
   */
  public void setCurVersion(Integer version) {
    moduleInterface.setCurVersion(moduleModel.getName(), version);
  }

  /**
   * 数据库构建.
   * 
   */
  public void build() {
    try {
      // 如果存在数据库
      if (this.existsTables()) {
        Integer curVersion = getCurVersion();
        if (curVersion == null || curVersion == 0) {
          this.moduleModel.setBeforeVersion(moduleModel.getVersion());
          setCurVersion(moduleModel.getVersion());
          // 设置模块日志
          log.info(String.format("setting module :%s version: %d ", moduleModel.getName(),
              moduleModel.getVersion()));
        } else {
          this.moduleModel.setBeforeVersion(curVersion);
          // 升级模块
          if (curVersion < moduleModel.getVersion()) {
            upgrade(curVersion);
            setCurVersion(moduleModel.getVersion());
            // 升级模块表日志
            log.info(String.format("upgrade module :%s version: %d  to %d", moduleModel.getName(),
                curVersion, moduleModel.getVersion()));
            // 视图创建
            view();
          }
        }
      } else {
        this.moduleModel.setBeforeVersion(0);
        // 创建新数据库
        create();
        // 初始化数据
        init();
        // 视图创建
        view();

        setCurVersion(moduleModel.getVersion());
        // 创建模块表日志
        log.info(String.format("create module :%s version: %d ", moduleModel.getName(),
            moduleModel.getVersion()));
      }
    } catch (Exception ex) {
      log.error("module build error:", ex);
    }
  }

  /**
   * 创建数据库.
   */
  public void create() throws IOException {
    if (this.existsTables()) {
      return;
    }
    String fileName = getCreateSqlFileName();
    List<String> sqls = findFileSql(fileName, false);
    for (String sql : sqls) {
      try {
        log.debug(sql);
        int updateCount = this.jdbcTemplate.execSql(sql);
        log.debug(String.format("%d rows affected", updateCount));
      } catch (Exception ex) {
        log.error("SQL Exec Error " + sql + " :", ex);
      }
    }
  }

  /**
   * 升级数据库.
   */
  public void upgrade(int curVersion) throws IOException {
    for (int i = curVersion; i < this.moduleModel.getVersion(); i++) {
      String fileName = getUpgradeSqlFileName(i);
      List<String> sqls = findFileSql(fileName, false);
      for (String sql : sqls) {
        try {
          log.debug(sql);
          int updateCount = this.jdbcTemplate.execSql(sql);
          log.debug(String.format("%d rows affected", updateCount));
        } catch (Exception ex) {
          log.error("SQL Exec Error " + sql + " :", ex);
        }
      }
    }
  }

  /**
   * 删除数据库.
   */
  public void drop() throws IOException {
    String fileName = getDropSqlFileName();
    List<String> sqls = findFileSql(fileName, false);
    for (String sql : sqls) {
      try {
        log.debug(sql);
        int updateCount = this.jdbcTemplate.execSql(sql);
        log.debug(String.format("%d rows affected", updateCount));
      } catch (Exception ex) {
        log.error("SQL Exec Error " + sql + " :", ex);
      }
    }
  }

  /**
   * 视图数据库.
   */
  public void view() throws IOException {
    String fileName = getViewSqlFileName();
    List<String> sqls = findFileSql(fileName, false);
    for (String sql : sqls) {
      try {
        log.debug(sql);
        int updateCount = this.jdbcTemplate.execSql(sql);
        log.debug(String.format("%d rows affected", updateCount));
      } catch (Exception ex) {
        log.error("SQL Exec Error " + sql + " :", ex);
      }
    }
  }

  /**
   * 初始化数据库.
   */
  public void init() throws IOException {
    String fileName = getInitSqlFileName();
    List<String> sqls = findFileSql(fileName, false);
    for (String sql : sqls) {
      try {
        log.debug(sql);
        int updateCount = this.jdbcTemplate.execSql(sql);
        log.debug(String.format("%d rows affected", updateCount));
      } catch (Exception ex) {
        log.error("SQL Exec Error " + sql + " :", ex);
      }
    }
  }

  /**
   * 判断是否存在表.
   */
  public boolean existsTables() {
    try {
      return jdbcTemplate.existTables(moduleModel.getTables());
    } catch (SQLException e) {
      log.error("", e);
    }
    return true;
  }

  /**
   * 读取文件内容.
   */
  public String getFileContent(String fileName) {
    InputStream inputStream = callingClass.getClassLoader().getResourceAsStream(fileName);
    if (inputStream == null) {
      return StringUtils.EMPTY;
    }
    try {
      return IOUtils.toString(inputStream, "utf-8");
    } catch (Exception ex) {
      return StringUtils.EMPTY;
    }
  }

  /**
   * 获取文件中的SQL语句.
   */
  public List<String> findFileSql(String fileName, boolean keepformat) throws IOException {
    InputStream inputStream = callingClass.getClassLoader().getResourceAsStream(fileName);
    List<String> sqls = new ArrayList<String>();
    if (inputStream == null) {
      return sqls;
    }
    StringBuffer sql = new StringBuffer();
    String line = "";
    BufferedReader in = new BufferedReader(new InputStreamReader(inputStream, "utf-8"));

    while ((line = in.readLine()) != null) {
      if (!keepformat) {
        line = line.trim();
      }
      // line = getProject().replaceProperties(line);
      if (!keepformat) {
        if (line.startsWith("//")) {
          continue;
        }
        if (line.startsWith("--")) {
          continue;
        }
        if (line.startsWith("/*")) {
          continue;
        }
        if (line.endsWith("*/")) {
          continue;
        }
        StringTokenizer st = new StringTokenizer(line);
        if (st.hasMoreTokens()) {
          String token = st.nextToken();
          if ("REM".equalsIgnoreCase(token)) {
            continue;
          }
        }
      }
      if (!keepformat) {
        sql.append(" " + line);
      } else {
        sql.append("\n" + line);
      }

      if ((!keepformat) && (line.indexOf("--") >= 0)) {
        sql.append("\n");
      }

      if (!sql.toString().endsWith(";") || !line.endsWith(";")) {
        continue;
      }
      String s = sql.substring(0, sql.length() - ";".length());
      if (!StringUtils.isEmpty(s)) {
        sqls.add(s);
      }
      sql.replace(0, sql.length(), "");
    }
    return sqls;
  }

  /**
   * 创建数据库脚本.
   */
  public String getCreateSqlFileName() {
    return String.format(ModuleConfig.CONFIG + "create/%s.create.sql", this.moduleModel.getName());
  }

  /**
   * 初始化数据库脚本.
   */
  public String getInitSqlFileName() {
    return String.format(ModuleConfig.CONFIG + "create/%s.init.sql", this.moduleModel.getName());
  }

  /**
   * 删除数据库脚本.
   */
  public String getDropSqlFileName() {
    return String.format(ModuleConfig.CONFIG + "drop/%s.drop.sql", this.moduleModel.getName());
  }

  /**
   * 视图数据库脚本.
   */
  public String getViewSqlFileName() {
    return String.format(ModuleConfig.CONFIG + "create/%s.view.sql", this.moduleModel.getName());
  }

  /**
   * 升级数据库脚本.
   */
  public String getUpgradeSqlFileName(int next) {
    return String.format(ModuleConfig.CONFIG + "upgrade/%s.upgrade.%d.to.%d.sql",
        this.moduleModel.getName(), next, next + 1);
  }
}
