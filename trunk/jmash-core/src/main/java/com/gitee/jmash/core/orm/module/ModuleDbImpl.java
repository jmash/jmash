
package com.gitee.jmash.core.orm.module;

import com.gitee.jmash.core.orm.data.DataTable;
import com.gitee.jmash.core.orm.jdbc.JdbcTemplate;
import jakarta.enterprise.context.Dependent;
import jakarta.enterprise.inject.Typed;
import java.sql.SQLException;
import java.time.LocalDateTime;
import org.apache.commons.logging.LogFactory;

/**
 * 模块数据库版本实现.
 */
@Dependent
@Typed(ModuleInterface.class)
public class ModuleDbImpl implements ModuleInterface {

  private static String tableName = "os_properties";

  private JdbcTemplate jdbcTemplate;

  public ModuleDbImpl() {}

  public ModuleDbImpl(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  /**
   * 数据库查询模块版本.
   *
   * @param moduleName 模块名称
   * @return 模块版本
   */
  private Integer getVersion(String moduleName) throws SQLException {
    String sql = String.format(
        "select int_value from %s where entity_id=? and entity_name = ? and key_name= ?",
        tableName);
    DataTable dt =
        this.jdbcTemplate.querySqlParam(sql, new Object[] {"version", "system", moduleName});
    if (dt == null || dt.getRows().isEmpty()) {
      return null;
    }
    return (Integer) dt.getRows().get(0).getValue(0);

  }

  /**
   * 设置数据库模块版本.
   *
   * @param moduleName 模块名称
   * @param version 版本
   * @return 执行数
   */
  private int setVersion(String moduleName, Integer version) throws SQLException {
    if (getVersion(moduleName) == null) {
      String sql = String.format(
          "insert %s (entity_id,entity_name,key_name,int_value,type_,create_time) values(?,?,?,?,?,?)",
          tableName);
      return this.jdbcTemplate.execSqlParam(sql,
          new Object[] {"version", "system", moduleName, version, 2, LocalDateTime.now()});
    } else {
      String sql = String.format(
          "update %s set int_value=?,update_time=? where entity_id=? and entity_name=? and key_name=?",
          tableName);
      return this.jdbcTemplate.execSqlParam(sql,
          new Object[] {version, LocalDateTime.now(), "version", "system", moduleName});
    }
  }

  @Override
  public Integer getCurVersion(String moduleName) {
    try {
      if (jdbcTemplate.existTables(tableName)) {
        Integer version = getVersion(moduleName);
        return version == null ? 0 : version;
      }
    } catch (SQLException e) {
      LogFactory.getLog(ModuleDbImpl.class).error("", e);
    }
    return 0;

  }

  @Override
  public void setCurVersion(String moduleName, Integer version) {
    try {
      if (jdbcTemplate.existTables(tableName)) {
        setVersion(moduleName, version);
      }
    } catch (SQLException e) {
      LogFactory.getLog(ModuleDbImpl.class).error("", e);
    }
  }
}
