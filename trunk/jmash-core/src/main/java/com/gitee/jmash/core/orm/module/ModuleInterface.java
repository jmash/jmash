
package com.gitee.jmash.core.orm.module;

/**
 * 模块版本接口.
 */
public interface ModuleInterface {
  /**
   * 获取当前模块数据库版本.
   *
   * @param moduleName 模块名
   * @return 当前模块版本
   */
  Integer getCurVersion(String moduleName);

  /**
   * 保存当前模块数据库版本.
   *
   * @param moduleName 模块名
   * @param version    当前模块版本
   */
  void setCurVersion(String moduleName, Integer version);
}
