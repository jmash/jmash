
package com.gitee.jmash.core.orm.module;

/**
 * 模块配置.
 */
public class ModuleModel {

  /** 名称. */
  private String name;
  /** 版本. */
  private Integer version;
  /** 检查的表名. */
  private String[] tables;
  /** 依赖的模块名. */
  private String[] dependencies;
  /** 升级前版本. */
  private Integer beforeVersion;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getVersion() {
    return version;
  }

  public void setVersion(Integer version) {
    this.version = version;
  }

  public String[] getTables() {
    return tables;
  }

  public void setTables(String[] tables) {
    this.tables = tables;
  }

  public String[] getDependencies() {
    return dependencies;
  }

  public void setDependencies(String[] dependencies) {
    this.dependencies = dependencies;
  }

  public Integer getBeforeVersion() {
    return beforeVersion;
  }

  public void setBeforeVersion(Integer beforeVersion) {
    this.beforeVersion = beforeVersion;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((name == null) ? 0 : name.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    ModuleModel other = (ModuleModel) obj;
    if (name == null) {
      if (other.name != null) {
        return false;
      }
    } else if (!name.equals(other.name)) {
      return false;
    }
    return true;
  }

}
