
package com.gitee.jmash.core.orm.tenant;

import com.gitee.jmash.core.jta.DataSourceManage;
import com.gitee.jmash.core.orm.cdi.DataSourceConfig;
import com.gitee.jmash.core.orm.cdi.JpaUtil;
import com.gitee.jmash.core.orm.jdbc.JdbcTemplate;
import com.gitee.jmash.core.orm.module.DbBuild;
import com.gitee.jmash.core.utils.AnnotationUtils;
import jakarta.enterprise.inject.spi.CDI;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javax.sql.DataSource;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.engine.jdbc.connections.spi.MultiTenantConnectionProvider;
import org.hibernate.service.UnknownUnwrapTypeException;

/**
 * JPA MultiTenant Database Connection Provider.
 *
 * @author CGD
 *
 */
public class JpaMultiTenantConnectionProvider
    implements MultiTenantConnectionProvider<JpaMultiTenantIdentifier> {

  private static final long serialVersionUID = 1L;

  /** 数据库模块租户初始化. */
  protected static final Map<String, Boolean> moduleTenantMap = new ConcurrentHashMap<>();

  /** 相同数据库读写数据源. */
  protected static final Map<String, DataSource> dataSourceMap = new ConcurrentHashMap<>();

  public DataSource getAnyDataSource() {
    return DataSourceConfig.getWriteDataSource();
  }

  /** 获取数据源. */
  public DataSource getTenantDataSource(JpaMultiTenantIdentifier tenantObj) {
    if (tenantObj.getTenant() == null) {
      throw new RuntimeException("tenant is null!");
    }
    if (tenantObj.getTenant().contains("-")) {
      throw new RuntimeException("tenant 不能包含 '-' 字符 !");
    }
    // 模块租户数据库是否初始化.
    String moduleTenantKey = getModuleTenantKey(tenantObj);
    if (!moduleTenantMap.containsKey(moduleTenantKey)) {
      initModuleTenantDb(tenantObj);
    }
    // 数据源.
    String key = getKey(tenantObj.getDatabase(), tenantObj.isWrite());
    if (dataSourceMap.containsKey(key)) {
      return dataSourceMap.get(key);
    }
    return createDataSource(tenantObj);
  }

  /** 数据库模块租户初始化. */
  public synchronized void initModuleTenantDb(JpaMultiTenantIdentifier tenantObj) {
    String moduleTenantKey = getModuleTenantKey(tenantObj);
    if (moduleTenantMap.containsKey(moduleTenantKey)) {
      return;
    }
    DataSource writeDataSource = DataSourceManage.getDataSource(tenantObj.getDatabase(), true);
    dataSourceMap.put(getKey(tenantObj.getDatabase(), true), writeDataSource);
    // 检查并创建数据库模式（后期兼容postgresql）.
    DbBuild dbBuild = CDI.current().select(DbBuild.class).get();
    dbBuild.createSchema(tenantObj.getDatabase());
    // 更新数据库
    JdbcTemplate jdbc = JpaUtil.getJdbcTemplate(writeDataSource);
    dbBuild.build(jdbc, tenantObj.getUnitName());
    moduleTenantMap.put(moduleTenantKey, true);
  }

  /** 数据源Key. */
  private String getModuleTenantKey(JpaMultiTenantIdentifier tenantObj) {
    return tenantObj.getUnitName() + "_" + tenantObj.getTenantName();
  }

  /** 同步创建. */
  private synchronized DataSource createDataSource(JpaMultiTenantIdentifier tenantObj) {
    String key = getKey(tenantObj.getDatabase(), tenantObj.isWrite());
    if (dataSourceMap.containsKey(key)) {
      return dataSourceMap.get(key);
    }
    // 默认数据源.
    if (StringUtils.isBlank(tenantObj.getDatabase())) {
      dataSourceMap.put(key, getDataSource(tenantObj.isWrite()));
      return dataSourceMap.get(key);
    }
    // 非默认数据源.
    DataSource readDataSource = DataSourceManage.getDataSource(tenantObj.getDatabase(), false);
    dataSourceMap.put(getKey(tenantObj.getDatabase(), false), readDataSource);
    return dataSourceMap.get(key);
  }

  /** 数据源Key. */
  private String getKey(String database, boolean write) {
    return database + "_" + write;
  }

  @Override
  public boolean isUnwrappableAs(Class<?> unwrapType) {
    return DataSource.class.isAssignableFrom(unwrapType)
        || MultiTenantConnectionProvider.class.isAssignableFrom(unwrapType);
  }

  @Override
  @SuppressWarnings("unchecked")
  public <T> T unwrap(Class<T> unwrapType) {
    if (MultiTenantConnectionProvider.class.isAssignableFrom(unwrapType)) {
      return (T) this;
    } else if (DataSource.class.isAssignableFrom(unwrapType)) {
      return (T) getAnyDataSource();
    } else {
      throw new UnknownUnwrapTypeException(unwrapType);
    }
  }

  @Override
  public Connection getAnyConnection() throws SQLException {
    return getAnyDataSource().getConnection();
  }

  @Override
  public void releaseAnyConnection(Connection connection) throws SQLException {
    connection.close();
  }

  @Override
  public Connection getConnection(JpaMultiTenantIdentifier tenantIdentifier) throws SQLException {
    return getTenantDataSource(tenantIdentifier).getConnection();
  }

  @Override
  public void releaseConnection(JpaMultiTenantIdentifier tenantIdentifier, Connection connection)
      throws SQLException {
    connection.close();
  }

  @Override
  public boolean supportsAggressiveRelease() {
    return false;
  }

  /** 读写数据源. */
  public DataSource getDataSource(boolean write) {
    if (write) {
      return CDI.current()
          .select(DataSource.class, AnnotationUtils.getNamedAnnotation("writeDataSource")).get();
    } else {
      return CDI.current()
          .select(DataSource.class, AnnotationUtils.getNamedAnnotation("readDataSource")).get();
    }
  }
}
