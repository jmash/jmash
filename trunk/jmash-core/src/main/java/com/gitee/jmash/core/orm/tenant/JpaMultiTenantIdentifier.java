package com.gitee.jmash.core.orm.tenant;

import com.gitee.jmash.core.utils.TenantUtil;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.microprofile.config.Config;
import org.eclipse.microprofile.config.ConfigProvider;

/**
 * 多租户标识.
 *
 * @author cgd
 */
public class JpaMultiTenantIdentifier extends TenantIdentifier {

  private static final long serialVersionUID = 1L;

  private static Log log = LogFactory.getLog(JpaMultiTenantIdentifier.class);

  // 是否采用集成多个模块
  protected static String MODULE = "module.db";

  protected static String MODULE_DB = "module.db.";

  protected static String MODULE_TENANT = "module.tenant.";

  protected static String TENANT_DB = "tenant.db.";
  // 租户数据库配置缓存.
  protected static Map<String, String> tenantDBMap = new ConcurrentHashMap<>();

  protected Pattern pattern = Pattern.compile("[pe]\\d{2,5}");

  // JAP UnitName
  private String unitName;
  // 租户名称
  private String tenant;
  // 读/写
  private boolean write = false;

  public JpaMultiTenantIdentifier() {
    super();
  }

  public JpaMultiTenantIdentifier(boolean write) {
    super();
    this.write = write;
  }

  /** 租户构造. */
  public JpaMultiTenantIdentifier(String unitName, boolean write) {
    super();
    this.unitName = unitName;
    this.write = write;
  }

  /** 租户构造. */
  public JpaMultiTenantIdentifier(String unitName, String tenant, boolean write) {
    super();
    this.unitName = unitName;
    this.tenant = tenant;
    this.tenantIdentifier = TenantUtil.getTenantIdentifierStr(this.tenant);
    this.write = write;
  }

  /** 数据库名称. */
  public String getDatabase() {
    // 允许tenant 为“”
    if (null == tenant) {
      return StringUtils.EMPTY;
    }
    String tenantName = this.getTenantName();
    String key = unitName + "." + this.getTenantName();
    if (tenantDBMap.containsKey(key)) {
      return tenantDBMap.get(key);
    }
    tenantDBMap.put(key, tenantDatabase());
    log.info(String.format("模块：%s,租户：%s,数据库：%s", unitName, tenantName, tenantDBMap.get(key)));
    return tenantDBMap.get(key);
  }

  /** 获取租户数据库配置. */
  private String tenantDatabase() {
    Config config = ConfigProvider.getConfig();
    String tenantName = this.getTenantName();
    // 访问数据个人或组织数据.
    Matcher matcher = pattern.matcher(tenantName);
    // 是否采用集成多个模块
    Optional<Boolean> module = config.getOptionalValue(MODULE, Boolean.class);
    if (module.isPresent() && module.get()) {
      if (matcher.matches()) {
        Optional<String> prefix = multiModuleTenant(config, tenantName.substring(0, 1));
        return prefix.isPresent() ? prefix.get() + "_" + tenantName
            : multiModule(config, tenantName, StringUtils.EMPTY);
      }
      return multiModule(config, tenantName, StringUtils.EMPTY);
    } else {
      if (matcher.matches()) {
        String dbPrefix = singleModule(config, tenantName.substring(0, 1), StringUtils.EMPTY);
        return StringUtils.isBlank(dbPrefix) ? StringUtils.EMPTY : dbPrefix + "_" + tenantName;
      }
      return singleModule(config, tenantName, StringUtils.EMPTY);
    }
  }

  /** 集成多个模块场景，获取租户配置. */
  public String multiModule(Config config, String tenantName, String defaultValue) {
    // 获取模块,租户配置.
    Optional<String> database = multiModuleTenant(config, tenantName);
    if (database.isPresent()) {
      return database.get();
    }
    // 获取模块配置.
    String key = MODULE_DB + unitName;
    database = config.getOptionalValue(key, String.class);
    return database.orElse(defaultValue);
  }

  /** 集成多个模块场景，获取租户配置. */
  public Optional<String> multiModuleTenant(Config config, String tenantName) {
    // 获取模块,租户配置.
    String key = MODULE_DB + unitName + "." + tenantName;
    Optional<String> database = config.getOptionalValue(key, String.class);
    if (database.isPresent()) {
      return database;
    }
    // 获取租户配置.
    key = MODULE_TENANT + tenantName;
    database = config.getOptionalValue(key, String.class);
    return database;
  }

  /** 单模块场景，获取该模块租户配置. */
  public String singleModule(Config config, String tenantName, String defaultValue) {
    String key = TENANT_DB + tenantName;
    Optional<String> database = config.getOptionalValue(key, String.class);
    return database.orElse(defaultValue);
  }


  /** 0191c4439482453799a3996ca7a2528c@core 返回 core. */
  public String getTenantName() {
    return TenantUtil.getTenantName(this.tenant);
  }

  public String getUnitName() {
    return unitName;
  }

  public void setUnitName(String unitName) {
    this.unitName = unitName;
  }

  public String getTenant() {
    return tenant;
  }

  public void setTenant(String tenant) {
    this.tenant = tenant;
    this.tenantIdentifier = TenantUtil.getTenantIdentifierStr(this.tenant);
  }

  public boolean isWrite() {
    return write;
  }

  public void setWrite(boolean write) {
    this.write = write;
  }

  @Override
  public String toString() {
    return "JpaMultiTenant [unitName=" + unitName + ", tenant=" + tenant + ", write=" + write
        + ", getDatabase()=" + getDatabase() + "]";
  }

  @Override
  public int hashCode() {
    return Objects.hash(tenant, unitName, write);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    // 父类比较
    if (obj instanceof TenantIdentifier) {
      TenantIdentifier other = (TenantIdentifier) obj;
      return Objects.equals(tenantIdentifier, other.tenantIdentifier);
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    JpaMultiTenantIdentifier other = (JpaMultiTenantIdentifier) obj;
    return Objects.equals(tenant, other.tenant) && Objects.equals(unitName, other.unitName)
        && write == other.write;
  }
}
