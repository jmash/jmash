
package com.gitee.jmash.core.orm.tenant;

import org.hibernate.context.spi.CurrentTenantIdentifierResolver;

/**
 * JPA Current MuiltiTenantResolver.
 *
 * @author CGD
 *
 */
public class JpaMultiTenantResolver
    implements CurrentTenantIdentifierResolver<TenantIdentifier> {

  private JpaMultiTenantIdentifier tenantIdentifier = new JpaMultiTenantIdentifier();


  public void setTenantIdentifier(JpaMultiTenantIdentifier tenantIdentifier) {
    this.tenantIdentifier = tenantIdentifier;
  }

  @Override
  public TenantIdentifier resolveCurrentTenantIdentifier() {
    return tenantIdentifier;
  }

  @Override
  public boolean validateExistingCurrentSessions() {
    return false;
  }


}
