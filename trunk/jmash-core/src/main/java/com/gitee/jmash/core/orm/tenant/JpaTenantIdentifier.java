package com.gitee.jmash.core.orm.tenant;

/** 租户标识接口. */
public interface JpaTenantIdentifier  {

  public TenantIdentifier getTenantId();

  public void setTenantId(TenantIdentifier tenantId);

}
