package com.gitee.jmash.core.orm.tenant;

import java.util.Objects;
import org.apache.commons.lang3.StringUtils;

/** 租户标识. */
public class TenantIdentifier implements java.io.Serializable {

  private static final long serialVersionUID = 1L;

  public static final String DEFAULT = "default";

  /** 默认租户. */
  public static boolean isDefault(TenantIdentifier obj) {
    return null != obj && StringUtils.equals(obj.tenantIdentifier, DEFAULT);
  }

  String tenantIdentifier = DEFAULT;

  public TenantIdentifier() {
    super();
  }

  public TenantIdentifier(String tenantIdentifier) {
    super();
    this.tenantIdentifier = (tenantIdentifier == null) ? null : tenantIdentifier.toLowerCase();
  }

  public String getTenantIdentifier() {
    return tenantIdentifier;
  }

  public void setTenantIdentifier(String tenantIdentifier) {
    this.tenantIdentifier = (tenantIdentifier == null) ? null : tenantIdentifier.toLowerCase();
  }

  @Override
  public int hashCode() {
    return Objects.hash(tenantIdentifier);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (!(obj instanceof TenantIdentifier)) {
      return false;
    }
    TenantIdentifier other = (TenantIdentifier) obj;
    return Objects.equals(tenantIdentifier, other.tenantIdentifier);
  }


}
