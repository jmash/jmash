package com.gitee.jmash.core.orm.tenant;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Objects;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.usertype.UserType;

/** 租户标识UserType. */
public class TenantIdentifierType implements UserType<TenantIdentifier> {

  public static final TenantIdentifierType INSTANCE = new TenantIdentifierType();

  @Override
  public int getSqlType() {
    return Types.VARCHAR;
  }

  @Override
  public Class<TenantIdentifier> returnedClass() {
    return TenantIdentifier.class;
  }

  @Override
  public boolean equals(TenantIdentifier x, TenantIdentifier y) {
    if (x != null && y != null) {
      return Objects.equals(x.getTenantIdentifier(), y.getTenantIdentifier());
    }
    return Objects.equals(x, y);
  }

  @Override
  public int hashCode(TenantIdentifier x) {
    return Objects.hashCode(x.getTenantIdentifier());
  }

  @Override
  public TenantIdentifier nullSafeGet(ResultSet rs, int position,
      SharedSessionContractImplementor session, Object owner) throws SQLException {
    String columnValue = rs.getString(position);
    if (rs.wasNull()) {
      columnValue = null;
    }
    // log.debugv("Result set column {0} value is {1}", position, columnValue);
    return new TenantIdentifier(columnValue);
  }

  @Override
  public void nullSafeSet(PreparedStatement st, TenantIdentifier value, int index,
      SharedSessionContractImplementor session) throws SQLException {
    if (value == null) {
      // log.debugv("Binding null to parameter {0} ", index);
      st.setNull(index, Types.VARCHAR);
    } else {
      String stringValue = value.getTenantIdentifier();
      // log.debugv("Binding {0} to parameter {1} ", stringValue, index);
      st.setString(index, stringValue);
    }
  }

  @Override
  public TenantIdentifier deepCopy(TenantIdentifier tenantIdentifier) {
    return tenantIdentifier == null ? null
        : new TenantIdentifier(tenantIdentifier.tenantIdentifier);
  }

  @Override
  public boolean isMutable() {
    return true;
  }

  @Override
  public Serializable disassemble(TenantIdentifier value) {
    return deepCopy(value);
  }

  @Override
  public TenantIdentifier assemble(Serializable cached, Object owner) {
    return deepCopy((TenantIdentifier) cached);
  }

}
