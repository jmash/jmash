
package com.gitee.jmash.core.orm.tenant;

/** 租户服务接口. */
public interface TenantService extends AutoCloseable {

  /** 设置租户. */
  public <T extends TenantService> T setTenant(String tenant);

  /** 获取租户. */
  public String getTenant();
  
  /** 仅仅设置租户名称. */
  public void setTenantOnly(String tenant);

}
