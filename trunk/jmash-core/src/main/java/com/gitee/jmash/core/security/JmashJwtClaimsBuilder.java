package com.gitee.jmash.core.security;

import com.gitee.jmash.common.utils.UUIDUtil;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.jwt.Claims;
import org.jose4j.jwt.JwtClaims;

/** JWT Builder. */
public class JmashJwtClaimsBuilder {

  JwtClaims claims = new JwtClaims();

  public static JmashJwtClaimsBuilder newBuilder() {
    return new JmashJwtClaimsBuilder().setTokenId(UUIDUtil.uuid32(UUID.randomUUID()));
  }

  protected JmashJwtClaimsBuilder() {
    super();
  }

  /** 登录tenant. */
  public JmashJwtClaimsBuilder setTenant(String tenant) {
    // 发行机构
    claims.setIssuer(tenant);
    return this;
  }

  /** Token ID,Token 唯一. */
  public JmashJwtClaimsBuilder setTokenId(String tokenId) {
    claims.setJwtId(tokenId);
    return this;
  }

  /** user principal of the token,unique principal name. */
  public JmashJwtClaimsBuilder setName(String principalName) {
    claims.setClaim(Claims.upn.name(), principalName);
    return this;
  }

  /** user unifiedId 生态唯一ID. */
  public JmashJwtClaimsBuilder setUnifiedId(String unifiedId) {
    if (StringUtils.isNotBlank(unifiedId)) {
      claims.setClaim(Claims.dest.name(), unifiedId.toLowerCase());
    }
    return this;
  }
  
  /** 个人信息存储区. */
  public JmashJwtClaimsBuilder setStorage(String storage) {
    // 源标识 Originating Identity
    if (StringUtils.isNotBlank(storage)) {
      claims.setClaim(Claims.orig.name(), storage);
    }
    return this;
  }

  /** 登录用户名/手机号/电子邮件地址. */
  public JmashJwtClaimsBuilder setSubject(String subject) {
    if (StringUtils.isNotBlank(subject)) {
      claims.setSubject(subject);
    }
    return this;
  }

  /** 被授权方 ClientId. */
  public JmashJwtClaimsBuilder setClientId(String clientId) {
    if (StringUtils.isNotBlank(clientId)) {
      // 被授权方.
      claims.setClaim(Claims.azp.name(), clientId);
    }
    return this;
  }

  /** 授权范围 scope. */
  public JmashJwtClaimsBuilder setScope(String scope) {
    if (StringUtils.isNotBlank(scope)) {
      // Scope Groups 存放List,获取Set.
      List<String> scopes = Arrays.asList(scope.split(",")).stream().collect(Collectors.toList());
      claims.setClaim(Claims.groups.name(), scopes);
    }
    return this;
  }

  /** 设置过期时间（秒）. */
  public JmashJwtClaimsBuilder setExpiresIn(long expiresIn) {
    // 设置过期分钟数
    claims.setExpirationTimeMinutesInTheFuture(expiresIn / (float) 60);
    return this;
  }

  public JwtClaims getJwtClaims() {
    return this.claims;
  }

}
