package com.gitee.jmash.core.service;

/**
 * 服务层的异常ServiceException.
 *
 * @author cgd email:cgd@crenjoy.com
 * @version V1.0
 */
public class ServiceException extends RuntimeException {

  /**
   * 版本号.
   */
  private static final long serialVersionUID = 1L;

  public ServiceException() {
    super();
  }

  public ServiceException(String msg) {
    super(msg);
  }

  public ServiceException(String message, Throwable cause) {
    super(message, cause);
  }

  public ServiceException(Throwable cause) {
    super(cause);
  }

}
