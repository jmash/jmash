
package com.gitee.jmash.core.shiro.cache;

import com.gitee.jmash.common.redis.callback.RedisTemplate;
import jakarta.enterprise.inject.literal.NamedLiteral;
import jakarta.enterprise.inject.spi.CDI;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.lang.ShiroException;
import org.apache.shiro.lang.util.Destroyable;
import org.apache.shiro.lang.util.Initializable;
import org.apache.shiro.subject.SimplePrincipalCollection;

/**
 * Redis 存储Session,Use Redis 1.
 *
 * @author CGD
 *
 */
public class RedisCacheManager implements CacheManager, Initializable, Destroyable {

  Map<String, Cache<?, ?>> map = null;

  @Override
  public void destroy() throws Exception {
    map.clear();
    map = null;
  }

  @Override
  public void init() throws ShiroException {
    map = new HashMap<>();

  }

  @SuppressWarnings("unchecked")
  @Override
  public <K, V> Cache<K, V> getCache(String name) throws CacheException {
    if (map.containsKey(name)) {
      return (Cache<K, V>) map.get(name);
    }
    RedisTemplate redisTemplate =
        CDI.current().select(RedisTemplate.class, NamedLiteral.of("redisShiroCache")).get();
    RedisShiroCacheImpl<K, V> impl = null;
    if (name.endsWith("authorizationCache")) {
      // 角色,权限缓存.
      impl =
          new RedisShiroCacheImpl<>(redisTemplate, SimplePrincipalCollection.class, "shiro:authz");
    } else {
      impl = new RedisShiroCacheImpl<>(redisTemplate, Serializable.class, name);
    }

    map.put(name, impl);
    return impl;
  }

}
