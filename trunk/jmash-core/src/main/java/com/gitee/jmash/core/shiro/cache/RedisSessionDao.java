
package com.gitee.jmash.core.shiro.cache;

import com.gitee.jmash.common.redis.callback.RedisTemplate;
import jakarta.enterprise.inject.literal.NamedLiteral;
import jakarta.enterprise.inject.spi.CDI;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.UnknownSessionException;
import org.apache.shiro.session.mgt.eis.AbstractSessionDAO;
import org.apache.shiro.util.CollectionUtils;

/**
 * Redis 存储Session,Use Redis 1.
 *
 * @author cgd
 *
 */
public class RedisSessionDao extends AbstractSessionDAO {

  private RedisShiroCacheImpl<Serializable, Session> sessions;

  /** Session Cache Use Redis . */
  public RedisSessionDao() {
    RedisTemplate redisTemplate =
        CDI.current().select(RedisTemplate.class, NamedLiteral.of("redisShiroCache")).get();
    sessions = new RedisShiroCacheImpl<>(redisTemplate, String.class, "shiro:session");
  }

  protected Serializable doCreate(Session session) {
    if (null == session.getId()) {
      Serializable sessionId = generateSessionId(session);
      assignSessionId(session, sessionId);
    }
    storeSession(session.getId(), session);
    return session.getId();
  }

  protected Session storeSession(Serializable id, Session session) {
    if (id == null) {
      throw new NullPointerException("id argument cannot be null.");
    }
    if (sessions.get(id) != null) {
      sessions.put(id, session, session.getTimeout() / 1000);
      return session;
    } else {
      sessions.put(id, session, session.getTimeout() / 1000);
      return null;
    }
  }

  protected Session doReadSession(Serializable sessionId) {
    return sessions.get(sessionId);
  }

  public void update(Session session) throws UnknownSessionException {
    storeSession(session.getId(), session);
  }

  /** Delete Session . */
  public void delete(Session session) {
    if (session == null) {
      throw new NullPointerException("session argument cannot be null.");
    }
    Serializable id = session.getId();
    if (id != null) {
      sessions.remove(id);
    }
  }

  /** Active Sessions . */
  public Collection<Session> getActiveSessions() {
    Set<Serializable> keys = sessions.keys();
    Collection<Session> values = new ArrayList<Session>();
    for (Serializable key : keys) {
      Session s = (Session) sessions.get(key);
      values.add(s);
    }
    if (CollectionUtils.isEmpty(values)) {
      return Collections.emptySet();
    } else {
      return Collections.unmodifiableCollection(values);
    }
  }

}
