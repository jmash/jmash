
package com.gitee.jmash.core.shiro.cache;

import com.gitee.jmash.common.redis.callback.RadisTransCallback;
import com.gitee.jmash.common.redis.callback.RedisTemplate;
import com.gitee.jmash.common.utils.SerializationUtil;
import com.gitee.jmash.common.utils.UUIDUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.apache.shiro.subject.SimplePrincipalCollection;
import org.bouncycastle.util.Arrays;
import org.eclipse.microprofile.jwt.JsonWebToken;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Transaction;

/**
 * Redis Cache For Shiro Cache.
 *
 * @author CGD
 */
public class RedisShiroCacheImpl<K, V> implements Cache<K, V> {
  /** Key分隔符. */
  private static final String DELIMIT = ":";
  /** 默认过期时间. */
  private static long EXPIRE_SECONDS = 7200;

  private RedisTemplate redisTemplate;
  /** Key前缀. */
  private String keyPrefix = "default";
  /** Key前缀. */
  private Class<?> keyClass = Serializable.class;

  public RedisShiroCacheImpl(RedisTemplate redisTemplate) {
    super();
    this.redisTemplate = redisTemplate;
  }

  /** Redis、Key前缀、过期时间配置. */
  public RedisShiroCacheImpl(RedisTemplate redisTemplate, Class<?> keyClass, String keyPrefix) {
    super();
    this.redisTemplate = redisTemplate;
    this.keyPrefix = keyPrefix;
    this.keyClass = keyClass;
  }

  @Override
  public V get(K key) throws CacheException {
    try (Jedis jedis = redisTemplate.getResource()) {
      return getObject(key, jedis);
    }
  }

  @Override
  public V put(K key, V value) throws CacheException {
    return redisTemplate.transactionMulti(new RadisTransCallback<V>() {
      public V call(Transaction transaction) {
        byte[] byteKey = serializeKey(key);
        transaction.set(byteKey, SerializationUtil.serialize((Serializable) value));
        transaction.expire(byteKey, EXPIRE_SECONDS);
        return value;
      }
    });
  }

  /** 增加缓存过期时间. */
  public V put(K key, V value, long expireSeconds) throws CacheException {
    return redisTemplate.transactionMulti(new RadisTransCallback<V>() {
      public V call(Transaction transaction) {
        byte[] byteKey = serializeKey(key);
        transaction.set(byteKey, SerializationUtil.serialize((Serializable) value));
        transaction.expire(byteKey, expireSeconds);
        return value;
      }
    });
  }

  @Override
  public V remove(K key) throws CacheException {
    V obj = get(key);
    redisTemplate.transactionMulti(new RadisTransCallback<Boolean>() {
      public Boolean call(Transaction transaction) {
        transaction.del(serializeKey(key));
        return true;
      }
    });
    return obj;
  }

  @Override
  public void clear() throws CacheException {
    final Set<K> keys = keys();
    redisTemplate.transactionMulti(new RadisTransCallback<Boolean>() {
      public Boolean call(Transaction transaction) {
        for (K key : keys) {
          transaction.del(serializeKey(key));
        }
        return true;
      }
    });
  }

  @Override
  public int size() {
    return keys().size();
  }

  @Override
  @SuppressWarnings("unchecked")
  public Set<K> keys() {
    try (Jedis jedis = redisTemplate.getResource()) {
      Set<byte[]> keys = jedis.keys((keyPrefix + DELIMIT + "*").getBytes());
      Set<K> list = new HashSet<>();
      for (byte[] key : keys) {
        list.add((K) deserializeKey(key));
      }
      return list;
    }
  }

  @Override
  public Collection<V> values() {
    final Set<K> keys = keys();
    try (Jedis jedis = redisTemplate.getResource()) {
      Collection<V> list = new ArrayList<>();
      for (K key : keys) {
        V obj = getObject(key, jedis);
        list.add(obj);
      }
      return list;
    }
  }

  /**
   * Redis 获取key 反序列化对象.
   */
  @SuppressWarnings("unchecked")
  private V getObject(K key, Jedis jedis) {
    byte[] value = jedis.get(serializeKey(key));
    return (V) SerializationUtil.deserialize(value);
  }

  /** Key 序列化. */
  public byte[] serializeKey(K key) {
    byte[] keyPrefixByte = (keyPrefix + DELIMIT).getBytes();
    if (String.class.equals(keyClass)) {
      return mergedByte(keyPrefixByte, ((String) key).getBytes());
    } else if (UUID.class.equals(keyClass)) {
      String key32 = UUIDUtil.uuid32((UUID) key);
      return mergedByte(keyPrefixByte, UUIDUtil.uuidToByte(key32));
    } else if (SimplePrincipalCollection.class.equals(keyClass)) {
      if (key instanceof SimplePrincipalCollection) {
        // Shrio 权限和角色缓存信息(TokenID关联).
        SimplePrincipalCollection principal = (SimplePrincipalCollection) key;
        JsonWebToken token = (JsonWebToken) principal.getPrimaryPrincipal();
        return mergedByte(keyPrefixByte, token.getTokenID().getBytes());
      } else {
        return mergedByte(keyPrefixByte, ((String) key).getBytes());
      }
    } else {
      byte[] keyByte = SerializationUtil.serialize((Serializable) key);
      return mergedByte(keyPrefixByte, keyByte);
    }
  }

  /** Key 反序列化. */
  public Object deserializeKey(byte[] keyAllByte) {
    byte[] keyPrefixByte = (keyPrefix + DELIMIT).getBytes();
    byte[] keyByte = Arrays.copyOfRange(keyAllByte, keyPrefixByte.length, keyAllByte.length);
    if (String.class.equals(keyClass)) {
      return new String(keyByte);
    } else if (UUID.class.equals(keyClass)) {
      return UUIDUtil.byteToUUID(keyByte);
    } else if (SimplePrincipalCollection.class.equals(keyClass)) {
      return new String(keyByte);
    } else {
      return SerializationUtil.deserialize(keyByte);
    }
  }

  private byte[] mergedByte(byte[] keyPrefixByte, byte[] keyByte) {
    byte[] merge = new byte[keyPrefixByte.length + keyByte.length];
    System.arraycopy(keyPrefixByte, 0, merge, 0, keyPrefixByte.length);
    System.arraycopy(keyByte, 0, merge, keyPrefixByte.length, keyByte.length);
    return merge;
  }

}
