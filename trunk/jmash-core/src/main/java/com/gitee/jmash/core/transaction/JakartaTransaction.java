
package com.gitee.jmash.core.transaction;

import jakarta.persistence.EntityManager;

/**
 * Jakarta Transaction .
 *
 * @author CGD
 *
 */
public interface JakartaTransaction {

  public EntityManager getEntityManager();

  public void setEntityManager(EntityManager entityManager);

}
