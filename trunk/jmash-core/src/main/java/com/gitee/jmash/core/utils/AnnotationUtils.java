package com.gitee.jmash.core.utils;

import jakarta.inject.Named;
import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;

/** 注解匹配工具类. */
public class AnnotationUtils {

  /**
   * 静态方法来创建并返回 Named 注解实例.
   *
   * @param value 名称 可以为""
   * @return Named注解实例
   */
  public static Named getNamedAnnotation(final String value) {
    return new Named() {
      @Override
      public Class<? extends Annotation> annotationType() {
        return Named.class;
      }

      @Override
      public String value() {
        return value;
      }
    };
  }

  /** 嵌套查找注解. */
  public static <A extends Annotation> A findNestAnnotation(AnnotatedElement annotatedElement,
      Class<A> annotationType) {
    A annotation = annotatedElement.getAnnotation(annotationType);
    if (annotation != null) {
      return annotation;
    }
    for (Annotation declaredAnnotation : annotatedElement.getDeclaredAnnotations()) {
      annotation = findAnnotation(declaredAnnotation.annotationType(), annotationType);
      if (annotation != null) {
        return annotation;
      }
    }
    return null;
  }

  /** 单层查找注解. */
  public static <A extends Annotation> A findAnnotation(AnnotatedElement annotatedElement,
      Class<A> annotationType) {
    A annotation = annotatedElement.getAnnotation(annotationType);
    if (annotation != null) {
      return annotation;
    }
    return null;
  }

}
