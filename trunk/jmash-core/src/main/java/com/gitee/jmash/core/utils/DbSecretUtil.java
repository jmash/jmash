package com.gitee.jmash.core.utils;

import com.gitee.jmash.crypto.cipher.AesEcbUtil;
import com.gitee.jmash.crypto.cipher.CipherUtil;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Base64;
import java.util.Optional;
import javax.crypto.NoSuchPaddingException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.LogFactory;
import org.bouncycastle.util.encoders.Hex;
import org.eclipse.microprofile.config.Config;
import org.eclipse.microprofile.config.ConfigProvider;

/** 数据库加解密. */
public class DbSecretUtil {

  private static String SECRET_KEY = "jmash.jpa.key";

  private static byte[] secretKey;

  /** 获取密钥. */
  private static byte[] getSecretKey() {
    if (secretKey != null && secretKey.length > 0) {
      return secretKey;
    }
    Config config = ConfigProvider.getConfig();
    Optional<String> keyOptional = config.getOptionalValue(SECRET_KEY, String.class);
    String key = keyOptional.orElse("am1hc2gAAAAAAAAAAAAAAA==");
    secretKey = Base64.getDecoder().decode(key);
    return secretKey;
  }

  private static CipherUtil getCipher()
      throws NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException {
    return AesEcbUtil.get();
  }

  /** 加密. */
  public static String encrypt(String attribute) {
    if (StringUtils.isBlank(attribute)) {
      return StringUtils.EMPTY;
    }
    byte[] encrypted;
    try {
      encrypted = getCipher().encrypt(getSecretKey(), attribute.getBytes());
    } catch (Exception e) {
      LogFactory.getLog(DbSecretUtil.class).error(e);
      return attribute;
    }
    return Hex.toHexString(encrypted).toUpperCase();
  }

  /** 解密. */
  public static String decrypt(String dbData) {
    if (StringUtils.isBlank(dbData)) {
      return StringUtils.EMPTY;
    }
    byte[] encrypted;
    try {
      encrypted = getCipher().decrypt(getSecretKey(), Hex.decode(dbData));
    } catch (Exception e) {
      LogFactory.getLog(DbSecretUtil.class).error(e);
      return dbData;
    }
    return new String(encrypted);
  }

}
