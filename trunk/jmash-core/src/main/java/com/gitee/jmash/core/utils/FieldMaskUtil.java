
package com.gitee.jmash.core.utils;

import com.crenjoy.proto.beanutils.ProtoBeanUtils;
import com.gitee.jmash.common.text.StringUtil;
import com.gitee.jmash.core.service.ServiceException;
import com.google.protobuf.FieldMask;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/** FieldMask Util. */
public class FieldMaskUtil {

  private static Log log = LogFactory.getLog(FieldMaskUtil.class);

  /** Filter FieldMask . */
  public static FieldMask filterMask(final Object orig, final FieldMask fieldMask) {
    FieldMask.Builder newMask = FieldMask.newBuilder();
    // 更新资源的掩码FieldMask updateMask
    for (String path : fieldMask.getPathsList()) {
      String name = convert(path);
      try {
        // PropertyUtils直接获取Object Value.
        PropertyUtils.getProperty(orig, name);
        newMask.addPaths(path);
      } catch (Exception ex) {
        try {
          // repeated proto值获取.
          PropertyUtils.getProperty(orig, name + "List");
          newMask.addPaths(path);
        } catch (Exception e) {
          log.info("FieldMask filterMask Path : " + path);
        }
      }
    }
    return newMask.build();
  }
  
  /**
   * Copy Mask Properties.
   *
   * @param dest 目标实体
   * @param orig 源实体
   * @param fieldMask 掩码字段
   */
  public static void copyMask(final Object dest, final Object orig, final FieldMask fieldMask) {
    Map<String, Object> updateMap = FieldMaskUtil.getFieldValueMap(orig, fieldMask);
    FieldMaskUtil.updateEntityMap(dest, updateMap);
  }

  /** Mask是否包含字段(与),包含全部即返回true. */
  public static boolean containsAllFields(final FieldMask fieldMask, String... fields) {
    Set<String> pathMap =
        fieldMask.getPathsList().stream().map(path -> convert(path)).collect(Collectors.toSet());
    for (String field : fields) {
      if (!pathMap.contains(field)) {
        return false;
      }
    }
    return true;
  }

  /** Mask是否包含字段(或),包含一个即返回true. */
  public static boolean containsOneFields(final FieldMask fieldMask, String... fields) {
    Set<String> pathMap =
        fieldMask.getPathsList().stream().map(path -> convert(path)).collect(Collectors.toSet());
    for (String field : fields) {
      if (pathMap.contains(field)) {
        return true;
      }
    }
    return false;
  }

  /** POLO Entity通过FieldMask获取到掩码字段值Map. */
  public static Map<String, Object> getFieldValueMap(Object entity, FieldMask fieldMask) {
    Map<String, Object> map = new HashMap<>();
    // 更新资源的掩码FieldMask updateMask
    for (String path : fieldMask.getPathsList()) {
      String name = convert(path);
      try {
        // PropertyUtils直接获取Object Value.
        Object value = PropertyUtils.getProperty(entity, name);
        map.put(name, value);
      } catch (Exception ex) {
        try {
          // repeated proto值获取.
          Object value = PropertyUtils.getProperty(entity, name + "List");
          map.put(name, value);
        } catch (Exception e) {
          log.debug("FieldMask Update Path Error: " + path);
          throw new ServiceException("FieldMask Update Path Error: " + path);
        }
      }
    }
    return map;
  }

  /** 下划线转驼峰. */
  private static String convert(String path) {
    String name = path;
    if (path.contains("_")) {
      name = StringUtil.lowerCaseFirst(StringUtil.underlineToCamel(path));
    }
    return name;
  }

  /** Update Entity Property. */
  public static void updateEntityMap(Object entity, Map<String, Object> updateMap) {
    for (Map.Entry<String, Object> entry : updateMap.entrySet()) {
      try {
        ProtoBeanUtils.setProperty(entity, entry.getKey(), entry.getValue());
      } catch (Exception e) {
        log.error("FieldMask Update Entity " + entry.getKey(), e);
      }
    }
  }

}
