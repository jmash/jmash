
package com.gitee.jmash.core.utils;

import com.gitee.jmash.common.config.FileProps;
import com.gitee.jmash.common.enums.MimeType;
import com.gitee.jmash.common.utils.ImageUtil;
import com.gitee.jmash.common.utils.UUIDUtil;
import jakarta.enterprise.inject.spi.CDI;
import java.io.File;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.UUID;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.config.inject.ConfigProperties;


/**
 * File Path Util.
 *
 * @author CGD
 *
 */
public class FilePathUtil {

  public static FileProps getFileProps() {
    return CDI.current().select(FileProps.class, ConfigProperties.Literal.NO_PREFIX).get();
  }

  /** 租户路径. */
  public static String dirPath(String tenant, String serverDir) {
    if (StringUtils.isBlank(tenant)) {
      return serverDir;
    }
    if (StringUtils.isBlank(serverDir)) {
      return tenant;
    }
    return tenant + "/" + serverDir;
  }

  /**
   * 通过相对路径获取真实路径 _tmp/123.jpg转换为D:\\test\\_tmp\\123.jpg.
   * image/2016_12/123.doc转换为D:\\test\\image\\2016_12\\123.jpg.
   *
   * @param relativePath 相对路径
   * @param createDir 是否创建目录
   * @return 真实路径
   */
  public static String realPath(String relativePath, boolean createDir) {
    // 排除安全隐患.
    if (relativePath.contains("..")) {
      return null;
    }
    if (relativePath.contains(":")) {
      return null;
    }
    if (relativePath.contains(",")) {
      return null;
    }
    if (relativePath.contains("'")) {
      return null;
    }
    String realFileDir = internalRealPath(relativePath);
    // 父目录不存在,创建父目录.
    if (createDir && !(new File(realFileDir)).getParentFile().exists()) {
      (new File(realFileDir)).getParentFile().mkdirs();
    }
    return realFileDir;
  }

  /** 内部路径转换. */
  private static String internalRealPath(String relativePath) {
    // 临时文件路径
    if (relativePath.startsWith("_tmp/")) {
      return getFileProps().getTempPath() + File.separator
          + relativePath.substring(5).replace("/", File.separator);
    }
    if (relativePath.startsWith("_thumb/")) {
      return getFileProps().getThumbPath() + File.separator
          + relativePath.substring(7).replace("/", File.separator);
    } else {
      return getFileProps().getPath() + File.separator + relativePath.replace("/", File.separator);
    }
  }

  /** 创建新的可保存的相对文件路径. */
  public static String createNewFile(String oldFileName, String tenant, String fileDir) {
    return createNewFile(oldFileName, dirPath(tenant, fileDir));
  }

  /**
   * 创建新的可保存的相对文件路径.
   *
   * @param oldFileName 老文件名
   * @param fileDir 保存文件路径
   * @return 相对路径
   */
  public static String createNewFile(String oldFileName, String fileDir) {
    // 后缀名
    String anyfileExt = MimeType.getExtension(oldFileName).toLowerCase();
    // 目标文件路径
    if (StringUtils.isBlank(fileDir)) {
      fileDir = ImageUtil.getFileType(oldFileName).name();
    } else {
      fileDir = fileDir + "/" + ImageUtil.getFileType(oldFileName).name();
    }
    return fileDir + "/" + DateTimeFormatter.ofPattern("yyyy_MM").format(LocalDate.now()) + "/"
        + UUIDUtil.uuid32(UUID.randomUUID()) + "." + anyfileExt;
  }

  /**
   * 创建新的可保存的相对文件路径.
   *
   * @param oldFileName 老文件名
   * @param fileDir 保存文件路径
   * @return 相对路径
   */
  public static String createNewTempFile(String oldFileName, String fileDir) {
    // 后缀名
    String anyfileExt = MimeType.getExtension(oldFileName).toLowerCase();
    // 目标文件路径
    if (StringUtils.isEmpty(fileDir)) {
      fileDir = ImageUtil.getFileType(oldFileName).name();
    }
    return "_tmp/" + fileDir + "/" + DateTimeFormatter.ofPattern("yyyy_MM").format(LocalDate.now())
        + "/" + UUIDUtil.uuid32(UUID.randomUUID()) + "." + anyfileExt;
  }

  /**
   * 创建临时文件.
   *
   * @param fileName 临时文件名
   * @param fileDir 保存文件路径
   * @return 相对路径
   */
  public static String createTempFile(String fileName, String fileDir) {
    // 目标文件路径
    if (StringUtils.isEmpty(fileDir)) {
      fileDir = ImageUtil.getFileType(fileName).name();
    }
    return "_tmp/" + fileDir + "/" + DateTimeFormatter.ofPattern("yyyy_MM").format(LocalDate.now())
        + "/" + fileName;
  }

  /**
   * 创建新的可保存的相对文件路径.
   *
   * @param oldFileName 老文件名
   * @param fileDir 保存文件路径
   * @return 相对路径
   */
  public static String createNewThumbFile(String oldFileName, String fileDir) {
    // 后缀名
    String anyfileExt = MimeType.getExtension(oldFileName).toLowerCase();
    // 目标文件路径
    if (StringUtils.isEmpty(fileDir)) {
      fileDir = ImageUtil.getFileType(oldFileName).name();
    }
    return "_thumb/" + fileDir + "/"
        + DateTimeFormatter.ofPattern("yyyy_MM").format(LocalDate.now()) + "/"
        + UUIDUtil.uuid32(UUID.randomUUID()) + "." + anyfileExt;
  }
}
