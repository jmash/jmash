package com.gitee.jmash.core.utils;

import com.gitee.jmash.common.config.FileProps;
import com.gitee.jmash.common.enums.FileType;
import com.gitee.jmash.common.enums.MimeType;
import com.gitee.jmash.common.image.Thumbnail;
import com.gitee.jmash.common.image.Thumbnail.WebThumbType;
import com.google.api.HttpBody;
import com.google.protobuf.Any;
import com.google.protobuf.ByteString;
import com.google.protobuf.ByteString.Output;
import io.grpc.Status;
import io.grpc.stub.StreamObserver;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import javax.imageio.ImageIO;
import javax.imageio.stream.ImageOutputStream;
import jmash.protobuf.FileInfo;
import jmash.protobuf.FileUploadReq;
import jmash.protobuf.FileWebUploadReq;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;



/**
 * Grpc文件服务工具类.
 *
 * @author CGD
 */
public class FileServiceUtil {

  private static Log log = LogFactory.getLog(FileServiceUtil.class);
  // grpc 默认Stream 4M限制,这里使用3.5M+Stream Head
  public static final int MAX_FILE_SIZE = 4 * 1024 * 1024 - 512 * 1024;

  /** 上传文件. */
  public static StreamObserver<FileUploadReq> uploadFile(StreamObserver<FileInfo> respObserver) {
    return new StreamObserver<FileUploadReq>() {
      File file;
      String fileSrc;
      // 内容类型
      String contentType;

      @Override
      public void onNext(FileUploadReq req) {
        contentType = req.getContentType();
        if (null == file) {
          fileSrc = FilePathUtil.createNewFile(req.getFileName(), req.getFileDir());
          file = new File(FilePathUtil.realPath(fileSrc, true));
          if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
          }
        }
        try (FileOutputStream output = new FileOutputStream(file, true)) {
          output.write(req.getBody().toByteArray());
        } catch (Exception e) {
          log.error("Error:", e);
        }
      }

      @Override
      public void onError(Throwable t) {
        try {
          FileUtils.forceDelete(file);
        } catch (IOException e) {
          log.error("Error:", e);
        }
        log.error("Error:", t);
        respObserver.onError(Status.INTERNAL.withDescription(t.getMessage()).asException());
      }

      @Override
      public void onCompleted() {
        FileInfo.Builder builder = FileInfo.newBuilder();
        builder.setContentType(contentType);
        builder.setFileSrc(fileSrc);
        // 后缀名.
        String fileExt = MimeType.getExtension(fileSrc).toLowerCase();
        builder.setFileExt(fileExt);
        builder.setFileSize(file.length());
        respObserver.onNext(builder.build());
        respObserver.onCompleted();
      }
    };
  }

  /** 下载文件. */
  public static void downloadFile(StreamObserver<HttpBody> responseObserver, String filePath) {
    File file = new File(filePath);
    downloadFile(responseObserver, file);
  }

  /** 下载文件. */
  public static void downloadFile(StreamObserver<HttpBody> responseObserver, File file,
      Any... exts) {
    MimeType mimeType = MimeType.fileMimeType(file.getName());
    downloadFile(responseObserver, file, mimeType.getMimeType());
  }

  /** 下载文件. */
  public static void downloadFile(final StreamObserver<HttpBody> responseObserver, final File file,
      final String contentType, final Any... exts) {
    if (!file.exists()) {
      responseObserver
          .onError(Status.NOT_FOUND.withDescription("找不到文件" + file.getName()).asException());
      return;
    }
    if (file.isDirectory()) {
      responseObserver
          .onError(Status.NOT_FOUND.withDescription("不是文件，是文件夹：" + file.getName()).asException());
      return;
    }
    try (FileInputStream input = new FileInputStream(file)) {
      download(responseObserver, input, contentType, Arrays.asList(exts));
    } catch (Exception ex) {
      log.error("ERROR", ex);
      responseObserver.onError(Status.NOT_FOUND.withDescription(ex.getMessage()).asException());
    }
  }

  /** 下载图片文件. */
  public static void downloadImage(final StreamObserver<HttpBody> responseObserver,
      final BufferedImage image, MimeType mimeType, Any... exts) {
    if (null == mimeType || !FileType.image.equals(mimeType.getFileType())) {
      throw new RuntimeException("下载图片,必须设置图片类型!");
    }
    byte[] buf = getImageStream(image, mimeType.getFileExt());
    download(responseObserver, new ByteArrayInputStream(buf), mimeType.getMimeType(),
        Arrays.asList(exts));
  }

  /** 下载字节文件. */
  public static void downloadBuffer(StreamObserver<HttpBody> responseObserver, byte[] buf,
      String contentType, Any... exts) {
    download(responseObserver, new ByteArrayInputStream(buf), contentType, Arrays.asList(exts));
  }

  /** 下载文件流. */
  public static void download(StreamObserver<HttpBody> responseObserver, InputStream is,
      String contentType, List<Any> extensions) {
    HttpBody.Builder body = HttpBody.newBuilder().setContentType(contentType);
    if (!extensions.isEmpty()) {
      body.addAllExtensions(extensions);
    }
    try {
      int len = 0;
      byte[] buffer = new byte[MAX_FILE_SIZE];
      while ((len = is.read(buffer)) != -1) {
        try (Output out = ByteString.newOutput()) {
          out.write(buffer, 0, len);
          responseObserver.onNext(body.setData(out.toByteString()).build());
        }
      }
      is.close();
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("ERROR", ex);
      responseObserver
          .onError(Status.FAILED_PRECONDITION.withDescription(ex.getMessage()).asException());
    }
  }

  /** 图片转换为字节数组. */
  public static byte[] getImageStream(BufferedImage image, String formatName) {
    ByteArrayOutputStream bs = new ByteArrayOutputStream();
    ImageOutputStream imOut;
    try {
      imOut = ImageIO.createImageOutputStream(bs);
      ImageIO.write(image, formatName, imOut);
      return bs.toByteArray();
    } catch (IOException ex) {
      log.error("", ex);
    }
    return new byte[0];
  }

  /**
   * 缩略图.
   *
   * @param fileSrc 文件Src
   * @param thumbType 缩略类型
   * @param width 宽度
   * @param height 高度
   * @return 路径
   */
  public static String thumb(String fileSrc, WebThumbType thumbType, int width, int height) {
    try {
      if (0 == width && 0 == height) {
        return fileSrc;
      }
      String fileExt = WebThumbType.trans.equals(thumbType) ? MimeType.M_png.getFileExt()
          : thumbType.getFileExt();
      String fileName = FilePathUtil.realPath(fileSrc, false);
      String destFile =
          String.format("_thumb/%s_%d_%d_%s.%s", fileSrc, width, height, thumbType.name(), fileExt);
      String thumbPath = FilePathUtil.realPath(destFile, true);

      File descFile = new File(thumbPath);
      if (!descFile.exists()) {
        // 目录转换时，缩略图目录可能不存在需要先创建目录
        if (!descFile.getParentFile().exists()) {
          descFile.getParentFile().mkdirs();
        }
        Thumbnail thumb = new Thumbnail(fileName);
        thumb.setDestFile(thumbPath);
        if (WebThumbType.trans.equals(thumbType)) {
          thumb.setMimeType(MimeType.M_png);
        }
        thumb.resizeWeb(width, height, thumbType);
      }
      return destFile;
    } catch (Exception ex) {
      log.error("", ex);
      return "";
    }
  }

  /** 文件大小. */
  public static int filePart(long fileSize) {
    Long part = fileSize / MAX_FILE_SIZE;
    if (fileSize % MAX_FILE_SIZE > 0) {
      return part.intValue() + 1;
    }
    return part.intValue();
  }

  /** 上传校验. */
  public static boolean uploadVerify(Long fileSize, String fileName, String contentType,
      StreamObserver<FileInfo> responseObserver, FileProps fileProps) {
    if (fileSize > fileProps.getMaxSize()) {
      String message = String.format("超过允许的超过最大文件长度限制%s", fileProps.maxSizeDisplay());
      responseObserver.onError(Status.OUT_OF_RANGE.withDescription(message).asException());
      return false;
    }

    // 后缀名.
    String fileExt = MimeType.getExtension(fileName).toLowerCase();

    // 文件类型.
    MimeType mimeType = MimeType.convert(fileExt);
    if (null == mimeType) {
      String message = String.format("服务器不支持该文件类型，后缀为%s的文件。", fileExt);
      responseObserver.onError(Status.PERMISSION_DENIED.withDescription(message).asException());
      return false;
    }

    // 严格控制服务器接收类型，不接受的文件.
    if (!MimeType.acceptMimeType().contains(mimeType)) {
      String message = String.format("服务器不接受该文件类型，后缀为%s的文件。", fileExt);
      responseObserver.onError(Status.PERMISSION_DENIED.withDescription(message).asException());
      return false;
    }

    // 文件类型不匹配的文件,记录
    if (!MimeType.mimeType(contentType).contains(mimeType)) {
      String message = String.format("您上传的%s文件类型与文件后缀不匹配，请检查！", fileName);
      log.warn(message + String.format(" 实际 %s ，期望 %s ", contentType, mimeType.getMimeType()));
    }
    return true;
  }

  /** 上次文件保存. */
  public static File uploadFileWebSave(FileWebUploadReq req,
      StreamObserver<FileInfo> responseObserver, FileProps fileProps) {
    boolean verify = uploadVerify(req.getFileSize(), req.getFileName(), req.getContentType(),
        responseObserver, fileProps);
    if (!verify) {
      return null;
    }
    String tempFile = FilePathUtil.createTempFile(req.getRequestId(), req.getFileDir());
    File file = new File(FilePathUtil.realPath(tempFile, true));
    if (!file.getParentFile().exists()) {
      file.getParentFile().mkdirs();
    }

    try (FileOutputStream output = new FileOutputStream(file, true)) {
      output.write(req.getBody().toByteArray());
    } catch (Exception e) {
      log.error("Error:", e);
    }
    return file;
  }

}
