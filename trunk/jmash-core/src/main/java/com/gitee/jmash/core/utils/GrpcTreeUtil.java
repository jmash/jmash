package com.gitee.jmash.core.utils;

import com.google.protobuf.MessageOrBuilder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Grpc Tree Build.
 *
 * @author cgd.
 */
public class GrpcTreeUtil {

  /**
   * 将给定的扁平化层级数据列表转换为带有层级关系的结构化列表.
   *
   * @param flatItemList 扁平化的层级数据列表
   * @param idExtractor 提取项ID的函数
   * @param parentIdExtractor 提取父项ID的函数
   * @param builderFactory 从项创建构建器的工厂方法
   * @param buildFactory builderFactory反向
   * @return 结构化列表
   */
  public static <T extends MessageOrBuilder, U extends MessageOrBuilder> List<T> buildTree(
      List<T> flatItemList, Function<T, String> idExtractor, Function<T, String> parentIdExtractor,
      Function<T, U> builderFactory, Function<U, T> buildFactory,
      BiConsumer<U, U> addChildFunction) {
    // 全部信息映射
    Map<String, U> itemMap = flatItemList.stream()
        .collect(Collectors.toMap(idExtractor, builderFactory, (oldValue, newValue) -> oldValue));

    // 倒序添加孩子
    List<T> reversedList = new ArrayList<>(flatItemList);
    Collections.reverse(reversedList);

    // 数据建立父子关系
    List<T> roots = new ArrayList<>();
    for (T item : reversedList) {
      String itemId = idExtractor.apply(item);
      String parentId = parentIdExtractor.apply(item);
      U itemBuilder = itemMap.get(itemId);
      U parentBuilder = itemMap.get(parentId);
      if (parentBuilder == null) {
        roots.add(0, buildFactory.apply(itemBuilder));
      } else {
        addChildFunction.accept(parentBuilder, itemBuilder);
      }
    }
    return roots;
  }
}
