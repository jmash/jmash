package com.gitee.jmash.core.utils;

import com.crenjoy.proto.beanutils.ProtoBeanUtils;
import com.gitee.jmash.core.jaxrs.models.JsonFieldMask;
import com.gitee.jmash.core.service.ServiceException;
import java.beans.PropertyDescriptor;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/** Json FieldMask Util. */
public class JsonFieldMaskUtil {

  private static Log log = LogFactory.getLog(JsonFieldMaskUtil.class);

  /** Filter FieldMask . */
  public static JsonFieldMask filterMask(final Object orig, final JsonFieldMask fieldMask) {
    JsonFieldMask r = new JsonFieldMask();
    for (String path : fieldMask.getPaths()) {
      try {
        PropertyDescriptor pd = PropertyUtils.getPropertyDescriptor(orig, path);
        if (pd != null) {
          r.addPaths(path);
        }
      } catch (Exception ex) {
        log.info("JsonFieldMask Path Error: " + path);
      }
    }
    return r;
  }

  /**
   * Copy Mask Properties.
   *
   * @param dest 目标实体
   * @param orig 源实体
   * @param fieldMask 掩码字段
   */
  public static void copyMask(final Object dest, final Object orig, final JsonFieldMask fieldMask) {
    Map<String, Object> updateMap = JsonFieldMaskUtil.getFieldValueMap(orig, fieldMask);
    FieldMaskUtil.updateEntityMap(dest, updateMap);
  }

  /** POLO Entity通过FieldMask获取到掩码字段值Map. */
  public static Map<String, Object> getFieldValueMap(Object entity, JsonFieldMask fieldMask) {
    Map<String, Object> map = new HashMap<>();
    // 更新资源的掩码FieldMask updateMask
    for (String path : fieldMask.getPaths()) {
      try {
        Object value = PropertyUtils.getProperty(entity, path);
        map.put(path, value);
      } catch (Exception ex) {
        log.debug("JsonFieldMask Update Path Error: " + path);
        throw new ServiceException("FieldMask Update Path Error: " + path);
      }
    }
    return map;
  }

  /** Update Entity Property. */
  public static void updateEntityMap(Object entity, Map<String, Object> updateMap) {
    for (Map.Entry<String, Object> entry : updateMap.entrySet()) {
      try {
        ProtoBeanUtils.setProperty(entity, entry.getKey(), entry.getValue());
      } catch (Exception e) {
        log.error("FieldMask Update Entity " + entry.getKey(), e);
      }
    }
  }
}
