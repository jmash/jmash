package com.gitee.jmash.core.utils;

import jakarta.json.bind.Jsonb;
import jakarta.json.bind.JsonbBuilder;
import jakarta.ws.rs.core.GenericType;
import org.apache.commons.logging.LogFactory;

/**
 * Json 与实体间转换.
 */
public class JsonbUtil {

  /** JSON转实体. */
  public static <T> T fromJson(String json, Class<T> clazz) {
    try (Jsonb jsonb = JsonbBuilder.create()) {
      return jsonb.fromJson(json, clazz);
    } catch (Exception ex) {
      LogFactory.getLog(JsonbUtil.class).error("", ex);
    }
    return null;
  }

  /** JSON转实体. */
  public static <T> T fromJson(String json, GenericType<T> genericType) {
    try (Jsonb jsonb = JsonbBuilder.create()) {
      return jsonb.fromJson(json, genericType.getType());
    } catch (Exception ex) {
      LogFactory.getLog(JsonbUtil.class).error("", ex);
    }
    return null;
  }
  
  /** 实体转JSON. */
  public static String toJson(Object obj) {
    try (Jsonb jsonb = JsonbBuilder.create()) {
      return jsonb.toJson(obj);
    } catch (Exception ex) {
      LogFactory.getLog(JsonbUtil.class).error("", ex);
    }
    return "";
  }
  
}
