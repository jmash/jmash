package com.gitee.jmash.core.utils;

import com.gitee.jmash.crypto.AsymmetricUtil;
import com.gitee.jmash.crypto.sm2.Sm2AsymmetricUtil;
import com.gitee.jmash.crypto.sm2.Sm2KeyPairUtil;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Optional;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.LogFactory;
import org.eclipse.microprofile.config.Config;
import org.eclipse.microprofile.config.ConfigProvider;

/** 网络数据加解密. */
public class NetSecretUtil {

  private static String PRIVATE_KEY = "jmash.network.privatekey";

  private static String PUBLIC_KEY = "jmash.network.publickey";

  private static PrivateKey privateKey;

  private static PublicKey publicKey;

  /** 获取密钥. */
  private static PrivateKey getPrivateKey() throws Exception {
    if (privateKey != null) {
      return privateKey;
    }
    Config config = ConfigProvider.getConfig();
    Optional<String> keyOptional = config.getOptionalValue(PRIVATE_KEY, String.class);
    String key =
        keyOptional.orElse("ed083af5fefd32c049e93c7d38ed39213a2c0c72fd68ea2de318c1325509241b");
    privateKey = Sm2KeyPairUtil.get().getPrivateKey(key);
    return privateKey;
  }

  /** 获取密钥. */
  private static PublicKey getPublicKey() throws Exception {
    if (publicKey != null) {
      return publicKey;
    }
    Config config = ConfigProvider.getConfig();
    Optional<String> keyOptional = config.getOptionalValue(PUBLIC_KEY, String.class);
    String key = keyOptional.orElse(
        "04ad5161202264bb7c201f4cc39d6fb00fed81fc2cec1b3f23ddfb933d3f32977be40e4285cff6da4bad65933777c135b95f41846363086e34624ff73b3685e84c");
    publicKey = Sm2KeyPairUtil.get().getPublicKey(key);
    return publicKey;
  }

  private static AsymmetricUtil getAsymmetric() {
    return Sm2AsymmetricUtil.get();
  }

  /** 加密. */
  public static String encrypt(String attribute) {
    if (StringUtils.isBlank(attribute)) {
      return StringUtils.EMPTY;
    }
    try {
      return getAsymmetric().encryptAndEncode(attribute.getBytes(), getPublicKey());
    } catch (Exception e) {
      LogFactory.getLog(NetSecretUtil.class).error(e);
      return attribute;
    }
  }

  /** 解密. */
  public static String decrypt(String dbData) {
    if (StringUtils.isBlank(dbData)) {
      return StringUtils.EMPTY;
    }
    byte[] encrypted;
    try {
      encrypted = getAsymmetric().decrypt(dbData, getPrivateKey());
    } catch (Exception e) {
      LogFactory.getLog(NetSecretUtil.class).error(e);
      return dbData;
    }
    return new String(encrypted);
  }

}
