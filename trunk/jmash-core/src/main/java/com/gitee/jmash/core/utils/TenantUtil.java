package com.gitee.jmash.core.utils;

import com.gitee.jmash.core.orm.tenant.JpaTenantIdentifier;
import com.gitee.jmash.core.orm.tenant.TenantIdentifier;
import org.apache.commons.lang3.StringUtils;

/** 租户工具类. */
public class TenantUtil {

  /** 是否包含租户标识. */
  public static boolean hasTenantIdentifier(String tenant) {
    return StringUtils.isNotBlank(tenant) && tenant.contains("@");
  }

  public static TenantIdentifier getTenantIdentifier(String tenant) {
    return new TenantIdentifier(getTenantIdentifierStr(tenant));
  }

  /** 0191c4439482453799a3996ca7a2528c@core 返回 0191c4439482453799a3996ca7a2528c. */
  public static String getTenantIdentifierStr(String tenant) {
    if (hasTenantIdentifier(tenant)) {
      return tenant.split("@")[0].toLowerCase();
    }
    return TenantIdentifier.DEFAULT;
  }

  /** 0191c4439482453799a3996ca7a2528c@core 返回 core. */
  public static String getTenantName(String tenant) {
    if (hasTenantIdentifier(tenant)) {
      return tenant.split("@")[1];
    }
    return tenant;
  }

  /** 数据插入前，租户设置. */
  public static void persistPre(Object entity, TenantIdentifier tenantIdentifier) {
    if (entity instanceof JpaTenantIdentifier) {
      ((JpaTenantIdentifier) entity).setTenantId(tenantIdentifier);
    }
  }

  /** 数据merge前,租户设置/检查. */
  public static void mergePre(Object entity, TenantIdentifier tenantIdentifier) {
    if (entity instanceof JpaTenantIdentifier) {
      JpaTenantIdentifier tenantEntity = ((JpaTenantIdentifier) entity);
      if (null == tenantEntity.getTenantId()) {
        ((JpaTenantIdentifier) entity).setTenantId(tenantIdentifier);
      } else if (!allowOper(tenantIdentifier, tenantEntity.getTenantId())) {
        throw new RuntimeException("禁止跨租户插入或更新数据!");
      }
    }
  }

  /** 删除数据，租户检查. */
  public static void removePre(Object entity, TenantIdentifier tenantIdentifier) {
    if (entity instanceof JpaTenantIdentifier) {
      JpaTenantIdentifier tenantEntity = ((JpaTenantIdentifier) entity);
      if (!allowOper(tenantIdentifier, tenantEntity.getTenantId())) {
        throw new RuntimeException("禁止跨租户删除数据!");
      }
    }
  }

  /** 是否允许操作数据. */
  public static boolean allowOper(TenantIdentifier useTenant, TenantIdentifier dataTenant) {
    if (TenantIdentifier.isDefault(useTenant)) {
      return true;
    }
    return useTenant.equals(dataTenant);
  }


}
