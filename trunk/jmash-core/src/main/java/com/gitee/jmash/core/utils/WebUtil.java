
package com.gitee.jmash.core.utils;

import jakarta.servlet.http.HttpServletRequest;
import java.util.LinkedHashSet;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 获取web用户的ip地址情况.
 *
 * @author CGD
 * 
 */
public class WebUtil {

  /**
   * 访问网站IP地址序列.
   */
  public static String[] forwardIp(HttpServletRequest request) {
    if (request == null) {
      return new String[0];
    }
    Set<String> forwordIps = new LinkedHashSet<String>();
    String realIps = request.getHeader("x-forwarded-for");
    if (!StringUtils.isEmpty(realIps)) {
      while ((!StringUtils.isEmpty(realIps))
          && StringUtils.equalsIgnoreCase("unknown", realIps.trim())) {
        realIps = request.getHeader("x-forwarded-for");
      }
    }
    addIps(forwordIps, realIps);

    realIps = request.getHeader("X-Real-IP");
    addIps(forwordIps, realIps);

    realIps = request.getHeader("Proxy-Client-IP");
    addIps(forwordIps, realIps);

    realIps = request.getHeader("WL-Proxy-Client-IP");
    addIps(forwordIps, realIps);

    realIps = request.getRemoteAddr();
    addIps(forwordIps, realIps);

    return forwordIps.toArray(new String[0]);
  }

  /**
   * 增加IP 不重复.
   */
  private static void addIps(Set<String> ips, String ipStrs) {
    if (StringUtils.isEmpty(ipStrs)) {
      return;
    }
    String[] ipArray = ipStrs.split(",");
    for (String ip : ipArray) {
      if (!StringUtils.isEmpty(ip) && !StringUtils.equalsIgnoreCase("unknown", ip.trim())) {
        ips.add(ip.trim());
      }
    }
  }

  /**
   * 获取用户的真实IP地址.
   */
  public static String realityIp(HttpServletRequest request) {
    if (request == null) {
      return StringUtils.EMPTY;
    }
    String[] forwardIps = forwardIp(request);
    if (forwardIps.length > 0) {
      return forwardIps[0];
    }
    return StringUtils.EMPTY;
  }

  /**
   * 获取用户的最后一次代理IP.
   */
  public static String proxyIp(HttpServletRequest request) {
    if (request == null) {
      return StringUtils.EMPTY;
    }
    String[] forwardIps = forwardIp(request);
    if (forwardIps.length > 1) {
      return forwardIps[1];
    }
    return StringUtils.EMPTY;
  }

  /** Main. */
  public static void main(String[] args) {
    Set<String> forwardIps = new LinkedHashSet<String>();
    WebUtil.addIps(forwardIps, "171.116.104.210, 112.124.214.6");
    WebUtil.addIps(forwardIps, "112.124.214.6");
    WebUtil.addIps(forwardIps, "171.116.104.210, 112.124.214.6");
    WebUtil.addIps(forwardIps, "127.0.0.1");
    for (String ip : forwardIps.toArray(new String[0])) {
      Log log = LogFactory.getLog(WebUtil.class);
      log.info(ip);
    }
  }
}
