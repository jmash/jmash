
package com.gitee.jmash.core.validate;

import com.gitee.jmash.common.grpc.GrpcContext;
import java.util.Locale;
import org.hibernate.validator.spi.messageinterpolation.LocaleResolver;
import org.hibernate.validator.spi.messageinterpolation.LocaleResolverContext;

/**
 * Grpc LocaleResolver.
 *
 * @author CGD
 *
 */
public class GrpcLocaleResolver implements LocaleResolver {

  @Override
  public Locale resolve(LocaleResolverContext context) {
    Locale locale = GrpcContext.GRPC_LOCALE.get();
    return locale;
  }

}
