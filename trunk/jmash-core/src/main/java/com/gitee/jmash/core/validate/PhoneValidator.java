
package com.gitee.jmash.core.validate;

import com.gitee.jmash.core.validator.constraint.Phone;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Mobile Phone Validator.
 *
 * @author CGD
 *
 */
public class PhoneValidator implements ConstraintValidator<Phone, String> {

  private static final Pattern PATTERN = Pattern.compile("^[1][3,4,5,6,7,8,9][0-9]{9}$");

  @Override
  public boolean isValid(String value, ConstraintValidatorContext context) {
    if (value == null || value.length() == 0) {
      return true;
    }

    Matcher matcher = PATTERN.matcher(value);
    return matcher.matches();
  }

}
