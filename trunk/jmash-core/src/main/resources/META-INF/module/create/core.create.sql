/*!40101 SET NAMES utf8 */;
CREATE TABLE os_properties(
    `entity_id` VARCHAR(36) NOT NULL   COMMENT '属性实体ID' ,
    `entity_name` VARCHAR(30) NOT NULL   COMMENT '属性实体名称' ,
    `key_name` VARCHAR(30) NOT NULL   COMMENT '属性Key' ,
    `bool_value` TINYINT    COMMENT '' ,
    `data_` BLOB    COMMENT '' ,
    `date_value` DATETIME    COMMENT '' ,
    `double_value` DECIMAL(24,6)    COMMENT '' ,
    `int_value` INT    COMMENT '' ,
    `long_value` BIGINT(20)    COMMENT '' ,
    `object_data` BLOB    COMMENT '' ,
    `string_value` VARCHAR(255)    COMMENT '' ,
    `text_value` TEXT    COMMENT '' ,
    `type_` INT    COMMENT '属性类型' ,
    `create_time` TIMESTAMP NOT NULL   COMMENT '创建时间' ,
    `update_time` TIMESTAMP NULL   COMMENT '更新时间' ,
    PRIMARY KEY (entity_id,entity_name,key_name)
)  COMMENT = '属性表' ENGINE=InnoDB DEFAULT CHARSET=utf8;

create index Index_1 on os_properties
(
   entity_id,
   entity_name
);

CREATE TABLE `hibernate_sequences` (
  `sequence_name` varchar(255) NOT NULL,
  `next_val` bigint(20) DEFAULT NULL,
  `sequence_next_hi_value` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`sequence_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;