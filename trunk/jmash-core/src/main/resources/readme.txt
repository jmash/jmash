1、主库创建写用户  

create user 'dev_write'@'%' identified by 'jmash123456';
grant all privileges on *.* to 'dev_write'@'%';
flush privileges;

SET PASSWORD FOR 'dev_write'@'%' = PASSWORD('jmash123456');

2、备库创建读用户  

create user 'dev_read'@'%' identified by 'jmash123456';
grant SELECT  on *.* to 'dev_read'@'%';
flush privileges;

SET PASSWORD FOR 'dev_read'@'%' = PASSWORD('jmash123456');
