
package com.gitee.jmash.core.module.test;

import com.gitee.jmash.core.orm.module.DbBuild;
import jakarta.enterprise.inject.se.SeContainer;
import jakarta.enterprise.inject.se.SeContainerInitializer;
import java.sql.SQLException;
import org.junit.Test;

/** 自动数据库构建. */
public class DbBuildTest {

  @Test
  public void test() throws SQLException {
    try (SeContainer container = SeContainerInitializer.newInstance().initialize()) {
      // 构建数据库.
      DbBuild dbBuild = container.select(DbBuild.class).get();
      dbBuild.build();
    }
  }

}
