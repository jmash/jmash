
package com.gitee.jmash.core.orm.jdbc.test;

import com.gitee.jmash.common.utils.ConfigSecretUtil;
import com.gitee.jmash.core.orm.data.DataSet;
import com.gitee.jmash.core.orm.data.DataTable;
import com.gitee.jmash.core.orm.jdbc.DataSourceConnectionAccess;
import com.gitee.jmash.core.orm.jdbc.JdbcTemplate;
import java.sql.SQLException;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.microprofile.config.Config;
import org.eclipse.microprofile.config.ConfigProvider;
import org.junit.Assert;
import org.junit.Test;
import org.mariadb.jdbc.MariaDbDataSource;

/** JDBC 测试. */
public class JdbcTemplateTest {

  private static Log log = LogFactory.getLog(JdbcTemplateTest.class);

  @Test
  public void test() throws SQLException {

    Config config = ConfigProvider.getConfig();
    String url = config.getValue("write.jdbc.url", String.class);
    String username = config.getValue("write.jdbc.username", String.class);
    String password = config.getValue("write.jdbc.password", String.class);
    password = ConfigSecretUtil.decrypt(password);

    MariaDbDataSource ds = new MariaDbDataSource(url);

    ds.setUser(username);
    ds.setPassword(password);
    JdbcTemplate jt = new JdbcTemplate(new DataSourceConnectionAccess(ds), false);

    List<String> schemas = jt.getSchemas();
    log.info(schemas.get(0));
    log.info(schemas.get(1));
    log.info(jt.existSchema("jmash_nft"));

    DataSet dataset = jt.getTables();

    for (int i = 0; i < dataset.size(); i++) {
      DataTable dt = dataset.getDataTable(i);
      log.info(dt.getTableName());
      dt = jt.getColumns(dt);
      dt = jt.getPrimaryKeys(dt);
      dt = jt.getIndexs(dt);
    }
    Assert.assertEquals(jt.existTables("cms_article", "cms_a"), false);
    Assert.assertEquals(jt.existTables("test_a"), false);
    log.info(jt.getDataBaseName());

  }

}
