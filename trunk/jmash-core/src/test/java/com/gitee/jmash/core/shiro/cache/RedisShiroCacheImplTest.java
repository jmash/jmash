package com.gitee.jmash.core.shiro.cache;

import com.gitee.jmash.common.redis.RedisProps;
import com.gitee.jmash.common.redis.RedisUtil;
import com.gitee.jmash.common.redis.callback.RedisTemplate;
import java.io.Serializable;
import org.apache.shiro.cache.Cache;
import org.eclipse.microprofile.config.Config;
import org.eclipse.microprofile.config.ConfigProvider;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import redis.clients.jedis.JedisPool;

class RedisShiroCacheImplTest {

  private static Cache<Serializable, Serializable> sessions;


  public static RedisProps getRedisProps() {
    RedisProps redisProps = new RedisProps();
    redisProps.setClientName("TestClient");
    Config config = ConfigProvider.getConfig();
    redisProps.setHost(config.getValue("jedis.host", String.class));
    redisProps.setPort(config.getValue("jedis.port", Integer.class));
    redisProps.setPassword(config.getValue("jedis.password", String.class));
    return redisProps;
  }

  /**
   * Setup.
   */
  @BeforeAll
  public static void setUpBeforeClass() {
    RedisProps redisProps = getRedisProps();
    // kubectl port-forward -n jmash redis 6379:6379
    JedisPool jdeisPool = RedisUtil.getJedisPool(redisProps);
    RedisTemplate redisTemplate = new RedisTemplate(jdeisPool);
    sessions = new RedisShiroCacheImpl<>(redisTemplate, Serializable.class, "test0");
  }

  @AfterAll
  public static void tearDownAfterClass() throws Exception {
    // sessions.clear();
  }

  @Test
  void test1() {
    sessions.put("test0", "abc");
    Assertions.assertEquals(sessions.get("test0"), "abc");
    sessions.remove("test0");
    Assertions.assertEquals(sessions.get("test0"), null);
  }

  @Test
  void test2() {
    sessions.put("test1", "abc1");
    sessions.put("test2", "abc2");
    sessions.put("test3", "abc3");
    Assertions.assertEquals(sessions.size(), 3);
    sessions.remove("test1");
    Assertions.assertEquals(sessions.size(), 2);
  }



}
