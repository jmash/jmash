package com.gitee.jmash.core.shiro.cache;

import com.gitee.jmash.common.redis.RedisProps;
import com.gitee.jmash.common.redis.RedisUtil;
import com.gitee.jmash.common.redis.callback.RedisTemplate;
import org.apache.shiro.cache.Cache;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import redis.clients.jedis.JedisPool;

class RedisShiroCacheImplTest2 {

  private static Cache<String, String> sessions;

  /**
   * Setup.
   */
  @BeforeAll
  public static void setUpBeforeClass() {
    RedisProps redisProps = RedisShiroCacheImplTest.getRedisProps();
    redisProps.setClientName("TestClient");
    // kubectl port-forward -n jmash redis 6379:6379
    JedisPool jdeisPool = RedisUtil.getJedisPool(redisProps);
    RedisTemplate redisTemplate = new RedisTemplate(jdeisPool);
    sessions = new RedisShiroCacheImpl<>(redisTemplate, String.class, "test1");
  }

  @AfterAll
  public static void tearDownAfterClass() throws Exception {
    //sessions.clear();
  }

  @Test
  void test1() {
    sessions.put("test0", "abc");
    Assertions.assertEquals(sessions.get("test0"), "abc");
    sessions.remove("test0");
    Assertions.assertEquals(sessions.get("test0"), null);
  }

  @Test
  void test2() {
    sessions.put("test1", "abc1");
    sessions.put("test2", "abc2");
    sessions.put("test3", "abc3");
    Assertions.assertEquals(sessions.size(), 3);
    sessions.remove("test1");
    Assertions.assertEquals(sessions.size(), 2);
  }



}
