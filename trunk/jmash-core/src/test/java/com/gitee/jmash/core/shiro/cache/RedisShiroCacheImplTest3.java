package com.gitee.jmash.core.shiro.cache;

import com.gitee.jmash.common.redis.RedisProps;
import com.gitee.jmash.common.redis.RedisUtil;
import com.gitee.jmash.common.redis.callback.RedisTemplate;
import java.util.UUID;
import org.apache.shiro.cache.Cache;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import redis.clients.jedis.JedisPool;

class RedisShiroCacheImplTest3 {

  private static Cache<UUID, String> sessions;

  /**
   * Setup.
   */
  @BeforeAll
  public static void setUpBeforeClass() {
    RedisProps redisProps = RedisShiroCacheImplTest.getRedisProps();
    redisProps.setClientName("TestClient");
    // kubectl port-forward -n jmash redis 6379:6379
    JedisPool jdeisPool = RedisUtil.getJedisPool(redisProps);
    RedisTemplate redisTemplate = new RedisTemplate(jdeisPool);
    sessions = new RedisShiroCacheImpl<>(redisTemplate, UUID.class, "test2");
  }

  @AfterAll
  public static void tearDownAfterClass() throws Exception {
    // sessions.clear();
  }

  @Test
  void test1() {
    UUID t = UUID.randomUUID();
    sessions.put(t, "abc");
    Assertions.assertEquals(sessions.get(t), "abc");
    sessions.remove(t);
    Assertions.assertEquals(sessions.get(t), null);
  }

  @Test
  void test2() {
    UUID t = UUID.randomUUID();
    sessions.put(t, "abc1");
    sessions.put(UUID.randomUUID(), "abc2");
    sessions.put(UUID.randomUUID(), "abc3");
    Assertions.assertEquals(sessions.size(), 3);
    sessions.remove(t);
    Assertions.assertEquals(sessions.size(), 2);
  }



}
