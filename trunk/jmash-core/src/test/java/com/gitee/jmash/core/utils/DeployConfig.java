package com.gitee.jmash.core.utils;

import com.gitee.jmash.common.utils.ConfigSecretUtil;
import com.gitee.jmash.common.utils.RsaUtil;
import java.security.KeyPair;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * 部署配置信息生成.
 */
public class DeployConfig {

  /**
   * microprofile-config.properties 信息加密.
   */
  @Test
  void configEncrypt() {
    // test 加密结果.
    String encrypt = "C803448FB6A6170EFA0C8BF449C6064F";
    Assertions.assertEquals(encrypt, ConfigSecretUtil.encrypt("test"));
    String original = "test";
    String result = ConfigSecretUtil.encrypt(original);
    System.out.println("microprofile-config.properties 信息加密");
    System.out.println("原文信息：" + original);
    System.out.println("加密信息：" + result);
  }

  /**
   * 生成Token密钥.
   */
  @Test
  void token() throws Exception {
    // 签名密钥
    KeyPair sign = RsaUtil.genKeyPair();
    // 加解密密钥
    KeyPair encrypt = RsaUtil.genKeyPair();
    System.out.println("");

    System.out.println("#JWT Server Config 仅 RBAC 服务端配置 ");
    System.out.println("smallrye.jwt.sign.key=" + RsaUtil.getBase64Key(sign.getPrivate()));
    System.out.println("smallrye.jwt.encrypt.key=" + RsaUtil.getBase64Key(encrypt.getPublic()));
    System.out.println("");
    System.out.println("#JWT Client Config 与服务端配套使用 ");
    System.out.println("mp.jwt.verify.publickey=" + RsaUtil.getBase64Key(sign.getPublic()));
    System.out.println("smallrye.jwt.decrypt.key=" + RsaUtil.getBase64Key(encrypt.getPrivate()));
    
  }



}
