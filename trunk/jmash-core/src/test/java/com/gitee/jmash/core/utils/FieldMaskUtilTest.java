package com.gitee.jmash.core.utils;

import com.google.protobuf.FieldMask;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class FieldMaskUtilTest {

  FieldMask mask = FieldMask.newBuilder().addPaths("jmash_abc").addPaths("jmash_bcd")
      .addPaths("workTime").addPaths("workArea").build();

  @Test
  void test1() {
    boolean a = FieldMaskUtil.containsAllFields(mask, "jmashAbc", "workTime");
    Assertions.assertEquals(a, true);
  }

  @Test
  void test2() {
    boolean a = FieldMaskUtil.containsAllFields(mask, "jmashAbc", "workTime1");
    Assertions.assertEquals(a, false);
  }

  @Test
  void test3() {
    boolean a = FieldMaskUtil.containsOneFields(mask, "jmashAbc", "workTime1", "workArea");
    Assertions.assertEquals(a, true);
  }

  @Test
  void test4() {
    boolean a = FieldMaskUtil.containsOneFields(mask, "jmash_abc", "jmash_bcd");
    Assertions.assertEquals(a, false);
  }

}
