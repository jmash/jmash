
package com.gitee.jmash.core.validate.test;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import java.util.Locale;
import java.util.Set;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

/**
 * Car Test.
 *
 * @author CGD
 *
 */
public class CarTest {

  private static Validator validator;

  @BeforeAll
  public static void setUpValidator() {
    Locale.setDefault(Locale.US);
    validator = Validation.buildDefaultValidatorFactory().getValidator();
  }

  @Test
  public void manufacturerIsNull() {
    Car car = new Car(null, "DD-AB-123", 4);

    Set<ConstraintViolation<Car>> constraintViolations = validator.validate(car);

    Assertions.assertEquals(1, constraintViolations.size());
    Assertions.assertEquals("must not be null",
        constraintViolations.iterator().next().getMessage());
  }

  @Test
  public void licensePlateTooShort() {
    Car car = new Car("Morris", "D", 4);

    Set<ConstraintViolation<Car>> constraintViolations = validator.validate(car);

    Assertions.assertEquals(1, constraintViolations.size());
    Assertions.assertEquals("size must be between 2 and 14",
        constraintViolations.iterator().next().getMessage());
  }

  @Test
  public void seatCountTooLow() {
    Car car = new Car("Morris", "DD-AB-123", 1);

    Set<ConstraintViolation<Car>> constraintViolations = validator.validate(car);

    Assertions.assertEquals(1, constraintViolations.size());
    Assertions.assertEquals("must be greater than or equal to 2",
        constraintViolations.iterator().next().getMessage());
  }

  @Test
  public void carIsValid() {
    Car car = new Car("Morris", "DD-AB-123", 2);

    Set<ConstraintViolation<Car>> constraintViolations = validator.validate(car);

    Assertions.assertEquals(0, constraintViolations.size());
  }
}
