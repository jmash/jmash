package com.gitee.jmash.crypto;

import com.gitee.jmash.crypto.util.EncodeUtil;
import java.security.PrivateKey;
import java.security.PublicKey;

/** 非对称密钥算法接口. */
public abstract class AsymmetricUtil {

  /** 编码/解码 AES 默认base64 ,SM2 默认Hex. */
  protected EncodeType encode = EncodeType.base64;

  /**
   * 用私钥对信息生成数字签名.
   *
   * @param data 签名数据
   * @param privateKey 私钥
   * @return 默认算法数字签名
   */
  public String signAndEncode(final byte[] data,final PrivateKey privateKey) throws Exception {
    byte[] sign = sign(data, privateKey);
    return EncodeUtil.encode(encode, sign);
  }

  /**
   * 用私钥对信息生成数字签名.
   *
   * @param data 签名数据
   * @param privateKey 私钥
   * @return 默认算法数字签名
   */
  public abstract byte[] sign(final byte[] data, final PrivateKey privateKey) throws Exception;

  /**
   * 用公钥校验数字签名.
   *
   * @param data 已加密数据
   * @param publicKey 公钥
   * @param sign 数字签名
   */
  public boolean verify(final byte[] data,final PublicKey publicKey,String sign) throws Exception {
    byte[] signByte = EncodeUtil.decode(encode, sign);
    return verify(data, publicKey, signByte);
  }

  /**
   * 用公钥校验数字签名.
   *
   * @param data 已加密数据
   * @param publicKey 公钥
   * @param sign 数字签名
   */
  public abstract boolean verify(final byte[] data,final PublicKey publicKey,final byte[] sign) throws Exception;

  /**
   * 公钥加密.
   *
   * @param data 源数据
   * @param publicKey 公钥(BASE64编码)
   * @return 加密后数据
   */
  public String encryptAndEncode(final byte[] data,final PublicKey publicKey) throws Exception {
    byte[] result = encrypt(data, publicKey);
    return EncodeUtil.encode(encode, result);
  }

  /**
   * 公钥加密.
   *
   * @param data 源数据
   * @param publicKey 公钥(BASE64编码)
   * @return 加密后数据
   */
  public abstract byte[] encrypt(final byte[] data,final PublicKey publicKey) throws Exception;

  /**
   * 私钥解密.
   *
   * @param data 已加密数据
   * @param privateKey 私钥(BASE64编码)
   * @return 源数据
   */
  public byte[] decrypt(String data,final PrivateKey privateKey) throws Exception {
    byte[] dataByte = EncodeUtil.decode(encode, data);
    return decrypt(dataByte, privateKey);
  }

  /**
   * 私钥解密.
   *
   * @param data 已加密数据
   * @param privateKey 私钥(BASE64编码)
   * @return 源数据
   */
  public abstract byte[] decrypt(final byte[] data,final PrivateKey privateKey) throws Exception;



}
