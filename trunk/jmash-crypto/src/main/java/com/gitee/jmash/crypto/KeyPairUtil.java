package com.gitee.jmash.crypto;

import java.security.Key;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;

/**
 * 密钥对工具类.
 * 
 * @author cgd
 */
public abstract class KeyPairUtil {
  
  /** 生成密钥对(公钥和私钥). */
  public abstract KeyPair createKeyPair() throws Exception;

  /** 获取Base64/Hex Key. */
  public abstract String getKeyStr(final Key key);

  /** Base64/Hex To PublicKey. */
  public abstract PublicKey getPublicKey(String publicKey) throws Exception;

  /** Base64/Hex To PrivateKey. */
  public abstract PrivateKey getPrivateKey(String privateKey) throws Exception;

}
