package com.gitee.jmash.crypto.cipher;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;

public class AesCbcUtil {

  public static CipherUtil get()
      throws NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException {
    Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
    return new CipherUtil(cipher, "AES", initIv(cipher));
  }

  /** 初始化IV. */
  public static byte[] initIv(Cipher cipher) {
    int blockSize = cipher.getBlockSize();
    byte[] iv = new byte[blockSize];
    for (int i = 0; i < blockSize; ++i) {
      iv[i] = 0;
    }
    return iv;
  }

}
