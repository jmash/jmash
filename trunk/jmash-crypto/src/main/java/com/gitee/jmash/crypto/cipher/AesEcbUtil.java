package com.gitee.jmash.crypto.cipher;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;

public class AesEcbUtil {

  public static CipherUtil get()
      throws NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException {
    Cipher cipher = Cipher.getInstance("AES");
    return new CipherUtil(cipher, "AES");
  }
  
}
