package com.gitee.jmash.crypto.cipher;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.util.Base64;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.bouncycastle.util.encoders.Hex;

/**
 * 对称加解密算法.
 *
 */
public class CipherUtil {

  // 加解密算法.
  Cipher cipher;
  // 算法名称.
  String algorithm;

  byte[] bytesIv;

  public CipherUtil(Cipher cipher, String algorithm) {
    this.cipher = cipher;
    this.algorithm = algorithm;
  }

  public CipherUtil(Cipher cipher, String algorithm, byte[] bytesIv) {
    this.cipher = cipher;
    this.algorithm = algorithm;
    this.bytesIv = bytesIv;
  }

  /** 16进制Key串转化为Key字节. */
  public byte[] decodeKey(String hexKey) {
    return Hex.decode(hexKey);
  }

  /** 解码Base64Key. */
  public byte[] decodeBase64Key(String base64Key) {
    return Base64.getDecoder().decode(base64Key.getBytes());
  }



  /**
   * 加密.
   * 
   * @param key 密钥
   * @param content 内存
   * @param charset 内容编码
   * @return 加密后结果Base64
   */
  public String encrypt(byte[] key, String content, String charset)
      throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException,
      UnsupportedEncodingException, InvalidAlgorithmParameterException {
    byte[] encryptBytes = encrypt(key, content.getBytes(charset));
    return Base64.getEncoder().encodeToString(encryptBytes);
  }

  /**
   * 加密.
   * 
   * @param key 密钥
   * @param data 数据
   * @return 加密后结果
   */
  public byte[] encrypt(byte[] key, byte[] data) throws InvalidKeyException,
      IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException {
    Key keySpec = new SecretKeySpec(key, this.algorithm);
    if (null == bytesIv) {
      cipher.init(Cipher.ENCRYPT_MODE, keySpec);
    } else {
      IvParameterSpec iv = new IvParameterSpec(bytesIv);
      cipher.init(Cipher.ENCRYPT_MODE, keySpec, iv);
    }
    return cipher.doFinal(data);
  }

  /**
   * 解密.
   * 
   * @param key 密钥
   * @param encryptConent 加密后Base64
   * @param charset 解密数据编码
   * @return 解密后的结果
   */
  public String decrypt(byte[] key, String encryptConent, String charset)
      throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException,
      UnsupportedEncodingException, InvalidAlgorithmParameterException {
    byte[] encryptedBytes = Base64.getDecoder().decode(encryptConent.getBytes());
    byte[] bytes = decrypt(key, encryptedBytes);
    return new String(bytes, charset);
  }

  /**
   * 解密.
   * 
   * @param key 密钥
   * @param encryptData 加密后的数据
   * @return 解密后结果
   */
  public byte[] decrypt(byte[] key, byte[] encryptData) throws InvalidKeyException,
      IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException {
    Key keySpec = new SecretKeySpec(key, this.algorithm);
    if (null == bytesIv) {
      cipher.init(Cipher.DECRYPT_MODE, keySpec);
    } else {
      IvParameterSpec iv = new IvParameterSpec(bytesIv);
      cipher.init(Cipher.DECRYPT_MODE, keySpec, iv);
    }
    return cipher.doFinal(encryptData);
  }

}
