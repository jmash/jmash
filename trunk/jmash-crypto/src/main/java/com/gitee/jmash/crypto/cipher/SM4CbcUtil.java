package com.gitee.jmash.crypto.cipher;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;
import java.security.Security;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class SM4CbcUtil {

	static {
		Security.addProvider((Provider) new BouncyCastleProvider());
	}

	public static CipherUtil get() throws NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException {
		Cipher cipher = Cipher.getInstance("SM4/CBC/PKCS5Padding","BC");
		return new CipherUtil(cipher, "SM4",initIv(cipher));
	}
	
	/** 初始化IV. */
	public static byte[] initIv(Cipher cipher) {
		int blockSize = cipher.getBlockSize();
		byte[] iv = new byte[blockSize];
		for (int i = 0; i < blockSize; ++i) {
			iv[i] = 0;
		}
		return iv;
	}
}
