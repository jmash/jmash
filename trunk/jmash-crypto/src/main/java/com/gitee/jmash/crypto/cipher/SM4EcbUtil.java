package com.gitee.jmash.crypto.cipher;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;
import java.security.Security;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class SM4EcbUtil {

	static {
		Security.addProvider((Provider) new BouncyCastleProvider());
	}

	public static CipherUtil get() throws NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException {
		Cipher cipher = Cipher.getInstance("SM4/ECB/PKCS5Padding", "BC");
		return new CipherUtil(cipher, "SM4");
	}
}
