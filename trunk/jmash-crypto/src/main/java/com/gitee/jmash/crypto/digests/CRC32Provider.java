
package com.gitee.jmash.crypto.digests;

import java.security.Provider;

/**
 * CRC32 Provider 实现 java.security.Provider.
 *
 * @author CGD
 * 
 */
public class CRC32Provider extends Provider {

	private static final long serialVersionUID = 1L;

	public CRC32Provider() {
		super("CRC32Provider", "1.0", "Jmash CRC32 algorithm");
		put("CRC32", "CRC32");
	}

}
