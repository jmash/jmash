package com.gitee.jmash.crypto.digests;

/**
 * MD5.
 *
 * @author CGD
 *
 */
public class CRC32Util {

  public static DigestUtil get() {
    return new DigestUtil(new CRC32MessageDigest());
  }

}
