package com.gitee.jmash.crypto.digests;

import java.security.MessageDigest;

import org.bouncycastle.util.encoders.Hex;

/**
 * 摘要算法工具类封装.
 * 
 * @author CGD
 *
 */
public class DigestUtil {

	MessageDigest md;

	public DigestUtil(MessageDigest md) {
		this.md = md;
	}

	/** 获取算法信息. */
	public MessageDigest getMessageDigest() {
		return md;
	}

	/** 字符串摘要. */
	public String digest(String data) {
		byte[] datas = data.getBytes();
		byte[] hash = digest(datas);
		return Hex.toHexString(hash);
	}

	/** 字节摘要. */
	public byte[] digest(byte[] data) {
		return md.digest(data);
	}

	/** 字节摘要串. */
	public String digestStr(byte[] data) {
		byte[] hash = digest(data);
		return Hex.toHexString(hash);
	}

}
