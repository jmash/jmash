package com.gitee.jmash.crypto.digests;

import java.security.MessageDigest;
import org.apache.commons.logging.LogFactory;

/**
 * MD5.
 *
 * @author CGD
 *
 */
public class MD5Util {


  public static DigestUtil get() {
    try {
      return new DigestUtil(MessageDigest.getInstance("MD5"));
    } catch (Exception e) {
      LogFactory.getLog(MD5Util.class).error("MD5 Error:",e);
      return null;
    }
  }

}
