package com.gitee.jmash.crypto.digests;

import java.security.MessageDigest;
import org.apache.commons.logging.LogFactory;

/**
 * SHA.
 *
 * @author CGD
 *
 */
public class SHA1Util {

  public static DigestUtil get() {
    try {
      return new DigestUtil(MessageDigest.getInstance("SHA"));
    } catch (Exception e) {
      LogFactory.getLog(SHA1Util.class).error("SHA Error:", e);
      return null;
    }
  }
  
}
