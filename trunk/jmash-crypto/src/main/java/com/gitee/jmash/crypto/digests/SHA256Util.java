package com.gitee.jmash.crypto.digests;

import java.security.MessageDigest;
import org.apache.commons.logging.LogFactory;

/**
 * SHA.
 *
 * @author CGD
 *
 */
public class SHA256Util {

  public static DigestUtil get() {
    try {
      return new DigestUtil(MessageDigest.getInstance("SHA-256"));
    } catch (Exception e) {
      LogFactory.getLog(SHA256Util.class).error("SHA-256 Error:", e);
      return null;
    }
  }
  
}
