package com.gitee.jmash.crypto.digests;

import java.security.MessageDigest;
import java.security.Provider;
import java.security.Security;
import org.apache.commons.logging.LogFactory;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

/**
 * 国密SM3算法，类似MD5、SHA.
 *
 * @author CGD
 *
 */
public class SM3Util {

  static {
    Security.addProvider((Provider) new BouncyCastleProvider());
  }

  /** 算法不是线程安全的，需要为每个线程创建. */
  public static DigestUtil get() {
    try {
      return new DigestUtil(MessageDigest.getInstance("SM3"));
    } catch (Exception e) {
      LogFactory.getLog(SM3Util.class).error("SM3 Error:", e);
      return null;
    }
  }

}
