package com.gitee.jmash.crypto.rsa;

import com.gitee.jmash.crypto.AsymmetricUtil;
import com.gitee.jmash.crypto.EncodeType;
import java.io.ByteArrayOutputStream;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import javax.crypto.Cipher;

/** RSA 非对称密钥. */
public class RsaAsymmetricUtil extends AsymmetricUtil {

  /** 签名算法. */
  public static final String SIGNATURE_ALGORITHM = "SHA256withRSA";
  private static final int KEY_SIZE = 2048;
  /** RSA最大加密明文大小. */
  private static final int MAX_ENCRYPT_BLOCK = KEY_SIZE / 8 - 11;

  /** RSA最大解密密文大小. */
  private static final int MAX_DECRYPT_BLOCK = KEY_SIZE / 8;

  public static AsymmetricUtil get() {
    return new RsaAsymmetricUtil();
  }

  public RsaAsymmetricUtil() {
    super();
  }

  public RsaAsymmetricUtil(EncodeType encode) {
    super();
    this.encode = encode;
  }

  @Override
  public byte[] sign(byte[] data, PrivateKey privateKey) throws Exception {
    Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM);
    signature.initSign(privateKey);
    signature.update(data);
    return signature.sign();
  }

  @Override
  public boolean verify(byte[] data, PublicKey publicKey, byte[] sign) throws Exception {
    Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM);
    signature.initVerify(publicKey);
    signature.update(data);
    return signature.verify(sign);
  }

  @Override
  public byte[] encrypt(byte[] data, PublicKey key) throws Exception {
    // 对数据加密
    Cipher cipher = Cipher.getInstance(key.getAlgorithm());
    cipher.init(Cipher.ENCRYPT_MODE, key);
    int inputLen = data.length;
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    int offSet = 0;
    byte[] cache;
    int i = 0;
    // 对数据分段加密
    while (inputLen - offSet > 0) {
      if (inputLen - offSet > MAX_ENCRYPT_BLOCK) {
        cache = cipher.doFinal(data, offSet, MAX_ENCRYPT_BLOCK);
      } else {
        cache = cipher.doFinal(data, offSet, inputLen - offSet);
      }
      out.write(cache, 0, cache.length);
      i++;
      offSet = i * MAX_ENCRYPT_BLOCK;
    }
    byte[] encryptedData = out.toByteArray();
    out.close();
    return encryptedData;
  }

  @Override
  public byte[] decrypt(byte[] data, PrivateKey key) throws Exception {
    Cipher cipher = Cipher.getInstance(key.getAlgorithm());
    cipher.init(Cipher.DECRYPT_MODE, key);
    int inputLen = data.length;
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    int offSet = 0;
    byte[] cache;
    int i = 0;
    // 对数据分段解密
    while (inputLen - offSet > 0) {
      if (inputLen - offSet > MAX_DECRYPT_BLOCK) {
        cache = cipher.doFinal(data, offSet, MAX_DECRYPT_BLOCK);
      } else {
        cache = cipher.doFinal(data, offSet, inputLen - offSet);
      }
      out.write(cache, 0, cache.length);
      i++;
      offSet = i * MAX_DECRYPT_BLOCK;
    }
    byte[] decryptedData = out.toByteArray();
    out.close();
    return decryptedData;
  }

}
