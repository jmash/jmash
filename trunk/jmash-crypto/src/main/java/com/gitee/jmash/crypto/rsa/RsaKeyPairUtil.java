package com.gitee.jmash.crypto.rsa;

import com.gitee.jmash.crypto.EncodeType;
import com.gitee.jmash.crypto.KeyPairUtil;
import com.gitee.jmash.crypto.util.EncodeUtil;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

/** RSA 生成密钥算法. */
public class RsaKeyPairUtil extends KeyPairUtil {

  /** 加密算法RSA. */
  public static final String KEY_ALGORITHM = "RSA";

  private static final int KEY_SIZE = 2048;


  public static KeyPairUtil get() {
    return new RsaKeyPairUtil();
  }

  /** 编码/解码 AES 默认base64 ,SM2 默认Hex. */
  protected EncodeType encode = EncodeType.base64;

  public RsaKeyPairUtil() {
    super();
  }

  public RsaKeyPairUtil(EncodeType encode) {
    super();
    this.encode = encode;
  }


  @Override
  public KeyPair createKeyPair() throws Exception {
    KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance(KEY_ALGORITHM);
    keyPairGen.initialize(KEY_SIZE);
    return keyPairGen.generateKeyPair();
  }

  @Override
  public String getKeyStr(Key key) {
    return EncodeUtil.encode(encode, key.getEncoded());
  }

  @Override
  public PublicKey getPublicKey(String publicKey) throws Exception {
    byte[] keyBytes = EncodeUtil.decode(encode, publicKey);
    X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(keyBytes);
    KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
    return keyFactory.generatePublic(x509KeySpec);
  }

  @Override
  public PrivateKey getPrivateKey(String privateKey) throws Exception {
    byte[] keyBytes = EncodeUtil.decode(encode, privateKey);
    PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(keyBytes);
    KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
    return keyFactory.generatePrivate(pkcs8KeySpec);
  }

}
