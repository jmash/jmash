package com.gitee.jmash.crypto.sm2;

import com.gitee.jmash.crypto.AsymmetricUtil;
import com.gitee.jmash.crypto.EncodeType;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.engines.SM2Engine;
import org.bouncycastle.crypto.engines.SM2Engine.Mode;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import org.bouncycastle.crypto.signers.SM2Signer;
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPrivateKey;
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPublicKey;
import org.bouncycastle.jcajce.provider.asymmetric.util.ECUtil;
import org.bouncycastle.jce.spec.ECParameterSpec;

/** SM2 算法. */
public class Sm2AsymmetricUtil extends AsymmetricUtil {

  public static AsymmetricUtil get() {
    return new Sm2AsymmetricUtil();
  }

  // 采用新模式加密标准C1C3C2,旧模式加密标准C1C2C3
  protected SM2Engine.Mode mode = SM2Engine.Mode.C1C3C2;

  public Sm2AsymmetricUtil() {
    super();
    this.encode = EncodeType.hex;
  }

  public Sm2AsymmetricUtil(Mode mode, EncodeType encode) {
    super();
    this.mode = mode;
    this.encode = encode;
  }

  @Override
  public byte[] sign(byte[] data, PrivateKey privateKey) throws Exception {
    // 创建SM2签名器
    SM2Signer signer = new SM2Signer();
    BCECPrivateKey privateK = (BCECPrivateKey) privateKey;

    // 获取EC参数规范
    ECParameterSpec ecParams = privateK.getParameters();

    // 设置私钥参数
    CipherParameters params =
        new ECPrivateKeyParameters(privateK.getD(), ECUtil.getDomainParameters(null, ecParams));
    signer.init(true, params);

    // 进行签名
    signer.update(data, 0, data.length);
    return signer.generateSignature();
  }

  @Override
  public boolean verify(byte[] data, PublicKey publicKey, byte[] sign) throws Exception {
    // 创建SM2签名验证器
    SM2Signer verifier = new SM2Signer();
    BCECPublicKey publicK = (BCECPublicKey) publicKey;
    // 获取EC参数规范
    ECParameterSpec ecParams = publicK.getParameters();
    // 设置公钥参数
    CipherParameters params =
        new ECPublicKeyParameters(publicK.getQ(), ECUtil.getDomainParameters(null, ecParams));
    verifier.init(false, params);
    // 验证签名
    verifier.update(data, 0, data.length);
    return verifier.verifySignature(sign);
  }

  @Override
  public byte[] encrypt(byte[] data, PublicKey publicKey) throws Exception {
    BCECPublicKey bcecPublicKey = (BCECPublicKey) publicKey;
    // 通过公钥对象获取公钥的基本域参数。
    ECParameterSpec ecParameterSpec = bcecPublicKey.getParameters();
    ECDomainParameters ecDomainParameters = new ECDomainParameters(ecParameterSpec.getCurve(),
        ecParameterSpec.getG(), ecParameterSpec.getN());
    // 通过公钥值和公钥基本参数创建公钥参数对象
    ECPublicKeyParameters ecPublicKeyParameters =
        new ECPublicKeyParameters(bcecPublicKey.getQ(), ecDomainParameters);
    // 根据加密模式实例化SM2公钥加密引擎
    SM2Engine sm2Engine = new SM2Engine(mode);
    // 初始化加密引擎
    sm2Engine.init(true, new ParametersWithRandom(ecPublicKeyParameters, new SecureRandom()));

    byte[] arrayOfBytes = null;
    try {
      // 通过加密引擎对字节数串行加密
      arrayOfBytes = sm2Engine.processBlock(data, 0, data.length);
    } catch (Exception e) {
      System.out.println("SM2加密时出现异常:" + e.getMessage());
      e.printStackTrace();
    }
    return arrayOfBytes;
  }

  /**
   * 私钥解密.
   *
   * @param data 已加密数据
   * @param privateKey 私钥(BASE64编码)
   * @return 源数据
   */
  @Override
  public byte[] decrypt(String data, PrivateKey privateKey) throws Exception {
    // JS sm-crypto sm2解码自动补04,需要兼容.
    if (EncodeType.hex.equals(encode) && !data.startsWith("04")) {
      return super.decrypt("04" + data, privateKey);
    }
    return super.decrypt(data, privateKey);
  }

  @Override
  public byte[] decrypt(byte[] data, PrivateKey privateKey) throws Exception {
    BCECPrivateKey bcecPrivateKey = (BCECPrivateKey) privateKey;
    // 通过私钥对象获取私钥的基本域参数。
    ECParameterSpec ecParameterSpec = bcecPrivateKey.getParameters();
    ECDomainParameters ecDomainParameters = new ECDomainParameters(ecParameterSpec.getCurve(),
        ecParameterSpec.getG(), ecParameterSpec.getN());

    // 通过私钥值和私钥基本参数创建私钥参数对象
    ECPrivateKeyParameters ecPrivateKeyParameters =
        new ECPrivateKeyParameters(bcecPrivateKey.getD(), ecDomainParameters);

    // 通过解密模式创建解密引擎并初始化
    SM2Engine sm2Engine = new SM2Engine(SM2Engine.Mode.C1C3C2);
    sm2Engine.init(false, ecPrivateKeyParameters);

    byte[] arrayOfBytes = null;
    try {
      // 通过解密引擎对密文字节串进行解密
      arrayOfBytes = sm2Engine.processBlock(data, 0, data.length);
    } catch (Exception e) {
      System.out.println("SM2解密时出现异常" + e.getMessage());
    }
    return arrayOfBytes;
  }

}
