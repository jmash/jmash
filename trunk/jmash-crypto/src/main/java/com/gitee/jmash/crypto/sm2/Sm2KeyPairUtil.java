package com.gitee.jmash.crypto.sm2;

import com.gitee.jmash.crypto.KeyPairUtil;
import java.math.BigInteger;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.spec.ECGenParameterSpec;
import org.bouncycastle.asn1.gm.GMNamedCurves;
import org.bouncycastle.asn1.x9.X9ECParameters;
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPrivateKey;
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPublicKey;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.spec.ECParameterSpec;
import org.bouncycastle.jce.spec.ECPrivateKeySpec;
import org.bouncycastle.jce.spec.ECPublicKeySpec;
import org.bouncycastle.util.encoders.Hex;

/** SM2 生成密钥算法. */
public class Sm2KeyPairUtil extends KeyPairUtil {

  static {
    Security.addProvider((Provider) new BouncyCastleProvider());
  }

  public static KeyPairUtil get() {
    return new Sm2KeyPairUtil();
  }

  public Sm2KeyPairUtil() {
    super();
  }

  // 椭圆曲线ECParameters ASN.1 结构
  private static X9ECParameters x9ECParameters = GMNamedCurves.getByName("sm2p256v1");
  // 椭圆曲线公钥或私钥的基本域参数。
  private static ECParameterSpec ecDomainParameters =
      new ECParameterSpec(x9ECParameters.getCurve(), x9ECParameters.getG(), x9ECParameters.getN());

  @Override
  public KeyPair createKeyPair() throws Exception {
    // 使用标准名称创建EC参数生成的参数规范
    final ECGenParameterSpec sm2Spec = new ECGenParameterSpec("sm2p256v1");
    // 获取一个椭圆曲线类型的密钥对生成器
    final KeyPairGenerator kpg = KeyPairGenerator.getInstance("EC", new BouncyCastleProvider());
    // 使用SM2算法域参数集初始化密钥生成器（默认使用最高优先级安装的提供者的 SecureRandom 的实现作为随机源）
    // kpg.initialize(sm2Spec);
    // 使用SM2的算法域参数集和指定的随机源初始化密钥生成器
    kpg.initialize(sm2Spec, new SecureRandom());
    // 通过密钥生成器生成密钥对
    return kpg.generateKeyPair();
  }

  @Override
  public String getKeyStr(Key key) {
    if (key instanceof BCECPublicKey) {
      // 获取65字节非压缩的十六进制公钥串(0x04)
      byte[] publicKeyBytes = ((BCECPublicKey) key).getQ().getEncoded(false);
      return Hex.toHexString(publicKeyBytes);

    } else if (key instanceof BCECPrivateKey) {
      // 获取32字节十六进制私钥串
      return ((BCECPrivateKey) key).getD().toString(16);
    }
    throw new RuntimeException("key is Not BCECPublicKey or BCECPrivateKey ");
  }

  @Override
  public PublicKey getPublicKey(String publicKey) throws Exception {
    // 截取64字节有效的SM2公钥（如果公钥首字节为0x04）
    if (publicKey.length() > 128) {
      publicKey = publicKey.substring(publicKey.length() - 128);
    }
    // 将公钥拆分为x,y分量（各32字节）
    String stringX = publicKey.substring(0, 64);
    String stringY = publicKey.substring(stringX.length());
    // 将公钥x、y分量转换为BigInteger类型
    BigInteger x = new BigInteger(stringX, 16);
    BigInteger y = new BigInteger(stringY, 16);
    // 通过公钥x、y分量创建椭圆曲线公钥规范
    ECPublicKeySpec ecPublicKeySpec =
        new ECPublicKeySpec(x9ECParameters.getCurve().createPoint(x, y), ecDomainParameters);
    // 通过椭圆曲线公钥规范，创建出椭圆曲线公钥对象（可用于SM2加密及验签）
    return new BCECPublicKey("EC", ecPublicKeySpec, BouncyCastleProvider.CONFIGURATION);

  }

  @Override
  public PrivateKey getPrivateKey(String privateKey) throws Exception {
    // 将十六进制私钥字符串转换为BigInteger对象
    BigInteger d = new BigInteger(privateKey, 16);
    // 通过私钥和私钥域参数集创建椭圆曲线私钥规范
    ECPrivateKeySpec ecPrivateKeySpec = new ECPrivateKeySpec(d, ecDomainParameters);
    // 通过椭圆曲线私钥规范，创建出椭圆曲线私钥对象（可用于SM2解密和签名）
    return new BCECPrivateKey("EC", ecPrivateKeySpec, BouncyCastleProvider.CONFIGURATION);
  }

}
