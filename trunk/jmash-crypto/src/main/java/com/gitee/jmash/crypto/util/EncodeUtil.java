package com.gitee.jmash.crypto.util;

import com.gitee.jmash.crypto.EncodeType;
import java.util.Base64;
import org.bouncycastle.util.encoders.Hex;

public class EncodeUtil {

  /** 编码 */
  public static String encode(EncodeType encode, byte[] data) {
    if (EncodeType.base64.equals(encode)) {
      return Base64.getEncoder().encodeToString(data);
    } else if (EncodeType.hex.equals(encode)) {
      return Hex.toHexString(data);
    } else {
      throw new RuntimeException("NOT Impl.");
    }
  }

  /** 解码 */
  public static byte[] decode(EncodeType encode, String data) {
    if (EncodeType.base64.equals(encode)) {
      return Base64.getDecoder().decode(data);
    } else if (EncodeType.hex.equals(encode)) {
      return Hex.decode(data);
    } else {
      throw new RuntimeException("NOT Impl.");
    }
  }
  
}
