package com.gitee.jmash.crypto.cipher.test;

import com.gitee.jmash.crypto.cipher.AesEcbUtil;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Arrays;
import java.util.Base64;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class AesKeyTest {

  @Test
  public void mysql() throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException,
      UnsupportedEncodingException, InvalidAlgorithmParameterException, NoSuchAlgorithmException,
      NoSuchProviderException, NoSuchPaddingException {
    String value = "123456789012345678901223";
    // AES_ENCRYPT
    byte[] a = AesEcbUtil.get().encrypt(getKey("test"), value.getBytes());
    System.out.println(new String(Hex.encode(a)).toUpperCase());
    // AES_DECRYPT
    byte[] b = AesEcbUtil.get().decrypt(getKey("test"), a);
    System.out.println(new String(b));
    Assertions.assertEquals(value, new String(b));
  }

  @Test
  public void createAesKey() throws NoSuchAlgorithmException {
    SecretKey key = KeyGenerator.getInstance("AES").generateKey();
    System.out.println(Base64.getEncoder().encodeToString(key.getEncoded()));
  }

  @Test
  public void createAesKey1() throws NoSuchAlgorithmException {
    System.out.println(Base64.getEncoder().encodeToString(getKey("test")));
  }

  public byte[] getKey(String key) {
    byte[] iv = Arrays.copyOf(key.getBytes(), 16);
    for (int i = key.length(); i < 16; ++i) {
      iv[i] = 0;
    }
    return iv;
  }
}
