package com.gitee.jmash.crypto.cipher.test;

import com.gitee.jmash.crypto.cipher.AesCbcUtil;
import com.gitee.jmash.crypto.cipher.AesEcbUtil;
import com.gitee.jmash.crypto.cipher.CipherUtil;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

public class AesUtilTest {

	@Test
	public void testECB() throws NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException,
			InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException, InvalidAlgorithmParameterException {
		CipherUtil cipherUtil = AesEcbUtil.get();
		byte[] key = "jmash00000000000".getBytes();
		//cipherUtil.decodeBase64Key("hz1Virh4EX0SiO8ccCZoUQ==");
		String orig = "test";
		String encrypt = cipherUtil.encrypt(key, orig, "UTF-8");
		Assertions.assertEquals("3pHeg8IxBM05a3h9Neg7UQ==", encrypt);
		String decrypt = cipherUtil.decrypt(key, encrypt, "UTF-8");
		Assertions.assertEquals(orig, decrypt);
	}

	@Test
	public void testCBC() throws NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException,
			InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException, InvalidAlgorithmParameterException {
		CipherUtil cipherUtil = AesCbcUtil.get();
		byte[] key = cipherUtil.decodeBase64Key("VrGs6iJSlrVzBwZLxj0H0w==");
		String orig = "test";
		String encrypt = cipherUtil.encrypt(key, orig, "UTF-8");
		Assertions.assertEquals("Q5Fm/W3KwyJHHfQPxWCW3Q==", encrypt);
		String decrypt = cipherUtil.decrypt(key, encrypt, "UTF-8");
		Assertions.assertEquals(orig, decrypt);
	}
}
