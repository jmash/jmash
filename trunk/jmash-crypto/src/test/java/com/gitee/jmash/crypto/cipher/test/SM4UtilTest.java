package com.gitee.jmash.crypto.cipher.test;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import com.gitee.jmash.crypto.cipher.CipherUtil;
import com.gitee.jmash.crypto.cipher.SM4CbcUtil;
import com.gitee.jmash.crypto.cipher.SM4EcbUtil;

public class SM4UtilTest {

	@Test
	public void testECB() throws NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException,
			InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException, InvalidAlgorithmParameterException {
		CipherUtil cipherUtil = SM4EcbUtil.get();
		byte[] key = cipherUtil.decodeKey("4A65463855397748464F4D6673325938");
		String orig = "ftppub";
		String encrypt = cipherUtil.encrypt(key, orig, "UTF-8");
		Assertions.assertEquals("krnjSOxZn636gnu2E1vq9g==", encrypt);
		String decrypt = cipherUtil.decrypt(key, encrypt, "UTF-8");
		Assertions.assertEquals(orig, decrypt);
	}

	@Test
	public void testCBC() throws NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException,
			InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException, InvalidAlgorithmParameterException {
		CipherUtil cipherUtil = SM4CbcUtil.get();
		byte[] key = cipherUtil.decodeKey("4A65463855397748464F4D6673325938");
		String orig = "ftppub";
		String encrypt = cipherUtil.encrypt(key, orig, "UTF-8");
		Assertions.assertEquals("krnjSOxZn636gnu2E1vq9g==", encrypt);
		String decrypt = cipherUtil.decrypt(key, encrypt, "UTF-8");
		Assertions.assertEquals(orig, decrypt);
	}
}
