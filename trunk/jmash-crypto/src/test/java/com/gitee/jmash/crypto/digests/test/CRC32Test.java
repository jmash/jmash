package com.gitee.jmash.crypto.digests.test;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import com.gitee.jmash.crypto.digests.CRC32Util;

public class CRC32Test {

	@Test
	public void test() {
		String d1 = CRC32Util.get().digest("123456");
		Assertions.assertEquals("0972d361", d1);
		System.out.println(d1);
	}

}
