package com.gitee.jmash.crypto.digests.test;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import com.gitee.jmash.crypto.digests.MD5Util;

public class MD5Test {

	@Test
	public void test() {
		String d1 = MD5Util.get().digest("123456");
		Assertions.assertEquals("e10adc3949ba59abbe56e057f20f883e", d1);
		System.out.println(d1);
	}

}
