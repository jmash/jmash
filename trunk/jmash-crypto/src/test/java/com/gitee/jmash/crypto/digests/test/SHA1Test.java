package com.gitee.jmash.crypto.digests.test;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import com.gitee.jmash.crypto.digests.SHA1Util;

public class SHA1Test {

	@Test
	public void test() {
		String d1 = SHA1Util.get().digest("123456");
		Assertions.assertEquals("7c4a8d09ca3762af61e59520943dc26494f8941b", d1);
		System.out.println(d1);
	}

}
