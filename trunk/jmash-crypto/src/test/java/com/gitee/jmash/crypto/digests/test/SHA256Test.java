package com.gitee.jmash.crypto.digests.test;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import com.gitee.jmash.crypto.digests.SHA256Util;

public class SHA256Test {

	@Test
	public void test() {
		String d1 = SHA256Util.get().digest("123456");
		Assertions.assertEquals("8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92", d1);
		System.out.println(d1);
	}

}
