package com.gitee.jmash.crypto.digests.test;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import com.gitee.jmash.crypto.digests.SM3Util;

public class SM3Test {

	@Test
	public void test() {
		String d1 = SM3Util.get().digest("123456");
		Assertions.assertEquals("207cf410532f92a47dee245ce9b11ff71f578ebd763eb3bbea44ebd043d018fb", d1);
		System.out.println(d1);
	}

}
