package com.gitee.jmash.crypto.rsa.test;

import com.gitee.jmash.crypto.AsymmetricUtil;
import com.gitee.jmash.crypto.KeyPairUtil;
import com.gitee.jmash.crypto.rsa.RsaAsymmetricUtil;
import com.gitee.jmash.crypto.rsa.RsaKeyPairUtil;
import com.gitee.jmash.crypto.test.AsymmetricUtilTest;

/** RSA TEST. */
public class RsaTest extends AsymmetricUtilTest {

  @Override
  protected KeyPairUtil getKeyPairUtil() {
    return RsaKeyPairUtil.get();
  }

  @Override
  protected AsymmetricUtil getAsymmetricUtil() {
    return RsaAsymmetricUtil.get();
  }
  
}
