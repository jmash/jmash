package com.gitee.jmash.crypto.sm2.test;

import com.gitee.jmash.crypto.AsymmetricUtil;
import com.gitee.jmash.crypto.KeyPairUtil;
import com.gitee.jmash.crypto.sm2.Sm2AsymmetricUtil;
import com.gitee.jmash.crypto.sm2.Sm2KeyPairUtil;
import com.gitee.jmash.crypto.test.AsymmetricUtilTest;
import java.util.Base64;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class Sm2Test extends AsymmetricUtilTest {

  @Override
  protected KeyPairUtil getKeyPairUtil() {
    return Sm2KeyPairUtil.get();
  }

  @Override
  protected AsymmetricUtil getAsymmetricUtil() {
    return Sm2AsymmetricUtil.get();
  }

  @Test
  public void test1() throws Exception {
    String privateKey = "c4a9e15828f168103d05b308ac53a71ed17d24e032ebc4699df7cd1ded39c178";
    String publicKey =
        "049e3ca73b61681f4cbfefad70d3d1a2e7ec57ac62202c6380e5583e054fce8620175df19203d6c5d6fe0f41b49dbb6aee3037e4c8ceffaee3bc1583cfb690810e";
    System.out.println("Private:" + privateKey);
    System.out.println("Public:" + publicKey);

    KeyPairUtil keyPairUtil = getKeyPairUtil();
    AsymmetricUtil rsaUtil = getAsymmetricUtil();
    String data = "test";
    byte[] encrypt = rsaUtil.encrypt(data.getBytes(), keyPairUtil.getPublicKey(publicKey));
    System.out.println("加密前:" + data);
    System.out.println("加密后字节长度:" + encrypt.length);
    System.out.println("加密后Hex:" + Hex.toHexString(encrypt));
    System.out.println("加密后Base64:" + Base64.getEncoder().encodeToString(encrypt));
    byte[] result = rsaUtil.decrypt(encrypt, keyPairUtil.getPrivateKey(privateKey));
    Assertions.assertArrayEquals(data.getBytes(), result);

    String a="d2d4496c8d297f65b79bb881d168814eb00977264bd7ae5a0a583ae6acae117f21f765111b88c318f0db5a4ae589c5757ea58c41a5bc956f93000992087ba667e6c33227ce5fc63098628424d08f281ada762534eb2d4f63514bd539c18367ccfccf589a";
    result = rsaUtil.decrypt(a, keyPairUtil.getPrivateKey(privateKey));
    Assertions.assertArrayEquals(data.getBytes(), result);
  }



}
