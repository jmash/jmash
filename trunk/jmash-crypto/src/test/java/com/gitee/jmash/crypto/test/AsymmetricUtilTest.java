package com.gitee.jmash.crypto.test;

import com.gitee.jmash.crypto.AsymmetricUtil;
import com.gitee.jmash.crypto.KeyPairUtil;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPrivateKey;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/** 抽象类测试非对称密钥. */
public abstract class AsymmetricUtilTest {

  protected abstract KeyPairUtil getKeyPairUtil();

  protected abstract AsymmetricUtil getAsymmetricUtil();

  @Test
  public void keyPair() throws Exception {
    KeyPairUtil keyPairUtil = getKeyPairUtil();
    KeyPair keyPair = keyPairUtil.createKeyPair();
    String privateKey = keyPairUtil.getKeyStr(keyPair.getPrivate());
    String publicKey = keyPairUtil.getKeyStr(keyPair.getPublic());

    PrivateKey privateK = keyPairUtil.getPrivateKey(privateKey);
    PublicKey publicK = keyPairUtil.getPublicKey(publicKey);
    Assertions.assertEquals(keyPair.getPublic(), publicK);
    if (privateK instanceof BCECPrivateKey) {
      // keyPair.getPrivate()含由公钥信息,对象比对不成功.
      Assertions.assertEquals(keyPair.getPrivate().toString(), privateK.toString());
    } else {
      Assertions.assertEquals(keyPair.getPrivate(), privateK);
    }
  }

  @Test
  public void sign() throws Exception {
    KeyPairUtil keyPairUtil = getKeyPairUtil();
    AsymmetricUtil rsaUtil = getAsymmetricUtil();
    KeyPair keyPair = keyPairUtil.createKeyPair();
    String data = "test";
    byte[] sign = rsaUtil.sign(data.getBytes(), keyPair.getPrivate());
    boolean v = rsaUtil.verify(data.getBytes(), keyPair.getPublic(), sign);
    Assertions.assertTrue(v);
  }

  @Test
  public void encrypt() throws Exception {
    KeyPairUtil keyPairUtil = getKeyPairUtil();
    AsymmetricUtil rsaUtil = getAsymmetricUtil();
    KeyPair keyPair = keyPairUtil.createKeyPair();
    String data = "test";
    byte[] encrypt = rsaUtil.encrypt(data.getBytes(), keyPair.getPublic());
    byte[] result = rsaUtil.decrypt(encrypt, keyPair.getPrivate());
    Assertions.assertArrayEquals(data.getBytes(), result);
  }

  @Test
  public void signStr() throws Exception {
    KeyPairUtil keyPairUtil = getKeyPairUtil();
    AsymmetricUtil rsaUtil = getAsymmetricUtil();
    KeyPair keyPair = keyPairUtil.createKeyPair();
    String data = "test";
    String sign = rsaUtil.signAndEncode(data.getBytes(), keyPair.getPrivate());
    boolean v = rsaUtil.verify(data.getBytes(), keyPair.getPublic(), sign);
    Assertions.assertTrue(v);
  }

  @Test
  public void encryptStr() throws Exception {
    KeyPairUtil keyPairUtil = getKeyPairUtil();
    AsymmetricUtil rsaUtil = getAsymmetricUtil();
    KeyPair keyPair = keyPairUtil.createKeyPair();
    String data = "test";
    String encrypt = rsaUtil.encryptAndEncode(data.getBytes(), keyPair.getPublic());
    byte[] result = rsaUtil.decrypt(encrypt, keyPair.getPrivate());
    Assertions.assertArrayEquals(data.getBytes(), result);
  }
}
