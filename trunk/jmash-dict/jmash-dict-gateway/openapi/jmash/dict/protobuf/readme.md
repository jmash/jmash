# Protocol Documentation
<a name="top"></a>

## Table of Contents

- [jmash/dict/protobuf/os_dict_entry_message.proto](#jmash_dict_protobuf_os_dict_entry_message-proto)
    - [DictEntryCreateReq](#jmash-dict-DictEntryCreateReq)
    - [DictEntryEnableKey](#jmash-dict-DictEntryEnableKey)
    - [DictEntryKey](#jmash-dict-DictEntryKey)
    - [DictEntryKeyList](#jmash-dict-DictEntryKeyList)
    - [DictEntryList](#jmash-dict-DictEntryList)
    - [DictEntryModel](#jmash-dict-DictEntryModel)
    - [DictEntryModelTotal](#jmash-dict-DictEntryModelTotal)
    - [DictEntryMoveKey](#jmash-dict-DictEntryMoveKey)
    - [DictEntryPage](#jmash-dict-DictEntryPage)
    - [DictEntryReq](#jmash-dict-DictEntryReq)
    - [DictEntryUpdateReq](#jmash-dict-DictEntryUpdateReq)
  
- [jmash/dict/protobuf/os_dict_lay_entry_message.proto](#jmash_dict_protobuf_os_dict_lay_entry_message-proto)
    - [DictLayEntryCreateReq](#jmash-dict-DictLayEntryCreateReq)
    - [DictLayEntryEnableKey](#jmash-dict-DictLayEntryEnableKey)
    - [DictLayEntryKey](#jmash-dict-DictLayEntryKey)
    - [DictLayEntryKeyList](#jmash-dict-DictLayEntryKeyList)
    - [DictLayEntryList](#jmash-dict-DictLayEntryList)
    - [DictLayEntryModel](#jmash-dict-DictLayEntryModel)
    - [DictLayEntryModelTotal](#jmash-dict-DictLayEntryModelTotal)
    - [DictLayEntryMoveKey](#jmash-dict-DictLayEntryMoveKey)
    - [DictLayEntryPage](#jmash-dict-DictLayEntryPage)
    - [DictLayEntryReq](#jmash-dict-DictLayEntryReq)
    - [DictLayEntryUpdateReq](#jmash-dict-DictLayEntryUpdateReq)
  
- [jmash/dict/protobuf/os_dict_type_message.proto](#jmash_dict_protobuf_os_dict_type_message-proto)
    - [DictTypeCreateReq](#jmash-dict-DictTypeCreateReq)
    - [DictTypeEnableKey](#jmash-dict-DictTypeEnableKey)
    - [DictTypeExportReq](#jmash-dict-DictTypeExportReq)
    - [DictTypeImportReq](#jmash-dict-DictTypeImportReq)
    - [DictTypeKey](#jmash-dict-DictTypeKey)
    - [DictTypeKeyList](#jmash-dict-DictTypeKeyList)
    - [DictTypeList](#jmash-dict-DictTypeList)
    - [DictTypeModel](#jmash-dict-DictTypeModel)
    - [DictTypeModelTotal](#jmash-dict-DictTypeModelTotal)
    - [DictTypeMoveKey](#jmash-dict-DictTypeMoveKey)
    - [DictTypePage](#jmash-dict-DictTypePage)
    - [DictTypeReq](#jmash-dict-DictTypeReq)
    - [DictTypeUpdateReq](#jmash-dict-DictTypeUpdateReq)
  
    - [EntryTypes](#jmash-dict-EntryTypes)
  
- [Scalar Value Types](#scalar-value-types)



<a name="jmash_dict_protobuf_os_dict_entry_message-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## jmash/dict/protobuf/os_dict_entry_message.proto



<a name="jmash-dict-DictEntryCreateReq"></a>

### DictEntryCreateReq
数据字典新增实体


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  |  |
| request_id | [string](#string) |  | 用于检测重复请求的唯一字符串ID |
| validate_only | [bool](#bool) |  | 如果为true，则表示给定的请求仅需要被检验，而不是被执行 |
| type_code | [string](#string) |  | 字典类型编码 |
| dict_code | [string](#string) |  | 字典编码 |
| dict_name | [string](#string) |  | 字典名称 |
| description_ | [string](#string) |  | 描述 |
| enable_ | [bool](#bool) |  | 是否启用 |






<a name="jmash-dict-DictEntryEnableKey"></a>

### DictEntryEnableKey
启用/禁用


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  |  |
| dict_code | [string](#string) |  |  |
| type_code | [string](#string) |  |  |
| enable | [bool](#bool) |  |  |






<a name="jmash-dict-DictEntryKey"></a>

### DictEntryKey
数据字典主键


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  |  |
| dict_code | [string](#string) |  |  |
| type_code | [string](#string) |  |  |






<a name="jmash-dict-DictEntryKeyList"></a>

### DictEntryKeyList
数据字典List


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  |  |
| dict_entry_key | [DictEntryKey](#jmash-dict-DictEntryKey) | repeated |  |






<a name="jmash-dict-DictEntryList"></a>

### DictEntryList
数据字典列表


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [DictEntryModel](#jmash-dict-DictEntryModel) | repeated | 当前页内容 |






<a name="jmash-dict-DictEntryModel"></a>

### DictEntryModel
数据字典实体


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| type_code | [string](#string) |  | 字典类型编码 |
| dict_code | [string](#string) |  | 字典编码 |
| dict_name | [string](#string) |  | 字典名称 |
| list_order | [int32](#int32) |  | 排序 |
| enable_ | [bool](#bool) |  | 是否启用 |
| description_ | [string](#string) |  | 描述 |






<a name="jmash-dict-DictEntryModelTotal"></a>

### DictEntryModelTotal
合计


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| total_size | [int32](#int32) |  | 总记录数 |






<a name="jmash-dict-DictEntryMoveKey"></a>

### DictEntryMoveKey
上移/下移


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  |  |
| dict_code | [string](#string) |  |  |
| type_code | [string](#string) |  |  |
| up | [bool](#bool) |  |  |






<a name="jmash-dict-DictEntryPage"></a>

### DictEntryPage
数据字典分页


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [DictEntryModel](#jmash-dict-DictEntryModel) | repeated | 当前页内容 |
| cur_page | [int32](#int32) |  | 当前页码 |
| page_size | [int32](#int32) |  | 页尺寸 |
| total_size | [int32](#int32) |  | 总记录数 |
| sub_total_dto | [DictEntryModelTotal](#jmash-dict-DictEntryModelTotal) |  | 本页小计 |
| total_dto | [DictEntryModelTotal](#jmash-dict-DictEntryModelTotal) |  | 合计 |






<a name="jmash-dict-DictEntryReq"></a>

### DictEntryReq
数据字典查询


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户 |
| cur_page | [int32](#int32) |  | 当前页码 |
| page_size | [int32](#int32) |  | 页尺寸 |
| order_name | [string](#string) |  | 排序名称 |
| order_asc | [bool](#bool) |  | 是否升序排序 |
| type_code | [string](#string) |  | 字典类型编码(非空) |
| dict_code | [string](#string) |  | 字典编码 |
| has_enable | [bool](#bool) |  | 是否包含启用字段 |
| enable_ | [bool](#bool) |  | 是否启用 |






<a name="jmash-dict-DictEntryUpdateReq"></a>

### DictEntryUpdateReq
数据字典修改实体


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  |  |
| request_id | [string](#string) |  | 用于检测重复请求的唯一字符串ID |
| validate_only | [bool](#bool) |  | 如果为true，则表示给定的请求仅需要被检验，而不是被执行 |
| update_mask | [google.protobuf.FieldMask](#google-protobuf-FieldMask) |  | 更新字段掩码. |
| type_code | [string](#string) |  | 字典类型编码 |
| dict_code | [string](#string) |  | 字典编码 |
| dict_name | [string](#string) |  | 字典名称 |
| description_ | [string](#string) |  | 描述 |
| enable_ | [bool](#bool) |  | 是否启用 |





 

 

 

 



<a name="jmash_dict_protobuf_os_dict_lay_entry_message-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## jmash/dict/protobuf/os_dict_lay_entry_message.proto



<a name="jmash-dict-DictLayEntryCreateReq"></a>

### DictLayEntryCreateReq
层级字典实体新增实体


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  |  |
| request_id | [string](#string) |  | 用于检测重复请求的唯一字符串ID |
| validate_only | [bool](#bool) |  | 如果为true，则表示给定的请求仅需要被检验，而不是被执行 |
| type_code | [string](#string) |  | 字典类型编码 |
| dict_code | [string](#string) |  | 字典编码 |
| dict_name | [string](#string) |  | 字典名称 |
| parent_id | [string](#string) |  | 字典父ID |
| description_ | [string](#string) |  | 描述 |
| enable_ | [bool](#bool) |  | 是否启用 |






<a name="jmash-dict-DictLayEntryEnableKey"></a>

### DictLayEntryEnableKey
启用/禁用


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  |  |
| dict_id | [string](#string) |  |  |
| enable | [bool](#bool) |  |  |






<a name="jmash-dict-DictLayEntryKey"></a>

### DictLayEntryKey
层级字典实体主键


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  |  |
| dict_id | [string](#string) |  |  |






<a name="jmash-dict-DictLayEntryKeyList"></a>

### DictLayEntryKeyList
层级字典实体List


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  |  |
| dict_id | [string](#string) | repeated |  |






<a name="jmash-dict-DictLayEntryList"></a>

### DictLayEntryList
层级字典实体列表


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [DictLayEntryModel](#jmash-dict-DictLayEntryModel) | repeated | 当前页内容 |






<a name="jmash-dict-DictLayEntryModel"></a>

### DictLayEntryModel
层级字典实体实体


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| dict_id | [string](#string) |  | 字典ID |
| type_code | [string](#string) |  | 字典类型编码 |
| dict_code | [string](#string) |  | 字典编码 |
| dict_name | [string](#string) |  | 字典名称 |
| parent_id | [string](#string) |  | 字典父ID |
| depth_ | [int32](#int32) |  | 深度 |
| list_order | [int32](#int32) |  | 排序 |
| enable_ | [bool](#bool) |  | 是否启用 |
| description_ | [string](#string) |  | 描述 |
| children | [DictLayEntryModel](#jmash-dict-DictLayEntryModel) | repeated | 孩子 |






<a name="jmash-dict-DictLayEntryModelTotal"></a>

### DictLayEntryModelTotal
合计


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| total_size | [int32](#int32) |  | 总记录数 |






<a name="jmash-dict-DictLayEntryMoveKey"></a>

### DictLayEntryMoveKey
上移/下移


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  |  |
| dict_id | [string](#string) |  |  |
| up | [bool](#bool) |  |  |






<a name="jmash-dict-DictLayEntryPage"></a>

### DictLayEntryPage
层级字典实体分页


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [DictLayEntryModel](#jmash-dict-DictLayEntryModel) | repeated | 当前页内容 |
| cur_page | [int32](#int32) |  | 当前页码 |
| page_size | [int32](#int32) |  | 页尺寸 |
| total_size | [int32](#int32) |  | 总记录数 |
| sub_total_dto | [DictLayEntryModelTotal](#jmash-dict-DictLayEntryModelTotal) |  | 本页小计 |
| total_dto | [DictLayEntryModelTotal](#jmash-dict-DictLayEntryModelTotal) |  | 合计 |






<a name="jmash-dict-DictLayEntryReq"></a>

### DictLayEntryReq
层级字典实体查询


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  |  |
| type_code | [string](#string) |  | 字典类型编码(非空) |
| dict_code | [string](#string) |  | 字典编码 |
| has_enable | [bool](#bool) |  | 是否包含启用字段 |
| enable_ | [bool](#bool) |  | 是否启用 |
| parent_id | [string](#string) |  | 字典父ID |
| depth_ | [int32](#int32) |  | 深度 |
| exclude_id | [string](#string) |  | 不包含ID及其孩子. |






<a name="jmash-dict-DictLayEntryUpdateReq"></a>

### DictLayEntryUpdateReq
层级字典实体修改实体


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  |  |
| request_id | [string](#string) |  | 用于检测重复请求的唯一字符串ID |
| validate_only | [bool](#bool) |  | 如果为true，则表示给定的请求仅需要被检验，而不是被执行 |
| update_mask | [google.protobuf.FieldMask](#google-protobuf-FieldMask) |  | 更新字段掩码. |
| dict_id | [string](#string) |  | 字典ID |
| dict_name | [string](#string) |  | 字典名称 |
| parent_id | [string](#string) |  | 字典父ID |
| description_ | [string](#string) |  | 描述 |
| enable_ | [bool](#bool) |  | 是否启用 |





 

 

 

 



<a name="jmash_dict_protobuf_os_dict_type_message-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## jmash/dict/protobuf/os_dict_type_message.proto



<a name="jmash-dict-DictTypeCreateReq"></a>

### DictTypeCreateReq
字典类型新增实体


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  |  |
| request_id | [string](#string) |  | 用于检测重复请求的唯一字符串ID |
| validate_only | [bool](#bool) |  | 如果为true，则表示给定的请求仅需要被检验，而不是被执行 |
| type_code | [string](#string) |  | 字典类型编码 |
| type_name | [string](#string) |  | 字典类型名称 |
| entry_type | [EntryTypes](#jmash-dict-EntryTypes) |  | 字典实体类型 |
| description_ | [string](#string) |  | 描述 |
| enable_ | [bool](#bool) |  | 是否启用 |
| is_open | [bool](#bool) |  | 是否开放访问 |






<a name="jmash-dict-DictTypeEnableKey"></a>

### DictTypeEnableKey
启用/禁用


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  |  |
| type_code | [string](#string) |  |  |
| enable | [bool](#bool) |  |  |






<a name="jmash-dict-DictTypeExportReq"></a>

### DictTypeExportReq



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| title | [string](#string) |  | 标题 |
| table_heads | [jmash.protobuf.TableHead](#jmash-protobuf-TableHead) | repeated | 字段列表 |
| file_name | [string](#string) |  | 显示文件名 |
| req | [DictTypeReq](#jmash-dict-DictTypeReq) |  | 筛选条件 |






<a name="jmash-dict-DictTypeImportReq"></a>

### DictTypeImportReq
导入请求


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| request_id | [string](#string) |  | 用于检测重复请求的唯一字符串ID |
| file_names | [string](#string) |  | 文件名 |
| add_flag | [bool](#bool) |  | 是否新增标识 |
| update_mask | [google.protobuf.FieldMask](#google-protobuf-FieldMask) |  | 更新字段掩码. |






<a name="jmash-dict-DictTypeKey"></a>

### DictTypeKey
字典类型主键


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  |  |
| type_code | [string](#string) |  |  |






<a name="jmash-dict-DictTypeKeyList"></a>

### DictTypeKeyList
字典类型List


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  |  |
| type_code | [string](#string) | repeated |  |






<a name="jmash-dict-DictTypeList"></a>

### DictTypeList
字典类型列表


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [DictTypeModel](#jmash-dict-DictTypeModel) | repeated | 当前页内容 |






<a name="jmash-dict-DictTypeModel"></a>

### DictTypeModel
字典类型实体


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| type_code | [string](#string) |  | 字典类型编码 |
| type_name | [string](#string) |  | 字典类型名称 |
| list_order | [int32](#int32) |  | 排序 |
| enable_ | [bool](#bool) |  | 是否启用 |
| entry_type | [EntryTypes](#jmash-dict-EntryTypes) |  | 字典实体类型 |
| description_ | [string](#string) |  | 描述 |
| is_open | [bool](#bool) |  | 是否开放访问 |






<a name="jmash-dict-DictTypeModelTotal"></a>

### DictTypeModelTotal
合计


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| total_size | [int32](#int32) |  | 总记录数 |






<a name="jmash-dict-DictTypeMoveKey"></a>

### DictTypeMoveKey
上移/下移


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  |  |
| type_code | [string](#string) |  |  |
| up | [bool](#bool) |  |  |






<a name="jmash-dict-DictTypePage"></a>

### DictTypePage
字典类型分页


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [DictTypeModel](#jmash-dict-DictTypeModel) | repeated | 当前页内容 |
| cur_page | [int32](#int32) |  | 当前页码 |
| page_size | [int32](#int32) |  | 页尺寸 |
| total_size | [int32](#int32) |  | 总记录数 |
| sub_total_dto | [DictTypeModelTotal](#jmash-dict-DictTypeModelTotal) |  | 本页小计 |
| total_dto | [DictTypeModelTotal](#jmash-dict-DictTypeModelTotal) |  | 合计 |






<a name="jmash-dict-DictTypeReq"></a>

### DictTypeReq
字典类型查询


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  |  |
| cur_page | [int32](#int32) |  | 当前页码 |
| page_size | [int32](#int32) |  | 页尺寸 |
| order_name | [string](#string) |  | 排序名称 |
| order_asc | [bool](#bool) |  | 是否升序排序 |
| type_code | [string](#string) |  | 字典类型编码 |
| like_type_name | [string](#string) |  | 字典类型名称(模糊查询) |
| has_enable | [bool](#bool) |  | 是否包含启用字段 |
| enable_ | [bool](#bool) |  | 是否启用 |
| has_entry_type | [bool](#bool) |  | 是否包含字典实体类型 |
| entry_type | [EntryTypes](#jmash-dict-EntryTypes) |  | 字典实体类型 |






<a name="jmash-dict-DictTypeUpdateReq"></a>

### DictTypeUpdateReq
字典类型修改实体


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  |  |
| request_id | [string](#string) |  | 用于检测重复请求的唯一字符串ID |
| validate_only | [bool](#bool) |  | 如果为true，则表示给定的请求仅需要被检验，而不是被执行 |
| update_mask | [google.protobuf.FieldMask](#google-protobuf-FieldMask) |  | 更新字段掩码. |
| type_code | [string](#string) |  | 字典类型编码 |
| type_name | [string](#string) |  | 字典类型名称 |
| enable_ | [bool](#bool) |  | 是否启用 |
| description_ | [string](#string) |  | 描述 |
| is_open | [bool](#bool) |  | 是否开放访问 |





 


<a name="jmash-dict-EntryTypes"></a>

### EntryTypes


| Name | Number | Description |
| ---- | ------ | ----------- |
| normal | 0 | 普通字典 |
| layout | 1 | 层级字典 |


 

 

 



## Scalar Value Types

| .proto Type | Notes | C++ | Java | Python | Go | C# | PHP | Ruby |
| ----------- | ----- | --- | ---- | ------ | -- | -- | --- | ---- |
| <a name="double" /> double |  | double | double | float | float64 | double | float | Float |
| <a name="float" /> float |  | float | float | float | float32 | float | float | Float |
| <a name="int32" /> int32 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint32 instead. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="int64" /> int64 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint64 instead. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="uint32" /> uint32 | Uses variable-length encoding. | uint32 | int | int/long | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="uint64" /> uint64 | Uses variable-length encoding. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum or Fixnum (as required) |
| <a name="sint32" /> sint32 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int32s. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sint64" /> sint64 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int64s. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="fixed32" /> fixed32 | Always four bytes. More efficient than uint32 if values are often greater than 2^28. | uint32 | int | int | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="fixed64" /> fixed64 | Always eight bytes. More efficient than uint64 if values are often greater than 2^56. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum |
| <a name="sfixed32" /> sfixed32 | Always four bytes. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sfixed64" /> sfixed64 | Always eight bytes. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="bool" /> bool |  | bool | boolean | boolean | bool | bool | boolean | TrueClass/FalseClass |
| <a name="string" /> string | A string must always contain UTF-8 encoded or 7-bit ASCII text. | string | String | str/unicode | string | string | string | String (UTF-8) |
| <a name="bytes" /> bytes | May contain any arbitrary sequence of bytes. | string | ByteString | str | []byte | ByteString | string | String (ASCII-8BIT) |

