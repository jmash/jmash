import subprocess
import argparse
import pathlib 
import sys
# export PYTHONPATH="/data/work/jmash/python/jmash-cmd"
import jmash_cmd.mvn
import jmash_cmd.gw
import jmash_cmd.docker
import jmash_cmd.k8s


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Jmash K8S应用程序管理脚本")
    parser.add_argument("action", choices=["package","publish","deploy", "restart", "delete", "scale", "test","notest","all"], help="要执行的操作")
    parser.add_argument("arch", choices=["amd64", "arm64"], help="目标架构")         
    
   
    parser.add_argument("--namespace", default="jmash", help="命名空间")
    parser.add_argument("--name", default="dict", help="应用程序名称")
    parser.add_argument("--version", default="1.0.0", help="版本")
    parser.add_argument("--replicas", default="1", help="副本数量")
    args = parser.parse_args()
    # 路径
    args.path= pathlib.Path.cwd()
    print(args);
    mirror="harbor.crenjoy.com:9443"
    
    
    if args.action == "all":
        jmash_cmd.mvn.mvn_main(args.namespace,args.name)
        jmash_cmd.gw.gw_main(f"../{args.namespace}-{args.name}-gateway",args.arch,args.namespace,args.name)
        jmash_cmd.docker.docker_build_push(args.namespace,args.name,args.arch, args.version,mirror)
        jmash_cmd.k8s.k8s_deploy(args.namespace,args.name,args.arch,args.version,mirror)
    elif args.action == "package":
        jmash_cmd.mvn.mvn_main(args.namespace,args.name)
        jmash_cmd.gw.gw_main(f"../{args.namespace}-{args.name}-gateway",args.arch,args.namespace,args.name)
    elif args.action == "publish":
        jmash_cmd.docker.docker_build_push(args.namespace,args.name,args.arch, args.version,mirror)
    elif args.action == "deploy":
        jmash_cmd.k8s.k8s_deploy(args.namespace,args.name,args.arch,args.version,mirror)
    elif args.action == "restart":
        jmash_cmd.k8s.k8s_restart(args.namespace,args.name,args.arch)
    elif args.action == "delete":
        jmash_cmd.k8s.k8s_delete(args.namespace,args.name, args.arch)
    elif args.action == "scale":
        jmash_cmd.k8s.k8s_scale(args.namespace,args.name,args.replicas)
    elif args.action == "test":
        jmash_cmd.k8s.k8s_test()
    elif args.action == "notest":
        jmash_cmd.k8s.k8s_notest()

