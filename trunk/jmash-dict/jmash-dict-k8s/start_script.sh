#!/bin/sh

# Start the first process
java -jar -server jmash-dict-service-1.0.0.jar &

# Start the second process
./gateway &

# Wait for any process to exit
wait

# Exit with status of process that exited first
exit $?
