
package com.gitee.jmash.dict.client;

import com.gitee.jmash.common.grpc.client.GrpcChannel;
import jmash.dict.DictGrpc;

/** Dict Client . */
public class DictClient {

  /** DictBlockingStub. */
  public static DictGrpc.DictBlockingStub getDictBlockingStub() {
    return DictGrpc.newBlockingStub(DictClientConfig.getManagedChannel())
        .withCallCredentials(GrpcChannel.getCallCredentials());
  }

  /** DictFutureStub. */
  public static DictGrpc.DictFutureStub getDictFutureStub() {
    return DictGrpc.newFutureStub(DictClientConfig.getManagedChannel())
        .withCallCredentials(GrpcChannel.getCallCredentials());
  }

  /** DictStub. */
  public static DictGrpc.DictStub getDictStub() {
    return DictGrpc.newStub(DictClientConfig.getManagedChannel())
        .withCallCredentials(GrpcChannel.getCallCredentials());
  }

}
