package com.gitee.jmash.dict;

import com.gitee.jmash.dict.service.DictEntryRead;
import com.gitee.jmash.dict.service.DictEntryWrite;
import com.gitee.jmash.dict.service.DictLayEntryRead;
import com.gitee.jmash.dict.service.DictLayEntryWrite;
import com.gitee.jmash.dict.service.DictTypeRead;
import com.gitee.jmash.dict.service.DictTypeWrite;
import jakarta.enterprise.inject.spi.CDI;

/**
 * 模块服务工厂类.
 *
 * @author CGD
 *
 */
public class DictFactory {

  public static DictEntryRead getDictEntryRead(String tenant) {
    return CDI.current().select(DictEntryRead.class).get().setTenant(tenant);
  }

  public static DictLayEntryRead getDictLayEntryRead(String tenant) {
    return CDI.current().select(DictLayEntryRead.class).get().setTenant(tenant);
  }

  public static DictTypeRead getDictTypeRead(String tenant) {
    return CDI.current().select(DictTypeRead.class).get().setTenant(tenant);
  }

  public static DictEntryWrite getDictEntryWrite(String tenant) {
    return CDI.current().select(DictEntryWrite.class).get().setTenant(tenant);
  }

  public static DictLayEntryWrite getDictLayEntryWrite(String tenant) {
    return CDI.current().select(DictLayEntryWrite.class).get().setTenant(tenant);
  }

  public static DictTypeWrite getDictTypeWrite(String tenant) {
    return CDI.current().select(DictTypeWrite.class).get().setTenant(tenant);
  }

  public static void destroy(Object instance) {
    if (null != instance) {
      CDI.current().destroy(instance);
    }
  }
}
