
package com.gitee.jmash.dict;

import com.crenjoy.proto.mapper.BeanMapper;
import com.crenjoy.proto.mapper.ProtoMapper;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * Dict Mapper.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 *
 */
@Mapper
public interface DictMapper extends BeanMapper, ProtoMapper {

  DictMapper INSTANCE = Mappers.getMapper(DictMapper.class);

}
