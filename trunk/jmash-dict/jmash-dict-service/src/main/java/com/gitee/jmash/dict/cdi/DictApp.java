
package com.gitee.jmash.dict.cdi;

import com.gitee.jmash.core.grpc.DefaultGrpcServer;
import com.gitee.jmash.core.orm.module.DbBuild;
import com.gitee.jmash.rbac.client.shiro.JmashClientShiroConfig;
import jakarta.enterprise.inject.se.SeContainer;
import jakarta.enterprise.inject.se.SeContainerInitializer;

public class DictApp {

  public static void main(String[] args) throws Exception {
    try (SeContainer container = SeContainerInitializer.newInstance().initialize()) {
      // 构建数据库.
      DbBuild dbBuild = container.select(DbBuild.class).get();
      dbBuild.build();
      JmashClientShiroConfig.config();
      final DefaultGrpcServer server = container.select(DefaultGrpcServer.class).get();
      server.start(false);
      server.blockUntilShutdown();
    }
  }

}
