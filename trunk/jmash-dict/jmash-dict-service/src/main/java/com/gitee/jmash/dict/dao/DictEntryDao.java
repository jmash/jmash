
package com.gitee.jmash.dict.dao;

import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.orm.jpa.BaseDao;
import com.gitee.jmash.core.orm.jpa.SqlBuilder;
import com.gitee.jmash.core.orm.jpa.TenantEntityManager;
import com.gitee.jmash.dict.entity.DictEntryEntity;
import com.gitee.jmash.dict.entity.DictEntryEntity.DictEntryPk;
import com.gitee.jmash.dict.model.DictEntryTotal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import jmash.dict.protobuf.DictEntryMoveKey;
import jmash.dict.protobuf.DictEntryReq;
import org.apache.commons.lang3.StringUtils;

/**
 * DictEntry实体的Dao层（使用JPA实现）.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public class DictEntryDao extends BaseDao<DictEntryEntity, DictEntryPk> {

  public DictEntryDao() {
    super();
  }

  public DictEntryDao(TenantEntityManager tem) {
    super(tem);
  }

  /**
   * 查询记录数.
   */
  public int findCount(DictEntryReq req) {
    SqlBuilder sqlBuilder = createSql(req);
    String query = sqlBuilder.getQuerySql("select count(s) ");
    Object obj = this.findSingleResultByParams(query, sqlBuilder.getParams());
    return (obj == null) ? 0 : Integer.valueOf(obj.toString());
  }


  /**
   * 综合查询.
   */
  public List<DictEntryEntity> findListByReq(DictEntryReq req) {
    SqlBuilder sqlBuilder = createSql(req);
    String query = sqlBuilder.getQuerySql("select s ");
    return this.findListByParams(query, sqlBuilder.getParams());
  }

  /**
   * 综合查询Page.
   */
  public DtoPage<DictEntryEntity, DictEntryTotal> findPageByReq(DictEntryReq req) {
    SqlBuilder sqlBuilder = createSql(req);
    String query = sqlBuilder.getQuerySql("select s ");
    String totalQuery = sqlBuilder.getTotalSql("select count(s) as totalSize ");
    return this.findDtoPageByParams(req.getCurPage(), req.getPageSize(), query, totalQuery,
        DictEntryTotal.class, sqlBuilder.getParams());
  }

  /** Create SQL By Req . */
  public SqlBuilder createSql(DictEntryReq req) {
    StringBuilder sql = new StringBuilder(" from DictEntryEntity s where 1=1  ");
    Map<String, Object> params = new HashMap<String, Object>();

    if (StringUtils.isNotBlank(req.getTypeCode())) {
      sql.append(" and s.typeCode = :typeCode ");
      params.put("typeCode", req.getTypeCode());
    }

    if (StringUtils.isNotBlank(req.getDictCode())) {
      sql.append(" and s.dictCode = :dictCode ");
      params.put("dictCode", req.getDictCode());
    }

    if (req.getHasEnable()) {
      sql.append(" and s.enable = :enable ");
      params.put("enable", req.getEnable());
    }

    String orderSql = " order by s.listOrder ";
    if (StringUtils.isNotBlank(req.getOrderName())) {
      orderSql = String.format(" order by %s %s ", req.getOrderName(),
          req.getOrderAsc() ? " asc " : " desc ");
    }
    return SqlBuilder.build().setSql(sql).setParams(params).setOrderSql(orderSql);
  }

  // 获取数据字典类型,排序数据.
  public List<DictEntryEntity> selectListByOrder(Integer orderBy, DictEntryMoveKey moveKey) {
    StringBuilder sb =
        new StringBuilder("select s from DictEntryEntity s where typeCode=:typeCode  ");
    boolean moveUp = moveKey.getUp();
    if (moveUp) {
      sb.append(" and s.listOrder <= :orderBy order by s.listOrder desc limit 2");
    } else {
      sb.append(" and s.listOrder >= :orderBy order by s.listOrder asc limit 2");
    }
    return getEntityManager().createQuery(sb.toString(), DictEntryEntity.class)
        .setParameter("typeCode", moveKey.getTypeCode()).setParameter("orderBy", orderBy)
        .getResultList();
  }

  // 获取排序最大值.
  public int getMaxListOrder(String typeCode) {
    String sql = "select max(s.listOrder) from DictEntryEntity s where s.typeCode =?1";
    Object obj = this.findSingleResult(sql, typeCode);
    if (obj instanceof Long) {
      return ((Long) obj).intValue();
    }
    if (obj instanceof Integer) {
      return (Integer) obj;
    }
    return 0;
  }


}
