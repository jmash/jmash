
package com.gitee.jmash.dict.dao;

import com.gitee.jmash.core.orm.jpa.BaseDao;
import com.gitee.jmash.core.orm.jpa.SqlBuilder;
import com.gitee.jmash.core.orm.jpa.TenantEntityManager;
import com.gitee.jmash.dict.entity.DictLayEntryEntity;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import jmash.dict.protobuf.DictLayEntryReq;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * DictLayEntry实体的Dao层（使用JPA实现）.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public class DictLayEntryDao extends BaseDao<DictLayEntryEntity, UUID> {

  public DictLayEntryDao() {
    super();
  }

  public DictLayEntryDao(TenantEntityManager tem) {
    super(tem);
  }

  /**
   * 查询记录数.
   */
  public int findCount(DictLayEntryReq req) {
    SqlBuilder sqlBuilder = createSql(req);
    String query = sqlBuilder.getQuerySql("select count(s) ");
    Object obj = this.findSingleResultByParams(query, sqlBuilder.getParams());
    return (obj == null) ? 0 : Integer.valueOf(obj.toString());
  }

  /**
   * 综合查询.
   */
  public List<DictLayEntryEntity> findListByReq(DictLayEntryReq req) {
    SqlBuilder sqlBuilder = createSql(req);
    String query = sqlBuilder.getQuerySql("select s ");
    return this.findListByParams(query, sqlBuilder.getParams());
  }


  /** Create SQL By Req . */
  public SqlBuilder createSql(DictLayEntryReq req) {
    StringBuilder sql = new StringBuilder(" from DictLayEntryEntity s where 1=1  ");
    Map<String, Object> params = new HashMap<String, Object>();

    if (StringUtils.isNotBlank(req.getTypeCode())) {
      sql.append(" and s.typeCode = :typeCode ");
      params.put("typeCode", req.getTypeCode());
    }

    if (StringUtils.isNotBlank(req.getDictCode())) {
      sql.append(" and s.dictCode = :dictCode ");
      params.put("dictCode", req.getDictCode());
    }

    if (StringUtils.isNotBlank(req.getParentId())) {
      sql.append(" and s.parentId = :parentId ");
      params.put("parentId", req.getParentId());
    }

    if (req.getDepth() != 0) {
      sql.append(" and s.depth = :depth ");
      params.put("depth", req.getDepth());
    }

    if (req.getHasEnable()) {
      sql.append(" and s.enable = :enable ");
      params.put("enable", req.getEnable());
    }

    String orderSql = " order by s.depth , s.listOrder ";
    return SqlBuilder.build().setSql(sql).setParams(params).setOrderSql(orderSql);
  }

  // 获取排序最大值.
  public Integer maxLayOrderByParentId(UUID parentId) {
    String queryString = "select max(s.listOrder) from DictLayEntryEntity  s where s.parentId=?1  ";
    Object value = this.findSingleResult(queryString, parentId);
    return (Integer) ConvertUtils.convert(value, Integer.class);
  }

}
