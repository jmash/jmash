
package com.gitee.jmash.dict.dao;

import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.orm.jpa.BaseDao;
import com.gitee.jmash.core.orm.jpa.SqlBuilder;
import com.gitee.jmash.core.orm.jpa.TenantEntityManager;
import com.gitee.jmash.dict.entity.DictTypeEntity;
import com.gitee.jmash.dict.model.DictTypeTotal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import jmash.dict.protobuf.DictTypeMoveKey;
import jmash.dict.protobuf.DictTypeReq;
import org.apache.commons.lang3.StringUtils;

/**
 * DictType实体的Dao层（使用JPA实现）.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public class DictTypeDao extends BaseDao<DictTypeEntity, String> {

  public DictTypeDao() {
    super();
  }

  public DictTypeDao(TenantEntityManager tem) {
    super(tem);
  }


  /**
   * 综合查询.
   */
  public List<DictTypeEntity> findListByReq(DictTypeReq req) {
    SqlBuilder sqlBuilder = createSql(req);
    String query = sqlBuilder.getQuerySql("select s ");
    return this.findListByParams(query, sqlBuilder.getParams());
  }

  /**
   * 综合查询Page.
   */
  public DtoPage<DictTypeEntity, DictTypeTotal> findPageByReq(DictTypeReq req) {
    SqlBuilder sqlBuilder = createSql(req);
    String query = sqlBuilder.getQuerySql("select s ");
    String totalQuery = sqlBuilder.getTotalSql("select count(s) as totalSize ");
    return this.findDtoPageByParams(req.getCurPage(), req.getPageSize(), query, totalQuery,
        DictTypeTotal.class, sqlBuilder.getParams());
  }

  /** Create SQL By Req . */
  public SqlBuilder createSql(DictTypeReq req) {
    StringBuilder sql = new StringBuilder(" from DictTypeEntity s where 1=1  ");
    Map<String, Object> params = new HashMap<String, Object>();

    if (StringUtils.isNotBlank(req.getTypeCode())) {
      sql.append(" and s.typeCode = :typeCode ");
      params.put("typeCode", req.getTypeCode());
    }

    if (req.getHasEnable()) {
      sql.append(" and s.enable = :enable ");
      params.put("enable", req.getEnable());
    }

    if (req.getHasEntryType()) {
      sql.append(" and s.entryType = :entryType ");
      params.put("entryType", req.getEntryType());
    }

    if (StringUtils.isNotBlank(req.getLikeTypeName())) {
      sql.append(" and s.typeName like :likeTypeName ");
      params.put("likeTypeName", "%" + req.getLikeTypeName() + "%");
    }

    String orderSql = "order by s.listOrder ";
    if (StringUtils.isNotBlank(req.getOrderName())) {
      orderSql = String.format(" order by %s %s ", req.getOrderName(),
          req.getOrderAsc() ? " asc " : " desc ");
    }
    return SqlBuilder.build().setSql(sql).setParams(params).setOrderSql(orderSql);
  }

  // 获取数据字典类型,排序数据.
  public List<DictTypeEntity> selectListByOrder(Integer orderBy, DictTypeMoveKey moveKey) {
    StringBuilder sb = new StringBuilder("select s from DictTypeEntity s where 1=1  ");
    boolean moveUp = moveKey.getUp();
    if (moveUp) {
      sb.append(" and s.listOrder <= :orderBy order by s.listOrder desc limit 2");
    } else {
      sb.append(" and s.listOrder >= :orderBy order by s.listOrder asc limit 2");
    }
    return getEntityManager().createQuery(sb.toString(), DictTypeEntity.class)
        .setParameter("orderBy", orderBy).getResultList();
  }

  // 获取排序最大值.
  public int getMaxListOrder() {
    Object obj = getMaxField("listOrder");
    if (obj instanceof Long) {
      return ((Long) obj).intValue();
    }
    if (obj instanceof Integer) {
      return (Integer) obj;
    }
    return 0;
  }

}
