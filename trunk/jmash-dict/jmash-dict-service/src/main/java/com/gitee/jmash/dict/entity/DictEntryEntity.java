
package com.gitee.jmash.dict.entity;


import com.gitee.jmash.dict.entity.DictEntryEntity.DictEntryPk;
import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import jakarta.validation.constraints.NotNull;
import java.io.Serializable;


/**
 * 本代码为自动生成工具生成 数据字典表 .
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Entity
@Table(name = "os_dict_entry")
@IdClass(DictEntryPk.class)
public class DictEntryEntity implements Serializable {

  private static final long serialVersionUID = 1L;

  /** 字典类型编码. */
  private String typeCode;
  /** 字典编码. */
  private String dictCode;
  /** 字典名称. */
  private String dictName;
  /** 排序. */
  private Integer listOrder = 0;
  /** 是否启用. */
  private Boolean enable = false;
  /** 描述. */
  private String description;

  /**
   * 默认构造函数.
   */
  public DictEntryEntity() {
    super();
  }

  /** 实体主键. */
  @Transient
  @JsonbTransient
  public DictEntryPk getEntityPk() {
    DictEntryPk pk = new DictEntryPk();
    pk.setDictCode(this.dictCode);
    pk.setTypeCode(this.typeCode);
    return pk;
  }

  /** 实体主键. */
  public void setEntityPk(DictEntryPk pk) {
    this.dictCode = pk.getDictCode();
    this.typeCode = pk.getTypeCode();
  }

  /** 字典类型编码(TypeCode). */
  @Id

  @Column(name = "type_code", nullable = false)
  public String getTypeCode() {
    return typeCode;
  }

  /** 字典类型编码(TypeCode). */
  public void setTypeCode(String typeCode) {
    this.typeCode = typeCode;
  }

  /** 字典编码(DictCode). */
  @Id

  @Column(name = "dict_code", nullable = false)
  public String getDictCode() {
    return dictCode;
  }

  /** 字典编码(DictCode). */
  public void setDictCode(String dictCode) {
    this.dictCode = dictCode;
  }

  /** 字典名称(DictName). */

  @Column(name = "dict_name", nullable = false)
  public String getDictName() {
    return dictName;
  }

  /** 字典名称(DictName). */
  public void setDictName(String dictName) {
    this.dictName = dictName;
  }

  /** 排序(ListOrder). */
  @Column(name = "list_order")
  public Integer getListOrder() {
    return listOrder;
  }

  /** 排序(ListOrder). */
  public void setListOrder(Integer listOrder) {
    this.listOrder = listOrder;
  }

  /** 是否启用(Enable). */

  @Column(name = "enable_", nullable = false)
  public Boolean getEnable() {
    return enable;
  }

  /** 是否启用(Enable). */
  public void setEnable(Boolean enable) {
    this.enable = enable;
  }

  /** 描述(Description). */
  @Column(name = "description_")
  public String getDescription() {
    return description;
  }

  /** 描述(Description). */
  public void setDescription(String description) {
    this.description = description;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((getEntityPk() == null) ? 0 : getEntityPk().hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final DictEntryEntity other = (DictEntryEntity) obj;
    if (getEntityPk() == null) {
      if (other.getEntityPk() != null) {
        return false;
      }
    } else if (!getEntityPk().equals(other.getEntityPk())) {
      return false;
    }
    return true;
  }


  /**
   * DictEntryPK（直接对应数据库的配置 由生成工具产生）.
   *
   * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
   */
  @Embeddable
  public static class DictEntryPk implements Serializable {

    private static final long serialVersionUID = 1L;

    /** 字典类型编码. */
    @NotNull
    private String typeCode;
    /** 字典编码. */
    @NotNull
    private String dictCode;

    public DictEntryPk() {
      super();
    }

    public DictEntryPk(@NotNull String typeCode, @NotNull String dictCode) {
      super();
      this.dictCode = dictCode;
      this.typeCode = typeCode;
    }

    /** 字典编码(DictCode). */
    @Column(name = "dict_code", nullable = false)
    public String getDictCode() {
      return dictCode;
    }

    /** 字典编码(DictCode). */
    public void setDictCode(String dictCode) {
      this.dictCode = dictCode;
    }

    /** 字典类型编码(TypeCode). */
    @Column(name = "type_code", nullable = false)
    public String getTypeCode() {
      return typeCode;
    }

    /** 字典类型编码(TypeCode). */
    public void setTypeCode(String typeCode) {
      this.typeCode = typeCode;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((dictCode == null) ? 0 : dictCode.hashCode());
      result = prime * result + ((typeCode == null) ? 0 : typeCode.hashCode());
      return result;
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj) {
        return true;
      }
      if (obj == null) {
        return false;
      }
      if (getClass() != obj.getClass()) {
        return false;
      }
      final DictEntryPk other = (DictEntryPk) obj;

      if (dictCode == null) {
        if (other.dictCode != null) {
          return false;
        }
      } else if (!dictCode.equals(other.dictCode)) {
        return false;
      }
      if (typeCode == null) {
        if (other.typeCode != null) {
          return false;
        }
      } else if (!typeCode.equals(other.typeCode)) {
        return false;
      }
      return true;
    }
  }

}
