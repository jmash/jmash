
package com.gitee.jmash.dict.entity;


import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import java.io.Serializable;
import java.util.UUID;


/**
 * 本代码为自动生成工具生成 层级字典实体表 .
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Entity
@Table(name = "os_dict_lay_entry")
public class DictLayEntryEntity implements Serializable {

  private static final long serialVersionUID = 1L;

  /** 字典ID. */
  private UUID dictId = UUID.randomUUID();
  /** 字典类型编码. */
  private String typeCode;
  /** 字典编码. */
  private String dictCode;
  /** 字典名称. */
  private String dictName;
  /** 字典父ID. */
  private UUID parentId;
  /** 深度. */
  private Integer depth = 0;
  /** 排序. */
  private Integer listOrder = 0;
  /** 是否启用. */
  private Boolean enable = false;
  /** 描述. */
  private String description;

  /**
   * 默认构造函数.
   */
  public DictLayEntryEntity() {
    super();
  }

  /** 实体主键. */
  @Transient
  @JsonbTransient
  public UUID getEntityPk() {
    return this.dictId;
  }

  /** 实体主键. */
  public void setEntityPk(UUID pk) {
    this.dictId = pk;
  }

  /** 字典ID(DictId). */
  @Id
  @Column(name = "dict_id", nullable = false,columnDefinition = "BINARY(16)")
  public UUID getDictId() {
    return dictId;
  }

  /** 字典ID(DictId). */
  public void setDictId(UUID dictId) {
    this.dictId = dictId;
  }

  /** 字典类型编码(TypeCode). */

  @Column(name = "type_code", nullable = false)
  public String getTypeCode() {
    return typeCode;
  }

  /** 字典类型编码(TypeCode). */
  public void setTypeCode(String typeCode) {
    this.typeCode = typeCode;
  }

  /** 字典编码(DictCode). */

  @Column(name = "dict_code", nullable = false)
  public String getDictCode() {
    return dictCode;
  }

  /** 字典编码(DictCode). */
  public void setDictCode(String dictCode) {
    this.dictCode = dictCode;
  }

  /** 字典名称(DictName). */

  @Column(name = "dict_name", nullable = false)
  public String getDictName() {
    return dictName;
  }

  /** 字典名称(DictName). */
  public void setDictName(String dictName) {
    this.dictName = dictName;
  }

  /** 字典父ID(ParentId). */
  @Column(name = "parent_id",columnDefinition = "BINARY(16)")
  public UUID getParentId() {
    return parentId;
  }

  /** 字典父ID(ParentId). */
  public void setParentId(UUID parentId) {
    this.parentId = parentId;
  }

  /** 深度(Depth). */

  @Column(name = "depth_", nullable = false)
  public Integer getDepth() {
    return depth;
  }

  /** 深度(Depth). */
  public void setDepth(Integer depth) {
    this.depth = depth;
  }

  /** 排序(ListOrder). */

  @Column(name = "list_order", nullable = false)
  public Integer getListOrder() {
    return listOrder;
  }

  /** 排序(ListOrder). */
  public void setListOrder(Integer listOrder) {
    this.listOrder = listOrder;
  }

  /** 是否启用(Enable). */

  @Column(name = "enable_", nullable = false)
  public Boolean getEnable() {
    return enable;
  }

  /** 是否启用(Enable). */
  public void setEnable(Boolean enable) {
    this.enable = enable;
  }

  /** 描述(Description). */
  @Column(name = "description_")
  public String getDescription() {
    return description;
  }

  /** 描述(Description). */
  public void setDescription(String description) {
    this.description = description;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((dictId == null) ? 0 : dictId.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final DictLayEntryEntity other = (DictLayEntryEntity) obj;
    if (dictId == null) {
      if (other.dictId != null) {
        return false;
      }
    } else if (!dictId.equals(other.dictId)) {
      return false;
    }
    return true;
  }



}
