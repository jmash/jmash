
package com.gitee.jmash.dict.entity;


import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import java.io.Serializable;
import jmash.dict.protobuf.EntryTypes;


/**
 * 本代码为自动生成工具生成 字典类型表 .
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Entity
@Table(name = "os_dict_type")
public class DictTypeEntity implements Serializable {

  private static final long serialVersionUID = 1L;

  /** 字典类型编码. */
  private String typeCode;
  /** 字典类型名称. */
  private String typeName;
  /** 排序. */
  private Integer listOrder = 0;
  /** 是否启用. */
  private Boolean enable = true;
  /** 是否开放访问 */
  private Boolean isOpen = false;
  /** 字典实体类型. */
  private EntryTypes entryType;
  /** 描述. */
  private String description;

  /**
   * 默认构造函数.
   */
  public DictTypeEntity() {
    super();
  }

  /** 实体主键. */
  @Transient
  @JsonbTransient
  public String getEntityPk() {
    return this.typeCode;
  }

  /** 实体主键. */
  public void setEntityPk(String pk) {
    this.typeCode = pk;
  }

  /** 字典类型编码(TypeCode). */
  @Id

  @Column(name = "type_code", nullable = false)
  public String getTypeCode() {
    return typeCode;
  }

  /** 字典类型编码(TypeCode). */
  public void setTypeCode(String typeCode) {
    this.typeCode = typeCode;
  }

  /** 字典类型名称(TypeName). */

  @Column(name = "type_name", nullable = false)
  public String getTypeName() {
    return typeName;
  }

  /** 字典类型名称(TypeName). */
  public void setTypeName(String typeName) {
    this.typeName = typeName;
  }

  /** 排序(ListOrder). */
  @Column(name = "list_order")
  public Integer getListOrder() {
    return listOrder;
  }

  /** 排序(ListOrder). */
  public void setListOrder(Integer listOrder) {
    this.listOrder = listOrder;
  }

  /** 是否启用(Enable). */
  @Column(name = "enable_", nullable = false)
  public Boolean getEnable() {
    return enable;
  }

  /** 是否启用(Enable). */
  public void setEnable(Boolean enable) {
    this.enable = enable;
  }

  /** 是否开放访问. */
  @Column(name = "is_open", nullable = false)
  public Boolean getIsOpen() {
    return isOpen;
  }

  /** 是否开放访问. */
  public void setIsOpen(Boolean isOpen) {
    this.isOpen = isOpen;
  }

  /** 字典实体类型(EntryType). */
  @Column(name = "entry_type")
  @Enumerated(EnumType.STRING)
  public EntryTypes getEntryType() {
    return entryType;
  }

  /** 字典实体类型(EntryType). */
  public void setEntryType(EntryTypes entryType) {
    this.entryType = entryType;
  }

  /** 描述(Description). */
  @Column(name = "description_")
  public String getDescription() {
    return description;
  }

  /** 描述(Description). */
  public void setDescription(String description) {
    this.description = description;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((typeCode == null) ? 0 : typeCode.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final DictTypeEntity other = (DictTypeEntity) obj;
    if (typeCode == null) {
      if (other.typeCode != null) {
        return false;
      }
    } else if (!typeCode.equals(other.typeCode)) {
      return false;
    }
    return true;
  }



}
