package com.gitee.jmash.dict.enums;

/** 表名. */
public enum TableName {
  /** 字典类型. */
  Type,
  /** 普通字典. */
  Entry,
  /** 层级字典. */
  LayEntry;
}
