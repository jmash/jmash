package com.gitee.jmash.dict.excel;

import com.gitee.jmash.common.excel.ExcelExport;
import com.gitee.jmash.common.excel.write.HeaderExport;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import jmash.protobuf.TableHead;

/** 数据字典导出. */
public class DictEntryHeaderExport {

  /** 默认表头. */
  public static final String TITLE = "数据字典表";

  /**
   * 规则Header
   * 
   * @return
   */
  public static List<HeaderExport> getHeaderExports() {
    List<HeaderExport> list = new ArrayList<HeaderExport>();
    list.add(HeaderExport.field("字典类型编码", "typeCode", 4 * 1000));
    list.add(HeaderExport.field("字典编码", "dictCode", 4 * 1000));
    list.add(HeaderExport.field("字典名称", "dictName", 4 * 1000));
    list.add(HeaderExport.dict("是否启用", "enable", 3 * 1000, DictTypeHeaderExport.getEnabledMap()));
    list.add(HeaderExport.field("描述", "description", 4 * 1000));
    return list;
  }

  /**
   * Map HeaderExport
   * 
   * @return
   */
  public static Map<String, HeaderExport> getHeaderExportMap() {
    Map<String, HeaderExport> map = getHeaderExports().stream()
        .collect(Collectors.toMap(HeaderExport::getField, Function.identity()));
    return map;
  }

  /** 增加表头. */
  public static void addHeaderExport(ExcelExport excelExport, List<TableHead> tableHeads) {
    if (tableHeads.isEmpty()) {
      for (HeaderExport header : getHeaderExports()) {
        excelExport.addHeader(header);
      }
    } else {
      Map<String, HeaderExport> map = getHeaderExportMap();
      for (TableHead tableHead : tableHeads) {
        if (map.containsKey(tableHead.getProp())) {
          excelExport.addHeader(map.get(tableHead.getProp()));
        }
      }
    }
  }

}
