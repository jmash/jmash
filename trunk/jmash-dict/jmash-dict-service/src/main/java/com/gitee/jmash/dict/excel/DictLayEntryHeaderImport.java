package com.gitee.jmash.dict.excel;

import com.gitee.jmash.common.excel.read.HeaderField;
import java.util.ArrayList;
import java.util.List;

/** 层级字典实体导入. */
public class DictLayEntryHeaderImport {

  /** 规则Header. */
  public static List<HeaderField> getHeaderImports() {
    List<HeaderField> list = new ArrayList<HeaderField>();    
    list.add(HeaderField.field("字典ID", "dictId", true));
    list.add(HeaderField.field("字典类型编码", "typeCode", true));
    list.add(HeaderField.field("字典编码", "dictCode", true));
    list.add(HeaderField.field("字典名称", "dictName", true));
    list.add(HeaderField.field("字典父ID", "parentId", true));
    list.add(HeaderField.dict("是否启用", "enable", true,DictTypeHeaderImport.getEnabledCodeMap()));
    list.add(HeaderField.field("描述", "description", true));
    return list;
  }
  
}
