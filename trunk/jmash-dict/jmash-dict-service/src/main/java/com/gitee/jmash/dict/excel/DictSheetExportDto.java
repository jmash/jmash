package com.gitee.jmash.dict.excel;

import com.gitee.jmash.common.excel.write.HeaderExport;
import com.gitee.jmash.dict.enums.TableName;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class DictSheetExportDto implements Serializable {
  private static final long serialVersionUID = 1L;

  /**
   * sheet页名称
   */
  private String sheetName;

  /**
   * 字段, 别名 如："name", "姓名"
   */
  private Map<String, String> fieldAndAlias;

  /**
   * 数据集
   */
  private List<?> dataList;

  private Collection<HeaderExport> headerCollection;

  public DictSheetExportDto() {
    super();
  }

  /**
   * @param sheetName sheet页名称
   * @param dataList 数据集
   */
  public DictSheetExportDto(String sheetName, List<?> dataList, TableName tableName) {
    super();
    this.sheetName = sheetName;
    this.dataList = dataList;
    this.headerCollection = getHeaderExport(tableName);
  }

  public List<?> getDataList() {
    return dataList;
  }

  public void setDataList(List<?> dataList) {
    this.dataList = dataList;
  }

  public String getSheetName() {
    return sheetName;
  }

  public void setSheetName(String sheetName) {
    this.sheetName = sheetName;
  }

  public Map<String, String> getFieldAndAlias() {
    return fieldAndAlias;
  }

  public void setFieldAndAlias(Map<String, String> fieldAndAlias) {
    this.fieldAndAlias = fieldAndAlias;
  }


  public Collection<HeaderExport> getHeaderCollection() {
    return headerCollection;
  }

  public void setHeaderCollection(Collection<HeaderExport> headerCollection) {
    this.headerCollection = headerCollection;
  }

  public List<HeaderExport> getHeaderExport(TableName tableName) {
    if (TableName.Type.equals(tableName)) {
      return DictTypeHeaderExport.getHeaderExports();
    } else if (TableName.LayEntry.equals(tableName)) {
      return DictLayEntryHeaderExport.getHeaderExports();
    } else {
      return DictEntryHeaderExport.getHeaderExports();
    }
  }
}
