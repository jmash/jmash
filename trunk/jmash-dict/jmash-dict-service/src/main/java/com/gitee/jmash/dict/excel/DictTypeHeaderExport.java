package com.gitee.jmash.dict.excel;

import com.gitee.jmash.common.excel.ExcelExport;
import com.gitee.jmash.common.excel.write.HeaderExport;
import com.gitee.jmash.core.lib.ProtoEnumUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import jmash.dict.protobuf.EntryTypes;
import jmash.protobuf.Entry;
import jmash.protobuf.TableHead;

/** 字典类型导出. */
public class DictTypeHeaderExport {

  /** 默认表头. */
  public static final String TITLE = "字典类型表";

  /**
   * 规则Header
   * 
   * @return
   */
  public static List<HeaderExport> getHeaderExports() {
    List<HeaderExport> list = new ArrayList<HeaderExport>();
    list.add(HeaderExport.field("字典类型编码", "typeCode", 4 * 1000));
    list.add(HeaderExport.field("字典类型名称", "typeName", 4 * 1000));
    list.add(HeaderExport.dict("是否启用", "enable", 3 * 1000, getEnabledMap()));
    list.add(HeaderExport.dict("字典实体类型", "entryType", 3 * 1000, getEntryTypesMap()));
    list.add(HeaderExport.field("描述", "description", 6 * 1000));
    return list;
  }

  /**
   * Map HeaderExport
   * 
   * @return
   */
  public static Map<String, HeaderExport> getHeaderExportMap() {
    Map<String, HeaderExport> map = getHeaderExports().stream()
        .collect(Collectors.toMap(HeaderExport::getField, Function.identity()));
    return map;
  }

  /** 增加表头. */
  public static void addHeaderExport(ExcelExport excelExport, List<TableHead> tableHeads) {
    if (tableHeads.isEmpty()) {
      for (HeaderExport header : getHeaderExports()) {
        excelExport.addHeader(header);
      }
    } else {
      Map<String, HeaderExport> map = getHeaderExportMap();
      for (TableHead tableHead : tableHeads) {
        if (map.containsKey(tableHead.getProp())) {
          excelExport.addHeader(map.get(tableHead.getProp()));
        }
      }
    }
  }

  /** 是否启用Map. */
  public static Map<String, String> getEnabledMap() {
    return getEnabled().stream().collect(Collectors.toMap(Entry::getKey, Entry::getValue));
  }

  /** 是否启用. */
  public static List<Entry> getEnabled() {
    List<Entry> list = new ArrayList<>();
    list.add(Entry.newBuilder().setKey("true").setValue("启用").build());
    list.add(Entry.newBuilder().setKey("false").setValue("禁用").build());
    return list;
  }

  /** 字典实体类型Map. */
  public static Map<String, String> getEntryTypesMap() {
    return ProtoEnumUtil.getEnumNameMap(EntryTypes.class, 0);
  }

}
