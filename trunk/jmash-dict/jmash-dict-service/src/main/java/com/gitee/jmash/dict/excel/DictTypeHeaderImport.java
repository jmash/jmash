package com.gitee.jmash.dict.excel;

import com.gitee.jmash.common.excel.read.HeaderField;
import com.gitee.jmash.core.lib.ProtoEnumUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import jmash.dict.protobuf.EntryTypes;
import jmash.protobuf.Entry;

/** 字典类型导入. */
public class DictTypeHeaderImport {

  /** 规则Header. */
  public static List<HeaderField> getHeaderImports() {
    List<HeaderField> list = new ArrayList<HeaderField>();    
    list.add(HeaderField.field("字典类型编码", "typeCode", true));
    list.add(HeaderField.field("字典类型名称", "typeName", true));
    list.add(HeaderField.dict("是否启用", "enable", true,getEnabledCodeMap()));
    list.add(HeaderField.dict("字典实体类型", "entryType", true,getEntryTypesCodeMap()));
    list.add(HeaderField.field("描述", "description", true));
    return list;
  }
  
  
  /** 启用禁用Map Name,Code. */
  public static Map<String, String> getEnabledCodeMap() {
    return DictTypeHeaderExport.getEnabled().stream()
        .collect(Collectors.toMap(Entry::getValue, Entry::getKey));
  }


  /** 字典实体类型Map Name,Code.. */
  public static Map<String, String> getEntryTypesCodeMap() {
    return ProtoEnumUtil.getEnumCodeMap(EntryTypes.class, -1);
  }
}
