
package com.gitee.jmash.dict.grpc;

import com.gitee.jmash.common.utils.UUIDUtil;
import com.gitee.jmash.common.utils.VersionUtil;
import com.gitee.jmash.core.grpc.cdi.GrpcService;
import com.gitee.jmash.core.lib.ProtoEnumUtil;
import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.utils.FileServiceUtil;
import com.gitee.jmash.dict.DictFactory;
import com.gitee.jmash.dict.entity.DictEntryEntity;
import com.gitee.jmash.dict.entity.DictEntryEntity.DictEntryPk;
import com.gitee.jmash.dict.entity.DictLayEntryEntity;
import com.gitee.jmash.dict.entity.DictTypeEntity;
import com.gitee.jmash.dict.mapper.DictEntryMapper;
import com.gitee.jmash.dict.mapper.DictLayEntryMapper;
import com.gitee.jmash.dict.mapper.DictTypeMapper;
import com.gitee.jmash.dict.model.DictEntryTotal;
import com.gitee.jmash.dict.model.DictTypeTotal;
import com.gitee.jmash.dict.service.DictEntryRead;
import com.gitee.jmash.dict.service.DictEntryWrite;
import com.gitee.jmash.dict.service.DictLayEntryRead;
import com.gitee.jmash.dict.service.DictLayEntryWrite;
import com.gitee.jmash.dict.service.DictTypeRead;
import com.gitee.jmash.dict.service.DictTypeWrite;
import com.gitee.jmash.dict.util.PermUtils;
import com.google.api.HttpBody;
import com.google.protobuf.BoolValue;
import com.google.protobuf.Empty;
import com.google.protobuf.EnumValue;
import com.google.protobuf.Int32Value;
import com.google.protobuf.StringValue;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import io.grpc.Status;
import io.grpc.stub.StreamObserver;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import jmash.dict.protobuf.DictEntryCreateReq;
import jmash.dict.protobuf.DictEntryEnableKey;
import jmash.dict.protobuf.DictEntryKey;
import jmash.dict.protobuf.DictEntryKeyList;
import jmash.dict.protobuf.DictEntryList;
import jmash.dict.protobuf.DictEntryModel;
import jmash.dict.protobuf.DictEntryMoveKey;
import jmash.dict.protobuf.DictEntryPage;
import jmash.dict.protobuf.DictEntryReq;
import jmash.dict.protobuf.DictEntryUpdateReq;
import jmash.dict.protobuf.DictLayEntryCreateReq;
import jmash.dict.protobuf.DictLayEntryEnableKey;
import jmash.dict.protobuf.DictLayEntryKey;
import jmash.dict.protobuf.DictLayEntryKeyList;
import jmash.dict.protobuf.DictLayEntryList;
import jmash.dict.protobuf.DictLayEntryModel;
import jmash.dict.protobuf.DictLayEntryMoveKey;
import jmash.dict.protobuf.DictLayEntryReq;
import jmash.dict.protobuf.DictLayEntryUpdateReq;
import jmash.dict.protobuf.DictTypeCreateReq;
import jmash.dict.protobuf.DictTypeEnableKey;
import jmash.dict.protobuf.DictTypeExportReq;
import jmash.dict.protobuf.DictTypeImportReq;
import jmash.dict.protobuf.DictTypeKey;
import jmash.dict.protobuf.DictTypeKeyList;
import jmash.dict.protobuf.DictTypeList;
import jmash.dict.protobuf.DictTypeModel;
import jmash.dict.protobuf.DictTypeMoveKey;
import jmash.dict.protobuf.DictTypePage;
import jmash.dict.protobuf.DictTypeReq;
import jmash.dict.protobuf.DictTypeUpdateReq;
import jmash.protobuf.CustomEnumValue;
import jmash.protobuf.CustomEnumValueMap;
import jmash.protobuf.Entry;
import jmash.protobuf.EntryList;
import jmash.protobuf.EnumEntryReq;
import jmash.protobuf.EnumValueList;


@GrpcService
public class DictImpl extends jmash.dict.DictGrpc.DictImplBase {

  private static Log log = LogFactory.getLog(DictImpl.class);

  // 模块版本
  public static final String version = "v1.0.0";

  @Override
  public void version(Empty request, StreamObserver<StringValue> responseObserver) {
    responseObserver.onNext(StringValue.of(version + "-" + VersionUtil.snapshot(DictImpl.class)));
    responseObserver.onCompleted();
  }

  @Override
  public void findEnumList(StringValue request, StreamObserver<EnumValueList> responseObserver) {
    try {
      List<EnumValue> list = ProtoEnumUtil.getEnumList(request.getValue());
      responseObserver.onNext(EnumValueList.newBuilder().addAllValues(list).build());
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  public void findEnumMap(StringValue request,
      StreamObserver<CustomEnumValueMap> responseObserver) {
    try {
      Map<Integer, CustomEnumValue> values = ProtoEnumUtil.getEnumMap(request.getValue());
      responseObserver.onNext(CustomEnumValueMap.newBuilder().putAllValues(values).build());
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  public void findEnumEntry(EnumEntryReq request, StreamObserver<EntryList> responseObserver) {
    try {
      List<Entry> entryList =
          ProtoEnumUtil.getEnumCodeList(request.getClassName(), request.getType());
      responseObserver.onNext(EntryList.newBuilder().addAllValues(entryList).build());
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("dict:dict_entry:list")
  public void findDictEntryPage(DictEntryReq request,
      StreamObserver<DictEntryPage> responseObserver) {
    try (DictEntryRead dictEntryRead = DictFactory.getDictEntryRead(request.getTenant())) {
      DtoPage<DictEntryEntity, DictEntryTotal> page = dictEntryRead.findPageByReq(request);
      DictEntryPage modelPage = DictEntryMapper.INSTANCE.pageDictEntry(page);
      responseObserver.onNext(modelPage);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }


  /**
   * 开放访问的数据字典类型无需权限，授权访问的数据字典需认证权限.
   */
  @Override
  public void findDictEntryList(DictEntryReq request,
      StreamObserver<DictEntryList> responseObserver) {
    if (!PermUtils.isAuthenticated()
        && !PermUtils.checkDictTypeOpen(request.getTenant(), request.getTypeCode())) {
      responseObserver.onError(Status.PERMISSION_DENIED.withDescription("403没有访问权限").asException());
      return;
    }
    try (DictEntryRead dictEntryRead = DictFactory.getDictEntryRead(request.getTenant())) {
      List<DictEntryEntity> list = dictEntryRead.findListByReq(request);
      List<DictEntryModel> modelList = DictEntryMapper.INSTANCE.listDictEntry(list);
      responseObserver.onNext(DictEntryList.newBuilder().addAllResults(modelList).build());
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("dict:dict_entry:view")
  public void findDictEntryById(DictEntryKey request,
      StreamObserver<DictEntryModel> responseObserver) {
    try (DictEntryRead dictEntryRead = DictFactory.getDictEntryRead(request.getTenant())) {
      DictEntryPk pk = DictEntryMapper.INSTANCE.pk(request);
      DictEntryEntity entity = dictEntryRead.findById(pk);
      DictEntryModel model = DictEntryMapper.INSTANCE.model(entity);
      responseObserver.onNext(model);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }


  @Override
  @RequiresPermissions("dict:dict_entry:add")
  public void createDictEntry(DictEntryCreateReq request,
      StreamObserver<DictEntryModel> responseObserver) {
    try (DictEntryWrite dictEntryWrite = DictFactory.getDictEntryWrite(request.getTenant())) {
      DictEntryEntity entity = dictEntryWrite.insert(request);
      DictEntryModel model = DictEntryMapper.INSTANCE.model(entity);
      responseObserver.onNext(model);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("dict:dict_entry:update")
  public void updateDictEntry(DictEntryUpdateReq request,
      StreamObserver<DictEntryModel> responseObserver) {
    try (DictEntryWrite dictEntryWrite = DictFactory.getDictEntryWrite(request.getTenant())) {
      DictEntryEntity entity = dictEntryWrite.update(request);
      DictEntryModel model = DictEntryMapper.INSTANCE.model(entity);
      responseObserver.onNext(model);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("dict:dict_entry:delete")
  public void deleteDictEntry(DictEntryKey request,
      StreamObserver<DictEntryModel> responseObserver) {
    try (DictEntryWrite dictEntryWrite = DictFactory.getDictEntryWrite(request.getTenant())) {
      DictEntryPk pk = DictEntryMapper.INSTANCE.pk(request);
      DictEntryEntity entity = dictEntryWrite.delete(pk);
      DictEntryModel model = DictEntryMapper.INSTANCE.model(entity);
      responseObserver.onNext(model);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("dict:dict_entry:delete")
  public void batchDeleteDictEntry(DictEntryKeyList request,
      StreamObserver<Int32Value> responseObserver) {
    try (DictEntryWrite dictEntryWrite = DictFactory.getDictEntryWrite(request.getTenant())) {
      final List<DictEntryKey> list = request.getDictEntryKeyList();
      final Set<DictEntryPk> set =
          list.stream().map(key -> DictEntryMapper.INSTANCE.pk(key)).collect(Collectors.toSet());

      int r = dictEntryWrite.batchDelete(set);
      responseObserver.onNext(Int32Value.of(r));
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("dict:dict_entry:update")
  public void moveDictEntry(DictEntryMoveKey request, StreamObserver<BoolValue> responseObserver) {
    try (DictEntryWrite dictEntryWrite = DictFactory.getDictEntryWrite(request.getTenant())) {
      boolean value = dictEntryWrite.moveByOrderBy(request);
      responseObserver.onNext(BoolValue.of(value));
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("dict:dict_entry:enable")
  public void enableDictEntry(DictEntryEnableKey request,
      StreamObserver<BoolValue> responseObserver) {
    try (DictEntryWrite dictEntryWrite = DictFactory.getDictEntryWrite(request.getTenant())) {
      boolean value = dictEntryWrite.enableDictEntry(request);
      responseObserver.onNext(BoolValue.of(value));
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  /**
   * 开放访问的数据字典类型无需权限，授权访问的数据字典需认证权限.
   */
  @Override
  public void findDictLayEntryList(DictLayEntryReq request,
      StreamObserver<DictLayEntryList> responseObserver) {
    if (!PermUtils.isAuthenticated()
        && !PermUtils.checkDictTypeOpen(request.getTenant(), request.getTypeCode())) {
      responseObserver.onError(Status.PERMISSION_DENIED.withDescription("403没有访问权限").asException());
      return;
    }
    try (DictLayEntryRead dictLayEntryRead = DictFactory.getDictLayEntryRead(request.getTenant())) {
      List<DictLayEntryEntity> list = dictLayEntryRead.findListByReq(request);
      DictLayEntryList modelList = DictLayEntryMapper.INSTANCE.listTree(list);
      responseObserver.onNext(modelList);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("dict:dict_lay_entry:view")
  public void findDictLayEntryById(DictLayEntryKey request,
      StreamObserver<DictLayEntryModel> responseObserver) {
    try (DictLayEntryRead dictLayEntryRead = DictFactory.getDictLayEntryRead(request.getTenant())) {
      DictLayEntryEntity entity =
          dictLayEntryRead.findById(UUIDUtil.fromString(request.getDictId()));
      DictLayEntryModel model = DictLayEntryMapper.INSTANCE.model(entity);
      responseObserver.onNext(model);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }


  @Override
  @RequiresPermissions("dict:dict_lay_entry:add")
  public void createDictLayEntry(DictLayEntryCreateReq request,
      StreamObserver<DictLayEntryModel> responseObserver) {
    try (DictLayEntryWrite dictLayEntryWrite =
        DictFactory.getDictLayEntryWrite(request.getTenant())) {
      DictLayEntryEntity entity = dictLayEntryWrite.insert(request);
      DictLayEntryModel model = DictLayEntryMapper.INSTANCE.model(entity);
      responseObserver.onNext(model);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("dict:dict_lay_entry:update")
  public void updateDictLayEntry(DictLayEntryUpdateReq request,
      StreamObserver<DictLayEntryModel> responseObserver) {
    try (DictLayEntryWrite dictLayEntryWrite =
        DictFactory.getDictLayEntryWrite(request.getTenant())) {
      DictLayEntryEntity entity = dictLayEntryWrite.update(request);
      DictLayEntryModel model = DictLayEntryMapper.INSTANCE.model(entity);
      responseObserver.onNext(model);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("dict:dict_lay_entry:delete")
  public void deleteDictLayEntry(DictLayEntryKey request,
      StreamObserver<DictLayEntryModel> responseObserver) {
    try (DictLayEntryWrite dictLayEntryWrite =
        DictFactory.getDictLayEntryWrite(request.getTenant())) {
      DictLayEntryEntity entity =
          dictLayEntryWrite.delete(UUIDUtil.fromString(request.getDictId()));
      DictLayEntryModel model = DictLayEntryMapper.INSTANCE.model(entity);
      responseObserver.onNext(model);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("dict:dict_lay_entry:delete")
  public void batchDeleteDictLayEntry(DictLayEntryKeyList request,
      StreamObserver<Int32Value> responseObserver) {
    try (DictLayEntryWrite dictLayEntryWrite =
        DictFactory.getDictLayEntryWrite(request.getTenant())) {
      final Set<UUID> set = request.getDictIdList().stream()
          .map(dictId -> UUIDUtil.fromString(dictId)).collect(Collectors.toSet());

      int r = dictLayEntryWrite.batchDelete(set);
      responseObserver.onNext(Int32Value.of(r));
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("dict:dict_lay_entry:move")
  public void moveDictLayEntry(DictLayEntryMoveKey request,
      StreamObserver<BoolValue> responseObserver) {
    try (DictLayEntryWrite dictLayEntryWrite =
        DictFactory.getDictLayEntryWrite(request.getTenant())) {
      boolean value = dictLayEntryWrite.moveByOrderBy(request);
      responseObserver.onNext(BoolValue.of(value));
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("dict:dict_lay_entry:enable")
  public void enableDictLayEntry(DictLayEntryEnableKey request,
      StreamObserver<BoolValue> responseObserver) {
    try (DictLayEntryWrite dictEntryWrite = DictFactory.getDictLayEntryWrite(request.getTenant())) {
      boolean value = dictEntryWrite.enableDictEntry(request);
      responseObserver.onNext(BoolValue.of(value));
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("dict:dict_type:list")
  public void findDictTypePage(DictTypeReq request, StreamObserver<DictTypePage> responseObserver) {
    try (DictTypeRead dictTypeRead = DictFactory.getDictTypeRead(request.getTenant())) {
      DtoPage<DictTypeEntity, DictTypeTotal> page = dictTypeRead.findPageByReq(request);
      DictTypePage modelPage = DictTypeMapper.INSTANCE.pageDictType(page);
      responseObserver.onNext(modelPage);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }


  @Override
  @RequiresPermissions("dict:dict_type:list")
  public void findDictTypeList(DictTypeReq request, StreamObserver<DictTypeList> responseObserver) {
    try (DictTypeRead dictTypeRead = DictFactory.getDictTypeRead(request.getTenant())) {
      List<DictTypeEntity> list = dictTypeRead.findListByReq(request);
      List<DictTypeModel> modelList = DictTypeMapper.INSTANCE.listDictType(list);
      responseObserver.onNext(DictTypeList.newBuilder().addAllResults(modelList).build());
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("dict:dict_type:view")
  public void findDictTypeById(DictTypeKey request,
      StreamObserver<DictTypeModel> responseObserver) {
    try (DictTypeRead dictTypeRead = DictFactory.getDictTypeRead(request.getTenant())) {
      DictTypeEntity entity = dictTypeRead.findById(request.getTypeCode());
      DictTypeModel model = DictTypeMapper.INSTANCE.model(entity);
      responseObserver.onNext(model);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }


  @Override
  @RequiresPermissions("dict:dict_type:add")
  public void createDictType(DictTypeCreateReq request,
      StreamObserver<DictTypeModel> responseObserver) {
    try (DictTypeWrite dictTypeWrite = DictFactory.getDictTypeWrite(request.getTenant())) {
      DictTypeEntity entity = dictTypeWrite.insert(request);
      DictTypeModel model = DictTypeMapper.INSTANCE.model(entity);
      responseObserver.onNext(model);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("dict:dict_type:update")
  public void updateDictType(DictTypeUpdateReq request,
      StreamObserver<DictTypeModel> responseObserver) {
    try (DictTypeWrite dictTypeWrite = DictFactory.getDictTypeWrite(request.getTenant())) {
      DictTypeEntity entity = dictTypeWrite.update(request);
      DictTypeModel model = DictTypeMapper.INSTANCE.model(entity);
      responseObserver.onNext(model);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("dict:dict_type:delete")
  public void deleteDictType(DictTypeKey request, StreamObserver<DictTypeModel> responseObserver) {
    try (DictTypeWrite dictTypeWrite = DictFactory.getDictTypeWrite(request.getTenant())) {
      DictTypeEntity entity = dictTypeWrite.delete(request.getTypeCode());
      DictTypeModel model = DictTypeMapper.INSTANCE.model(entity);
      responseObserver.onNext(model);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("dict:dict_type:delete")
  public void batchDeleteDictType(DictTypeKeyList request,
      StreamObserver<Int32Value> responseObserver) {
    try (DictTypeWrite dictTypeWrite = DictFactory.getDictTypeWrite(request.getTenant())) {
      final Set<String> set = request.getTypeCodeList().stream().collect(Collectors.toSet());

      int r = dictTypeWrite.batchDelete(set);
      responseObserver.onNext(Int32Value.of(r));
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("dict:dict_type:export")
  public void downloadDictType(DictTypeExportReq request,
      StreamObserver<HttpBody> responseObserver) {
    try (DictTypeRead dictTypeRead = DictFactory.getDictTypeRead(request.getTenant())) {
      String realFileSrc = dictTypeRead.exportDictInfoTemplate(request);
      FileServiceUtil.downloadFile(responseObserver, realFileSrc);
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("dict:dict_type:import")
  public void importDictType(DictTypeImportReq request,
      StreamObserver<StringValue> responseObserver) {
    try (DictTypeWrite dictTypeWrite = DictFactory.getDictTypeWrite(request.getTenant())) {
      String r = dictTypeWrite.importDictInfo(request);
      responseObserver.onNext(StringValue.of(r));
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("dict:dict_type:export")
  public void exportDictType(DictTypeExportReq request, StreamObserver<HttpBody> responseObserver) {
    try (DictTypeRead dictTypeRead = DictFactory.getDictTypeRead(request.getTenant())) {
      String realFileSrc = dictTypeRead.exportDictInfo(request);
      FileServiceUtil.downloadFile(responseObserver, realFileSrc);
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("dict:dict_type:move")
  public void moveDictType(DictTypeMoveKey request, StreamObserver<BoolValue> responseObserver) {
    try (DictTypeWrite dictTypeWrite = DictFactory.getDictTypeWrite(request.getTenant())) {
      boolean value = dictTypeWrite.moveByOrderBy(request);
      responseObserver.onNext(BoolValue.of(value));
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("dict:dict_type:enable")
  public void enableDictType(DictTypeEnableKey request,
      StreamObserver<BoolValue> responseObserver) {
    try (DictTypeWrite dictEntryWrite = DictFactory.getDictTypeWrite(request.getTenant())) {
      boolean value = dictEntryWrite.enableDictEntry(request);
      responseObserver.onNext(BoolValue.of(value));
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

}
