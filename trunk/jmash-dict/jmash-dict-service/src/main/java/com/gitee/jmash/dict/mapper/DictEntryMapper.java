
package com.gitee.jmash.dict.mapper;

import com.crenjoy.proto.mapper.BeanMapper;
import com.crenjoy.proto.mapper.ProtoMapper;
import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.dict.entity.DictEntryEntity;
import com.gitee.jmash.dict.entity.DictEntryEntity.DictEntryPk;
import com.gitee.jmash.dict.model.DictEntryTotal;
import com.google.protobuf.FieldMask;
import java.util.List;
import java.util.UUID;
import jmash.dict.protobuf.DictEntryCreateReq;
import jmash.dict.protobuf.DictEntryKey;
import jmash.dict.protobuf.DictEntryModel;
import jmash.dict.protobuf.DictEntryPage;
import jmash.dict.protobuf.DictEntryUpdateReq;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * DictEntryMapper.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Mapper
public interface DictEntryMapper extends BeanMapper,ProtoMapper {

  DictEntryMapper INSTANCE = Mappers.getMapper(DictEntryMapper.class);
  
  FieldMask filedMask = FieldMask.newBuilder().addPaths("dictName").addPaths("enable")
      .addPaths("parentId").addPaths("description").build();
  

  List<DictEntryModel> listDictEntry(List<DictEntryEntity> list);

  DictEntryPage pageDictEntry(DtoPage<DictEntryEntity, DictEntryTotal> page);
    
  DictEntryModel model(DictEntryEntity entity);
  
  DictEntryPk pk(DictEntryKey key);  
  
  DictEntryKey key(DictEntryModel model);  
  
  DictEntryEntity create(DictEntryCreateReq req);

  DictEntryEntity clone(DictEntryEntity entity);
  
  default DictEntryCreateReq genCreate(DictEntryModel model) {
    DictEntryCreateReq.Builder build = buildCreate(model).toBuilder();
    return build.setRequestId(UUID.randomUUID().toString()).build();
  }

  default DictEntryUpdateReq genUpdate(DictEntryModel model) {
    DictEntryUpdateReq.Builder build = buildUpdate(model).toBuilder();
    build.setUpdateMask(filedMask);
    return build.setRequestId(UUID.randomUUID().toString()).build();
  }

  DictEntryCreateReq buildCreate(DictEntryModel model);

  DictEntryUpdateReq buildUpdate(DictEntryModel model);
  
}
