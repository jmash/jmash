
package com.gitee.jmash.dict.mapper;

import com.crenjoy.proto.mapper.BeanMapper;
import com.crenjoy.proto.mapper.ProtoMapper;
import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.utils.GrpcTreeUtil;
import com.gitee.jmash.dict.entity.DictLayEntryEntity;
import com.gitee.jmash.dict.model.DictLayEntryTotal;
import com.google.protobuf.FieldMask;
import java.util.List;
import java.util.UUID;
import jmash.dict.protobuf.DictLayEntryCreateReq;
import jmash.dict.protobuf.DictLayEntryList;
import jmash.dict.protobuf.DictLayEntryModel;
import jmash.dict.protobuf.DictLayEntryPage;
import jmash.dict.protobuf.DictLayEntryUpdateReq;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * DictLayEntryMapper.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Mapper
public interface DictLayEntryMapper extends BeanMapper, ProtoMapper {

  DictLayEntryMapper INSTANCE = Mappers.getMapper(DictLayEntryMapper.class);

  FieldMask filedMask = FieldMask.newBuilder().addPaths("dictName").addPaths("enable")
      .addPaths("description").build();

  default DictLayEntryList listTree(List<DictLayEntryEntity> list) {
    List<DictLayEntryModel> modelList = listDictLayEntry(list);
    // 構建Tree.
    List<DictLayEntryModel> builderList = GrpcTreeUtil.buildTree(modelList,
        DictLayEntryModel::getDictId, DictLayEntryModel::getParentId, DictLayEntryModel::toBuilder,
        DictLayEntryModel.Builder::build,
        (parentBuilder, child) -> parentBuilder.addChildren(0, child));

    return DictLayEntryList.newBuilder().addAllResults(builderList).build();
  }

  List<DictLayEntryModel> listDictLayEntry(List<DictLayEntryEntity> list);

  DictLayEntryPage pageDictLayEntry(DtoPage<DictLayEntryEntity, DictLayEntryTotal> page);

  DictLayEntryModel model(DictLayEntryEntity entity);

  DictLayEntryEntity create(DictLayEntryCreateReq req);

  DictLayEntryEntity clone(DictLayEntryEntity entity);

  default DictLayEntryCreateReq genCreate(DictLayEntryModel model) {
    DictLayEntryCreateReq.Builder build = buildCreate(model).toBuilder();
    return build.setRequestId(UUID.randomUUID().toString()).build();
  }

  default DictLayEntryUpdateReq genUpdate(DictLayEntryModel model) {
    DictLayEntryUpdateReq.Builder build = buildUpdate(model).toBuilder();
    build.setUpdateMask(filedMask);
    return build.setRequestId(UUID.randomUUID().toString()).build();
  }

  DictLayEntryCreateReq buildCreate(DictLayEntryModel model);

  DictLayEntryUpdateReq buildUpdate(DictLayEntryModel model);

}
