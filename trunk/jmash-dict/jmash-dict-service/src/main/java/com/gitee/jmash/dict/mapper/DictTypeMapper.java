
package com.gitee.jmash.dict.mapper;

import com.crenjoy.proto.mapper.BeanMapper;
import com.crenjoy.proto.mapper.ProtoMapper;
import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.dict.entity.DictTypeEntity;
import com.gitee.jmash.dict.model.DictTypeTotal;
import com.google.protobuf.FieldMask;
import java.util.List;
import java.util.UUID;
import jmash.dict.protobuf.DictTypeCreateReq;
import jmash.dict.protobuf.DictTypeModel;
import jmash.dict.protobuf.DictTypePage;
import jmash.dict.protobuf.DictTypeUpdateReq;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * DictTypeMapper.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Mapper
public interface DictTypeMapper extends BeanMapper, ProtoMapper {

  DictTypeMapper INSTANCE = Mappers.getMapper(DictTypeMapper.class);

  FieldMask filedMask = FieldMask.newBuilder().addPaths("typeName").addPaths("enable")
      .addPaths("description").build();

  List<DictTypeModel> listDictType(List<DictTypeEntity> list);

  DictTypePage pageDictType(DtoPage<DictTypeEntity, DictTypeTotal> page);

  DictTypeModel model(DictTypeEntity entity);

  DictTypeEntity create(DictTypeCreateReq req);

  DictTypeEntity clone(DictTypeEntity entity);

  default DictTypeCreateReq genCreate(DictTypeModel model) {
    DictTypeCreateReq.Builder build = buildCreate(model).toBuilder();
    return build.setRequestId(UUID.randomUUID().toString()).build();
  }

  default DictTypeUpdateReq genUpdate(DictTypeModel model) {
    DictTypeUpdateReq.Builder build = buildUpdate(model).toBuilder();
    build.setUpdateMask(filedMask);
    return build.setRequestId(UUID.randomUUID().toString()).build();
  }

  DictTypeCreateReq buildCreate(DictTypeModel model);

  DictTypeUpdateReq buildUpdate(DictTypeModel model);

}
