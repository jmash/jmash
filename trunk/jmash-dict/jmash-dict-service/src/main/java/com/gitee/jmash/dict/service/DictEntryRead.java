package com.gitee.jmash.dict.service;

import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.dict.entity.DictEntryEntity;
import com.gitee.jmash.dict.entity.DictEntryEntity.DictEntryPk;
import com.gitee.jmash.dict.model.DictEntryTotal;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.util.List;
import jmash.dict.protobuf.DictEntryReq;

 /**
 * 数据字典 os_dict_entry服务Read接口.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public interface DictEntryRead extends TenantService {

  /** 根据主键查询. */
  public DictEntryEntity findById(@NotNull DictEntryPk entityId);

  /** 查询页信息. */
  public DtoPage<DictEntryEntity,DictEntryTotal> findPageByReq(@NotNull @Valid DictEntryReq req);

  /** 综合查询. */
  public List<DictEntryEntity> findListByReq(@NotNull @Valid DictEntryReq req);
  
}
