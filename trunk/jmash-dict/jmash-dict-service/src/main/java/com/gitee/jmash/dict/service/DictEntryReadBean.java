
package com.gitee.jmash.dict.service;

import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.orm.cdi.JpaTenantService;
import com.gitee.jmash.core.orm.jpa.TenantEntityManager;
import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.core.transaction.JakartaTransaction;
import com.gitee.jmash.dict.dao.DictEntryDao;
import com.gitee.jmash.dict.entity.DictEntryEntity;
import com.gitee.jmash.dict.entity.DictEntryEntity.DictEntryPk;
import com.gitee.jmash.dict.model.DictEntryTotal;
import jakarta.enterprise.inject.Typed;
import jakarta.enterprise.inject.spi.CDI;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import jakarta.transaction.Transactional.TxType;
import jakarta.validation.executable.ValidateOnExecution;
import java.util.List;
import jmash.dict.protobuf.DictEntryReq;

/**
 * 数据字典 os_dict_entry读服务.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Typed(DictEntryRead.class)
@Transactional(TxType.SUPPORTS)
@JpaTenantService
@ValidateOnExecution
public class DictEntryReadBean implements DictEntryRead, JakartaTransaction {

  protected TenantEntityManager tem = new TenantEntityManager();

  protected DictEntryDao dictEntryDao = new DictEntryDao(tem);
  
  @PersistenceContext(unitName = "ReadDict")
  public void setEntityManager(EntityManager entityManager) {
    this.tem.setEntityManager(entityManager,false);
  }
	
  @Override
  public EntityManager getEntityManager() {
    return this.tem.getEntityManager();
  }

  @Override
  @SuppressWarnings("unchecked")
  public <T extends TenantService> T setTenant(String tenant) {
    this.tem.setTenant(tenant);
    return (T) this;
  }
  
  @Override
  public void setTenantOnly(String tenant) {
     this.tem.setTenantOnly(tenant);
  }

  @Override
  public String getTenant() {
    return this.tem.getTenant();
  }

  @Override
  public DictEntryEntity findById(DictEntryPk entityId) {
    return dictEntryDao.find(entityId);
  }


  @Override
  public DtoPage<DictEntryEntity,DictEntryTotal> findPageByReq(DictEntryReq req) {
    return dictEntryDao.findPageByReq(req);
  }

  @Override
  public List<DictEntryEntity> findListByReq(DictEntryReq req) {
    return dictEntryDao.findListByReq(req);
  }
  
  @Override
  public void close() throws Exception {
    CDI.current().destroy(this);
  }
}
