package com.gitee.jmash.dict.service;

import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.dict.entity.DictEntryEntity;
import com.gitee.jmash.dict.entity.DictEntryEntity.DictEntryPk;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.util.Set;
import jmash.dict.protobuf.DictEntryCreateReq;
import jmash.dict.protobuf.DictEntryEnableKey;
import jmash.dict.protobuf.DictEntryModel;
import jmash.dict.protobuf.DictEntryMoveKey;
import jmash.dict.protobuf.DictEntryUpdateReq;

 /**
 * 数据字典 os_dict_entry服务Write接口.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public interface DictEntryWrite extends TenantService {
		
  /** 插入实体. */
  public DictEntryEntity insert(@NotNull @Valid DictEntryCreateReq  dictEntry);

  /** update 实体. */
  public DictEntryEntity update(@NotNull @Valid DictEntryUpdateReq  dictEntry);
  
  /** Save实体. */
  public DictEntryEntity save(@NotNull DictEntryModel dictModel);

  /** 根据主键删除. */
  public DictEntryEntity delete(@NotNull DictEntryPk entityId);

  /** 根据主键数组删除. */
  public Integer batchDelete(@NotNull @Valid Set< @NotNull DictEntryPk > entityIds);
  /** 上移、下移 */
  public boolean moveByOrderBy(@NotNull DictEntryMoveKey dictEntryMoveKey);
  /** 启用/禁用 */
  public boolean enableDictEntry(@NotNull DictEntryEnableKey dictEntryEnableKey);

}
