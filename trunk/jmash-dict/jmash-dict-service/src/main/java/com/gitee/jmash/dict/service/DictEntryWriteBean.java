
package com.gitee.jmash.dict.service;

import com.gitee.jmash.common.lock.DistributedLock;
import com.gitee.jmash.core.jaxrs.ParamsValidationException;
import com.gitee.jmash.core.orm.cdi.JpaTenantService;
import com.gitee.jmash.core.transaction.JakartaTransaction;
import com.gitee.jmash.core.utils.FieldMaskUtil;
import com.gitee.jmash.dict.entity.DictEntryEntity;
import com.gitee.jmash.dict.entity.DictEntryEntity.DictEntryPk;
import com.gitee.jmash.dict.mapper.DictEntryMapper;
import jakarta.enterprise.inject.Typed;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import jakarta.transaction.Transactional.TxType;
import jakarta.validation.ValidationException;
import jakarta.validation.executable.ValidateOnExecution;
import java.util.List;
import java.util.Set;
import jmash.dict.protobuf.DictEntryCreateReq;
import jmash.dict.protobuf.DictEntryEnableKey;
import jmash.dict.protobuf.DictEntryModel;
import jmash.dict.protobuf.DictEntryMoveKey;
import jmash.dict.protobuf.DictEntryUpdateReq;

/**
 * 数据字典 os_dict_entry写服务.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Typed(DictEntryWrite.class)
@Transactional(TxType.REQUIRED)
@JpaTenantService
@ValidateOnExecution
public class DictEntryWriteBean extends DictEntryReadBean
    implements DictEntryWrite, JakartaTransaction {

  @Inject
  DistributedLock lock;

  @Override
  @PersistenceContext(unitName = "WriteDict")
  public void setEntityManager(EntityManager entityManager) {
    this.tem.setEntityManager(entityManager, true);
  }

  @Override
  public DictEntryEntity insert(DictEntryCreateReq dictEntry) {
    DictEntryEntity entity = DictEntryMapper.INSTANCE.create(dictEntry);
    // 1.业务校验.
    // 2.仅校验,不执行.
    if (dictEntry.getValidateOnly()) {
      return entity;
    }
    // 3.检查是否重复请求.
    if (!lock.lock(dictEntry.getRequestId(), 60)) {
      throw new ParamsValidationException("requestId", "客户端发起重复请求");
    }
    // 4.执行业务(创建人及时间内部处理.)
    entity.setListOrder(dictEntryDao.getMaxListOrder(dictEntry.getTypeCode()) + 1);
    dictEntryDao.persist(entity);
    return entity;
  }

  @Override
  public DictEntryEntity update(DictEntryUpdateReq req) {
    DictEntryPk pk = new DictEntryPk(req.getTypeCode(), req.getDictCode());
    DictEntryEntity entity = dictEntryDao.find(pk, req.getValidateOnly());
    if (null == entity) {
      throw new ValidationException("找不到实体:" + pk);
    }
    // 无需更新,返回当前数据库数据.
    if (req.getUpdateMask().getPathsCount() == 0) {
      return entity;
    }
    // 更新掩码属性
    FieldMaskUtil.copyMask(entity, req, req.getUpdateMask());
    // 1.业务校验.
    // 2.仅校验,不执行.
    if (req.getValidateOnly()) {
      return entity;
    }

    // 3.检查是否重复请求.
    if (!lock.lock(req.getRequestId(), 60)) {
      throw new ParamsValidationException("requestId", "客户端发起重复请求");
    }

    // 4.执行业务
    dictEntryDao.merge(entity);
    return entity;
  }

  @Override
  public DictEntryEntity save(DictEntryModel dictModel) {
    DictEntryPk pk = new DictEntryPk(dictModel.getTypeCode(), dictModel.getDictCode());
    DictEntryEntity entity = this.dictEntryDao.find(pk);
    if (entity == null) {
      return insert(DictEntryMapper.INSTANCE.genCreate(dictModel));
    } else {
      return update(DictEntryMapper.INSTANCE.genUpdate(dictModel));
    }
  }

  @Override
  public DictEntryEntity delete(DictEntryPk entityId) {
    DictEntryEntity entity = dictEntryDao.removeById(entityId);
    return entity;
  }

  @Override
  public Integer batchDelete(Set<DictEntryPk> entityIds) {
    int i = 0;
    for (DictEntryPk entityId : entityIds) {
      dictEntryDao.removeById(entityId);
      i++;
    }
    return i;
  }

  @Override
  public boolean moveByOrderBy(DictEntryMoveKey dictEntryMoveKey) {
    DictEntryPk pk =
        new DictEntryPk(dictEntryMoveKey.getTypeCode(), dictEntryMoveKey.getDictCode());
    DictEntryEntity dictEntry = dictEntryDao.find(pk);
    if (dictEntry == null) {
      throw new ValidationException("无法找到相应实体");
    }
    boolean moveUp = dictEntryMoveKey.getUp();
    List<DictEntryEntity> entityList =
        dictEntryDao.selectListByOrder(dictEntry.getListOrder(), dictEntryMoveKey);
    if (entityList.size() < 2) {
      String msg = "";
      if (moveUp) {
        msg = "当前已经是第一条，无法向上移动！";
      } else {
        msg = "当前已经是最后一条，无法向下移动！";
      }
      throw new UnsupportedOperationException(msg);
    } else {
      int orderBy1 = entityList.get(1).getListOrder();
      int orderBy2 = entityList.get(0).getListOrder();

      if (orderBy1 == orderBy2 && moveUp) {
        orderBy1 = orderBy2 + 1;
      }
      if (orderBy1 == orderBy2 && !moveUp) {
        orderBy2 = orderBy1 + 1;
      }

      DictEntryEntity entryEntity1 = entityList.get(0);
      entryEntity1.setListOrder(orderBy1);
      dictEntryDao.merge(entryEntity1);

      DictEntryEntity entryEntity2 = entityList.get(1);
      entryEntity2.setListOrder(orderBy2);
      dictEntryDao.merge(entryEntity2);
    }
    return true;
  }

  @Override
  public boolean enableDictEntry(DictEntryEnableKey dictEntryEnableKey) {
    DictEntryPk pk =
        new DictEntryPk(dictEntryEnableKey.getTypeCode(), dictEntryEnableKey.getDictCode());
    DictEntryEntity entity = dictEntryDao.find(pk);
    if (entity == null) {
      throw new ValidationException("无法找到对应实体数据！");
    }
    entity.setEnable(dictEntryEnableKey.getEnable());
    dictEntryDao.merge(entity);
    return true;
  }
}
