package com.gitee.jmash.dict.service;

import com.gitee.jmash.common.tree.Tree;
import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.dict.entity.DictLayEntryEntity;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;
import jmash.dict.protobuf.DictLayEntryReq;

/**
 * 层级字典实体 os_dict_lay_entry服务Read接口.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public interface DictLayEntryRead extends TenantService {

  /** 根据主键查询. */
  public DictLayEntryEntity findById(@NotNull UUID entityId);

  /** 综合查询. */
  public List<DictLayEntryEntity> findListByReq(@NotNull @Valid DictLayEntryReq req);

  /** 查询树状接口数据 */
  public Tree<DictLayEntryEntity, UUID> findTree(String typeCode);

}
