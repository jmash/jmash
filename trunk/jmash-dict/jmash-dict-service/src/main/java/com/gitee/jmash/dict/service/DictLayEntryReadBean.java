
package com.gitee.jmash.dict.service;

import com.gitee.jmash.common.tree.Tree;
import com.gitee.jmash.common.utils.UUIDUtil;
import com.gitee.jmash.core.orm.cdi.JpaTenantService;
import com.gitee.jmash.core.orm.jpa.TenantEntityManager;
import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.core.transaction.JakartaTransaction;
import com.gitee.jmash.dict.dao.DictLayEntryDao;
import com.gitee.jmash.dict.entity.DictLayEntryEntity;
import jakarta.enterprise.inject.Typed;
import jakarta.enterprise.inject.spi.CDI;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import jakarta.transaction.Transactional.TxType;
import jakarta.validation.executable.ValidateOnExecution;
import java.util.List;
import java.util.UUID;
import jmash.dict.protobuf.DictLayEntryReq;
import org.apache.commons.lang3.StringUtils;

/**
 * 层级字典实体 os_dict_lay_entry读服务.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Typed(DictLayEntryRead.class)
@Transactional(TxType.SUPPORTS)
@JpaTenantService
@ValidateOnExecution
public class DictLayEntryReadBean implements DictLayEntryRead, JakartaTransaction {

  protected TenantEntityManager tem = new TenantEntityManager();

  protected DictLayEntryDao dictLayEntryDao = new DictLayEntryDao(tem);
  
  @PersistenceContext(unitName = "ReadDict")
  public void setEntityManager(EntityManager entityManager) {
    this.tem.setEntityManager(entityManager, false);
  }

  @Override
  public EntityManager getEntityManager() {
    return this.tem.getEntityManager();
  }

  @Override
  @SuppressWarnings("unchecked")
  public <T extends TenantService> T setTenant(String tenant) {
    this.tem.setTenant(tenant);
    return (T) this;
  }
  
  @Override
  public void setTenantOnly(String tenant) {
     this.tem.setTenantOnly(tenant);
  }

  @Override
  public String getTenant() {
    return this.tem.getTenant();
  }

  @Override
  public DictLayEntryEntity findById(UUID entityId) {
    return dictLayEntryDao.find(entityId);
  }

  @Override
  public List<DictLayEntryEntity> findListByReq(DictLayEntryReq req) {
    List<DictLayEntryEntity> dictLayEntryEntityList = dictLayEntryDao.findListByReq(req);
    Tree<DictLayEntryEntity, UUID> tree = findListTree(dictLayEntryEntityList);
    if (StringUtils.isNotBlank(req.getExcludeId())) {
      return tree.getListExclude(UUIDUtil.fromString(req.getExcludeId()));
    }
    return tree.getAllList();
  }

  @Override
  public void close() throws Exception {
    CDI.current().destroy(this);
  }

  @Override
  public Tree<DictLayEntryEntity, UUID> findTree(String typeCode) {
    // 得到所有节点list信息然后放到内存树中
    Tree<DictLayEntryEntity, UUID> tree = new Tree<>();
    List<DictLayEntryEntity> dictLayEntryEntityList =
        dictLayEntryDao.findListByReq(DictLayEntryReq.newBuilder().setTypeCode(typeCode).build());
    if (dictLayEntryEntityList.size() == 0) {
      return tree;
    }
    for (DictLayEntryEntity dictLayEntryEntity : dictLayEntryEntityList) {
      tree.add(dictLayEntryEntity.getDictId(), dictLayEntryEntity, dictLayEntryEntity.getParentId(),
          dictLayEntryEntity.getListOrder());
    }
    return tree;
  }

  public Tree<DictLayEntryEntity, UUID> findListTree(List<DictLayEntryEntity> layEntryEntityList) {
    // 得到所有节点list信息然后放到内存树中
    Tree<DictLayEntryEntity, UUID> tree = new Tree<>();
    if (layEntryEntityList.size() == 0) {
      return tree;
    }
    for (DictLayEntryEntity dictLayEntryEntity : layEntryEntityList) {
      tree.add(dictLayEntryEntity.getDictId(), dictLayEntryEntity, dictLayEntryEntity.getParentId(),
          dictLayEntryEntity.getListOrder());
    }
    return tree;
  }
}
