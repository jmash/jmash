package com.gitee.jmash.dict.service;

import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.dict.entity.DictLayEntryEntity;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.util.Set;
import java.util.UUID;
import jmash.dict.protobuf.DictLayEntryCreateReq;
import jmash.dict.protobuf.DictLayEntryEnableKey;
import jmash.dict.protobuf.DictLayEntryModel;
import jmash.dict.protobuf.DictLayEntryMoveKey;
import jmash.dict.protobuf.DictLayEntryUpdateReq;

/**
 * 层级字典实体 os_dict_lay_entry服务Write接口.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public interface DictLayEntryWrite extends TenantService {

  /** 插入实体. */
  public DictLayEntryEntity insert(@NotNull @Valid DictLayEntryCreateReq dictLayEntry);

  /** update 实体. */
  public DictLayEntryEntity update(@NotNull @Valid DictLayEntryUpdateReq dictLayEntry);

  /** Save实体. */
  public DictLayEntryEntity save(@NotNull DictLayEntryModel dictModel);

  /** 根据主键删除. */
  public DictLayEntryEntity delete(@NotNull UUID entityId);

  /** 根据主键数组删除. */
  public Integer batchDelete(@NotNull @Valid Set<@NotNull UUID> entityIds);

  /** 上移、下移 */
  public boolean moveByOrderBy(@NotNull DictLayEntryMoveKey dictLayEntryMoveKey);

  /** 启用/禁用 */
  public boolean enableDictEntry(@NotNull DictLayEntryEnableKey dictLayEntryEnableKey);

}
