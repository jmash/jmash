
package com.gitee.jmash.dict.service;

import com.gitee.jmash.common.lock.DistributedLock;
import com.gitee.jmash.common.tree.Node;
import com.gitee.jmash.common.tree.Tree;
import com.gitee.jmash.common.utils.UUIDUtil;
import com.gitee.jmash.core.jaxrs.ParamsValidationException;
import com.gitee.jmash.core.orm.cdi.JpaTenantService;
import com.gitee.jmash.core.transaction.JakartaTransaction;
import com.gitee.jmash.core.utils.FieldMaskUtil;
import com.gitee.jmash.dict.entity.DictLayEntryEntity;
import com.gitee.jmash.dict.mapper.DictLayEntryMapper;
import jakarta.enterprise.inject.Typed;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import jakarta.transaction.Transactional.TxType;
import jakarta.validation.ValidationException;
import jakarta.validation.executable.ValidateOnExecution;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import jmash.dict.protobuf.DictLayEntryCreateReq;
import jmash.dict.protobuf.DictLayEntryEnableKey;
import jmash.dict.protobuf.DictLayEntryModel;
import jmash.dict.protobuf.DictLayEntryMoveKey;
import jmash.dict.protobuf.DictLayEntryUpdateReq;
import org.apache.commons.lang3.StringUtils;

/**
 * 层级字典实体 os_dict_lay_entry写服务.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Typed(DictLayEntryWrite.class)
@Transactional(TxType.REQUIRED)
@JpaTenantService
@ValidateOnExecution
public class DictLayEntryWriteBean extends DictLayEntryReadBean
    implements DictLayEntryWrite, JakartaTransaction {

  @Inject
  DistributedLock lock;

  @Override
  @PersistenceContext(unitName = "WriteDict")
  public void setEntityManager(EntityManager entityManager) {
    this.tem.setEntityManager(entityManager, true);
  }

  @Override
  public DictLayEntryEntity insert(DictLayEntryCreateReq dictLayEntry) {
    DictLayEntryEntity entity = DictLayEntryMapper.INSTANCE.create(dictLayEntry);
    // 1.业务校验.
    // 2.仅校验,不执行.
    if (dictLayEntry.getValidateOnly()) {
      return entity;
    }
    // 3.检查是否重复请求.
    if (!lock.lock(dictLayEntry.getRequestId(), 60)) {
      throw new ParamsValidationException("requestId", "客户端发起重复请求");
    }
    // 4.执行业务(创建人及时间内部处理.)
    UUID parentId = UUIDUtil.fromString(dictLayEntry.getParentId());
    if (!UUIDUtil.equalsEmpty(parentId)) {
      entity.setParentId(parentId);
      DictLayEntryEntity parent = this.dictLayEntryDao.find(parentId);
      entity.setDepth(parent.getDepth() + 1);
    } else {
      entity.setParentId(UUIDUtil.emptyUUID());
      entity.setDepth(1);
    }
    int listOrder = dictLayEntryDao.maxLayOrderByParentId(entity.getParentId()) + 1;
    entity.setListOrder(listOrder);
    dictLayEntryDao.persist(entity);
    return entity;
  }

  @Override
  public DictLayEntryEntity update(DictLayEntryUpdateReq req) {
    DictLayEntryEntity entity =
        dictLayEntryDao.find(UUIDUtil.fromString(req.getDictId()), req.getValidateOnly());
    if (null == entity) {
      throw new ValidationException("找不到实体:" + req.getDictId());
    }
    // 无需更新,返回当前数据库数据.
    if (req.getUpdateMask().getPathsCount() == 0) {
      return entity;
    }
    // 更新掩码属性
    if (StringUtils.isBlank(req.getParentId())) {
      req=req.toBuilder().setParentId(UUIDUtil.emptyUUIDStr()).build();
    }
    FieldMaskUtil.copyMask(entity, req, req.getUpdateMask());
    // 1.业务校验.
    // 2.仅校验,不执行.
    if (req.getValidateOnly()) {
      return entity;
    }
    // 3.检查是否重复请求.
    if (!lock.lock(req.getRequestId(), 60)) {
      throw new ParamsValidationException("requestId", "客户端发起重复请求");
    }

    // 4.执行业务
    Tree<DictLayEntryEntity, UUID> tree = findTree(entity.getTypeCode());
    tree.changeNodeParent(UUIDUtil.fromString(req.getDictId()), entity.getParentId());
    // 保存变化后的结果
    for (DictLayEntryEntity dictLayEntryEntity : tree.getAllList()) {
      Node<DictLayEntryEntity, UUID> node = tree.getNode(dictLayEntryEntity.getDictId());
      dictLayEntryEntity.setParentId(node.getParentKey());
      dictLayEntryEntity.setDepth(node.getDepth());
      this.dictLayEntryDao.merge(dictLayEntryEntity);
    }
    dictLayEntryDao.merge(entity);
    return entity;
  }

  @Override
  public DictLayEntryEntity save(DictLayEntryModel dictModel) {
    UUID dictId = UUIDUtil.fromString(dictModel.getDictId());
    DictLayEntryEntity entity = this.findById(dictId);
    if (entity == null) {
      return insert(DictLayEntryMapper.INSTANCE.genCreate(dictModel));
    } else {
      return update(DictLayEntryMapper.INSTANCE.genUpdate(dictModel));
    }
  }

  @Override
  public DictLayEntryEntity delete(UUID entityId) {
    DictLayEntryEntity dictLayEntryEntity = dictLayEntryDao.find(entityId);
    if (dictLayEntryEntity == null) {
      return null;
    }
    Tree<DictLayEntryEntity, UUID> tree = findTree(dictLayEntryEntity.getTypeCode());
    List<DictLayEntryEntity> list = tree.getAllChildsList(entityId);
    tree.removeNode(entityId);
    for (DictLayEntryEntity entity : list) {
      Node<DictLayEntryEntity, UUID> node = tree.getNode(entity.getDictId());
      entity.setParentId(node.getParentKey());
      entity.setDepth(node.getDepth());
      this.dictLayEntryDao.merge(entity);
    }
    this.dictLayEntryDao.remove(dictLayEntryEntity);
    return dictLayEntryEntity;
  }

  @Override
  public Integer batchDelete(Set<UUID> entityIds) {
    int i = 0;
    for (UUID entityId : entityIds) {
      delete(entityId);
      i++;
    }
    return i;
  }

  @Override
  public boolean moveByOrderBy(DictLayEntryMoveKey moveKey) {
    UUID dictId = UUIDUtil.fromString(moveKey.getDictId());
    DictLayEntryEntity dictEntry = dictLayEntryDao.find(dictId);
    if (dictEntry == null) {
      throw new ValidationException("无法找到相应实体");
    }

    Tree<DictLayEntryEntity, UUID> tree = findTree(dictEntry.getTypeCode());
    List<DictLayEntryEntity> dictLayEntryEntityList = tree.moveOrder(dictId, moveKey.getUp());
    int i = 1;
    for (DictLayEntryEntity dictLayEntryEntity : dictLayEntryEntityList) {
      if (!dictLayEntryEntity.getListOrder().equals(i)) {
        dictLayEntryEntity.setListOrder(i);
        this.dictLayEntryDao.merge(dictLayEntryEntity);
      }
      i++;
    }
    return true;
  }

  @Override
  public boolean enableDictEntry(DictLayEntryEnableKey dictLayEntryEnableKey) {
    DictLayEntryEntity entity =
        dictLayEntryDao.find(UUIDUtil.fromString(dictLayEntryEnableKey.getDictId()));
    if (entity == null) {
      throw new ValidationException("无法找到对应实体数据！");
    }
    entity.setEnable(dictLayEntryEnableKey.getEnable());
    dictLayEntryDao.merge(entity);
    return true;
  }
}
