package com.gitee.jmash.dict.service;

import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.dict.entity.DictTypeEntity;
import com.gitee.jmash.dict.model.DictTypeTotal;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.io.IOException;
import java.util.List;
import jmash.dict.protobuf.DictTypeExportReq;
import jmash.dict.protobuf.DictTypeReq;

 /**
 * 字典类型 os_dict_type服务Read接口.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public interface DictTypeRead extends TenantService {

  /** 根据主键查询. */
  public DictTypeEntity findById(@NotNull String entityId);

  /** 查询页信息. */
  public DtoPage<DictTypeEntity,DictTypeTotal> findPageByReq(@NotNull @Valid DictTypeReq req);

  /** 综合查询. */
  public List<DictTypeEntity> findListByReq(@NotNull @Valid DictTypeReq req);

  public String exportDictInfo(@NotNull DictTypeExportReq req) throws IOException;

  public String exportDictInfoTemplate(@NotNull DictTypeExportReq req) throws IOException;
  
}
