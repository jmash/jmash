
package com.gitee.jmash.dict.service;

import com.gitee.jmash.common.excel.ExcelExport;
import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.orm.cdi.JpaTenantService;
import com.gitee.jmash.core.orm.jpa.TenantEntityManager;
import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.core.transaction.JakartaTransaction;
import com.gitee.jmash.core.utils.FilePathUtil;
import com.gitee.jmash.dict.dao.DictEntryDao;
import com.gitee.jmash.dict.dao.DictLayEntryDao;
import com.gitee.jmash.dict.dao.DictTypeDao;
import com.gitee.jmash.dict.entity.DictEntryEntity;
import com.gitee.jmash.dict.entity.DictLayEntryEntity;
import com.gitee.jmash.dict.entity.DictTypeEntity;
import com.gitee.jmash.dict.enums.TableName;
import com.gitee.jmash.dict.excel.DictSheetExportDto;
import com.gitee.jmash.dict.model.DictTypeTotal;
import jakarta.enterprise.inject.Typed;
import jakarta.enterprise.inject.spi.CDI;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import jakarta.transaction.Transactional.TxType;
import jakarta.validation.executable.ValidateOnExecution;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import jmash.dict.protobuf.DictEntryReq;
import jmash.dict.protobuf.DictLayEntryReq;
import jmash.dict.protobuf.DictTypeExportReq;
import jmash.dict.protobuf.DictTypeReq;
import jmash.dict.protobuf.EntryTypes;

/**
 * 字典类型 os_dict_type读服务.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Typed(DictTypeRead.class)
@Transactional(TxType.SUPPORTS)
@JpaTenantService
@ValidateOnExecution
public class DictTypeReadBean implements DictTypeRead, JakartaTransaction {

  protected TenantEntityManager tem = new TenantEntityManager();

  protected DictTypeDao dictTypeDao = new DictTypeDao(tem);
  protected DictLayEntryDao dictLayEntryDao = new DictLayEntryDao(tem);
  protected DictEntryDao dictEntryDao = new DictEntryDao(tem);
  
  @PersistenceContext(unitName = "ReadDict")
  public void setEntityManager(EntityManager entityManager) {
    this.tem.setEntityManager(entityManager, false);
  }

  @Override
  public EntityManager getEntityManager() {
    return this.tem.getEntityManager();
  }

  @Override
  @SuppressWarnings("unchecked")
  public <T extends TenantService> T setTenant(String tenant) {
    this.tem.setTenant(tenant);
    return (T) this;
  }

  @Override
  public void setTenantOnly(String tenant) {
     this.tem.setTenantOnly(tenant);
  }
  
  @Override
  public String getTenant() {
    return this.tem.getTenant();
  }

  @Override
  public DictTypeEntity findById(String entityId) {
    return dictTypeDao.find(entityId);
  }


  @Override
  public DtoPage<DictTypeEntity, DictTypeTotal> findPageByReq(DictTypeReq req) {
    return dictTypeDao.findPageByReq(req);
  }

  @Override
  public List<DictTypeEntity> findListByReq(DictTypeReq req) {
    return dictTypeDao.findListByReq(req);
  }

  @Override
  public void close() throws Exception {
    CDI.current().destroy(this);
  }

  @Override
  public String exportDictInfo(DictTypeExportReq req) throws IOException {
    List<DictSheetExportDto> dictSheetList = new ArrayList<>();
    List<DictTypeEntity> dictTypeEntityList = dictTypeDao.findListByReq(req.getReq());
    dictSheetList.add(new DictSheetExportDto("字典类型", dictTypeEntityList, TableName.Type));
    int i = 0;
    for (DictTypeEntity typeEntity : dictTypeEntityList) {
      i++;
      if (EntryTypes.normal.equals(typeEntity.getEntryType())) {
        List<DictEntryEntity> dictEntryEntityList = dictEntryDao
            .findListByReq(DictEntryReq.newBuilder().setTypeCode(typeEntity.getTypeCode()).build());
        dictSheetList.add(
            new DictSheetExportDto("普通"+i+"-"+typeEntity.getTypeName(), dictEntryEntityList, TableName.Entry));
      } else if (EntryTypes.layout.equals(typeEntity.getEntryType())) {
        List<DictLayEntryEntity> dictLayEntryEntityList = dictLayEntryDao.findListByReq(
            DictLayEntryReq.newBuilder().setTypeCode(typeEntity.getTypeCode()).build());
        dictSheetList.add(new DictSheetExportDto("层级"+i+"-"+typeEntity.getTypeName(), dictLayEntryEntityList,
            TableName.LayEntry));
      }
    }
    return exportDictInfo(dictSheetList);
  }

  public String exportDictInfo(List<DictSheetExportDto> dictSheetExportDTOList) throws IOException {
    ExcelExport export = new ExcelExport();
    for (DictSheetExportDto dictSheet : dictSheetExportDTOList) {
      export.createSheet(dictSheet.getSheetName());
      export.addHeaderAll(dictSheet.getHeaderCollection());
      export.writeHeader();
      export.writeListData(dictSheet.getDataList());
      // 增加数据校验
      export.dictDataValidation(dictSheet.getDataList().size(), 3000);
    }
    String fileSrc = FilePathUtil.createNewTempFile("数据字典详表" + ".xlsx", "excel");
    String realFileSrc = FilePathUtil.realPath(fileSrc,true);
    export.getWb().write(new FileOutputStream(realFileSrc));
    return realFileSrc;
  }

  @Override
  public String exportDictInfoTemplate(DictTypeExportReq req) throws IOException {
    List<DictSheetExportDto> dictSheetList = new ArrayList<>();
    List<DictTypeEntity> dictTypeEntityList = new ArrayList<>();
    dictSheetList.add(new DictSheetExportDto("字典类型", dictTypeEntityList, TableName.Type));
    List<DictEntryEntity> dictEntryEntityList = new ArrayList<>();
    dictSheetList.add(new DictSheetExportDto("普通字典", dictEntryEntityList, TableName.Entry));
    List<DictLayEntryEntity> dictLayEntryEntityList = new ArrayList<>();
    dictSheetList.add(new DictSheetExportDto("层级字典", dictLayEntryEntityList, TableName.LayEntry));
    return exportDictInfo(dictSheetList);
  }
}
