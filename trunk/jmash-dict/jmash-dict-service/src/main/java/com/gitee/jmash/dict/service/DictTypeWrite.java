package com.gitee.jmash.dict.service;

import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.dict.entity.DictTypeEntity;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.util.Set;
import jmash.dict.protobuf.DictTypeCreateReq;
import jmash.dict.protobuf.DictTypeEnableKey;
import jmash.dict.protobuf.DictTypeImportReq;
import jmash.dict.protobuf.DictTypeMoveKey;
import jmash.dict.protobuf.DictTypeUpdateReq;

/**
 * 字典类型 os_dict_type服务Write接口.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public interface DictTypeWrite extends TenantService {

  /** 插入实体. */
  public DictTypeEntity insert(@NotNull @Valid DictTypeCreateReq dictType);

  /** update 实体. */
  public DictTypeEntity update(@NotNull @Valid DictTypeUpdateReq dictType);

  /** 根据主键删除. */
  public DictTypeEntity delete(@NotNull String entityId);

  /** 根据主键数组删除. */
  public Integer batchDelete(@NotNull @Valid Set<@NotNull String> entityIds);

  /** 上移、下移 */
  public boolean moveByOrderBy(@NotNull DictTypeMoveKey dictTypeMoveKey);

  /** 启用/禁用 */
  public boolean enableDictEntry(@NotNull DictTypeEnableKey dictTypeEnableKey);

  /** 字典导入 */
  public String importDictInfo(DictTypeImportReq dictTypeImportReq) throws Exception;
}
