
package com.gitee.jmash.dict.service;

import com.gitee.jmash.common.excel.ExcelImport;
import com.gitee.jmash.common.lock.DistributedLock;
import com.gitee.jmash.core.jaxrs.ParamsValidationException;
import com.gitee.jmash.core.orm.cdi.JpaTenantService;
import com.gitee.jmash.core.service.ServiceException;
import com.gitee.jmash.core.transaction.JakartaTransaction;
import com.gitee.jmash.core.utils.FieldMaskUtil;
import com.gitee.jmash.dict.entity.DictLayEntryEntity;
import com.gitee.jmash.dict.entity.DictTypeEntity;
import com.gitee.jmash.dict.excel.DictEntryHeaderImport;
import com.gitee.jmash.dict.excel.DictLayEntryHeaderImport;
import com.gitee.jmash.dict.excel.DictTypeHeaderImport;
import com.gitee.jmash.dict.mapper.DictTypeMapper;
import com.gitee.jmash.file.client.cdi.FileClient;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import jmash.dict.protobuf.DictEntryModel;
import jmash.dict.protobuf.DictEntryReq;
import jmash.dict.protobuf.DictLayEntryModel;
import jmash.dict.protobuf.DictLayEntryReq;
import jmash.dict.protobuf.DictTypeCreateReq;
import jmash.dict.protobuf.DictTypeEnableKey;
import jmash.dict.protobuf.DictTypeImportReq;
import jmash.dict.protobuf.DictTypeModel;
import jmash.dict.protobuf.DictTypeMoveKey;
import jmash.dict.protobuf.DictTypeUpdateReq;
import jmash.dict.protobuf.EntryTypes;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Workbook;
import jakarta.enterprise.inject.Typed;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import jakarta.transaction.Transactional.TxType;
import jakarta.validation.ValidationException;
import jakarta.validation.executable.ValidateOnExecution;

/**
 * 字典类型 os_dict_type写服务.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Typed(DictTypeWrite.class)
@Transactional(TxType.REQUIRED)
@JpaTenantService
@ValidateOnExecution
public class DictTypeWriteBean extends DictTypeReadBean
    implements DictTypeWrite, JakartaTransaction {

  private static final Log log = LogFactory.getLog(DictTypeWriteBean.class);

  @Inject
  DistributedLock lock;

  @Inject
  DictEntryWrite entryWrite;

  @Inject
  DictLayEntryWrite layEntryWrite;

  @Override
  @PersistenceContext(unitName = "WriteDict")
  public void setEntityManager(EntityManager entityManager) {
    this.tem.setEntityManager(entityManager, true);
  }

  @Override
  public DictTypeEntity insert(DictTypeCreateReq dictType) {
    DictTypeEntity entity = DictTypeMapper.INSTANCE.create(dictType);
    // 1.业务校验.
    DictTypeEntity exist = this.dictTypeDao.find(dictType.getTypeCode());
    if (exist != null) {
      throw new ParamsValidationException("typeCode", dictType.getTypeCode() + "数据字典类型已存在!");
    }
    // 2.仅校验,不执行.
    if (dictType.getValidateOnly()) {
      return entity;
    }
    // 3.检查是否重复请求.
    if (!lock.lock(dictType.getRequestId(), 60)) {
      throw new ParamsValidationException("requestId", "客户端发起重复请求");
    }
    // 4.执行业务(创建人及时间内部处理.)
    entity.setListOrder(dictTypeDao.getMaxListOrder() + 1);
    dictTypeDao.persist(entity);
    return entity;
  }

  @Override
  public DictTypeEntity update(DictTypeUpdateReq req) {
    DictTypeEntity entity = dictTypeDao.find(req.getTypeCode(), req.getValidateOnly());
    if (null == entity) {
      throw new ValidationException("找不到实体:" + req.getTypeCode());
    }
    // 无需更新,返回当前数据库数据.
    if (req.getUpdateMask().getPathsCount() == 0) {
      return entity;
    }
    // 更新掩码属性
    FieldMaskUtil.copyMask(entity, req, req.getUpdateMask());
    // 1.业务校验.
    // 2.仅校验,不执行.
    if (req.getValidateOnly()) {
      return entity;
    }

    // 3.检查是否重复请求.
    if (!lock.lock(req.getRequestId(), 60)) {
      throw new ParamsValidationException("requestId", "客户端发起重复请求");
    }

    // 4.执行业务
    dictTypeDao.merge(entity);
    return entity;
  }

  @Override
  public DictTypeEntity delete(String entityId) {
    DictTypeEntity entity = dictTypeDao.find(entityId);
    if (null==entity) {
      return null;
    }
    int count = 0;
    if (EntryTypes.normal.equals(entity.getEntryType())) {
      count = this.dictEntryDao
          .findCount(DictEntryReq.newBuilder().setTypeCode(entity.getTypeCode()).build());
    } else {
      count = this.dictLayEntryDao
          .findCount(DictLayEntryReq.newBuilder().setTypeCode(entity.getTypeCode()).build());
    }
    if (count > 0) {
      throw new ServiceException("存在字典数据,不能删除!");
    }
    dictTypeDao.remove(entity);
    return entity;
  }

  @Override
  public Integer batchDelete(Set<String> entityIds) {
    int i = 0;
    for (String entityId : entityIds) {
      try {
        delete(entityId);
      } catch (Exception ex) {
        log.debug(ex.getMessage());
      }
      i++;
    }
    return i;
  }

  @Override
  public boolean moveByOrderBy(DictTypeMoveKey dictTypeMoveKey) {
    DictTypeEntity dictEntry = dictTypeDao.find(dictTypeMoveKey.getTypeCode());
    if (dictEntry == null) {
      throw new ValidationException("无法找到相应实体");
    }
    boolean moveUp = dictTypeMoveKey.getUp();
    List<DictTypeEntity> entityList =
        dictTypeDao.selectListByOrder(dictEntry.getListOrder(), dictTypeMoveKey);
    if (entityList.size() < 2) {
      String msg = "";
      if (moveUp) {
        msg = "当前已经是第一条，无法向上移动！";
      } else {
        msg = "当前已经是最后一条，无法向下移动！";
      }
      throw new UnsupportedOperationException(msg);
    } else {
      int orderBy1 = entityList.get(1).getListOrder();
      int orderBy2 = entityList.get(0).getListOrder();

      if (orderBy1 == orderBy2 && moveUp) {
        orderBy1 = orderBy2 + 1;
      }
      if (orderBy1 == orderBy2 && !moveUp) {
        orderBy2 = orderBy1 + 1;
      }

      DictTypeEntity entryEntity1 = entityList.get(0);
      entryEntity1.setListOrder(orderBy1);
      dictTypeDao.merge(entryEntity1);

      DictTypeEntity entryEntity2 = entityList.get(1);
      entryEntity2.setListOrder(orderBy2);
      dictTypeDao.merge(entryEntity2);
    }
    return true;
  }

  @Override
  public boolean enableDictEntry(DictTypeEnableKey dictTypeEnableKey) {
    DictTypeEntity entity = dictTypeDao.find(dictTypeEnableKey.getTypeCode());
    if (entity == null) {
      throw new ValidationException("无法找到对应实体数据！");
    }
    entity.setEnable(dictTypeEnableKey.getEnable());
    dictTypeDao.merge(entity);
    return true;
  }

  @Override
  public String importDictInfo(DictTypeImportReq dictTypeImportReq) throws Exception {
    // 检查是否重复请求.
    if (!lock.lock(dictTypeImportReq.getRequestId(), 60)) {
      throw new ParamsValidationException("requestId", "客户端发起重复请求");
    }
    // 文件检查
    File file = FileClient.downloadFile(dictTypeImportReq.getFileNames());
    if (!file.exists()) {
      return "找不到文件：" + dictTypeImportReq.getFileNames();
    }

    int success = 0;
    int fail = 0;
    ExcelImport excel = new ExcelImport();
    excel.openExcel(file);
    excel.setHeaders(DictTypeHeaderImport.getHeaderImports());
    excel.setStartHeader(0);
    List<DictTypeModel> typeModels = excel.importEntity(0, DictTypeModel.getDefaultInstance());
    // 字典类型导入
    for (DictTypeModel typeModel : typeModels) {
      try {
        DictTypeEntity entity = this.dictTypeDao.find(typeModel.getTypeCode());
        if (entity == null) {
          insert(DictTypeMapper.INSTANCE.genCreate(typeModel));
        } else {
          update(DictTypeMapper.INSTANCE.genUpdate(typeModel));
        }
        success++;
      } catch (Exception ex) {
        log.error("", ex);
        fail++;
      }
    }

    Workbook wb = excel.getWb();
    // 字典导入
    for (int i = 1; i < wb.getNumberOfSheets(); i++) {
      String sheetName = wb.getSheetName(i);
      if (!sheetName.startsWith("层级")) {
        // 普通字典
        excel.setHeaders(DictEntryHeaderImport.getHeaderImports());
        List<DictEntryModel> entryModels =
            excel.importEntity(i, DictEntryModel.getDefaultInstance());
        for (DictEntryModel entryModel : entryModels) {
          try {
            entryWrite.save(entryModel);
            success++;
          } catch (Exception ex) {
            log.error("", ex);
            fail++;
          }
        }
      } else {
        // 层级字典
        excel.setHeaders(DictLayEntryHeaderImport.getHeaderImports());
        List<DictLayEntryModel> layEntryModels =
            excel.importEntity(i, DictLayEntryModel.getDefaultInstance());
        // 资源新ID存放.
        Map<String, String> newIdMap = new HashMap<>();
        for (DictLayEntryModel layEntryModel : layEntryModels) {
          try {
            // 替换新ID
            if (newIdMap.containsKey(layEntryModel.getParentId())) {
              layEntryModel = layEntryModel.toBuilder().setParentId(newIdMap.get(layEntryModel.getParentId())).build();
            }
            DictLayEntryEntity entity = layEntryWrite.save(layEntryModel);
            // 存放新ID.
            newIdMap.put(layEntryModel.getDictId(), entity.getDictId().toString());
            success++;
          } catch (Exception ex) {
            log.error("", ex);
            fail++;
          }
        }
      }
    }
    return String.format("导入成功%d行,失败%d行.<br/>%s", success, fail, excel.getErrorMsg());
  }
}
