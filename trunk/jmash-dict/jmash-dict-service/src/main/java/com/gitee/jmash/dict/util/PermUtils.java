package com.gitee.jmash.dict.util;

import com.gitee.jmash.common.grpc.GrpcContext;
import com.gitee.jmash.dict.DictFactory;
import com.gitee.jmash.dict.entity.DictTypeEntity;
import com.gitee.jmash.dict.service.DictTypeRead;
import org.apache.shiro.subject.Subject;

/** 权限检查. */
public class PermUtils {

  /** 检查是否登录认证. */
  public static boolean isAuthenticated() {
    Subject subject = (Subject) GrpcContext.USER_SUBJECT.get();
    return subject != null && subject.isAuthenticated();
  }

  /** 检查启用的数据字典类型是否开放访问. */
  public static boolean checkDictTypeOpen(String tenant, String typeCode) {
    try (DictTypeRead dictTypeRead = DictFactory.getDictTypeRead(tenant)) {
      DictTypeEntity entity = dictTypeRead.findById(typeCode);
      return (entity != null) && entity.getIsOpen() && entity.getEnable();
    } catch (Exception ex) {
      return false;
    }
  }


}
