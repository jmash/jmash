/*!40101 SET NAMES utf8 */;

/*==============================================================*/
/* Table: os_dict_entry                                         */
/*==============================================================*/
create table os_dict_entry
(
   type_code            varchar(63) not null comment '字典类型编码',
   dict_code            varchar(63) not null comment '字典编码',
   dict_name            varchar(255) not null comment '字典名称',
   list_order           int comment '排序',
   enable_              bool not null comment '是否启用',
   description_         varchar(255) comment '描述',
   primary key (type_code, dict_code)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

alter table os_dict_entry comment '数据字典';

/*==============================================================*/
/* Table: os_dict_lay_entry                                     */
/*==============================================================*/
create table os_dict_lay_entry
(
   dict_id              binary(16) not null comment '字典ID',
   type_code            varchar(63) not null comment '字典类型编码',
   dict_code            varchar(63) not null comment '字典编码$编码唯一',
   dict_name            varchar(255) not null comment '字典名称',
   parent_id            binary(16) comment '字典父ID',
   depth_               int not null comment '深度',
   list_order           int not null comment '排序',
   enable_              bool not null comment '是否启用',
   description_         varchar(255) comment '描述',
   primary key (dict_id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

alter table os_dict_lay_entry comment '层级字典实体';

/*==============================================================*/
/* Table: os_dict_type                                          */
/*==============================================================*/
create table os_dict_type
(
   type_code            varchar(63) not null comment '字典类型编码',
   type_name            varchar(255) not null comment '字典类型名称',
   list_order           int comment '排序',
   enable_              bool not null comment '是否启用',
   is_open              bool NOT NULL   COMMENT '是否开放访问' ,
   entry_type           varchar(50) comment '字典实体类型$普通字典、层级字典',
   description_         varchar(255) comment '描述',
   primary key (type_code)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

alter table os_dict_type comment '字典类型';

/*==============================================================*/
/* 外键                                                         */
/*==============================================================*/

alter table os_dict_entry add constraint FK_os_dict_entry_1 foreign key (type_code)
      references os_dict_type (type_code) on delete restrict on update restrict;

alter table os_dict_lay_entry add constraint FK_os_dict_lay_entry_1 foreign key (type_code)
      references os_dict_type (type_code) on delete restrict on update restrict;

/*==============================================================*/
/* 索引                                                         */
/*==============================================================*/
      
create index index_os_dict_entry_1 on os_dict_entry ( type_code , enable_ );

create index index_os_dict_lay_entry_1 on os_dict_lay_entry ( type_code , dict_code );

create index index_os_dict_type_1 on os_dict_type ( entry_type , enable_ );

