/*!40101 SET NAMES utf8 */;
ALTER TABLE os_dict_type  ADD COLUMN is_open bool AFTER enable_;
update os_dict_type set is_open = false where is_open is null;