
package com.gitee.jmash.dict.test;


import com.google.protobuf.FieldMask;
import com.google.protobuf.Int32Value;
import io.grpc.StatusRuntimeException;
import java.util.UUID;
import jmash.dict.DictGrpc;
import jmash.dict.protobuf.DictEntryCreateReq;
import jmash.dict.protobuf.DictEntryEnableKey;
import jmash.dict.protobuf.DictEntryKey;
import jmash.dict.protobuf.DictEntryKeyList;
import jmash.dict.protobuf.DictEntryModel;
import jmash.dict.protobuf.DictEntryMoveKey;
import jmash.dict.protobuf.DictEntryPage;
import jmash.dict.protobuf.DictEntryReq;
import jmash.dict.protobuf.DictEntryUpdateReq;
import jmash.dict.protobuf.DictTypeCreateReq;
import jmash.dict.protobuf.DictTypeKey;
import jmash.dict.protobuf.DictTypeModel;
import jmash.dict.protobuf.EntryTypes;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

/**
 * 测试.
 *
 * @author CGD
 *
 */
@TestMethodOrder(OrderAnnotation.class)
public class DictEntryTest extends DictTest {

  private DictGrpc.DictBlockingStub dictStub = null;

  @Test
  @Order(1)
  public void validatorTest() {
    dictStub = DictGrpc.newBlockingStub(channel);
    dictStub = dictStub.withCallCredentials(getAuthcCallCredentials());
    StatusRuntimeException exception = Assertions.assertThrows(StatusRuntimeException.class, () -> {
      DictEntryCreateReq req = DictEntryCreateReq.newBuilder().build();
      dictStub.createDictEntry(req);
    });
    Assertions.assertEquals("INTERNAL", exception.getStatus().getCode().name());
    Assertions.assertNotNull(exception.getMessage());
  }

  private String TEST_TYPE_CODE = "test_2";
  private String TEST_DICT_CODE_1 = "01";
  private String TEST_DICT_CODE_2 = "02";
  private String TEST_DICT_CODE_3 = "03";


  @Test
  @Order(2)
  public void testAdd() {
    dictStub = DictGrpc.newBlockingStub(channel);
    dictStub = dictStub.withCallCredentials(getAuthcCallCredentials());

    // 创建Type
    DictTypeCreateReq.Builder req1 = DictTypeCreateReq.newBuilder();
    req1.setRequestId(UUID.randomUUID().toString());
    req1.setTypeCode(TEST_TYPE_CODE);
    req1.setTenant(TENANT);
    req1.setDescription("Test 1");
    req1.setEntryType(EntryTypes.normal);
    req1.setTypeName("Test 1");
    DictTypeModel entity1 = dictStub.createDictType(req1.build());
    Assertions.assertEquals(TEST_TYPE_CODE, entity1.getTypeCode());
    Assertions.assertEquals(EntryTypes.normal, entity1.getEntryType());

    // 创建01
    DictEntryCreateReq.Builder req = DictEntryCreateReq.newBuilder();
    req.setRequestId(UUID.randomUUID().toString());
    req.setTenant(TENANT);
    req.setTypeCode(TEST_TYPE_CODE);
    req.setDictCode(TEST_DICT_CODE_1);
    req.setDictName("测试字典");
    DictEntryModel entry = dictStub.createDictEntry(req.build());

    req = DictEntryCreateReq.newBuilder();
    req.setRequestId(UUID.randomUUID().toString());
    req.setTenant(TENANT);
    req.setTypeCode(TEST_TYPE_CODE);
    req.setDictCode(TEST_DICT_CODE_2);
    req.setDictName("测试字典02");
    entry = dictStub.createDictEntry(req.build());

    req = DictEntryCreateReq.newBuilder();
    req.setRequestId(UUID.randomUUID().toString());
    req.setTenant(TENANT);
    req.setTypeCode(TEST_TYPE_CODE);
    req.setDictCode(TEST_DICT_CODE_3);
    req.setDictName("测试字典03");
    entry = dictStub.createDictEntry(req.build());
    Assertions.assertEquals(TEST_TYPE_CODE, entry.getTypeCode());
    Assertions.assertEquals(TEST_DICT_CODE_3, entry.getDictCode());
  }

  @Test
  @Order(3)
  public void findPage() {
    dictStub = DictGrpc.newBlockingStub(channel);
    dictStub = dictStub.withCallCredentials(getAuthcCallCredentials());
    final DictEntryReq.Builder request = DictEntryReq.newBuilder();
    request.setTypeCode(TEST_TYPE_CODE).setTenant(TENANT);
    final DictEntryPage modelPage = dictStub.findDictEntryPage(request.build());
    Assertions.assertEquals(true, modelPage.getResultsCount() >= 3);
  }

  @Test
  @Order(4)
  public void testUpdate() {
    dictStub = DictGrpc.newBlockingStub(channel);
    dictStub = dictStub.withCallCredentials(getAuthcCallCredentials());
    // findById
    DictEntryKey key = DictEntryKey.newBuilder().setTenant(TENANT).setDictCode(TEST_DICT_CODE_1)
        .setTypeCode(TEST_TYPE_CODE).build();
    DictEntryModel entity = dictStub.findDictEntryById(key);
    Assertions.assertNotEquals("abc", entity.getDictName());
    // Update
    DictEntryUpdateReq.Builder updateReq = DictEntryUpdateReq.newBuilder();
    updateReq.setRequestId(UUID.randomUUID().toString());
    updateReq.setTenant(TENANT);
    updateReq.setDictCode(TEST_DICT_CODE_1);
    updateReq.setTypeCode(TEST_TYPE_CODE);
    updateReq.setDictName("abc");

    FieldMask.Builder fieldMask = FieldMask.newBuilder();
    fieldMask.addPaths("dictName");
    updateReq.setUpdateMask(fieldMask.build());

    entity = dictStub.updateDictEntry(updateReq.build());
    Assertions.assertEquals("abc", entity.getDictName());
  }

  @Test
  @Order(5)
  public void moveDictEntry() {
    dictStub = DictGrpc.newBlockingStub(channel);
    dictStub = dictStub.withCallCredentials(getAuthcCallCredentials());
    // findById
    DictEntryKey key = DictEntryKey.newBuilder().setTenant(TENANT).setDictCode(TEST_DICT_CODE_2)
        .setTypeCode(TEST_TYPE_CODE).build();
    DictEntryModel entity = dictStub.findDictEntryById(key);
    int listOrder = entity.getListOrder();
    // Move
    DictEntryMoveKey req = DictEntryMoveKey.newBuilder().setTenant(TENANT)
        .setDictCode(TEST_DICT_CODE_2).setTypeCode(TEST_TYPE_CODE).setUp(true).build();
    dictStub.moveDictEntry(req);
    // findById
    entity = dictStub.findDictEntryById(key);
    // listOrder
    Assertions.assertTrue(listOrder > entity.getListOrder());
  }

  @Test
  @Order(6)
  public void enableDictEntry() {
    dictStub = DictGrpc.newBlockingStub(channel);

    dictStub = dictStub.withCallCredentials(getAuthcCallCredentials());
    // findById
    DictEntryKey key = DictEntryKey.newBuilder().setTenant(TENANT).setDictCode(TEST_DICT_CODE_2)
        .setTypeCode(TEST_TYPE_CODE).build();
    DictEntryModel entity = dictStub.findDictEntryById(key);
    boolean enable = entity.getEnable();
    // Move
    DictEntryEnableKey req = DictEntryEnableKey.newBuilder().setTenant(TENANT)
        .setDictCode(TEST_DICT_CODE_2).setTypeCode(TEST_TYPE_CODE).setEnable(!enable)
        .build();
    dictStub.enableDictEntry(req);
    // findById
    entity = dictStub.findDictEntryById(key);
    // listOrder
    Assertions.assertTrue(enable == !entity.getEnable());
  }
  
  @Test
  @Order(7)
  public void batchDeleteTest() {
    dictStub = DictGrpc.newBlockingStub(channel);
    dictStub= dictStub.withCallCredentials(getAuthcCallCredentials());
    DictEntryKey key1 = DictEntryKey.newBuilder().setTenant(TENANT).setDictCode(TEST_DICT_CODE_1)
        .setTypeCode(TEST_TYPE_CODE).build();
    DictEntryKey key2 = DictEntryKey.newBuilder().setTenant(TENANT).setDictCode(TEST_DICT_CODE_2)
        .setTypeCode(TEST_TYPE_CODE).build();
    DictEntryKeyList.Builder request = DictEntryKeyList.newBuilder().setTenant(TENANT).addDictEntryKey(key1).addDictEntryKey(key2);
    final Int32Value result = dictStub.batchDeleteDictEntry(request.build());
    Assertions.assertEquals(2, result.getValue());
  }
  
  @Test
  @Order(8)
  public void deleteTest() {
    dictStub = DictGrpc.newBlockingStub(channel);
    dictStub= dictStub.withCallCredentials(getAuthcCallCredentials());
    DictEntryKey key3 = DictEntryKey.newBuilder().setTenant(TENANT).setDictCode(TEST_DICT_CODE_3)
        .setTypeCode(TEST_TYPE_CODE).build(); 
    DictEntryModel model = dictStub.deleteDictEntry(key3);
    Assertions.assertNotNull(model);
    // Delete Type
    DictTypeKey pk = DictTypeKey.newBuilder().setTypeCode(TEST_TYPE_CODE).setTenant(TENANT).build();
    dictStub.deleteDictType(pk);
  }
  
}
