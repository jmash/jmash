
package com.gitee.jmash.dict.test;


import com.google.protobuf.FieldMask;
import io.grpc.StatusRuntimeException;
import java.util.UUID;
import jmash.dict.DictGrpc;
import jmash.dict.protobuf.DictLayEntryCreateReq;
import jmash.dict.protobuf.DictLayEntryEnableKey;
import jmash.dict.protobuf.DictLayEntryKey;
import jmash.dict.protobuf.DictLayEntryKeyList;
import jmash.dict.protobuf.DictLayEntryList;
import jmash.dict.protobuf.DictLayEntryModel;
import jmash.dict.protobuf.DictLayEntryMoveKey;
import jmash.dict.protobuf.DictLayEntryReq;
import jmash.dict.protobuf.DictLayEntryUpdateReq;
import jmash.dict.protobuf.DictTypeCreateReq;
import jmash.dict.protobuf.DictTypeKey;
import jmash.dict.protobuf.DictTypeModel;
import jmash.dict.protobuf.EntryTypes;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

/**
 * 测试.
 *
 * @author CGD
 *
 */
@TestMethodOrder(OrderAnnotation.class)
public class DictLayEntryTest extends DictTest {

  private DictGrpc.DictBlockingStub dictStub = null;

  @Test
  @Order(1)
  public void validatorTest() {
    dictStub = DictGrpc.newBlockingStub(channel);
    dictStub = dictStub.withCallCredentials(getAuthcCallCredentials());
    StatusRuntimeException exception = Assertions.assertThrows(StatusRuntimeException.class, () -> {
      DictLayEntryCreateReq req = DictLayEntryCreateReq.newBuilder().build();
      dictStub.createDictLayEntry(req);
    });
    Assertions.assertEquals("INTERNAL", exception.getStatus().getCode().name());
    Assertions.assertNotNull(exception.getMessage());
  }

  private String typeCode = "LAY_TEST_1";

  private String dictCode1 = "P1";
  private String dictCode2 = "C1";
  private String dictCode3 = "C2";

  @Test
  @Order(2)
  public void test() {
    dictStub = DictGrpc.newBlockingStub(channel);
    dictStub = dictStub.withCallCredentials(getAuthcCallCredentials());

    // 创建Type
    DictTypeCreateReq.Builder req1 = DictTypeCreateReq.newBuilder();
    req1.setRequestId(UUID.randomUUID().toString());
    req1.setTypeCode(typeCode);
    req1.setTenant(TENANT);
    req1.setDescription("Test lay");
    req1.setEntryType(EntryTypes.layout);
    req1.setTypeName("Test lay");
    DictTypeModel entity1 = dictStub.createDictType(req1.build());
    Assertions.assertEquals(typeCode, entity1.getTypeCode());
    Assertions.assertEquals(EntryTypes.layout, entity1.getEntryType());

    // 创建
    DictLayEntryCreateReq.Builder req = DictLayEntryCreateReq.newBuilder();
    req.setRequestId(UUID.randomUUID().toString());
    req.setTenant(TENANT);
    req.setDictCode(dictCode1);
    req.setTypeCode(typeCode);
    req.setDictName("父1");
    DictLayEntryModel p1 = dictStub.createDictLayEntry(req.build());
    Assertions.assertNotEquals("f1", p1.getDictName());

    req = DictLayEntryCreateReq.newBuilder();
    req.setRequestId(UUID.randomUUID().toString());
    req.setTenant(TENANT);
    req.setDictCode(dictCode2);
    req.setTypeCode(typeCode);
    req.setParentId(p1.getDictId());
    req.setDictName("孩1");
    dictStub.createDictLayEntry(req.build());

    req = DictLayEntryCreateReq.newBuilder();
    req.setRequestId(UUID.randomUUID().toString());
    req.setTenant(TENANT);
    req.setDictCode(dictCode3);
    req.setTypeCode(typeCode);
    req.setParentId(p1.getDictId());
    req.setDictName("孩2");
    dictStub.createDictLayEntry(req.build());

    // Update
    DictLayEntryUpdateReq.Builder updateReq = DictLayEntryUpdateReq.newBuilder();
    updateReq.setRequestId(UUID.randomUUID().toString());
    updateReq.setDictId(p1.getDictId());
    updateReq.setDictName("f1");
    updateReq.setTenant(TENANT);
    // ...
    FieldMask.Builder fieldMask = FieldMask.newBuilder();
    fieldMask.addPaths("dictName");
    updateReq.setUpdateMask(fieldMask.build());
    p1 = dictStub.updateDictLayEntry(updateReq.build());
    Assertions.assertEquals("f1", p1.getDictName());
  }

  @Test
  @Order(3)
  public void findList() {
    dictStub = DictGrpc.newBlockingStub(channel);
    dictStub = dictStub.withCallCredentials(getAuthcCallCredentials());
    final DictLayEntryReq.Builder request =
        DictLayEntryReq.newBuilder().setTenant(TENANT).setTypeCode(typeCode);
    dictStub.findDictLayEntryList(request.build());
  }

  @Test
  @Order(4)
  public void moveDictLayEntry() {
    dictStub = DictGrpc.newBlockingStub(channel);
    dictStub = dictStub.withCallCredentials(getAuthcCallCredentials());
    final DictLayEntryReq.Builder request =
        DictLayEntryReq.newBuilder().setTenant(TENANT).setTypeCode(typeCode);
    DictLayEntryList list = dictStub.findDictLayEntryList(request.build());
    DictLayEntryModel model = list.getResults(0).getChildren(0);
    int listOrder=model.getListOrder();
    DictLayEntryMoveKey req = DictLayEntryMoveKey.newBuilder()
        .setDictId(model.getDictId()).setTenant(TENANT).setUp(false).build();
    dictStub.moveDictLayEntry(req);
    // find by id
    model = dictStub.findDictLayEntryById(
        DictLayEntryKey.newBuilder().setTenant(TENANT).setDictId(model.getDictId()).build());
    Assertions.assertTrue(listOrder < model.getListOrder());
  }

  @Test
  @Order(5)
  public void enableDictLayEntry() {
    dictStub = DictGrpc.newBlockingStub(channel);
    dictStub = dictStub.withCallCredentials(getAuthcCallCredentials());

    final DictLayEntryReq.Builder request =
        DictLayEntryReq.newBuilder().setTenant(TENANT).setTypeCode(typeCode);
    DictLayEntryList list = dictStub.findDictLayEntryList(request.build());
    DictLayEntryModel model = list.getResults(0);
    boolean enable = model.getEnable();

    DictLayEntryEnableKey req = DictLayEntryEnableKey.newBuilder().setDictId(model.getDictId())
        .setTenant(TENANT).setEnable(!enable).build();
    dictStub.enableDictLayEntry(req);
    // find by id
    model = dictStub.findDictLayEntryById(
        DictLayEntryKey.newBuilder().setTenant(TENANT).setDictId(model.getDictId()).build());
    Assertions.assertEquals(model.getEnable(), !enable);
  }

  @Test
  @Order(6)
  public void batchDeleteTest() {
    dictStub = DictGrpc.newBlockingStub(channel);
    dictStub = dictStub.withCallCredentials(getAuthcCallCredentials());
    final DictLayEntryReq.Builder request =
        DictLayEntryReq.newBuilder().setTenant(TENANT).setTypeCode(typeCode);
    DictLayEntryList list = dictStub.findDictLayEntryList(request.build());
    for (DictLayEntryModel model : list.getResultsList()) {
      delDictLayEntryModel(model);
    }

    // Delete Type
    DictTypeKey pk = DictTypeKey.newBuilder().setTypeCode(typeCode).setTenant(TENANT).build();
    dictStub.deleteDictType(pk);
  }

  // 递归删除.
  public void delDictLayEntryModel(DictLayEntryModel model) {
    if (model.getChildrenCount() > 0) {
      for (DictLayEntryModel child : model.getChildrenList()) {
        delDictLayEntryModel(child);
      }
    }
    DictLayEntryKeyList.Builder delReq =
        DictLayEntryKeyList.newBuilder().setTenant(TENANT).addDictId(model.getDictId());
    dictStub.batchDeleteDictLayEntry(delReq.build());
  }

}
