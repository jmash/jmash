
package com.gitee.jmash.dict.test;


import com.gitee.jmash.core.lib.FileClientUtil;
import com.google.api.HttpBody;
import com.google.protobuf.FieldMask;
import com.google.protobuf.Int32Value;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import java.io.File;
import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import jmash.dict.DictGrpc;
import jmash.dict.protobuf.DictTypeCreateReq;
import jmash.dict.protobuf.DictTypeEnableKey;
import jmash.dict.protobuf.DictTypeExportReq;
import jmash.dict.protobuf.DictTypeImportReq;
import jmash.dict.protobuf.DictTypeKey;
import jmash.dict.protobuf.DictTypeKeyList;
import jmash.dict.protobuf.DictTypeModel;
import jmash.dict.protobuf.DictTypeMoveKey;
import jmash.dict.protobuf.DictTypePage;
import jmash.dict.protobuf.DictTypeReq;
import jmash.dict.protobuf.DictTypeUpdateReq;
import jmash.dict.protobuf.EntryTypes;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

/**
 * 测试.
 *
 * @author CGD
 *
 */
@TestMethodOrder(OrderAnnotation.class)
public class DictTypeTest extends DictTest {

  private DictGrpc.DictBlockingStub dictStub = null;

  @Test
  @Order(1)
  public void validatorTest() {
    dictStub = DictGrpc.newBlockingStub(channel);
    dictStub = dictStub.withCallCredentials(getAuthcCallCredentials());
    StatusRuntimeException exception = Assertions.assertThrows(StatusRuntimeException.class, () -> {
      DictTypeCreateReq req = DictTypeCreateReq.newBuilder().build();
      dictStub.createDictType(req);
    });
    Assertions.assertEquals("INTERNAL", exception.getStatus().getCode().name());
    Assertions.assertNotNull(exception.getMessage());
  }

  String typeCode1 = "13fca2c0-5ad9-4fe0-ae1b-6260a582a50a";
  String typeCode2 = "17cf158c-61ca-4268-afd4-07b08c1a4c69";
  String typeCode3 = "a81b28d9-302f-4d4e-9b4b-9739efbaadc9";

  @Test
  @Order(2)
  public void testAdd() {
    dictStub = DictGrpc.newBlockingStub(channel);
    dictStub = dictStub.withCallCredentials(getAuthcCallCredentials());
    // 创建Test 1
    DictTypeCreateReq.Builder req = DictTypeCreateReq.newBuilder();
    req.setRequestId(UUID.randomUUID().toString());
    req.setTypeCode(typeCode1);
    req.setTenant(TENANT);
    req.setDescription("Test 1");
    req.setEntryType(EntryTypes.normal);
    req.setTypeName("Test 1");
    DictTypeModel entity = dictStub.createDictType(req.build());
    Assertions.assertEquals(typeCode1, entity.getTypeCode());
    Assertions.assertEquals(EntryTypes.normal, entity.getEntryType());

    // 创建Test 2
    req = DictTypeCreateReq.newBuilder();
    req.setRequestId(UUID.randomUUID().toString());
    req.setTypeCode(typeCode2);
    req.setTenant(TENANT);
    req.setDescription("Test 2");
    req.setEntryType(EntryTypes.layout);
    req.setTypeName("Test 2");
    entity = dictStub.createDictType(req.build());
    Assertions.assertEquals(typeCode2, entity.getTypeCode());
    Assertions.assertEquals(EntryTypes.layout, entity.getEntryType());

    // 创建Test 3
    req = DictTypeCreateReq.newBuilder();
    req.setRequestId(UUID.randomUUID().toString());
    req.setTypeCode(typeCode3);
    req.setTenant(TENANT);
    req.setDescription("Test 3");
    req.setTypeName("Test 3");
    entity = dictStub.createDictType(req.build());
    Assertions.assertEquals(typeCode3, entity.getTypeCode());
    Assertions.assertEquals("Test 3", entity.getTypeName());
    Assertions.assertEquals(EntryTypes.normal, entity.getEntryType());
  }

  @Test
  @Order(3)
  public void findPage() {
    dictStub = DictGrpc.newBlockingStub(channel);
    dictStub = dictStub.withCallCredentials(getAuthcCallCredentials());
    final DictTypeReq.Builder request = DictTypeReq.newBuilder().setTenant(TENANT);
    final DictTypePage modelPage = dictStub.findDictTypePage(request.build());
    Assertions.assertEquals(true, modelPage.getResultsCount() >= 3);
  }

  @Test
  @Order(4)
  public void testUpdate() {
    dictStub = DictGrpc.newBlockingStub(channel);
    dictStub = dictStub.withCallCredentials(getAuthcCallCredentials());
    // findById
    DictTypeKey pk = DictTypeKey.newBuilder().setTypeCode(typeCode1).setTenant(TENANT).build();
    DictTypeModel entity = dictStub.findDictTypeById(pk);

    // Update
    DictTypeUpdateReq.Builder updateReq = DictTypeUpdateReq.newBuilder();
    updateReq.setRequestId(UUID.randomUUID().toString());
    updateReq.setTypeCode(entity.getTypeCode());
    updateReq.setTypeName("UpdateName");
    updateReq.setTenant(TENANT);
    // ...
    FieldMask.Builder fieldMask = FieldMask.newBuilder();
    fieldMask.addPaths("typeName");
    updateReq.setUpdateMask(fieldMask.build());

    entity = dictStub.updateDictType(updateReq.build());
    Assertions.assertEquals("UpdateName", entity.getTypeName());
  }

  @Test
  @Order(5)
  public void moveDictType() {
    dictStub = DictGrpc.newBlockingStub(channel);
    dictStub = dictStub.withCallCredentials(getAuthcCallCredentials());

    DictTypeKey pk = DictTypeKey.newBuilder().setTypeCode(typeCode2).setTenant(TENANT).build();
    DictTypeModel entity = dictStub.findDictTypeById(pk);
    int listOrder = entity.getListOrder();

    DictTypeMoveKey req =
        DictTypeMoveKey.newBuilder().setTypeCode(typeCode2).setTenant(TENANT).setUp(true).build();
    dictStub.moveDictType(req);
    pk = DictTypeKey.newBuilder().setTypeCode(typeCode2).setTenant(TENANT).build();
    entity = dictStub.findDictTypeById(pk);
    Assertions.assertTrue(listOrder > entity.getListOrder());
  }

  @Test
  @Order(6)
  public void enableDictType() {
    dictStub = DictGrpc.newBlockingStub(channel);
    dictStub = dictStub.withCallCredentials(getAuthcCallCredentials());
    DictTypeEnableKey req = DictTypeEnableKey.newBuilder().setTypeCode(typeCode2).setTenant(TENANT)
        .setEnable(false).build();
    dictStub.enableDictType(req);
    DictTypeKey pk = DictTypeKey.newBuilder().setTypeCode(typeCode2).setTenant(TENANT).build();
    DictTypeModel entity = dictStub.findDictTypeById(pk);
    Assertions.assertEquals(false, entity.getEnable());
  }

  @Test
  @Order(7)
  public void downloadDictInfoTemplate() throws IOException, InterruptedException {
    DictGrpc.DictStub dictStub1 = DictGrpc.newStub(channel);
    dictStub1 = dictStub1.withCallCredentials(getAuthcCallCredentials());
    final CountDownLatch latchFinish = new CountDownLatch(1);
    StreamObserver<HttpBody> resp = FileClientUtil
        .createHttpBodyResp(new File("/data/jmash/test/dict_template.xlsx"), latchFinish);
    DictTypeExportReq req = DictTypeExportReq.newBuilder().setTenant(TENANT).build();
    dictStub1.downloadDictType(req, resp);
    latchFinish.await();
  }

  @Test
  @Order(8)
  public void exportDictInfo() throws IOException, InterruptedException {
    DictGrpc.DictStub dictStub1 = DictGrpc.newStub(channel);
    dictStub1 = dictStub1.withCallCredentials(getAuthcCallCredentials());
    final CountDownLatch latchFinish = new CountDownLatch(1);
    StreamObserver<HttpBody> resp =
        FileClientUtil.createHttpBodyResp(new File("/data/jmash/test/dict.xlsx"), latchFinish);
    DictTypeExportReq req = DictTypeExportReq.newBuilder().setTenant(TENANT)
        .setReq(DictTypeReq.newBuilder().setTenant(TENANT).build()).build();
    dictStub1.exportDictType(req, resp);
    latchFinish.await();
  }


  @Test
  @Order(9)
  public void importDictInfo() throws IOException, InterruptedException {
    dictStub = DictGrpc.newBlockingStub(channel);
    dictStub = dictStub.withCallCredentials(getAuthcCallCredentials());
    DictTypeImportReq.Builder req =
        DictTypeImportReq.newBuilder().setRequestId(UUID.randomUUID().toString()).setTenant(TENANT);
    req.setFileNames("dict.xlsx");
    dictStub.importDictType(req.build());
  }

  @Test
  @Order(10)
  public void testDelete() {
    dictStub = DictGrpc.newBlockingStub(channel);
    dictStub = dictStub.withCallCredentials(getAuthcCallCredentials());
    // Delete
    DictTypeKey pk = DictTypeKey.newBuilder().setTypeCode(typeCode1).setTenant(TENANT).build();
    dictStub.deleteDictType(pk);
  }

  @Test
  @Order(11)
  public void batchDeleteTest() {
    dictStub = DictGrpc.newBlockingStub(channel);
    dictStub = dictStub.withCallCredentials(getAuthcCallCredentials());
    DictTypeKeyList.Builder request = DictTypeKeyList.newBuilder().setTenant(TENANT)
        .addTypeCode(typeCode2).addTypeCode(typeCode3);
    final Int32Value result = dictStub.batchDeleteDictType(request.build());
    System.out.println(result);
  }


}
