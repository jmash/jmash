module gitee.com/jmash/jmash/trunk/jmash-file/jmash-file-gateway

go 1.22.7

toolchain go1.22.9

require (
	gitee.com/jmash/jmash/trunk/jmash-core-gateway v0.0.0-20250314154102-c000f35f501f
	github.com/grpc-ecosystem/grpc-gateway/v2 v2.23.0
	google.golang.org/genproto/googleapis/api v0.0.0-20241021214115-324edc3d5d38
	google.golang.org/grpc v1.68.0
	google.golang.org/protobuf v1.35.1
)

require (
	github.com/golang/glog v1.2.3 // indirect
	github.com/golang/protobuf v1.5.4 // indirect
	golang.org/x/net v0.29.0 // indirect
	golang.org/x/sys v0.25.0 // indirect
	golang.org/x/text v0.19.0 // indirect
	google.golang.org/genproto/googleapis/rpc v0.0.0-20241021214115-324edc3d5d38 // indirect
)
