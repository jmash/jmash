# Protocol Documentation
<a name="top"></a>

## Table of Contents

- [jmash/file/protobuf/file_message.proto](#jmash_file_protobuf_file_message-proto)
    - [JmashFileKey](#jmash-file-JmashFileKey)
    - [JmashFileKeyList](#jmash-file-JmashFileKeyList)
    - [JmashFileList](#jmash-file-JmashFileList)
    - [JmashFileModel](#jmash-file-JmashFileModel)
    - [JmashFileModelTotal](#jmash-file-JmashFileModelTotal)
    - [JmashFilePage](#jmash-file-JmashFilePage)
    - [JmashFileReq](#jmash-file-JmashFileReq)
  
- [Scalar Value Types](#scalar-value-types)



<a name="jmash_file_protobuf_file_message-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## jmash/file/protobuf/file_message.proto



<a name="jmash-file-JmashFileKey"></a>

### JmashFileKey
List


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| file_id | [string](#string) |  |  |






<a name="jmash-file-JmashFileKeyList"></a>

### JmashFileKeyList
List


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| file_id | [string](#string) | repeated |  |






<a name="jmash-file-JmashFileList"></a>

### JmashFileList
列表


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [JmashFileModel](#jmash-file-JmashFileModel) | repeated | 当前页内容 |






<a name="jmash-file-JmashFileModel"></a>

### JmashFileModel
实体


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| file_id | [string](#string) |  | 文件ID |
| file_src | [string](#string) |  | 文件存储路径 |
| content_type | [string](#string) |  | 文件内容类型 |
| file_ext | [string](#string) |  | 文件后缀 |
| file_size | [int64](#int64) |  | 文件大小 |
| hash_sm3 | [bytes](#bytes) |  | 文件Hash SM3值 |
| hash_sha256 | [bytes](#bytes) |  | 文件Hash SHA256值 |
| file_width | [int32](#int32) |  | 图片/视频宽度 |
| file_height | [int32](#int32) |  | 图片/视频高度 |
| file_time | [int32](#int32) |  | 音频/视频时长 |
| file_location | [string](#string) |  | 图片/视频/音频拍摄位置 |
| geo_longitude | [string](#string) |  | 经度 |
| geo_latitude | [string](#string) |  | 纬度 |
| geo_accuracy | [int32](#int32) |  | 精度 |
| geo_altitude | [int64](#int64) |  | 海拔高度 |
| geo_altitude_accracy | [int32](#int32) |  | 海拔高度精度 |
| create_date | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  | 创建时间 |






<a name="jmash-file-JmashFileModelTotal"></a>

### JmashFileModelTotal
合计


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| total_size | [int32](#int32) |  | 总记录数 |






<a name="jmash-file-JmashFilePage"></a>

### JmashFilePage
分页


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [JmashFileModel](#jmash-file-JmashFileModel) | repeated | 当前页内容 |
| cur_page | [int32](#int32) |  | 当前页码 |
| page_size | [int32](#int32) |  | 页尺寸 |
| total_size | [int32](#int32) |  | 总记录数 |
| sub_total_dto | [JmashFileModelTotal](#jmash-file-JmashFileModelTotal) |  | 本页小计 |
| total_dto | [JmashFileModelTotal](#jmash-file-JmashFileModelTotal) |  | 合计 |






<a name="jmash-file-JmashFileReq"></a>

### JmashFileReq
查询


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| cur_page | [int32](#int32) |  | 当前页码 |
| page_size | [int32](#int32) |  | 页尺寸 |
| order_name | [string](#string) |  | 排序名称 |
| order_asc | [bool](#bool) |  | 是否升序排序 |





 

 

 

 



## Scalar Value Types

| .proto Type | Notes | C++ | Java | Python | Go | C# | PHP | Ruby |
| ----------- | ----- | --- | ---- | ------ | -- | -- | --- | ---- |
| <a name="double" /> double |  | double | double | float | float64 | double | float | Float |
| <a name="float" /> float |  | float | float | float | float32 | float | float | Float |
| <a name="int32" /> int32 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint32 instead. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="int64" /> int64 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint64 instead. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="uint32" /> uint32 | Uses variable-length encoding. | uint32 | int | int/long | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="uint64" /> uint64 | Uses variable-length encoding. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum or Fixnum (as required) |
| <a name="sint32" /> sint32 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int32s. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sint64" /> sint64 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int64s. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="fixed32" /> fixed32 | Always four bytes. More efficient than uint32 if values are often greater than 2^28. | uint32 | int | int | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="fixed64" /> fixed64 | Always eight bytes. More efficient than uint64 if values are often greater than 2^56. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum |
| <a name="sfixed32" /> sfixed32 | Always four bytes. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sfixed64" /> sfixed64 | Always eight bytes. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="bool" /> bool |  | bool | boolean | boolean | bool | bool | boolean | TrueClass/FalseClass |
| <a name="string" /> string | A string must always contain UTF-8 encoded or 7-bit ASCII text. | string | String | str/unicode | string | string | string | String (UTF-8) |
| <a name="bytes" /> bytes | May contain any arbitrary sequence of bytes. | string | ByteString | str | []byte | ByteString | string | String (ASCII-8BIT) |

