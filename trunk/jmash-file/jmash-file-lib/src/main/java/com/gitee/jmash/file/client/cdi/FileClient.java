
package com.gitee.jmash.file.client.cdi;

import com.gitee.jmash.common.grpc.client.GrpcChannel;
import com.gitee.jmash.core.lib.FileClientUtil;
import com.gitee.jmash.core.utils.FilePathUtil;
import com.gitee.jmash.rbac.client.RbacClient;
import com.google.api.HttpBody;
import io.grpc.stub.StreamObserver;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.concurrent.CountDownLatch;
import jmash.basic.FileBasicGrpc;
import jmash.file.FileGrpc;
import jmash.protobuf.DownloadSrcReq;
import jmash.protobuf.ThumbSrcDownloadReq;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/** File Client . */
public class FileClient {

  private static final Log log = LogFactory.getLog(FileClient.class);

  /** 异步文件下载. */
  public static File downloadFile(String fileNames) throws IOException {
    String realFileDir = FilePathUtil.realPath(fileNames,false);
    File file = new File(realFileDir);
    if (file.exists()) {
      return file;
    }
    try {
      // 异步下载文件服务器
      DownloadSrcReq req = DownloadSrcReq.newBuilder().setFileSrc(fileNames).build();
      final CountDownLatch latchFinish = new CountDownLatch(1);
      StreamObserver<HttpBody> resp = FileClientUtil.createHttpBodyResp(file, latchFinish);
      FileClient.getFileBasicStub().downloadFileBySrc(req, resp);
      latchFinish.await();
    } catch (InterruptedException ex) {
      log.error("异步下载文件异常：", ex);
    }
    if (!file.exists()) {
      log.error("找不到文件：" + realFileDir);
    }
    return file;
  }

  /** （小尺寸）同步图片下载. */
  public static BufferedImage downloadImage(ThumbSrcDownloadReq req) throws IOException {
    Iterator<com.google.api.HttpBody> resp =
        FileClient.getFileBasicBlockingStub().downloadThumbBySrc(req);
    return FileClientUtil.downloadImage(resp);
  }

  public static FileGrpc.FileBlockingStub getFileBlockingStub() {
    return FileGrpc.newBlockingStub(FileClientConfig.getManagedChannel())
        .withCallCredentials(GrpcChannel.getCallCredentials());
  }

  public static FileGrpc.FileFutureStub getFileFutureStub() {
    return FileGrpc.newFutureStub(FileClientConfig.getManagedChannel())
        .withCallCredentials(GrpcChannel.getCallCredentials());
  }

  public static FileGrpc.FileStub getFileStub() {
    return FileGrpc.newStub(FileClientConfig.getManagedChannel())
        .withCallCredentials(GrpcChannel.getCallCredentials());
  }

  public static FileBasicGrpc.FileBasicBlockingStub getFileBasicBlockingStub() {
    return FileBasicGrpc.newBlockingStub(FileClientConfig.getManagedChannel())
        .withCallCredentials(GrpcChannel.getCallCredentials());
  }

  public static FileBasicGrpc.FileBasicFutureStub getFileBasicFutureStub() {
    return FileBasicGrpc.newFutureStub(FileClientConfig.getManagedChannel())
        .withCallCredentials(GrpcChannel.getCallCredentials());
  }

  public static FileBasicGrpc.FileBasicStub getFileBasicStub() {
    return FileBasicGrpc.newStub(FileClientConfig.getManagedChannel())
        .withCallCredentials(GrpcChannel.getCallCredentials());
  }

  public static FileBasicGrpc.FileBasicBlockingStub getFileBasicSystemBlockingStub(String tenant) {
    return FileBasicGrpc.newBlockingStub(FileClientConfig.getManagedChannel())
        .withCallCredentials(RbacClient.getSystemCredentials(tenant));
  }

  public static FileBasicGrpc.FileBasicFutureStub getFileBasicSystemFutureStub(String tenant) {
    return FileBasicGrpc.newFutureStub(FileClientConfig.getManagedChannel())
        .withCallCredentials(RbacClient.getSystemCredentials(tenant));
  }

  public static FileBasicGrpc.FileBasicStub getFileBasicSystemStub(String tenant) {
    return FileBasicGrpc.newStub(FileClientConfig.getManagedChannel())
        .withCallCredentials(RbacClient.getSystemCredentials(tenant));
  }


}
