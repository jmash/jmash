package com.gitee.jmash.file.client.cdi;

import com.gitee.jmash.common.grpc.client.GrpcChannel;
import io.grpc.ManagedChannel;

/** File Client Config . */
public class FileClientConfig {

  protected static ManagedChannel channel = null;

  public static synchronized ManagedChannel getManagedChannel() {
    if (null != channel && !channel.isShutdown() && !channel.isTerminated()) {
      return channel;
    }
    // k8s环境获取后端服务,本地环境获取测试服务.
    channel = GrpcChannel.getServiceChannel("jmash-file-service.jmash.svc.cluster.local");
    return channel;
  }

}
