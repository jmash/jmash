package com.gitee.jmash.file;

import com.gitee.jmash.file.service.JmashFileRead;
import com.gitee.jmash.file.service.JmashFileWrite;
import jakarta.enterprise.inject.spi.CDI;

/**
 * 模块服务工厂类.
 *
 * @author CGD
 *
 */
public class FileFactory {

  /** 销毁CDI. */
  public static void destroy(Object instance) {
    if (null != instance) {
      CDI.current().destroy(instance);
    }
  }

  public static JmashFileRead getJmashFileRead(String tenant) {
    return CDI.current().select(JmashFileRead.class).get().setTenant(tenant);
  }

  public static JmashFileWrite getJmashFileWrite(String tenant) {
    return CDI.current().select(JmashFileWrite.class).get().setTenant(tenant);
  }

}
