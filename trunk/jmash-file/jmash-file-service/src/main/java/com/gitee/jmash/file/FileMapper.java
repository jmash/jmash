
package com.gitee.jmash.file;

import com.crenjoy.proto.mapper.BeanMapper;
import com.crenjoy.proto.mapper.ProtoMapper;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * File Mapper.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 *
 */
@Mapper
public interface FileMapper extends BeanMapper, ProtoMapper {

  FileMapper INSTANCE = Mappers.getMapper(FileMapper.class);

}
