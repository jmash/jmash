
package com.gitee.jmash.file.cdi;

import com.gitee.jmash.core.grpc.DefaultGrpcServer;
import com.gitee.jmash.core.orm.module.DbBuild;
import com.gitee.jmash.rbac.client.shiro.JmashClientShiroConfig;
import com.luciad.imageio.webp.WebP;
import jakarta.enterprise.inject.se.SeContainer;
import jakarta.enterprise.inject.se.SeContainerInitializer;

/** 项目入口. */
public class FileApp {

  /** 启动服务. */
  public static void main(String[] args) throws Exception {
    try (SeContainer container = SeContainerInitializer.newInstance().initialize()) {
      // 构建数据库.
      DbBuild dbBuild = container.select(DbBuild.class).get();
      dbBuild.build();
      JmashClientShiroConfig.config();
      // Webp加载
      WebP.loadNativeLibrary();
      // 启动服务
      final DefaultGrpcServer server = container.select(DefaultGrpcServer.class).get();
      server.start(false);
      server.blockUntilShutdown();
    }
  }

}
