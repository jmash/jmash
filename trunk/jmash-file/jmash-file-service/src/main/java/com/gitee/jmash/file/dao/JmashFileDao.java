
package com.gitee.jmash.file.dao;

import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.orm.jpa.BaseDao;
import com.gitee.jmash.core.orm.jpa.SqlBuilder;
import com.gitee.jmash.core.orm.jpa.TenantEntityManager;
import com.gitee.jmash.file.entity.JmashFileEntity;
import com.gitee.jmash.file.model.JmashFileTotal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import jmash.file.protobuf.JmashFileReq;
import jmash.protobuf.FileHash;
import org.apache.commons.lang3.StringUtils;

/**
 * JmashFile实体的Dao层（使用JPA实现）.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public class JmashFileDao extends BaseDao<JmashFileEntity, UUID> {

  public JmashFileDao() {
    super();
  }

  public JmashFileDao(TenantEntityManager tem) {
    super(tem);
  }

  /**
   * 通过Hash 查询文件.
   *
   * @param fileHash
   * @return
   */
  public JmashFileEntity findByHash(FileHash fileHash) {
    StringBuffer sql = new StringBuffer(
        "select s from JmashFileEntity s where s.fileSize=?1 and s.hashSm3 = ?2 and s.hashSha256 = ?3 ");
    return this.findSingle(sql.toString(), fileHash.getFileSize(),
        fileHash.getHashSm3().toByteArray(), fileHash.getHashSha256().toByteArray());
  }


  /**
   * 综合查询.
   */
  public List<JmashFileEntity> findListByReq(JmashFileReq req) {
    SqlBuilder sqlBuilder = createSql(req);
    String query = sqlBuilder.getQuerySql("select s ");
    return this.findListByParams(query, sqlBuilder.getParams());
  }

  /**
   * 综合查询Page.
   */
  public DtoPage<JmashFileEntity, JmashFileTotal> findPageByReq(JmashFileReq req) {
    SqlBuilder sqlBuilder = createSql(req);
    String query = sqlBuilder.getQuerySql("select s ");
    String totalQuery = sqlBuilder.getTotalSql("select count(s) as totalSize ");
    return this.findDtoPageByParams(req.getCurPage(), req.getPageSize(), query, totalQuery,
        JmashFileTotal.class, sqlBuilder.getParams());
  }

  /** Create SQL By Req . */
  public SqlBuilder createSql(JmashFileReq req) {
    StringBuilder sql = new StringBuilder(" from JmashFileEntity s where 1=1  ");
    Map<String, Object> params = new HashMap<String, Object>();
    String orderSql = "";
    if (StringUtils.isNotBlank(req.getOrderName())) {
      orderSql = String.format(" order by %s %s ", req.getOrderName(),
          req.getOrderAsc() ? " asc " : " desc ");
    }
    return SqlBuilder.build().setSql(sql).setParams(params).setOrderSql(orderSql);
  }
}
