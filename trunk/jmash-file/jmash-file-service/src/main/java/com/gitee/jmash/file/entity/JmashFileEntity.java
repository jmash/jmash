
package com.gitee.jmash.file.entity;


import com.google.protobuf.ByteString;
import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;
import jmash.protobuf.FileHash;


/**
 * 本代码为自动生成工具生成 表 .
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Entity
@Table(name = "os_jmash_file")
public class JmashFileEntity implements Serializable {

  private static final long serialVersionUID = 1L;

  /** 文件ID. */
  private UUID fileId = UUID.randomUUID();
  /** 文件存储路径. */
  private String fileSrc;
  /** 文件内容类型. */
  private String contentType;
  /** 文件后缀. */
  private String fileExt;
  /** 文件大小. */
  private Long fileSize;
  /** 文件Hash SM3值. */
  private byte[] hashSm3;
  /** 文件Hash SHA256值. */
  private byte[] hashSha256;
  /** 图片/视频宽度. */
  private Integer fileWidth;
  /** 图片/视频高度. */
  private Integer fileHeight;
  /** 音频/视频时长. */
  private Long fileTime;
  /** 图片/视频/音频拍摄位置. */
  private String fileLocation;
  /** 经度. */
  private BigDecimal geoLongitude;
  /** 纬度. */
  private BigDecimal geoLatitude;
  /** 精度. */
  private Integer geoAccuracy;
  /** 海拔高度. */
  private Long geoAltitude;
  /** 海拔高度精度. */
  private Integer geoAltitudeAccracy;
  /** 创建时间. */
  private LocalDateTime createDate;

  public static FileHash.Builder createHash(Long fileSize, byte[] hashSm3, byte[] hashSha256) {
    return FileHash.newBuilder().setHashSm3(ByteString.copyFrom(hashSm3)).setFileSize(fileSize)
        .setHashSha256(ByteString.copyFrom(hashSha256));
  }

  /**
   * 默认构造函数.
   */
  public JmashFileEntity() {
    super();
  }

  @Transient
  @JsonbTransient
  public FileHash.Builder getFileHash() {
    return FileHash.newBuilder().setHashSm3(ByteString.copyFrom(this.getHashSm3()))
        .setFileSize(this.getFileSize()).setHashSha256(ByteString.copyFrom(this.getHashSha256()));
  }

  public void setFileHash(FileHash fileHash) {
    this.setFileSize(fileHash.getFileSize());
    this.setHashSm3(fileHash.getHashSm3().toByteArray());
    this.setHashSha256(fileHash.getHashSha256().toByteArray());
  }

  /** 实体主键. */
  @Transient
  @JsonbTransient
  public UUID getEntityPk() {
    return this.fileId;
  }

  /** 实体主键. */
  public void setEntityPk(UUID pk) {
    this.fileId = pk;
  }

  /** 文件ID(FileId). */
  @Id
  @Column(name = "file_id", nullable = false, columnDefinition = "BINARY(16)")
  public UUID getFileId() {
    return fileId;
  }

  /** 文件ID(FileId). */
  public void setFileId(UUID fileId) {
    this.fileId = fileId;
  }

  /** 文件存储路径(FileSrc). */
  @Column(name = "file_src")
  public String getFileSrc() {
    return fileSrc;
  }

  /** 文件存储路径(FileSrc). */
  public void setFileSrc(String fileSrc) {
    this.fileSrc = fileSrc;
  }

  /** 文件内容类型(ContentType). */
  @Column(name = "content_type")
  public String getContentType() {
    return contentType;
  }

  /** 文件内容类型(ContentType). */
  public void setContentType(String contentType) {
    this.contentType = contentType;
  }

  /** 文件后缀(FileExt). */
  @Column(name = "file_ext")
  public String getFileExt() {
    return fileExt;
  }

  /** 文件后缀(FileExt). */
  public void setFileExt(String fileExt) {
    this.fileExt = fileExt;
  }

  /** 文件大小(FileSize). */
  @Column(name = "file_size")
  public Long getFileSize() {
    return fileSize;
  }

  /** 文件大小(FileSize). */
  public void setFileSize(Long fileSize) {
    this.fileSize = fileSize;
  }

  /** 文件Hash SM3值(HashSm3). */
  @Column(name = "hash_sm3")
  public byte[] getHashSm3() {
    return hashSm3;
  }

  /** 文件Hash SM3值(HashSm3). */
  public void setHashSm3(byte[] hashSm3) {
    this.hashSm3 = hashSm3;
  }

  /** 文件Hash SHA256值(HashSha256). */
  @Column(name = "hash_sha256")
  public byte[] getHashSha256() {
    return hashSha256;
  }

  /** 文件Hash SHA256值(HashSha256). */
  public void setHashSha256(byte[] hashSha256) {
    this.hashSha256 = hashSha256;
  }

  /** 图片/视频宽度(FileWidth). */
  @Column(name = "file_width")
  public Integer getFileWidth() {
    return fileWidth;
  }

  /** 图片/视频宽度(FileWidth). */
  public void setFileWidth(Integer fileWidth) {
    this.fileWidth = fileWidth;
  }

  /** 图片/视频高度(FileHeight). */
  @Column(name = "file_height")
  public Integer getFileHeight() {
    return fileHeight;
  }

  /** 图片/视频高度(FileHeight). */
  public void setFileHeight(Integer fileHeight) {
    this.fileHeight = fileHeight;
  }

  /** 音频/视频时长(FileTime). */
  @Column(name = "file_time")
  public Long getFileTime() {
    return fileTime;
  }

  /** 音频/视频时长(FileTime). */
  public void setFileTime(Long fileTime) {
    this.fileTime = fileTime;
  }

  /** 图片/视频/音频拍摄位置(FileLocation). */
  @Column(name = "file_location")
  public String getFileLocation() {
    return fileLocation;
  }

  /** 图片/视频/音频拍摄位置(FileLocation). */
  public void setFileLocation(String fileLocation) {
    this.fileLocation = fileLocation;
  }

  /** 经度(GeoLongitude). */
  @Column(name = "geo_longitude")
  public BigDecimal getGeoLongitude() {
    return geoLongitude;
  }

  /** 经度(GeoLongitude). */
  public void setGeoLongitude(BigDecimal geoLongitude) {
    this.geoLongitude = geoLongitude;
  }

  /** 纬度(GeoLatitude). */
  @Column(name = "geo_latitude")
  public BigDecimal getGeoLatitude() {
    return geoLatitude;
  }

  /** 纬度(GeoLatitude). */
  public void setGeoLatitude(BigDecimal geoLatitude) {
    this.geoLatitude = geoLatitude;
  }

  /** 精度(GeoAccuracy). */
  @Column(name = "geo_accuracy")
  public Integer getGeoAccuracy() {
    return geoAccuracy;
  }

  /** 精度(GeoAccuracy). */
  public void setGeoAccuracy(Integer geoAccuracy) {
    this.geoAccuracy = geoAccuracy;
  }

  /** 海拔高度(GeoAltitude). */
  @Column(name = "geo_altitude")
  public Long getGeoAltitude() {
    return geoAltitude;
  }

  /** 海拔高度(GeoAltitude). */
  public void setGeoAltitude(Long geoAltitude) {
    this.geoAltitude = geoAltitude;
  }

  /** 海拔高度精度(GeoAltitudeAccracy). */
  @Column(name = "geo_altitude_accracy")
  public Integer getGeoAltitudeAccracy() {
    return geoAltitudeAccracy;
  }

  /** 海拔高度精度(GeoAltitudeAccracy). */
  public void setGeoAltitudeAccracy(Integer geoAltitudeAccracy) {
    this.geoAltitudeAccracy = geoAltitudeAccracy;
  }

  /** 创建时间(CreateDate). */
  @Column(name = "create_date")
  public LocalDateTime getCreateDate() {
    return createDate;
  }

  /** 创建时间(CreateDate). */
  public void setCreateDate(LocalDateTime createDate) {
    this.createDate = createDate;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((fileId == null) ? 0 : fileId.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final JmashFileEntity other = (JmashFileEntity) obj;
    if (fileId == null) {
      if (other.fileId != null) {
        return false;
      }
    } else if (!fileId.equals(other.fileId)) {
      return false;
    }
    return true;
  }

}
