package com.gitee.jmash.file.grpc;

import com.gitee.jmash.common.config.FileProps;
import com.gitee.jmash.common.enums.MimeType;
import com.gitee.jmash.common.image.Thumbnail.WebThumbType;
import com.gitee.jmash.common.utils.UUIDUtil;
import com.gitee.jmash.common.utils.VersionUtil;
import com.gitee.jmash.core.grpc.cdi.GrpcService;
import com.gitee.jmash.core.grpc.file.FileRangeUtil;
import com.gitee.jmash.core.utils.FilePathUtil;
import com.gitee.jmash.core.utils.FileServiceUtil;
import com.gitee.jmash.file.FileFactory;
import com.gitee.jmash.file.entity.JmashFileEntity;
import com.gitee.jmash.file.mapper.JmashFileMapper;
import com.gitee.jmash.file.service.JmashFileRead;
import com.gitee.jmash.file.service.JmashFileWrite;
import com.gitee.jmash.file.util.ImageBase64;
import com.gitee.jmash.file.util.MinioFileUtil;
import com.google.api.HttpBody;
import com.google.protobuf.BoolValue;
import com.google.protobuf.ByteString;
import com.google.protobuf.Empty;
import com.google.protobuf.StringValue;
import io.grpc.Status;
import io.grpc.stub.StreamObserver;
import jakarta.inject.Inject;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import jmash.basic.FileBasicGrpc;
import jmash.protobuf.DownloadReq;
import jmash.protobuf.DownloadSrcReq;
import jmash.protobuf.FileBase64Req;
import jmash.protobuf.FileHash;
import jmash.protobuf.FileHashHex;
import jmash.protobuf.FileInfo;
import jmash.protobuf.FileRangeReq;
import jmash.protobuf.FileRangeResp;
import jmash.protobuf.FileSrcReq;
import jmash.protobuf.FileUploadReq;
import jmash.protobuf.FileWebUploadReq;
import jmash.protobuf.ThumbDownloadReq;
import jmash.protobuf.ThumbSrcDownloadReq;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.eclipse.microprofile.config.inject.ConfigProperties;

/** 基本文件服务. */
@GrpcService
public class FileBasicImpl extends FileBasicGrpc.FileBasicImplBase {

  private Log log = LogFactory.getLog(FileBasicImpl.class);

  // 模块版本
  public static String version = "v1.0.0";

  @Inject
  @ConfigProperties
  private FileProps fileProps;

  @Override
  public void version(Empty request, StreamObserver<StringValue> responseObserver) {
    responseObserver
        .onNext(StringValue.of(version + "-" + VersionUtil.snapshot(FileBasicImpl.class)));
    responseObserver.onCompleted();
  }

  @Override
  @RequiresAuthentication
  public StreamObserver<FileUploadReq> uploadFile(StreamObserver<FileInfo> responseObserver) {
    return new StreamObserver<FileUploadReq>() {

      String tenant;
      String contentType;

      String fileSrc;
      File file;

      @Override
      public void onNext(FileUploadReq req) {
        boolean verify = FileServiceUtil.uploadVerify(req.getFileSize(), req.getFileName(),
            req.getContentType(), responseObserver, fileProps);
        if (!verify) {
          return;
        }
        tenant = req.getTenant();
        contentType = req.getContentType();
        if (null == file) {
          fileSrc = FilePathUtil.createNewFile(req.getFileName(), tenant, req.getFileDir());
          file = new File(FilePathUtil.realPath(fileSrc, true));
          if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
          }
        }
        try (FileOutputStream output = new FileOutputStream(file, true)) {
          output.write(req.getBody().toByteArray());
        } catch (Exception e) {
          log.error("Error:", e);
        }
      }

      @Override
      public void onError(Throwable t) {
        log.error("Error:", t);
        try {
          FileUtils.forceDelete(file);
        } catch (IOException e) {
          log.error("Error:", e);
        }
      }

      @Override
      public void onCompleted() {
        try (JmashFileWrite jmashFileWrite = FileFactory.getJmashFileWrite(tenant)) {
          FileInfo fileInfo = jmashFileWrite.saveCloudFile(fileSrc, contentType, file);
          responseObserver.onNext(fileInfo);
          responseObserver.onCompleted();
        } catch (Exception ex) {
          log.error("Error:", ex);
          responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
        }
      }
    };
  }

  @Override
  @RequiresAuthentication
  public void uploadFileWeb(FileWebUploadReq req, StreamObserver<FileInfo> responseObserver) {
    File file = FileServiceUtil.uploadFileWebSave(req, responseObserver, fileProps);
    if (null == file) {
      responseObserver
          .onError(Status.NOT_FOUND.withDescription("找不到文件：" + req.getFileName()).asException());
      return;
    }
    if (FileServiceUtil.filePart(req.getFileSize()) == req.getFilePart()) {
      String fileSrc =
          FilePathUtil.createNewFile(req.getFileName(), req.getTenant(), req.getFileDir());
      try (JmashFileWrite jmashFileWrite = FileFactory.getJmashFileWrite(req.getTenant())) {
        FileInfo fileInfo = jmashFileWrite.saveCloudFile(fileSrc, req.getContentType(), file);
        responseObserver.onNext(fileInfo);
        responseObserver.onCompleted();
      } catch (Exception ex) {
        responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
      }
    } else {
      responseObserver.onError(Status.INVALID_ARGUMENT.withDescription("数据传输异常").asException());
    }
  }

  @Override
  @RequiresAuthentication
  public void uploadBase64File(FileBase64Req request, StreamObserver<FileInfo> responseObserver) {
    // 确定文件类型
    String fileExt = MimeType.getExtension(request.getFileName()).toLowerCase();
    MimeType mimeType = ImageBase64.mimeType(request.getBase64());
    if (null == mimeType) {
      mimeType = MimeType.convert(fileExt);
    } else {
      fileExt = mimeType.getFileExt();
    }
    if (null == mimeType) {
      String message = String.format("服务器不支持该文件类型，后缀为%s的文件。", fileExt);
      responseObserver.onError(Status.PERMISSION_DENIED.withDescription(message).asException());
      return;
    }

    // 获取是否已有云文件
    String base64 = ImageBase64.base64(request.getBase64());
    // Base64 File
    String fileSrc = FilePathUtil.createNewFile(request.getFileName(), request.getTenant(),
        request.getFileDir());
    ImageBase64.base64ToFile(base64, FilePathUtil.realPath(fileSrc, true));
    File file = new File(FilePathUtil.realPath(fileSrc, true));

    try (JmashFileWrite jmashFileWrite = FileFactory.getJmashFileWrite(request.getTenant())) {
      FileInfo fileInfo = jmashFileWrite.saveCloudFile(fileSrc, request.getContentType(), file);
      responseObserver.onNext(fileInfo);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("Error:", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  public void existFile(FileHash request, StreamObserver<FileInfo> responseObserver) {
    try (JmashFileRead jmashFileRead = FileFactory.getJmashFileRead(request.getTenant())) {
      JmashFileEntity entity = jmashFileRead.findByHash(request);
      if (null != entity) {
        String fileName = FilePathUtil.realPath(entity.getFileSrc(), false);
        File file = new File(fileName);
        if (!file.exists()) {
          log.error("服务器文件丢失:" + file.getName());
          responseObserver
              .onError(Status.NOT_FOUND.withDescription("服务器文件丢失：" + file.getName()).asException());
          return;
        }
      }
      if (null != entity) {
        FileInfo fileInfo = JmashFileMapper.INSTANCE.toFileInfo(entity);
        responseObserver.onNext(fileInfo);
        responseObserver.onCompleted();
      } else {
        responseObserver
            .onError(Status.NOT_FOUND.withDescription("不存在该文件或该文件第一次上传，请耐心等待...").asException());
      }
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  /** FileHash Hex. */
  @Override
  public void existFileHex(FileHashHex req, StreamObserver<FileInfo> responseObserver) {
    try {
      FileHash request = FileHash.newBuilder().setTenant(req.getTenant())
          .setFileSize(req.getFileSize()).setHashSm3(ByteString.fromHex(req.getHashSm3()))
          .setHashSha256(ByteString.fromHex(req.getHashSha256())).build();
      this.existFile(request, responseObserver);
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }


  @Override
  public void downloadFile(DownloadReq request, StreamObserver<HttpBody> responseObserver) {
    try (JmashFileRead jmashFileRead = FileFactory.getJmashFileRead(request.getTenant())) {
      JmashFileEntity cloudFile = jmashFileRead.findById(UUIDUtil.fromString(request.getFileId()));
      if (null == cloudFile) {
        responseObserver.onError(
            Status.NOT_FOUND.withDescription("找不到该UUID文件:" + request.getFileId()).asException());
        return;
      }
      String fileName = FilePathUtil.realPath(cloudFile.getFileSrc(), false);
      FileServiceUtil.downloadFile(responseObserver, new File(fileName),
          cloudFile.getContentType());
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  public void downloadFileBySrc(DownloadSrcReq request, StreamObserver<HttpBody> responseObserver) {
    try {
      // 防止越过本参数访问其他目录
      if (request.getFileSrc().contains("..")) {
        responseObserver.onError(
            Status.PERMISSION_DENIED.withDescription("没有权限:" + request.getFileSrc()).asException());
        return;
      }
      String fileName = FilePathUtil.realPath(request.getFileSrc(), false);
      String fileExt = MimeType.getExtension(fileName).toLowerCase();
      MimeType mimeType = MimeType.convert(fileExt);
      MinioFileUtil.downloadFile(mimeType.getMimeType(), request.getFileSrc());
      FileServiceUtil.downloadFile(responseObserver, new File(fileName), mimeType.getMimeType());
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  public void downloadThumb(ThumbDownloadReq request, StreamObserver<HttpBody> responseObserver) {
    try (JmashFileRead jmashFileRead = FileFactory.getJmashFileRead(request.getTenant())) {
      JmashFileEntity cloudFile = jmashFileRead.findById(UUIDUtil.fromString(request.getFileId()));
      if (null == cloudFile) {
        responseObserver.onError(
            Status.NOT_FOUND.withDescription("找不到该UUID文件:" + request.getFileId()).asException());
        return;
      }
      WebThumbType thumbType = WebThumbType.valueOf(request.getThumbType().name());
      String distSrc = FileServiceUtil.thumb(cloudFile.getFileSrc(), thumbType, request.getWidth(),
          request.getHeight());
      String distFile = FilePathUtil.realPath(distSrc, false);
      String contentType = WebThumbType.trans.equals(thumbType) ? MimeType.M_png.getMimeType()
          : cloudFile.getContentType();
      FileServiceUtil.downloadFile(responseObserver, new File(distFile), contentType);
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  public void downloadThumbBySrc(ThumbSrcDownloadReq request,
      StreamObserver<HttpBody> responseObserver) {
    try {
      // 防止越过本参数访问其他目录
      if (request.getFileSrc().contains("..")) {
        responseObserver.onError(
            Status.PERMISSION_DENIED.withDescription("没有权限:" + request.getFileSrc()).asException());
        return;
      }

      String fileExt = MimeType.getExtension(request.getFileSrc()).toLowerCase();
      MimeType mimeType = MimeType.convert(fileExt);
      if (null == mimeType) {
        responseObserver.onError(Status.PERMISSION_DENIED
            .withDescription("不支持未知文件类型!" + request.getFileSrc()).asException());
        return;
      }

      MinioFileUtil.downloadFile(mimeType.getMimeType(), request.getFileSrc());
      WebThumbType thumbType = WebThumbType.valueOf(request.getThumbType().name());
      mimeType = WebThumbType.trans.equals(thumbType) ? MimeType.M_png : mimeType;
      String distSrc = FileServiceUtil.thumb(request.getFileSrc(), thumbType, request.getWidth(),
          request.getHeight());
      String distFile = FilePathUtil.realPath(distSrc, false);
      FileServiceUtil.downloadFile(responseObserver, new File(distFile), mimeType.getMimeType());
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresAuthentication
  public void removeFile(FileSrcReq request, StreamObserver<BoolValue> responseObserver) {
    try {
      // 防止越过本参数访问其他目录
      if (request.getFileSrc().contains("..")) {
        responseObserver.onError(
            Status.PERMISSION_DENIED.withDescription("没有权限:" + request.getFileSrc()).asException());
        return;
      }
      File file = new File(FilePathUtil.realPath(request.getFileSrc(), false));
      if (file.exists()) {
        FileUtils.forceDelete(file);
        responseObserver.onNext(BoolValue.of(true));
      } else {
        responseObserver.onNext(BoolValue.of(false));
      }
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  public void downloadFileRange(FileRangeReq request,
      StreamObserver<FileRangeResp> responseObserver) {
    try {
      FileRangeUtil.downloadFile(request, responseObserver, true);
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

}
