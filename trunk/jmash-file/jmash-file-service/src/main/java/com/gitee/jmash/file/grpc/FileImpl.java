
package com.gitee.jmash.file.grpc;

import com.gitee.jmash.common.utils.UUIDUtil;
import com.gitee.jmash.core.grpc.cdi.GrpcService;
import com.gitee.jmash.core.lib.ProtoEnumUtil;
import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.file.FileFactory;
import com.gitee.jmash.file.entity.JmashFileEntity;
import com.gitee.jmash.file.mapper.JmashFileMapper;
import com.gitee.jmash.file.model.JmashFileTotal;
import com.gitee.jmash.file.service.JmashFileRead;
import com.gitee.jmash.file.service.JmashFileWrite;
import com.google.protobuf.EnumValue;
import com.google.protobuf.Int32Value;
import com.google.protobuf.StringValue;
import io.grpc.Status;
import io.grpc.stub.StreamObserver;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import jmash.file.protobuf.JmashFileKey;
import jmash.file.protobuf.JmashFileKeyList;
import jmash.file.protobuf.JmashFileList;
import jmash.file.protobuf.JmashFileModel;
import jmash.file.protobuf.JmashFilePage;
import jmash.file.protobuf.JmashFileReq;
import jmash.protobuf.CustomEnumValue;
import jmash.protobuf.CustomEnumValueMap;
import jmash.protobuf.EnumValueList;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;


@GrpcService
public class FileImpl extends jmash.file.FileGrpc.FileImplBase {

  private static Log log = LogFactory.getLog(FileImpl.class);

  @Override
  public void findEnumList(StringValue request, StreamObserver<EnumValueList> responseObserver) {
    try {
      List<EnumValue> list = ProtoEnumUtil.getEnumList(request.getValue());
      responseObserver.onNext(EnumValueList.newBuilder().addAllValues(list).build());
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  public void findEnumMap(StringValue request,
      StreamObserver<CustomEnumValueMap> responseObserver) {
    try {
      Map<Integer, CustomEnumValue> values = ProtoEnumUtil.getEnumMap(request.getValue());
      responseObserver.onNext(CustomEnumValueMap.newBuilder().putAllValues(values).build());
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }


  @Override
  @RequiresPermissions("file:jmash_file:list")
  public void findJmashFilePage(JmashFileReq request,
      StreamObserver<JmashFilePage> responseObserver) {
    try (JmashFileRead jmashFileRead = FileFactory.getJmashFileRead(request.getTenant())) {
      DtoPage<JmashFileEntity, JmashFileTotal> page = jmashFileRead.findPageByReq(request);
      JmashFilePage modelPage = JmashFileMapper.INSTANCE.pageJmashFile(page);
      responseObserver.onNext(modelPage);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }


  @Override
  @RequiresPermissions("file:jmash_file:list")
  public void findJmashFileList(JmashFileReq request,
      StreamObserver<JmashFileList> responseObserver) {
    try (JmashFileRead jmashFileRead = FileFactory.getJmashFileRead(request.getTenant())) {
      List<JmashFileEntity> list = jmashFileRead.findListByReq(request);
      List<JmashFileModel> modelList = JmashFileMapper.INSTANCE.listJmashFile(list);
      responseObserver.onNext(JmashFileList.newBuilder().addAllResults(modelList).build());
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresAuthentication
  public void findJmashFileById(JmashFileKey request,
      StreamObserver<JmashFileModel> responseObserver) {
    try (JmashFileRead jmashFileRead = FileFactory.getJmashFileRead(request.getTenant())) {
      JmashFileEntity entity = jmashFileRead.findById(UUIDUtil.fromString(request.getFileId()));
      JmashFileModel model = JmashFileMapper.INSTANCE.model(entity);
      responseObserver.onNext(model);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }


  @Override
  @RequiresPermissions("file:jmash_file:delete")
  public void deleteJmashFile(JmashFileKey request,
      StreamObserver<JmashFileModel> responseObserver) {
    try (JmashFileWrite jmashFileWrite = FileFactory.getJmashFileWrite(request.getTenant())) {
      JmashFileEntity entity = jmashFileWrite.delete(UUIDUtil.fromString(request.getFileId()));
      JmashFileModel model = JmashFileMapper.INSTANCE.model(entity);
      responseObserver.onNext(model);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("file:jmash_file:delete")
  public void batchDeleteJmashFile(JmashFileKeyList request,
      StreamObserver<Int32Value> responseObserver) {
    try (JmashFileWrite jmashFileWrite = FileFactory.getJmashFileWrite(request.getTenant())) {
      final List<String> list = request.getFileIdList();
      final Set<UUID> set =
          list.stream().map(v -> UUIDUtil.fromString(v)).collect(Collectors.toSet());

      int r = jmashFileWrite.batchDelete(set);
      responseObserver.onNext(Int32Value.of(r));
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }
}
