
package com.gitee.jmash.file.mapper;

import com.crenjoy.proto.mapper.BeanMapper;
import com.crenjoy.proto.mapper.ProtoMapper;
import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.file.entity.JmashFileEntity;
import com.gitee.jmash.file.model.JmashFileTotal;
import java.util.List;
import jmash.file.protobuf.JmashFileModel;
import jmash.file.protobuf.JmashFilePage;
import jmash.protobuf.FileInfo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * JmashFileMapper.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Mapper
public interface JmashFileMapper extends BeanMapper,ProtoMapper {

  JmashFileMapper INSTANCE = Mappers.getMapper(JmashFileMapper.class);
  

  List<JmashFileModel> listJmashFile(List<JmashFileEntity> list);

  JmashFilePage pageJmashFile(DtoPage<JmashFileEntity, JmashFileTotal> page);
    
  JmashFileModel model(JmashFileEntity entity);
  
  JmashFileEntity clone(JmashFileEntity entity);
  
  FileInfo toFileInfo(JmashFileEntity entity);
  
  
}
