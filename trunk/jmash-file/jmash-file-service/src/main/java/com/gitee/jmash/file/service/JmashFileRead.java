package com.gitee.jmash.file.service;

import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.file.entity.JmashFileEntity;
import com.gitee.jmash.file.model.JmashFileTotal;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.io.File;
import java.util.List;
import java.util.UUID;
import jmash.file.protobuf.JmashFileReq;
import jmash.protobuf.FileHash;

 /**
 *  os_jmash_file服务Read接口.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public interface JmashFileRead extends TenantService {

  /** 根据主键查询. */
  public JmashFileEntity findById(@NotNull UUID entityId);

  /** 查询页信息. */
  public DtoPage<JmashFileEntity,JmashFileTotal> findPageByReq(@NotNull @Valid JmashFileReq req);

  /** 综合查询. */
  public List<JmashFileEntity> findListByReq(@NotNull @Valid JmashFileReq req);
  
  /** JMash Find By File .*/
  public JmashFileEntity findByFile(File file);

  /** JMash Find By File Hash.*/
  public JmashFileEntity findByHash(FileHash fileHash);
  
}