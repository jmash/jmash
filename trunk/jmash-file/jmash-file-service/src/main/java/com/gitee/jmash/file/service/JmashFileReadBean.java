
package com.gitee.jmash.file.service;

import com.gitee.jmash.common.utils.FileHashUtil;
import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.orm.cdi.JpaTenantService;
import com.gitee.jmash.core.orm.jpa.TenantEntityManager;
import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.core.transaction.JakartaTransaction;
import com.gitee.jmash.crypto.digests.SM3Util;
import com.gitee.jmash.file.dao.JmashFileDao;
import com.gitee.jmash.file.entity.JmashFileEntity;
import com.gitee.jmash.file.model.JmashFileTotal;
import jakarta.enterprise.inject.Typed;
import jakarta.enterprise.inject.spi.CDI;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import jakarta.transaction.Transactional.TxType;
import jakarta.validation.executable.ValidateOnExecution;
import java.io.File;
import java.util.List;
import java.util.UUID;
import jmash.file.protobuf.JmashFileReq;
import jmash.protobuf.FileHash;

/**
 * $Id$
 * 
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 * @version $Revision$
 */
@Typed(JmashFileRead.class)
@Transactional(TxType.SUPPORTS)
@JpaTenantService
@ValidateOnExecution
public class JmashFileReadBean implements JmashFileRead, JakartaTransaction {

  // private static Log log = LogFactory.getLog(JmashFileReadBean.class);

  protected TenantEntityManager tem = new TenantEntityManager();

  protected JmashFileDao jmashFileDao = new JmashFileDao(tem);

  @PersistenceContext(unitName = "ReadFile")
  public void setEntityManager(EntityManager entityManager) {
    this.tem.setEntityManager(entityManager, false);
  }

  @Override
  public EntityManager getEntityManager() {
    return this.tem.getEntityManager();
  }

  @Override
  @SuppressWarnings("unchecked")
  public <T extends TenantService> T setTenant(String tenant) {
    this.tem.setTenant(tenant);
    return (T) this;
  }
  
  @Override
  public void setTenantOnly(String tenant) {
     this.tem.setTenantOnly(tenant);
  }

  @Override
  public String getTenant() {
    return this.tem.getTenant();
  }

  @Override
  public JmashFileEntity findById(UUID entityId) {
    return jmashFileDao.find(entityId);
  }


  @Override
  public DtoPage<JmashFileEntity, JmashFileTotal> findPageByReq(JmashFileReq req) {
    return jmashFileDao.findPageByReq(req);
  }

  @Override
  public List<JmashFileEntity> findListByReq(JmashFileReq req) {
    return jmashFileDao.findListByReq(req);
  }

  @Override
  public JmashFileEntity findByFile(File file) {
    List<byte[]> hashArg = FileHashUtil.getFileHashByte(file,
        new String[] {SM3Util.get().getMessageDigest().getAlgorithm(), FileHashUtil.SHA256});
    FileHash fileHash =
        JmashFileEntity.createHash(file.length(), hashArg.get(0), hashArg.get(1)).build();
    JmashFileEntity entity = findByHash(fileHash);
    return entity;
  }

  @Override
  public JmashFileEntity findByHash(FileHash fileHash) {
    return jmashFileDao.findByHash(fileHash);
  }

  @Override
  public void close() throws Exception {
    CDI.current().destroy(this);
  }

}
