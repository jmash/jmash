
package com.gitee.jmash.file.service;

import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.file.entity.JmashFileEntity;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.io.File;
import java.io.IOException;
import java.util.Set;
import java.util.UUID;
import jmash.protobuf.FileInfo;

/**
 * JMash File os_jmash_file服务Write接口 $Id$.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 * @version $Revision$
 */
public interface JmashFileWrite extends TenantService {

  /**
   * JMash Create Or Find.
   */
  public JmashFileEntity createOrFind(String fileSrc, String contentType, File file);

  /** 保存云文件. */
  public FileInfo saveCloudFile(String fileSrc, String contentType, File file) throws IOException;

  /** 根据主键删除. */
  public JmashFileEntity delete(@NotNull UUID entityId) throws IOException;

  /** 根据主键数组删除. */
  public Integer batchDelete(@NotNull @Valid Set<@NotNull UUID> entityIds);

}
