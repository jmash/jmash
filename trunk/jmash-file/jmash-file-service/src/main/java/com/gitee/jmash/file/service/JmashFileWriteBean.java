
package com.gitee.jmash.file.service;

import com.gitee.jmash.common.enums.FileType;
import com.gitee.jmash.common.enums.MimeType;
import com.gitee.jmash.common.utils.FileHashUtil;
import com.gitee.jmash.core.orm.cdi.JpaTenantService;
import com.gitee.jmash.core.transaction.JakartaTransaction;
import com.gitee.jmash.core.utils.FilePathUtil;
import com.gitee.jmash.crypto.digests.SM3Util;
import com.gitee.jmash.file.entity.JmashFileEntity;
import com.gitee.jmash.file.mapper.JmashFileMapper;
import com.gitee.jmash.file.util.MediaUtil;
import jakarta.enterprise.inject.Typed;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import jakarta.transaction.Transactional.TxType;
import jakarta.validation.executable.ValidateOnExecution;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import jmash.protobuf.FileHash;
import jmash.protobuf.FileInfo;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * $Id$.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 * @version $Revision$
 */
@Typed(JmashFileWrite.class)
@Transactional(TxType.REQUIRED)
@JpaTenantService
@ValidateOnExecution
public class JmashFileWriteBean extends JmashFileReadBean
    implements JmashFileWrite, JakartaTransaction {

  private static Log log = LogFactory.getLog(JmashFileWriteBean.class);

  @Override
  @PersistenceContext(unitName = "WriteFile")
  public void setEntityManager(EntityManager entityManager) {
    this.tem.setEntityManager(entityManager, true);
  }

  @Override
  public JmashFileEntity createOrFind(String fileSrc, String contentType, File file) {
    List<byte[]> hashArg = FileHashUtil.getFileHashByte(file,
        new String[] {SM3Util.get().getMessageDigest().getAlgorithm(), FileHashUtil.SHA256});
    FileHash fileHash =
        JmashFileEntity.createHash(file.length(), hashArg.get(0), hashArg.get(1)).build();
    JmashFileEntity entity = jmashFileDao.findByHash(fileHash);
    if (null != entity) {
      return entity;
    }

    entity = new JmashFileEntity();
    entity.setFileSrc(fileSrc);
    entity.setContentType(contentType);
    // 后缀名.
    String fileExt = MimeType.getExtension(fileSrc).toLowerCase();
    entity.setFileExt(fileExt);
    entity.setFileSize(file.length());
    entity.setFileHash(fileHash);


    MimeType mimeType = MimeType.convert(fileExt);
    // 宽度、高度
    try {
      if (null != mimeType && FileType.image.equals(mimeType.getFileType())) {
        Image img = javax.imageio.ImageIO.read(file); // 构造Image对象
        int fileWidth = img.getWidth(null); // 得到源图宽
        int fileHeight = img.getHeight(null); // 得到源图长
        entity.setFileWidth(fileWidth);
        entity.setFileHeight(fileHeight);
      }
    } catch (Exception ex) {
      log.error("", ex);
    }

    // 时长
    try {
      if (MediaUtil.acceptTimeType(mimeType)) {
        long fileTime = MediaUtil.getDuration(file, mimeType.getFileType());
        entity.setFileTime(fileTime);
      }
    } catch (Exception ex) {
      log.error("", ex);
    }

    // 地理位置
    if (null != mimeType && (FileType.image.equals(mimeType.getFileType())
        || FileType.video.equals(mimeType.getFileType())
        || FileType.audio.equals(mimeType.getFileType()))) {
      MediaUtil media = new MediaUtil(file);
      entity.setGeoLatitude(media.getGeoLatitudeD());
      entity.setGeoLongitude(media.getGeoLongitudeD());
    }

    entity.setCreateDate(LocalDateTime.now());
    jmashFileDao.persist(entity);
    return entity;
  }


  @Override
  public FileInfo saveCloudFile(String fileSrc, String contentType, File file) throws IOException {
    JmashFileEntity cloudFileEntity = createOrFind(fileSrc, contentType, file);
    File distFile = new File(FilePathUtil.realPath(cloudFileEntity.getFileSrc(), false));
    if (!distFile.exists()) {
      FileUtils.moveFile(file, distFile);
    } else {
      if (!fileSrc.equals(cloudFileEntity.getFileSrc())) {
        FileUtils.forceDelete(file);
      }
    }
    FileInfo fileInfo = JmashFileMapper.INSTANCE.toFileInfo(cloudFileEntity);
    return fileInfo;
  }

  @Override
  public JmashFileEntity delete(UUID entityId) throws IOException {
    JmashFileEntity entity = jmashFileDao.removeById(entityId);
    if (null != entity) {
      File distFile = new File(FilePathUtil.realPath(entity.getFileSrc(), false));
      if (distFile.exists()) {
        FileUtils.forceDelete(distFile);
      }
    }
    return entity;
  }

  @Override
  public Integer batchDelete(Set<UUID> entityIds) {
    int i = 0;
    for (UUID entityId : entityIds) {
      jmashFileDao.removeById(entityId);
      i++;
    }
    return i;
  }


}
