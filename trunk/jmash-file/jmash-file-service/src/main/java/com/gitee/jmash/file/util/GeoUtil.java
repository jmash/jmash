package com.gitee.jmash.file.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 37° 47' 40".
 *
 * @author lenovo
 *
 */
public class GeoUtil {

  private static Log log = LogFactory.getLog(GeoUtil.class);

  String geoDegrees;

  BigDecimal geoDecimal = new BigDecimal(0);

  BigDecimal degree;

  BigDecimal minute;

  BigDecimal second;

  /** 地理信息. */
  public GeoUtil(String geoDegrees) {
    this.geoDegrees = geoDegrees;
    if (StringUtils.isBlank(geoDegrees)) {
      return;
    }
    // 创建 Pattern 对象
    String p = "(\\d+)°\\s(\\d+)'\\s([\\d\\.]+)\"";
    Pattern r = Pattern.compile(p);

    // 现在创建 matcher 对象
    Matcher m = r.matcher(geoDegrees);
    if (m.find()) {
      degree = new BigDecimal(m.group(1));
      minute = new BigDecimal(m.group(2));
      second = new BigDecimal(m.group(3));
      BigDecimal md = minute.setScale(13).divide(new BigDecimal(60), RoundingMode.HALF_UP);
      BigDecimal sd = second.setScale(13).divide(new BigDecimal(3600), RoundingMode.HALF_UP);
      log.debug(md);
      log.debug(sd);

      geoDecimal = degree.add(md).add(sd);
    } else {
      LogFactory.getLog(GeoUtil.class).error("不是GPS标准格式：" + geoDecimal);
    }
  }

  /** 地理信息. */
  public GeoUtil(BigDecimal geoDecimal) {
    this.geoDecimal = geoDecimal;

    BigDecimal[] parts = geoDecimal.divideAndRemainder(BigDecimal.ONE);
    degree = parts[0];

    parts = parts[1].multiply(new BigDecimal(60)).divideAndRemainder(BigDecimal.ONE);
    minute = parts[0];

    second = parts[1].multiply(new BigDecimal(60)).setScale(2, RoundingMode.HALF_UP);

    this.geoDegrees =
        String.format("%d° %d' %s\"", degree.intValue(), minute.intValue(), second.toString());
  }


  public String getGeoDegrees() {
    return geoDegrees;
  }


  public void setGeoDegrees(String geoDegrees) {
    this.geoDegrees = geoDegrees;
  }


  public BigDecimal getGeoDecimal() {
    return geoDecimal;
  }


  public void setGeoDecimal(BigDecimal geoDecimal) {
    this.geoDecimal = geoDecimal;
  }


  public BigDecimal getDegree() {
    return degree;
  }

  public void setDegree(BigDecimal degree) {
    this.degree = degree;
  }

  public BigDecimal getMinute() {
    return minute;
  }

  public void setMinute(BigDecimal minute) {
    this.minute = minute;
  }

  public BigDecimal getSecond() {
    return second;
  }

  public void setSecond(BigDecimal second) {
    this.second = second;
  }



}
