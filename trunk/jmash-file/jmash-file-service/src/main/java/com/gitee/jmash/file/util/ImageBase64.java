
package com.gitee.jmash.file.util;

import com.gitee.jmash.common.enums.MimeType;
import com.gitee.jmash.core.utils.FilePathUtil;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Base64;
import java.util.List;
import javax.imageio.ImageIO;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.LogFactory;

/** Image Base64. */
public class ImageBase64 {

  /**
   * data:image/jpeg;base64, base64图片开始固定的内容.
   *
   * @return image/jpeg
   */
  public static MimeType mimeType(String base64) {
    if (base64.indexOf("base64,") > -1) {
      String mimeTypeStr = StringUtils.substringBetween(base64, "data:", ";base64,");
      if (StringUtils.isNotEmpty(mimeTypeStr)) {
        List<MimeType> list = MimeType.mimeType(mimeTypeStr);
        return (list.size() > 0) ? list.get(0) : null;
      }
    }
    return null;
  }

  /**
   * data:image/jpeg;base64, base64图片开始固定的内容.
   *
   * @return base64图片开始固定的内容
   */
  public static String base64(String base64) {
    if (base64.indexOf("base64,") > -1) {
      return StringUtils.substringAfterLast(base64, ";base64,");
    }
    return base64;
  }

  /**
   * 将图片文件转化为字节数组字符串，并对其进行Base64编码处理.
   *
   * @param path 图片路径
   */
  public static String imageToBase64(String path) {
    byte[] data = null;
    // 读取图片字节数组
    try (InputStream in = new FileInputStream(path)) {
      data = new byte[in.available()];
      if (in.read(data) > 0) {
        // 对字节数组Base64编码
        // 返回Base64编码过的字节数组字符串
        return Base64.getEncoder().encodeToString(data);
      }
    } catch (IOException e) {
      LogFactory.getLog(ImageBase64.class).error("", e);
    }
    return StringUtils.EMPTY;
  }

  /**
   * 对字节数组字符串进行Base64解码并生成文件.
   *
   * @param base64 文件Base64数据
   * @param path 文件路径
   */
  public static boolean base64ToFile(String base64, String path) {
    if (base64 == null) { // 图像数据为空
      return false;
    }
    File file = new File(path);
    if (!file.getParentFile().exists()) {
      file.getParentFile().mkdirs();
    }
    // Base64解码
    byte[] bytes = Base64.getDecoder().decode(base64);
    for (int i = 0; i < bytes.length; ++i) {
      if (bytes[i] < 0) {
        // 调整异常数据
        bytes[i] += 256;
      }
    }
    // 生成jpeg图片
    try (OutputStream out = new FileOutputStream(path)) {
      out.write(bytes);
      out.flush();
      return true;
    } catch (Exception e) {
      return false;
    }
  }

  /**
   * 保存base64图片到服务器.
   *
   * @param base64String base64字符串
   * @param dirName 另建的目录
   * @return 路径+文件名
   */
  public static String saveImageToServer(String base64String, String dirName) throws Exception {
    // 获取文件类型
    String anyfileExt = "jpg";
    MimeType mimeType = ImageBase64.mimeType(base64String);
    if (null == mimeType) {
      mimeType = MimeType.convert(anyfileExt);
    } else {
      anyfileExt = mimeType.getFileExt();
    }
    // 获取文件Base64
    String base64ImgData = ImageBase64.base64(base64String);

    byte[] byteImage = Base64.getDecoder().decode(base64ImgData);
    ByteArrayInputStream inputStream = new ByteArrayInputStream(byteImage);
    BufferedImage bufferedImg = ImageIO.read(inputStream);

    String tempFile = FilePathUtil.createTempFile(anyfileExt, dirName);
    String realFile = FilePathUtil.realPath(tempFile, true);

    final File newImg = new File(realFile);
    if (!(newImg.getParentFile().exists())) {
      newImg.getParentFile().mkdirs();
    }
    // jpg不管输出什么格式图片，此处不需改动
    ImageIO.write(bufferedImg, "jpg", newImg);
    return tempFile;
  }

  /**
   * 测试.
   */
  public static void main(String[] args) {
    String b64 =
        "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAoHBwgHBgoICAgLCgoLDhgQDg0NDh0VFhEYIx8lJCIfIiEmKzcvJik0KSEiMEExNDk7Pj4+JS5ESUM8SDc9Pjv/2wBDAQoLCw4NDhwQEBw7KCIoOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozv/wAARCAAyAOoDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwC0xUAhcDn1+vvTGcZIz69/r71ECxfBJ/zn3pASCxzzz3+vvXxXMz7lRJHmCggZ6+v196Y0rtk7sDPGPx96Q9Pf/wDX70Djkdee/wBfeqKSQ2RyActk/wD6/eoCcZ5/X61JK2Seenv9feo2PX/Pr70mzaKsIev/ANf6+9LwMndk+n50AgNyevv9fehiuT1/A/X3qkUN38nJOPr9femtlec5H1+vvTyAc7W/X6007hx/X/69MpCH2wR/+ukPXsv0/GgjGSePx/8Ar0p54zx35q0MaWCg88DPf6+9Mz8x+b8aV23HAzgUrIxxhTTKQqJK8wVMk59f/r10ksEcyBZVDY9T06+9Y1leNZgqYAcnls4Pf3rZhuFni3j5euQT06+9aRPPxTndO2iM/WSsNj5cahd7dvx96wPeumu7mzlR4ZXyOR9Dz71jNboG2q+VB6+vX3oZ0YWfLC0kVd5BO4H3pNxQcjA579aumJdxHXGe/wBaayIoGQOvr9feqsdPtEVVbO7BpkuTEMD/ADzV8hNxxjoabkEnnAA4/WqGp67GeS5VuDmqmragNOg3Iyeew+VWPT3/APrVqFlCsSw4zgZ6nmj4eaBbeINVvdU1CJbhbciNFcZXeeScew4rrwtJTblLZHl5rjZ0YKFPRy69kcTLqt9MpWS4mTec4U/KT9BxU9rrN4jLFMwljUY2sBkD2PX8692uvDGiy2zi4sLUR7eWZFXH415l4k+H4hFzqOhXKSwRL5jW5bJGOu09xjtXqXi1ytaHysZ1IT9pCTuRWflXFsJ4XLpIMg9D9KlEUQXa4OR6msLQLt47eaFTjawPTHUVq+fJIjAtnAzmvIqw9nNxR91ga08Th41Xu/8AhieRYyu0D86iKR56D/P41WLsc5Y/iaZk56mszvVN9z0doIbaFyigEA8k89/f/P8ALFJ6rnnPXP1966GZ1RTvOBg9fx96o/YDeSSGzQyFFLtg/dHPPWuOcG7KJ8xRq2u5feZpwMkHHvn6+9Ru/UA59/z96sMwxxj6g/X3ppPXn9fr71gztUiqVbtn8/r70GNueP1+vvWlaadeX+/7LA8uz723t196qnPODnBPf6+9VyO17DVVN2QkOnyTjcHUAHnn6+9QyWxjdlZsY/8Ar+9XoL1rcFeNvPf6+9QSzNNIWZ8k+/196dlbQUZ1OZ32KxhAz8x/zn3pSgwRk/n9fermoWN1pjIl5GYmkBKgkcj86qNMig8E/j9feqcXF2ZpGfOrxd0GF5B5/H6+9KQgBwf88+9NZ2YExxkkHnGT61GftH/PJh/wE01cpK49sDOP89felLHBIGMd/wA/eodtwW5R/wDvk+9Nk8yM5k3j0zTSZXL5kxYNnLcjPU/X3qWO+8hHjDYLD16dfes48scf560rtwxHbj+dUmU6SejJHuF3E5JOf8fekW5DOAAe/f61XPfn9afGcAk4AHf86aNXBJErTsZGxgcev/16qXOr20DAyT5Xodgziq2pPMJ4baNS7XHKqg3M5zgAD8/8itSP4W6vqSLLdvHZqefLLZYD3x3r06GGjKPNLqfP43MZU6jp0lt1KsN3DeJ5kMwkXvgkEfUU4twRu5Gcc1g36XOi640TrsaGQI69AU6D9K3HDBmwvasa9JUnpsz0cuxjxMGpKzRC5Yjk8HOK6HwRo91Botw1m8pka8ZgqzmJSNq43EAk9x+FYOG27WjyK6nwTqjW13JYuoVZfmjyf4h2/L+VPDVOWVu4s3w/taHOt4/l1Owmga9tFgmZPMBIDOoYAjvg8GmjT3t7Ty7iUzll2liirkYweAABVeO7OZkk3DyWOVfCk/TuRVwTN5UbT4BPIQnOPavTPkmmjyW/8PRaDDZx7dt3LG8k2DyQzHZkduBj8KrSEINo6kc0sUF8GnkvZTLPJMzF3fcQOgGc9ABTmtnJOSPWvMrzUqjaPuMsh7LCwjLR/wCZXzRtY+n51MbZu7CnCLAxvrG6PSc13PV7nVNas9J0ldSSAXV3qKW1wgUMpjYtwO3QCtWFJor26RLbTkgEeI9hIcnjhxjAH51yyQJLomkx2V4t1Ha6gtx580h+fbuJGcfhWnJNLFdzXKaPpvmXC7Xla7KtIvof3fTpWDxup+fTo9Eu/l19Sl4n8y28JtPNa6fFcfaFXNnyuPqQDmsfRtJvtZZfLTy4f4pm+6P15NX9at2l8NS2dtZafYpG/nlYbgsDtGTxsHJqU6t4pnhspdM0+1htxGG2EgiTK+mRgc5wKznUjUakzupTqU6TUbXbe72I9R8YW+gTw6VoMSXAhkzdSNyG9QD6+/QYp+o6NFrUR1fw9IpL8zWpOMN3x6H26VJ/a/jf/oHaafz/APi6jn1LxzNEUS0sYDkHenJ4OccsRz0/GtJV4yXLdW9TOKnBqULKXV817+pgRwrDqKC9hlKIwMsPKtjuOorp9E/4RnUtTEFpot7FJEPN8yZmCDB/3znr0xWPqfibxDaavbvcW1tBNHbkbR8wbceT14+70/xrd0rXNZv9BnuPLt2vCzLCOVQemevvWaqRpq7s7m+JlWnTUtumktChq/iLwtf6rI17o+p3M1uTD5qZCEAnphxx17VRudU8KPaypb6DqaTMjeWzE4DYOCf3nrWqt544x839l59g1V9Q1zxhplm11cDTfLUgHarE88etavEQm+jZlTjKNoxf/k7/AMifwK1x/Zmp/Zti3O0eX5n3Q+GxnHbOKdY6v4yu9XvNNc6fC9qu4yNCxVv7uOc89fwrK0a9uW0DxHcScS3EUkhZDjaSrnj860dWv9Vl8DSTi3CX8tsguHXhtnc/kTx2yaFieT3F3sKvTbrSbSd2lr00Qy08QeMrjWl0qWGyt5iu8l4iQq4znhvwqD4hTT+VoP2h42leOTzWj+4WxHkj2znFXY9Wvv8AhEDqos9+pLaFFm/iZf73/s2K53xRMZdH8LhgRstCM56/JHTWJc3yv0+4uhT/ANojJRStdad7MobEKnI/X6+9NYRY2jGP/wBfvUbNk9f1oADZ5x/n61jzH0SXmOZUXsOaGdcbTjj/AOvSAfKM9jzz9aCMks3GR/jTTYzodB011/4qCGB7iaNfJhhiRSVH8TfMRyeB19a6+6vp7fTxcCFmfGSgRmP/AHyOc1zvgm+I0+5tk+Z438zaOpU+n5V0H25lMbKHYv8AKqhMlj/Svew8r0kz47G03HETXmef+OdCuNYm0++t4Qs0soiZSpXKnnJB5GMGs6VthYZ7kV2HizVHt3EaAGYg9/uD1+tceeVO7HPPX6+9cWMmnJRXQ9rKKLhB1Jdf0IjKPf8AOlSZlkDISrKcgg4x+tRXMtvaqWmmC47Z5rEuvEJJY2/yIvT1b/Cs6OFq1dVojsxWY4bD6N3fZHsGj37XdhBM9qsspBG4kDoSM/oa0o7SW9n3tgMeEUdF96xNBtUsPDuhW2X+1raiScspKlXJbk/3gW/x7Vf8a+J7fwx4Oe5sJ1kvLsmC3cHJ3fxN/wABAJ+uBXsxpNaM+NqVoyblFWv07HnGuatp0fizUrC3f9xDL5aOTwxAAbn/AHt1M8w8HAPoc9R+dcHJuRxjJ5zuJ/nV+11O4tnBjY7epQ9DWFbAqT5oOzPWwWdSpJQrK679f+CdTIxGOlRmQ56/5/Os3+34CuZ0aP8A3ea00aJ0V1kUhgCP85rzp4erB6o+loZhha0bwkvnod/b20D/AA9mDQxsEt5nUFQdrYbke/vW7/ZtheQQG6srecpGApliVtowOmRRRXnP4vmz55/F82cl470+ysre0NrZwQFpCCYolXIx7CuZn1G+iUJHe3CKowAsrADr70UVrL4Udi/ho6zxVd3Nv4L02WG4ljkdotzo5BOUJ5IrltS1K/QWmy9uF3W6k4lYZOTz1oorKnsctDb5sSwuJrm5DzzSSsOAXYscc+tbOoIo8O6lIFAdLgBWA5Uc9DRRVv4kehL4Ec9pUkj6hbK7symVQQTkEZrr/iNI6Lp0SOyxs75QHAOMY4ooqqn8SJz1P49MueDFD2V6jgMpCgg8gj5qvQO7+N72FmLRfYU+QnK/ePb8TRRXLL4mcmI/jT/rsWclfEcECkiL7C58sfd++o6VynjMBLXw+FAUCF+Bx2SiiqpfGv66MeE/jR/rozDJO3qf85oBO/qev+NFFWtz6ToTuTzz6/1qCUnd1P8AnNFFWyYmv4RZh4ktwGIyGB568GvQwiiRmCgHHUCiivXwX8L5nzeb/wC8fJfqed+JSf7Ul5P3B/I1j5O7r2/xoorgrfxGe7gv93h6I47WmY6ncgsT+9bvVI/c/wCA0UV9LT+BHwtb+JL1Z9Daj8mh2JX5T9n7cfwLXlvj4kW+gqDhTZu5HbcZOT9Tgc+1FFavYzOIn+6PrUqfd/Giip6iGXX3F/3jVxXYKAGOMetFFSxo/9k=";
    try {
      String filePath = saveImageToServer(b64, "test");
      System.out.println(filePath);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

}
