package com.gitee.jmash.file.util;

import com.coremedia.iso.IsoFile;
import com.drew.imaging.ImageMetadataReader;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;
import com.gitee.jmash.common.enums.FileType;
import com.gitee.jmash.common.enums.MimeType;
import com.googlecode.mp4parser.FileDataSourceImpl;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.LogFactory;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.mp3.MP3AudioHeader;
import org.jaudiotagger.audio.mp3.MP3File;

/**
 * 文件辅助.
 */
public class MediaUtil {

  private Map<String, String> attrMap;

  /**
   * 文件辅助信息.
   */
  public MediaUtil(File file) {
    attrMap = new LinkedHashMap<String, String>();
    try {
      Metadata metadata = ImageMetadataReader.readMetadata(file);
      for (Directory directory : metadata.getDirectories()) {
        for (Tag tag : directory.getTags()) {
          String tagName = tag.getTagName(); // 标签名
          String desc = tag.getDescription(); // 标签信息
          attrMap.put(tagName, desc);
        }
      }
    } catch (Exception ex) {
      LogFactory.getLog(MediaUtil.class).error("", ex);
    }
  }

  /**
   * 图片高度.
   */
  public int getImageHeight() {
    String desc = (String) attrMap.get("Image Height");
    desc = desc.replace(" pixels", "");
    return Integer.valueOf(desc);
  }

  /**
   * 图片宽度.
   */
  public int getImageWidth() {
    String desc = (String) attrMap.get("Image Width");
    desc = desc.replace(" pixels", "");
    return Integer.valueOf(desc);
  }

  /**
   * 拍摄时间.
   */
  public String getTime() {
    String desc = (String) attrMap.get("Date/Time Original");
    return desc;
  }

  /**
   * 纬度.
   */
  public String getGeoLatitude() {
    String desc = (String) attrMap.get("GPS Latitude");
    return desc;
  }

  /**
   * 经度.
   */
  public String getGeoLongitude() {
    String desc = (String) attrMap.get("GPS Longitude");
    return desc;
  }

  /**
   * 纬度.
   */
  public BigDecimal getGeoLatitudeD() {
    String desc = (String) attrMap.get("GPS Latitude");
    return new GeoUtil(desc).getGeoDecimal();
  }

  /**
   * 经度.
   */
  public BigDecimal getGeoLongitudeD() {
    String desc = (String) attrMap.get("GPS Longitude");
    return new GeoUtil(desc).getGeoDecimal();
  }

  /**
   * Geo高度.
   */
  public int getGeoAltitude() {
    String desc = (String) attrMap.get("GPS Altitude");
    if (StringUtils.isBlank(desc)) {
      return 0;
    }
    desc = desc.replace(" metres", "");
    return Integer.valueOf(desc);
  }


  public Map<String, String> getAttrMap() {
    return attrMap;
  }

  public void setAttrMap(Map<String, String> attrMap) {
    this.attrMap = attrMap;
  }

  /** 获取时长支持的类型. */
  public static boolean acceptTimeType(MimeType mimeType) {
    if (null == mimeType) {
      return false;
    }
    if (MimeType.M_mp4.equals(mimeType)) {
      return true;
    } else if (MimeType.M_mov.equals(mimeType)) {
      return true;
    } else if (MimeType.M_mp3.equals(mimeType)) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * 得到语音或视频文件时长,单位秒.
   */
  public static long getDuration(File file, FileType fileType) throws IOException {
    if (null == fileType) {
      return 0L;
    }
    if (FileType.video.equals(fileType)) {
      return getMp4Duration(file);
    }
    if (FileType.audio.equals(fileType)) {
      return getMp3Duration(file);
    }
    return 0L;
  }

  /**
   * 获取视频文件的播放长度(mp4、mov格式).
   *
   * @return 单位为毫秒
   */
  public static long getMp4Duration(File file) throws IOException {
    try (IsoFile isoFile = new IsoFile(new FileDataSourceImpl(file))) {
      long lengthInSeconds = isoFile.getMovieBox().getMovieHeaderBox().getDuration()
          / isoFile.getMovieBox().getMovieHeaderBox().getTimescale();
      return lengthInSeconds;
    }
  }

  /**
   * 获取mp3语音文件播放时长(秒) mp3.
   */
  public static long getMp3Duration(File file) {
    try {
      MP3File f = (MP3File) AudioFileIO.read(file);
      MP3AudioHeader audioHeader = (MP3AudioHeader) f.getAudioHeader();
      Float seconds = Float.parseFloat(audioHeader.getTrackLength() + "");
      return seconds.longValue();
    } catch (Exception ex) {
      LogFactory.getLog(MediaUtil.class).error("", ex);
      return 0L;
    }
  }

}
