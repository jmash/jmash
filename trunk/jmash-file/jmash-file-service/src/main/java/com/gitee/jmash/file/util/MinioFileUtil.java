package com.gitee.jmash.file.util;

import com.gitee.jmash.common.config.AppProps;
import com.gitee.jmash.common.enums.MimeType;
import com.gitee.jmash.core.utils.FilePathUtil;
import jakarta.enterprise.inject.spi.CDI;
import jakarta.inject.Inject;
import jakarta.json.bind.Jsonb;
import jakarta.json.bind.JsonbBuilder;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.microprofile.config.inject.ConfigProperties;

/**
 * Weixin Restful Client Util.
 */
public class MinioFileUtil {
  private static Log log = LogFactory.getLog(MinioFileUtil.class);

  @Inject
  @ConfigProperties
  AppProps appProps;

  /** 下载文件. */
  public static synchronized void downloadFile(String contentType, String fileSrc) {
    File file = new File(FilePathUtil.realPath(fileSrc, false));
    try {
      if (file.exists()) {
        return;
      }
      File downFile = null;
      if ((fileSrc.startsWith("static") || fileSrc.startsWith("xyvcard"))) {
        downFile = doGetFile("https://img.xyvcard.com/", fileSrc);
      }
      AppProps appProps =
          CDI.current().select(AppProps.class, ConfigProperties.Literal.NO_PREFIX).get();
      if (downFile == null && !appProps.getDevMode()) {
        downFile = doGetFile("https://gh.sooyie.cn/v1/file/path/", fileSrc);
      }
      if (downFile == null) {
        return;
      }
      if (!file.getParentFile().exists()) {
        file.getParentFile().mkdirs();
      }
      FileUtils.moveFile(downFile, file);
    } catch (Exception ex) {
      log.error("", ex);
    }
  }

  /**
   * 保存文件.
   */
  protected static File saveFile(InputStream inputStream, File file) {
    try (FileOutputStream fos = new FileOutputStream(file)) {
      byte[] buffer = new byte[1024];
      int bytesRead;
      while ((bytesRead = inputStream.read(buffer)) != -1) {
        fos.write(buffer, 0, bytesRead);
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    return file;
  }

  /**
   * Get请求,返回Response.
   */
  protected static Response doGet(WebTarget target, String url, String... queryParams) {
    if (queryParams.length % 2 != 0) {
      throw new RuntimeException("动态参数必须为键值对!");
    }
    target = target.path(url);
    for (int i = 0; i < queryParams.length / 2; i++) {
      target = target.queryParam(queryParams[i * 2], queryParams[i * 2 + 1]);
    }
    System.out.println("========================================" + target.getUri());
    Response resp = target.request(MediaType.APPLICATION_JSON).get();
    return resp;

  }

  /**
   * Get请求,返回实体.
   */
  protected static File doGetFile(String targetUrl, String url, String... queryParams) {
    try (Client client = ClientBuilder.newClient(); Jsonb jsonb = JsonbBuilder.create()) {
      WebTarget target = client.target(targetUrl);
      Response resp = doGet(target, url, queryParams);
      if (resp.getStatus() == 404) {
        return null;
      }
      if (resp.getStatus() != 200) {
        throw new RuntimeException(resp.getStatusInfo().getReasonPhrase());
      }
      String fileExt = MimeType.getExtension(url).toLowerCase();
      InputStream inputStream = resp.readEntity(InputStream.class);
      final File tempFile = File.createTempFile("download_", "." + fileExt);
      saveFile(inputStream, tempFile);
      return tempFile;
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

}
