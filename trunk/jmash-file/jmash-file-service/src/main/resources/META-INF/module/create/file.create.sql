/*!40101 SET NAMES utf8 */;

create table os_jmash_file
(
   file_id              binary(16) not null comment '文件ID',
   file_src             varchar(510) comment '文件存储路径',
   content_type         varchar(126) comment '文件内容类型',
   file_ext             varchar(14) comment '文件后缀',
   file_size            bigint comment '文件大小',
   hash_sm3             binary(32) comment '文件Hash SM3值',
   hash_sha256          binary(32) comment '文件Hash SHA256值',
   file_width           int comment '图片/视频宽度',
   file_height          int comment '图片/视频高度',
   file_time            int comment '音频/视频时长',
   file_location        varchar(510) comment '图片/视频/音频拍摄位置',
   geo_longitude        decimal(10,7) comment '经度',
   geo_latitude         decimal(10,7) comment '纬度',
   geo_accuracy         int comment '精度',
   geo_altitude         bigint comment '海拔高度',
   geo_altitude_accracy int comment '海拔高度精度',
   create_date          datetime comment '创建时间',
   primary key (file_id)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_general_ci;

alter table os_jmash_file comment 'Jmash File ';

/*==============================================================*/
/* 索引                                                         */
/*==============================================================*/
      
create index index_1 on os_jmash_file
(
   file_src
);

create index index_2 on os_jmash_file
(
   file_size,hash_sm3,hash_sha256
);