package com.gitee.jmash.file.test;

import com.gitee.jmash.core.lib.FileClientUtil;
import com.gitee.jmash.file.client.cdi.FileClient;
import com.google.api.HttpBody;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.concurrent.CountDownLatch;
import javax.imageio.ImageIO;
import io.grpc.stub.StreamObserver;
import org.junit.jupiter.api.Test;
import jmash.basic.FileBasicGrpc;
import jmash.protobuf.ThumbSrcDownloadReq;
import jmash.protobuf.ThumbType;

/**
 * 测试客户端调用文件服务器。
 */
public class FileClientTest extends FileTest {

  /** 异步，下载大文件. */
  @Test
  public void downloadImageSrc() throws IOException, InterruptedException {
    FileBasicGrpc.FileBasicStub fileBasicStub = FileClient.getFileBasicStub();
    ThumbSrcDownloadReq req = ThumbSrcDownloadReq.newBuilder()
        .setFileSrc("core/vcard/draw/image/2024_10/fb954601-339d-492b-b735-0e3e86231b3c.png")
        .setThumbType(ThumbType.trans).setWidth(400).setHeight(246).build();
    final File destFile = new File("/data/jmash/test/VCARD_Test.png");
    final CountDownLatch latchFinish = new CountDownLatch(1);
    StreamObserver<HttpBody> resp = FileClientUtil.createHttpBodyResp(destFile, latchFinish);
    fileBasicStub.downloadThumbBySrc(req, resp);
    latchFinish.await();
  }

  /** 异步，下载大文件. */
  @Test
  public void downloadFile1() throws IOException, InterruptedException {
    final File destFile = FileClient
        .downloadFile("core/vcard/draw/image/2024_10/fb954601-339d-492b-b735-0e3e86231b3c.png");
    System.out.println(destFile.getPath());
  }

  @Test
  public void downloadImageSrc2() throws IOException, InterruptedException {
    FileBasicGrpc.FileBasicBlockingStub fileBasicStub = FileClient.getFileBasicBlockingStub();
    ThumbSrcDownloadReq req = ThumbSrcDownloadReq.newBuilder()
        .setFileSrc("core/vcard/draw/image/2024_10/fb954601-339d-492b-b735-0e3e86231b3c.png")
        .setThumbType(ThumbType.trans).setWidth(400).setHeight(246).build();
    Iterator<com.google.api.HttpBody> resp = fileBasicStub.downloadThumbBySrc(req);
    BufferedImage image = FileClientUtil.downloadImage(resp);
    ImageIO.write(image, "png", new File("/data/jmash/test/VCARD_Test2.png"));
  }

}
