
package com.gitee.jmash.file.test;

import com.gitee.jmash.common.grpc.auth.BasicCredentials;
import com.gitee.jmash.common.grpc.auth.OAuth2Credentials;
import com.gitee.jmash.common.grpc.client.GrpcChannel;
import com.gitee.jmash.core.grpc.DefaultGrpcServer;
import com.gitee.jmash.rbac.client.RbacClient;
import com.gitee.jmash.rbac.client.shiro.JmashClientShiroConfig;
import io.grpc.CallCredentials;
import io.grpc.ManagedChannel;
import jmash.rbac.RbacGrpc;
import jmash.rbac.protobuf.LoginReq;
import jmash.rbac.protobuf.TokenResp;
import org.apache.commons.logging.LogFactory;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import jakarta.enterprise.inject.se.SeContainer;
import jakarta.enterprise.inject.se.SeContainerInitializer;

public abstract class FileTest {

  protected static DefaultGrpcServer server;

  protected static ManagedChannel channel = null;

  protected static TokenResp token;
  
  protected static String TENANT = "file";

  @BeforeAll
  public static synchronized void setup() throws Exception {
    if (server == null) {
      SeContainer container = SeContainerInitializer.newInstance().initialize();
      // Shiro 初始化.
      JmashClientShiroConfig.config();
      server = (DefaultGrpcServer) container.select(DefaultGrpcServer.class).get();
      server.start(true);
      channel = GrpcChannel.getInProcessChannel();
    }
  }

  @AfterAll
  public static void stop() throws InterruptedException {
    // channel.shutdownNow();
    // server.stop();
  }

  /** 认证授权. */
  public static CallCredentials getOAuthCallCredentials() {
    String clientSecret =
        "694f956fe6ccfbb2e2905f1da3f00ddc7cf72737a7bab7bfe09f8cf6b13178e679bb3e699d1a2709";
    return new BasicCredentials("TestClient", clientSecret);
  }


  /** 授权. */
  public static CallCredentials getAuthcCallCredentials() {
    if (token == null) {
      String p =
          "0425561d7d70045b7d2fe3198cc5d33398ef0b48f13e7d82f8fbf36cfa7bec7e1339c3b5edfcfc6e2d1c1d"
              + "ab3cf6ee6e19fabc95df5fbf4373ca5d5d630c35c67b7ff3d71d63a26c645b7476ae0e6acecf8d5293"
              + "4b9d1a774593a239dc557677da481740d5cd586a3c7b5d7d4182";
      token = login("admin", p);
    }
    return new OAuth2Credentials(token.getAccessToken());
  }

  /** 登录系统. */
  public static TokenResp login(String userName, String pwd) {
    RbacGrpc.RbacBlockingStub rbacStub = RbacClient.getRbacBlockingStub();
    LoginReq request = LoginReq.newBuilder().setTenant(TENANT).setDirectoryId("jmash")
        .setUserName(userName).setEncodePwd(pwd).setClientId("test").build();
    LogFactory.getLog(FileTest.class).warn(request);
    return rbacStub.login(request);
  }

}
