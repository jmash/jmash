
package com.gitee.jmash.file.test;

import com.gitee.jmash.core.lib.FileClientUtil;
import com.google.api.HttpBody;
import io.grpc.stub.StreamObserver;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import jmash.basic.FileBasicGrpc;
import jmash.protobuf.DownloadReq;
import jmash.protobuf.DownloadSrcReq;
import jmash.protobuf.FileRangeReq;
import jmash.protobuf.FileRangeResp;
import jmash.protobuf.ThumbDownloadReq;
import jmash.protobuf.ThumbSrcDownloadReq;
import jmash.protobuf.ThumbType;
import org.junit.jupiter.api.Test;


public class JmashFileBasicDownloadTest extends FileTest {

  public static final int MAX_FILE_SIZE = 4 * 1024 * 1024 - 512 * 1024;

  private FileBasicGrpc.FileBasicStub fileBasicStub = null;

  @Test
  public void download1() throws IOException, InterruptedException {
    fileBasicStub = FileBasicGrpc.newStub(channel);
    // fileBasicStub = fileBasicStub.withCallCredentials(getAuthcCallCredentials(token));
    DownloadReq req = DownloadReq.newBuilder().setTenant(TENANT)
        .setFileId("787adce7-7099-4a6a-9c8b-a54154d0d745").build();

    final File destFile = new File("/data/jmash/test/787adce7-7099-4a6a-9c8b-a54154d0d745.tar.gz");

    final CountDownLatch latchFinish = new CountDownLatch(1);
    StreamObserver<HttpBody> resp = FileClientUtil.createHttpBodyResp(destFile, latchFinish);
    fileBasicStub.downloadFile(req, resp);
    latchFinish.await();
  }


  @Test
  public void download() throws IOException, InterruptedException {
    fileBasicStub = FileBasicGrpc.newStub(channel);
    // fileBasicStub = fileBasicStub.withCallCredentials(getAuthcCallCredentials(token));
    DownloadReq req = DownloadReq.newBuilder().setTenant(TENANT)
        .setFileId("A06A972ED4C4466DB2359659E3A9BE7A").build();

    final File destFile = new File("/data/jmash/test/A06A972ED4C4466DB2359659E3A9BE7A.pdf");

    final CountDownLatch latchFinish = new CountDownLatch(1);
    StreamObserver<HttpBody> resp = FileClientUtil.createHttpBodyResp(destFile, latchFinish);
    fileBasicStub.downloadFile(req, resp);
    latchFinish.await();
  }

  @Test
  public void downloadSrc() throws IOException, InterruptedException {
    fileBasicStub = FileBasicGrpc.newStub(channel);
    fileBasicStub = fileBasicStub.withCallCredentials(getAuthcCallCredentials());
    DownloadSrcReq req = DownloadSrcReq.newBuilder()
        .setFileSrc("text/2023_12/95096453-041e-4fc1-be6a-830ea85ac4ea.pdf").build();

    final File destFile = new File("/data/jmash/test/A06A972ED4C4466DB2359659E3A9BE7A_src.pdf");

    final CountDownLatch latchFinish = new CountDownLatch(1);
    StreamObserver<HttpBody> resp = FileClientUtil.createHttpBodyResp(destFile, latchFinish);
    fileBasicStub.downloadFileBySrc(req, resp);
    latchFinish.await();
  }

  @Test
  public void downloadImage() throws IOException, InterruptedException {
    fileBasicStub = FileBasicGrpc.newStub(channel);
    fileBasicStub = fileBasicStub.withCallCredentials(getAuthcCallCredentials());
    ThumbDownloadReq req = ThumbDownloadReq.newBuilder().setTenant(TENANT)
        .setFileId("2FD72553AB604DDF97A2F3EF666F5DA6").setThumbType(ThumbType.trans).setWidth(300)
        .build();

    final File destFile = new File("/data/jmash/test/2FD72553AB604DDF97A2F3EF666F5DA6.png");

    final CountDownLatch latchFinish = new CountDownLatch(1);
    StreamObserver<HttpBody> resp = FileClientUtil.createHttpBodyResp(destFile, latchFinish);
    fileBasicStub.downloadThumb(req, resp);
    latchFinish.await();
  }

  @Test
  public void downloadImageSrc() throws IOException, InterruptedException {
    fileBasicStub = FileBasicGrpc.newStub(channel);
    fileBasicStub = fileBasicStub.withCallCredentials(getAuthcCallCredentials());
    ThumbSrcDownloadReq req = ThumbSrcDownloadReq.newBuilder()
        .setFileSrc("core/image/2023_12/7a5a1358-550d-417a-a1a4-7cbcf134d9f3.png").build();

    final File destFile = new File("/data/jmash/test/2FD72553AB604DDF97A2F3EF666F5DA6-1.png");

    final CountDownLatch latchFinish = new CountDownLatch(1);
    StreamObserver<HttpBody> resp = FileClientUtil.createHttpBodyResp(destFile, latchFinish);
    fileBasicStub.downloadThumbBySrc(req, resp);
    latchFinish.await();
  }

  @Test
  public void downloadImageSrc1() throws IOException, InterruptedException {
    fileBasicStub = FileBasicGrpc.newStub(channel);
    fileBasicStub = fileBasicStub.withCallCredentials(getAuthcCallCredentials());
    ThumbSrcDownloadReq req =
        ThumbSrcDownloadReq.newBuilder().setFileSrc("wxapp/images/template/template_blue.jpg")
            .setThumbType(ThumbType.resize).setWidth(600).setHeight(600).build();

    final File destFile = new File("/data/jmash/test/01.png");

    final CountDownLatch latchFinish = new CountDownLatch(1);
    StreamObserver<HttpBody> resp = FileClientUtil.createHttpBodyResp(destFile, latchFinish);
    fileBasicStub.downloadThumbBySrc(req, resp);
    latchFinish.await();
  }


  @Test
  public void downloadFileRange() throws IOException, InterruptedException {
    fileBasicStub = FileBasicGrpc.newStub(channel);
    fileBasicStub = fileBasicStub.withCallCredentials(getAuthcCallCredentials());
    FileRangeReq req = FileRangeReq.newBuilder()
        .setFileSrc("wxapp/images/template/template_blue.jpg").putHeaders("Range","bytes=0-").setMethod("get").build();

    final File destFile = new File("/data/jmash/test/01.png");

    final CountDownLatch latchFinish = new CountDownLatch(1);
    StreamObserver<FileRangeResp> resp = new StreamObserver<FileRangeResp>() {
      @Override
      public void onNext(FileRangeResp value) {
        System.out.println(value.getStatusCode());
        System.out.println(value.getContentType());
        System.out.println(value.getHeadersMap());
        System.out.println(value.getData());
      }

      @Override
      public void onError(Throwable t) {
        System.out.println("ERROR" + t.getMessage());
        if (null != latchFinish) {
          latchFinish.countDown();
        }
      }

      @Override
      public void onCompleted() {
        if (null != latchFinish) {
          latchFinish.countDown();
        }
      }
    };
    fileBasicStub.downloadFileRange(req, resp);
    latchFinish.await();
  }



}
