package com.gitee.jmash.file.test;

import com.gitee.jmash.common.utils.FileHashUtil;
import com.gitee.jmash.core.lib.FileClientUtil;
import com.gitee.jmash.crypto.digests.SM3Util;
import com.google.protobuf.ByteString;
import io.grpc.stub.StreamObserver;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import jmash.basic.FileBasicGrpc;
import jmash.protobuf.FileHash;
import jmash.protobuf.FileHashHex;
import jmash.protobuf.FileInfo;
import jmash.protobuf.FileSrcReq;
import jmash.protobuf.FileUploadReq;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

/**
 * $Id$.
 *
 * @author <a href="mailto:cgd321@crenjoy.net">cgd</a>.
 * @version $Revision$
 */
@TestMethodOrder(OrderAnnotation.class)
public class JmashFileBasicUploadTest extends FileTest {


  public static final int MAX_FILE_SIZE = 4 * 1024 * 1024 - 512 * 1024;

  private static Log log = LogFactory.getLog(JmashFileBasicDownloadTest.class);

  private FileBasicGrpc.FileBasicStub fileBasicStub = null;

  private FileBasicGrpc.FileBasicBlockingStub fileBasicBlockingStub = null;



  @Test
  @Order(1)
  public void uploadFile() throws InterruptedException, IOException {
    fileBasicStub = FileBasicGrpc.newStub(channel);
    fileBasicStub = fileBasicStub.withCallCredentials(getAuthcCallCredentials());
    String fileName = "D:\\data\\jmash\\entdata\\taiyuan.xlsx";
    final CountDownLatch latchFinish = new CountDownLatch(1);

    StreamObserver<FileUploadReq> req =
        fileBasicStub.uploadFile(FileClientUtil.createFileInfoResp(latchFinish));
    FileClientUtil.upload(TENANT, req, fileName);

    latchFinish.await();
  }

  @Test
  @Order(2)
  public void uploadImage() throws InterruptedException, IOException {
    fileBasicStub = FileBasicGrpc.newStub(channel);
    fileBasicStub = fileBasicStub.withCallCredentials(getAuthcCallCredentials());
    String fileName = "/data/jmash/test/test.png";
    final CountDownLatch latchFinish = new CountDownLatch(1);

    StreamObserver<FileUploadReq> req =
        fileBasicStub.uploadFile(FileClientUtil.createFileInfoResp(latchFinish));
    FileClientUtil.upload(TENANT, req, fileName);

    latchFinish.await();
  }

  @Test
  @Order(3)
  public void removeFile() throws InterruptedException, IOException {
    fileBasicBlockingStub = FileBasicGrpc.newBlockingStub(channel);
    fileBasicBlockingStub = fileBasicBlockingStub.withCallCredentials(getAuthcCallCredentials());
    FileSrcReq req = FileSrcReq.newBuilder()
        .setFileSrc("file/image/2024_05/672a9698-865e-4304-9a50-af327193a6a0.png").build();
    fileBasicBlockingStub.removeFile(req);
  }

  // @Test
  @Order(4)
  public void existFile() {
    fileBasicBlockingStub = FileBasicGrpc.newBlockingStub(channel);
    fileBasicBlockingStub = fileBasicBlockingStub.withCallCredentials(getAuthcCallCredentials());
    String fileName = "/data/jmash/test/test.pdf";
    File file = new File(fileName);
    List<byte[]> hashArg = FileHashUtil.getFileHashByte(file,
        new String[] {SM3Util.get().getMessageDigest().getAlgorithm(), FileHashUtil.SHA256});

    FileHash fileHash =
        FileHash.newBuilder().setTenant(TENANT).setHashSm3(ByteString.copyFrom(hashArg.get(0)))
            .setFileSize(file.length()).setHashSha256(ByteString.copyFrom(hashArg.get(1))).build();
    FileInfo fileInfo = fileBasicBlockingStub.existFile(fileHash);
    log.info(fileInfo.getFileSrc());
  }

  // @Test
  @Order(5)
  public void existFileHex() {
    fileBasicBlockingStub = FileBasicGrpc.newBlockingStub(channel);
    fileBasicBlockingStub = fileBasicBlockingStub.withCallCredentials(getAuthcCallCredentials());
    String fileName = "/data/jmash/test/test.pdf";
    File file = new File(fileName);
    String[] hashArg = FileHashUtil.getFileHashString(file,
        new String[] {SM3Util.get().getMessageDigest().getAlgorithm(), FileHashUtil.SHA256});


    FileHashHex fileHash = FileHashHex.newBuilder().setTenant(TENANT).setHashSm3(hashArg[0])
        .setFileSize(file.length()).setHashSha256(hashArg[1]).build();
    FileInfo fileInfo = fileBasicBlockingStub.existFileHex(fileHash);
    log.info(fileInfo.getFileSrc());
  }

}
