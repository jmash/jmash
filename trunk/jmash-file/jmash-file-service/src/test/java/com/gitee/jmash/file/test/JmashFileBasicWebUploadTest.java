package com.gitee.jmash.file.test;

import com.gitee.jmash.core.lib.FileClientUtil;
import java.io.IOException;
import jmash.protobuf.FileInfo;
import jmash.basic.FileBasicGrpc;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.jupiter.api.Test;

public class JmashFileBasicWebUploadTest extends FileTest {

  private static Log log = LogFactory.getLog(JmashFileBasicUploadTest.class);

  public static final int MAX_FILE_SIZE = 4 * 1024 * 1024 - 512 * 1024;

  private FileBasicGrpc.FileBasicBlockingStub fileBasicStub = null;

  @Test
  public void uploadFileWeb() throws InterruptedException, IOException {
    fileBasicStub = FileBasicGrpc.newBlockingStub(channel);
//    fileBasicStub = fileBasicStub.withCallCredentials(getAuthcCallCredentials(token));
    String fileName = "D:\\test\\4.zip";

    FileInfo fileInfo = FileClientUtil.uploadWeb(fileBasicStub, fileName);

    log.info(fileInfo);
  }

}
