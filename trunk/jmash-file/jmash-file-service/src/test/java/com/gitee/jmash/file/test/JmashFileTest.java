
package com.gitee.jmash.file.test;

import com.google.protobuf.Int32Value;
import jmash.file.FileGrpc;
import jmash.file.protobuf.JmashFileKey;
import jmash.file.protobuf.JmashFileKeyList;
import jmash.file.protobuf.JmashFileModel;
import jmash.file.protobuf.JmashFilePage;
import jmash.file.protobuf.JmashFileReq;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

/**
 * 测试.
 *
 * @author CGD
 *
 */
@TestMethodOrder(OrderAnnotation.class)
public class JmashFileTest extends FileTest {

  private FileGrpc.FileBlockingStub fileStub = null;

  @Test
  @Order(1)
  public void findPage() {
    fileStub = FileGrpc.newBlockingStub(channel);
    fileStub = fileStub.withCallCredentials(getAuthcCallCredentials());
    final JmashFileReq.Builder request = JmashFileReq.newBuilder().setTenant(TENANT);
    final JmashFilePage modelPage = fileStub.findJmashFilePage(request.build());
    System.out.println(modelPage);
  }

  @Test
  @Order(2)
  public void deleteTest() {
    fileStub = FileGrpc.newBlockingStub(channel);
    fileStub = fileStub.withCallCredentials(getAuthcCallCredentials());
    JmashFileKey.Builder request =
        JmashFileKey.newBuilder().setTenant(TENANT).setFileId("8A8E6FAEABA24F5790DB8A2FC31A54DC");
    final JmashFileModel result = fileStub.deleteJmashFile(request.build());
    System.out.println(result.getFileId());
  }

  @Test
  @Order(3)
  public void batchDeleteTest() {
    fileStub = FileGrpc.newBlockingStub(channel);
    fileStub = fileStub.withCallCredentials(getAuthcCallCredentials());
    JmashFileKeyList.Builder request = JmashFileKeyList.newBuilder().setTenant(TENANT)
        .addFileId("403D2029F9404CF794550B0075353F0E");
    final Int32Value result = fileStub.batchDeleteJmashFile(request.build());
    System.out.println(result);
  }

  @Test
  @Order(4)
  public void test() {
    fileStub = FileGrpc.newBlockingStub(channel);
    fileStub = fileStub.withCallCredentials(getAuthcCallCredentials());
    // findById
    JmashFileKey pk = JmashFileKey.newBuilder().setFileId("2FD72553AB604DDF97A2F3EF666F5DA6")
        .setTenant(TENANT).build();
    JmashFileModel entity = fileStub.findJmashFileById(pk);
  }

}
