
package com.gitee.jmash.file.util;

import java.io.File;
import java.math.BigDecimal;
import java.util.Map;
import org.apache.commons.logging.LogFactory;

/** 测试. */
public class MediaUtilTest {

  /** 测试. */
  //@Test
  public void test() {
    String filename = "/data/jmash/test/test.png";
    MediaUtil util = new MediaUtil(new File(filename));
    Map<String, String> map = util.getAttrMap();
    for (Map.Entry<String, String> entry : map.entrySet()) {
      LogFactory.getLog(MediaUtilTest.class).error(entry.getKey() + ":" + entry.getValue());
    }
    LogFactory.getLog(MediaUtilTest.class).error(" " + util.getImageHeight());
    LogFactory.getLog(MediaUtilTest.class).error(" " + util.getImageWidth());

    LogFactory.getLog(MediaUtilTest.class).error(" " + util.getGeoLatitude());
    BigDecimal a = new GeoUtil(util.getGeoLatitude()).getGeoDecimal();
    LogFactory.getLog(MediaUtilTest.class).error(" " + a);

    String a1 = new GeoUtil(a).getGeoDegrees();
    LogFactory.getLog(MediaUtilTest.class).error(" " + a1);

    LogFactory.getLog(MediaUtilTest.class).error(" " + util.getGeoLongitude());
    BigDecimal b = new GeoUtil(util.getGeoLongitude()).getGeoDecimal();
    LogFactory.getLog(MediaUtilTest.class).error(" " + b);

    String b1 = new GeoUtil(b).getGeoDegrees();
    LogFactory.getLog(MediaUtilTest.class).error(" " + b1);

    LogFactory.getLog(MediaUtilTest.class).error(" " + util.getGeoAltitude());

  }
}
