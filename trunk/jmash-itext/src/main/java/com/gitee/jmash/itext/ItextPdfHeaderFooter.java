package com.gitee.jmash.itext;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * itext pdf 页脚.
 *
 * @author CGD
 *
 */
public class ItextPdfHeaderFooter extends PdfPageEventHelper {

  private BaseFont bf;

  private int presentFontSize = 8;

  private Rectangle pageSize = null;

  private int pageNumber = 1;

  /**
   * 页脚页码.
   *
   * @param bf              基本字体
   * @param presentFontSize 字号
   * @param pageSize        打印纸张A4
   */
  public ItextPdfHeaderFooter(BaseFont bf, int presentFontSize, Rectangle pageSize) {
    this.bf = bf;
    this.presentFontSize = presentFontSize;
    this.pageSize = pageSize;
  }

  @Override
  public void onEndPage(PdfWriter writer, Document document) {

    PdfContentByte footer = writer.getDirectContent();
    // 设置颜色
    footer.setColorFill(BaseColor.GRAY);
    // 开始设置
    footer.beginText();
    // 设置字体参数及大小
    footer.setFontAndSize(bf, presentFontSize);
    // 设置水印对齐方式 水印内容 X坐标 Y坐标 旋转角度
    footer.showTextAligned(Element.ALIGN_CENTER, String.format("第%d页", pageNumber),
        pageSize.getWidth() / 2, 20, 0);
    pageNumber++;

    // 结束设置
    footer.endText();
    footer.stroke();
  }

}
