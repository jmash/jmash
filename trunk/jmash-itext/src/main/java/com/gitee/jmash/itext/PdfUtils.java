
package com.gitee.jmash.itext;

import com.gitee.jmash.common.utils.LoadLibUtil;
import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * itext PDF 工具类.
 *
 * @author CGD
 *
 */
public class PdfUtils {

  private static Log log = LogFactory.getLog(PdfUtils.class);

  /**
   * 字体路径.
   *
   * @return 路径
   */
  public static String getFontNamePath() {
    String userDir = System.getProperty("user.dir");
    String path = userDir + "/target/fonts/simsun.ttf".replace("/", File.separator);
    File file = new File(path);
    if (file.exists()) {
      return path;
    }
    file.getParentFile().mkdirs();
    LoadLibUtil.copyJarFile(PdfUtils.class, "/fonts/simsun.ttf", path);
    return path;
  }

  /**
   * HTML to PDF .
   *
   * @param htmlStr   HTML
   * @param title     文件标题
   * @param operator  操作人
   * @param landscape 横向打印、纵向打印
   */
  public static File convertToPdf(String htmlStr, String title, String operator,
      boolean landscape) {
    try {
      File tempFile = File.createTempFile(UUID.randomUUID().toString(), ".pdf");
      if (!tempFile.exists()) {
        tempFile.getParentFile().mkdirs();
      }
      OutputStream outS = new FileOutputStream(tempFile);
      PdfUtils.convertToPdf(htmlStr, outS, title, operator, false);
      outS.close();
      return tempFile;
    } catch (Exception e) {
      log.error("", e);
      return null;
    }
  }

  /**
   * HTML to PDF .
   *
   * @param htmlStr   HTML
   * @param os        输出Stream
   * @param title     文件标题
   * @param operator  操作人
   * @param landscape 横向打印、纵向打印
   */
  public static void convertToPdf(String htmlStr, OutputStream os, String title, String operator,
      boolean landscape) {
    try {
      String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
      String mark = String.format("%s 操作人： %s 时间： %s", title, operator, date);
      convertToPdf(htmlStr, os, new Watermark(mark), landscape);
    } catch (Exception e) {
      log.error("", e);
    }
  }

  /**
   * PDF文件生成.
   *
   * @param htmlStr   HTML
   * @param os        打印输出
   * @param watermark 水印
   * @param landscape 横向打印、纵向打印
   */
  public static void convertToPdf(String htmlStr, OutputStream os, Watermark watermark,
      boolean landscape) {
    // 横向
    Rectangle pageSize = null;
    if (landscape) {
      pageSize = new Rectangle(PageSize.A4.getHeight(), PageSize.A4.getWidth());
    } else {
      pageSize = new Rectangle(PageSize.A4.getWidth(), PageSize.A4.getHeight());

    }
    Document document = new Document(pageSize, 20, 20, 20, 20);
    try {
      PdfWriter writer = PdfWriter.getInstance(document, os);
      // 页码
      setFooter(writer, watermark.bfChinese, 8, pageSize);
      document.open();
      // 水印
      writer.setPageEvent(watermark);
      XMLWorkerHelper.getInstance().parseXHtml(writer, document,
          new ByteArrayInputStream(htmlStr.getBytes("UTF-8")), null, Charset.forName("UTF-8"),
          new AsianFontProvider());
    } catch (Exception e) {
      log.error("", e);
    } finally {
      document.close();
    }
  }

  public static void setFooter(PdfWriter writer, BaseFont bf, int presentFontSize,
      Rectangle pageSize) {
    ItextPdfHeaderFooter headerFooter = new ItextPdfHeaderFooter(bf, presentFontSize, pageSize);
    writer.setPageEvent(headerFooter);
  }

}
