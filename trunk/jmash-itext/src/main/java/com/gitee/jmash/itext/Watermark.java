package com.gitee.jmash.itext;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfGState;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.IOException;

/**
 * PDF水印.
 *
 * @author CGD
 *
 */
public class Watermark extends PdfPageEventHelper {

  //设置字体对象为Windows系统默认的字体
  BaseFont bfChinese = BaseFont.createFont(PdfUtils.getFontNamePath(), BaseFont.IDENTITY_H,
      BaseFont.NOT_EMBEDDED);

  // Font font = new Font(bfChinese, 20, Font.NORMAL, new GrayColor(0.9f));

  // 水印内容
  private String waterContent;

  public Watermark() throws IOException, DocumentException {

  }

  public Watermark(String waterContent) throws IOException, DocumentException {
    this.waterContent = waterContent;
  }

  @Override
  public void onEndPage(PdfWriter writer, Document document) {
    // 加入水印
    PdfContentByte waterMark = writer.getDirectContent();
    // 设置水印颜色
    waterMark.setColorFill(BaseColor.GRAY);
    // 设置文本为描边模式，这样下面的文本就不会加粗了
    waterMark.setTextRenderingMode(0);
    // 开始设置水印
    waterMark.beginText();
    // 设置水印透明度
    PdfGState gs = new PdfGState();
    // 设置填充字体不透明度为0.4f
    gs.setFillOpacity(0.3f);
    // 设置透明度
    waterMark.setGState(gs);
    // 设置水印字体参数及大小(字体参数，字体编码格式，是否将字体信息嵌入到pdf中（一般不需要嵌入），字体大小)
    waterMark.setFontAndSize(bfChinese, 15);
    // 设置水印对齐方式 水印内容 X坐标 Y坐标 旋转角度
    for (int i = 0; i < 4; i++) {
      for (int j = 0; j < 7; j++) {
        waterMark.showTextAligned(Element.ALIGN_CENTER,
            waterContent == null ? "HGCloud" : this.waterContent, i * 430f, 560f - j * 80f, 45f);
      }
    }
    // 结束设置
    waterMark.endText();
    waterMark.stroke();
  }
}
