package com.gitee.jmash.itext.test;

import com.gitee.jmash.itext.PdfUtils;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.jupiter.api.Test;

/**
 * itext PDFUtils 工具类.
 *
 * @author CGD
 *
 */
public class PdfUtilsTest {

  private static Log log = LogFactory.getLog(PdfUtilsTest.class);

  @Test
  public void test() throws IOException {
    String userDir = System.getProperty("user.dir");
    String realFileDir = userDir + "/target/test.pdf".replace("/", File.separator);
    log.info(realFileDir);
    if (!new File(realFileDir).exists()) {
      (new File(realFileDir)).getParentFile().mkdirs();
    }

    StringBuffer html = new StringBuffer("<html><head><title>Test PDF</title></head><body>");
    for (int i = 0; i < 50; i++) {
      html.append("<h1>Test PDF Title</h1>");
      html.append("<h1>中文测试</h1>");
    }
    html.append("</body></html>");

    OutputStream outS = new FileOutputStream(realFileDir);
    PdfUtils.convertToPdf(html.toString(), outS, "PDF to Title", "CGD", false);
    outS.close();
  }
}
