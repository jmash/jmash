
set cur_cd=%cd%

cd %~dp0

:: 初始化go mod 
go mod init gitee.com/jmash/jmash/trunk/jmash-jobs/jmash-jobs-gateway

# go get gitee.com/jmash/jmash/trunk/jmash-core-gateway@master

:: 生成Go Grpc网关
protoc -I ../jmash-jobs-lib/src/main/proto  --go_out ./src  --go-grpc_out ./src   --grpc-gateway_out=./src  --openapiv2_out=./openapi  ../jmash-jobs-lib/src/main/proto/jmash/jobs/protobuf/*.proto

protoc -I ../jmash-jobs-lib/src/main/proto  --go_out ./src  --go-grpc_out ./src   --grpc-gateway_out=./src  --openapiv2_out=./openapi  ../jmash-jobs-lib/src/main/proto/jmash/jobs/*.proto

:: 生成OpenApi文档
protoc -I ../jmash-jobs-lib/src/main/proto --doc_out=./openapi/jmash/jobs --doc_opt=html,index.html  ../jmash-jobs-lib/src/main/proto/jmash/jobs/protobuf/*.proto
protoc -I ../jmash-jobs-lib/src/main/proto --doc_out=./openapi/jmash/jobs --doc_opt=markdown,readme.md  ../jmash-jobs-lib/src/main/proto/jmash/jobs/*.proto

:: copy 
xcopy openapi bin\openapi\ /s /e /y

:: 更新mod 
go mod tidy

:: build 

SET GOOS=windows
SET GOARCH=amd64

go build -o ./bin/main.exe ./src/main.go

SET CGO_ENABLED=0
SET GOOS=linux
SET GOARCH=amd64

go build -o ./bin/main_amd64 ./src/main.go

SET GOARCH=arm64
go build -o ./bin/main_arm64 ./src/main.go

cd %cur_cd%
