# Protocol Documentation
<a name="top"></a>

## Table of Contents

- [jmash/jobs/protobuf/job_define_message.proto](#jmash_jobs_protobuf_job_define_message-proto)
    - [JobDefineCreateReq](#jmash-jobs-JobDefineCreateReq)
    - [JobDefineEnableKey](#jmash-jobs-JobDefineEnableKey)
    - [JobDefineKey](#jmash-jobs-JobDefineKey)
    - [JobDefineKeyList](#jmash-jobs-JobDefineKeyList)
    - [JobDefineList](#jmash-jobs-JobDefineList)
    - [JobDefineModel](#jmash-jobs-JobDefineModel)
    - [JobDefineModelTotal](#jmash-jobs-JobDefineModelTotal)
    - [JobDefinePage](#jmash-jobs-JobDefinePage)
    - [JobDefineReq](#jmash-jobs-JobDefineReq)
    - [JobDefineUpdateReq](#jmash-jobs-JobDefineUpdateReq)
  
- [jmash/jobs/protobuf/job_instance_message.proto](#jmash_jobs_protobuf_job_instance_message-proto)
    - [JobInstanceAssignJob](#jmash-jobs-JobInstanceAssignJob)
    - [JobInstanceHeartbeat](#jmash-jobs-JobInstanceHeartbeat)
    - [JobInstanceInitReq](#jmash-jobs-JobInstanceInitReq)
    - [JobInstanceKey](#jmash-jobs-JobInstanceKey)
    - [JobInstanceKeyList](#jmash-jobs-JobInstanceKeyList)
    - [JobInstanceList](#jmash-jobs-JobInstanceList)
    - [JobInstanceModel](#jmash-jobs-JobInstanceModel)
    - [JobInstanceModelTotal](#jmash-jobs-JobInstanceModelTotal)
    - [JobInstancePage](#jmash-jobs-JobInstancePage)
    - [JobInstanceReq](#jmash-jobs-JobInstanceReq)
  
    - [InstanceState](#jmash-jobs-InstanceState)
  
- [jmash/jobs/protobuf/job_record_message.proto](#jmash_jobs_protobuf_job_record_message-proto)
    - [JobRecordCreateReq](#jmash-jobs-JobRecordCreateReq)
    - [JobRecordDelReq](#jmash-jobs-JobRecordDelReq)
    - [JobRecordKey](#jmash-jobs-JobRecordKey)
    - [JobRecordKeyList](#jmash-jobs-JobRecordKeyList)
    - [JobRecordList](#jmash-jobs-JobRecordList)
    - [JobRecordModel](#jmash-jobs-JobRecordModel)
    - [JobRecordModelTotal](#jmash-jobs-JobRecordModelTotal)
    - [JobRecordPage](#jmash-jobs-JobRecordPage)
    - [JobRecordReq](#jmash-jobs-JobRecordReq)
  
- [jmash/jobs/protobuf/job_trigger_message.proto](#jmash_jobs_protobuf_job_trigger_message-proto)
    - [JobTriggerCreateReq](#jmash-jobs-JobTriggerCreateReq)
    - [JobTriggerKey](#jmash-jobs-JobTriggerKey)
    - [JobTriggerKeyList](#jmash-jobs-JobTriggerKeyList)
    - [JobTriggerList](#jmash-jobs-JobTriggerList)
    - [JobTriggerModel](#jmash-jobs-JobTriggerModel)
    - [JobTriggerReq](#jmash-jobs-JobTriggerReq)
    - [JobTriggerUpdateReq](#jmash-jobs-JobTriggerUpdateReq)
  
- [Scalar Value Types](#scalar-value-types)



<a name="jmash_jobs_protobuf_job_define_message-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## jmash/jobs/protobuf/job_define_message.proto



<a name="jmash-jobs-JobDefineCreateReq"></a>

### JobDefineCreateReq
计划任务定义新增实体


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  |  |
| request_id | [string](#string) |  | 用于检测重复请求的唯一字符串ID |
| validate_only | [bool](#bool) |  | 如果为true，则表示给定的请求仅需要被检验，而不是被执行 |
| classify_ | [string](#string) |  | 分类 |
| job_cn_name | [string](#string) |  | 计划任务中文名称 |
| job_describe | [string](#string) |  | 计划任务描述 |
| weight_ | [int32](#int32) |  | 任务运行权重 |
| event_name | [string](#string) |  | 任务触发事件名称 |
| event_body | [string](#string) |  | 任务触发事件JOSN |
| enable_ | [bool](#bool) |  | 是否启用 |






<a name="jmash-jobs-JobDefineEnableKey"></a>

### JobDefineEnableKey
启用/禁用


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  |  |
| job_name | [string](#string) |  |  |
| enable | [bool](#bool) |  |  |






<a name="jmash-jobs-JobDefineKey"></a>

### JobDefineKey
计划任务定义主键


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  |  |
| job_name | [string](#string) |  |  |






<a name="jmash-jobs-JobDefineKeyList"></a>

### JobDefineKeyList
计划任务定义List


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  |  |
| job_name | [string](#string) | repeated |  |






<a name="jmash-jobs-JobDefineList"></a>

### JobDefineList
计划任务定义列表


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [JobDefineModel](#jmash-jobs-JobDefineModel) | repeated | 当前页内容 |






<a name="jmash-jobs-JobDefineModel"></a>

### JobDefineModel
计划任务定义实体


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| job_name | [string](#string) |  | 计划名称 |
| classify_ | [string](#string) |  | 分类 |
| job_cn_name | [string](#string) |  | 计划任务中文名称 |
| job_describe | [string](#string) |  | 计划任务描述 |
| weight_ | [int32](#int32) |  | 任务运行权重 |
| event_name | [string](#string) |  | 任务触发事件名称 |
| event_body | [string](#string) |  | 任务触发事件JOSN |
| instance_name | [string](#string) |  | 运行实例 |
| enable_ | [bool](#bool) |  | 是否启用 |
| version_ | [int32](#int32) |  | 乐观锁 |
| create_by | [string](#string) |  | 创建人 |
| create_time | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  | 创建时间 |
| update_by | [string](#string) |  | 更新人 |
| update_time | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  | 更新时间 |






<a name="jmash-jobs-JobDefineModelTotal"></a>

### JobDefineModelTotal
合计


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| total_size | [int32](#int32) |  | 总记录数 |






<a name="jmash-jobs-JobDefinePage"></a>

### JobDefinePage
计划任务定义分页


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [JobDefineModel](#jmash-jobs-JobDefineModel) | repeated | 当前页内容 |
| cur_page | [int32](#int32) |  | 当前页码 |
| page_size | [int32](#int32) |  | 页尺寸 |
| total_size | [int32](#int32) |  | 总记录数 |
| sub_total_dto | [JobDefineModelTotal](#jmash-jobs-JobDefineModelTotal) |  | 本页小计 |
| total_dto | [JobDefineModelTotal](#jmash-jobs-JobDefineModelTotal) |  | 合计 |






<a name="jmash-jobs-JobDefineReq"></a>

### JobDefineReq
计划任务定义查询


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户 |
| cur_page | [int32](#int32) |  | 当前页码 |
| page_size | [int32](#int32) |  | 页尺寸 |
| order_name | [string](#string) |  | 排序名称 |
| order_asc | [bool](#bool) |  | 是否升序排序 |
| classify | [string](#string) |  | 分类 |
| has_enable | [bool](#bool) |  | 是否启用 |
| enable | [bool](#bool) |  | 状态 |
| has_assign | [bool](#bool) |  | 是否包含分配参数 |
| assign | [bool](#bool) |  | 是否分配 |
| instance_name | [string](#string) |  | 运行实例 |






<a name="jmash-jobs-JobDefineUpdateReq"></a>

### JobDefineUpdateReq
计划任务定义修改实体


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  |  |
| request_id | [string](#string) |  | 用于检测重复请求的唯一字符串ID |
| validate_only | [bool](#bool) |  | 如果为true，则表示给定的请求仅需要被检验，而不是被执行 |
| update_mask | [google.protobuf.FieldMask](#google-protobuf-FieldMask) |  | 更新字段掩码. |
| job_name | [string](#string) |  | 计划名称 |
| job_cn_name | [string](#string) |  | 计划任务中文名称 |
| job_describe | [string](#string) |  | 计划任务描述 |
| weight_ | [int32](#int32) |  | 任务运行权重 |
| event_name | [string](#string) |  | 任务触发事件名称 |
| event_body | [string](#string) |  | 任务触发事件JOSN |
| enable_ | [bool](#bool) |  | 是否启用 |





 

 

 

 



<a name="jmash_jobs_protobuf_job_instance_message-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## jmash/jobs/protobuf/job_instance_message.proto



<a name="jmash-jobs-JobInstanceAssignJob"></a>

### JobInstanceAssignJob
任务分配


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户 |
| request_id | [string](#string) |  | 用于检测重复请求的唯一字符串ID |
| classify_ | [string](#string) |  | 分类 |
| instance_name | [string](#string) |  | 实例 |






<a name="jmash-jobs-JobInstanceHeartbeat"></a>

### JobInstanceHeartbeat
心跳


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户 |
| classify_ | [string](#string) |  | 分类 |
| instance_name | [string](#string) |  | 实例 |
| interval | [int32](#int32) |  | 心跳间隔（秒） |






<a name="jmash-jobs-JobInstanceInitReq"></a>

### JobInstanceInitReq
运行实例新增实体


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户 |
| request_id | [string](#string) |  | 用于检测重复请求的唯一字符串ID |
| validate_only | [bool](#bool) |  | 如果为true，则表示给定的请求仅需要被检验，而不是被执行 |
| instance_name | [string](#string) |  | 运行实例 |
| classify_ | [string](#string) |  | 分类 |






<a name="jmash-jobs-JobInstanceKey"></a>

### JobInstanceKey
运行实例主键


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  |  |
| classify_ | [string](#string) |  |  |
| instance_name | [string](#string) |  |  |






<a name="jmash-jobs-JobInstanceKeyList"></a>

### JobInstanceKeyList
运行实例List


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  |  |
| job_instance_key | [JobInstanceKey](#jmash-jobs-JobInstanceKey) | repeated |  |






<a name="jmash-jobs-JobInstanceList"></a>

### JobInstanceList
运行实例列表


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [JobInstanceModel](#jmash-jobs-JobInstanceModel) | repeated | 当前页内容 |






<a name="jmash-jobs-JobInstanceModel"></a>

### JobInstanceModel
运行实例实体


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| instance_name | [string](#string) |  | 运行实例 |
| classify_ | [string](#string) |  | 分类 |
| heartbeat_ | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  | 心跳时间 |
| due_date | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  | 过期时间 |
| state_ | [InstanceState](#jmash-jobs-InstanceState) |  | 状态 |






<a name="jmash-jobs-JobInstanceModelTotal"></a>

### JobInstanceModelTotal
合计


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| total_size | [int32](#int32) |  | 总记录数 |






<a name="jmash-jobs-JobInstancePage"></a>

### JobInstancePage
运行实例分页


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [JobInstanceModel](#jmash-jobs-JobInstanceModel) | repeated | 当前页内容 |
| cur_page | [int32](#int32) |  | 当前页码 |
| page_size | [int32](#int32) |  | 页尺寸 |
| total_size | [int32](#int32) |  | 总记录数 |
| sub_total_dto | [JobInstanceModelTotal](#jmash-jobs-JobInstanceModelTotal) |  | 本页小计 |
| total_dto | [JobInstanceModelTotal](#jmash-jobs-JobInstanceModelTotal) |  | 合计 |






<a name="jmash-jobs-JobInstanceReq"></a>

### JobInstanceReq
运行实例查询


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户 |
| cur_page | [int32](#int32) |  | 当前页码 |
| page_size | [int32](#int32) |  | 页尺寸 |
| order_name | [string](#string) |  | 排序名称 |
| order_asc | [bool](#bool) |  | 是否升序排序 |
| classify_ | [string](#string) |  | 分类 |
| has_state | [bool](#bool) |  | 是否包含状态 |
| state_ | [InstanceState](#jmash-jobs-InstanceState) |  | 状态 |





 


<a name="jmash-jobs-InstanceState"></a>

### InstanceState
实例状态.

| Name | Number | Description |
| ---- | ------ | ----------- |
| start | 0 | 已开始 |
| run | 1 | 运行中 |
| stop | 2 | 已停止 |


 

 

 



<a name="jmash_jobs_protobuf_job_record_message-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## jmash/jobs/protobuf/job_record_message.proto



<a name="jmash-jobs-JobRecordCreateReq"></a>

### JobRecordCreateReq
计划任务记录新增实体


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  |  |
| request_id | [string](#string) |  | 用于检测重复请求的唯一字符串ID |
| validate_only | [bool](#bool) |  | 如果为true，则表示给定的请求仅需要被检验，而不是被执行 |
| job_name | [string](#string) |  | 计划名称 |
| start_date | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  | 开始时间 |
| end_date | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  | 结束时间 |
| error_msg | [string](#string) |  | 错误信息 |
| state_ | [bool](#bool) |  | 状态 |






<a name="jmash-jobs-JobRecordDelReq"></a>

### JobRecordDelReq
批量删除请求


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户 |
| year | [int32](#int32) |  | 仅能删除至少1年以前执行任务记录 |






<a name="jmash-jobs-JobRecordKey"></a>

### JobRecordKey
计划任务记录主键


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  |  |
| record_id | [string](#string) |  |  |






<a name="jmash-jobs-JobRecordKeyList"></a>

### JobRecordKeyList
计划任务记录List


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  |  |
| record_id | [string](#string) | repeated |  |






<a name="jmash-jobs-JobRecordList"></a>

### JobRecordList
计划任务记录列表


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [JobRecordModel](#jmash-jobs-JobRecordModel) | repeated | 当前页内容 |






<a name="jmash-jobs-JobRecordModel"></a>

### JobRecordModel
计划任务记录实体


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| record_id | [string](#string) |  | 计划执行ID |
| job_name | [string](#string) |  | 计划名称 |
| start_date | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  | 开始时间 |
| end_date | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  | 结束时间 |
| error_msg | [string](#string) |  | 错误信息 |
| state_ | [bool](#bool) |  | 状态 |






<a name="jmash-jobs-JobRecordModelTotal"></a>

### JobRecordModelTotal
合计


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| total_size | [int32](#int32) |  | 总记录数 |






<a name="jmash-jobs-JobRecordPage"></a>

### JobRecordPage
计划任务记录分页


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [JobRecordModel](#jmash-jobs-JobRecordModel) | repeated | 当前页内容 |
| cur_page | [int32](#int32) |  | 当前页码 |
| page_size | [int32](#int32) |  | 页尺寸 |
| total_size | [int32](#int32) |  | 总记录数 |
| sub_total_dto | [JobRecordModelTotal](#jmash-jobs-JobRecordModelTotal) |  | 本页小计 |
| total_dto | [JobRecordModelTotal](#jmash-jobs-JobRecordModelTotal) |  | 合计 |






<a name="jmash-jobs-JobRecordReq"></a>

### JobRecordReq
计划任务记录查询


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户 |
| cur_page | [int32](#int32) |  | 当前页码 |
| page_size | [int32](#int32) |  | 页尺寸 |
| order_name | [string](#string) |  | 排序名称 |
| order_asc | [bool](#bool) |  | 是否升序排序 |
| job_name | [string](#string) |  | 计划名称 |
| has_state | [bool](#bool) |  | 是否包含状态 |
| state_ | [bool](#bool) |  | 状态 |
| start_date | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  | 开始时间 |
| end_date | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  | 结束时间 |





 

 

 

 



<a name="jmash_jobs_protobuf_job_trigger_message-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## jmash/jobs/protobuf/job_trigger_message.proto



<a name="jmash-jobs-JobTriggerCreateReq"></a>

### JobTriggerCreateReq
任务触发器新增实体


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  |  |
| request_id | [string](#string) |  | 用于检测重复请求的唯一字符串ID |
| validate_only | [bool](#bool) |  | 如果为true，则表示给定的请求仅需要被检验，而不是被执行 |
| job_name | [string](#string) |  | 计划名称 |
| start_now | [bool](#bool) |  | 是否当前日期开始 |
| start_date | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  | 启动时间 |
| cron_ | [string](#string) |  | cron表达式 |
| cron_name | [string](#string) |  | cron描述 |
| is_custom | [bool](#bool) |  | 是否自定义 |






<a name="jmash-jobs-JobTriggerKey"></a>

### JobTriggerKey
任务触发器主键


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  |  |
| trigger_id | [string](#string) |  |  |






<a name="jmash-jobs-JobTriggerKeyList"></a>

### JobTriggerKeyList
任务触发器List


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  |  |
| trigger_id | [string](#string) | repeated |  |






<a name="jmash-jobs-JobTriggerList"></a>

### JobTriggerList
任务触发器列表


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [JobTriggerModel](#jmash-jobs-JobTriggerModel) | repeated | 当前页内容 |






<a name="jmash-jobs-JobTriggerModel"></a>

### JobTriggerModel
任务触发器实体


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| trigger_id | [string](#string) |  | 触发ID |
| job_name | [string](#string) |  | 计划名称 |
| start_now | [bool](#bool) |  | 是否当前日期开始 |
| start_date | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  | 启动时间 |
| cron_ | [string](#string) |  | cron表达式 |
| cron_name | [string](#string) |  | cron描述 |
| is_custom | [bool](#bool) |  | 是否自定义 |






<a name="jmash-jobs-JobTriggerReq"></a>

### JobTriggerReq
任务触发器查询


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  |  |
| cur_page | [int32](#int32) |  | 当前页码 |
| page_size | [int32](#int32) |  | 页尺寸 |
| order_name | [string](#string) |  | 排序名称 |
| order_asc | [bool](#bool) |  | 是否升序排序 |
| job_name | [string](#string) |  | 计划名称 |






<a name="jmash-jobs-JobTriggerUpdateReq"></a>

### JobTriggerUpdateReq
任务触发器修改实体


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  |  |
| request_id | [string](#string) |  | 用于检测重复请求的唯一字符串ID |
| validate_only | [bool](#bool) |  | 如果为true，则表示给定的请求仅需要被检验，而不是被执行 |
| update_mask | [google.protobuf.FieldMask](#google-protobuf-FieldMask) |  | 更新字段掩码. |
| trigger_id | [string](#string) |  | 触发ID |
| start_now | [bool](#bool) |  | 是否当前日期开始 |
| start_date | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  | 启动时间 |
| cron_ | [string](#string) |  | cron表达式 |
| cron_name | [string](#string) |  | cron描述 |
| is_custom | [bool](#bool) |  | 是否自定义 |





 

 

 

 



## Scalar Value Types

| .proto Type | Notes | C++ | Java | Python | Go | C# | PHP | Ruby |
| ----------- | ----- | --- | ---- | ------ | -- | -- | --- | ---- |
| <a name="double" /> double |  | double | double | float | float64 | double | float | Float |
| <a name="float" /> float |  | float | float | float | float32 | float | float | Float |
| <a name="int32" /> int32 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint32 instead. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="int64" /> int64 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint64 instead. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="uint32" /> uint32 | Uses variable-length encoding. | uint32 | int | int/long | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="uint64" /> uint64 | Uses variable-length encoding. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum or Fixnum (as required) |
| <a name="sint32" /> sint32 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int32s. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sint64" /> sint64 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int64s. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="fixed32" /> fixed32 | Always four bytes. More efficient than uint32 if values are often greater than 2^28. | uint32 | int | int | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="fixed64" /> fixed64 | Always eight bytes. More efficient than uint64 if values are often greater than 2^56. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum |
| <a name="sfixed32" /> sfixed32 | Always four bytes. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sfixed64" /> sfixed64 | Always eight bytes. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="bool" /> bool |  | bool | boolean | boolean | bool | bool | boolean | TrueClass/FalseClass |
| <a name="string" /> string | A string must always contain UTF-8 encoded or 7-bit ASCII text. | string | String | str/unicode | string | string | string | String (UTF-8) |
| <a name="bytes" /> bytes | May contain any arbitrary sequence of bytes. | string | ByteString | str | []byte | ByteString | string | String (ASCII-8BIT) |

