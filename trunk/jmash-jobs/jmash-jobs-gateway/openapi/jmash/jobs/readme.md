# Protocol Documentation
<a name="top"></a>

## Table of Contents

- [jmash/jobs/jobs_rpc.proto](#jmash_jobs_jobs_rpc-proto)
    - [Jobs](#jmash-jobs-Jobs)
  
- [Scalar Value Types](#scalar-value-types)



<a name="jmash_jobs_jobs_rpc-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## jmash/jobs/jobs_rpc.proto


 

 

 


<a name="jmash-jobs-Jobs"></a>

### Jobs
Jobs Service

| Method Name | Request Type | Response Type | Description |
| ----------- | ------------ | ------------- | ------------|
| version | [.google.protobuf.Empty](#google-protobuf-Empty) | [.google.protobuf.StringValue](#google-protobuf-StringValue) | 版本 |
| findEnumList | [.google.protobuf.StringValue](#google-protobuf-StringValue) | [.jmash.protobuf.EnumValueList](#jmash-protobuf-EnumValueList) | 枚举值列表 |
| findEnumMap | [.google.protobuf.StringValue](#google-protobuf-StringValue) | [.jmash.protobuf.CustomEnumValueMap](#jmash-protobuf-CustomEnumValueMap) | 枚举值Map |
| findEnumEntry | [.jmash.protobuf.EnumEntryReq](#jmash-protobuf-EnumEntryReq) | [.jmash.protobuf.EntryList](#jmash-protobuf-EntryList) | 枚举值 |
| findJobInstancePage | [JobInstanceReq](#jmash-jobs-JobInstanceReq) | [JobInstancePage](#jmash-jobs-JobInstancePage) | 查询翻页信息运行实例; |
| findJobInstanceList | [JobInstanceReq](#jmash-jobs-JobInstanceReq) | [JobInstanceList](#jmash-jobs-JobInstanceList) | 查询列表信息运行实例; |
| findJobInstanceById | [JobInstanceKey](#jmash-jobs-JobInstanceKey) | [JobInstanceModel](#jmash-jobs-JobInstanceModel) | 查询运行实例; |
| initJobInstance | [JobInstanceInitReq](#jmash-jobs-JobInstanceInitReq) | [JobInstanceModel](#jmash-jobs-JobInstanceModel) | 初始化运行实例. |
| heartbeatJobInstance | [JobInstanceHeartbeat](#jmash-jobs-JobInstanceHeartbeat) | [.google.protobuf.BoolValue](#google-protobuf-BoolValue) | 运行实例心跳. |
| assignJobs | [JobInstanceAssignJob](#jmash-jobs-JobInstanceAssignJob) | [.google.protobuf.BoolValue](#google-protobuf-BoolValue) | 实例分配任务. |
| deleteJobInstance | [JobInstanceKey](#jmash-jobs-JobInstanceKey) | [JobInstanceModel](#jmash-jobs-JobInstanceModel) | 删除运行实例; |
| findClassify | [.jmash.protobuf.TenantReq](#jmash-protobuf-TenantReq) | [.jmash.protobuf.EntryList](#jmash-protobuf-EntryList) | 查询全部分类 |
| findJobDefinePage | [JobDefineReq](#jmash-jobs-JobDefineReq) | [JobDefinePage](#jmash-jobs-JobDefinePage) | 查询翻页信息计划任务定义; |
| findJobDefineList | [JobDefineReq](#jmash-jobs-JobDefineReq) | [JobDefineList](#jmash-jobs-JobDefineList) | 查询列表信息计划任务定义; |
| findJobDefineById | [JobDefineKey](#jmash-jobs-JobDefineKey) | [JobDefineModel](#jmash-jobs-JobDefineModel) | 查询计划任务定义; |
| createJobDefine | [JobDefineCreateReq](#jmash-jobs-JobDefineCreateReq) | [JobDefineModel](#jmash-jobs-JobDefineModel) | 创建实体计划任务定义; |
| updateJobDefine | [JobDefineUpdateReq](#jmash-jobs-JobDefineUpdateReq) | [JobDefineModel](#jmash-jobs-JobDefineModel) | 修改实体计划任务定义; |
| enableJobDefine | [JobDefineEnableKey](#jmash-jobs-JobDefineEnableKey) | [.google.protobuf.BoolValue](#google-protobuf-BoolValue) | 启用/禁用 |
| deleteJobDefine | [JobDefineKey](#jmash-jobs-JobDefineKey) | [JobDefineModel](#jmash-jobs-JobDefineModel) | 删除计划任务定义; |
| batchDeleteJobDefine | [JobDefineKeyList](#jmash-jobs-JobDefineKeyList) | [.google.protobuf.Int32Value](#google-protobuf-Int32Value) | 批量删除计划任务定义; |
| findJobRecordPage | [JobRecordReq](#jmash-jobs-JobRecordReq) | [JobRecordPage](#jmash-jobs-JobRecordPage) | 查询翻页信息计划任务记录; |
| findJobRecordList | [JobRecordReq](#jmash-jobs-JobRecordReq) | [JobRecordList](#jmash-jobs-JobRecordList) | 查询列表信息计划任务记录; |
| findJobRecordById | [JobRecordKey](#jmash-jobs-JobRecordKey) | [JobRecordModel](#jmash-jobs-JobRecordModel) | 查询计划任务记录; |
| createJobRecord | [JobRecordCreateReq](#jmash-jobs-JobRecordCreateReq) | [JobRecordModel](#jmash-jobs-JobRecordModel) | 创建实体计划任务记录; |
| deleteJobRecord | [JobRecordKey](#jmash-jobs-JobRecordKey) | [JobRecordModel](#jmash-jobs-JobRecordModel) | 删除计划任务记录; |
| batchDeleteJobRecord | [JobRecordDelReq](#jmash-jobs-JobRecordDelReq) | [.google.protobuf.Int32Value](#google-protobuf-Int32Value) | 批量删除计划任务记录; |
| findJobTriggerList | [JobTriggerReq](#jmash-jobs-JobTriggerReq) | [JobTriggerList](#jmash-jobs-JobTriggerList) | 查询列表信息任务触发器; |
| findJobTriggerById | [JobTriggerKey](#jmash-jobs-JobTriggerKey) | [JobTriggerModel](#jmash-jobs-JobTriggerModel) | 查询任务触发器; |
| createJobTrigger | [JobTriggerCreateReq](#jmash-jobs-JobTriggerCreateReq) | [JobTriggerModel](#jmash-jobs-JobTriggerModel) | 创建实体任务触发器; |
| updateJobTrigger | [JobTriggerUpdateReq](#jmash-jobs-JobTriggerUpdateReq) | [JobTriggerModel](#jmash-jobs-JobTriggerModel) | 修改实体任务触发器; |
| deleteJobTrigger | [JobTriggerKey](#jmash-jobs-JobTriggerKey) | [JobTriggerModel](#jmash-jobs-JobTriggerModel) | 删除任务触发器; |
| batchDeleteJobTrigger | [JobTriggerKeyList](#jmash-jobs-JobTriggerKeyList) | [.google.protobuf.Int32Value](#google-protobuf-Int32Value) | 批量删除任务触发器; |

 



## Scalar Value Types

| .proto Type | Notes | C++ | Java | Python | Go | C# | PHP | Ruby |
| ----------- | ----- | --- | ---- | ------ | -- | -- | --- | ---- |
| <a name="double" /> double |  | double | double | float | float64 | double | float | Float |
| <a name="float" /> float |  | float | float | float | float32 | float | float | Float |
| <a name="int32" /> int32 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint32 instead. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="int64" /> int64 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint64 instead. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="uint32" /> uint32 | Uses variable-length encoding. | uint32 | int | int/long | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="uint64" /> uint64 | Uses variable-length encoding. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum or Fixnum (as required) |
| <a name="sint32" /> sint32 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int32s. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sint64" /> sint64 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int64s. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="fixed32" /> fixed32 | Always four bytes. More efficient than uint32 if values are often greater than 2^28. | uint32 | int | int | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="fixed64" /> fixed64 | Always eight bytes. More efficient than uint64 if values are often greater than 2^56. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum |
| <a name="sfixed32" /> sfixed32 | Always four bytes. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sfixed64" /> sfixed64 | Always eight bytes. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="bool" /> bool |  | bool | boolean | boolean | bool | bool | boolean | TrueClass/FalseClass |
| <a name="string" /> string | A string must always contain UTF-8 encoded or 7-bit ASCII text. | string | String | str/unicode | string | string | string | String (UTF-8) |
| <a name="bytes" /> bytes | May contain any arbitrary sequence of bytes. | string | ByteString | str | []byte | ByteString | string | String (ASCII-8BIT) |

