
package com.gitee.jobs.client;

import com.gitee.jmash.common.grpc.auth.OAuth2Credentials;
import com.gitee.jmash.common.grpc.client.GrpcChannel;
import com.gitee.jmash.rbac.client.token.SystemAccessToken;
import jmash.jobs.JobsGrpc;

/** Jobs Client . */
public class JobsClient {

  /** JobsBlockingStub. */
  public static JobsGrpc.JobsBlockingStub getJobsBlockingStub() {
    return JobsGrpc.newBlockingStub(JobsClientConfig.getManagedChannel())
        .withCallCredentials(GrpcChannel.getCallCredentials());
  }

  /** JobsFutureStub. */
  public static JobsGrpc.JobsFutureStub getJobsFutureStub() {
    return JobsGrpc.newFutureStub(JobsClientConfig.getManagedChannel())
        .withCallCredentials(GrpcChannel.getCallCredentials());
  }

  /** JobsStub. */
  public static JobsGrpc.JobsStub getJobsStub() {
    return JobsGrpc.newStub(JobsClientConfig.getManagedChannel())
        .withCallCredentials(GrpcChannel.getCallCredentials());
  }

  /** System Client. */
  public static JobsGrpc.JobsBlockingStub getSystemStub(String tenant) {
    String accessToken = SystemAccessToken.getAccessToken(tenant);
    return JobsGrpc.newBlockingStub(JobsClientConfig.getManagedChannel())
        .withCallCredentials(new OAuth2Credentials(accessToken));
  }

}
