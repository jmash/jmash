package com.gitee.jobs.client.event;

import java.io.Serializable;
import java.util.Objects;

/**
 * 定时任务事件.
 * 
 * @author cgd
 */
public class JobsEvent implements Serializable {

  private static final long serialVersionUID = 1L;

  /** 任务ID. */
  String jobName;
  /** 租户. */
  String tenant;
  /** 事件名称. */
  String eventName;
  /** 事件内容. */
  String eventBody;

  public JobsEvent() {
    super();
  }

  public JobsEvent(String jobName, String tenant, String eventName, String eventBody) {
    super();
    this.jobName = jobName;
    this.tenant = tenant;
    this.eventName = eventName;
    this.eventBody = eventBody;
  }

  public String getJobName() {
    return jobName;
  }

  public void setJobName(String jobName) {
    this.jobName = jobName;
  }

  public String getTenant() {
    return tenant;
  }

  public void setTenant(String tenant) {
    this.tenant = tenant;
  }

  public String getEventName() {
    return eventName;
  }

  public void setEventName(String eventName) {
    this.eventName = eventName;
  }

  public String getEventBody() {
    return eventBody;
  }

  public void setEventBody(String eventBody) {
    this.eventBody = eventBody;
  }

  @Override
  public String toString() {
    return "JobsEvent [jobName=" + jobName + ", tenant=" + tenant + ", eventName=" + eventName
        + ", eventBody=" + eventBody + "]";
  }

  @Override
  public int hashCode() {
    return Objects.hash(jobName, tenant);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    JobsEvent other = (JobsEvent) obj;
    return Objects.equals(jobName, other.jobName) && Objects.equals(tenant, other.tenant);
  }

}
