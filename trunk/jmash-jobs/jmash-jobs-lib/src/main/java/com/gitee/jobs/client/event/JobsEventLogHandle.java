
package com.gitee.jobs.client.event;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.event.Observes;
import jakarta.enterprise.inject.spi.EventMetadata;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Event Log .
 */
@ApplicationScoped
public class JobsEventLogHandle {

  private static Log log = LogFactory.getLog(JobsEventLogHandle.class);


  /** 同步实体日志. */
  public void handleEvent(@Observes JobsEvent event, EventMetadata metadata) {
    if (log.isInfoEnabled()) {
      log.info(event.toString());
    }
  }

}
