package com.gitee.jobs.client.task;

import com.crenjoy.proto.utils.TypeUtil;
import com.gitee.jobs.client.JobsClient;
import java.time.LocalDateTime;
import java.util.UUID;
import jmash.jobs.protobuf.JobRecordCreateReq;
import org.apache.commons.lang3.StringUtils;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.quartz.JobListener;



/**
 * 全局job监听器.
 *
 * @author bailj
 */
public class GlobalJobListener implements JobListener {

  /** 开始执行时间 */
  public static final String START_DATE = "start_date";
  /** 错误信息 */
  public static final String ERROR_MSG = "error_msg";

  /** 租户. */
  private String tenant;

  public GlobalJobListener(String tenant) {
    super();
    this.tenant = tenant;
  }

  @Override
  public String getName() {
    return this.getClass().getSimpleName();
  }

  /**
   * 任务执行之前执行.
   */
  @Override
  public void jobToBeExecuted(JobExecutionContext context) {
    JobDataMap jobDataMap = context.getJobDetail().getJobDataMap();
    // 开始执行时间
    jobDataMap.put(START_DATE, LocalDateTime.now());
  }

  @Override
  public void jobExecutionVetoed(JobExecutionContext context) {

  }

  /**
   * 任务执行完成后执行.
   */
  @Override
  public void jobWasExecuted(JobExecutionContext context, JobExecutionException jobException) {
    JobDataMap jobDataMap = context.getJobDetail().getJobDataMap();
    LocalDateTime startDate = (LocalDateTime) jobDataMap.get(START_DATE);
    JobKey jobKey = context.getJobDetail().getKey();
    // Job执行记录.
    JobRecordCreateReq.Builder req = JobRecordCreateReq.newBuilder();
    req.setTenant(tenant).setRequestId(UUID.randomUUID().toString());
    req.setJobName(jobKey.getName()).setStartDate(TypeUtil.INSTANCE.toProtoTimestamp(startDate));
    req.setEndDate(TypeUtil.INSTANCE.toProtoTimestamp(LocalDateTime.now()));

    // 存在异常
    if (jobException != null) {
      // 定时器异常记录
      req.setState(false).setErrorMsg(jobException.toString());
      JobsClient.getSystemStub(tenant).createJobRecord(req.build());
      return;
    }
    String errorMsg = jobDataMap.getString(ERROR_MSG);
    // 执行记录插入
    if (StringUtils.isBlank(errorMsg)) {
      // 成功记录
      req.setState(true);
      JobsClient.getSystemStub(tenant).createJobRecord(req.build());
    } else {
      req.setState(false).setErrorMsg(errorMsg);
      JobsClient.getSystemStub(tenant).createJobRecord(req.build());
    }
  }

}
