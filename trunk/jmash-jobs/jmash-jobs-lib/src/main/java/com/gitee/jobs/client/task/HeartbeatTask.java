package com.gitee.jobs.client.task;


import com.gitee.jobs.client.JobsClient;
import java.lang.management.ManagementFactory;
import java.util.Map;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import jmash.jobs.protobuf.JobDefineList;
import jmash.jobs.protobuf.JobDefineModel;
import jmash.jobs.protobuf.JobDefineReq;
import jmash.jobs.protobuf.JobInstanceAssignJob;
import jmash.jobs.protobuf.JobInstanceHeartbeat;
import jmash.jobs.protobuf.JobInstanceInitReq;
import org.jboss.logging.Logger;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.impl.StdSchedulerFactory;

/**
 * 心跳任务.
 * 
 * @author CGD
 *
 */
public class HeartbeatTask extends TimerTask {

  private static SchedulerFactory schedulerFactory = new StdSchedulerFactory();

  private Logger log = Logger.getLogger(HeartbeatTask.class);

  /** 实例名称. */
  public static String getInstanceName() {
    return ManagementFactory.getRuntimeMXBean().getName().split("@")[1];
  }

  /** 任务过期时间 */
  public Map<String, Long> jobModifyMap = new ConcurrentHashMap<String, Long>();

  /** 租户. */
  private String tenant;
  /** 定时任务分类 */
  private String classify;
  /** 间隔时间m */
  private int interval;
  /** 实例名 */
  private String instanceName;
  /** 调度器 */
  private Scheduler scheduler;
  // 是否初始化
  private boolean init = false;


  /**
   * 心跳监视任务
   * 
   * @param classify 定时任务分类
   * @param interval 间隔时间（秒）
   * @throws SchedulerException
   */
  public HeartbeatTask(String tenant, String classify, int interval) throws SchedulerException {
    this.tenant = tenant;
    this.classify = classify;
    this.interval = interval;
    this.scheduler = schedulerFactory.getScheduler();
    this.instanceName = getInstanceName();
    // 注册job全局监听器
    this.scheduler.getListenerManager().addJobListener(new GlobalJobListener(tenant));
  }

  // 初始化.
  public void init() {
    // 检查实例是否存在，不存在创建
    JobInstanceInitReq.Builder req = JobInstanceInitReq.newBuilder();
    req.setRequestId(UUID.randomUUID().toString());
    req.setTenant(tenant).setClassify(classify).setInstanceName(instanceName);
    JobsClient.getSystemStub(tenant).initJobInstance(req.build());
    init = true;
  }


  @Override
  public void run() {
    log.info("run...");
    try {
      //初始化.
      if (!init) {
        init();
      }
      // 1.先心跳
      JobInstanceHeartbeat.Builder req = JobInstanceHeartbeat.newBuilder();
      req.setTenant(tenant).setClassify(classify).setInstanceName(instanceName)
          .setInterval(interval);
      JobsClient.getSystemStub(tenant).heartbeatJobInstance(req.build());

      // 2.任务分配
      JobInstanceAssignJob.Builder assign = JobInstanceAssignJob.newBuilder();
      assign.setTenant(tenant).setClassify(classify).setInstanceName(instanceName);
      JobsClient.getSystemStub(tenant).assignJobs(assign.build());

      // 3.加载或重新加载实例任务
      JobDefineReq.Builder builder = JobDefineReq.newBuilder().setTenant(tenant)
          .setInstanceName(instanceName).setClassify(classify).setHasEnable(true).setEnable(true);
      JobDefineList runJobs = JobsClient.getSystemStub(tenant).findJobDefineList(builder.build());

      Map<String, Long> tempModifyMap = new ConcurrentHashMap<String, Long>();
      tempModifyMap.putAll(jobModifyMap);
      for (JobDefineModel job : runJobs.getResultsList()) {
        tempModifyMap.remove(job.getJobName());
        if (!jobModifyMap.containsKey(job.getJobName())) {
          // 新增任务
          JobManage.addJob(tenant, scheduler, job.getJobName());
          jobModifyMap.put(job.getJobName(), job.getUpdateTime().getSeconds());
          continue;
        }
        if (jobModifyMap.get(job.getJobName()) != job.getUpdateTime().getSeconds()) {
          // 更新任务
          JobManage.updateJob(tenant, scheduler, job.getJobName());
          jobModifyMap.put(job.getJobName(), job.getUpdateTime().getSeconds());
          continue;
        }
      }

      for (String temp : tempModifyMap.keySet()) {
        // 删除任务
        JobManage.deleteJob(tenant, scheduler, temp);
        jobModifyMap.remove(temp);
      }

      // 7.启动
      if (!scheduler.isStarted()) {
        scheduler.start();
      }
    } catch (Exception e) {
      log.error("Jobs:", e);
    }
  }



}
