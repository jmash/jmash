package com.gitee.jobs.client.task;

import com.crenjoy.proto.beanutils.ProtoConvertUtils;
import com.gitee.jobs.client.JobsClient;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import jmash.jobs.protobuf.JobDefineKey;
import jmash.jobs.protobuf.JobDefineModel;
import jmash.jobs.protobuf.JobTriggerList;
import jmash.jobs.protobuf.JobTriggerModel;
import jmash.jobs.protobuf.JobTriggerReq;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;

/** 任务管理类. */
public class JobManage {

  /**
   * 添加任务
   * 
   * @param scheduler
   * @param jobName
   */
  public static void addJob(String tenant, Scheduler scheduler, String jobName)
      throws SchedulerException {
    JobTriggerReq.Builder req = JobTriggerReq.newBuilder().setTenant(tenant).setJobName(jobName);
    JobTriggerList triggers = JobsClient.getSystemStub(tenant).findJobTriggerList(req.build());
    if (triggers.getResultsCount() == 0) {
      return;
    }
    JobDefineKey.Builder key = JobDefineKey.newBuilder().setTenant(tenant).setJobName(jobName);
    JobDefineModel define = JobsClient.getSystemStub(tenant).findJobDefineById(key.build());
    JobDetail jobDetail = createJob(tenant, define);
    Set<Trigger> triggersForJob = createTrigger(define, triggers);
    scheduler.scheduleJob(jobDetail, triggersForJob, false);

  }

  /**
   * 创建Job
   * 
   * @return
   */
  public static JobDetail createJob(String tenant, JobDefineModel define) {
    JobDataMap jobDataMap = new JobDataMap();
    jobDataMap.put("event_name", define.getEventName());
    jobDataMap.put("event_body", define.getEventBody());
    return JobBuilder.newJob(TaskJob.class).withIdentity(define.getJobName(), tenant)
        .setJobData(jobDataMap).build();
  }

  /**
   * 创建触发器
   * 
   * @param define
   * @param list
   * @return
   */
  public static Set<Trigger> createTrigger(JobDefineModel define, JobTriggerList list) {
    Set<Trigger> set = new HashSet<Trigger>();
    for (JobTriggerModel entity : list.getResultsList()) {
      if (entity.getStartNow()) {
        Trigger trigger = TriggerBuilder.newTrigger()
            .forJob(define.getJobName().toString(), define.getClassify())
            .withIdentity(entity.getTriggerId().toString(), define.getClassify())
            .withSchedule(CronScheduleBuilder.cronSchedule(entity.getCron())).startNow().build();
        set.add(trigger);
      } else {
        Date date = ProtoConvertUtils.convert(entity.getStartDate(), Date.class);
        Trigger trigger = TriggerBuilder.newTrigger()
            .forJob(define.getJobName().toString(), define.getClassify())
            .withIdentity(entity.getTriggerId().toString(), define.getClassify())
            .withSchedule(CronScheduleBuilder.cronSchedule(entity.getCron())).startAt(date).build();
        set.add(trigger);
      }
    }
    return set;
  }

  /**
   * 更新任务
   * 
   * @param scheduler
   * @param jobName
   */
  public static void updateJob(String tenant, Scheduler scheduler, String jobName)
      throws SchedulerException {
    deleteJob(tenant, scheduler, jobName);
    addJob(tenant, scheduler, jobName);
  }

  /**
   * 删除任务
   * 
   * @param scheduler
   * @param jobName
   */
  public static void deleteJob(String tenant, Scheduler scheduler, String jobName)
      throws SchedulerException {
    JobKey jobKey = new JobKey(jobName, tenant);
    List<? extends Trigger> triggersForJob = scheduler.getTriggersOfJob(jobKey);
    for (Trigger trigger : triggersForJob) {
      // 停止触发器
      scheduler.pauseTrigger(trigger.getKey());
      // 移除触发器
      scheduler.unscheduleJob(trigger.getKey());
    }
    // 删除任务
    scheduler.deleteJob(jobKey);
  }

}
