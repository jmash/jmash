package com.gitee.jobs.client.task;

import com.gitee.jobs.client.event.JobsEvent;
import jakarta.enterprise.event.Event;
import jakarta.enterprise.inject.spi.CDI;
import jakarta.enterprise.util.TypeLiteral;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;

/** TaskJob. */
public class TaskJob implements Job {

  Event<JobsEvent> event;

  /** CDI 获取Event. */
  public Event<JobsEvent> getEvent() {
    if (event == null) {
      TypeLiteral<Event<JobsEvent>> eventType = new TypeLiteral<Event<JobsEvent>>() {
        private static final long serialVersionUID = 1L;

      };
      event = CDI.current().select(eventType).get();
    }
    return event;
  }


  @Override
  public void execute(JobExecutionContext context) throws JobExecutionException {
    JobDataMap jobDataMap = context.getJobDetail().getJobDataMap();
    JobKey jobKey = context.getJobDetail().getKey();
    String jobName = jobKey.getName();
    String tenant = jobKey.getGroup();
    String eventName = jobDataMap.getString("event_name");
    String eventBody = jobDataMap.getString("event_body");
    // 同步调用.
    getEvent().fire(new JobsEvent(jobName, tenant, eventName, eventBody));
  }

}
