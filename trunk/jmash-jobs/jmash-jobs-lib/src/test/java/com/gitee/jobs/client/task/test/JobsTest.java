package com.gitee.jobs.client.task.test;

import com.gitee.jobs.client.task.HeartbeatTask;
import java.util.Timer;
import java.util.concurrent.CountDownLatch;
import org.quartz.SchedulerException;

public class JobsTest {

  static Timer timer = new Timer();
  
  public static void main(String[] args) throws SchedulerException, InterruptedException {
    final CountDownLatch latchFinish = new CountDownLatch(1);
    // 定时任务.
    int interval = 10;
    HeartbeatTask heartbeat = new HeartbeatTask("jobs", "jobs", interval);
    heartbeat.init();
    timer.schedule(heartbeat, interval * 1000, interval * 1000);
    // latchFinish.countDown();
    latchFinish.await();
  }
  
}
