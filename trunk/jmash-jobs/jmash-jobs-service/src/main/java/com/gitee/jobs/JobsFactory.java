package com.gitee.jobs;

import com.gitee.jobs.service.JobDefineRead;
import com.gitee.jobs.service.JobDefineWrite;
import com.gitee.jobs.service.JobInstanceRead;
import com.gitee.jobs.service.JobInstanceWrite;
import com.gitee.jobs.service.JobRecordRead;
import com.gitee.jobs.service.JobRecordWrite;
import com.gitee.jobs.service.JobTriggerRead;
import com.gitee.jobs.service.JobTriggerWrite;
import jakarta.enterprise.inject.spi.CDI;

/**
 * 模块服务工厂类.
 *
 * @author CGD
 *
 */
public class JobsFactory {

  public static JobDefineRead getJobDefineRead(String tenant) {
    return CDI.current().select(JobDefineRead.class).get().setTenant(tenant);
  }

  public static JobInstanceRead getJobInstanceRead(String tenant) {
    return CDI.current().select(JobInstanceRead.class).get().setTenant(tenant);
  }

  public static JobRecordRead getJobRecordRead(String tenant) {
    return CDI.current().select(JobRecordRead.class).get().setTenant(tenant);
  }

  public static JobTriggerRead getJobTriggerRead(String tenant) {
    return CDI.current().select(JobTriggerRead.class).get().setTenant(tenant);
  }

  public static JobDefineWrite getJobDefineWrite(String tenant) {
    return CDI.current().select(JobDefineWrite.class).get().setTenant(tenant);
  }

  public static JobInstanceWrite getJobInstanceWrite(String tenant) {
    return CDI.current().select(JobInstanceWrite.class).get().setTenant(tenant);
  }

  public static JobRecordWrite getJobRecordWrite(String tenant) {
    return CDI.current().select(JobRecordWrite.class).get().setTenant(tenant);
  }

  public static JobTriggerWrite getJobTriggerWrite(String tenant) {
    return CDI.current().select(JobTriggerWrite.class).get().setTenant(tenant);
  }

  /** CDI回收实例. */
  public static void destroy(Object instance) {
    if (null != instance) {
      CDI.current().destroy(instance);
    }
  }
}
