
package com.gitee.jobs;

import com.crenjoy.proto.mapper.BeanMapper;
import com.crenjoy.proto.mapper.ProtoMapper;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * Jobs Mapper.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 *
 */
@Mapper
public interface JobsMapper extends BeanMapper, ProtoMapper {

  JobsMapper INSTANCE = Mappers.getMapper(JobsMapper.class);

}
