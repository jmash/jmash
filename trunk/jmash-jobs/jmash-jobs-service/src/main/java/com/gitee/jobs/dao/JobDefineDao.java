
package com.gitee.jobs.dao;

import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.orm.jpa.BaseDao;
import com.gitee.jmash.core.orm.jpa.SqlBuilder;
import com.gitee.jmash.core.orm.jpa.TenantEntityManager;
import com.gitee.jobs.entity.JobDefineEntity;
import com.gitee.jobs.model.JobDefineTotal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import jmash.jobs.protobuf.InstanceState;
import jmash.jobs.protobuf.JobDefineReq;
import org.apache.commons.lang3.StringUtils;

/**
 * JobDefine实体的Dao层（使用JPA实现）.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public class JobDefineDao extends BaseDao<JobDefineEntity, UUID> {

  public JobDefineDao() {
    super();
  }

  public JobDefineDao(TenantEntityManager tem) {
    super(tem);
  }

  /**
   * 查询任务分类列表（去重）.
   */
  @SuppressWarnings("all")
  public List<String> findClassifyList() {
    String sql = "select s.classify from JobDefineEntity s  group by s.classify ";
    List<String> list = (List) this.findList(sql);
    return list;
  }

  /**
   * 查询记录数.
   */
  public Integer findCount(JobDefineReq req) {
    SqlBuilder sqlBuilder = createSql(req);
    String query = sqlBuilder.getQuerySql("select count(s) ");
    Object obj = this.findSingleResultByParams(query, sqlBuilder.getParams());
    return (obj == null) ? 0 : Integer.valueOf(obj.toString());
  }

  /**
   * 综合查询.
   */
  public List<JobDefineEntity> findListByReq(JobDefineReq req) {
    SqlBuilder sqlBuilder = createSql(req);
    String query = sqlBuilder.getQuerySql("select s ");
    return this.findListByParams(query, sqlBuilder.getParams());
  }

  /**
   * 综合查询Page.
   */
  public DtoPage<JobDefineEntity, JobDefineTotal> findPageByReq(JobDefineReq req) {
    SqlBuilder sqlBuilder = createSql(req);
    String query = sqlBuilder.getQuerySql("select s ");
    String totalQuery = sqlBuilder.getTotalSql("select count(s) as totalSize ");
    return this.findDtoPageByParams(req.getCurPage(), req.getPageSize(), query, totalQuery,
        JobDefineTotal.class, sqlBuilder.getParams());
  }

  /** Create SQL By Req . */
  public SqlBuilder createSql(JobDefineReq req) {
    StringBuilder sql = new StringBuilder(" from JobDefineEntity s where 1=1  ");
    Map<String, Object> params = new HashMap<String, Object>();


    if (StringUtils.isNotBlank(req.getClassify())) {
      sql.append(" and s.classify = :classify");
      params.put("classify", req.getClassify());
    }

    if (req.getHasEnable()) {
      sql.append(" and s.enable = :enable");
      params.put("enable", req.getEnable());
    }

    if (req.getHasAssign()) {
      sql.append(" and s.assign = :assign");
      params.put("assign", req.getAssign());
    }

    if (StringUtils.isNotBlank(req.getInstanceName())) {
      sql.append(" and s.instanceName = :instanceName");
      params.put("instanceName", req.getInstanceName());
    }

    String orderSql = " order by s.classify , s.createTime desc ";
    if (StringUtils.isNotBlank(req.getOrderName())) {
      orderSql = String.format(" order by %s %s ", req.getOrderName(),
          req.getOrderAsc() ? " asc " : " desc ");
    }

    return SqlBuilder.build().setSql(sql).setParams(params).setOrderSql(orderSql);
  }

  /**
   * 查询未被分配的任务.
   */
  public List<JobDefineEntity> unassignedJobs(String classify) {
    String sql = "select s from JobDefineEntity s where s.classify =?1 and s.enable =?2 "
        + " and not exists ( select x "
        + " from JobInstanceEntity x where x.instanceName = s.instanceName and x.classify= s.classify  and x.state<>?3) "
        + " order by s.weight desc ";
    return this.findList(sql, classify, true, InstanceState.stop);
  }


}
