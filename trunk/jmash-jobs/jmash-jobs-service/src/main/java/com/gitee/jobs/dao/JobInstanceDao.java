
package com.gitee.jobs.dao;

import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.orm.jpa.BaseDao;
import com.gitee.jmash.core.orm.jpa.SqlBuilder;
import com.gitee.jmash.core.orm.jpa.TenantEntityManager;
import com.gitee.jobs.entity.JobInstanceEntity;
import com.gitee.jobs.entity.JobInstanceEntity.JobInstancePk;
import com.gitee.jobs.model.JobInstanceTotal;
import com.gitee.jobs.model.JobInstanceWeight;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import jmash.jobs.protobuf.InstanceState;
import jmash.jobs.protobuf.JobInstanceReq;
import org.apache.commons.lang3.StringUtils;

/**
 * JobInstance实体的Dao层（使用JPA实现）.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public class JobInstanceDao extends BaseDao<JobInstanceEntity, JobInstancePk> {

  public JobInstanceDao() {
    super();
  }

  public JobInstanceDao(TenantEntityManager tem) {
    super(tem);
  }


  /**
   * 查询记录数.
   */
  public Integer findCount(JobInstanceReq req) {
    SqlBuilder sqlBuilder = createSql(req);
    String query = sqlBuilder.getQuerySql("select count(s) ");
    Object obj = this.findSingleResultByParams(query, sqlBuilder.getParams());
    return (obj == null) ? 0 : Integer.valueOf(obj.toString());
  }

  /**
   * 综合查询.
   */
  public List<JobInstanceEntity> findListByReq(JobInstanceReq req) {
    SqlBuilder sqlBuilder = createSql(req);
    String query = sqlBuilder.getQuerySql("select s ");
    return this.findListByParams(query, sqlBuilder.getParams());
  }

  /**
   * 综合查询Page.
   */
  public DtoPage<JobInstanceEntity, JobInstanceTotal> findPageByReq(JobInstanceReq req) {
    SqlBuilder sqlBuilder = createSql(req);
    String query = sqlBuilder.getQuerySql("select s ");
    String totalQuery = sqlBuilder.getTotalSql("select count(s) as totalSize ");
    return this.findDtoPageByParams(req.getCurPage(), req.getPageSize(), query, totalQuery,
        JobInstanceTotal.class, sqlBuilder.getParams());
  }

  /** Create SQL By Req . */
  public SqlBuilder createSql(JobInstanceReq req) {
    StringBuilder sql = new StringBuilder(" from JobInstanceEntity s where 1=1  ");
    Map<String, Object> params = new HashMap<String, Object>();

    if (StringUtils.isNotBlank(req.getClassify())) {
      sql.append(" and s.classify = :classify");
      params.put("classify", req.getClassify());
    }

    if (req.getHasState()) {
      sql.append(" and s.state = :state");
      params.put("state", req.getState());
    }

    String orderSql = " order by s.heartbeat desc ";
    if (StringUtils.isNotBlank(req.getOrderName())) {
      orderSql = String.format(" order by %s %s ", req.getOrderName(),
          req.getOrderAsc() ? " asc " : " desc ");
    }
    return SqlBuilder.build().setSql(sql).setParams(params).setOrderSql(orderSql);
  }

  /**
   * 停止过期实例.
   */
  public int stopDueInstance(String classify) {
    String sql =
        "update  JobInstanceEntity s  set s.state =?1 where s.classify = ?2 and s.state <> ?3 and s.dueDate <?4  ";
    return this.executeUpdate(sql, InstanceState.stop, classify, InstanceState.stop,
        LocalDateTime.now());
  }

  /**
   * 查询实例运行的任务权重.
   */
  public List<JobInstanceWeight> findInstanceWeight(String classify) {
    String sql =
        "select x.instanceName as instanceName,sum(s.weight) as weight from JobInstanceEntity x left join  JobDefineEntity s "
            + " on x.instanceName = s.instanceName and x.classify = s.classify and s.enable =?1 "
            + " where x.classify =?2 and x.state<>?3 " + " group by x.instanceName ";
    return this.findDtoList(sql, JobInstanceWeight.class, true, classify, InstanceState.stop);
  }

}
