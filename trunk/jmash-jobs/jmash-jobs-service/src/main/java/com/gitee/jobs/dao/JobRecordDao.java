
package com.gitee.jobs.dao;

import com.crenjoy.proto.utils.TypeUtil;
import com.gitee.jmash.common.utils.UUIDUtil;
import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.orm.jpa.BaseDao;
import com.gitee.jmash.core.orm.jpa.SqlBuilder;
import com.gitee.jmash.core.orm.jpa.TenantEntityManager;
import com.gitee.jobs.entity.JobRecordEntity;
import com.gitee.jobs.model.JobRecordTotal;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import jmash.jobs.protobuf.JobRecordReq;
import org.apache.commons.lang3.StringUtils;

/**
 * JobRecord实体的Dao层（使用JPA实现）.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public class JobRecordDao extends BaseDao<JobRecordEntity, UUID> {

  public JobRecordDao() {
    super();
  }

  public JobRecordDao(TenantEntityManager tem) {
    super(tem);
  }

  public Integer batchDelete(int year) {
    return this.executeUpdate("delete from JobRecordEntity s where s.startDate <=?1 ",
        LocalDateTime.now().plusYears(0 - year));
  }

  /**
   * 查询记录数.
   */
  public Integer findCount(JobRecordReq req) {
    SqlBuilder sqlBuilder = createSql(req);
    String query = sqlBuilder.getQuerySql("select count(s) ");
    Object obj = this.findSingleResultByParams(query, sqlBuilder.getParams());
    return (obj == null) ? 0 : Integer.valueOf(obj.toString());
  }

  /**
   * 综合查询.
   */
  public List<JobRecordEntity> findListByReq(JobRecordReq req) {
    SqlBuilder sqlBuilder = createSql(req);
    String query = sqlBuilder.getQuerySql("select s ");
    return this.findListByParams(query, sqlBuilder.getParams());
  }

  /**
   * 综合查询Page.
   */
  public DtoPage<JobRecordEntity, JobRecordTotal> findPageByReq(JobRecordReq req) {
    SqlBuilder sqlBuilder = createSql(req);
    String query = sqlBuilder.getQuerySql("select s ");
    String totalQuery = sqlBuilder.getTotalSql("select count(s) as totalSize ");
    return this.findDtoPageByParams(req.getCurPage(), req.getPageSize(), query, totalQuery,
        JobRecordTotal.class, sqlBuilder.getParams());
  }

  /** Create SQL By Req . */
  public SqlBuilder createSql(JobRecordReq req) {
    StringBuilder sql = new StringBuilder(" from JobRecordEntity s where 1=1  ");
    Map<String, Object> params = new HashMap<String, Object>();

    if (StringUtils.isNotBlank(req.getJobName())) {
      sql.append(" and s.jobName = :jobName");
      params.put("jobName", UUIDUtil.fromString(req.getJobName()));
    }

    if (req.getHasState()) {
      sql.append(" and s.state = :state");
      params.put("state", req.getState());
    }

    if (!TypeUtil.isEmpty(req.getStartDate())) {
      sql.append(" and s.startDate >= :startDate");
      params.put("startDate", req.getStartDate());
    }

    if (!TypeUtil.isEmpty(req.getEndDate())) {
      sql.append(" and s.startDate <= :endDate");
      params.put("endDate", req.getEndDate());
    }

    String orderSql = " order by s.startDate desc ";
    if (StringUtils.isNotBlank(req.getOrderName())) {
      orderSql = String.format(" order by %s %s ", req.getOrderName(),
          req.getOrderAsc() ? " asc " : " desc ");
    }

    return SqlBuilder.build().setSql(sql).setParams(params).setOrderSql(orderSql);
  }


}
