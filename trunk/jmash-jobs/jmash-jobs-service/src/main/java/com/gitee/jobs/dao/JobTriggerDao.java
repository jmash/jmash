
package com.gitee.jobs.dao;

import com.gitee.jmash.common.utils.UUIDUtil;
import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.orm.jpa.BaseDao;
import com.gitee.jmash.core.orm.jpa.SqlBuilder;
import com.gitee.jmash.core.orm.jpa.TenantEntityManager;
import com.gitee.jobs.entity.JobTriggerEntity;
import com.gitee.jobs.model.JobTriggerTotal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import jmash.jobs.protobuf.JobTriggerReq;
import org.apache.commons.lang3.StringUtils;

/**
 * JobTrigger实体的Dao层（使用JPA实现）.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public class JobTriggerDao extends BaseDao<JobTriggerEntity, UUID> {

  public JobTriggerDao() {
    super();
  }

  public JobTriggerDao(TenantEntityManager tem) {
    super(tem);
  }
  
  /** 删除任务下的全部触发器. */
  public int deleteByJobName(UUID jobName) {
    return this.executeUpdate("delete from JobTriggerEntity s where s.jobName=?1 ", jobName);
  }


  /**
   * 查询记录数.
   */
  public Integer findCount(JobTriggerReq req) {
    SqlBuilder sqlBuilder = createSql(req);
    String query = sqlBuilder.getQuerySql("select count(s) ");
    Object obj = this.findSingleResultByParams(query, sqlBuilder.getParams());
    return (obj == null) ? 0 : Integer.valueOf(obj.toString());
  }

  /**
   * 综合查询.
   */
  public List<JobTriggerEntity> findListByReq(JobTriggerReq req) {
    SqlBuilder sqlBuilder = createSql(req);
    String query = sqlBuilder.getQuerySql("select s ");
    return this.findListByParams(query, sqlBuilder.getParams());
  }

  /**
   * 综合查询Page.
   */
  public DtoPage<JobTriggerEntity, JobTriggerTotal> findPageByReq(JobTriggerReq req) {
    SqlBuilder sqlBuilder = createSql(req);
    String query = sqlBuilder.getQuerySql("select s ");
    String totalQuery = sqlBuilder.getTotalSql("select count(s) as totalSize ");
    return this.findDtoPageByParams(req.getCurPage(), req.getPageSize(), query, totalQuery,
        JobTriggerTotal.class, sqlBuilder.getParams());
  }

  /** Create SQL By Req . */
  public SqlBuilder createSql(JobTriggerReq req) {
    StringBuilder sql = new StringBuilder(" from JobTriggerEntity s where 1=1  ");
    Map<String, Object> params = new HashMap<String, Object>();

    if (StringUtils.isNotBlank(req.getJobName())) {
      sql.append(" and s.jobName = :jobName");
      params.put("jobName", UUIDUtil.fromString(req.getJobName()));
    }

    String orderSql = "";
    if (StringUtils.isNotBlank(req.getOrderName())) {
      orderSql = String.format(" order by %s %s ", req.getOrderName(),
          req.getOrderAsc() ? " asc " : " desc ");
    }

    return SqlBuilder.build().setSql(sql).setParams(params).setOrderSql(orderSql);
  }


}
