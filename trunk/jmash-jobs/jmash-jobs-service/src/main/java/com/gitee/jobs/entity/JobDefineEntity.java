
package com.gitee.jobs.entity;


import com.gitee.jmash.core.orm.jpa.entity.BasicEntity;
import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import java.util.UUID;


/**
 * 定时任务 计划任务定义;表 .
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Entity
@Table(name = "job_define")
public class JobDefineEntity extends BasicEntity<UUID> {

  private static final long serialVersionUID = 1L;

  /** 计划名称. */
  private UUID jobName = UUID.randomUUID();
  /** 分类. */
  private String classify;
  /** 计划任务中文名称. */
  private String jobCnName;
  /** 计划任务描述. */
  private String jobDescribe;
  /** 任务运行权重. */
  private Integer weight;
  /** 任务触发事件名称. */
  private String eventName;
  /** 任务触发事件JOSN. */
  private String eventBody;
  /** 运行实例. */
  private String instanceName;
  /** 是否启用. */
  private Boolean enable = false;

  /**
   * 默认构造函数.
   */
  public JobDefineEntity() {
    super();
  }

  /** 实体主键. */
  @Transient
  @JsonbTransient
  public UUID getEntityPk() {
    return this.jobName;
  }

  /** 实体主键. */
  public void setEntityPk(UUID pk) {
    this.jobName = pk;
  }

  /** 计划名称(JobName). */
  @Id
  @Column(name = "job_name", nullable = false, columnDefinition = "BINARY(16)")
  public UUID getJobName() {
    return jobName;
  }

  /** 计划名称(JobName). */
  public void setJobName(UUID jobName) {
    this.jobName = jobName;
  }

  /** 分类(Classify). */

  @Column(name = "classify_", nullable = false)
  public String getClassify() {
    return classify;
  }

  /** 分类(Classify). */
  public void setClassify(String classify) {
    this.classify = classify;
  }

  /** 计划任务中文名称(JobCnName). */

  @Column(name = "job_cn_name", nullable = false)
  public String getJobCnName() {
    return jobCnName;
  }

  /** 计划任务中文名称(JobCnName). */
  public void setJobCnName(String jobCnName) {
    this.jobCnName = jobCnName;
  }

  /** 计划任务描述(JobDescribe). */
  @Column(name = "job_describe")
  public String getJobDescribe() {
    return jobDescribe;
  }

  /** 计划任务描述(JobDescribe). */
  public void setJobDescribe(String jobDescribe) {
    this.jobDescribe = jobDescribe;
  }

  /** 任务运行权重(Weight). */

  @Column(name = "weight_", nullable = false)
  public Integer getWeight() {
    return weight;
  }

  /** 任务运行权重(Weight). */
  public void setWeight(Integer weight) {
    this.weight = weight;
  }

  /** 任务触发事件名称(EventName). */

  @Column(name = "event_name", nullable = false)
  public String getEventName() {
    return eventName;
  }

  /** 任务触发事件名称(EventName). */
  public void setEventName(String eventName) {
    this.eventName = eventName;
  }

  /** 任务触发事件JOSN(EventBody). */
  @Column(name = "event_body")
  public String getEventBody() {
    return eventBody;
  }

  /** 任务触发事件JOSN(EventBody). */
  public void setEventBody(String eventBody) {
    this.eventBody = eventBody;
  }

  /** 运行实例(InstanceName). */
  @Column(name = "instance_name")
  public String getInstanceName() {
    return instanceName;
  }

  /** 运行实例(InstanceName). */
  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }

  /** 是否启用(Enable). */

  @Column(name = "enable_", nullable = false)
  public Boolean getEnable() {
    return enable;
  }

  /** 是否启用(Enable). */
  public void setEnable(Boolean enable) {
    this.enable = enable;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((jobName == null) ? 0 : jobName.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final JobDefineEntity other = (JobDefineEntity) obj;
    if (jobName == null) {
      if (other.jobName != null) {
        return false;
      }
    } else if (!jobName.equals(other.jobName)) {
      return false;
    }
    return true;
  }



}
