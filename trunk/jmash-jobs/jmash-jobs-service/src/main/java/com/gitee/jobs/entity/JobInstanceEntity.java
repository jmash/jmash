
package com.gitee.jobs.entity;


import com.gitee.jobs.entity.JobInstanceEntity.JobInstancePk;
import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import jakarta.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import jmash.jobs.protobuf.InstanceState;

/**
 * 定时任务 运行实例;表 .
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Entity
@Table(name = "job_instance")
@IdClass(JobInstancePk.class)
public class JobInstanceEntity implements Serializable {

  private static final long serialVersionUID = 1L;

  /** 运行实例. */
  private String instanceName;
  /** 分类. */
  private String classify;
  /** 心跳时间. */
  private LocalDateTime heartbeat;
  /** 过期时间. */
  private LocalDateTime dueDate;
  /** 状态. */
  private InstanceState state = InstanceState.start;

  /**
   * 默认构造函数.
   */
  public JobInstanceEntity() {
    super();
  }

  public JobInstanceEntity(String classify, String instanceName) {
    super();
    this.instanceName = instanceName;
    this.classify = classify;
  }

  /** 实体主键. */
  @Transient
  @JsonbTransient
  public JobInstancePk getEntityPk() {
    JobInstancePk pk = new JobInstancePk();
    pk.setClassify(this.classify);
    pk.setInstanceName(this.instanceName);
    return pk;
  }

  /** 实体主键. */
  public void setEntityPk(JobInstancePk pk) {
    this.classify = pk.getClassify();
    this.instanceName = pk.getInstanceName();
  }

  /** 运行实例(InstanceName). */
  @Id

  @Column(name = "instance_name", nullable = false)
  public String getInstanceName() {
    return instanceName;
  }

  /** 运行实例(InstanceName). */
  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }

  /** 分类(Classify). */
  @Id

  @Column(name = "classify_", nullable = false)
  public String getClassify() {
    return classify;
  }

  /** 分类(Classify). */
  public void setClassify(String classify) {
    this.classify = classify;
  }

  /** 心跳时间(Heartbeat). */
  @Column(name = "heartbeat_")
  public LocalDateTime getHeartbeat() {
    return heartbeat;
  }

  /** 心跳时间(Heartbeat). */
  public void setHeartbeat(LocalDateTime heartbeat) {
    this.heartbeat = heartbeat;
  }

  /** 过期时间(DueDate). */
  @Column(name = "due_date")
  public LocalDateTime getDueDate() {
    return dueDate;
  }

  /** 过期时间(DueDate). */
  public void setDueDate(LocalDateTime dueDate) {
    this.dueDate = dueDate;
  }

  /** 状态(State). */
  @Column(name = "state_")
  @Enumerated(EnumType.STRING)
  public InstanceState getState() {
    return state;
  }

  /** 状态(State). */
  public void setState(InstanceState state) {
    this.state = state;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((getEntityPk() == null) ? 0 : getEntityPk().hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final JobInstanceEntity other = (JobInstanceEntity) obj;
    if (getEntityPk() == null) {
      if (other.getEntityPk() != null) {
        return false;
      }
    } else if (!getEntityPk().equals(other.getEntityPk())) {
      return false;
    }
    return true;
  }


  /**
   * JobInstancePK（直接对应数据库的配置 由生成工具产生）.
   *
   * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
   */
  @Embeddable
  public static class JobInstancePk implements Serializable {

    private static final long serialVersionUID = 1L;

    /** 分类. */
    @NotNull
    private String classify;
    /** 运行实例. */
    @NotNull
    private String instanceName;

    public JobInstancePk() {
      super();
    }

    public JobInstancePk(@NotNull String classify, @NotNull String instanceName) {
      super();
      this.classify = classify;
      this.instanceName = instanceName;
    }

    /** 分类(Classify). */
    @Column(name = "classify_", nullable = false)
    public String getClassify() {
      return classify;
    }

    /** 分类(Classify). */
    public void setClassify(String classify) {
      this.classify = classify;
    }

    /** 运行实例(InstanceName). */
    @Column(name = "instance_name", nullable = false)
    public String getInstanceName() {
      return instanceName;
    }

    /** 运行实例(InstanceName). */
    public void setInstanceName(String instanceName) {
      this.instanceName = instanceName;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((classify == null) ? 0 : classify.hashCode());
      result = prime * result + ((instanceName == null) ? 0 : instanceName.hashCode());
      return result;
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj) {
        return true;
      }
      if (obj == null) {
        return false;
      }
      if (getClass() != obj.getClass()) {
        return false;
      }
      final JobInstancePk other = (JobInstancePk) obj;

      if (classify == null) {
        if (other.classify != null) {
          return false;
        }
      } else if (!classify.equals(other.classify)) {
        return false;
      }
      if (instanceName == null) {
        if (other.instanceName != null) {
          return false;
        }
      } else if (!instanceName.equals(other.instanceName)) {
        return false;
      }
      return true;
    }
  }

}
