
package com.gitee.jobs.entity;


import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Lob;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;



/**
 * 定时任务 计划任务记录;表 .
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Entity
@Table(name = "job_record")
public class JobRecordEntity implements Serializable {

  private static final long serialVersionUID = 1L;

  /** 计划执行ID. */
  private UUID recordId = UUID.randomUUID();
  /** 计划名称. */
  private UUID jobName;
  /** 开始时间. */
  private LocalDateTime startDate;
  /** 结束时间. */
  private LocalDateTime endDate;
  /** 错误信息. */
  private String errorMsg;
  /** 状态. */
  private Boolean state = false;

  /**
   * 默认构造函数.
   */
  public JobRecordEntity() {
    super();
  }

  /** 实体主键. */
  @Transient
  @JsonbTransient
  public UUID getEntityPk() {
    return this.recordId;
  }

  /** 实体主键. */
  public void setEntityPk(UUID pk) {
    this.recordId = pk;
  }

  /** 计划执行ID(RecordId). */
  @Id
  @Column(name = "record_id", nullable = false, columnDefinition = "BINARY(16)")
  public UUID getRecordId() {
    return recordId;
  }

  /** 计划执行ID(RecordId). */
  public void setRecordId(UUID recordId) {
    this.recordId = recordId;
  }

  /** 计划名称(JobName). */
  @Column(name = "job_name", nullable = false, columnDefinition = "BINARY(16)")
  public UUID getJobName() {
    return jobName;
  }

  /** 计划名称(JobName). */
  public void setJobName(UUID jobName) {
    this.jobName = jobName;
  }

  /** 开始时间(StartDate). */

  @Column(name = "start_date", nullable = false)
  public LocalDateTime getStartDate() {
    return startDate;
  }

  /** 开始时间(StartDate). */
  public void setStartDate(LocalDateTime startDate) {
    this.startDate = startDate;
  }

  /** 结束时间(EndDate). */
  @Column(name = "end_date")
  public LocalDateTime getEndDate() {
    return endDate;
  }

  /** 结束时间(EndDate). */
  public void setEndDate(LocalDateTime endDate) {
    this.endDate = endDate;
  }

  /** 错误信息(ErrorMsg). */
  @Column(name = "error_msg")
  @Lob
  public String getErrorMsg() {
    return errorMsg;
  }

  /** 错误信息(ErrorMsg). */
  public void setErrorMsg(String errorMsg) {
    this.errorMsg = errorMsg;
  }

  /** 状态(State). */

  @Column(name = "state_", nullable = false)
  public Boolean getState() {
    return state;
  }

  /** 状态(State). */
  public void setState(Boolean state) {
    this.state = state;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((recordId == null) ? 0 : recordId.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final JobRecordEntity other = (JobRecordEntity) obj;
    if (recordId == null) {
      if (other.recordId != null) {
        return false;
      }
    } else if (!recordId.equals(other.recordId)) {
      return false;
    }
    return true;
  }



}
