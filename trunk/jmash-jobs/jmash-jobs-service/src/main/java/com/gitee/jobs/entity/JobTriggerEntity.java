
package com.gitee.jobs.entity;


import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;



/**
 * 定时任务 任务触发器;表 .
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Entity
@Table(name = "job_trigger")
public class JobTriggerEntity implements Serializable {

  private static final long serialVersionUID = 1L;

  /** 触发ID. */
  private UUID triggerId = UUID.randomUUID();
  /** 计划名称. */
  private UUID jobName;
  /** 是否当前日期开始. */
  private Boolean startNow = false;
  /** 启动时间. */
  private LocalDateTime startDate;
  /** cron表达式. */
  private String cron;
  /** cron描述. */
  private String cronName;
  /** 是否自定义. */
  private Boolean isCustom;

  /**
   * 默认构造函数.
   */
  public JobTriggerEntity() {
    super();
  }

  /** 实体主键. */
  @Transient
  @JsonbTransient
  public UUID getEntityPk() {
    return this.triggerId;
  }

  /** 实体主键. */
  public void setEntityPk(UUID pk) {
    this.triggerId = pk;
  }

  /** 触发ID(TriggerId). */
  @Id
  @Column(name = "trigger_id", nullable = false, columnDefinition = "BINARY(16)")
  public UUID getTriggerId() {
    return triggerId;
  }

  /** 触发ID(TriggerId). */
  public void setTriggerId(UUID triggerId) {
    this.triggerId = triggerId;
  }

  /** 计划名称(JobName). */
  @Column(name = "job_name", nullable = false, columnDefinition = "BINARY(16)")
  public UUID getJobName() {
    return jobName;
  }

  /** 计划名称(JobName). */
  public void setJobName(UUID jobName) {
    this.jobName = jobName;
  }

  /** 是否当前日期开始(StartNow). */

  @Column(name = "start_now", nullable = false)
  public Boolean getStartNow() {
    return startNow;
  }

  /** 是否当前日期开始(StartNow). */
  public void setStartNow(Boolean startNow) {
    this.startNow = startNow;
  }

  /** 启动时间(StartDate). */
  @Column(name = "start_date")
  public LocalDateTime getStartDate() {
    return startDate;
  }

  /** 启动时间(StartDate). */
  public void setStartDate(LocalDateTime startDate) {
    this.startDate = startDate;
  }

  /** cron表达式(Cron). */
  @Column(name = "cron_")
  public String getCron() {
    return cron;
  }

  /** cron表达式(Cron). */
  public void setCron(String cron) {
    this.cron = cron;
  }

  /** cron描述(CronName). */
  @Column(name = "cron_name")
  public String getCronName() {
    return cronName;
  }

  /** cron描述(CronName). */
  public void setCronName(String cronName) {
    this.cronName = cronName;
  }

  /** 是否自定义(IsCustom). */
  @Column(name = "is_custom")
  public Boolean getIsCustom() {
    return isCustom;
  }

  /** 是否自定义(IsCustom). */
  public void setIsCustom(Boolean isCustom) {
    this.isCustom = isCustom;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((triggerId == null) ? 0 : triggerId.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final JobTriggerEntity other = (JobTriggerEntity) obj;
    if (triggerId == null) {
      if (other.triggerId != null) {
        return false;
      }
    } else if (!triggerId.equals(other.triggerId)) {
      return false;
    }
    return true;
  }



}
