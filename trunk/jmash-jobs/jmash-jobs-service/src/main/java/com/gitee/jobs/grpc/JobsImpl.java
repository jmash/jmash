
package com.gitee.jobs.grpc;

import com.gitee.jmash.common.utils.UUIDUtil;
import com.gitee.jmash.common.utils.VersionUtil;
import com.gitee.jmash.core.grpc.cdi.GrpcService;
import com.gitee.jmash.core.lib.ProtoEnumUtil;
import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jobs.JobsFactory;
import com.gitee.jobs.entity.JobDefineEntity;
import com.gitee.jobs.entity.JobInstanceEntity;
import com.gitee.jobs.entity.JobInstanceEntity.JobInstancePk;
import com.gitee.jobs.entity.JobRecordEntity;
import com.gitee.jobs.entity.JobTriggerEntity;
import com.gitee.jobs.mapper.JobDefineMapper;
import com.gitee.jobs.mapper.JobInstanceMapper;
import com.gitee.jobs.mapper.JobRecordMapper;
import com.gitee.jobs.mapper.JobTriggerMapper;
import com.gitee.jobs.model.JobDefineTotal;
import com.gitee.jobs.model.JobInstanceTotal;
import com.gitee.jobs.model.JobRecordTotal;
import com.gitee.jobs.service.JobDefineRead;
import com.gitee.jobs.service.JobDefineWrite;
import com.gitee.jobs.service.JobInstanceRead;
import com.gitee.jobs.service.JobInstanceWrite;
import com.gitee.jobs.service.JobRecordRead;
import com.gitee.jobs.service.JobRecordWrite;
import com.gitee.jobs.service.JobTriggerRead;
import com.gitee.jobs.service.JobTriggerWrite;
import com.google.protobuf.BoolValue;
import com.google.protobuf.Empty;
import com.google.protobuf.EnumValue;
import com.google.protobuf.Int32Value;
import com.google.protobuf.StringValue;
import io.grpc.Status;
import io.grpc.stub.StreamObserver;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import jmash.jobs.protobuf.JobDefineCreateReq;
import jmash.jobs.protobuf.JobDefineEnableKey;
import jmash.jobs.protobuf.JobDefineKey;
import jmash.jobs.protobuf.JobDefineKeyList;
import jmash.jobs.protobuf.JobDefineList;
import jmash.jobs.protobuf.JobDefineModel;
import jmash.jobs.protobuf.JobDefinePage;
import jmash.jobs.protobuf.JobDefineReq;
import jmash.jobs.protobuf.JobDefineUpdateReq;
import jmash.jobs.protobuf.JobInstanceAssignJob;
import jmash.jobs.protobuf.JobInstanceHeartbeat;
import jmash.jobs.protobuf.JobInstanceInitReq;
import jmash.jobs.protobuf.JobInstanceKey;
import jmash.jobs.protobuf.JobInstanceList;
import jmash.jobs.protobuf.JobInstanceModel;
import jmash.jobs.protobuf.JobInstancePage;
import jmash.jobs.protobuf.JobInstanceReq;
import jmash.jobs.protobuf.JobRecordCreateReq;
import jmash.jobs.protobuf.JobRecordDelReq;
import jmash.jobs.protobuf.JobRecordKey;
import jmash.jobs.protobuf.JobRecordList;
import jmash.jobs.protobuf.JobRecordModel;
import jmash.jobs.protobuf.JobRecordPage;
import jmash.jobs.protobuf.JobRecordReq;
import jmash.jobs.protobuf.JobTriggerCreateReq;
import jmash.jobs.protobuf.JobTriggerKey;
import jmash.jobs.protobuf.JobTriggerKeyList;
import jmash.jobs.protobuf.JobTriggerList;
import jmash.jobs.protobuf.JobTriggerModel;
import jmash.jobs.protobuf.JobTriggerReq;
import jmash.jobs.protobuf.JobTriggerUpdateReq;
import jmash.protobuf.CustomEnumValue;
import jmash.protobuf.CustomEnumValueMap;
import jmash.protobuf.Entry;
import jmash.protobuf.EntryList;
import jmash.protobuf.EnumEntryReq;
import jmash.protobuf.EnumValueList;
import jmash.protobuf.TenantReq;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;

/** Grpc服务实现. */
@GrpcService
public class JobsImpl extends jmash.jobs.JobsGrpc.JobsImplBase {

  private static Log log = LogFactory.getLog(JobsImpl.class);

  // 模块版本
  public static final String version = "v1.0.0";

  @Override
  public void version(Empty request, StreamObserver<StringValue> responseObserver) {
    responseObserver.onNext(StringValue.of(version + "-" + VersionUtil.snapshot(JobsImpl.class)));
    responseObserver.onCompleted();
  }

  @Override
  public void findEnumList(StringValue request, StreamObserver<EnumValueList> responseObserver) {
    try {
      List<EnumValue> list = ProtoEnumUtil.getEnumList(request.getValue());
      responseObserver.onNext(EnumValueList.newBuilder().addAllValues(list).build());
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  public void findEnumMap(StringValue request,
      StreamObserver<CustomEnumValueMap> responseObserver) {
    try {
      Map<Integer, CustomEnumValue> values = ProtoEnumUtil.getEnumMap(request.getValue());
      responseObserver.onNext(CustomEnumValueMap.newBuilder().putAllValues(values).build());
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  public void findEnumEntry(EnumEntryReq request, StreamObserver<EntryList> responseObserver) {
    try {
      List<Entry> entryList =
          ProtoEnumUtil.getEnumCodeList(request.getClassName(), request.getType());
      responseObserver.onNext(EntryList.newBuilder().addAllValues(entryList).build());
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresAuthentication
  public void findClassify(TenantReq request, StreamObserver<EntryList> responseObserver) {
    try (JobDefineRead jobDefineRead = JobsFactory.getJobDefineRead(request.getTenant())) {
      EntryList list = jobDefineRead.findClassifyList();
      responseObserver.onNext(list);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("jobs:job_define:list")
  public void findJobDefinePage(JobDefineReq request,
      StreamObserver<JobDefinePage> responseObserver) {
    try (JobDefineRead jobDefineRead = JobsFactory.getJobDefineRead(request.getTenant())) {
      DtoPage<JobDefineEntity, JobDefineTotal> page = jobDefineRead.findPageByReq(request);
      JobDefinePage modelPage = JobDefineMapper.INSTANCE.pageJobDefine(page);
      responseObserver.onNext(modelPage);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }


  @Override
  @RequiresAuthentication
  public void findJobDefineList(JobDefineReq request,
      StreamObserver<JobDefineList> responseObserver) {
    try (JobDefineRead jobDefineRead = JobsFactory.getJobDefineRead(request.getTenant())) {
      List<JobDefineEntity> list = jobDefineRead.findListByReq(request);
      List<JobDefineModel> modelList = JobDefineMapper.INSTANCE.listJobDefine(list);
      responseObserver.onNext(JobDefineList.newBuilder().addAllResults(modelList).build());
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresAuthentication
  public void findJobDefineById(JobDefineKey request,
      StreamObserver<JobDefineModel> responseObserver) {
    try (JobDefineRead jobDefineRead = JobsFactory.getJobDefineRead(request.getTenant())) {
      JobDefineEntity entity = jobDefineRead.findById(UUIDUtil.fromString(request.getJobName()));
      JobDefineModel model = JobDefineMapper.INSTANCE.model(entity);
      responseObserver.onNext(model);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("jobs:job_define:enable")
  public void enableJobDefine(JobDefineEnableKey request,
      StreamObserver<BoolValue> responseObserver) {
    try (JobDefineWrite jobDefineWrite = JobsFactory.getJobDefineWrite(request.getTenant())) {
      boolean value = jobDefineWrite.enable(request);
      responseObserver.onNext(BoolValue.of(value));
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }


  @Override
  @RequiresPermissions("jobs:job_define:add")
  public void createJobDefine(JobDefineCreateReq request,
      StreamObserver<JobDefineModel> responseObserver) {
    try (JobDefineWrite jobDefineWrite = JobsFactory.getJobDefineWrite(request.getTenant())) {
      JobDefineEntity entity = jobDefineWrite.insert(request);
      JobDefineModel model = JobDefineMapper.INSTANCE.model(entity);
      responseObserver.onNext(model);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("jobs:job_define:update")
  public void updateJobDefine(JobDefineUpdateReq request,
      StreamObserver<JobDefineModel> responseObserver) {
    try (JobDefineWrite jobDefineWrite = JobsFactory.getJobDefineWrite(request.getTenant())) {
      JobDefineEntity entity = jobDefineWrite.update(request);
      JobDefineModel model = JobDefineMapper.INSTANCE.model(entity);
      responseObserver.onNext(model);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("jobs:job_define:delete")
  public void deleteJobDefine(JobDefineKey request,
      StreamObserver<JobDefineModel> responseObserver) {
    try (JobDefineWrite jobDefineWrite = JobsFactory.getJobDefineWrite(request.getTenant())) {
      JobDefineEntity entity = jobDefineWrite.delete(UUIDUtil.fromString(request.getJobName()));
      JobDefineModel model = JobDefineMapper.INSTANCE.model(entity);
      responseObserver.onNext(model);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("jobs:job_define:delete")
  public void batchDeleteJobDefine(JobDefineKeyList request,
      StreamObserver<Int32Value> responseObserver) {
    try (JobDefineWrite jobDefineWrite = JobsFactory.getJobDefineWrite(request.getTenant())) {
      final List<String> list = request.getJobNameList();
      final Set<UUID> set =
          list.stream().map(v -> UUIDUtil.fromString(v)).collect(Collectors.toSet());

      int r = jobDefineWrite.batchDelete(set);
      responseObserver.onNext(Int32Value.of(r));
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }


  @Override
  @RequiresPermissions("jobs:job_instance:list")
  public void findJobInstancePage(JobInstanceReq request,
      StreamObserver<JobInstancePage> responseObserver) {
    try (JobInstanceRead jobInstanceRead = JobsFactory.getJobInstanceRead(request.getTenant())) {
      DtoPage<JobInstanceEntity, JobInstanceTotal> page = jobInstanceRead.findPageByReq(request);
      JobInstancePage modelPage = JobInstanceMapper.INSTANCE.pageJobInstance(page);
      responseObserver.onNext(modelPage);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }


  @Override
  @RequiresPermissions("jobs:job_instance:list")
  public void findJobInstanceList(JobInstanceReq request,
      StreamObserver<JobInstanceList> responseObserver) {
    try (JobInstanceRead jobInstanceRead = JobsFactory.getJobInstanceRead(request.getTenant())) {
      List<JobInstanceEntity> list = jobInstanceRead.findListByReq(request);
      List<JobInstanceModel> modelList = JobInstanceMapper.INSTANCE.listJobInstance(list);
      responseObserver.onNext(JobInstanceList.newBuilder().addAllResults(modelList).build());
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("jobs:job_instance:view")
  public void findJobInstanceById(JobInstanceKey request,
      StreamObserver<JobInstanceModel> responseObserver) {
    try (JobInstanceRead jobInstanceRead = JobsFactory.getJobInstanceRead(request.getTenant())) {
      JobInstancePk pk = JobInstanceMapper.INSTANCE.pk(request);
      JobInstanceEntity entity = jobInstanceRead.findById(pk);
      JobInstanceModel model = JobInstanceMapper.INSTANCE.model(entity);
      responseObserver.onNext(model);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresRoles(value = {"system", "admin"}, logical = Logical.OR)
  public void initJobInstance(JobInstanceInitReq request,
      StreamObserver<JobInstanceModel> responseObserver) {
    try (JobInstanceWrite jobInstanceWrite = JobsFactory.getJobInstanceWrite(request.getTenant())) {
      JobInstanceEntity entity = jobInstanceWrite.init(request);
      JobInstanceModel model = JobInstanceMapper.INSTANCE.model(entity);
      responseObserver.onNext(model);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresRoles(value = {"system", "admin"}, logical = Logical.OR)
  public void heartbeatJobInstance(JobInstanceHeartbeat request,
      StreamObserver<BoolValue> responseObserver) {
    try (JobInstanceWrite jobInstanceWrite = JobsFactory.getJobInstanceWrite(request.getTenant())) {
      boolean result = jobInstanceWrite.heartbeat(request);
      responseObserver.onNext(BoolValue.of(result));
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresRoles(value = {"system", "admin"}, logical = Logical.OR)
  public void assignJobs(JobInstanceAssignJob request, StreamObserver<BoolValue> responseObserver) {
    try (JobInstanceWrite jobInstanceWrite = JobsFactory.getJobInstanceWrite(request.getTenant())) {
      boolean result = jobInstanceWrite.assignJobs(request);
      responseObserver.onNext(BoolValue.of(result));
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("jobs:job_instance:delete")
  public void deleteJobInstance(JobInstanceKey request,
      StreamObserver<JobInstanceModel> responseObserver) {
    try (JobInstanceWrite jobInstanceWrite = JobsFactory.getJobInstanceWrite(request.getTenant())) {
      JobInstancePk pk = JobInstanceMapper.INSTANCE.pk(request);
      JobInstanceEntity entity = jobInstanceWrite.delete(pk);
      JobInstanceModel model = JobInstanceMapper.INSTANCE.model(entity);
      responseObserver.onNext(model);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("jobs:job_record:list")
  public void findJobRecordPage(JobRecordReq request,
      StreamObserver<JobRecordPage> responseObserver) {
    try (JobRecordRead jobRecordRead = JobsFactory.getJobRecordRead(request.getTenant())) {
      DtoPage<JobRecordEntity, JobRecordTotal> page = jobRecordRead.findPageByReq(request);
      JobRecordPage modelPage = JobRecordMapper.INSTANCE.pageJobRecord(page);
      responseObserver.onNext(modelPage);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }


  @Override
  @RequiresPermissions("jobs:job_record:list")
  public void findJobRecordList(JobRecordReq request,
      StreamObserver<JobRecordList> responseObserver) {
    try (JobRecordRead jobRecordRead = JobsFactory.getJobRecordRead(request.getTenant())) {
      List<JobRecordEntity> list = jobRecordRead.findListByReq(request);
      List<JobRecordModel> modelList = JobRecordMapper.INSTANCE.listJobRecord(list);
      responseObserver.onNext(JobRecordList.newBuilder().addAllResults(modelList).build());
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("jobs:job_record:view")
  public void findJobRecordById(JobRecordKey request,
      StreamObserver<JobRecordModel> responseObserver) {
    try (JobRecordRead jobRecordRead = JobsFactory.getJobRecordRead(request.getTenant())) {
      JobRecordEntity entity = jobRecordRead.findById(UUIDUtil.fromString(request.getRecordId()));
      JobRecordModel model = JobRecordMapper.INSTANCE.model(entity);
      responseObserver.onNext(model);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresRoles(value = {"system", "admin"}, logical = Logical.OR)
  public void createJobRecord(JobRecordCreateReq request,
      StreamObserver<JobRecordModel> responseObserver) {
    try (JobRecordWrite jobRecordWrite = JobsFactory.getJobRecordWrite(request.getTenant())) {
      JobRecordEntity entity = jobRecordWrite.insert(request);
      JobRecordModel model = JobRecordMapper.INSTANCE.model(entity);
      responseObserver.onNext(model);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("jobs:job_record:delete")
  public void deleteJobRecord(JobRecordKey request,
      StreamObserver<JobRecordModel> responseObserver) {
    try (JobRecordWrite jobRecordWrite = JobsFactory.getJobRecordWrite(request.getTenant())) {
      JobRecordEntity entity = jobRecordWrite.delete(UUIDUtil.fromString(request.getRecordId()));
      JobRecordModel model = JobRecordMapper.INSTANCE.model(entity);
      responseObserver.onNext(model);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("jobs:job_record:delete")
  public void batchDeleteJobRecord(JobRecordDelReq request,
      StreamObserver<Int32Value> responseObserver) {
    try (JobRecordWrite jobRecordWrite = JobsFactory.getJobRecordWrite(request.getTenant())) {
      int count = jobRecordWrite.batchDelete(request.getYear());
      responseObserver.onNext(Int32Value.of(count));
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresAuthentication
  public void findJobTriggerList(JobTriggerReq request,
      StreamObserver<JobTriggerList> responseObserver) {
    try (JobTriggerRead jobTriggerRead = JobsFactory.getJobTriggerRead(request.getTenant())) {
      List<JobTriggerEntity> list = jobTriggerRead.findListByReq(request);
      List<JobTriggerModel> modelList = JobTriggerMapper.INSTANCE.listJobTrigger(list);
      responseObserver.onNext(JobTriggerList.newBuilder().addAllResults(modelList).build());
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("jobs:job_trigger:view")
  public void findJobTriggerById(JobTriggerKey request,
      StreamObserver<JobTriggerModel> responseObserver) {
    try (JobTriggerRead jobTriggerRead = JobsFactory.getJobTriggerRead(request.getTenant())) {
      JobTriggerEntity entity =
          jobTriggerRead.findById(UUIDUtil.fromString(request.getTriggerId()));
      JobTriggerModel model = JobTriggerMapper.INSTANCE.model(entity);
      responseObserver.onNext(model);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("jobs:job_trigger:add")
  public void createJobTrigger(JobTriggerCreateReq request,
      StreamObserver<JobTriggerModel> responseObserver) {
    try (JobTriggerWrite jobTriggerWrite = JobsFactory.getJobTriggerWrite(request.getTenant())) {
      JobTriggerEntity entity = jobTriggerWrite.insert(request);
      JobTriggerModel model = JobTriggerMapper.INSTANCE.model(entity);
      responseObserver.onNext(model);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("jobs:job_trigger:update")
  public void updateJobTrigger(JobTriggerUpdateReq request,
      StreamObserver<JobTriggerModel> responseObserver) {
    try (JobTriggerWrite jobTriggerWrite = JobsFactory.getJobTriggerWrite(request.getTenant())) {
      JobTriggerEntity entity = jobTriggerWrite.update(request);
      JobTriggerModel model = JobTriggerMapper.INSTANCE.model(entity);
      responseObserver.onNext(model);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("jobs:job_trigger:delete")
  public void deleteJobTrigger(JobTriggerKey request,
      StreamObserver<JobTriggerModel> responseObserver) {
    try (JobTriggerWrite jobTriggerWrite = JobsFactory.getJobTriggerWrite(request.getTenant())) {
      JobTriggerEntity entity = jobTriggerWrite.delete(UUIDUtil.fromString(request.getTriggerId()));
      JobTriggerModel model = JobTriggerMapper.INSTANCE.model(entity);
      responseObserver.onNext(model);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("jobs:job_trigger:delete")
  public void batchDeleteJobTrigger(JobTriggerKeyList request,
      StreamObserver<Int32Value> responseObserver) {
    try (JobTriggerWrite jobTriggerWrite = JobsFactory.getJobTriggerWrite(request.getTenant())) {
      final List<String> list = request.getTriggerIdList();
      final Set<UUID> set =
          list.stream().map(v -> UUIDUtil.fromString(v)).collect(Collectors.toSet());

      int r = jobTriggerWrite.batchDelete(set);
      responseObserver.onNext(Int32Value.of(r));
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

}
