
package com.gitee.jobs.mapper;

import com.crenjoy.proto.mapper.BeanMapper;
import com.crenjoy.proto.mapper.ProtoMapper;
import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jobs.entity.JobDefineEntity;
import com.gitee.jobs.model.JobDefineTotal;
import java.util.List;
import jmash.jobs.protobuf.JobDefineCreateReq;
import jmash.jobs.protobuf.JobDefineModel;
import jmash.jobs.protobuf.JobDefinePage;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * JobDefineMapper.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Mapper
public interface JobDefineMapper extends BeanMapper, ProtoMapper {

  JobDefineMapper INSTANCE = Mappers.getMapper(JobDefineMapper.class);

  List<JobDefineModel> listJobDefine(List<JobDefineEntity> list);

  JobDefinePage pageJobDefine(DtoPage<JobDefineEntity, JobDefineTotal> page);

  JobDefineModel model(JobDefineEntity entity);

  JobDefineEntity create(JobDefineCreateReq req);

  JobDefineEntity clone(JobDefineEntity entity);

}
