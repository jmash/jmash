
package com.gitee.jobs.mapper;

import com.crenjoy.proto.mapper.BeanMapper;
import com.crenjoy.proto.mapper.ProtoMapper;
import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jobs.entity.JobInstanceEntity;
import com.gitee.jobs.entity.JobInstanceEntity.JobInstancePk;
import com.gitee.jobs.model.JobInstanceTotal;
import java.util.List;
import jmash.jobs.protobuf.JobInstanceInitReq;
import jmash.jobs.protobuf.JobInstanceKey;
import jmash.jobs.protobuf.JobInstanceModel;
import jmash.jobs.protobuf.JobInstancePage;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * JobInstanceMapper.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Mapper
public interface JobInstanceMapper extends BeanMapper, ProtoMapper {

  JobInstanceMapper INSTANCE = Mappers.getMapper(JobInstanceMapper.class);

  List<JobInstanceModel> listJobInstance(List<JobInstanceEntity> list);

  JobInstancePage pageJobInstance(DtoPage<JobInstanceEntity, JobInstanceTotal> page);

  JobInstanceModel model(JobInstanceEntity entity);

  JobInstancePk pk(JobInstanceKey key);

  JobInstanceKey key(JobInstanceModel model);

  JobInstanceEntity clone(JobInstanceEntity entity);

}
