
package com.gitee.jobs.mapper;

import com.crenjoy.proto.mapper.BeanMapper;
import com.crenjoy.proto.mapper.ProtoMapper;
import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jobs.entity.JobRecordEntity;
import com.gitee.jobs.model.JobRecordTotal;
import java.util.List;
import jmash.jobs.protobuf.JobRecordCreateReq;
import jmash.jobs.protobuf.JobRecordModel;
import jmash.jobs.protobuf.JobRecordPage;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * JobRecordMapper.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Mapper
public interface JobRecordMapper extends BeanMapper, ProtoMapper {

  JobRecordMapper INSTANCE = Mappers.getMapper(JobRecordMapper.class);

  List<JobRecordModel> listJobRecord(List<JobRecordEntity> list);

  JobRecordPage pageJobRecord(DtoPage<JobRecordEntity, JobRecordTotal> page);

  JobRecordModel model(JobRecordEntity entity);

  JobRecordEntity create(JobRecordCreateReq req);

  JobRecordEntity clone(JobRecordEntity entity);

}
