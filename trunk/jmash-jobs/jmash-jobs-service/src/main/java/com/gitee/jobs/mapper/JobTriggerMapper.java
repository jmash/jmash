
package com.gitee.jobs.mapper;

import com.crenjoy.proto.mapper.BeanMapper;
import com.crenjoy.proto.mapper.ProtoMapper;
import com.gitee.jobs.entity.JobTriggerEntity;
import java.util.List;
import jmash.jobs.protobuf.JobTriggerCreateReq;
import jmash.jobs.protobuf.JobTriggerModel;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * JobTriggerMapper.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Mapper
public interface JobTriggerMapper extends BeanMapper, ProtoMapper {

  JobTriggerMapper INSTANCE = Mappers.getMapper(JobTriggerMapper.class);

  List<JobTriggerModel> listJobTrigger(List<JobTriggerEntity> list);

  JobTriggerModel model(JobTriggerEntity entity);

  JobTriggerEntity create(JobTriggerCreateReq req);

  JobTriggerEntity clone(JobTriggerEntity entity);

}
