package com.gitee.jobs.model;

import java.util.Comparator;

public class JobInstanceComparator implements Comparator<JobInstanceWeight> {

  @Override
  public int compare(JobInstanceWeight arg0, JobInstanceWeight arg1) {
    return Integer.compare(arg0.getWeight(), arg1.getWeight());
  }

}
