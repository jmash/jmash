package com.gitee.jobs.model;

import java.util.Objects;

/** Job实例权重. */
public class JobInstanceWeight implements java.io.Serializable {

  private static final long serialVersionUID = 1L;

  /** 实例名称. */
  String instanceName;

  /** 累计权重. */
  int weight;

  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }

  public int getWeight() {
    return weight;
  }

  public void setWeight(int weight) {
    this.weight = weight;
  }

  @Override
  public int hashCode() {
    return Objects.hash(instanceName);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    JobInstanceWeight other = (JobInstanceWeight) obj;
    return Objects.equals(instanceName, other.instanceName);
  }
}
