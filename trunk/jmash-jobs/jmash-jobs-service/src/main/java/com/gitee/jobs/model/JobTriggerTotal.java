
package com.gitee.jobs.model;

import com.gitee.jmash.core.orm.DtoTotal;

/**
* 任务触发器;统计信息.
*/
public class JobTriggerTotal extends DtoTotal  {

  private static final long serialVersionUID = 1L;

}