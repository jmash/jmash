package com.gitee.jobs.service;

import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jobs.entity.JobDefineEntity;
import com.gitee.jobs.model.JobDefineTotal;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;
import jmash.jobs.protobuf.JobDefineReq;
import jmash.protobuf.EntryList;

/**
 * 计划任务定义; job_define服务Read接口.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public interface JobDefineRead extends TenantService {

  /** 根据主键查询. */
  public JobDefineEntity findById(@NotNull UUID entityId);

  /** 查询页信息. */
  public DtoPage<JobDefineEntity, JobDefineTotal> findPageByReq(@NotNull @Valid JobDefineReq req);

  /** 综合查询. */
  public List<JobDefineEntity> findListByReq(@NotNull @Valid JobDefineReq req);
  
  /** 查询任务分类Entry */
  public EntryList findClassifyList();

}
