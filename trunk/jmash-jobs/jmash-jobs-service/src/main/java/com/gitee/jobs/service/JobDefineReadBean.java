
package com.gitee.jobs.service;

import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.orm.cdi.JpaTenantService;
import com.gitee.jmash.core.orm.jpa.TenantEntityManager;
import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.core.transaction.JakartaTransaction;
import com.gitee.jobs.dao.JobDefineDao;
import com.gitee.jobs.dao.JobTriggerDao;
import com.gitee.jobs.entity.JobDefineEntity;
import com.gitee.jobs.model.JobDefineTotal;
import jakarta.enterprise.inject.Typed;
import jakarta.enterprise.inject.spi.CDI;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import jakarta.transaction.Transactional.TxType;
import jakarta.validation.executable.ValidateOnExecution;
import java.util.List;
import java.util.UUID;
import jmash.jobs.protobuf.JobDefineReq;
import jmash.protobuf.Entry;
import jmash.protobuf.EntryList;

/**
 * 计划任务定义; job_define读服务.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Typed(JobDefineRead.class)
@Transactional(TxType.SUPPORTS)
@JpaTenantService
@ValidateOnExecution
public class JobDefineReadBean implements JobDefineRead, JakartaTransaction {

  protected TenantEntityManager tem = new TenantEntityManager();

  protected JobDefineDao jobDefineDao = new JobDefineDao(tem);
  
  protected JobTriggerDao jobTriggerDao = new JobTriggerDao(tem);

  @PersistenceContext(unitName = "ReadJobs")
  public void setEntityManager(EntityManager entityManager) {
    this.tem.setEntityManager(entityManager, false);
  }

  @Override
  public EntityManager getEntityManager() {
    return this.tem.getEntityManager();
  }

  @Override
  @SuppressWarnings("unchecked")
  public <T extends TenantService> T setTenant(String tenant) {
    this.tem.setTenant(tenant);
    return (T) this;
  }

  @Override
  public void setTenantOnly(String tenant) {
    this.tem.setTenantOnly(tenant);
  }

  @Override
  public String getTenant() {
    return this.tem.getTenant();
  }

  @Override
  public JobDefineEntity findById(UUID entityId) {
    return jobDefineDao.find(entityId);
  }

  @Override
  public DtoPage<JobDefineEntity, JobDefineTotal> findPageByReq(JobDefineReq req) {
    return jobDefineDao.findPageByReq(req);
  }

  @Override
  public List<JobDefineEntity> findListByReq(JobDefineReq req) {
    return jobDefineDao.findListByReq(req);
  }

  @Override
  public EntryList findClassifyList() {
    List<String> list = jobDefineDao.findClassifyList();
    EntryList.Builder builder = EntryList.newBuilder();
    for (String classify : list) {
      builder.addValues(Entry.newBuilder().setKey(classify).setValue(classify).build());
    }
    return builder.build();
  }

  @Override
  public void close() throws Exception {
    CDI.current().destroy(this);
  }
}
