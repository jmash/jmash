package com.gitee.jobs.service;

import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jobs.entity.JobDefineEntity;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.util.Set;
import java.util.UUID;
import jmash.jobs.protobuf.JobDefineCreateReq;
import jmash.jobs.protobuf.JobDefineEnableKey;
import jmash.jobs.protobuf.JobDefineUpdateReq;

/**
 * 计划任务定义; job_define服务Write接口.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public interface JobDefineWrite extends TenantService {

  /** 插入实体. */
  public JobDefineEntity insert(@NotNull @Valid JobDefineCreateReq jobDefine);


  /** update 实体. */
  public JobDefineEntity update(@NotNull @Valid JobDefineUpdateReq jobDefine);

  /** 启用/禁用. */
  public boolean enable(@NotNull JobDefineEnableKey jobDefineEnableKey);

  /** 根据主键删除. */
  public JobDefineEntity delete(@NotNull UUID entityId);

  /** 根据主键数组删除. */
  public Integer batchDelete(@NotNull @Valid Set<@NotNull UUID> entityIds);



}
