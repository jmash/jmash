
package com.gitee.jobs.service;

import com.gitee.jmash.common.lock.DistributedLock;
import com.gitee.jmash.common.utils.UUIDUtil;
import com.gitee.jmash.core.jaxrs.ParamsValidationException;
import com.gitee.jmash.core.orm.cdi.JpaTenantService;
import com.gitee.jmash.core.transaction.JakartaTransaction;
import com.gitee.jmash.core.utils.FieldMaskUtil;
import com.gitee.jobs.entity.JobDefineEntity;
import com.gitee.jobs.mapper.JobDefineMapper;
import jakarta.enterprise.inject.Typed;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.LockModeType;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import jakarta.transaction.Transactional.TxType;
import jakarta.validation.ValidationException;
import jakarta.validation.executable.ValidateOnExecution;
import java.util.Set;
import java.util.UUID;
import jmash.jobs.protobuf.JobDefineCreateReq;
import jmash.jobs.protobuf.JobDefineEnableKey;
import jmash.jobs.protobuf.JobDefineUpdateReq;

/**
 * 计划任务定义; job_define写服务.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Typed(JobDefineWrite.class)
@Transactional(TxType.REQUIRED)
@JpaTenantService
@ValidateOnExecution
public class JobDefineWriteBean extends JobDefineReadBean
    implements JobDefineWrite, JakartaTransaction {

  @Inject
  DistributedLock lock;

  @Override
  @PersistenceContext(unitName = "WriteJobs")
  public void setEntityManager(EntityManager entityManager) {
    this.tem.setEntityManager(entityManager, true);
  }

  @Override
  public JobDefineEntity insert(JobDefineCreateReq jobDefine) {
    JobDefineEntity entity = JobDefineMapper.INSTANCE.create(jobDefine);
    // 1.业务校验.
    // 2.仅校验,不执行.
    if (jobDefine.getValidateOnly()) {
      return entity;
    }
    // 3.检查是否重复请求.
    if (!lock.lock(jobDefine.getRequestId(), 60)) {
      throw new ParamsValidationException("requestId", "客户端发起重复请求");
    }
    // 4.执行业务(创建人及时间内部处理.)
    jobDefineDao.persist(entity);
    return entity;
  }

  @Override
  public JobDefineEntity update(JobDefineUpdateReq req) {
    UUID id = UUIDUtil.fromString(req.getJobName());
    JobDefineEntity entity = jobDefineDao.find(id, req.getValidateOnly());
    if (null == entity) {
      throw new ValidationException("找不到实体:" + req.getJobName());
    }
    // 无需更新,返回当前数据库数据.
    if (req.getUpdateMask().getPathsCount() == 0) {
      return entity;
    }
    // 更新掩码属性
    FieldMaskUtil.copyMask(entity, req, req.getUpdateMask());
    // 1.业务校验.
    // 2.仅校验,不执行.
    if (req.getValidateOnly()) {
      return entity;
    }

    // 3.检查是否重复请求.
    if (!lock.lock(req.getRequestId(), 60)) {
      throw new ParamsValidationException("requestId", "客户端发起重复请求");
    }

    // 4.执行业务
    jobDefineDao.merge(entity);
    return entity;
  }

  @Override
  public boolean enable(JobDefineEnableKey jobDefineEnableKey) {
    UUID id = UUIDUtil.fromString(jobDefineEnableKey.getJobName());
    JobDefineEntity entity = jobDefineDao.find(id, LockModeType.PESSIMISTIC_WRITE);
    entity.setEnable(jobDefineEnableKey.getEnable());
    jobDefineDao.merge(entity);
    return true;
  }

  @Override
  public JobDefineEntity delete(UUID entityId) {
    //先移除触发器.
    jobTriggerDao.deleteByJobName(entityId);
    //删除任务.
    JobDefineEntity entity = jobDefineDao.removeById(entityId);
    return entity;
  }

  @Override
  public Integer batchDelete(Set<UUID> entityIds) {
    int i = 0;
    for (UUID entityId : entityIds) {
      jobDefineDao.removeById(entityId);
      i++;
    }
    return i;
  }

}
