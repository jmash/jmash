package com.gitee.jobs.service;

import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jobs.entity.JobInstanceEntity;
import com.gitee.jobs.entity.JobInstanceEntity.JobInstancePk;
import com.gitee.jobs.model.JobInstanceTotal;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.util.List;
import jmash.jobs.protobuf.JobInstanceReq;

/**
 * 运行实例; job_instance服务Read接口.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public interface JobInstanceRead extends TenantService {

  /** 根据主键查询. */
  public JobInstanceEntity findById(@NotNull JobInstancePk entityId);

  /** 查询页信息. */
  public DtoPage<JobInstanceEntity, JobInstanceTotal> findPageByReq(
      @NotNull @Valid JobInstanceReq req);

  /** 综合查询. */
  public List<JobInstanceEntity> findListByReq(@NotNull @Valid JobInstanceReq req);

}
