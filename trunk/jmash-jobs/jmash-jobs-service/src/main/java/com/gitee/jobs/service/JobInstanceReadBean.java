
package com.gitee.jobs.service;

import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.orm.cdi.JpaTenantService;
import com.gitee.jmash.core.orm.jpa.TenantEntityManager;
import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.core.transaction.JakartaTransaction;
import com.gitee.jobs.dao.JobDefineDao;
import com.gitee.jobs.dao.JobInstanceDao;
import com.gitee.jobs.entity.JobInstanceEntity;
import com.gitee.jobs.entity.JobInstanceEntity.JobInstancePk;
import com.gitee.jobs.model.JobInstanceTotal;
import jakarta.enterprise.inject.Typed;
import jakarta.enterprise.inject.spi.CDI;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import jakarta.transaction.Transactional.TxType;
import jakarta.validation.executable.ValidateOnExecution;
import java.util.List;
import jmash.jobs.protobuf.JobInstanceReq;

/**
 * 运行实例; job_instance读服务.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Typed(JobInstanceRead.class)
@Transactional(TxType.SUPPORTS)
@JpaTenantService
@ValidateOnExecution
public class JobInstanceReadBean implements JobInstanceRead, JakartaTransaction {

  protected TenantEntityManager tem = new TenantEntityManager();

  protected JobInstanceDao jobInstanceDao = new JobInstanceDao(tem);
  
  protected JobDefineDao jobDefineDao = new JobDefineDao(tem);

  @PersistenceContext(unitName = "ReadJobs")
  public void setEntityManager(EntityManager entityManager) {
    this.tem.setEntityManager(entityManager, false);
  }

  @Override
  public EntityManager getEntityManager() {
    return this.tem.getEntityManager();
  }

  @Override
  @SuppressWarnings("unchecked")
  public <T extends TenantService> T setTenant(String tenant) {
    this.tem.setTenant(tenant);
    return (T) this;
  }

  @Override
  public void setTenantOnly(String tenant) {
    this.tem.setTenantOnly(tenant);
  }

  @Override
  public String getTenant() {
    return this.tem.getTenant();
  }

  @Override
  public JobInstanceEntity findById(JobInstancePk entityId) {
    return jobInstanceDao.find(entityId);
  }


  @Override
  public DtoPage<JobInstanceEntity, JobInstanceTotal> findPageByReq(JobInstanceReq req) {
    return jobInstanceDao.findPageByReq(req);
  }

  @Override
  public List<JobInstanceEntity> findListByReq(JobInstanceReq req) {
    return jobInstanceDao.findListByReq(req);
  }

  @Override
  public void close() throws Exception {
    CDI.current().destroy(this);
  }
}
