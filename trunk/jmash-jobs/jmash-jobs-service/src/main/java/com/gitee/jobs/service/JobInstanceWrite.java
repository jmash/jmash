package com.gitee.jobs.service;

import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jobs.entity.JobInstanceEntity;
import com.gitee.jobs.entity.JobInstanceEntity.JobInstancePk;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import jmash.jobs.protobuf.JobInstanceAssignJob;
import jmash.jobs.protobuf.JobInstanceHeartbeat;
import jmash.jobs.protobuf.JobInstanceInitReq;

/**
 * 运行实例; job_instance服务Write接口.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public interface JobInstanceWrite extends TenantService {

  /** 插入实体. */
  public JobInstanceEntity init(@NotNull @Valid JobInstanceInitReq jobInstance);

  /** 根据主键删除. */
  public JobInstanceEntity delete(@NotNull JobInstancePk entityId);

  /** 心跳. */
  public boolean heartbeat(@NotNull @Valid JobInstanceHeartbeat heartbeat);
  
  /** 任务分配. */
  public boolean assignJobs(@NotNull @Valid JobInstanceAssignJob assignJob);


}
