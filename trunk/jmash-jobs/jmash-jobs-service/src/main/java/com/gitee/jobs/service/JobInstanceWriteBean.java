
package com.gitee.jobs.service;

import com.gitee.jmash.common.lock.DistributedLock;
import com.gitee.jmash.core.jaxrs.ParamsValidationException;
import com.gitee.jmash.core.orm.cdi.JpaTenantService;
import com.gitee.jmash.core.transaction.JakartaTransaction;
import com.gitee.jobs.entity.JobDefineEntity;
import com.gitee.jobs.entity.JobInstanceEntity;
import com.gitee.jobs.entity.JobInstanceEntity.JobInstancePk;
import com.gitee.jobs.model.JobInstanceComparator;
import com.gitee.jobs.model.JobInstanceWeight;
import jakarta.enterprise.inject.Typed;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.LockModeType;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import jakarta.transaction.Transactional.TxType;
import jakarta.validation.executable.ValidateOnExecution;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import jmash.jobs.protobuf.InstanceState;
import jmash.jobs.protobuf.JobInstanceAssignJob;
import jmash.jobs.protobuf.JobInstanceHeartbeat;
import jmash.jobs.protobuf.JobInstanceInitReq;
import org.apache.commons.lang3.StringUtils;

/**
 * 运行实例; job_instance写服务.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Typed(JobInstanceWrite.class)
@Transactional(TxType.REQUIRED)
@JpaTenantService
@ValidateOnExecution
public class JobInstanceWriteBean extends JobInstanceReadBean
    implements JobInstanceWrite, JakartaTransaction {

  @Inject
  DistributedLock lock;

  @Override
  @PersistenceContext(unitName = "WriteJobs")
  public void setEntityManager(EntityManager entityManager) {
    this.tem.setEntityManager(entityManager, true);
  }

  @Override
  public JobInstanceEntity init(JobInstanceInitReq jobInstance) {
    JobInstancePk fId = new JobInstancePk(jobInstance.getClassify(), jobInstance.getInstanceName());
    JobInstanceEntity entity = jobInstanceDao.find(fId, jobInstance.getValidateOnly());
    // 1.业务校验.
    // 2.仅校验,不执行.
    if (jobInstance.getValidateOnly()) {
      return entity;
    }
    // 3.检查是否重复请求.
    if (!lock.lock(jobInstance.getRequestId(), 60)) {
      throw new ParamsValidationException("requestId", "客户端发起重复请求");
    }
    // 4.执行业务(创建人及时间内部处理.)
    if (null == entity) {
      entity = new JobInstanceEntity(jobInstance.getClassify(), jobInstance.getInstanceName());
      jobInstanceDao.persist(entity);
      return entity;
    }
    return entity;
  }

  @Override
  public boolean heartbeat(JobInstanceHeartbeat heartbeat) {
    JobInstancePk fId = new JobInstancePk(heartbeat.getClassify(), heartbeat.getInstanceName());
    JobInstanceEntity entity = jobInstanceDao.find(fId, LockModeType.PESSIMISTIC_WRITE);
    if (null == entity) {
      return false;
    }
    entity.setHeartbeat(LocalDateTime.now());
    entity.setDueDate(LocalDateTime.now().plusSeconds(heartbeat.getInterval() + 10));
    if (InstanceState.stop.equals(entity.getState())) {
      entity.setState(InstanceState.start);
    }
    jobInstanceDao.merge(entity);
    return true;
  }


  @Override
  public boolean assignJobs(JobInstanceAssignJob assignJob) {
    // 1.检查心跳过期实例，设置为停止实例
    jobInstanceDao.stopDueInstance(assignJob.getClassify());
    // 2.获取未分配的任务.
    List<JobDefineEntity> jobs = jobDefineDao.unassignedJobs(assignJob.getClassify());
    if (jobs.isEmpty()) {
      return false;
    }
    // 3.获取可分配的实例.
    List<JobInstanceWeight> instances = jobInstanceDao.findInstanceWeight(assignJob.getClassify());
    if (instances.isEmpty()) {
      return false;
    }
    JobInstanceComparator comparator = new JobInstanceComparator();
    for (JobDefineEntity job : jobs) {
      // 权重升序排列
      Collections.sort(instances, comparator);
      JobInstanceWeight instance = instances.get(0);
      assignJob(job.getJobName(), instance.getInstanceName());
      instance.setWeight(instance.getWeight() + job.getWeight());
    }
    return true;
  }

  @Override
  public JobInstanceEntity delete(JobInstancePk entityId) {
    JobInstanceEntity entity = jobInstanceDao.removeById(entityId);
    return entity;
  }

  // 分配任务.
  private void assignJob(UUID jobName, String instanceName) {
    JobDefineEntity job = this.jobDefineDao.find(jobName);
    if (null == job || !job.getEnable()) {
      return;
    }
    if (StringUtils.isEmpty(instanceName)) {
      return;
    }
    job.setInstanceName(instanceName);
    this.jobDefineDao.merge(job);
  }

}
