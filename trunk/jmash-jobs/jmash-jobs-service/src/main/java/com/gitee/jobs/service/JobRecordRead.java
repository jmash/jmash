package com.gitee.jobs.service;

import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jobs.entity.JobRecordEntity;
import com.gitee.jobs.model.JobRecordTotal;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;
import jmash.jobs.protobuf.JobRecordReq;

/**
 * 计划任务记录; job_record服务Read接口.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public interface JobRecordRead extends TenantService {

  /** 根据主键查询. */
  public JobRecordEntity findById(@NotNull UUID entityId);

  /** 查询页信息. */
  public DtoPage<JobRecordEntity, JobRecordTotal> findPageByReq(@NotNull @Valid JobRecordReq req);

  /** 综合查询. */
  public List<JobRecordEntity> findListByReq(@NotNull @Valid JobRecordReq req);

}
