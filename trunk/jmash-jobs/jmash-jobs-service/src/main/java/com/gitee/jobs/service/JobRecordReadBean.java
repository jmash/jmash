
package com.gitee.jobs.service;

import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.orm.cdi.JpaTenantService;
import com.gitee.jmash.core.orm.jpa.TenantEntityManager;
import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.core.transaction.JakartaTransaction;
import com.gitee.jobs.dao.JobRecordDao;
import com.gitee.jobs.entity.JobRecordEntity;
import com.gitee.jobs.model.JobRecordTotal;
import jakarta.enterprise.inject.Typed;
import jakarta.enterprise.inject.spi.CDI;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import jakarta.transaction.Transactional.TxType;
import jakarta.validation.executable.ValidateOnExecution;
import java.util.List;
import java.util.UUID;
import jmash.jobs.protobuf.JobRecordReq;

/**
 * 计划任务记录; job_record读服务.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Typed(JobRecordRead.class)
@Transactional(TxType.SUPPORTS)
@JpaTenantService
@ValidateOnExecution
public class JobRecordReadBean implements JobRecordRead, JakartaTransaction {

  protected TenantEntityManager tem = new TenantEntityManager();

  protected JobRecordDao jobRecordDao = new JobRecordDao(tem);

  @PersistenceContext(unitName = "ReadJobs")
  public void setEntityManager(EntityManager entityManager) {
    this.tem.setEntityManager(entityManager, false);
  }

  @Override
  public EntityManager getEntityManager() {
    return this.tem.getEntityManager();
  }

  @Override
  @SuppressWarnings("unchecked")
  public <T extends TenantService> T setTenant(String tenant) {
    this.tem.setTenant(tenant);
    return (T) this;
  }

  @Override
  public void setTenantOnly(String tenant) {
    this.tem.setTenantOnly(tenant);
  }

  @Override
  public String getTenant() {
    return this.tem.getTenant();
  }

  @Override
  public JobRecordEntity findById(UUID entityId) {
    return jobRecordDao.find(entityId);
  }


  @Override
  public DtoPage<JobRecordEntity, JobRecordTotal> findPageByReq(JobRecordReq req) {
    return jobRecordDao.findPageByReq(req);
  }

  @Override
  public List<JobRecordEntity> findListByReq(JobRecordReq req) {
    return jobRecordDao.findListByReq(req);
  }

  @Override
  public void close() throws Exception {
    CDI.current().destroy(this);
  }
}
