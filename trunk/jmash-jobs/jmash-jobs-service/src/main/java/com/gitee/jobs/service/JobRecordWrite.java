package com.gitee.jobs.service;

import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jobs.entity.JobRecordEntity;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.util.UUID;
import jmash.jobs.protobuf.JobRecordCreateReq;

/**
 * 计划任务记录; job_record服务Write接口.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public interface JobRecordWrite extends TenantService {

  /** 插入实体. */
  public JobRecordEntity insert(@NotNull @Valid JobRecordCreateReq jobRecord);

  /** 根据主键删除. */
  public JobRecordEntity delete(@NotNull UUID entityId);

  /** 根据主键数组删除. */
  public Integer batchDelete(@NotNull @Valid int year);

}
