
package com.gitee.jobs.service;

import com.gitee.jmash.common.lock.DistributedLock;
import com.gitee.jmash.core.jaxrs.ParamsValidationException;
import com.gitee.jmash.core.orm.cdi.JpaTenantService;
import com.gitee.jmash.core.transaction.JakartaTransaction;
import com.gitee.jobs.entity.JobRecordEntity;
import com.gitee.jobs.mapper.JobRecordMapper;
import jakarta.enterprise.inject.Typed;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import jakarta.transaction.Transactional.TxType;
import jakarta.validation.executable.ValidateOnExecution;
import java.util.UUID;
import jmash.jobs.protobuf.JobRecordCreateReq;

/**
 * 计划任务记录; job_record写服务.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Typed(JobRecordWrite.class)
@Transactional(TxType.REQUIRED)
@JpaTenantService
@ValidateOnExecution
public class JobRecordWriteBean extends JobRecordReadBean
    implements JobRecordWrite, JakartaTransaction {

  @Inject
  DistributedLock lock;

  @Override
  @PersistenceContext(unitName = "WriteJobs")
  public void setEntityManager(EntityManager entityManager) {
    this.tem.setEntityManager(entityManager, true);
  }

  @Override
  public JobRecordEntity insert(JobRecordCreateReq jobRecord) {
    JobRecordEntity entity = JobRecordMapper.INSTANCE.create(jobRecord);
    // 1.业务校验.
    // 2.仅校验,不执行.
    if (jobRecord.getValidateOnly()) {
      return entity;
    }
    // 3.检查是否重复请求.
    if (!lock.lock(jobRecord.getRequestId(), 60)) {
      throw new ParamsValidationException("requestId", "客户端发起重复请求");
    }
    // 4.执行业务(创建人及时间内部处理.)
    jobRecordDao.persist(entity);
    return entity;
  }

  @Override
  public JobRecordEntity delete(UUID entityId) {
    JobRecordEntity entity = jobRecordDao.removeById(entityId);
    return entity;
  }

  @Override
  public Integer batchDelete(int year) {
    return jobRecordDao.batchDelete(year);
  }

}
