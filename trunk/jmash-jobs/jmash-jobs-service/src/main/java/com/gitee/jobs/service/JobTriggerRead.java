package com.gitee.jobs.service;

import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jobs.entity.JobTriggerEntity;
import com.gitee.jobs.model.JobTriggerTotal;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;
import jmash.jobs.protobuf.JobTriggerReq;

/**
 * 任务触发器; job_trigger服务Read接口.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public interface JobTriggerRead extends TenantService {

  /** 根据主键查询. */
  public JobTriggerEntity findById(@NotNull UUID entityId);

  /** 查询页信息. */
  public DtoPage<JobTriggerEntity, JobTriggerTotal> findPageByReq(
      @NotNull @Valid JobTriggerReq req);

  /** 综合查询. */
  public List<JobTriggerEntity> findListByReq(@NotNull @Valid JobTriggerReq req);

}
