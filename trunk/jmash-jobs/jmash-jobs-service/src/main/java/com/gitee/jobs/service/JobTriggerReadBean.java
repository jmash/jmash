
package com.gitee.jobs.service;

import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.orm.cdi.JpaTenantService;
import com.gitee.jmash.core.orm.jpa.TenantEntityManager;
import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.core.transaction.JakartaTransaction;
import com.gitee.jobs.dao.JobTriggerDao;
import com.gitee.jobs.entity.JobTriggerEntity;
import com.gitee.jobs.model.JobTriggerTotal;
import jakarta.enterprise.inject.Typed;
import jakarta.enterprise.inject.spi.CDI;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import jakarta.transaction.Transactional.TxType;
import jakarta.validation.executable.ValidateOnExecution;
import java.util.List;
import java.util.UUID;
import jmash.jobs.protobuf.JobTriggerReq;

/**
 * 任务触发器; job_trigger读服务.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Typed(JobTriggerRead.class)
@Transactional(TxType.SUPPORTS)
@JpaTenantService
@ValidateOnExecution
public class JobTriggerReadBean implements JobTriggerRead, JakartaTransaction {

  protected TenantEntityManager tem = new TenantEntityManager();

  protected JobTriggerDao jobTriggerDao = new JobTriggerDao(tem);

  @PersistenceContext(unitName = "ReadJobs")
  public void setEntityManager(EntityManager entityManager) {
    this.tem.setEntityManager(entityManager, false);
  }

  @Override
  public EntityManager getEntityManager() {
    return this.tem.getEntityManager();
  }

  @Override
  @SuppressWarnings("unchecked")
  public <T extends TenantService> T setTenant(String tenant) {
    this.tem.setTenant(tenant);
    return (T) this;
  }

  @Override
  public void setTenantOnly(String tenant) {
    this.tem.setTenantOnly(tenant);
  }

  @Override
  public String getTenant() {
    return this.tem.getTenant();
  }

  @Override
  public JobTriggerEntity findById(UUID entityId) {
    return jobTriggerDao.find(entityId);
  }


  @Override
  public DtoPage<JobTriggerEntity, JobTriggerTotal> findPageByReq(JobTriggerReq req) {
    return jobTriggerDao.findPageByReq(req);
  }

  @Override
  public List<JobTriggerEntity> findListByReq(JobTriggerReq req) {
    return jobTriggerDao.findListByReq(req);
  }

  @Override
  public void close() throws Exception {
    CDI.current().destroy(this);
  }
}
