package com.gitee.jobs.service;

import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jobs.entity.JobTriggerEntity;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.util.Set;
import java.util.UUID;
import jmash.jobs.protobuf.JobTriggerCreateReq;
import jmash.jobs.protobuf.JobTriggerUpdateReq;

/**
 * 任务触发器; job_trigger服务Write接口.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public interface JobTriggerWrite extends TenantService {

  /** 插入实体. */
  public JobTriggerEntity insert(@NotNull @Valid JobTriggerCreateReq jobTrigger);

  /** update 实体. */
  public JobTriggerEntity update(@NotNull @Valid JobTriggerUpdateReq jobTrigger);

  /** 根据主键删除. */
  public JobTriggerEntity delete(@NotNull UUID entityId);

  /** 根据主键数组删除. */
  public Integer batchDelete(@NotNull @Valid Set<@NotNull UUID> entityIds);



}
