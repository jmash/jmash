
package com.gitee.jobs.service;

import com.gitee.jmash.common.lock.DistributedLock;
import com.gitee.jmash.common.utils.UUIDUtil;
import com.gitee.jmash.core.jaxrs.ParamsValidationException;
import com.gitee.jmash.core.orm.cdi.JpaTenantService;
import com.gitee.jmash.core.transaction.JakartaTransaction;
import com.gitee.jmash.core.utils.FieldMaskUtil;
import com.gitee.jobs.entity.JobTriggerEntity;
import com.gitee.jobs.mapper.JobTriggerMapper;
import jakarta.enterprise.inject.Typed;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import jakarta.transaction.Transactional.TxType;
import jakarta.validation.ValidationException;
import jakarta.validation.executable.ValidateOnExecution;
import java.util.Set;
import java.util.UUID;
import jmash.jobs.protobuf.JobTriggerCreateReq;
import jmash.jobs.protobuf.JobTriggerUpdateReq;

/**
 * 任务触发器; job_trigger写服务.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Typed(JobTriggerWrite.class)
@Transactional(TxType.REQUIRED)
@JpaTenantService
@ValidateOnExecution
public class JobTriggerWriteBean extends JobTriggerReadBean
    implements JobTriggerWrite, JakartaTransaction {

  @Inject
  DistributedLock lock;

  @Override
  @PersistenceContext(unitName = "WriteJobs")
  public void setEntityManager(EntityManager entityManager) {
    this.tem.setEntityManager(entityManager, true);
  }

  @Override
  public JobTriggerEntity insert(JobTriggerCreateReq jobTrigger) {
    JobTriggerEntity entity = JobTriggerMapper.INSTANCE.create(jobTrigger);
    // 1.业务校验.
    // 2.仅校验,不执行.
    if (jobTrigger.getValidateOnly()) {
      return entity;
    }
    // 3.检查是否重复请求.
    if (!lock.lock(jobTrigger.getRequestId(), 60)) {
      throw new ParamsValidationException("requestId", "客户端发起重复请求");
    }
    // 4.执行业务(创建人及时间内部处理.)
    jobTriggerDao.persist(entity);
    return entity;
  }

  @Override
  public JobTriggerEntity update(JobTriggerUpdateReq req) {
    UUID id = UUIDUtil.fromString(req.getTriggerId());
    JobTriggerEntity entity = jobTriggerDao.find(id, req.getValidateOnly());
    if (null == entity) {
      throw new ValidationException("找不到实体:" + req.getTriggerId());
    }
    // 无需更新,返回当前数据库数据.
    if (req.getUpdateMask().getPathsCount() == 0) {
      return entity;
    }
    // 更新掩码属性
    FieldMaskUtil.copyMask(entity, req, req.getUpdateMask());
    // 1.业务校验.
    // 2.仅校验,不执行.
    if (req.getValidateOnly()) {
      return entity;
    }

    // 3.检查是否重复请求.
    if (!lock.lock(req.getRequestId(), 60)) {
      throw new ParamsValidationException("requestId", "客户端发起重复请求");
    }

    // 4.执行业务
    jobTriggerDao.merge(entity);
    return entity;
  }


  @Override
  public JobTriggerEntity delete(UUID entityId) {
    JobTriggerEntity entity = jobTriggerDao.removeById(entityId);
    return entity;
  }

  @Override
  public Integer batchDelete(Set<UUID> entityIds) {
    int i = 0;
    for (UUID entityId : entityIds) {
      jobTriggerDao.removeById(entityId);
      i++;
    }
    return i;
  }

}
