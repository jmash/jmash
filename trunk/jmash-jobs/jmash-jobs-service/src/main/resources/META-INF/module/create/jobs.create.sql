/*!40101 SET NAMES utf8 */;

CREATE TABLE job_define(
    `job_name` binary(16) NOT NULL  COMMENT '计划名称' ,
    `classify_` VARCHAR(50) NOT NULL  COMMENT '分类' ,
    `job_cn_name` VARCHAR(60) NOT NULL  COMMENT '计划任务中文名称' ,
    `job_describe` VARCHAR(240)   COMMENT '计划任务描述' ,
    `weight_` INT NOT NULL  COMMENT '任务运行权重' ,
    `event_name` VARCHAR(90) NOT NULL  COMMENT '任务触发事件名称' ,
    `event_body` VARCHAR(1000)   COMMENT '任务触发事件JOSN' ,
    `instance_name` VARCHAR(60)   COMMENT '运行实例' ,
    `enable_` bool NOT NULL  COMMENT '是否启用' ,
    `version_` INT NOT NULL  COMMENT '乐观锁' ,
    `create_by` binary(16) NOT NULL  COMMENT '创建人' ,
    `create_time` datetime NOT NULL  COMMENT '创建时间' ,
    `update_by` binary(16)   COMMENT '更新人' ,
    `update_time` datetime   COMMENT '更新时间' ,
    PRIMARY KEY (job_name)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT = '计划任务定义;';


CREATE TABLE job_instance(
    `instance_name` VARCHAR(60) NOT NULL  COMMENT '运行实例' ,
    `classify_` VARCHAR(60) NOT NULL  COMMENT '分类' ,
    `heartbeat_` datetime   COMMENT '心跳时间' ,
    `due_date` datetime   COMMENT '过期时间' ,
    `state_` VARCHAR(50)   COMMENT '状态$启动、运行、停止' ,
    PRIMARY KEY (instance_name,classify_)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT = '运行实例;';


CREATE TABLE job_record(
    `record_id` binary(16) NOT NULL  COMMENT '计划执行ID' ,
    `job_name` binary(16) NOT NULL  COMMENT '计划名称' ,
    `start_date` datetime NOT NULL  COMMENT '开始时间' ,
    `end_date` datetime   COMMENT '结束时间' ,
    `error_msg` TEXT   COMMENT '错误信息' ,
    `state_` bool NOT NULL  COMMENT '状态$成功 失败' ,
    PRIMARY KEY (record_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT = '计划任务记录;';


CREATE TABLE job_trigger(
    `trigger_id` binary(16) NOT NULL  COMMENT '触发ID' ,
    `job_name` binary(16) NOT NULL  COMMENT '计划名称' ,
    `start_now` bool NOT NULL  COMMENT '是否当前日期开始' ,
    `start_date` datetime   COMMENT '启动时间' ,
    `cron_` VARCHAR(60)   COMMENT 'cron表达式' ,
    `cron_name` VARCHAR(240)   COMMENT 'cron描述' ,
    `is_custom` bool   COMMENT '是否自定义$默认false' ,
    PRIMARY KEY (trigger_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT = '任务触发器;';

/*==============================================================*/
/* 外键                                                         */
/*==============================================================*/

ALTER TABLE job_record ADD CONSTRAINT FK_job_record_1 FOREIGN KEY (job_name) REFERENCES job_define (job_name) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE job_trigger ADD CONSTRAINT FK_job_trigger_1 FOREIGN KEY (job_name) REFERENCES job_define (job_name) ON DELETE RESTRICT ON UPDATE RESTRICT;

/*==============================================================*/
/* 索引                                                         */
/*==============================================================*/

create index job_define_1 on job_define
(
   classify_,
   enable_,
   create_time
);

create index job_define_2 on job_define
(
   classify_,
   enable_,
   instance_name,
   weight_
);

create index job_define_3 on job_define
(
   classify_,
   job_cn_name
);

create index job_instance_1 on job_instance
(
   classify_,
   state_
);

create index job_record_1 on job_record
(
   job_name,
   start_date,
   end_date
);

create index job_trigger_1 on job_trigger
(
   job_name,
   start_date
);




