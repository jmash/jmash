/*!40101 SET NAMES utf8 */;

DROP TABLE IF EXISTS job_record;

DROP TABLE IF EXISTS job_trigger;

DROP TABLE IF EXISTS job_instance;

DROP TABLE IF EXISTS job_define;