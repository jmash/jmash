
package com.gitee.jobs.test;


import com.google.protobuf.FieldMask;
import com.google.protobuf.Int32Value;
import io.grpc.StatusRuntimeException;
import java.util.UUID;
import jmash.jobs.JobsGrpc;
import jmash.jobs.protobuf.JobDefineCreateReq;
import jmash.jobs.protobuf.JobDefineEnableKey;
import jmash.jobs.protobuf.JobDefineKey;
import jmash.jobs.protobuf.JobDefineKeyList;
import jmash.jobs.protobuf.JobDefineModel;
import jmash.jobs.protobuf.JobDefinePage;
import jmash.jobs.protobuf.JobDefineReq;
import jmash.jobs.protobuf.JobDefineUpdateReq;
import jmash.protobuf.EntryList;
import jmash.protobuf.TenantReq;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

/**
 * 测试.
 *
 * @author CGD
 *
 */
@TestMethodOrder(OrderAnnotation.class)
public class JobDefineTest extends JobsTest {

  private JobsGrpc.JobsBlockingStub jobsStub = null;


  @Test
  @Order(0)
  public void validatorTest() {
    jobsStub = JobsGrpc.newBlockingStub(channel);
    jobsStub = jobsStub.withCallCredentials(getAuthcCallCredentials());
    StatusRuntimeException exception = Assertions.assertThrows(StatusRuntimeException.class, () -> {
      JobDefineCreateReq req = JobDefineCreateReq.newBuilder().build();
      jobsStub.createJobDefine(req);
    });
    Assertions.assertEquals("INTERNAL", exception.getStatus().getCode().name());
    Assertions.assertNotNull(exception.getMessage());
  }

  @Test
  @Order(1)
  public void findClassify() {
    jobsStub = JobsGrpc.newBlockingStub(channel);
    jobsStub = jobsStub.withCallCredentials(getAuthcCallCredentials());
    final TenantReq.Builder request = TenantReq.newBuilder();
    request.setTenant(TENANT);
    final EntryList list = jobsStub.findClassify(request.build());
    System.out.println(list);
  }

  @Test
  @Order(2)
  public void findPage() {
    jobsStub = JobsGrpc.newBlockingStub(channel);
    jobsStub = jobsStub.withCallCredentials(getAuthcCallCredentials());
    final JobDefineReq.Builder request = JobDefineReq.newBuilder();
    request.setTenant(TENANT);
    final JobDefinePage modelPage = jobsStub.findJobDefinePage(request.build());
    System.out.println(modelPage);
  }


  @Test
  @Order(3)
  public void test() {
    jobsStub = JobsGrpc.newBlockingStub(channel);
    jobsStub = jobsStub.withCallCredentials(getAuthcCallCredentials());

    // 创建
    JobDefineCreateReq.Builder req = JobDefineCreateReq.newBuilder();
    req.setTenant(TENANT).setRequestId(UUID.randomUUID().toString());
    req.setClassify("test").setJobCnName("计划任务测试名称").setJobDescribe("描述");
    req.setWeight(100).setEventName("testEvent").setEnable(true);
    JobDefineModel entity = jobsStub.createJobDefine(req.build());

    // findById
    JobDefineKey pk =
        JobDefineKey.newBuilder().setTenant(TENANT).setJobName(entity.getJobName()).build();
    entity = jobsStub.findJobDefineById(pk);

    // Update
    JobDefineUpdateReq.Builder updateReq = JobDefineUpdateReq.newBuilder();
    updateReq.setTenant(TENANT).setRequestId(UUID.randomUUID().toString());
    updateReq.setJobName(entity.getJobName());
    updateReq.setJobCnName("TEST updateName").setWeight(50).setEnable(false);
    // ...
    FieldMask.Builder fieldMask = FieldMask.newBuilder();
    fieldMask.addPaths("job_cn_name");
    fieldMask.addPaths("weight_");
    fieldMask.addPaths("enable");
    updateReq.setUpdateMask(fieldMask.build());

    entity = jobsStub.updateJobDefine(updateReq.build());
    // Enable
    Assertions.assertFalse(entity.getEnable());
    JobDefineEnableKey.Builder enableKey = JobDefineEnableKey.newBuilder();
    enableKey.setTenant(TENANT).setJobName(entity.getJobName()).setEnable(true);
    jobsStub.enableJobDefine(enableKey.build());
    // findById
    entity = jobsStub.findJobDefineById(pk);
    Assertions.assertTrue(entity.getEnable());

    // Delete
    pk = JobDefineKey.newBuilder().setTenant(TENANT).setJobName(entity.getJobName()).build();
    entity = jobsStub.deleteJobDefine(pk);
  }

  @Test
  @Order(4)
  public void batchDeleteTest() {
    jobsStub = JobsGrpc.newBlockingStub(channel);
    jobsStub = jobsStub.withCallCredentials(getAuthcCallCredentials());
    JobDefineKeyList.Builder request = JobDefineKeyList.newBuilder();
    request.setTenant(TENANT);
    final Int32Value result = jobsStub.batchDeleteJobDefine(request.build());
    System.out.println(result);
  }


}
