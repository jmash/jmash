
package com.gitee.jobs.test;


import io.grpc.StatusRuntimeException;
import java.lang.management.ManagementFactory;
import java.util.UUID;
import jmash.jobs.JobsGrpc;
import jmash.jobs.protobuf.JobInstanceAssignJob;
import jmash.jobs.protobuf.JobInstanceHeartbeat;
import jmash.jobs.protobuf.JobInstanceInitReq;
import jmash.jobs.protobuf.JobInstanceKey;
import jmash.jobs.protobuf.JobInstancePage;
import jmash.jobs.protobuf.JobInstanceReq;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

/**
 * 测试.
 *
 * @author CGD
 *
 */
@TestMethodOrder(OrderAnnotation.class)
public class JobInstanceTest extends JobsTest {

  private JobsGrpc.JobsBlockingStub jobsStub = null;

  /** 实例名称. */
  public static String getInstanceName() {
    return ManagementFactory.getRuntimeMXBean().getName().split("@")[1];
  }

  @Test
  @Order(0)
  public void validatorTest() {
    jobsStub = JobsGrpc.newBlockingStub(channel);
    jobsStub = jobsStub.withCallCredentials(getAuthcCallCredentials());
    StatusRuntimeException exception = Assertions.assertThrows(StatusRuntimeException.class, () -> {
      JobInstanceInitReq req = JobInstanceInitReq.newBuilder().build();
      jobsStub.initJobInstance(req);
    });
    Assertions.assertEquals("INTERNAL", exception.getStatus().getCode().name());
    Assertions.assertNotNull(exception.getMessage());
  }

  @Test
  @Order(1)
  public void initJobInstance() {
    jobsStub = JobsGrpc.newBlockingStub(channel);
    jobsStub = jobsStub.withCallCredentials(getAuthcCallCredentials());
    JobInstanceInitReq.Builder req = JobInstanceInitReq.newBuilder();
    req.setRequestId(UUID.randomUUID().toString());
    req.setTenant(TENANT).setClassify("test").setInstanceName(getInstanceName());
    jobsStub.initJobInstance(req.build());
  }

  @Test
  @Order(2)
  public void heartbeat() {
    jobsStub = JobsGrpc.newBlockingStub(channel);
    jobsStub = jobsStub.withCallCredentials(getAuthcCallCredentials());
    JobInstanceHeartbeat.Builder req = JobInstanceHeartbeat.newBuilder();
    req.setTenant(TENANT).setClassify("test").setInstanceName(getInstanceName()).setInterval(30);
    jobsStub.heartbeatJobInstance(req.build());
  }
  
  @Test
  @Order(3)
  public void assignJobs() {
    jobsStub = JobsGrpc.newBlockingStub(channel);
    jobsStub = jobsStub.withCallCredentials(getAuthcCallCredentials());
    JobInstanceAssignJob.Builder req = JobInstanceAssignJob.newBuilder();
    req.setTenant(TENANT).setClassify("test").setInstanceName(getInstanceName());
    jobsStub.assignJobs(req.build());
  }

  @Test
  @Order(4)
  public void findPage() {
    jobsStub = JobsGrpc.newBlockingStub(channel);
    jobsStub = jobsStub.withCallCredentials(getAuthcCallCredentials());
    final JobInstanceReq.Builder request = JobInstanceReq.newBuilder();
    request.setTenant(TENANT);
    final JobInstancePage modelPage = jobsStub.findJobInstancePage(request.build());
    System.out.println(modelPage);
  }

  @Test
  @Order(5)
  public void deleteJobInstance() {
    jobsStub = JobsGrpc.newBlockingStub(channel);
    jobsStub = jobsStub.withCallCredentials(getAuthcCallCredentials());
    JobInstanceKey key = JobInstanceKey.newBuilder().setTenant(TENANT).setClassify("test")
        .setInstanceName(getInstanceName()).build();
    // Delete
    jobsStub.deleteJobInstance(key);
  }

}
