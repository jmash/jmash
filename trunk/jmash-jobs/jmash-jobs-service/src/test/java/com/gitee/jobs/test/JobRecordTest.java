
package com.gitee.jobs.test;


import com.google.protobuf.Int32Value;
import com.google.protobuf.Timestamp;
import io.grpc.StatusRuntimeException;
import java.util.UUID;
import jmash.jobs.JobsGrpc;
import jmash.jobs.protobuf.JobDefineCreateReq;
import jmash.jobs.protobuf.JobDefineKey;
import jmash.jobs.protobuf.JobDefineModel;
import jmash.jobs.protobuf.JobRecordCreateReq;
import jmash.jobs.protobuf.JobRecordDelReq;
import jmash.jobs.protobuf.JobRecordKey;
import jmash.jobs.protobuf.JobRecordModel;
import jmash.jobs.protobuf.JobRecordPage;
import jmash.jobs.protobuf.JobRecordReq;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

/**
 * 测试.
 *
 * @author CGD
 *
 */
@TestMethodOrder(OrderAnnotation.class)
public class JobRecordTest extends JobsTest {

  private JobsGrpc.JobsBlockingStub jobsStub = null;

  @Test
  @Order(1)
  public void validatorTest() {
    jobsStub = JobsGrpc.newBlockingStub(channel);
    jobsStub = jobsStub.withCallCredentials(getAuthcCallCredentials());
    StatusRuntimeException exception = Assertions.assertThrows(StatusRuntimeException.class, () -> {
      JobRecordCreateReq req = JobRecordCreateReq.newBuilder().build();
      jobsStub.createJobRecord(req);
    });
    Assertions.assertEquals("INTERNAL", exception.getStatus().getCode().name());
    Assertions.assertNotNull(exception.getMessage());
  }

  @Test
  @Order(2)
  public void findPage() {
    jobsStub = JobsGrpc.newBlockingStub(channel);
    jobsStub = jobsStub.withCallCredentials(getAuthcCallCredentials());
    final JobRecordReq.Builder request = JobRecordReq.newBuilder();
    request.setTenant(TENANT);
    final JobRecordPage modelPage = jobsStub.findJobRecordPage(request.build());
    System.out.println(modelPage);
  }

  public JobDefineModel addDefine() {
    // 创建Define
    JobDefineCreateReq.Builder req = JobDefineCreateReq.newBuilder();
    req.setTenant(TENANT).setRequestId(UUID.randomUUID().toString());
    req.setClassify("test").setJobCnName("计划任务测试名称").setJobDescribe("描述");
    req.setWeight(100).setEventName("testEvent").setEnable(true);
    return jobsStub.createJobDefine(req.build());
  }

  public void delete(String jobName) {
    // Delete
    JobDefineKey pk = JobDefineKey.newBuilder().setTenant(TENANT).setJobName(jobName).build();
    jobsStub.deleteJobDefine(pk);
  }

  @Test
  @Order(4)
  public void test() {
    jobsStub = JobsGrpc.newBlockingStub(channel);
    jobsStub = jobsStub.withCallCredentials(getAuthcCallCredentials());

    JobDefineModel define = addDefine();
    // 创建
    JobRecordCreateReq.Builder req = JobRecordCreateReq.newBuilder();
    req.setTenant(TENANT).setRequestId(UUID.randomUUID().toString());
    req.setJobName(define.getJobName()).setStartDate(Timestamp.getDefaultInstance());
    req.setState(true);
    // ...
    JobRecordModel entity = jobsStub.createJobRecord(req.build());

    // findById
    JobRecordKey pk =
        JobRecordKey.newBuilder().setTenant(TENANT).setRecordId(entity.getRecordId()).build();
    entity = jobsStub.findJobRecordById(pk);


    // Delete
    pk = JobRecordKey.newBuilder().setTenant(TENANT).setRecordId(entity.getRecordId()).build();
    entity = jobsStub.deleteJobRecord(pk);

    delete(define.getJobName());
  }

  @Test
  @Order(5)
  public void deleteRecord() {
    jobsStub = JobsGrpc.newBlockingStub(channel);
    jobsStub = jobsStub.withCallCredentials(getAuthcCallCredentials());
    // Delete
    JobRecordDelReq pk = JobRecordDelReq.newBuilder().setTenant(TENANT).setYear(1).build();
    Int32Value i = jobsStub.batchDeleteJobRecord(pk);
  }

}
