
package com.gitee.jobs.test;


import com.google.protobuf.FieldMask;
import com.google.protobuf.Int32Value;
import io.grpc.StatusRuntimeException;
import java.util.UUID;
import jmash.jobs.JobsGrpc;
import jmash.jobs.protobuf.JobDefineCreateReq;
import jmash.jobs.protobuf.JobDefineKey;
import jmash.jobs.protobuf.JobDefineModel;
import jmash.jobs.protobuf.JobTriggerCreateReq;
import jmash.jobs.protobuf.JobTriggerKey;
import jmash.jobs.protobuf.JobTriggerKeyList;
import jmash.jobs.protobuf.JobTriggerList;
import jmash.jobs.protobuf.JobTriggerModel;
import jmash.jobs.protobuf.JobTriggerReq;
import jmash.jobs.protobuf.JobTriggerUpdateReq;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

/**
 * 测试.
 *
 * @author CGD
 *
 */
@TestMethodOrder(OrderAnnotation.class)
public class JobTriggerTest extends JobsTest {

  private JobsGrpc.JobsBlockingStub jobsStub = null;

  @Test
  @Order(1)
  public void validatorTest() {
    jobsStub = JobsGrpc.newBlockingStub(channel);
    jobsStub = jobsStub.withCallCredentials(getAuthcCallCredentials());
    StatusRuntimeException exception = Assertions.assertThrows(StatusRuntimeException.class, () -> {
      JobTriggerCreateReq req = JobTriggerCreateReq.newBuilder().build();
      jobsStub.createJobTrigger(req);
    });
    Assertions.assertEquals("INTERNAL", exception.getStatus().getCode().name());
    Assertions.assertNotNull(exception.getMessage());
  }

  @Test
  @Order(2)
  public void findList() {
    jobsStub = JobsGrpc.newBlockingStub(channel);
    jobsStub = jobsStub.withCallCredentials(getAuthcCallCredentials());
    final JobTriggerReq.Builder request = JobTriggerReq.newBuilder();
    request.setTenant(TENANT);
    final JobTriggerList modelPage = jobsStub.findJobTriggerList(request.build());
    System.out.println(modelPage);
  }

  public JobDefineModel addDefine() {
    // 创建Define
    JobDefineCreateReq.Builder req = JobDefineCreateReq.newBuilder();
    req.setTenant(TENANT).setRequestId(UUID.randomUUID().toString());
    req.setClassify("test").setJobCnName("计划任务测试名称").setJobDescribe("描述");
    req.setWeight(100).setEventName("testEvent").setEnable(true);
    return jobsStub.createJobDefine(req.build());
  }

  public void delete(String jobName) {
    // Delete
    JobDefineKey pk = JobDefineKey.newBuilder().setTenant(TENANT).setJobName(jobName).build();
    jobsStub.deleteJobDefine(pk);
  }
  
  //0x8FCFBA6633FD435F9B73D8C11A074F2C

  @Test
  @Order(3)
  public void test() {
    jobsStub = JobsGrpc.newBlockingStub(channel);
    jobsStub = jobsStub.withCallCredentials(getAuthcCallCredentials());

    JobDefineModel define=addDefine();
    // 创建
    JobTriggerCreateReq.Builder req = JobTriggerCreateReq.newBuilder();
    req.setTenant(TENANT).setRequestId(UUID.randomUUID().toString());
    req.setJobName(define.getJobName()).setCron("0/15 * * * * ?").setIsCustom(false);
    JobTriggerModel entity = jobsStub.createJobTrigger(req.build());
   

    // findById
    JobTriggerKey pk = JobTriggerKey.newBuilder().setTenant(TENANT).setTriggerId(entity.getTriggerId()).build();
    entity = jobsStub.findJobTriggerById(pk);

    // Update
    JobTriggerUpdateReq.Builder updateReq = JobTriggerUpdateReq.newBuilder();
    updateReq.setTenant(TENANT).setRequestId(UUID.randomUUID().toString());
    updateReq.setTriggerId(entity.getTriggerId()).setCron("0.0.0.30");
    FieldMask.Builder fieldMask = FieldMask.newBuilder();
    fieldMask.addPaths("cron");

    updateReq.setUpdateMask(fieldMask.build());

    entity = jobsStub.updateJobTrigger(updateReq.build());
    // findById
    pk = JobTriggerKey.newBuilder().setTenant(TENANT).setTriggerId(entity.getTriggerId()).build();
    entity = jobsStub.findJobTriggerById(pk);
    // Delete
    pk = JobTriggerKey.newBuilder().setTenant(TENANT).setTriggerId(entity.getTriggerId()).build();
    entity = jobsStub.deleteJobTrigger(pk);
    
    delete(define.getJobName());
  }
  
  @Test
  @Order(4)
  public void batchDeleteTest() {
    jobsStub = JobsGrpc.newBlockingStub(channel);
    jobsStub = jobsStub.withCallCredentials(getAuthcCallCredentials());
    JobTriggerKeyList.Builder request = JobTriggerKeyList.newBuilder();
    request.setTenant(TENANT);
    final Int32Value result = jobsStub.batchDeleteJobTrigger(request.build());
    System.out.println(result);
  }

}
