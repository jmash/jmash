
package com.gitee.jmash.mvc;

import jakarta.enterprise.inject.spi.CDI;
import jakarta.websocket.server.ServerEndpointConfig;

/**
 * Websocket CDI.
 *
 * @author CGD
 *
 */
public class CdiAwareConfigurator extends ServerEndpointConfig.Configurator {

  public <T> T getEndpointInstance(Class<T> endpointClass) throws InstantiationException {
    return CDI.current().select(endpointClass).get();
  }
}
