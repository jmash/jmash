
package com.gitee.jmash.mvc;

import com.gitee.jmash.common.config.AppProps;
import java.io.File;
import java.net.JarURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Enumeration;
import java.util.Optional;
import javax.naming.NamingException;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.WebResourceRoot;
import org.apache.catalina.WebResourceSet;
import org.apache.catalina.connector.Connector;
import org.apache.catalina.core.StandardContext;
import org.apache.catalina.startup.Tomcat;
import org.apache.catalina.webresources.DirResourceSet;
import org.apache.catalina.webresources.StandardRoot;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.microprofile.config.Config;
import org.eclipse.microprofile.config.ConfigProvider;

/**
 * Tomcat Embed Run.
 *
 * @author CGD
 *
 */
public class JmashApplication {

  private static Log log = LogFactory.getLog(JmashApplication.class);

  /**
   * Run Tomcat .
   */
  public static void run(Class<?> callingClass, String[] args) {
    try {
      Config config = ConfigProvider.getConfig();
      int port = config.getValue("jmash.server.port", int.class);
      Optional<String> context = config.getOptionalValue("jmash.server.context", String.class);

      AppProps props = new AppProps();
      props.setPort(port);
      props.setContext(context);
      new JmashApplication().runTomcat(callingClass, args, props);
    } catch (Exception ex) {
      log.error(ex);
    }
  }

  protected JmashApplication() {

  }

  protected File getRootFolder(Class<?> callingClass) {
    try {
      File root;
      String runningJarPath = callingClass.getProtectionDomain().getCodeSource().getLocation()
          .toURI().getPath().replaceAll("\\\\", "/");
      int lastIndexOf = runningJarPath.lastIndexOf("/target/");
      if (lastIndexOf < 0) {
        root = new File("");
      } else {
        root = new File(runningJarPath.substring(0, lastIndexOf));
      }
      log.info("Application Root Folder: " + root.getAbsolutePath());
      return root;
    } catch (URISyntaxException ex) {
      throw new RuntimeException(ex);
    }
  }

  /** Tomcat Embed . */
  public void runTomcat(Class<?> callingClass, String[] args, AppProps props)
      throws LifecycleException, NamingException {
    System.setProperty("org.apache.catalina.startup.EXIT_ON_INIT_FAILURE", "true");

    File root = getRootFolder(callingClass);

    Tomcat tomcat = new Tomcat();

    File base = new File(root, "target/tomcat");
    if (!base.exists()) {
      base.mkdirs();
    }
    log.info("Tomcat Work Path:" + base.getAbsolutePath());
    tomcat.setBaseDir(base.getAbsolutePath());

    Connector connector = tomcat.getConnector();
    connector.setPort(props.getPort());

    log.info("Url: http://127.0.0.1:" + props.getPort()+props.getContext().orElse(""));

    File app = new File(root.getAbsolutePath(), "src/main/webapp");
    if (!app.exists()) {
      app = new File(base.getAbsolutePath(), "webapp");
    }
    if (!app.exists()) {
      app.mkdirs();
    }

    log.info("Configuring Webapp Path: " + app.getAbsolutePath());

    StandardContext ctx = (StandardContext) tomcat.addWebapp(props.getContext().orElse(""),
        app.getAbsolutePath());

    // Set execution independent of current thread context classloader
    // (compatibility with exec:java mojo)
    ctx.setParentClassLoader(callingClass.getClassLoader());

    // Servlet 3.0 annotation will work
    WebResourceRoot resources = new StandardRoot(ctx);

    // Declare an alternative location for your "WEB-INF/classes" dir
    File webInfClassesFolder = new File(root.getAbsolutePath(), "target/classes");
    try {
      FileUtils.copyDirectory(new File(root.getAbsolutePath(), "src/main/resources"),
          webInfClassesFolder);
    } catch (Exception ex) {
      log.warn(ex);
    }
    addPreResources(resources, webInfClassesFolder, "/WEB-INF/classes");

    // Declare an alternative location for your "WEB-INF/lib" dir
    // web-fragment.xml
    // File webLibFolder = new File(root.getAbsolutePath(), "target/tomcat/lib");
    // webLibFolder.mkdirs();
    // copyWebFragementJar(webLibFolder);
    // addPreResources(resources, webLibFolder, "/WEB-INF/lib");

    ctx.setResources(resources);

    // JNDI Name.
    tomcat.enableNaming();

    tomcat.start();

    tomcat.getServer().await();
  }

  /** Copy WebFragement Jar . */
  public void copyWebFragementJar(File dir) {
    try {
      Enumeration<URL> urls = Thread.currentThread().getContextClassLoader()
          .getResources("META-INF/web-fragment.xml");
      while (urls.hasMoreElements()) {
        URL url = urls.nextElement();
        if (url.getProtocol().equals("jar")) {
          JarURLConnection connect = (JarURLConnection) url.openConnection();
          URI jarUri = connect.getJarFileURL().toURI();
          if (jarUri.toString().contains("tomcat-embed")) {
            continue;
          }
          if (jarUri.toString().contains("krazo-core")) {
            continue;
          }
          log.warn(jarUri.toString());
          FileUtils.copyFileToDirectory(new File(jarUri), dir);
        }
      }
    } catch (Exception ex) {
      log.warn(ex);
    }
  }

  /** /WEB-INF/lib. */
  public File findFolder(File file, String fileName) {
    if (file.getAbsolutePath().endsWith(fileName.replace("/", File.separator))) {
      return file;
    }
    if (file.isDirectory()) {
      for (File f : file.listFiles()) {
        File r = findFolder(f, fileName);
        if (r != null) {
          return r;
        }
      }
    }
    return null;
  }

  /** Servlet 3.0 annotation will Work. */
  public void addPreResources(WebResourceRoot resources, File webFolder, String folderName) {
    WebResourceSet resourceSet = null;
    if (webFolder.exists()) {
      resourceSet = new DirResourceSet(resources, folderName, webFolder.getAbsolutePath(), "/");
      log.info("Loading WEB-INF Resources Path : '" + webFolder.getAbsolutePath() + "'");
    }
    if (resourceSet != null) {
      resources.addPreResources(resourceSet);
    }
  }

}
