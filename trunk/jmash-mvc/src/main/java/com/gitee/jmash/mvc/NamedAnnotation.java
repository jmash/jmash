
package com.gitee.jmash.mvc;

import jakarta.inject.Named;
import java.lang.annotation.Annotation;

@SuppressWarnings("all")
public class NamedAnnotation implements jakarta.inject.Named {

  String name;

  public NamedAnnotation(String name) {
    this.name = name;
  }

  @Override
  public Class<? extends Annotation> annotationType() {
    return Named.class;
  }

  @Override
  public String value() {
    return this.name;
  }

}
