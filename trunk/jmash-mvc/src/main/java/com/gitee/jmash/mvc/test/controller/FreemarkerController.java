
package com.gitee.jmash.mvc.test.controller;

import com.gitee.jmash.mvc.test.models.Greeting;
import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import jakarta.mvc.Controller;
import jakarta.mvc.Models;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;

/** Freemarker Test Jakarta MVC . */
@Path("freemarker")
@Controller
@RequestScoped
public class FreemarkerController {

  @Inject
  private Models models;

  @Inject
  private Greeting greeting;

  /** Models + @Named. */
  @GET
  @Path("params")
  public String execute(@QueryParam("name") String name) {
    models.put("visitor", name);
    greeting.setMessage("Hello there!");
    return "params.ftl";
  }

  @GET
  @Path("hello")
  public String hello() {
    return "hello.ftl";
  }

}
