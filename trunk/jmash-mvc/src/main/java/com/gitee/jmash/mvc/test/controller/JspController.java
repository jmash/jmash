
package com.gitee.jmash.mvc.test.controller;

import com.gitee.jmash.mvc.test.models.Greeting;
import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import jakarta.mvc.Controller;
import jakarta.mvc.Models;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;

/**
 * JSP Test Jakarta MVC .
 */
@Path("jsp")
@Controller
@RequestScoped
public class JspController {

  @Inject
  private Models models;

  @Inject
  private Greeting greeting;

  /** Models + @Named. */
  @GET
  @Path("params")
  public String execute(@QueryParam("name") String name) {
    models.put("visitor", name);
    greeting.setMessage("Hello there!");
    return "params.jsp";
  }

  @GET
  @Path("hello")
  public String hello() {
    return "hello.jsp";
  }

}
