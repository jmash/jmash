
package com.gitee.jmash.mvc.test.models;

import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Named;
import java.time.LocalDateTime;

/**
 * Named .
 *
 * @author CGD
 *
 */
@Named("greeting")
@RequestScoped
public class Greeting {

  private String message;

  private LocalDateTime nowDate = LocalDateTime.now();

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public LocalDateTime getNowDate() {
    return nowDate;
  }

  public void setNowDate(LocalDateTime nowDate) {
    this.nowDate = nowDate;
  }

  @Override
  public String toString() {
    return "Greeting [message=" + message + ", nowDate=" + nowDate + "]";
  }

}
