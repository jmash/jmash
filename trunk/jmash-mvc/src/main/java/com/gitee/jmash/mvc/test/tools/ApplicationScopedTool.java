
package com.gitee.jmash.mvc.test.tools;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Named;

/**
 * Http Application @Named .
 *
 * @author CGD
 *
 */
@Named("applicationTool")
@ApplicationScoped
public class ApplicationScopedTool {

  public String getMessage() {
    return "Application Tool.";
  }

}