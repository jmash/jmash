
package com.gitee.jmash.mvc.test.tools;

import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import jakarta.servlet.http.HttpServletRequest;

/**
 * Http Request @Named .
 *
 * @author CGD
 *
 */
@Named("requestTool")
@RequestScoped
public class RequestScopedTool {

  @Inject
  private HttpServletRequest request;

  public String getMessage() {
    return "Request Tool , id=" + request.getRequestId();
  }

}