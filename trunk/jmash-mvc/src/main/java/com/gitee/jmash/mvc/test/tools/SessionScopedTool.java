
package com.gitee.jmash.mvc.test.tools;

import jakarta.enterprise.context.SessionScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import jakarta.servlet.http.HttpSession;
import java.io.Serializable;

/**
 * Http Session @Named .
 *
 * @author CGD
 *
 */
@Named("sessionTool")
@SessionScoped
public class SessionScopedTool implements Serializable {

  private static final long serialVersionUID = 1L;

  @Inject
  private HttpSession session;

  public String getMessage() {
    return "Session Tool , id=" + session.getId();
  }

}