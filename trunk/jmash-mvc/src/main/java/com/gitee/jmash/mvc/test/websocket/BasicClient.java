/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gitee.jmash.mvc.test.websocket;

import jakarta.websocket.ClientEndpoint;
import jakarta.websocket.ContainerProvider;
import jakarta.websocket.OnMessage;
import jakarta.websocket.OnOpen;
import jakarta.websocket.Session;
import jakarta.websocket.WebSocketContainer;
import java.io.IOException;
import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Basic WebSocket Client .
 *
 * @author Juneau
 */
@ClientEndpoint
public class BasicClient {

  Session session = null;

  private MessageHandler handler;

  /** Basic Client. */
  public BasicClient(URI endpointUri) {
    try {
      WebSocketContainer container = ContainerProvider.getWebSocketContainer();
      container.connectToServer(this, endpointUri);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  /** Basic OnOpen. */
  @OnOpen
  public void onOpen(Session session) {
    this.session = session;
    try {
      session.getBasicRemote().sendText("Opening connection");
    } catch (IOException ex) {
      System.out.println(ex);
    }
  }

  public void addMessageHandler(MessageHandler msgHandler) {
    this.handler = msgHandler;
  }

  @OnMessage
  public void processMessage(String message) {
    System.out.println("Received message in client: " + message);
    handler.handleMessage(message);
  }

  /** Basic sendMessage. */
  public void sendMessage(String message) {
    try {
      this.session.getBasicRemote().sendText(message);
    } catch (IOException ex) {
      Logger.getLogger(BasicClient.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  /** websocket close . */
  public void close() throws IOException {
    if (this.session != null) {
      this.session.close();
    }
  }

  /** Basic MessageHandler. */
  public static interface MessageHandler {

    public void handleMessage(String message);
  }
}
