/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gitee.jmash.mvc.test.websocket;

import jakarta.websocket.OnError;
import jakarta.websocket.OnMessage;
import jakarta.websocket.Session;
import jakarta.websocket.server.ServerEndpoint;
import java.io.IOException;

/**
 * Basic WebSocket Server.
 *
 * @author Juneau
 */
@ServerEndpoint(value = "/basicEndpoint")
public class BasicEndpoint {

  /** OnMessage Server. */
  @OnMessage
  public void onMessage(Session session, String message) {
    try {
      session.getBasicRemote().sendText("Sending from server endpoint: " + message);
    } catch (IOException ex) {
      ex.printStackTrace();
    }

  }

  @OnError
  public void onError(Session session, Throwable tshrowable) {
    System.out.println("There has been an error with session " + session.getId());
  }

}
