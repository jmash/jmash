/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gitee.jmash.mvc.test.websocket;

import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Named;
import java.util.HashMap;
import java.util.Map;

/**
 * Chat Session Controller.
 *
 * @author Juneau
 */
@Named
@ApplicationScoped
public class ChatSessionController implements java.io.Serializable {

  private static final long serialVersionUID = 1L;
  
  private Map<String, String> users = null;

  public ChatSessionController() {
  }

  @PostConstruct
  public void init() {
    users = new HashMap<>();
  }

  /**
   * the users.
   */
  public Map<String, String> getUsers() {
    return users;
  }

  /**
   * users the users to set.
   */
  public void setUsers(Map<String, String> users) {
    this.users = users;
  }

}
