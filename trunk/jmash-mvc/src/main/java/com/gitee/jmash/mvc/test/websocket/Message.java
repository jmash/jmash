/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gitee.jmash.mvc.test.websocket;

/**
 * Chat Message.
 *
 * @author Juneau
 */
public class Message implements java.io.Serializable {

  private static final long serialVersionUID = 1L;

  private String username;

  private String message;

  /**
   * the username.
   */
  public String getUsername() {
    return username;
  }

  /**
   * username the username to set.
   */
  public void setUsername(String username) {
    this.username = username;
  }

  /**
   * the message.
   */
  public String getMessage() {
    return message;
  }

  /**
   * message the message to set.
   */
  public void setMessage(String message) {
    this.message = message;
  }

  @Override
  public String toString() {
    return "Message [username=" + username + ", message=" + message + "]";
  }
  
  

}
