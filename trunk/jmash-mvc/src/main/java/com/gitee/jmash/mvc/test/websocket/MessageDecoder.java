/*
 * To change this license header, choose License Headers in Project Properties. To change this
 * template file, choose Tools | Templates and open the template in the editor.
 */

package com.gitee.jmash.mvc.test.websocket;

import jakarta.json.Json;
import jakarta.json.JsonObject;
import jakarta.json.JsonReader;
import jakarta.websocket.DecodeException;
import jakarta.websocket.Decoder;
import jakarta.websocket.EndpointConfig;
import java.io.StringReader;

/**
 * Message Decoder.
 *
 * @author Juneau
 */
public class MessageDecoder implements Decoder.Text<Message> {

  @Override
  public Message decode(String jsonMessage) throws DecodeException {
    try (JsonReader reader = Json.createReader(new StringReader(jsonMessage))) {
      JsonObject jsonObject = reader.readObject();
      Message message = new Message();
      message.setUsername(jsonObject.getString("username"));
      message.setMessage(jsonObject.getString("message"));
      return message;
    }
  }

  @Override
  public boolean willDecode(String jsonMessage) {
    try (JsonReader reader = Json.createReader(new StringReader(jsonMessage))) {
      // Check if incoming message is valid JSON
      reader.readObject();
      return true;
    } catch (Exception e) {
      return false;
    }
  }

  @Override
  public void init(EndpointConfig ec) {
    System.out.println("Initializing message decoder");
  }

  @Override
  public void destroy() {
    System.out.println("Destroyed message decoder");
  }

}
