/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gitee.jmash.mvc.test.websocket;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Test WebSocket.
 *
 * @author Juneau
 */
public class TestBasicWebSocket {

  /** . */
  public static void main(String[] args) {
    try {
      // open websocket
      final BasicClient clientEndPoint = new BasicClient(
          new URI("ws://localhost:8080/basicEndpoint"));

      // add listener
      clientEndPoint.addMessageHandler(new BasicClient.MessageHandler() {
        public void handleMessage(String message) {
          System.out.println(message);
        }
      });

      // send message to websocket
      clientEndPoint.sendMessage("Message sent from client!");

      
      // wait 5 seconds for messages from websocket
      Thread.sleep(5000);
      
      clientEndPoint.close();

    } catch (IOException | InterruptedException | URISyntaxException ex) {
      System.err.println("InterruptedException exception: " + ex.getMessage());
      Thread.currentThread().interrupt();
    }
  }
}
