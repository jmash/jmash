
package com.gitee.jmash.mvc.test.websocket;

import java.net.URI;
import java.net.URISyntaxException;

/** Test Chat . */
public class TestChatWebSocket {

  /** . */
  public static void main(String[] args) {
    try {
      // open websocket
      final ChatClient clientEndPoint = new ChatClient(
          new URI("ws://localhost:8080/chatEndpoint/testuser"));

      // add listener
      clientEndPoint.addMessageHandler(new ChatClient.MessageHandler() {
        public void handleMessage(Message message) {
          System.out.println(message);
        }
      });

      Message msg = new Message();
      msg.setMessage("Client Message.");
      msg.setUsername("cgd");
      // send message to websocket
      clientEndPoint.sendMessage(msg);

      // wait 5 seconds for messages from websocket
      Thread.sleep(500000000);

    } catch (InterruptedException | URISyntaxException ex) {
      System.err.println("InterruptedException exception: " + ex.getMessage());
      Thread.currentThread().interrupt();
    }

  }

}
