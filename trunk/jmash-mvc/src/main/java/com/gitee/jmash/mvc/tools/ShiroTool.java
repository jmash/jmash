
package com.gitee.jmash.mvc.tools;

import jakarta.enterprise.context.SessionScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import jakarta.servlet.http.HttpSession;
import java.io.Serializable;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.eclipse.microprofile.jwt.JsonWebToken;

/**
 * Shiro Secure annotation.
 *
 * @author CGD
 *
 */
@Named("shiro")
@SessionScoped
public class ShiroTool implements Serializable {

  private static final long serialVersionUID = 1L;

  // Delimeter that separates role names in tag attribute
  private static final String ROLE_NAMES_DELIMETER = ",";

  @Inject
  private HttpSession session;

  protected Subject getSubject() {
    return SecurityUtils.getSubject();
  }

  /** 用户没有身份验证. */
  public boolean getGuest() {
    return getSubject() == null || getSubject().getPrincipal() == null;
  }

  /** 用户已经经过认证/记住我登录. */
  public boolean getUser() {
    return getSubject() != null && getSubject().getPrincipal() != null;
  }

  /** 用户已经身份验证通过，即Subject.login登录成功，不是记住我登录的. */
  public boolean getAuthenticated() {
    return getSubject() != null && getSubject().isAuthenticated();
  }

  /** 用户未进行身份验证，即没有调用Subject.login进行登录，包括记住我自动登录的也属于未进行身份验证. */
  public boolean getNotAuthenticated() {
    return getSubject() == null || !getSubject().isAuthenticated();
  }

  /** 显示用户身份信息，默认调用Subject.getPrincipal() 获取，即 Primary Principal. */
  public JsonWebToken getPrincipal() {
    if (getSubject() != null) {
      return (JsonWebToken) getSubject().getPrincipal();
    }
    return null;
  }

  /** 如果当前 Subject 有角色. */
  public boolean hasRole(String roleName) {
    return getSubject() != null && getSubject().hasRole(roleName);
  }

  /** 如果当前 Subject 有角色. */
  public boolean hasAnyRoles(String roleNames) {
    boolean hasAnyRole = false;
    Subject subject = getSubject();

    if (subject != null) {
      // Iterate through roles and check to see if the user has one of the roles
      for (String role : roleNames.split(ROLE_NAMES_DELIMETER)) {
        if (subject.hasRole(role.trim())) {
          hasAnyRole = true;
          break;
        }
      }
    }

    return hasAnyRole;
  }

  /** 如果当前 Subject 没有角色. */
  public boolean lacksRole(String roleName) {
    boolean hasRole = getSubject() != null && getSubject().hasRole(roleName);
    return !hasRole;
  }

  /** 如果当前 Subject 有权限. */
  public boolean hasPermission(String p) {
    return getSubject() != null && getSubject().isPermitted(p);
  }

  /** 如果当前Subject没有权限. */
  public boolean lacksPermission(String p) {
    return !(getSubject() != null && getSubject().isPermitted(p));
  }

}
