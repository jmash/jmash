
package com.gitee.jmash.mvc.tools;

import com.gitee.jmash.common.utils.RunEnvInfo;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Named;

/**
 * Http Application @Named .
 *
 * @author CGD
 *
 */
@Named("sys")
@ApplicationScoped
public class SysTool {

  RunEnvInfo runEnvInfo = new RunEnvInfo();

  /** 运行环境信息. */
  public RunEnvInfo getRunEnvInfo() {
    return runEnvInfo;
  }

}