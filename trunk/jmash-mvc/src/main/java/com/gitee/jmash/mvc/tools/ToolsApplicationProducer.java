
package com.gitee.jmash.mvc.tools;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Produces;
import jakarta.inject.Named;
import org.apache.velocity.tools.generic.DateTool;
import org.apache.velocity.tools.generic.EscapeTool;
import org.apache.velocity.tools.generic.MathTool;
import org.apache.velocity.tools.generic.NumberTool;
import org.eclipse.krazo.ext.tools.JmashDateTool;

/**
 * Tools Application Producer.
 *
 * @author CGD
 *
 */
@ApplicationScoped
public class ToolsApplicationProducer {

  @Produces
  @Named("math")
  public MathTool getMathTool() {
    return new MathTool();
  }

  @Produces
  @Named("date")
  public DateTool getDateTool() {
    return new JmashDateTool();
  }

  @Produces
  @Named("esc")
  public EscapeTool getEscapeTool() {
    return new EscapeTool();
  }

  @Produces
  @Named("number")
  public NumberTool getNumberTool() {
    return new NumberTool();
  }

}
