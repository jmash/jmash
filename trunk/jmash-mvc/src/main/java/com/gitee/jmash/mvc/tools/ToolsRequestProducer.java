
package com.gitee.jmash.mvc.tools;

import jakarta.enterprise.context.RequestScoped;
import jakarta.enterprise.inject.Produces;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import jakarta.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import org.apache.velocity.tools.generic.RenderTool;
import org.apache.velocity.tools.generic.ResourceTool;

/**
 * Tools Request Producer.
 *
 * @author CGD
 *
 */
@RequestScoped
public class ToolsRequestProducer {

  @Inject
  private HttpServletRequest request;

  /** i18n. */
  @Produces
  @Named("text")
  public ResourceTool getResourceTool() {
    Map<String, Object> params = new HashMap<>();
    params.put("bundles", "jmash.messages,org.hibernate.validator.ValidationMessages");
    params.put("locale", request.getLocale().toLanguageTag());
    params.put("scope", "request");
    ResourceTool resource = new ResourceTool();
    resource.configure(params);
    return resource;
  }

  /** render. */
  @Produces
  @Named("render")
  public RenderTool getRenderTool() {
    Map<String, Object> params = new HashMap<>();
    params.put("scope", "request");
    RenderTool render = new RenderTool();
    render.configure(params);
    return render;
  }

}
