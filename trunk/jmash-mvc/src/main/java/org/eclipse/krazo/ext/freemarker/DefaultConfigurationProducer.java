/*
 * Copyright (c) 2014-2015 Oracle and/or its affiliates. All rights reserved. Copyright (c) 2018,
 * 2019 Eclipse Krazo committers and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package org.eclipse.krazo.ext.freemarker;

import com.gitee.jmash.common.freemarker.JmashObjectWrapper;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Produces;
import jakarta.inject.Inject;
import jakarta.servlet.ServletContext;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import org.eclipse.krazo.engine.ViewEngineConfig;
import freemarker.cache.TemplateLoader;
import freemarker.template.Configuration;

/**
 * Producer for the Freemarker {@link freemarker.template.Configuration} used by
 * {@link FreemarkerViewEngine}.
 *
 * @author Christian Kaltepoth
 */
@ApplicationScoped
public class DefaultConfigurationProducer {

  @Inject
  private ServletContext servletContext;

  /** Config Freemarker . */
  @Produces
  @ViewEngineConfig
  public Configuration getConfiguration() {

    Configuration configuration = new Configuration(Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS);
    configuration.setDefaultEncoding("UTF-8");
    configuration.setTemplateLoader(new TemplateLoader() {

      @Override
      public Object findTemplateSource(String s) throws IOException {
        // Servlet Get Url. Freemarker drops "/"
        URL url = servletContext.getResource("/" + s);
        if (url == null) {
          // 从Jar包中获取
          url = Thread.currentThread().getContextClassLoader().getResource("/" + s);
        }
        return url == null ? null : new UrlTemplateSource(url, false);
      }

      @Override
      public long getLastModified(Object templateSource) {
        return ((UrlTemplateSource) templateSource).lastModified();
      }

      @Override
      public Reader getReader(Object templateSource, String encoding) throws IOException {
        return new InputStreamReader(((UrlTemplateSource) templateSource).getInputStream(),
            encoding);
      }

      @Override
      public void closeTemplateSource(Object templateSource) throws IOException {
        ((UrlTemplateSource) templateSource).close();
      }
    });
    // freemarker 支持LocalDateTime等.
    configuration.setObjectWrapper(new JmashObjectWrapper(Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS));
    return configuration;

  }

}
