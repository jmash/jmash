
package org.eclipse.krazo.ext.freemarker;

import com.gitee.jmash.mvc.NamedAnnotation;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.DefaultObjectWrapperBuilder;
import freemarker.template.TemplateHashModel;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import jakarta.enterprise.context.spi.Contextual;
import jakarta.enterprise.inject.Instance;
import jakarta.enterprise.inject.spi.CDI;
import jakarta.mvc.Models;
import jakarta.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.weld.bean.ForwardingBean;
import org.jboss.weld.context.api.ContextualInstance;

/**
 * Default , CDI @Named , Models.
 *
 * @author CGD
 *
 */
public class DefaultTemplateHashModel implements TemplateHashModel {

  private static Log log = LogFactory.getLog(DefaultTemplateHashModel.class);

  HttpServletRequest request;

  Models models;

  Map<String, Object> defaultMap = new HashMap<>();

  /**
   * Default , CDI @Named , Models.
   */
  public DefaultTemplateHashModel(HttpServletRequest request, Models models) {
    super();
    this.request = request;
    this.models = models;
    // HTTP
    defaultMap.put("request", request);
    defaultMap.put("session", request.getSession());
    defaultMap.put("servletContext", request.getServletContext());
    // ContextPath
    defaultMap.put("base", request.getContextPath());
    // CDI @Named Models Process.
    modelsNamedProcess(request);
  }

  @Override
  public TemplateModel get(String key) throws TemplateModelException {
    DefaultObjectWrapper objectWrapper = new DefaultObjectWrapperBuilder(
        Configuration.VERSION_2_3_31).build();
    // 1.Default mvc.
    if (key.equals("mvc")) {
      return objectWrapper.wrap(models.get(key));
    }
    // 2.优先Default.
    if (defaultMap.containsKey(key) && null != models.get(key)) {
      log.warn("Models Not Put default '" + key + "' ");
    }
    if (defaultMap.containsKey(key)) {
      return objectWrapper.wrap(defaultMap.get(key));
    }
    // 3.CDI Named.
    Instance<Object> instance = CDI.current().select(Object.class, new NamedAnnotation(key));
    if (!instance.isUnsatisfied() && null != models.get(key)) {
      log.warn("Models Not Put default '" + key + "' ");
    }
    if (!instance.isUnsatisfied()) {
      return objectWrapper.wrap(instance.get());
    }
    // 4. Models.
    return objectWrapper.wrap(models.get(key));
  }

  @Override
  public boolean isEmpty() throws TemplateModelException {
    return false;
  }

  /**
   * CDI @Named Models Process.
   */
  private void modelsNamedProcess(HttpServletRequest request) {
    // CDI @Named Models Process.
    Enumeration<String> attrs = request.getAttributeNames();
    while (attrs.hasMoreElements()) {
      Object attr = request.getAttribute(attrs.nextElement());
      // Weld CDI
      if (attr instanceof ContextualInstance) {
        ContextualInstance<?> contextualInstance = (ContextualInstance<?>) attr;
        Contextual<?> contextual = contextualInstance.getContextual();
        if (contextual instanceof ForwardingBean) {
          String cdiName = ((ForwardingBean<?>) contextual).getName();
          if (null != cdiName && !"mvc".equals(cdiName)) {
            log.debug("CDI @Named Models :" + cdiName);
            // @Named DI FreeMarker.
            defaultMap.put(cdiName, contextualInstance.getInstance());
          }
        }
      }
    }
  }

}
