package org.eclipse.krazo.ext.tools;

import com.crenjoy.proto.beanutils.ProtoConvertUtils;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import org.apache.velocity.tools.ConversionUtils;
import org.apache.velocity.tools.config.DefaultKey;
import org.apache.velocity.tools.generic.DateTool;

@DefaultKey("date")
public class JmashDateTool extends DateTool {

  private static final long serialVersionUID = 1L;

  public Date toDate(String format, Object obj, Locale locale, TimeZone timezone) {
    Date date = ConversionUtils.toDate(obj, format, locale, timezone);
    if (date == null) {
      date = ProtoConvertUtils.convert(obj, Date.class);
    }
    return date;
  }

}
