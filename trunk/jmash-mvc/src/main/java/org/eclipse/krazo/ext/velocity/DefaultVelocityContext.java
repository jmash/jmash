
package org.eclipse.krazo.ext.velocity;

import com.gitee.jmash.mvc.NamedAnnotation;
import jakarta.enterprise.context.spi.Contextual;
import jakarta.enterprise.inject.Instance;
import jakarta.enterprise.inject.spi.CDI;
import jakarta.mvc.Models;
import jakarta.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.context.AbstractContext;
import org.jboss.weld.bean.ForwardingBean;
import org.jboss.weld.context.api.ContextualInstance;

/**
 * Default , CDI @Named , Models.
 *
 * @author CGD
 *
 */
public class DefaultVelocityContext extends AbstractContext implements Cloneable, Serializable {

  private static final long serialVersionUID = 1L;

  private static Log log = LogFactory.getLog(DefaultVelocityContext.class);

  HttpServletRequest request;

  Models models;

  Map<String, Object> defaultMap = new HashMap<>();

  Map<String, Object> context = null;

  /**
   * Default , CDI @Named , Models.
   */
  public DefaultVelocityContext(HttpServletRequest request, Models models) {
    super();
    this.request = request;
    this.models = models;
    // HTTP
    defaultMap.put("request", request);
    defaultMap.put("session", request.getSession());
    defaultMap.put("servletContext", request.getServletContext());
    // ContextPath
    defaultMap.put("base", request.getContextPath());
    // CDI @Named Models Process.
    modelsNamedProcess(request);
    // Create Context.
    context = new HashMap<>(defaultMap);
    // Add Models.
    for (Entry<String, Object> entry : models.asMap().entrySet()) {
      if (defaultMap.containsKey(entry.getKey())) {
        log.warn("Models Not Put default '" + entry.getKey() + "' ");
        continue;
      }
      context.put(entry.getKey(), entry.getValue());
    }
  }

  @Override
  public Object internalGet(String key) {
    // 1.Context.
    if (context.containsKey(key)) {
      return context.get(key);
    }
    // 2.CDI Named.
    Instance<Object> instance = CDI.current().select(Object.class, new NamedAnnotation(key));
    if (!instance.isUnsatisfied() && null != models.get(key)) {
      log.warn("Models Not Put default '" + key + "' ");
    }
    if (!instance.isUnsatisfied()) {
      return instance.get();
    }
    return null;
  }

  @Override
  public Object internalPut(String key, Object value) {
    if (defaultMap.containsKey(key)) {
      log.warn("Repeat default Not Put  : '" + key + "' ");
      return null;
    }
    if (null != models.get(key)) {
      log.warn("Repeat Models Not Put  : '" + key + "' ");
      return null;
    }
    Instance<Object> instance = CDI.current().select(Object.class, new NamedAnnotation(key));
    if (!instance.isUnsatisfied()) {
      log.warn("Repeat @Named Not Put  : '" + key + "' ");
      return null;
    }
    return context.put(key, value);
  }

  @Override
  public boolean internalContainsKey(String key) {
    // 1.Context
    if (context.containsKey(key)) {
      return true;
    }
    // 2.CDI Named.
    Instance<Object> instance = CDI.current().select(Object.class, new NamedAnnotation(key));
    return !instance.isUnsatisfied();
  }

  @Override
  public String[] internalGetKeys() {
    return context.keySet().toArray(new String[context.size()]);
  }

  @Override
  public Object internalRemove(String key) {
    return context.remove(key);
  }

  /**
   * CDI @Named Models Process.
   */
  private void modelsNamedProcess(HttpServletRequest request) {
    // CDI @Named Models Process.
    Enumeration<String> attrs = request.getAttributeNames();
    while (attrs.hasMoreElements()) {
      Object attr = request.getAttribute(attrs.nextElement());
      // Weld CDI
      if (attr instanceof ContextualInstance) {
        ContextualInstance<?> contextualInstance = (ContextualInstance<?>) attr;
        Contextual<?> contextual = contextualInstance.getContextual();
        if (contextual instanceof ForwardingBean) {
          String cdiName = ((ForwardingBean<?>) contextual).getName();
          if (null != cdiName && !"mvc".equals(cdiName)) {
            log.debug("CDI @Named Models :" + cdiName);
            // @Named DI FreeMarker.
            defaultMap.put(cdiName, contextualInstance.getInstance());
          }
        }
      }
    }
  }

}
