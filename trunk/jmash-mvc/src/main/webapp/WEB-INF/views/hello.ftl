<html lang="${mvc.locale}">
<head>
  <meta charset="UTF-8">
  <title>Hello Freemarker!</title>
</head>
<body>
  <h1>Hello Freemarker, Welcome!</h1>
  <h2>1.MVC Context</h2>
  <pre>
  Context Path                         : ${base}
  Application's base path              : ${mvc.basePath}
  Local                                : ${mvc.locale}
  URI (FreemarkerController#hello)     : ${mvc.uri("FreemarkerController#hello")}
  URI (FreemarkerController#execute)   : ${mvc.uri("FreemarkerController#execute")}
  ${mvc.csrf.name}                     : ${mvc.csrf.token}
  </pre>
</body>
</html>