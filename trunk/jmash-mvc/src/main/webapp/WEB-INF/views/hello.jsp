<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="${mvc.locale}">
<head>
    <meta charset="UTF-8">
    <title>Hello World!</title>
</head>
<body>
  <h1>Hello World!</h1>
  <pre>
  Context Path                     : ${pageContext.request.contextPath}
  Application's base path          : ${mvc.basePath}
  Local                            : ${mvc.locale}
  URI (JspController#hello)        : ${mvc.uri("JspController#hello")}
  URI (JspController#execute)      : ${mvc.uri("JspController#execute")}
  ${mvc.csrf.name}                 : ${mvc.csrf.token}
  </pre>
</body>
</html>