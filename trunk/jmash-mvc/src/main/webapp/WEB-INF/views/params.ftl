<html lang="${mvc.locale}">
<head>
    <meta charset="UTF-8">
    <title>Freemarker Params</title>
</head>
<body>
<h2>1.Models Put</h2>
<pre>Hello ${visitor}! , Welcome Jakarta MVC for Freemarker !</pre>

<h2>2.Models @Named</h2>
<pre>${greeting.message} , CDI Named !</pre>

<h2>3.CDI @Named</h2>
<pre>${applicationTool.message} ,Application Tool !</pre>
<pre>${sessionTool.message} ,Session Tool !</pre>
<pre>${requestTool.message} ,Request Tool !</pre>

<h2>4.i18n</h2>
<pre>${text.language.select} , ResourceTool !</pre>


<h2>5.Math</h2>
<pre>${math.add( 1 , 1 )} , MathTool !</pre>

<h2>6.Date</h2>
<pre>${greeting.nowDate}  !</pre>

<pre>${date.format( 'yyyy-MM-dd HH:mm',greeting.nowDate )}  </pre>

</body>
</html>