<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>JMash MVC</title>
</head>
<body >
    <h2>Jakarta MVC , Hello World!</h2>
    <br/>
     <a href="${pageContext.request.contextPath}/mvc/version" target="blank_"><h3>Jakarta Restful</h3> </a>
    <br/>
    <a href="${pageContext.request.contextPath}/mvc/jsp/hello" target="blank_"><h3>MVC for JSP</h3></a>
    <a href="${pageContext.request.contextPath}/mvc/jsp/params?name=JSP" target="blank_"><h3>MVC With Params for JSP</h3></a>
    <br/>
    <br/>
    <a href="${pageContext.request.contextPath}/mvc/freemarker/hello" target="blank_"><h3>MVC for FreeMarker</h3></a>
    <a href="${pageContext.request.contextPath}/mvc/freemarker/params?name=FreeMarker" target="blank_"><h3>MVC With Params for FreeMarker</h3></a>
    <br/>
    <br/>
    <a href="${pageContext.request.contextPath}/mvc/velocity/hello" target="blank_"><h3>MVC for Velocity</h3></a>
    <a href="${pageContext.request.contextPath}/mvc/velocity/params?name=Velocity" target="blank_"><h3>MVC With Params for Velocity</h3></a>
    <br/>
</body>
</html>