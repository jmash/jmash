
package com.gitee.jmash.propertyset;

import com.gitee.jmash.core.orm.jpa.TenantEntityManager;
import com.gitee.jmash.propertyset.service.PropertySetRead;
import com.gitee.jmash.propertyset.service.PropertySetReadBean;
import com.gitee.jmash.propertyset.service.PropertySetWrite;
import com.gitee.jmash.propertyset.service.PropertySetWriteBean;
import jakarta.enterprise.inject.spi.CDI;
import jakarta.persistence.spi.PersistenceUnitTransactionType;

public class PropSetFactory {

  //租户内部使用
  public static PropertySetReadBean getPropertySetReadBean(TenantEntityManager tem, String entityId,
      String entityName) {
    return new PropertySetReadBean(tem, entityId, entityName);
  }

  //租户内部使用
  public static PropertySetWriteBean getPropertySetWriteBean(TenantEntityManager tem,
      String entityId, String entityName) {
    return new PropertySetWriteBean(tem, PersistenceUnitTransactionType.RESOURCE_LOCAL, entityId,
        entityName);
  }

  //外部使用
  public static PropertySetRead getPropSetRead(String entityName, String key) {
    PropertySetRead p = CDI.current().select(PropertySetRead.class).get();
    if (p instanceof PropertySetReadBean) {
      ((PropertySetReadBean) p).setEntityName(entityName);
      ((PropertySetReadBean) p).setEntityId(key);
    }
    return p;
  }

  //外部使用
  public static PropertySetWrite getPropSetWrite(String entityName, String key) {
    PropertySetWrite p = CDI.current().select(PropertySetWrite.class).get();
    if (p instanceof PropertySetWriteBean) {
      ((PropertySetWriteBean) p).setEntityName(entityName);
      ((PropertySetWriteBean) p).setEntityId(key);
    }
    return p;
  }

  public static void destroy(Object instance) {
    CDI.current().destroy(instance);
  }

}
