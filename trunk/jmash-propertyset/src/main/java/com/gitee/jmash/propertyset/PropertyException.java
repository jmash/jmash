
package com.gitee.jmash.propertyset;

/**
 * 属性运行时异常.
 * 
 * @version 1.0 2009-4-2
 */
public class PropertyException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  public PropertyException() {
    super();
  }

  public PropertyException(String msg) {
    super(msg);
  }
}
