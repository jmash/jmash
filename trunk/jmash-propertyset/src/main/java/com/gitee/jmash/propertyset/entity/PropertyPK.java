
package com.gitee.jmash.propertyset.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import java.io.Serializable;

/**
 * @author Hani Suleiman Date: Nov 8, 2005 Time: 5:57:51 PM
 */
@Embeddable
public class PropertyPK implements Serializable {
  private static final long serialVersionUID = -6909978213448050937L;
  private String entityName;
  private String entityId;
  private String key;

  public PropertyPK() {
  }

  public PropertyPK(String entityName, String entityId, String key) {
    this.entityName = entityName;
    this.entityId = entityId;
    this.key = key;
  }

  @Column(name = "entity_name")
  public String getEntityName() {
    return entityName;
  }

  public void setEntityName(String entityName) {
    this.entityName = entityName;
  }

  @Column(name = "entity_id")
  public String getEntityId() {
    return entityId;
  }

  public void setEntityId(String entityId) {
    this.entityId = entityId;
  }

  @Column(name = "key_name")
  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((entityId == null) ? 0 : entityId.hashCode());
    result = prime * result + ((entityName == null) ? 0 : entityName.hashCode());
    result = prime * result + ((key == null) ? 0 : key.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    PropertyPK other = (PropertyPK) obj;
    if (entityId == null) {
      if (other.entityId != null)
        return false;
    } else if (!entityId.equals(other.entityId))
      return false;
    if (entityName == null) {
      if (other.entityName != null)
        return false;
    } else if (!entityName.equals(other.entityName))
      return false;
    if (key == null) {
      if (other.key != null)
        return false;
    } else if (!key.equals(other.key))
      return false;
    return true;
  }

}
