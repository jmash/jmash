package com.gitee.jmash.propertyset.enums;

import java.util.Date;
import java.util.Properties;

import org.w3c.dom.Document;

/**
 * 属性的类型枚举类
 * 
 * @author CGD
 * @version 1.0 2009-4-2
 */
public enum PropertyType {

	/** Value-type ANY 0 任何支持的类型 */
	ANY,

	/** Value-type boolean 1 */
	BOOLEAN,

	/** Value-type int 2 */
	INT,

	/** Value-type long 3 */
	LONG,

	/** Value-type double 4 */
	DOUBLE,

	/** Value-type {@link java.lang.String} (max length 255) 5 */
	STRING,

	/** Value-type text (unlimited length {@link java.lang.String}) 6 */
	TEXT,

	/** Value-type {@link java.util.Date} 7 */
	DATE,

	/** Value-type serializable {@link java.lang.Object} 8 */
	OBJECT,

	/** Value-type XML {@link org.w3c.dom.Document} 9 */
	XML,

	/** Value-type byte[] 10 */
	DATA,

	/** Value-type {@link java.util.Properties} 11 */
	PROPERTIES;

	@SuppressWarnings({ "rawtypes" })
	public Class getClassType() {
		return PropertyType.getClassType(this);
	}

	/**
	 * 通过原始顺序获取属性类型枚举
	 * 
	 * @param i 顺序
	 * @return 属性类型枚举
	 */
	public static PropertyType fromOrdinal(int i) {
		if (i < 0 || i >= PropertyType.values().length) {
			throw new IndexOutOfBoundsException("Invalid ordinal");
		}
		return PropertyType.values()[i];
	}

	/**
	 * 与实际类型转换
	 * 
	 * @param type
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public static Class getClassType(PropertyType type) {
		Class classType = null;
		switch (type) {
		case BOOLEAN:
			classType = Boolean.class;
			break;
		case INT:
			classType = Integer.class;
			break;
		case LONG:
			classType = Long.class;
			break;
		case DOUBLE:
			classType = Double.class;
			break;
		case STRING:
			classType = String.class;
			break;
		case TEXT:
			classType = String.class;
			break;
		case DATE:
			classType = Date.class;
			break;
		case XML:
			classType = Document.class;
			break;
		case DATA:
			classType = Byte[].class;
			break;
		case PROPERTIES:
			classType = Properties.class;
			break;
		case OBJECT:
			classType = Object.class;
			break;
		default:
			break;
		}
		return classType;
	}

	/**
	 * 当前值使用的类型
	 * 
	 * @param value
	 * @return
	 */
	public static PropertyType getPropertyType(Object value) {
		PropertyType type = null;
		if (value instanceof Boolean) {
			type = BOOLEAN;
		} else if (value instanceof Integer) {
			type = INT;
		} else if (value instanceof Long) {
			type = LONG;
		} else if (value instanceof Double) {
			type = DOUBLE;
		} else if (value instanceof String) {
			if (value.toString().length() > 255) {
				type = TEXT;
			} else {
				type = STRING;
			}
		} else if (value instanceof Date) {
			type = DATE;
		} else if (value instanceof Document) {
			type = XML;
		} else if (value instanceof byte[]) {
			type = DATA;
		} else if (value instanceof Properties) {
			type = PROPERTIES;
		} else {
			type = OBJECT;
		}
		return type;
	}

	// @SuppressWarnings("unchecked")
	// public static PropertyType getPropertyType(Class classType)
	// throws PropertyException {
	// PropertyType type=null;
	//
	// if (Boolean.class.equals(classType)) {
	// type = BOOLEAN;
	// } else if (Integer.class.equals(classType)) {
	// type = INT;
	// } else if (Long.class.equals(classType)) {
	// type = LONG;
	// } else if (Double.class.equals(classType)) {
	// type = DOUBLE;
	// } else if (String.class.equals(classType)) {
	// type = TEXT;
	// } else if (Date.class.equals(classType)) {
	// type = DATE;
	// } else if (Document.class.equals(classType)) {
	// type = XML;
	// } else if (Byte[].class.equals(classType)) {
	// type = DATA;
	// } else if (String.class.equals(classType)) {
	// type = PROPERTIES;
	// } else {
	// type = OBJECT;
	// }
	// return type;
	// }

}
