/*
 * Copyright (c) 2002-2003 by OpenSymphony
 * All rights reserved.
 */

package com.gitee.jmash.propertyset.service;

import com.gitee.jmash.common.utils.Data;
import com.gitee.jmash.propertyset.IllegalPropertyException;
import com.gitee.jmash.propertyset.PropertyException;
import com.gitee.jmash.propertyset.enums.PropertyType;
import java.util.Collection;
import java.util.Date;
import java.util.Properties;
import org.w3c.dom.Document;

/**
 * Base implementation of PropertySet.
 * 
 * <p>
 * Performs necessary casting for get???/set??? methods which wrap around the
 * following 2 methods which are declared <code>protected abstract</code> and
 * need to be implemented by subclasses:
 * </p>
 * 
 * <ul>
 * <li>{#get(int,java.lang.String)}</li>
 * <li>{#setImpl(int,java.lang.String,java.lang.Object)}</li>
 * </ul>
 * 
 * <p>
 * The following methods are declared <code>public abstract</code> and are the
 * remainder of the methods that need to be implemented at the very least:
 * </p>
 * 
 * <ul>
 * <li>{#exists(java.lang.String)}</li>
 * <li>{#remove(java.lang.String)}</li>
 * <li>{#getType(java.lang.String)}</li>
 * <li>{#getKeys(java.lang.String,int)}</li>
 * </ul>
 * 
 * <p>
 * The <code>supports???</code> methods are implemented and all return true by
 * default. Override if necessary.
 * </p>
 * 
 * @author <a href="mailto:joe@truemesh.com">Joe Walnes</a>
 * @author <a href="mailto:hani@fate.demon.co.uk">Hani Suleiman</a>
 * @version $Revision: 151 $
 */
public abstract class AbstractPropertySetWrite implements PropertySetWrite {
  // ~ Instance fields
  // ////////////////////////////////////////////////////////

  // ~ Methods
  // ////////////////////////////////////////////////////////////////
  public void setAsActualType(String key, Object value) throws PropertyException {
    PropertyType type = PropertyType.getPropertyType(value);
    set(type, key, value);
  }

  public void setBoolean(String key, boolean value) {
    set(PropertyType.BOOLEAN, key, value ? Boolean.TRUE : Boolean.FALSE);
  }

  /**
   * Constructs {com.opensymphony.util.Data} wrapper around bytes.
   */
  public void setData(String key, byte[] value) {
    set(PropertyType.DATA, key, new Data(value));
  }

  public void setDate(String key, Date value) {
    set(PropertyType.DATE, key, value);
  }

  public void setDouble(String key, double value) {
    set(PropertyType.DOUBLE, key, Double.valueOf(value));
  }

  public void setInt(String key, int value) {
    set(PropertyType.INT, key, Integer.valueOf(value));
  }

  public void setLong(String key, long value) {
    set(PropertyType.LONG, key, Long.valueOf(value));
  }

  public void setObject(String key, Object value) {
    set(PropertyType.OBJECT, key, value);
  }

  public void setProperties(String key, Properties value) {
    set(PropertyType.PROPERTIES, key, value);
  }

  /**
   * Returns true.
   */
  public boolean isSettable(String property) {
    return true;
  }

  /**
   * Returns true.
   */
  public boolean supportsType(PropertyType type) {
    return true;
  }

  /**
   * Returns true.
   */
  public boolean supportsTypes() {
    return true;
  }

  /**
   * Throws IllegalPropertyException if value length greater than 255.
   */
  public void setString(String key, String value) {
    if ((value != null) && (value.length() > 255)) {
      throw new IllegalPropertyException("String exceeds 255 characters.");
    }

    set(PropertyType.STRING, key, value);
  }

  public void setText(String key, String value) {
    set(PropertyType.TEXT, key, value);
  }

  public void setXML(String key, Document value) {
    set(PropertyType.XML, key, value);
  }

  protected abstract void setImpl(PropertyType type, String key, Object value)
      throws PropertyException;

  private void set(PropertyType type, String key, Object value) throws PropertyException {

    // we're ok this far, so call the actual setter.
    setImpl(type, key, value);
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * gcloud.propertyset.PropertySet#setMultiValueAsActualType(java.lang.String,
   * java.lang.Object[])
   */
  public void setMultiValueAsActualType(String key, Object[] value) throws PropertyException {
    if (value == null) {
      return;
    }
    String p = String.format("%s_%s", key, PropertySetRead.MULTI_VALUE);
    // 清除原值
    Collection<String> keys = getKeys(p, PropertyType.ANY);
    for (String s : keys) {
      if (s.startsWith(p)) {
        this.remove(s);
      }
    }
    if (value.length == 0) {
      return;
    }
    // 类型验证
    PropertyType type = PropertyType.getPropertyType(value[0]);

    // 保存属性
    for (int i = 0; i < value.length; i++) {
      this.setImpl(type, String.format("%s_%d", p, i), value[i]);
    }
  }

  /**
   * 获取Key值集合,指定prefix,指定PropertyType
   * 
   * @return
   * @throws PropertyException
   */
  public abstract Collection<String> getKeys(String prefix, PropertyType type)
      throws PropertyException;
}
