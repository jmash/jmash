package com.gitee.jmash.propertyset.service;

import com.gitee.jmash.propertyset.PropertyException;
import com.gitee.jmash.propertyset.enums.PropertyType;

import java.util.Collection;
import java.util.Date;
import java.util.Properties;

import org.w3c.dom.Document;


/**
 * 提供的属性集接口
 * 
 * @version $Revision: 1 $ 2009-4-2
 */
public interface PropertySetRead extends AutoCloseable {

	public static final String MULTI_VALUE="p"; 

	/**
	 * 获取所有的Key值
	 * @return
	 * @throws PropertyException
	 */
	Collection<String> getKeys() throws PropertyException;

	/**
	 * 获取Key值集合,指定PropertyType
	 * @return
	 * @throws PropertyException
	 */
	Collection<String> getKeys(PropertyType type) throws PropertyException;

	/**
	 * 获取Key值集合,指定prefix
	 * @return
	 * @throws PropertyException
	 */
	Collection<String> getKeys(String prefix) throws PropertyException;

	/**
	 * 获取Key值集合,指定prefix,指定PropertyType
	 * @return
	 * @throws PropertyException
	 */
	Collection<String> getKeys(String prefix, PropertyType type)
			throws PropertyException;

	/**
	 * Returns type of value.
	 * 
	 * @return Type of value. See static class variables.
	 */
	PropertyType getType(String key) throws PropertyException;

	/**
	 * Determine if property exists.
	 */
	boolean exists(String key) throws PropertyException;
	
	Object getAsActualType(String key) throws PropertyException;

	boolean getBoolean(String key) throws PropertyException;

	byte[] getData(String key) throws PropertyException;

	Date getDate(String key) throws PropertyException;

	double getDouble(String key) throws PropertyException;

	int getInt(String key) throws PropertyException;

	Document getXML(String key) throws PropertyException;
	/**
	 * {@link java.lang.String} of maximum 255 chars.
	 */
	String getString(String key) throws PropertyException;

	/**
	 * {@link java.lang.String} of unlimited length.
	 */
	String getText(String key) throws PropertyException;

	long getLong(String key) throws PropertyException;

	Object getObject(String key) throws PropertyException;

	Properties getProperties(String key) throws PropertyException;
	/**
	 * 支持获取多值
	 * @param key
	 * @return
	 * @throws PropertyException
	 */
	Object[] getMultiValueAsActualType(String key) throws PropertyException;

}
