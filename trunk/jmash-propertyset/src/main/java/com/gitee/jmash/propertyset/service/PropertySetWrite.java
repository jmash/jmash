
package com.gitee.jmash.propertyset.service;

import com.gitee.jmash.propertyset.PropertyException;
import com.gitee.jmash.propertyset.enums.PropertyType;
import java.util.Date;
import java.util.Properties;
import org.w3c.dom.Document;

/**
 * 提供的属性集接口.
 * 
 * @version $Revision: 1 $ 2009-4-2
 */
public interface PropertySetWrite extends AutoCloseable {

  /**
   * Whether this PropertySet implementation allows values to be set (as opposed
   * to read-only).
   */
  boolean isSettable(String property);

  /**
   * Removes property.
   */
  void remove(String key) throws PropertyException;

  /**
   * Remove the propertyset and all it associated keys.
   * 
   * @throws PropertyException if there is an error removing the propertyset.
   */
  void remove() throws PropertyException;

  /**
   * Whether this PropertySet implementation allows the type specified to be
   * stored or retrieved. PropertyType.ANY return false
   */
  boolean supportsType(PropertyType type);

  /**
   * Whether this PropertySet implementation supports types when storing values.
   * (i.e. the type of data is stored as well as the actual value).
   */
  boolean supportsTypes();

  void setAsActualType(String key, Object value) throws PropertyException;

  void setBoolean(String key, boolean value) throws PropertyException;

  void setData(String key, byte[] value) throws PropertyException;

  void setDate(String key, Date value) throws PropertyException;

  void setDouble(String key, double value) throws PropertyException;

  void setInt(String key, int value) throws PropertyException;

  void setXML(String key, Document value) throws PropertyException;

  void setString(String key, String value) throws PropertyException;

  void setText(String key, String value) throws PropertyException;

  void setLong(String key, long value) throws PropertyException;

  void setObject(String key, Object value) throws PropertyException;

  void setProperties(String key, Properties value) throws PropertyException;

  /**
   * 支持多值存储.
   * 
   * @param key
   * @param value
   * @throws PropertyException
   */
  void setMultiValueAsActualType(String key, Object[] value) throws PropertyException;

}
