/*
 * Copyright (c) 2002-2003 by OpenSymphony
 * All rights reserved.
 */

package com.gitee.jmash.propertyset.service.test;

import com.gitee.jmash.propertyset.IllegalPropertyException;
import com.gitee.jmash.propertyset.PropertyException;
import com.gitee.jmash.propertyset.enums.PropertyType;
import com.gitee.jmash.propertyset.service.PropertySetRead;
import com.gitee.jmash.propertyset.service.PropertySetWrite;

import java.text.DateFormat;
import java.util.Date;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;

/**
 * User: bbulger Date: May 22, 2004
 */
public abstract class AbstractPropertySetTest {
  // ~ Instance fields
  // ////////////////////////////////////////////////////////

  protected static PropertySetWrite psWrite;

  public abstract PropertySetRead getPsRead();

  protected final String TEXT_VALUE = "12345678901234567890123456789012345678901234567890"
      + "12345678901234567890123456789012345678901234567890"
      + "12345678901234567890123456789012345678901234567890"
      + "12345678901234567890123456789012345678901234567890"
      + "12345678901234567890123456789012345678901234567890"
      + "12345678901234567890123456789012345678901234567890";

  // ~ Methods
  // ////////////////////////////////////////////////////////////////

  @Test
  public void testExistsOnPropertyInPropertyType() {
    psWrite.setString("test1", "value1");
    Assertions.assertTrue(getPsRead().exists("test1"));
  }

  @Test
  public void testExistsOnPropertyNotInPropertyType() {
    Assertions.assertFalse(getPsRead().exists("test425"));
  }

  @Test
  public void testGetKeys() {
    psWrite.setString("test1", "value1");
    psWrite.setString("test2", "value2");
    psWrite.setString("test3", "value3");
    Assertions.assertEquals(3, getPsRead().getKeys().size());
  }

  @Test
  public void testGetKeysOfType() {
    if (psWrite.supportsTypes()) {
      psWrite.setString("test1", "value1");
      psWrite.setString("test2", "value2");
      psWrite.setInt("testInt", 14);
      Assertions.assertEquals(2, getPsRead().getKeys(PropertyType.STRING).size());
      Assertions.assertEquals(1, getPsRead().getKeys(PropertyType.INT).size());
    }
  }

  @Test
  public void testGetKeysWithPrefix() {
    psWrite.setString("test1", "value1");
    psWrite.setString("test2", "value2");
    psWrite.setString("username", "user1");
    Assertions.assertEquals(2, getPsRead().getKeys("test").size());
    Assertions.assertEquals(1, getPsRead().getKeys("user").size());
  }

  @Test
  public void testGetKeysWithPrefixOfType() {
    if (psWrite.supportsTypes()) {
      psWrite.setString("test1", "value1");
      psWrite.setString("test2", "value2");
      psWrite.setString("username", "user1");
      psWrite.setInt("testInt", 32);
      psWrite.setInt("usernum", 18);
      Assertions.assertEquals(2, getPsRead().getKeys("test", PropertyType.STRING).size());
      Assertions.assertEquals(1, getPsRead().getKeys("user", PropertyType.STRING).size());
      Assertions.assertEquals(1, getPsRead().getKeys("test", PropertyType.INT).size());
      Assertions.assertEquals(1, getPsRead().getKeys("user", PropertyType.INT).size());
    }
  }

  @Test
  public void testGetStringNotInPropertyType() {
    Assertions.assertNull(getPsRead().getString("test555"));
  }

  @Test
  public void testGetTypeForBoolean() {
    if (psWrite.supportsType(PropertyType.BOOLEAN)) {
      psWrite.setBoolean("testBoolean", true);
      Assertions.assertEquals(PropertyType.BOOLEAN, getPsRead().getType("testBoolean"));
    }
  }

  @Test
  public void testGetTypeForData() {
    if (psWrite.supportsType(PropertyType.DATA)) {
      psWrite.setData("testData", "value2".getBytes());
      Assertions.assertEquals(PropertyType.DATA, getPsRead().getType("testData"));
    }
  }

  @Test
  public void testGetTypeForDate() {
    if (psWrite.supportsType(PropertyType.DATE)) {
      psWrite.setDate("testDate", new Date());
      Assertions.assertEquals(PropertyType.DATE, getPsRead().getType("testDate"));
    }
  }

  @Test
  public void testGetTypeForDouble() {
    if (psWrite.supportsType(PropertyType.DOUBLE)) {
      psWrite.setDouble("testDouble", 10.456D);
      Assertions.assertEquals(PropertyType.DOUBLE, getPsRead().getType("testDouble"));
    }
  }

  @Test
  public void testGetTypeForInt() {
    if (psWrite.supportsType(PropertyType.INT)) {
      psWrite.setInt("testInt", 7);
      Assertions.assertEquals(PropertyType.INT, getPsRead().getType("testInt"));
    }
  }

  @Test
  public void testGetTypeForLong() {
    if (psWrite.supportsType(PropertyType.LONG)) {
      psWrite.setLong("testLong", 7L);
      Assertions.assertEquals(PropertyType.LONG, getPsRead().getType("testLong"));
    }
  }

  @Test
  public void testGetTypeForObject() {
    if (psWrite.supportsType(PropertyType.OBJECT)) {
      psWrite.setObject("testObject", new StringBuffer());
      Assertions.assertEquals(PropertyType.OBJECT, getPsRead().getType("testObject"));
    }
  }

  @Test
  public void testGetTypeForProperties() {
    if (psWrite.supportsType(PropertyType.PROPERTIES)) {
      psWrite.setProperties("testProperties", new Properties());
      Assertions.assertEquals(PropertyType.PROPERTIES, getPsRead().getType("testProperties"));
    }
  }

  @Test
  public void testGetTypeForString() {
    if (psWrite.supportsType(PropertyType.STRING)) {
      psWrite.setString("testString", "value7");
      Assertions.assertEquals(PropertyType.STRING, getPsRead().getType("testString"));
    }
  }

  @Test
  public void testGetTypeForText() {
    if (psWrite.supportsType(PropertyType.TEXT)) {
      psWrite.setText("testText", TEXT_VALUE);
      Assertions.assertEquals(PropertyType.TEXT, getPsRead().getType("testText"));
    }
  }

  @Test
  public void testGetTypeForXml() throws ParserConfigurationException {
    if (psWrite.supportsType(PropertyType.XML)) {
      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder builder = factory.newDocumentBuilder();

      Document doc = builder.newDocument();
      doc.appendChild(doc.createElement("root"));
      psWrite.setXML("testXml", doc);
      Assertions.assertEquals(doc.getFirstChild().getNodeName(),
          getPsRead().getXML("testXml").getFirstChild().getNodeName());
    }
  }

  @Test
  public void testRemoveAllKeys() {
    psWrite.setString("test1", "value1");
    psWrite.setString("test2", "value2");
    Assertions.assertEquals(2, getPsRead().getKeys().size());

    try {
      psWrite.remove();
      Assertions.assertEquals(0, getPsRead().getKeys().size());
    } catch (PropertyException e) {
      // this is ok too for read only PropertyTypes
    }
  }

  @Test
  public void testRemoveSingleKey() {
    psWrite.setString("test1", "value1");
    psWrite.setString("test2", "value2");

    try {
      psWrite.remove("test1");
      Assertions.assertEquals(1, getPsRead().getKeys().size());
    } catch (PropertyException e) {
      // this is ok too for read only PropertyTypes
    }
  }

  @Test
  public void testSetAsActualTypeGetAsActualTypeForBoolean() {
    if (psWrite.supportsType(PropertyType.BOOLEAN)) {
      psWrite.setAsActualType("testBoolean", Boolean.valueOf(true));
      Assertions.assertEquals(Boolean.valueOf(true), getPsRead().getAsActualType("testBoolean"));
    }
  }

  @Test
  public void testSetAsActualTypeGetAsActualTypeForData() {
    if (psWrite.supportsType(PropertyType.DATA)) {
      psWrite.setAsActualType("testData", "value1".getBytes());
      Assertions.assertEquals(new String("value1".getBytes()),
          new String((byte[]) getPsRead().getAsActualType("testData")));
    }
  }

  @Test
  public void testSetAsActualTypeGetAsActualTypeForDate() {
    if (psWrite.supportsType(PropertyType.DATE)) {
      DateFormat df = DateFormat.getInstance();
      Date now = new Date();
      psWrite.setAsActualType("testDate", now);
      Assertions.assertEquals(df.format(now), df.format(getPsRead().getAsActualType("testDate")));
    }
  }

  @Test
  public void testSetAsActualTypeGetAsActualTypeForDouble() {
    if (psWrite.supportsType(PropertyType.DOUBLE)) {
      psWrite.setAsActualType("testDouble", Double.valueOf(10.234));
      Assertions.assertEquals(Double.valueOf(10.234), (Double) getPsRead().getAsActualType("testDouble"));
    }
  }

  @Test
  public void testSetAsActualTypeGetAsActualTypeForInt() {
    if (psWrite.supportsType(PropertyType.INT)) {
      psWrite.setAsActualType("testInt", Integer.valueOf(7));
      Assertions.assertEquals(Integer.valueOf(7), getPsRead().getAsActualType("testInt"));
    }
  }

  @Test
  public void testSetAsActualTypeGetAsActualTypeForLong() {
    if (psWrite.supportsType(PropertyType.LONG)) {
      psWrite.setAsActualType("testLong", Long.valueOf(70000));
      Assertions.assertEquals(Long.valueOf(70000), getPsRead().getAsActualType("testLong"));
    }
  }

  @Test
  public void testSetAsActualTypeGetAsActualTypeForObject() {
    if (psWrite.supportsType(PropertyType.OBJECT)) {
      TestObject testObject = new TestObject(2);
      psWrite.setAsActualType("testObject", testObject);
      Assertions.assertEquals(testObject, (TestObject) getPsRead().getAsActualType("testObject"));
    }
  }

  @Test
  public void testSetAsActualTypeGetAsActualTypeForProperties() {
    if (psWrite.supportsType(PropertyType.PROPERTIES)) {
      Properties props = new Properties();
      props.setProperty("prop1", "value1");
      psWrite.setAsActualType("testProperties", props);
      Assertions.assertEquals(props, getPsRead().getAsActualType("testProperties"));
    }
  }

  @Test
  public void testSetAsActualTypeGetAsActualTypeForString() {
    if (psWrite.supportsType(PropertyType.STRING)) {
      psWrite.setAsActualType("testString", "value1");
      Assertions.assertEquals("value1", getPsRead().getAsActualType("testString"));
    }
  }

  @Test
  public void testSetAsActualTypeGetAsActualTypeForText() {
    if (psWrite.supportsType(PropertyType.TEXT)) {
      psWrite.setAsActualType("testText", TEXT_VALUE);
      Assertions.assertEquals(TEXT_VALUE, getPsRead().getAsActualType("testText"));
    }
  }

  @Test
  public void testSetAsActualTypeGetAsActualTypeForXml() throws ParserConfigurationException {
    if (psWrite.supportsType(PropertyType.XML)) {
      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder builder = factory.newDocumentBuilder();

      Document doc = builder.newDocument();

      doc.appendChild(doc.createElement("root"));
      psWrite.setAsActualType("testXml", doc);
      Assertions.assertEquals(doc.getFirstChild().getNodeName(),
          getPsRead().getXML("testXml").getFirstChild().getNodeName());
    }
  }

  @Test
  public void testSetBooleanGetBoolean() {
    if (psWrite.supportsType(PropertyType.BOOLEAN)) {
      psWrite.setBoolean("testBoolean", true);
      Assertions.assertTrue(getPsRead().getBoolean("testBoolean"));
      psWrite.setBoolean("testBoolean", false);
      Assertions.assertFalse(getPsRead().getBoolean("testBoolean"));
    }
  }

  @Test
  public void testSetDataGetData() {
    if (psWrite.supportsType(PropertyType.DATA)) {
      psWrite.setData("testData", "value1".getBytes());
      Assertions.assertEquals(new String("value1".getBytes()), new String(getPsRead().getData("testData")));
      psWrite.setData("testData", "value2".getBytes());
      Assertions.assertEquals(new String("value2".getBytes()), new String(getPsRead().getData("testData")));
    }
  }

  @Test
  public void testSetDateGetDate() {
    if (psWrite.supportsType(PropertyType.DATE)) {
      DateFormat df = DateFormat.getInstance();
      Date now = new Date();
      psWrite.setDate("testDate", now);
      Assertions.assertEquals(df.format(now), df.format(getPsRead().getDate("testDate")));
      psWrite.setDate("testDate", new Date());
      Assertions.assertEquals(df.format(now), df.format(getPsRead().getDate("testDate")));
    }
  }

  @Test
  public void testSetDoubleGetDouble() {
    if (psWrite.supportsType(PropertyType.DOUBLE)) {
      psWrite.setDouble("testDouble", 1D);
      Assertions.assertEquals(1D, getPsRead().getDouble("testDouble"), 0);
      psWrite.setDouble("testDouble", 100000D);
      Assertions.assertEquals(100000D, getPsRead().getDouble("testDouble"), 0);
    }
  }

  @Test
  public void testSetIntGetInt() {
    if (psWrite.supportsType(PropertyType.INT)) {
      psWrite.setInt("testInt", 7);
      Assertions.assertEquals(7, getPsRead().getInt("testInt"));
      psWrite.setInt("testInt", 11);
      Assertions.assertEquals(11, getPsRead().getInt("testInt"));
    }
  }

  @Test
  public void testSetLongGetLong() {
    if (psWrite.supportsType(PropertyType.LONG)) {
      psWrite.setLong("testLong", 1L);
      Assertions.assertEquals(1L, getPsRead().getLong("testLong"));
      psWrite.setLong("testLong", 100000);
      Assertions.assertEquals(100000, getPsRead().getLong("testLong"));
    }
  }

  @Test
  public void testSetObjectGetObject() {
    if (psWrite.supportsType(PropertyType.OBJECT)) {
      TestObject testObject = new TestObject(1);
      psWrite.setObject("testObject", testObject);
      Assertions.assertEquals(testObject, getPsRead().getObject("testObject"));
    }
  }

  @Test
  public void testSetPropertiesGetProperties() {
    if (psWrite.supportsType(PropertyType.PROPERTIES)) {
      Properties props = new Properties();
      props.setProperty("prop1", "propValue1");
      psWrite.setProperties("testProperties", props);
      Assertions.assertEquals(props, getPsRead().getProperties("testProperties"));
      props.setProperty("prop2", "propValue2");
      psWrite.setProperties("testProperties", props);
      Assertions.assertEquals(props, getPsRead().getProperties("testProperties"));
    }
  }

  @Test
  public void testSetStringGetStringLengthGreaterThan255() {
    try {
      psWrite.setString("testString", TEXT_VALUE);
      Assertions.fail("Should not be able to setString() with a String longer than 255 chars.");
    } catch (IllegalPropertyException e) {
      // expected
    }
  }

  @Test
  public void testSetStringGetStringLengthLessThan255() {
    psWrite.setString("testString", "value1");
    Assertions.assertTrue("value1".equals(getPsRead().getString("testString")));
    psWrite.setString("testString", "value2");
    Assertions.assertTrue("value2".equals(getPsRead().getString("testString")));
  }

  @Test
  public void testSetTextGetText() {
    if (psWrite.supportsType(PropertyType.TEXT)) {
      psWrite.setText("testText", TEXT_VALUE);
      Assertions.assertEquals(TEXT_VALUE, getPsRead().getText("testText"));
      psWrite.setText("testText", TEXT_VALUE + "A");
      Assertions.assertEquals(TEXT_VALUE + "A", getPsRead().getText("testText"));
    }
  }

  @Test
  public void testSetXmlGetXml() throws ParserConfigurationException {
    if (psWrite.supportsType(PropertyType.XML)) {
      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder builder = factory.newDocumentBuilder();

      Document doc = builder.newDocument();
      doc.appendChild(doc.createElement("root"));
      psWrite.setXML("testXml", doc);
      Assertions.assertEquals(doc.getFirstChild().getNodeName(),
          getPsRead().getXML("testXml").getFirstChild().getNodeName());
    }
  }
}
