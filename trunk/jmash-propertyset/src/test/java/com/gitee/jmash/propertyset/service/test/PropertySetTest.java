
package com.gitee.jmash.propertyset.service.test;

import com.gitee.jmash.propertyset.PropSetFactory;
import com.gitee.jmash.propertyset.service.PropertySetRead;
import jakarta.enterprise.inject.se.SeContainer;
import jakarta.enterprise.inject.se.SeContainerInitializer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;

/**
 * Spring jpa 实现用户部分的测试
 * 
 * @author cgd
 * @version V1.0
 */
public class PropertySetTest extends AbstractPropertySetTest {

  static SeContainer container;

  protected static PropertySetRead psRead;

  @BeforeAll
  public static void setUp() throws Exception {
    container = SeContainerInitializer.newInstance().initialize();
    psWrite = PropSetFactory.getPropSetWrite("testSpring", "2");
  }

  @Override
  public PropertySetRead getPsRead() {
    //Read 在同一个事务里,Hibernate 会使用一级缓存,每次查询数据会保持一致.
    if (psRead!=null) {
      try {
        psRead.close();
      } catch (Exception e) {
         e.printStackTrace();
      }
    }
    psRead = PropSetFactory.getPropSetRead("testSpring", "2");
    return psRead;
  }


  @AfterEach
  public void eachEnd() {
    psWrite.remove();
  }

  @AfterAll
  public static void tearDown() throws Exception {
    psRead.close();
    psWrite.close();
    container.close();
  }

}
