# Protocol Documentation
<a name="top"></a>

## Table of Contents

- [jmash/region/protobuf/os_dict_region_message.proto](#jmash_region_protobuf_os_dict_region_message-proto)
    - [DictRegionCreateReq](#jmash-region-DictRegionCreateReq)
    - [DictRegionEnableKey](#jmash-region-DictRegionEnableKey)
    - [DictRegionExportReq](#jmash-region-DictRegionExportReq)
    - [DictRegionImportReq](#jmash-region-DictRegionImportReq)
    - [DictRegionKey](#jmash-region-DictRegionKey)
    - [DictRegionKeyList](#jmash-region-DictRegionKeyList)
    - [DictRegionList](#jmash-region-DictRegionList)
    - [DictRegionModel](#jmash-region-DictRegionModel)
    - [DictRegionModelTotal](#jmash-region-DictRegionModelTotal)
    - [DictRegionMoveKey](#jmash-region-DictRegionMoveKey)
    - [DictRegionPage](#jmash-region-DictRegionPage)
    - [DictRegionReq](#jmash-region-DictRegionReq)
    - [DictRegionUpdateReq](#jmash-region-DictRegionUpdateReq)
  
    - [RegionCategory](#jmash-region-RegionCategory)
    - [RegionType](#jmash-region-RegionType)
  
- [Scalar Value Types](#scalar-value-types)



<a name="jmash_region_protobuf_os_dict_region_message-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## jmash/region/protobuf/os_dict_region_message.proto



<a name="jmash-region-DictRegionCreateReq"></a>

### DictRegionCreateReq
地区信息新增实体 .


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| request_id | [string](#string) |  | 用于检测重复请求的唯一字符串ID. |
| validate_only | [bool](#bool) |  | 如果为true，则表示给定的请求仅需要被检验，而不是被执行. |
| parent_id | [string](#string) |  | 父ID. |
| region_code | [string](#string) |  | 地区编码. |
| region_name | [string](#string) |  | 地区名称. |
| region_pinyin | [string](#string) |  | 地区拼音. |
| region_type | [RegionType](#jmash-region-RegionType) |  | 地区类型. |
| region_category | [RegionCategory](#jmash-region-RegionCategory) |  | 省分类. |
| description_ | [string](#string) |  | 描述. |






<a name="jmash-region-DictRegionEnableKey"></a>

### DictRegionEnableKey
启用/禁用.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| region_id | [string](#string) |  | 地区ID. |
| enable | [bool](#bool) |  | 是否启用/禁用. |






<a name="jmash-region-DictRegionExportReq"></a>

### DictRegionExportReq
导出请求.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| title | [string](#string) |  | 标题. |
| table_heads | [jmash.protobuf.TableHead](#jmash-protobuf-TableHead) | repeated | 字段列表. |
| file_name | [string](#string) |  | 显示文件名. |
| req | [DictRegionReq](#jmash-region-DictRegionReq) |  | 筛选条件. |






<a name="jmash-region-DictRegionImportReq"></a>

### DictRegionImportReq
导入请求.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| request_id | [string](#string) |  | 用于检测重复请求的唯一字符串ID. |
| file_names | [string](#string) |  | 文件名. |
| add_flag | [bool](#bool) |  | 是否新增标识. |
| update_mask | [google.protobuf.FieldMask](#google-protobuf-FieldMask) |  | 更新字段掩码. |






<a name="jmash-region-DictRegionKey"></a>

### DictRegionKey
地区信息主键 .


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| region_id | [string](#string) |  | 地区ID. |






<a name="jmash-region-DictRegionKeyList"></a>

### DictRegionKeyList
地区信息List.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| region_id | [string](#string) | repeated | 地区ID. |






<a name="jmash-region-DictRegionList"></a>

### DictRegionList
地区信息列表.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [DictRegionModel](#jmash-region-DictRegionModel) | repeated | 当前页内容. |






<a name="jmash-region-DictRegionModel"></a>

### DictRegionModel
地区信息实体 .


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| region_id | [string](#string) |  | 地区ID. |
| parent_id | [string](#string) |  | 父ID. |
| region_code | [string](#string) |  | 地区编码. |
| region_name | [string](#string) |  | 地区名称. |
| region_pinyin | [string](#string) |  | 地区拼音. |
| region_type | [RegionType](#jmash-region-RegionType) |  | 地区类型. |
| region_category | [RegionCategory](#jmash-region-RegionCategory) |  | 省分类. |
| depth_ | [int32](#int32) |  | 深度. |
| order_by | [int32](#int32) |  | 排序. |
| enable_ | [bool](#bool) |  | 是否启用. |
| description_ | [string](#string) |  | 描述. |
| has_children | [bool](#bool) |  | 是否包含子节点. |
| children | [DictRegionModel](#jmash-region-DictRegionModel) | repeated | 子节点 |






<a name="jmash-region-DictRegionModelTotal"></a>

### DictRegionModelTotal
合计 .


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| total_size | [int32](#int32) |  | 总记录数. |






<a name="jmash-region-DictRegionMoveKey"></a>

### DictRegionMoveKey
上移/下移.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| region_id | [string](#string) |  | 地区ID. |
| up | [bool](#bool) |  | 是否上移/下移. |






<a name="jmash-region-DictRegionPage"></a>

### DictRegionPage
地区信息分页 .


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [DictRegionModel](#jmash-region-DictRegionModel) | repeated | 当前页内容. |
| cur_page | [int32](#int32) |  | 当前页码. |
| page_size | [int32](#int32) |  | 页尺寸. |
| total_size | [int32](#int32) |  | 总记录数. |
| page_count | [int32](#int32) |  | 总页数 |
| sub_total_dto | [DictRegionModelTotal](#jmash-region-DictRegionModelTotal) |  | 本页小计. |
| total_dto | [DictRegionModelTotal](#jmash-region-DictRegionModelTotal) |  | 合计. |






<a name="jmash-region-DictRegionReq"></a>

### DictRegionReq
地区信息查询.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| cur_page | [int32](#int32) |  | 当前页码. |
| page_size | [int32](#int32) |  | 页尺寸. |
| order_name | [string](#string) |  | 排序名称. |
| order_asc | [bool](#bool) |  | 是否升序排序. |
| like_region_name | [string](#string) |  | 地区名称模糊. |
| region_code | [string](#string) |  | 地区编码. |
| has_enable | [bool](#bool) |  | 是否包含状态. |
| enable | [bool](#bool) |  | 是否启用. |
| exclude_id | [string](#string) |  | 隐藏ID. |
| has_parent | [bool](#bool) |  | 是否包含上级ID. |
| parent_id | [string](#string) |  | 上级ID. |
| region_id | [string](#string) |  | 获取地区ID及其孩子. |






<a name="jmash-region-DictRegionUpdateReq"></a>

### DictRegionUpdateReq
地区信息修改实体 .


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  |  |
| request_id | [string](#string) |  | 用于检测重复请求的唯一字符串ID. |
| validate_only | [bool](#bool) |  | 如果为true，则表示给定的请求仅需要被检验，而不是被执行. |
| update_mask | [google.protobuf.FieldMask](#google-protobuf-FieldMask) |  | 更新字段掩码. |
| region_id | [string](#string) |  | 地区ID. |
| parent_id | [string](#string) |  | 父ID. |
| region_code | [string](#string) |  | 地区编码. |
| region_name | [string](#string) |  | 地区名称. |
| region_pinyin | [string](#string) |  | 地区拼音. |
| region_type | [RegionType](#jmash-region-RegionType) |  | 地区类型. |
| region_category | [RegionCategory](#jmash-region-RegionCategory) |  | 省分类. |
| description_ | [string](#string) |  | 描述. |





 


<a name="jmash-region-RegionCategory"></a>

### RegionCategory
地区分类枚举.

| Name | Number | Description |
| ---- | ------ | ----------- |
| north | 0 | 华北. |
| east_north | 1 | 东北. |
| east | 2 | 华东. |
| center | 3 | 华中. |
| south | 4 | 华南. |
| west_south | 5 | 西南. |
| west_north | 6 | 西北. |
| over_seas | 7 | 港澳台. |



<a name="jmash-region-RegionType"></a>

### RegionType
地区类型枚举.

| Name | Number | Description |
| ---- | ------ | ----------- |
| country | 0 | 国家. |
| province | 1 | 省. |
| city | 2 | 市. |
| county | 3 | 区/县. |
| town | 4 | 乡镇. |
| village | 5 | 村. |


 

 

 



## Scalar Value Types

| .proto Type | Notes | C++ | Java | Python | Go | C# | PHP | Ruby |
| ----------- | ----- | --- | ---- | ------ | -- | -- | --- | ---- |
| <a name="double" /> double |  | double | double | float | float64 | double | float | Float |
| <a name="float" /> float |  | float | float | float | float32 | float | float | Float |
| <a name="int32" /> int32 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint32 instead. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="int64" /> int64 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint64 instead. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="uint32" /> uint32 | Uses variable-length encoding. | uint32 | int | int/long | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="uint64" /> uint64 | Uses variable-length encoding. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum or Fixnum (as required) |
| <a name="sint32" /> sint32 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int32s. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sint64" /> sint64 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int64s. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="fixed32" /> fixed32 | Always four bytes. More efficient than uint32 if values are often greater than 2^28. | uint32 | int | int | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="fixed64" /> fixed64 | Always eight bytes. More efficient than uint64 if values are often greater than 2^56. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum |
| <a name="sfixed32" /> sfixed32 | Always four bytes. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sfixed64" /> sfixed64 | Always eight bytes. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="bool" /> bool |  | bool | boolean | boolean | bool | bool | boolean | TrueClass/FalseClass |
| <a name="string" /> string | A string must always contain UTF-8 encoded or 7-bit ASCII text. | string | String | str/unicode | string | string | string | String (UTF-8) |
| <a name="bytes" /> bytes | May contain any arbitrary sequence of bytes. | string | ByteString | str | []byte | ByteString | string | String (ASCII-8BIT) |

