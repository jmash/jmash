
本地测试(主动开启反射或测试环境)
grpcurl -plaintext 127.0.0.1:50051 grpc.health.v1.Health/Check

# 无TLS测试(正式环境仅Captcha模块开启反射).
kubectl exec -it grpcurl -n jmash -- sh
grpcurl -plaintext jmash-region-service:50051 grpc.health.v1.Health/Check

正式环境测试(仅Captcha模块开启反射)
grpcurl dev.jmash.crenjoy.com:443 grpc.health.v1.Health/Check
