
package com.gitee.jmash.region.client;

import com.gitee.jmash.common.grpc.client.GrpcChannel;
import jmash.region.RegionGrpc;

/**
 * Region Client .
 */
public class RegionClient {

  /**
   * RegionBlockingStub.
   */
  public static RegionGrpc.RegionBlockingStub getRegionBlockingStub() {
    return RegionGrpc.newBlockingStub(RegionClientConfig.getManagedChannel())
        .withCallCredentials(GrpcChannel.getCallCredentials());
  }

  /**
   * RegionFutureStub.
   */
  public static RegionGrpc.RegionFutureStub getRegionFutureStub() {
    return RegionGrpc.newFutureStub(RegionClientConfig.getManagedChannel())
        .withCallCredentials(GrpcChannel.getCallCredentials());
  }

  /**
   * RegionStub.
   */
  public static RegionGrpc.RegionStub getRegionStub() {
    return RegionGrpc.newStub(RegionClientConfig.getManagedChannel())
        .withCallCredentials(GrpcChannel.getCallCredentials());
  }

}
