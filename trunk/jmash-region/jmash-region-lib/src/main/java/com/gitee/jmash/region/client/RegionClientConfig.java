
package com.gitee.jmash.region.client;

import com.gitee.jmash.common.grpc.client.GrpcChannel;
import io.grpc.ManagedChannel;
import jakarta.enterprise.context.ApplicationScoped;

/**
 * region Client Config .
 */
public class RegionClientConfig {

  protected static ManagedChannel channel = null;

  public static synchronized ManagedChannel getManagedChannel() {
    if (null != channel && !channel.isShutdown() && !channel.isTerminated()) {
      return channel;
    }
    // k8s环境获取后端服务,本地环境获取测试服务.
    channel = GrpcChannel.getServiceChannel("jmash-region-service.jmash.svc.cluster.local");
    return channel;
  }

}
