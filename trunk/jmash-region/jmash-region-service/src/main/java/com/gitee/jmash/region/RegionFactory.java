package com.gitee.jmash.region;

import com.gitee.jmash.region.service.DictRegionRead;
import com.gitee.jmash.region.service.DictRegionWrite;
import jakarta.enterprise.inject.spi.CDI;

/**
 * 模块服务工厂类.
 *
 * @author CGD
 */
public class RegionFactory {


  public static DictRegionRead getDictRegionRead(String tenant) {
    return CDI.current().select(DictRegionRead.class).get().setTenant(tenant);
  }

  public static DictRegionWrite getDictRegionWrite(String tenant) {
    return CDI.current().select(DictRegionWrite.class).get().setTenant(tenant);
  }

  /**
   * CDI回收实例.
   */
  public static void destroy(Object instance) {
    if (null != instance) {
      CDI.current().destroy(instance);
    }
  }
}
