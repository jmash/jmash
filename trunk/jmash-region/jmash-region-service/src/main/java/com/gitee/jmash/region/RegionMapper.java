
package com.gitee.jmash.region;

import com.crenjoy.proto.mapper.BeanMapper;
import com.crenjoy.proto.mapper.ProtoMapper;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * Region Mapper.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Mapper
public interface RegionMapper extends BeanMapper, ProtoMapper {

  RegionMapper INSTANCE = Mappers.getMapper(RegionMapper.class);

}
