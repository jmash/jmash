
package com.gitee.jmash.region.cdi;

import com.gitee.jmash.core.grpc.DefaultGrpcServer;
import com.gitee.jmash.core.orm.module.DbBuild;
import com.gitee.jmash.rbac.client.shiro.JmashClientShiroConfig;
import jakarta.enterprise.inject.se.SeContainer;
import jakarta.enterprise.inject.se.SeContainerInitializer;

/**
 * 模块启动类.
 */
public class RegionApp {

  /**
   * 程序入口.
   */
  public static void main(String[] args) throws Exception {
    try (SeContainer container = SeContainerInitializer.newInstance().initialize()) {
      // 构建数据库.
      DbBuild dbBuild = container.select(DbBuild.class).get();
      dbBuild.build();
      // Shiro Client 初始化.
      JmashClientShiroConfig.config();
      final DefaultGrpcServer server = container.select(DefaultGrpcServer.class).get();
      server.start(false);
      server.blockUntilShutdown();
    }
  }

}
