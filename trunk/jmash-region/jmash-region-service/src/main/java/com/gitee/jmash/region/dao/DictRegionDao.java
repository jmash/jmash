
package com.gitee.jmash.region.dao;

import com.crenjoy.proto.utils.TypeUtil;
import com.gitee.jmash.common.utils.UUIDUtil;
import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.orm.jpa.BaseDao;
import com.gitee.jmash.core.orm.jpa.SqlBuilder;
import com.gitee.jmash.core.orm.jpa.TenantEntityManager;
import com.gitee.jmash.core.orm.jpa.TreeBaseDao;
import com.gitee.jmash.region.entity.DictRegionEntity;
import com.gitee.jmash.region.model.DictRegionTotal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import jmash.region.protobuf.DictRegionReq;
import org.apache.commons.lang3.StringUtils;

/**
 * DictRegion实体的Dao层（使用JPA实现）.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public class DictRegionDao extends TreeBaseDao<DictRegionEntity, UUID> {

  public DictRegionDao() {
    super();
  }

  public DictRegionDao(TenantEntityManager tem) {
    super(tem);
  }


  /**
   * 查询记录数.
   */
  public Integer findCount(DictRegionReq req) {
    SqlBuilder sqlBuilder = createSql(req);
    String query = sqlBuilder.getQuerySql("select count(s) ");
    Object obj = this.findSingleResultByParams(query, sqlBuilder.getParams());
    return (obj == null) ? 0 : Integer.valueOf(obj.toString());
  }

  /**
   * 综合查询.
   */
  public List<DictRegionEntity> findListByReq(DictRegionReq req) {
    SqlBuilder sqlBuilder = createSql(req);
    String query = sqlBuilder.getQuerySql("select s ");
    return this.findListByParams(query, sqlBuilder.getParams());
  }

  /**
   * 综合查询Page.
   */
  public DtoPage<DictRegionEntity, DictRegionTotal> findPageByReq(DictRegionReq req) {
    SqlBuilder sqlBuilder = createSql(req);
    String query = sqlBuilder.getQuerySql("select s ");
    String totalQuery = sqlBuilder.getTotalSql("select count(s) as totalSize ");
    return this.findDtoPageByParams(req.getCurPage(), req.getPageSize(), query, totalQuery,
        DictRegionTotal.class, sqlBuilder.getParams());
  }

  /**
   * Create SQL By Req .
   */
  public SqlBuilder createSql(DictRegionReq req) {
    StringBuilder sql = new StringBuilder(" from DictRegionEntity s where 1=1  ");
    Map<String, Object> params = new HashMap<String, Object>();

    //地区名称模糊查询.
    if (StringUtils.isNotBlank(req.getLikeRegionName())) {
      sql.append(" and s.regionName like :likeRegionName ");
      params.put("likeRegionName", "%" + req.getLikeRegionName() + "%");
    }
    //地区编码查询.
    if (StringUtils.isNotBlank(req.getRegionCode())) {
      sql.append(" and s.regionCode = :regionCode ");
      params.put("regionCode", req.getRegionCode());
    }
    //状态查询.
    if (req.getHasEnable()) {
      sql.append(" and s.enable = :enable ");
      params.put("enable", req.getEnable());
    }
    //隐藏ID查询.
    if (StringUtils.isNotBlank(req.getExcludeId())) {
      sql.append(" and s.regionId != :excludeId ");
      params.put("excludeId", UUIDUtil.fromString(req.getExcludeId()));
    }
    //上级ID查询.
    if (StringUtils.isNotBlank(req.getParentId())) {
      sql.append(" and s.parentId = :parentId ");
      params.put("parentId", UUIDUtil.fromString(req.getParentId()));
    } else if (!req.getHasParent()) {
      //是否包含parent条件.
      sql.append(" and s.parentId = :parentId ");
      params.put("parentId", UUIDUtil.emptyUUID());
    }
    String orderSql = " order by s.depth asc, s.orderBy asc ";
    if (StringUtils.isNotBlank(req.getOrderName())) {
      orderSql = String.format(" order by %s %s ", req.getOrderName(),
          req.getOrderAsc() ? " asc " : " desc ");
    }

    return SqlBuilder.build().setSql(sql).setParams(params).setOrderSql(orderSql);
  }

  /**
   * 校验是否已存在相同的地区信息.
   *
   * @param regionId 地区ID.
   * @param regionName 地区名称.
   * @param regionCode 地区编码.
   * @return true 已存在，false 不存在.
   */
  public boolean validateRegion(UUID regionId, String regionName, String regionCode) {
    StringBuilder sql = new StringBuilder(" select count(s) from DictRegionEntity s where 1=1  ");
    Map<String, Object> params = new HashMap<String, Object>();

    //地区ID.
    if (!TypeUtil.isEmpty(regionId)) {
      sql.append(" and s.regionId != :regionId ");
      params.put("regionId", regionId);
    }
    //地区名称.
    if (StringUtils.isNotBlank(regionName)) {
      sql.append(" and s.regionName = :regionName ");
      params.put("regionName", regionName);
    }
    //地区编码.
    if (StringUtils.isNotBlank(regionCode)) {
      sql.append(" and s.regionCode = :regionCode ");
      params.put("regionCode", regionCode);
    }
    int count = Integer.parseInt(this.findSingleResultByParams(sql.toString(), params).toString());
    return count > 0;
  }

  /**
   * 根据地区编码查询.
   */
  public DictRegionEntity findByRegionCode(String regionCode) {
    String sql = "select s from DictRegionEntity s where 1=1 and regionCode = ?1 ";
    return this.findSingle(sql, regionCode);
  }


}
