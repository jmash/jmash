
package com.gitee.jmash.region.entity;


import com.gitee.jmash.core.orm.jpa.entity.TreeEntity;
import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;
import jmash.region.protobuf.RegionCategory;
import jmash.region.protobuf.RegionType;


/**
 * 行政区划 地区信息表 .
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Entity
@Table(name = "os_dict_region")

public class DictRegionEntity implements Serializable, TreeEntity<UUID> {

  private static final long serialVersionUID = 1L;

  /**
   * 地区ID.
   */
  private UUID regionId = UUID.randomUUID();
  /**
   * 父ID.
   */
  private UUID parentId;
  /**
   * 地区编码.
   */
  private String regionCode;
  /**
   * 地区名称.
   */
  private String regionName;
  /**
   * 地区拼音.
   */
  private String regionPinyin;
  /**
   * 地区类型.
   */
  private RegionType regionType;
  /**
   * 省分类.
   */
  private RegionCategory regionCategory;
  /**
   * 深度.
   */
  private Integer depth;
  /**
   * 排序.
   */
  private Integer orderBy;
  /**
   * 是否启用.
   */
  private Boolean enable = true;
  /**
   * 描述.
   */
  private String description;

  /**
   * 默认构造函数.
   */
  public DictRegionEntity() {
    super();
  }

  /**
   * 实体主键.
   */
  @Transient
  @JsonbTransient
  public UUID getEntityPk() {
    return this.regionId;
  }

  /**
   * 实体主键.
   */
  public void setEntityPk(UUID pk) {
    this.regionId = pk;
  }

  /**
   * 地区ID(RegionId).
   */
  @Id
  @Column(name = "region_id", nullable = false, columnDefinition = "BINARY(16)")
  public UUID getRegionId() {
    return regionId;
  }

  /**
   * 地区ID(RegionId).
   */
  public void setRegionId(UUID regionId) {
    this.regionId = regionId;
  }

  /**
   * 父ID(ParentId).
   */
  @Column(name = "parent_id", columnDefinition = "BINARY(16)")
  public UUID getParentId() {
    return parentId;
  }

  /**
   * 父ID(ParentId).
   */
  public void setParentId(UUID parentId) {
    this.parentId = parentId;
  }

  /**
   * 地区编码(RegionCode).
   */

  @Column(name = "region_code", nullable = false)
  public String getRegionCode() {
    return regionCode;
  }

  /**
   * 地区编码(RegionCode).
   */
  public void setRegionCode(String regionCode) {
    this.regionCode = regionCode;
  }

  /**
   * 地区名称(RegionName).
   */

  @Column(name = "region_name", nullable = false)
  public String getRegionName() {
    return regionName;
  }

  /**
   * 地区名称(RegionName).
   */
  public void setRegionName(String regionName) {
    this.regionName = regionName;
  }

  /**
   * 地区拼音(RegionPinyin).
   */
  @Column(name = "region_pinyin")
  public String getRegionPinyin() {
    return regionPinyin;
  }

  /**
   * 地区拼音(RegionPinyin).
   */
  public void setRegionPinyin(String regionPinyin) {
    this.regionPinyin = regionPinyin;
  }

  /**
   * 地区类型(RegionType).
   */
  @Column(name = "region_type")
  @Enumerated(EnumType.STRING)
  public RegionType getRegionType() {
    return regionType;
  }

  /**
   * 地区类型(RegionType).
   */
  public void setRegionType(RegionType regionType) {
    this.regionType = regionType;
  }

  /**
   * 省分类(RegionCategory).
   */
  @Column(name = "region_category")
  @Enumerated(EnumType.STRING)
  public RegionCategory getRegionCategory() {
    return regionCategory;
  }

  /**
   * 省分类(RegionCategory).
   */
  public void setRegionCategory(RegionCategory regionCategory) {
    this.regionCategory = regionCategory;
  }

  /**
   * 深度(Depth).
   */

  @Column(name = "depth_", nullable = false)
  public Integer getDepth() {
    return depth;
  }

  /**
   * 深度(Depth).
   */
  public void setDepth(Integer depth) {
    this.depth = depth;
  }

  /**
   * 排序(OrderBy).
   */
  @Column(name = "order_by")
  public Integer getOrderBy() {
    return orderBy;
  }

  /**
   * 排序(OrderBy).
   */
  public void setOrderBy(Integer orderBy) {
    this.orderBy = orderBy;
  }

  /**
   * 是否启用(Enable).
   */
  @Column(name = "enable_")
  public Boolean getEnable() {
    return enable;
  }

  /**
   * 是否启用(Enable).
   */
  public void setEnable(Boolean enable) {
    this.enable = enable;
  }

  /**
   * 描述(Description).
   */
  @Column(name = "description_")
  public String getDescription() {
    return description;
  }

  /**
   * 描述(Description).
   */
  public void setDescription(String description) {
    this.description = description;
  }

  @Override
  public int hashCode() {
    return Objects.hash(regionId);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }

    DictRegionEntity other = (DictRegionEntity) obj;

    return Objects.equals(regionId, other.regionId);
  }


}
