package com.gitee.jmash.region.excel;

import com.gitee.jmash.common.excel.ExcelExport;
import com.gitee.jmash.common.excel.write.HeaderExport;
import com.gitee.jmash.core.lib.ProtoEnumUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import jmash.protobuf.Entry;
import jmash.protobuf.TableHead;
import jmash.region.protobuf.RegionCategory;
import jmash.region.protobuf.RegionType;

/**
 * 地区信息导出.
 */
public class DictRegionHeaderExport {

  /**
   * 默认表头.
   */
  public static final String TITLE = "地区信息表";

  /**
   * 规则Header.
   *
   * @return 表头.
   */
  public static List<HeaderExport> getHeaderExports() {
    List<HeaderExport> list = new ArrayList<HeaderExport>();
    list.add(HeaderExport.field("地区ID", "regionId", 4 * 1000));
    list.add(HeaderExport.field("父ID", "parentId", 4 * 1000));
    list.add(HeaderExport.field("地区编码", "regionCode", 4 * 1000));
    list.add(HeaderExport.field("地区名称", "regionName", 4 * 1000));
    list.add(HeaderExport.field("地区拼音", "regionPinyin", 4 * 1000));
    list.add(HeaderExport.dict("地区类型", "regionType", 4 * 1000, getRegionTypeMap()));
    list.add(HeaderExport.dict("地区分类", "regionCategory", 4 * 1000, getRegionCategoryMap()));
    list.add(HeaderExport.dict("是否启用", "enable", 4 * 1000, getEnabledMap()));
    list.add(HeaderExport.field("描述", "description", 4 * 1000));
    return list;
  }

  /**
   * Map HeaderExport.
   *
   * @return Map.
   */
  public static Map<String, HeaderExport> getHeaderExportMap() {
    Map<String, HeaderExport> map = getHeaderExports().stream()
        .collect(Collectors.toMap(HeaderExport::getField, Function.identity()));
    return map;
  }

  /**
   * 增加表头.
   */
  public static void addHeaderExport(ExcelExport excelExport, List<TableHead> tableHeads) {
    if (tableHeads.isEmpty()) {
      for (HeaderExport header : getHeaderExports()) {
        excelExport.addHeader(header);
      }
    } else {
      Map<String, HeaderExport> map = getHeaderExportMap();
      for (TableHead tableHead : tableHeads) {
        if (map.containsKey(tableHead.getProp())) {
          excelExport.addHeader(map.get(tableHead.getProp()));
        }
      }
    }
  }

  /**
   * 地区类型Map.
   */
  public static Map<String, String> getRegionTypeMap() {
    return ProtoEnumUtil.getEnumNameMap(RegionType.class, 0);
  }

  /**
   * 地区分类Map.
   */
  public static Map<String, String> getRegionCategoryMap() {
    return ProtoEnumUtil.getEnumNameMap(RegionCategory.class, 0);
  }

  /**
   * 是否启用Map.
   */
  public static Map<String, String> getEnabledMap() {
    return getEnabled().stream().collect(Collectors.toMap(Entry::getKey, Entry::getValue));
  }

  /**
   * 是否启用.
   */
  public static List<Entry> getEnabled() {
    List<Entry> list = new ArrayList<>();
    list.add(Entry.newBuilder().setKey("true").setValue("启用").build());
    list.add(Entry.newBuilder().setKey("false").setValue("禁用").build());
    return list;
  }

}
