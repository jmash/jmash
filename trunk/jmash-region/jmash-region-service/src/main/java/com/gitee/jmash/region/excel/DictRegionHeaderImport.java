package com.gitee.jmash.region.excel;

import com.gitee.jmash.common.excel.read.CellLocalDateReader;
import com.gitee.jmash.common.excel.read.HeaderField;
import com.gitee.jmash.core.lib.ProtoEnumUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import jmash.protobuf.Entry;
import jmash.protobuf.Gender;
import jmash.rbac.protobuf.UserStatus;
import jmash.region.protobuf.RegionCategory;
import jmash.region.protobuf.RegionType;

/**
 * 地区信息导入.
 */
public class DictRegionHeaderImport {

  /**
   * 规则Header.
   */
  public static List<HeaderField> getHeaderImports() {
    List<HeaderField> list = new ArrayList<HeaderField>();
    list.add(HeaderField.field("地区ID", "regionId", true));
    list.add(HeaderField.field("父ID", "parentId", true));
    list.add(HeaderField.field("地区编码", "regionCode", true));
    list.add(HeaderField.field("地区名称", "regionName", true));
    list.add(HeaderField.field("地区拼音", "regionPinyin", true));
    list.add(HeaderField.dict("地区类型", "regionType", true, getRegionTypeMap()));
    list.add(HeaderField.dict("地区分类", "regionCategory", true, getRegionCategoryMap()));
    list.add(HeaderField.dict("是否启用", "enable", true, getEnabledCodeMap()));
    list.add(HeaderField.field("描述", "description", true));
    return list;
  }

  /**
   * 字典地区类型Map Name,Code..
   */
  public static Map<String, String> getRegionTypeMap() {
    return ProtoEnumUtil.getEnumCodeMap(RegionType.class, -1);
  }

  /**
   * 字典地区分类Map Name,Code..
   */
  public static Map<String, String> getRegionCategoryMap() {
    return ProtoEnumUtil.getEnumCodeMap(RegionCategory.class, -1);
  }

  /**
   * 启用禁用Map Name,Code.
   */
  public static Map<String, String> getEnabledCodeMap() {
    return DictRegionHeaderExport.getEnabled().stream()
        .collect(Collectors.toMap(Entry::getValue, Entry::getKey));
  }


}
