package com.gitee.jmash.region.excel;

import com.gitee.jmash.common.excel.write.HeaderExport;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 地区信息Sheet导出.
 */
public class DictRegionSheetExportDto implements Serializable {

  private static final long serialVersionUID = 1L;

  /**
   * sheet页名称.
   */
  private String sheetName;

  /**
   * Map字段别名.
   */
  private Map<String, String> fieldAndAlias;

  /**
   * 数据集.
   */
  private List<?> dataList;

  private Collection<HeaderExport> headerCollection;

  public DictRegionSheetExportDto() {
    super();
  }

  /**
   *构造一个导出数据传输对象（DTO），用于指定Excel工作表的名称和数据集.
   *
   * @param sheetName sheet页名称
   * @param dataList  数据集
   */
  public DictRegionSheetExportDto(String sheetName, List<?> dataList) {
    super();
    this.sheetName = sheetName;
    this.dataList = dataList;
    this.headerCollection = DictRegionHeaderExport.getHeaderExports();
  }

  public List<?> getDataList() {
    return dataList;
  }

  public void setDataList(List<?> dataList) {
    this.dataList = dataList;
  }

  public String getSheetName() {
    return sheetName;
  }

  public void setSheetName(String sheetName) {
    this.sheetName = sheetName;
  }

  public Map<String, String> getFieldAndAlias() {
    return fieldAndAlias;
  }

  public void setFieldAndAlias(Map<String, String> fieldAndAlias) {
    this.fieldAndAlias = fieldAndAlias;
  }


  public Collection<HeaderExport> getHeaderCollection() {
    return headerCollection;
  }

  public void setHeaderCollection(Collection<HeaderExport> headerCollection) {
    this.headerCollection = headerCollection;
  }
}
