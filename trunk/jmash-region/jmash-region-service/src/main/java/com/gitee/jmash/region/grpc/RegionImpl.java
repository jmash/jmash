
package com.gitee.jmash.region.grpc;


import com.gitee.jmash.common.utils.UUIDUtil;
import com.gitee.jmash.common.utils.VersionUtil;
import com.gitee.jmash.core.grpc.cdi.GrpcService;
import com.gitee.jmash.core.lib.ProtoEnumUtil;
import com.gitee.jmash.core.utils.FileServiceUtil;
import com.gitee.jmash.region.RegionFactory;
import com.gitee.jmash.region.entity.DictRegionEntity;
import com.gitee.jmash.region.mapper.DictRegionMapper;
import com.gitee.jmash.region.service.DictRegionRead;
import com.gitee.jmash.region.service.DictRegionWrite;
import com.google.api.HttpBody;
import com.google.protobuf.BoolValue;
import com.google.protobuf.Empty;
import com.google.protobuf.EnumValue;
import com.google.protobuf.Int32Value;
import com.google.protobuf.StringValue;
import io.grpc.Status;
import io.grpc.stub.StreamObserver;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import jmash.protobuf.CustomEnumValue;
import jmash.protobuf.CustomEnumValueMap;
import jmash.protobuf.Entry;
import jmash.protobuf.EntryList;
import jmash.protobuf.EnumEntryReq;
import jmash.protobuf.EnumValueList;
import jmash.region.protobuf.DictRegionCreateReq;
import jmash.region.protobuf.DictRegionEnableKey;
import jmash.region.protobuf.DictRegionExportReq;
import jmash.region.protobuf.DictRegionImportReq;
import jmash.region.protobuf.DictRegionKey;
import jmash.region.protobuf.DictRegionKeyList;
import jmash.region.protobuf.DictRegionList;
import jmash.region.protobuf.DictRegionModel;
import jmash.region.protobuf.DictRegionMoveKey;
import jmash.region.protobuf.DictRegionReq;
import jmash.region.protobuf.DictRegionUpdateReq;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;

/**
 * Grpc服务实现.
 */
@GrpcService
public class RegionImpl extends jmash.region.RegionGrpc.RegionImplBase {

  private static Log log = LogFactory.getLog(RegionImpl.class);

  // 模块版本
  public static final String version = "v1.0.0";

  @Override
  public void version(Empty request, StreamObserver<StringValue> responseObserver) {
    responseObserver.onNext(StringValue.of(version + "-" + VersionUtil.snapshot(RegionImpl.class)));
    responseObserver.onCompleted();
  }

  @Override
  public void findEnumList(StringValue request, StreamObserver<EnumValueList> responseObserver) {
    try {
      List<EnumValue> list = ProtoEnumUtil.getEnumList(request.getValue());
      responseObserver.onNext(EnumValueList.newBuilder().addAllValues(list).build());
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  public void findEnumMap(StringValue request,
      StreamObserver<CustomEnumValueMap> responseObserver) {
    try {
      Map<Integer, CustomEnumValue> values = ProtoEnumUtil.getEnumMap(request.getValue());
      responseObserver.onNext(CustomEnumValueMap.newBuilder().putAllValues(values).build());
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  public void findEnumEntry(EnumEntryReq request, StreamObserver<EntryList> responseObserver) {
    try {
      List<Entry> entryList =
          ProtoEnumUtil.getEnumCodeList(request.getClassName(), request.getType());
      responseObserver.onNext(EntryList.newBuilder().addAllValues(entryList).build());
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresAuthentication
  public void findDictRegionList(DictRegionReq request,
      StreamObserver<DictRegionList> responseObserver) {
    try (DictRegionRead dictRegionRead = RegionFactory.getDictRegionRead(request.getTenant())) {
      DictRegionList modelList = dictRegionRead.findModelListByReq(request);
      responseObserver.onNext(modelList);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("region:dict_region:view")
  public void findDictRegionById(DictRegionKey request,
      StreamObserver<DictRegionModel> responseObserver) {
    try (DictRegionRead dictRegionRead = RegionFactory.getDictRegionRead(request.getTenant())) {
      DictRegionEntity entity = dictRegionRead.findById(UUIDUtil.fromString(request.getRegionId()));
      DictRegionModel model = DictRegionMapper.INSTANCE.model(entity);
      responseObserver.onNext(model);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresAuthentication
  public void findDictRegionByReq(DictRegionReq request,
      StreamObserver<DictRegionModel> responseObserver) {
    try (DictRegionRead dictRegionRead = RegionFactory.getDictRegionRead(request.getTenant())) {
      DictRegionModel model = dictRegionRead.findRegionModelByReq(request);
      responseObserver.onNext(model);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("region:dict_region:update")
  public void moveDictRegion(DictRegionMoveKey request,
      StreamObserver<BoolValue> responseObserver) {
    try (DictRegionWrite dictRegionWrite = RegionFactory.getDictRegionWrite(request.getTenant())) {
      boolean value = dictRegionWrite.move(request);
      responseObserver.onNext(BoolValue.of(value));
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("region:dict_region:enable")
  public void enableDictRegion(DictRegionEnableKey request,
      StreamObserver<BoolValue> responseObserver) {
    try (DictRegionWrite dictRegionWrite = RegionFactory.getDictRegionWrite(request.getTenant())) {
      boolean value = dictRegionWrite.enable(request);
      responseObserver.onNext(BoolValue.of(value));
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }


  @Override
  @RequiresPermissions("region:dict_region:add")
  public void createDictRegion(DictRegionCreateReq request,
      StreamObserver<DictRegionModel> responseObserver) {
    try (DictRegionWrite dictRegionWrite = RegionFactory.getDictRegionWrite(request.getTenant())) {
      DictRegionEntity entity = dictRegionWrite.insert(request);
      DictRegionModel model = DictRegionMapper.INSTANCE.model(entity);
      responseObserver.onNext(model);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("region:dict_region:update")
  public void updateDictRegion(DictRegionUpdateReq request,
      StreamObserver<DictRegionModel> responseObserver) {
    try (DictRegionWrite dictRegionWrite = RegionFactory.getDictRegionWrite(request.getTenant())) {
      DictRegionEntity entity = dictRegionWrite.update(request);
      DictRegionModel model = DictRegionMapper.INSTANCE.model(entity);
      responseObserver.onNext(model);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("region:dict_region:delete")
  public void deleteDictRegion(DictRegionKey request,
      StreamObserver<DictRegionModel> responseObserver) {
    try (DictRegionWrite dictRegionWrite = RegionFactory.getDictRegionWrite(request.getTenant())) {
      DictRegionEntity entity = dictRegionWrite.delete(UUIDUtil.fromString(request.getRegionId()));
      DictRegionModel model = DictRegionMapper.INSTANCE.model(entity);
      responseObserver.onNext(model);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("region:dict_region:delete")
  public void batchDeleteDictRegion(DictRegionKeyList request,
      StreamObserver<Int32Value> responseObserver) {
    try (DictRegionWrite dictRegionWrite = RegionFactory.getDictRegionWrite(request.getTenant())) {
      final List<String> list = request.getRegionIdList();
      final Set<UUID> set = list.stream().map(v -> UUIDUtil.fromString(v))
          .collect(Collectors.toSet());

      int r = dictRegionWrite.batchDelete(set);
      responseObserver.onNext(Int32Value.of(r));
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("region:dict_region:import")
  public void downloadDictRegion(DictRegionExportReq request,
      StreamObserver<HttpBody> responseObserver) {
    try (DictRegionRead dictRegionRead = RegionFactory.getDictRegionRead(request.getTenant())) {
      String realFileSrc = dictRegionRead.downRegionTemplate(request);
      FileServiceUtil.downloadFile(responseObserver, realFileSrc);
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("region:dict_region:import")
  public void importDictRegion(DictRegionImportReq request,
      StreamObserver<StringValue> responseObserver) {
    try (DictRegionWrite dictRegionWrite = RegionFactory.getDictRegionWrite(request.getTenant())) {
      String r = dictRegionWrite.importRegionInfo(request);
      responseObserver.onNext(StringValue.of(r));
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("region:dict_region:export")
  public void exportDictRegion(DictRegionExportReq request,
      StreamObserver<HttpBody> responseObserver) {
    try (DictRegionRead dictRegionRead = RegionFactory.getDictRegionRead(request.getTenant())) {
      String realFileSrc = dictRegionRead.exportRegionInfo(request);
      FileServiceUtil.downloadFile(responseObserver, realFileSrc);
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }
}
