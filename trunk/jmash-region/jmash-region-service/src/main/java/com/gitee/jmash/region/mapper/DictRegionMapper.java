
package com.gitee.jmash.region.mapper;

import com.crenjoy.proto.mapper.BeanMapper;
import com.crenjoy.proto.mapper.ProtoMapper;
import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.region.entity.DictRegionEntity;
import com.gitee.jmash.region.model.DictRegionTotal;
import com.google.protobuf.FieldMask;
import java.util.List;
import java.util.UUID;
import jmash.region.protobuf.DictRegionCreateReq;
import jmash.region.protobuf.DictRegionModel;
import jmash.region.protobuf.DictRegionPage;
import jmash.region.protobuf.DictRegionUpdateReq;
import org.apache.commons.lang3.StringUtils;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * DictRegionMapper.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Mapper
public interface DictRegionMapper extends BeanMapper, ProtoMapper {

  DictRegionMapper INSTANCE = Mappers.getMapper(DictRegionMapper.class);

  FieldMask filedMask = FieldMask.newBuilder().addPaths("regionName").addPaths("regionCode")
      .addPaths("enable")
      .addPaths("description").build();

  List<DictRegionModel> listDictRegion(List<DictRegionEntity> list);

  DictRegionPage pageDictRegion(DtoPage<DictRegionEntity, DictRegionTotal> page);

  DictRegionModel model(DictRegionEntity entity);

  /**
   * 实体是否有子节点的组装.
   *
   * @param entity 地区实体对象.
   * @param hasChildren 是否有子节点.
   * @return 地区数据模型.
   */
  default DictRegionModel model(DictRegionEntity entity, boolean hasChildren) {
    DictRegionModel regionModel = model(entity);
    regionModel = regionModel.toBuilder().setHasChildren(hasChildren).build();
    return regionModel;
  }

  /**
   * 父节点和子节点的组装.
   *
   * @param entity 地区实体对象.
   * @param child 子节点数据模型.
   * @return 地区数据模型.
   */
  default DictRegionModel model(DictRegionEntity entity, DictRegionModel child) {
    DictRegionModel regionModel = model(entity);
    if (child != null && StringUtils.isNotBlank(child.getRegionId())) {
      regionModel = regionModel.toBuilder().setHasChildren(true).addChildren(child).build();
    }
    return regionModel;
  }

  DictRegionEntity create(DictRegionCreateReq req);

  DictRegionEntity clone(DictRegionEntity entity);

  /**
   * 组装创建请求.
   *
   * @param model 地区数据模型.
   * @return 创建请求对象.
   */
  default DictRegionCreateReq genCreate(DictRegionModel model) {
    DictRegionCreateReq.Builder build = buildCreate(model).toBuilder();
    return build.setRequestId(UUID.randomUUID().toString()).build();
  }

  /**
   * 组装更新请求.
   *
   * @param model 地区数据模型.
   * @return 更新请求对象.
   */
  default DictRegionUpdateReq genUpdate(DictRegionModel model) {
    DictRegionUpdateReq.Builder build = buildUpdate(model).toBuilder();
    build.setUpdateMask(filedMask);
    return build.setRequestId(UUID.randomUUID().toString()).build();
  }


  DictRegionCreateReq buildCreate(DictRegionModel model);

  DictRegionUpdateReq buildUpdate(DictRegionModel model);

}
