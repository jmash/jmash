package com.gitee.jmash.region.service;


import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.region.entity.DictRegionEntity;
import com.gitee.jmash.region.model.DictRegionTotal;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.io.IOException;
import java.util.List;
import java.util.UUID;
import jmash.region.protobuf.DictRegionExportReq;
import jmash.region.protobuf.DictRegionList;
import jmash.region.protobuf.DictRegionModel;
import jmash.region.protobuf.DictRegionReq;

/**
 * 地区信息 os_dict_region服务Read接口.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public interface DictRegionRead extends TenantService {

  /**
   * 根据主键查询.
   */
  public DictRegionEntity findById(@NotNull UUID entityId);

  /**
   * 查询页信息.
   */
  public DtoPage<DictRegionEntity, DictRegionTotal> findPageByReq(
      @NotNull @Valid DictRegionReq req);

  /**
   * 综合查询.
   */
  public List<DictRegionEntity> findListByReq(@NotNull @Valid DictRegionReq req);

  /**
   * 列表查询.
   */
  public DictRegionList findModelListByReq(@NotNull @Valid DictRegionReq req);

  /**
   * 区域信息查询.
   */
  public DictRegionModel findRegionModelByReq(@NotNull @Valid DictRegionReq req);

  /**
   * 区域信息列表.
   */
  public List<DictRegionEntity> findRegionList(DictRegionReq req);

  /**
   * 区域信息模板下载.
   */
  public String downRegionTemplate(@NotNull @Valid DictRegionExportReq req) throws IOException;

  /**
   * 区域信息导出.
   */
  public String exportRegionInfo(@NotNull @Valid DictRegionExportReq req) throws IOException;
}
