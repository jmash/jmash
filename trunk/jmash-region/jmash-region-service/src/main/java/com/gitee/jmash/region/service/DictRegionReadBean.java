
package com.gitee.jmash.region.service;


import com.gitee.jmash.common.excel.ExcelExport;
import com.gitee.jmash.common.tree.Tree;
import com.gitee.jmash.common.utils.UUIDUtil;
import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.orm.cdi.JpaTenantService;
import com.gitee.jmash.core.orm.jpa.TenantEntityManager;
import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.core.transaction.JakartaTransaction;
import com.gitee.jmash.core.utils.FilePathUtil;
import com.gitee.jmash.region.dao.DictRegionDao;
import com.gitee.jmash.region.entity.DictRegionEntity;
import com.gitee.jmash.region.excel.DictRegionHeaderExport;
import com.gitee.jmash.region.excel.DictRegionSheetExportDto;
import com.gitee.jmash.region.mapper.DictRegionMapper;
import com.gitee.jmash.region.model.DictRegionTotal;
import jakarta.enterprise.inject.Typed;
import jakarta.enterprise.inject.spi.CDI;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import jakarta.transaction.Transactional.TxType;
import jakarta.validation.executable.ValidateOnExecution;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import jmash.protobuf.TableHead;
import jmash.region.protobuf.DictRegionExportReq;
import jmash.region.protobuf.DictRegionList;
import jmash.region.protobuf.DictRegionModel;
import jmash.region.protobuf.DictRegionReq;
import org.apache.commons.lang3.StringUtils;

/**
 * 地区信息 os_dict_region读服务.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Typed(DictRegionRead.class)
@Transactional(TxType.SUPPORTS)
@JpaTenantService
@ValidateOnExecution
public class DictRegionReadBean implements DictRegionRead, JakartaTransaction {

  protected TenantEntityManager tem = new TenantEntityManager();

  protected DictRegionDao dictRegionDao = new DictRegionDao(tem);

  @PersistenceContext(unitName = "ReadRegion")
  public void setEntityManager(EntityManager entityManager) {
    this.tem.setEntityManager(entityManager, false);
  }

  @Override
  public EntityManager getEntityManager() {
    return this.tem.getEntityManager();
  }

  @Override
  @SuppressWarnings("unchecked")
  public <T extends TenantService> T setTenant(String tenant) {
    this.tem.setTenant(tenant);
    return (T) this;
  }

  @Override
  public void setTenantOnly(String tenant) {
    this.tem.setTenantOnly(tenant);
  }

  @Override
  public String getTenant() {
    return this.tem.getTenant();
  }

  @Override
  public DictRegionEntity findById(UUID entityId) {
    return dictRegionDao.find(entityId);
  }

  @Override
  public DtoPage<DictRegionEntity, DictRegionTotal> findPageByReq(DictRegionReq req) {
    return dictRegionDao.findPageByReq(req);
  }

  @Override
  public List<DictRegionEntity> findListByReq(DictRegionReq req) {
    return dictRegionDao.findListByReq(req);
  }

  @Override
  public DictRegionList findModelListByReq(DictRegionReq req) {
    DictRegionList.Builder result = DictRegionList.newBuilder();
    List<DictRegionEntity> list = dictRegionDao.findListByReq(req);
    for (DictRegionEntity entity : list) {
      int childCount = dictRegionDao.findCount(
          DictRegionReq.newBuilder().setParentId(entity.getRegionId().toString()).build());
      result.addResults(DictRegionMapper.INSTANCE.model(entity, childCount > 0));
    }
    return result.build();
  }

  @Override
  public DictRegionModel findRegionModelByReq(DictRegionReq req) {
    List<DictRegionEntity> regionList = dictRegionDao.findListByReq(
        req.toBuilder().setHasParent(true).build());
    if (regionList.size() == 0) {
      return null;
    }
    return buildRegionModel(regionList.get(0), DictRegionModel.getDefaultInstance());
  }

  /**
   * 构建地区信息树.
   */
  private DictRegionModel buildRegionModel(DictRegionEntity entity, DictRegionModel model) {
    model = DictRegionMapper.INSTANCE.model(entity, model);
    if (!UUIDUtil.emptyUUID().equals(entity.getParentId())) {
      entity = findById(entity.getParentId());
      model = buildRegionModel(entity, model);
    }
    return model;
  }

  /**
   * 查询地区信息树.
   */
  public Tree<DictRegionEntity, UUID> findTreeByReq(DictRegionReq req) {
    // 得到所有节点list信息然后放到内存树中
    Tree<DictRegionEntity, UUID> tree = new Tree<>();
    List<DictRegionEntity> regionList = dictRegionDao.findListByReq(req);
    if (regionList.size() == 0) {
      return tree;
    }
    for (DictRegionEntity regionEntity : regionList) {
      tree.add(regionEntity.getRegionId(), regionEntity, regionEntity.getParentId(),
          regionEntity.getOrderBy());
    }
    return tree;
  }

  @Override
  public List<DictRegionEntity> findRegionList(DictRegionReq req) {
    Tree<DictRegionEntity, UUID> tree = findTreeByReq(req);
    List<DictRegionEntity> regionList = StringUtils.isNotBlank(req.getRegionId())
        ? tree.getSelfChildsList(UUIDUtil.fromString(req.getRegionId()))
        : tree.getAllList();
    if (regionList.isEmpty()) {
      return Collections.emptyList();
    }
    return regionList;
  }

  @Override
  public String downRegionTemplate(DictRegionExportReq req) throws IOException {
    return exportRegionInfo(Collections.emptyList(), req.getTitle(), req.getTableHeadsList(),
        req.getFileName());
  }

  @Override
  public String exportRegionInfo(DictRegionExportReq req) throws IOException {
    List<DictRegionSheetExportDto> regionSheetList = new ArrayList<>();
    //获取第一级区域信息
    List<DictRegionEntity> parentList = dictRegionDao.findListByReq(
        DictRegionReq.newBuilder().setTenant(getTenant()).build());
    Tree<DictRegionEntity, UUID> tree = findTreeByReq(req.getReq());
    for (DictRegionEntity entity : parentList) {
      List<DictRegionEntity> regionList = tree.getSelfChildsList(entity.getRegionId());
      regionSheetList.add(new DictRegionSheetExportDto(entity.getRegionName(), regionList));
    }
    return exportRegionInfo(regionSheetList, req.getFileName());
  }

  /**
   * 导出地区信息.
   *
   * @param regionSheetList 区域信息集合.
   * @param fileName 文件名称.
   * @return 文件路径.
   * @throws IOException io异常.
   */
  public String exportRegionInfo(List<DictRegionSheetExportDto> regionSheetList, String fileName)
      throws IOException {
    ExcelExport export = new ExcelExport();
    for (DictRegionSheetExportDto regionSheet : regionSheetList) {
      export.createSheet(regionSheet.getSheetName());
      export.addHeaderAll(regionSheet.getHeaderCollection());
      export.writeHeader();
      export.writeListData(regionSheet.getDataList());
      // 增加数据校验.
      export.dictDataValidation(regionSheet.getDataList().size(), 3000);
    }
    String fileSrc = FilePathUtil.createNewTempFile(fileName + ".xlsx", "excel");
    String realFileSrc = FilePathUtil.realPath(fileSrc,true);
    export.getWb().write(new FileOutputStream(realFileSrc));
    return realFileSrc;
  }

  /**
   * 导出地区信息.
   *
   * @param regionList 区域信息.
   * @param title      标题.
   * @param tableHeads 表头.
   * @param fileName   文件名称.
   * @return 文件路径.
   * @throws IOException io异常.
   */
  public String exportRegionInfo(List<DictRegionEntity> regionList, String title,
      List<TableHead> tableHeads, String fileName) throws IOException {
    ExcelExport excelExport = new ExcelExport();
    // 页签
    excelExport.createSheet(DictRegionHeaderExport.TITLE);
    // 增加表头
    DictRegionHeaderExport.addHeaderExport(excelExport, tableHeads);
    // 写表头
    excelExport.writeHeader();
    // 写数据
    excelExport.writeListData(regionList);
    // 增加数据校验
    excelExport.dictDataValidation(regionList.size(), 3000);
    // 写入文件
    fileName = StringUtils.isBlank(fileName) ? title : fileName;
    String fileSrc = FilePathUtil.createNewTempFile(fileName + ".xlsx", "excel");
    String realFileSrc = FilePathUtil.realPath(fileSrc,true);
    excelExport.getWb().write(new FileOutputStream(realFileSrc));
    return realFileSrc;
  }

  @Override
  public void close() throws Exception {
    CDI.current().destroy(this);
  }
}
