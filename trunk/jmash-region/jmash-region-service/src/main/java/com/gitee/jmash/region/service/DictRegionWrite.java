package com.gitee.jmash.region.service;

import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.region.entity.DictRegionEntity;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.util.Set;
import java.util.UUID;
import jmash.region.protobuf.DictRegionCreateReq;
import jmash.region.protobuf.DictRegionEnableKey;
import jmash.region.protobuf.DictRegionImportReq;
import jmash.region.protobuf.DictRegionMoveKey;
import jmash.region.protobuf.DictRegionUpdateReq;

/**
 * 地区信息 os_dict_region服务Write接口.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public interface DictRegionWrite extends TenantService {

  /**
   * 插入实体.
   */
  public DictRegionEntity insert(@NotNull @Valid DictRegionCreateReq dictRegion);


  /**
   * update 实体.
   */
  public DictRegionEntity update(@NotNull @Valid DictRegionUpdateReq dictRegion);

  /**
   * 上移、下移.
   */
  public boolean move(@NotNull DictRegionMoveKey dictRegionMoveKey);

  /**
   * 启用/禁用.
   */
  public boolean enable(@NotNull DictRegionEnableKey dictRegionEnableKey);

  /**
   * 根据主键删除.
   */
  public DictRegionEntity delete(@NotNull UUID entityId);

  /**
   * 根据主键数组删除.
   */
  public Integer batchDelete(@NotNull @Valid Set<@NotNull UUID> entityIds);

  /**
   * 导入区域信息.
   */
  public String importRegionInfo(@NotNull @Valid DictRegionImportReq regionImportReq)
      throws Exception;
}
