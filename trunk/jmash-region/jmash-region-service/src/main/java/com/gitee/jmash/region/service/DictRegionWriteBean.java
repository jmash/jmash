
package com.gitee.jmash.region.service;


import com.gitee.jmash.common.excel.ExcelImport;
import com.gitee.jmash.common.lock.DistributedLock;
import com.gitee.jmash.common.utils.UUIDUtil;
import com.gitee.jmash.core.jaxrs.ParamsValidationException;
import com.gitee.jmash.core.orm.cdi.JpaTenantService;
import com.gitee.jmash.core.transaction.JakartaTransaction;
import com.gitee.jmash.core.utils.FieldMaskUtil;
import com.gitee.jmash.file.client.cdi.FileClient;
import com.gitee.jmash.region.entity.DictRegionEntity;
import com.gitee.jmash.region.excel.DictRegionHeaderImport;
import com.gitee.jmash.region.mapper.DictRegionMapper;
import jakarta.enterprise.inject.Typed;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import jakarta.transaction.Transactional.TxType;
import jakarta.validation.ValidationException;
import jakarta.validation.executable.ValidateOnExecution;
import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import jmash.region.protobuf.DictRegionCreateReq;
import jmash.region.protobuf.DictRegionEnableKey;
import jmash.region.protobuf.DictRegionImportReq;
import jmash.region.protobuf.DictRegionModel;
import jmash.region.protobuf.DictRegionMoveKey;
import jmash.region.protobuf.DictRegionReq;
import jmash.region.protobuf.DictRegionUpdateReq;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Workbook;

/**
 * 地区信息 os_dict_region写服务.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Typed(DictRegionWrite.class)
@Transactional(TxType.REQUIRED)
@JpaTenantService
@ValidateOnExecution
public class DictRegionWriteBean extends DictRegionReadBean implements DictRegionWrite,
    JakartaTransaction {

  private static final Log log = LogFactory.getLog(DictRegionWriteBean.class);

  @Inject
  DistributedLock lock;

  @Override
  @PersistenceContext(unitName = "WriteRegion")
  public void setEntityManager(EntityManager entityManager) {
    this.tem.setEntityManager(entityManager, true);
  }

  @Override
  public DictRegionEntity insert(DictRegionCreateReq dictRegion) {
    DictRegionEntity entity = DictRegionMapper.INSTANCE.create(dictRegion);
    // 1.业务校验.
    validate(null, entity);
    // 2.仅校验,不执行.
    if (dictRegion.getValidateOnly()) {
      return entity;
    }
    // 3.检查是否重复请求.
    if (!lock.lock(dictRegion.getRequestId(), 60)) {
      throw new ParamsValidationException("requestId", "客户端发起重复请求");
    }
    // 4.执行业务(创建人及时间内部处理.)
    // 深度和排序设置.
    dictRegionDao.insertTree(entity, UUIDUtil.fromString(dictRegion.getParentId()),
        Collections.emptyMap());
    dictRegionDao.persist(entity);
    return entity;
  }

  /**
   * 校验地区名称和编码是否重复.
   */
  private void validate(UUID regionId, DictRegionEntity entity) {
    boolean hasName = dictRegionDao.validateRegion(regionId, entity.getRegionName(),
        StringUtils.EMPTY);
    if (hasName) {
      throw new ValidationException("地区名称已存在");
    }
    boolean hasCode = dictRegionDao.validateRegion(regionId, StringUtils.EMPTY,
        entity.getRegionCode());
    if (hasCode) {
      throw new ValidationException("地区编码已存在");
    }
  }

  @Override
  public DictRegionEntity update(DictRegionUpdateReq req) {
    UUID id = UUIDUtil.fromString(req.getRegionId());
    DictRegionEntity entity = dictRegionDao.find(id, req.getValidateOnly());
    if (null == entity) {
      throw new ValidationException("找不到实体:" + req.getRegionId());
    }
    // 无需更新,返回当前数据库数据.
    if (req.getUpdateMask().getPathsCount() == 0) {
      return entity;
    }
    // 放到FieldMaskUtil.copyMask前
    final UUID newParentId = UUIDUtil.fromString(req.getParentId());
    final UUID oldParentId = entity.getParentId();
    // 更新掩码属性
    FieldMaskUtil.copyMask(entity, req, req.getUpdateMask());
    // 1.业务校验.
    validate(id, entity);
    // 2.仅校验,不执行.
    if (req.getValidateOnly()) {
      return entity;
    }

    // 3.检查是否重复请求.
    if (!lock.lock(req.getRequestId(), 60)) {
      throw new ParamsValidationException("requestId", "客户端发起重复请求");
    }

    // 4.执行业务
    dictRegionDao.updateTree(entity, oldParentId, newParentId, Collections.emptyMap());
    dictRegionDao.merge(entity);
    return entity;
  }

  @Override
  public boolean move(DictRegionMoveKey dictRegionMoveKey) {
    UUID id = UUIDUtil.fromString(dictRegionMoveKey.getRegionId());
    DictRegionEntity entity = dictRegionDao.find(id);
    if (null == entity) {
      throw new ValidationException("找不到实体:" + id);
    }
    return dictRegionDao.moveOrderBy(dictRegionMoveKey.getUp(), entity,
        Collections.singletonMap("parentId", Collections.emptyMap()));
  }

  @Override
  public boolean enable(DictRegionEnableKey dictRegionEnableKey) {
    UUID id = UUIDUtil.fromString(dictRegionEnableKey.getRegionId());
    DictRegionEntity entity = dictRegionDao.find(id);
    if (null == entity) {
      throw new ValidationException("找不到实体:" + id);
    }
    entity.setEnable(dictRegionEnableKey.getEnable());
    dictRegionDao.merge(entity);
    return true;
  }

  @Override
  public DictRegionEntity delete(UUID entityId) {
    DictRegionReq.Builder selReq = DictRegionReq.newBuilder();
    selReq.setTenant(getTenant()).setHasParent(true).setRegionId(entityId.toString());
    List<DictRegionEntity> regionList = findRegionList(selReq.build());
    DictRegionEntity entity = dictRegionDao.removeById(entityId);
    for (DictRegionEntity region : regionList) {
      dictRegionDao.removeById(region.getRegionId());
    }
    return entity;
  }

  @Override
  public Integer batchDelete(Set<UUID> entityIds) {
    int i = 0;
    for (UUID entityId : entityIds) {
      dictRegionDao.removeById(entityId);
      i++;
    }
    return i;
  }

  @Override
  public String importRegionInfo(DictRegionImportReq regionImportReq) throws Exception {
    // 检查是否重复请求.
    if (!lock.lock(regionImportReq.getRequestId(), 60)) {
      throw new ParamsValidationException("requestId", "客户端发起重复请求");
    }
    // 文件检查
    File file = FileClient.downloadFile(regionImportReq.getFileNames());
    if (!file.exists()) {
      return "找不到文件：" + regionImportReq.getFileNames();
    }
    int success = 0;
    int fail = 0;
    ExcelImport excel = new ExcelImport();
    excel.openExcel(file);
    excel.setHeaders(DictRegionHeaderImport.getHeaderImports());
    excel.setStartHeader(0);
    Workbook wb = excel.getWb();
    for (int i = 0; i < wb.getNumberOfSheets(); i++) {
      List<DictRegionModel> regionModels = excel.importEntity(i,
          DictRegionModel.getDefaultInstance());
      // 资源新ID存放.
      Map<String, String> newIdMap = new HashMap<>();
      // 地区信息导入.
      for (DictRegionModel regionModel : regionModels) {
        try {
          // 替换新ID
          if (newIdMap.containsKey(regionModel.getParentId())) {
            regionModel = regionModel.toBuilder()
                .setParentId(newIdMap.get(regionModel.getParentId())).build();
          }
          DictRegionEntity entity = this.dictRegionDao.findByRegionCode(
              regionModel.getRegionCode());
          if (entity == null) {
            entity = this.insert(DictRegionMapper.INSTANCE.genCreate(regionModel));
          } else {
            entity = this.update(DictRegionMapper.INSTANCE.genUpdate(regionModel));
          }
          // 存放新ID.
          newIdMap.put(regionModel.getRegionId(), entity.getRegionId().toString());
          success++;
        } catch (Exception ex) {
          log.error("", ex);
          fail++;
        }
      }
    }
    return String.format("导入成功%d行,失败%d行.<br/>%s", success, fail, "");
  }

}
