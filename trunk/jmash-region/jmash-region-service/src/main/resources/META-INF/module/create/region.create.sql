/*!40101 SET NAMES utf8 */;
CREATE TABLE os_dict_region
(
    `region_id`       binary(16) NOT NULL COMMENT '地区ID',
    `parent_id`       binary(16) COMMENT '父ID',
    `region_code`     VARCHAR(30)  NOT NULL COMMENT '地区编码',
    `region_name`     VARCHAR(120) NOT NULL COMMENT '地区名称',
    `region_pinyin`   VARCHAR(30) COMMENT '地区拼音',
    `region_type`     VARCHAR(50) COMMENT '地区类型$国家 省 市 县 可扩展多国支持',
    `region_category` VARCHAR(50) COMMENT '省分类$中国 分 华北 华南',
    `depth_`          INT          NOT NULL COMMENT '深度',
    `order_by`        INT COMMENT '排序',
    `enable_`         tinyint(1) COMMENT '是否启用',
    `description_`    VARCHAR(240) COMMENT '描述',
    PRIMARY KEY (region_id)
) COMMENT = '地区信息';
