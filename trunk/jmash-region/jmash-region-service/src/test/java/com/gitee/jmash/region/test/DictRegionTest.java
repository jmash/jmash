
package com.gitee.jmash.region.test;

import com.gitee.jmash.core.lib.FileClientUtil;
import com.google.api.HttpBody;
import com.google.protobuf.FieldMask;
import com.google.protobuf.Int32Value;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import java.io.File;
import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import jmash.rbac.RbacGrpc;
import jmash.rbac.protobuf.UserImportReq;
import jmash.region.RegionGrpc;
import jmash.region.protobuf.DictRegionCreateReq;
import jmash.region.protobuf.DictRegionExportReq;
import jmash.region.protobuf.DictRegionImportReq;
import jmash.region.protobuf.DictRegionKey;
import jmash.region.protobuf.DictRegionKeyList;
import jmash.region.protobuf.DictRegionList;
import jmash.region.protobuf.DictRegionModel;
import jmash.region.protobuf.DictRegionReq;
import jmash.region.protobuf.DictRegionUpdateReq;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

/**
 * 测试.
 *
 * @author CGD
 */
@TestMethodOrder(OrderAnnotation.class)
public class DictRegionTest extends RegionTest {

  private RegionGrpc.RegionBlockingStub regionStub = null;

  private RegionGrpc.RegionStub region = null;

  @Test
  @Order(1)
  public void validatorTest() {
    regionStub = RegionGrpc.newBlockingStub(channel);
    regionStub = regionStub.withCallCredentials(getAuthcCallCredentials());
    StatusRuntimeException exception = Assertions.assertThrows(StatusRuntimeException.class,
        () -> {
          DictRegionCreateReq req = DictRegionCreateReq.newBuilder().build();
          regionStub.createDictRegion(req);
        });
    Assertions.assertEquals("INTERNAL", exception.getStatus().getCode().name());
    Assertions.assertNotNull(exception.getMessage());
  }

  @Test
  @Order(3)
  public void batchDeleteTest() {
    regionStub = RegionGrpc.newBlockingStub(channel);
    regionStub = regionStub.withCallCredentials(getAuthcCallCredentials());
    DictRegionKeyList.Builder request = DictRegionKeyList.newBuilder();
    request.setTenant(TENANT);
    final Int32Value result = regionStub.batchDeleteDictRegion(request.build());
    System.out.println(result);
  }

  @Test
  @Order(4)
  public void test() {
    regionStub = RegionGrpc.newBlockingStub(channel);
    regionStub = regionStub.withCallCredentials(getAuthcCallCredentials());

    StatusRuntimeException exception = Assertions.assertThrows(StatusRuntimeException.class,
        () -> {
          // 创建
          DictRegionCreateReq.Builder req = DictRegionCreateReq.newBuilder();
          req.setRequestId(UUID.randomUUID().toString());
          req.setTenant(TENANT);
          req.setRegionName("河北省");
          req.setRegionCode("130000");
          //...
          DictRegionModel entity = regionStub.createDictRegion(req.build());

          // findById
          DictRegionKey pk = DictRegionKey.newBuilder().setRegionId(entity.getRegionId()).build();
          entity = regionStub.findDictRegionById(pk);

          // Update
          DictRegionUpdateReq.Builder updateReq = DictRegionUpdateReq.newBuilder();
          updateReq.setRequestId(UUID.randomUUID().toString());
          updateReq.setRegionId(entity.getRegionId());
          updateReq.setRegionName("河北省c");
          //...
          FieldMask.Builder fieldMask = FieldMask.newBuilder();
          fieldMask.addPaths("regionName");

          updateReq.setUpdateMask(fieldMask.build());

          entity = regionStub.updateDictRegion(updateReq.build());
          // findById
          pk = DictRegionKey.newBuilder().setRegionId(entity.getRegionId()).build();
          entity = regionStub.findDictRegionById(pk);
          // Delete
          pk = DictRegionKey.newBuilder().setRegionId(entity.getRegionId()).build();
          entity = regionStub.deleteDictRegion(pk);
        });
    Assertions.assertEquals("INTERNAL", exception.getStatus().getCode().name());
    Assertions.assertNotNull(exception.getMessage());
  }

  @Test
  public void findListTest() {
    regionStub = RegionGrpc.newBlockingStub(channel);
    regionStub = regionStub.withCallCredentials(getAuthcCallCredentials());
    DictRegionReq.Builder req = DictRegionReq.newBuilder();
    req.setTenant(TENANT);
    req.setParentId("4fd1f547-c48c-4fdb-861b-284eaeb3edcf");
    DictRegionList result = regionStub.findDictRegionList(req.build());
    System.out.println(result);
  }

  @Test
  public void downloadTemplate() throws IOException, InterruptedException {
    region = RegionGrpc.newStub(channel);
    region = region.withCallCredentials(getAuthcCallCredentials());
    final CountDownLatch latchFinish = new CountDownLatch(1);
    StreamObserver<HttpBody> resp = FileClientUtil
        .createHttpBodyResp(new File("/data/jmash/test/region_template.xlsx"), latchFinish);
    region.downloadDictRegion(DictRegionExportReq.newBuilder().setTenant(TENANT).build(), resp);
    latchFinish.await();
  }

  @Test
  public void exportRegionList() throws IOException, InterruptedException {
    region = RegionGrpc.newStub(channel);
    region = region.withCallCredentials(getAuthcCallCredentials());
    DictRegionExportReq req = DictRegionExportReq.newBuilder()
        .setReq(
            DictRegionReq.newBuilder().setHasParent(true)
                .setRegionId("775dcc8b-c53e-469b-9dc3-bc5bcfb59ddb").build())
        .setTenant(TENANT).build();
    final CountDownLatch latchFinish = new CountDownLatch(1);
    StreamObserver<HttpBody> resp =
        FileClientUtil.createHttpBodyResp(new File("/data/jmash/test/region.xlsx"), latchFinish);
    region.exportDictRegion(req, resp);
    latchFinish.await();
  }

  @Test
  public void findDictRegionByReq() {
    regionStub = RegionGrpc.newBlockingStub(channel);
    regionStub = regionStub.withCallCredentials(getAuthcCallCredentials());
    DictRegionReq req = DictRegionReq.newBuilder().setTenant(TENANT).setRegionCode("140110")
        .build();
    DictRegionModel result = regionStub.findDictRegionByReq(req);
    System.out.println(result);
  }

  @Test
  public void deleteDictRegion() {
    regionStub = RegionGrpc.newBlockingStub(channel);
    regionStub = regionStub.withCallCredentials(getAuthcCallCredentials());
    DictRegionKey req = DictRegionKey.newBuilder().setTenant(TENANT)
        .setRegionId("de9d901b-e4e8-4117-8852-0dc750e072c1").build();
    regionStub.deleteDictRegion(req);
  }

  @Test
  @Order(7)
  public void importInsertExcel() {
    regionStub = RegionGrpc.newBlockingStub(channel);
    regionStub = regionStub.withCallCredentials(getAuthcCallCredentials());
    FieldMask mask = FieldMask.newBuilder().build();
    regionStub.importDictRegion(
        DictRegionImportReq.newBuilder().setRequestId(UUID.randomUUID().toString()).setTenant(TENANT)
            .setFileNames("data/jmash/test/template.xlsx")
            .setAddFlag(true).setUpdateMask(mask).build());
  }

}
