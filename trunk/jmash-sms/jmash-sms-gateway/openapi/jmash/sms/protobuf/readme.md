# Protocol Documentation
<a name="top"></a>

## Table of Contents

- [jmash/sms/protobuf/sms_app_config_message.proto](#jmash_sms_protobuf_sms_app_config_message-proto)
    - [SmsAppConfigCreateReq](#jmash-sms-SmsAppConfigCreateReq)
    - [SmsAppConfigKey](#jmash-sms-SmsAppConfigKey)
    - [SmsAppConfigKeyList](#jmash-sms-SmsAppConfigKeyList)
    - [SmsAppConfigList](#jmash-sms-SmsAppConfigList)
    - [SmsAppConfigModel](#jmash-sms-SmsAppConfigModel)
    - [SmsAppConfigModelTotal](#jmash-sms-SmsAppConfigModelTotal)
    - [SmsAppConfigPage](#jmash-sms-SmsAppConfigPage)
    - [SmsAppConfigReq](#jmash-sms-SmsAppConfigReq)
    - [SmsAppConfigUpdateReq](#jmash-sms-SmsAppConfigUpdateReq)
  
    - [ConfigType](#jmash-sms-ConfigType)
  
- [jmash/sms/protobuf/sms_message.proto](#jmash_sms_protobuf_sms_message-proto)
    - [EmailBasicReq](#jmash-sms-EmailBasicReq)
    - [EmailCaptchaReq](#jmash-sms-EmailCaptchaReq)
    - [SmsBasicReq](#jmash-sms-SmsBasicReq)
    - [SmsBasicReq.ParamsEntry](#jmash-sms-SmsBasicReq-ParamsEntry)
    - [SmsCaptchaReq](#jmash-sms-SmsCaptchaReq)
    - [SmsResp](#jmash-sms-SmsResp)
  
    - [SmsProvider](#jmash-sms-SmsProvider)
  
- [jmash/sms/protobuf/sms_sign_message.proto](#jmash_sms_protobuf_sms_sign_message-proto)
    - [SmsSignKey](#jmash-sms-SmsSignKey)
    - [SmsSignKeyList](#jmash-sms-SmsSignKeyList)
    - [SmsSignList](#jmash-sms-SmsSignList)
    - [SmsSignModel](#jmash-sms-SmsSignModel)
    - [SmsSignModelTotal](#jmash-sms-SmsSignModelTotal)
    - [SmsSignPage](#jmash-sms-SmsSignPage)
    - [SmsSignReq](#jmash-sms-SmsSignReq)
  
    - [AuditStatus](#jmash-sms-AuditStatus)
  
- [jmash/sms/protobuf/sms_template_message.proto](#jmash_sms_protobuf_sms_template_message-proto)
    - [SmsTemplateKey](#jmash-sms-SmsTemplateKey)
    - [SmsTemplateKeyList](#jmash-sms-SmsTemplateKeyList)
    - [SmsTemplateList](#jmash-sms-SmsTemplateList)
    - [SmsTemplateModel](#jmash-sms-SmsTemplateModel)
    - [SmsTemplateModelTotal](#jmash-sms-SmsTemplateModelTotal)
    - [SmsTemplatePage](#jmash-sms-SmsTemplatePage)
    - [SmsTemplateReq](#jmash-sms-SmsTemplateReq)
  
    - [TemplateType](#jmash-sms-TemplateType)
  
- [Scalar Value Types](#scalar-value-types)



<a name="jmash_sms_protobuf_sms_app_config_message-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## jmash/sms/protobuf/sms_app_config_message.proto



<a name="jmash-sms-SmsAppConfigCreateReq"></a>

### SmsAppConfigCreateReq
消息应用配置新增实体 .


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| request_id | [string](#string) |  | 用于检测重复请求的唯一字符串ID. |
| validate_only | [bool](#bool) |  | 如果为true，则表示给定的请求仅需要被检验，而不是被执行. |
| config_code | [string](#string) |  | 应用配置Code. |
| config_name | [string](#string) |  | 配置名称. |
| config_type | [ConfigType](#jmash-sms-ConfigType) |  | 配置类型. |
| provider | [SmsProvider](#jmash-sms-SmsProvider) |  | 服务厂商 |
| is_default | [bool](#bool) |  | 是否默认. |
| access_key_id | [string](#string) |  | 访问key. |
| access_key_secret | [string](#string) |  | 访问密钥. |
| account_name | [string](#string) |  | 邮件账户名. |






<a name="jmash-sms-SmsAppConfigKey"></a>

### SmsAppConfigKey
消息应用配置主键 .


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| config_code | [string](#string) |  | 应用配置Code. |






<a name="jmash-sms-SmsAppConfigKeyList"></a>

### SmsAppConfigKeyList
消息应用配置List.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| config_code | [string](#string) | repeated | 应用配置Code. |






<a name="jmash-sms-SmsAppConfigList"></a>

### SmsAppConfigList
消息应用配置列表.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [SmsAppConfigModel](#jmash-sms-SmsAppConfigModel) | repeated | 当前页内容. |






<a name="jmash-sms-SmsAppConfigModel"></a>

### SmsAppConfigModel
消息应用配置实体 .


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| config_code | [string](#string) |  | 应用配置Code. |
| config_name | [string](#string) |  | 配置名称. |
| config_type | [ConfigType](#jmash-sms-ConfigType) |  | 消息类型. |
| provider | [SmsProvider](#jmash-sms-SmsProvider) |  | 服务厂商 |
| is_default | [bool](#bool) |  | 是否默认. |
| access_key_id | [string](#string) |  | 访问key. |
| create_by | [string](#string) |  | 创建人. |
| create_time | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  | 创建时间. |
| account_name | [string](#string) |  | 邮件账户名. |






<a name="jmash-sms-SmsAppConfigModelTotal"></a>

### SmsAppConfigModelTotal
合计 .


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| total_size | [int32](#int32) |  | 总记录数. |






<a name="jmash-sms-SmsAppConfigPage"></a>

### SmsAppConfigPage
消息应用配置分页 .


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [SmsAppConfigModel](#jmash-sms-SmsAppConfigModel) | repeated | 当前页内容. |
| cur_page | [int32](#int32) |  | 当前页码. |
| page_size | [int32](#int32) |  | 页尺寸. |
| total_size | [int32](#int32) |  | 总记录数. |
| page_count | [int32](#int32) |  | 总页数 |
| sub_total_dto | [SmsAppConfigModelTotal](#jmash-sms-SmsAppConfigModelTotal) |  | 本页小计. |
| total_dto | [SmsAppConfigModelTotal](#jmash-sms-SmsAppConfigModelTotal) |  | 合计. |






<a name="jmash-sms-SmsAppConfigReq"></a>

### SmsAppConfigReq
消息应用配置查询.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| cur_page | [int32](#int32) |  | 当前页码. |
| page_size | [int32](#int32) |  | 页尺寸. |
| order_name | [string](#string) |  | 排序名称. |
| order_asc | [bool](#bool) |  | 是否升序排序. |
| has_is_default | [bool](#bool) |  | 是否包含默认 |
| is_default | [bool](#bool) |  | 是否默认. |
| has_provider | [bool](#bool) |  | 是否指定服务厂商 |
| provider | [SmsProvider](#jmash-sms-SmsProvider) |  | 服务厂商 |
| has_config_type | [bool](#bool) |  | 是否包含消息类型. |
| config_type | [ConfigType](#jmash-sms-ConfigType) |  | 消息类型. |






<a name="jmash-sms-SmsAppConfigUpdateReq"></a>

### SmsAppConfigUpdateReq
消息应用配置修改实体 .


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  |  |
| request_id | [string](#string) |  | 用于检测重复请求的唯一字符串ID. |
| validate_only | [bool](#bool) |  | 如果为true，则表示给定的请求仅需要被检验，而不是被执行. |
| update_mask | [google.protobuf.FieldMask](#google-protobuf-FieldMask) |  | 更新字段掩码. |
| config_code | [string](#string) |  | 应用配置Code. |
| config_name | [string](#string) |  | 配置名称. |
| config_type | [ConfigType](#jmash-sms-ConfigType) |  | 配置类型. |
| provider | [SmsProvider](#jmash-sms-SmsProvider) |  | 服务厂商 |
| is_default | [bool](#bool) |  | 是否默认. |
| access_key_id | [string](#string) |  | 访问key. |
| access_key_secret | [string](#string) |  | 访问密钥. |
| account_name | [string](#string) |  | 邮件账户名. |





 


<a name="jmash-sms-ConfigType"></a>

### ConfigType
消息类型

| Name | Number | Description |
| ---- | ------ | ----------- |
| SMS | 0 | 短消息 |
| EMAIL | 1 | 邮件 |


 

 

 



<a name="jmash_sms_protobuf_sms_message-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## jmash/sms/protobuf/sms_message.proto



<a name="jmash-sms-EmailBasicReq"></a>

### EmailBasicReq
发送邮件


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| email | [string](#string) |  | 电子邮件 |
| subject | [string](#string) |  | 主题 |
| html_body | [string](#string) |  | 邮件 html 正文 |
| text_body | [string](#string) |  | 邮件 text 正文 |
| reply_address | [string](#string) |  | 回信地址(可选) |
| has_provider | [bool](#bool) |  | 是否指定服务厂商 |
| provider | [SmsProvider](#jmash-sms-SmsProvider) |  | 服务厂商 |
| config_code | [string](#string) |  | 应用配置编码 |






<a name="jmash-sms-EmailCaptchaReq"></a>

### EmailCaptchaReq
发送邮件验证码


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| account_name | [string](#string) |  | 账户名 |
| email | [string](#string) |  | 电子邮件 |
| captcha | [string](#string) |  | 验证码 |
| product | [string](#string) |  | 产品名称 |
| has_provider | [bool](#bool) |  | 是否指定服务厂商 |
| provider | [SmsProvider](#jmash-sms-SmsProvider) |  | 服务厂商 |
| config_code | [string](#string) |  | 应用配置编码 |






<a name="jmash-sms-SmsBasicReq"></a>

### SmsBasicReq
发送短信(通用)


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| mobile_phone | [string](#string) |  | 手机号 |
| sms_sign | [string](#string) |  | 短信签名 |
| template_code | [string](#string) |  | 短信模板Code |
| params | [SmsBasicReq.ParamsEntry](#jmash-sms-SmsBasicReq-ParamsEntry) | repeated | 参数 |
| has_provider | [bool](#bool) |  | 是否指定服务厂商 |
| provider | [SmsProvider](#jmash-sms-SmsProvider) |  | 服务厂商 |
| config_code | [string](#string) |  | 应用配置编码 |






<a name="jmash-sms-SmsBasicReq-ParamsEntry"></a>

### SmsBasicReq.ParamsEntry



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| key | [string](#string) |  |  |
| value | [string](#string) |  |  |






<a name="jmash-sms-SmsCaptchaReq"></a>

### SmsCaptchaReq
发送短信验证码


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| mobile_phone | [string](#string) |  | 手机号 |
| sms_sign | [string](#string) |  | 短信签名 |
| captcha | [string](#string) |  | 验证码 |
| product | [string](#string) |  | 产品名称 |
| has_provider | [bool](#bool) |  | 是否指定服务厂商 |
| provider | [SmsProvider](#jmash-sms-SmsProvider) |  | 服务厂商 |
| config_code | [string](#string) |  | 应用配置编码 |






<a name="jmash-sms-SmsResp"></a>

### SmsResp
发送结果.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| success | [bool](#bool) |  | 是否成功 |
| reason | [string](#string) |  | 测试通道，返回短信内容/发送失败显示原因 |





 


<a name="jmash-sms-SmsProvider"></a>

### SmsProvider
服务厂商

| Name | Number | Description |
| ---- | ------ | ----------- |
| Test | 0 |  |
| Aliyun | 1 |  |


 

 

 



<a name="jmash_sms_protobuf_sms_sign_message-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## jmash/sms/protobuf/sms_sign_message.proto



<a name="jmash-sms-SmsSignKey"></a>

### SmsSignKey
短信签名主键 .


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| sign_id | [string](#string) |  | 签名ID. |






<a name="jmash-sms-SmsSignKeyList"></a>

### SmsSignKeyList
短信签名List.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| sign_id | [string](#string) | repeated | 签名ID. |






<a name="jmash-sms-SmsSignList"></a>

### SmsSignList
短信签名列表.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [SmsSignModel](#jmash-sms-SmsSignModel) | repeated | 当前页内容. |






<a name="jmash-sms-SmsSignModel"></a>

### SmsSignModel
短信签名实体 .


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| sign_id | [string](#string) |  | 签名ID. |
| sign_name | [string](#string) |  | 签名名称. |
| config_code | [string](#string) |  | 应用配置code. |
| order_id | [string](#string) |  | 工单号. |
| business_type | [string](#string) |  | 验证码类型. |
| use_scene | [string](#string) |  | 适用场景. |
| audit_status | [AuditStatus](#jmash-sms-AuditStatus) |  | 审核状态. |
| create_by | [string](#string) |  | 创建人. |
| create_time | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  | 创建时间. |






<a name="jmash-sms-SmsSignModelTotal"></a>

### SmsSignModelTotal
合计 .


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| total_size | [int32](#int32) |  | 总记录数. |






<a name="jmash-sms-SmsSignPage"></a>

### SmsSignPage
短信签名分页 .


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [SmsSignModel](#jmash-sms-SmsSignModel) | repeated | 当前页内容. |
| cur_page | [int32](#int32) |  | 当前页码. |
| page_size | [int32](#int32) |  | 页尺寸. |
| total_size | [int32](#int32) |  | 总记录数. |
| page_count | [int32](#int32) |  | 总页数 |
| sub_total_dto | [SmsSignModelTotal](#jmash-sms-SmsSignModelTotal) |  | 本页小计. |
| total_dto | [SmsSignModelTotal](#jmash-sms-SmsSignModelTotal) |  | 合计. |






<a name="jmash-sms-SmsSignReq"></a>

### SmsSignReq
短信签名查询.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| cur_page | [int32](#int32) |  | 当前页码. |
| page_size | [int32](#int32) |  | 页尺寸. |
| order_name | [string](#string) |  | 排序名称. |
| order_asc | [bool](#bool) |  | 是否升序排序. |
| config_code | [string](#string) |  | 应用配置Code. |





 


<a name="jmash-sms-AuditStatus"></a>

### AuditStatus
审核状态

| Name | Number | Description |
| ---- | ------ | ----------- |
| AUDIT_STATE_INIT | 0 | 审核中 |
| AUDIT_STATE_PASS | 1 | 审核通过 |
| AUDIT_STATE_NOT_PASS | 2 | 审核未通过 |
| AUDIT_STATE_CANCEL | 3 | 取消审核 |


 

 

 



<a name="jmash_sms_protobuf_sms_template_message-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## jmash/sms/protobuf/sms_template_message.proto



<a name="jmash-sms-SmsTemplateKey"></a>

### SmsTemplateKey
短消息模板主键 .


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| template_id | [string](#string) |  | 模板ID. |






<a name="jmash-sms-SmsTemplateKeyList"></a>

### SmsTemplateKeyList
短消息模板List.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| template_id | [string](#string) | repeated | 模板ID. |






<a name="jmash-sms-SmsTemplateList"></a>

### SmsTemplateList
短消息模板列表.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [SmsTemplateModel](#jmash-sms-SmsTemplateModel) | repeated | 当前页内容. |






<a name="jmash-sms-SmsTemplateModel"></a>

### SmsTemplateModel
短消息模板实体 .


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| template_id | [string](#string) |  | 模板ID. |
| template_code | [string](#string) |  | 模板CODE. |
| template_name | [string](#string) |  | 模板名称. |
| config_code | [string](#string) |  | 应用配置code. |
| order_id | [string](#string) |  | 工单号. |
| template_content | [string](#string) |  | 模板内容. |
| template_type | [TemplateType](#jmash-sms-TemplateType) |  | 模板类型. |
| audit_status | [AuditStatus](#jmash-sms-AuditStatus) |  | 审核状态. |
| create_by | [string](#string) |  | 创建人. |
| create_time | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  | 创建时间. |
| template_params | [string](#string) | repeated | 模板参数. |






<a name="jmash-sms-SmsTemplateModelTotal"></a>

### SmsTemplateModelTotal
合计 .


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| total_size | [int32](#int32) |  | 总记录数. |






<a name="jmash-sms-SmsTemplatePage"></a>

### SmsTemplatePage
短消息模板分页 .


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [SmsTemplateModel](#jmash-sms-SmsTemplateModel) | repeated | 当前页内容. |
| cur_page | [int32](#int32) |  | 当前页码. |
| page_size | [int32](#int32) |  | 页尺寸. |
| total_size | [int32](#int32) |  | 总记录数. |
| page_count | [int32](#int32) |  | 总页数 |
| sub_total_dto | [SmsTemplateModelTotal](#jmash-sms-SmsTemplateModelTotal) |  | 本页小计. |
| total_dto | [SmsTemplateModelTotal](#jmash-sms-SmsTemplateModelTotal) |  | 合计. |






<a name="jmash-sms-SmsTemplateReq"></a>

### SmsTemplateReq
短消息模板查询.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| cur_page | [int32](#int32) |  | 当前页码. |
| page_size | [int32](#int32) |  | 页尺寸. |
| order_name | [string](#string) |  | 排序名称. |
| order_asc | [bool](#bool) |  | 是否升序排序. |
| config_code | [string](#string) |  | 应用配置Code. |
| has_template_type | [bool](#bool) |  | 是否包含模板类型 |
| template_type | [TemplateType](#jmash-sms-TemplateType) |  | 模板类型. |





 


<a name="jmash-sms-TemplateType"></a>

### TemplateType
模板类型

| Name | Number | Description |
| ---- | ------ | ----------- |
| Notify | 0 | 0: 短信通知 |
| Promote | 1 | 1: 推广短信 |
| Captcha | 2 | 2：验证码短信 |
| Inter3 | 3 | 3：国际/港澳台短信 |
| Inter6 | 6 | 6：国际/港澳台短信 |
| Digital | 7 | 7：数字短信 |


 

 

 



## Scalar Value Types

| .proto Type | Notes | C++ | Java | Python | Go | C# | PHP | Ruby |
| ----------- | ----- | --- | ---- | ------ | -- | -- | --- | ---- |
| <a name="double" /> double |  | double | double | float | float64 | double | float | Float |
| <a name="float" /> float |  | float | float | float | float32 | float | float | Float |
| <a name="int32" /> int32 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint32 instead. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="int64" /> int64 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint64 instead. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="uint32" /> uint32 | Uses variable-length encoding. | uint32 | int | int/long | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="uint64" /> uint64 | Uses variable-length encoding. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum or Fixnum (as required) |
| <a name="sint32" /> sint32 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int32s. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sint64" /> sint64 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int64s. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="fixed32" /> fixed32 | Always four bytes. More efficient than uint32 if values are often greater than 2^28. | uint32 | int | int | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="fixed64" /> fixed64 | Always eight bytes. More efficient than uint64 if values are often greater than 2^56. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum |
| <a name="sfixed32" /> sfixed32 | Always four bytes. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sfixed64" /> sfixed64 | Always eight bytes. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="bool" /> bool |  | bool | boolean | boolean | bool | bool | boolean | TrueClass/FalseClass |
| <a name="string" /> string | A string must always contain UTF-8 encoded or 7-bit ASCII text. | string | String | str/unicode | string | string | string | String (UTF-8) |
| <a name="bytes" /> bytes | May contain any arbitrary sequence of bytes. | string | ByteString | str | []byte | ByteString | string | String (ASCII-8BIT) |

