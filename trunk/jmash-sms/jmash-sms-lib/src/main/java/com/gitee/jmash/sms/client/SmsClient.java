
package com.gitee.jmash.sms.client;

import com.gitee.jmash.common.grpc.client.GrpcChannel;
import jmash.sms.SmsGrpc;

/** Sms Client . */
public class SmsClient {

  /** SmsBlockingStub. */
  public static SmsGrpc.SmsBlockingStub getSmsBlockingStub() {
    return SmsGrpc.newBlockingStub(SmsClientConfig.getManagedChannel())
        .withCallCredentials(GrpcChannel.getCallCredentials());
  }

  /** SmsFutureStub. */
  public static SmsGrpc.SmsFutureStub getSmsFutureStub() {
    return SmsGrpc.newFutureStub(SmsClientConfig.getManagedChannel())
        .withCallCredentials(GrpcChannel.getCallCredentials());
  }

  /** SmsStub. */
  public static SmsGrpc.SmsStub getSmsStub() {
    return SmsGrpc.newStub(SmsClientConfig.getManagedChannel())
        .withCallCredentials(GrpcChannel.getCallCredentials());
  }

}
