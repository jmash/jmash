
package com.gitee.jmash.sms.client;

import com.gitee.jmash.common.grpc.client.GrpcChannel;
import io.grpc.ManagedChannel;

/** sms Client Config . */
public class SmsClientConfig {

  protected static ManagedChannel channel = null;

  public static synchronized ManagedChannel getManagedChannel() {
    if (null != channel && !channel.isShutdown() && !channel.isTerminated()) {
      return channel;
    }
    // k8s环境获取后端服务,本地环境获取测试服务.
    channel = GrpcChannel.getServiceChannel("jmash-sms-service.jmash.svc.cluster.local");
    return channel;
  }

}
