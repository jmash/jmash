package com.gitee.jmash.sms;

import com.gitee.jmash.sms.entity.SmsAppConfigEntity;
import com.gitee.jmash.sms.impl.EmailBase;
import com.gitee.jmash.sms.impl.SmsBase;
import com.gitee.jmash.sms.impl.aliyun.EmailAliyunImpl;
import com.gitee.jmash.sms.impl.aliyun.SmsAliyunImpl;
import com.gitee.jmash.sms.impl.test.EmailTestImpl;
import com.gitee.jmash.sms.impl.test.SmsTestImpl;
import com.gitee.jmash.sms.service.SmsAppConfigRead;
import com.gitee.jmash.sms.service.SmsAppConfigWrite;
import com.gitee.jmash.sms.service.SmsSignRead;
import com.gitee.jmash.sms.service.SmsSignWrite;
import com.gitee.jmash.sms.service.SmsTemplateRead;
import com.gitee.jmash.sms.service.SmsTemplateWrite;
import jakarta.enterprise.inject.spi.CDI;
import jmash.sms.protobuf.SmsProvider;

/**
 * 模块服务工厂类.
 *
 * @author CGD
 */
public class SmsFactory {

  /**
   * 获取各通道实现.
   */
  public static SmsBase getSmsImpl(SmsAppConfigEntity appConfig) {
    if (SmsProvider.Aliyun.equals(appConfig.getProvider())) {
      return new SmsAliyunImpl(appConfig);
    } else {
      return new SmsTestImpl(appConfig);
    }
  }

  /**
   * 获取各通道实现.
   */
  public static EmailBase getEmailImpl(SmsAppConfigEntity appConfig) {
    if (SmsProvider.Aliyun.equals(appConfig.getProvider())) {
      return new EmailAliyunImpl(appConfig);
    } else {
      return new EmailTestImpl(appConfig);
    }
  }

  public static SmsAppConfigRead getSmsAppConfigRead(String tenant) {
    return CDI.current().select(SmsAppConfigRead.class).get().setTenant(tenant);
  }

  public static SmsSignRead getSmsSignRead(String tenant) {
    return CDI.current().select(SmsSignRead.class).get().setTenant(tenant);
  }

  public static SmsTemplateRead getSmsTemplateRead(String tenant) {
    return CDI.current().select(SmsTemplateRead.class).get().setTenant(tenant);
  }

  public static SmsAppConfigWrite getSmsAppConfigWrite(String tenant) {
    return CDI.current().select(SmsAppConfigWrite.class).get().setTenant(tenant);
  }

  public static SmsSignWrite getSmsSignWrite(String tenant) {
    return CDI.current().select(SmsSignWrite.class).get().setTenant(tenant);
  }

  public static SmsTemplateWrite getSmsTemplateWrite(String tenant) {
    return CDI.current().select(SmsTemplateWrite.class).get().setTenant(tenant);
  }

  /**
   * CDI回收实例.
   */
  public static void destroy(Object instance) {
    if (null != instance) {
      CDI.current().destroy(instance);
    }
  }
}
