
package com.gitee.jmash.sms;

import com.crenjoy.proto.mapper.BeanMapper;
import com.crenjoy.proto.mapper.ProtoMapper;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * Sms Mapper.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Mapper
public interface SmsMapper extends BeanMapper, ProtoMapper {

  SmsMapper INSTANCE = Mappers.getMapper(SmsMapper.class);

}
