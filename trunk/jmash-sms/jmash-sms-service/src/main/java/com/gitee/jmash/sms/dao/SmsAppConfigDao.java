
package com.gitee.jmash.sms.dao;

import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.orm.jpa.BaseDao;
import com.gitee.jmash.core.orm.jpa.SqlBuilder;
import com.gitee.jmash.core.orm.jpa.TenantEntityManager;
import com.gitee.jmash.sms.entity.SmsAppConfigEntity;
import com.gitee.jmash.sms.model.SmsAppConfigTotal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import jmash.sms.protobuf.SmsAppConfigReq;

/**
 * SmsAppConfig实体的Dao层（使用JPA实现）.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public class SmsAppConfigDao extends BaseDao<SmsAppConfigEntity, String> {

  public SmsAppConfigDao() {
    super();
  }

  public SmsAppConfigDao(TenantEntityManager tem) {
    super(tem);
  }


  /**
   * 查询记录数.
   */
  public Integer findCount(SmsAppConfigReq req) {
    SqlBuilder sqlBuilder = createSql(req);
    String query = sqlBuilder.getQuerySql("select count(s) ");
    Object obj = this.findSingleResultByParams(query, sqlBuilder.getParams());
    return (obj == null) ? 0 : Integer.valueOf(obj.toString());
  }

  /**
   * 综合查询.
   */
  public List<SmsAppConfigEntity> findListByReq(SmsAppConfigReq req) {
    SqlBuilder sqlBuilder = createSql(req);
    String query = sqlBuilder.getQuerySql("select s ");
    return this.findListByParams(query, sqlBuilder.getParams());
  }

  /**
   * 综合查询Page.
   */
  public DtoPage<SmsAppConfigEntity, SmsAppConfigTotal> findPageByReq(SmsAppConfigReq req) {
    SqlBuilder sqlBuilder = createSql(req);
    String query = sqlBuilder.getQuerySql("select s ");
    String totalQuery = sqlBuilder.getTotalSql("select count(s) as totalSize ");
    return this.findDtoPageByParams(req.getCurPage(), req.getPageSize(), query, totalQuery,
        SmsAppConfigTotal.class, sqlBuilder.getParams());
  }

  /**
   * Create SQL By Req .
   */
  public SqlBuilder createSql(SmsAppConfigReq req) {
    StringBuilder sql = new StringBuilder(" from SmsAppConfigEntity s where 1=1  ");
    Map<String, Object> params = new HashMap<String, Object>();
    if (req.getHasConfigType()) {
      sql.append(" and s.configType = :configType ");
      params.put("configType", req.getConfigType());
    }

    if (req.getHasIsDefault()) {
      sql.append(" and s.isDefault = :isDefault");
      params.put("isDefault", req.getIsDefault());
    }

    if (req.getHasProvider()) {
      sql.append(" and s.provider = :provider");
      params.put("provider", req.getProvider());
    }

    String orderSql = " order by s.isDefault desc , s.createTime desc ";
    if (StringUtils.isNotBlank(req.getOrderName())) {
      orderSql = String.format(" order by %s %s ", req.getOrderName(),
          req.getOrderAsc() ? " asc " : " desc ");
    }
    return SqlBuilder.build().setSql(sql).setParams(params).setOrderSql(orderSql);
  }


}
