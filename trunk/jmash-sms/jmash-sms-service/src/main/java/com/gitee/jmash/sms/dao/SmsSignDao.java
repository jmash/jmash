
package com.gitee.jmash.sms.dao;

import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.orm.jpa.BaseDao;
import com.gitee.jmash.core.orm.jpa.SqlBuilder;
import com.gitee.jmash.core.orm.jpa.TenantEntityManager;
import com.gitee.jmash.sms.entity.SmsSignEntity;
import com.gitee.jmash.sms.model.SmsSignTotal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.apache.commons.lang3.StringUtils;
import jmash.sms.protobuf.SmsSignReq;

/**
 * SmsSign实体的Dao层（使用JPA实现）.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public class SmsSignDao extends BaseDao<SmsSignEntity, UUID> {

  public SmsSignDao() {
    super();
  }

  public SmsSignDao(TenantEntityManager tem) {
    super(tem);
  }


  public SmsSignEntity findBy(String configCode, String signName) {
    return this.findSingle(
        "select s from SmsSignEntity s where s.configCode = ?1  and s.signName = ?2", configCode,
        signName);
  }

  /**
   * 查询记录数.
   */
  public Integer findCount(SmsSignReq req) {
    SqlBuilder sqlBuilder = createSql(req);
    String query = sqlBuilder.getQuerySql("select count(s) ");
    Object obj = this.findSingleResultByParams(query, sqlBuilder.getParams());
    return (obj == null) ? 0 : Integer.valueOf(obj.toString());
  }

  /**
   * 综合查询.
   */
  public List<SmsSignEntity> findListByReq(SmsSignReq req) {
    SqlBuilder sqlBuilder = createSql(req);
    String query = sqlBuilder.getQuerySql("select s ");
    return this.findListByParams(query, sqlBuilder.getParams());
  }

  /**
   * 综合查询Page.
   */
  public DtoPage<SmsSignEntity, SmsSignTotal> findPageByReq(SmsSignReq req) {
    SqlBuilder sqlBuilder = createSql(req);
    String query = sqlBuilder.getQuerySql("select s ");
    String totalQuery = sqlBuilder.getTotalSql("select count(s) as totalSize ");
    return this.findDtoPageByParams(req.getCurPage(), req.getPageSize(), query, totalQuery,
        SmsSignTotal.class, sqlBuilder.getParams());
  }

  /**
   * Create SQL By Req .
   */
  public SqlBuilder createSql(SmsSignReq req) {
    StringBuilder sql = new StringBuilder(" from SmsSignEntity s where 1=1  ");
    Map<String, Object> params = new HashMap<String, Object>();

    if (StringUtils.isNotBlank(req.getConfigCode())) {
      sql.append(" and s.configCode = :configCode");
      params.put("configCode", req.getConfigCode());
    }

    String orderSql = " order by s.createTime desc ";
    if (StringUtils.isNotBlank(req.getOrderName())) {
      orderSql = String.format(" order by %s %s ", req.getOrderName(),
          req.getOrderAsc() ? " asc " : " desc ");
    }

    return SqlBuilder.build().setSql(sql).setParams(params).setOrderSql(orderSql);
  }


}
