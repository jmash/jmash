
package com.gitee.jmash.sms.dao;

import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.orm.jpa.BaseDao;
import com.gitee.jmash.core.orm.jpa.SqlBuilder;
import com.gitee.jmash.core.orm.jpa.TenantEntityManager;
import com.gitee.jmash.sms.entity.SmsTemplateEntity;
import com.gitee.jmash.sms.model.SmsTemplateTotal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.apache.commons.lang3.StringUtils;
import jmash.sms.protobuf.SmsTemplateReq;

/**
 * SmsTemplate实体的Dao层（使用JPA实现）.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public class SmsTemplateDao extends BaseDao<SmsTemplateEntity, UUID> {

  public SmsTemplateDao() {
    super();
  }

  public SmsTemplateDao(TenantEntityManager tem) {
    super(tem);
  }

  public SmsTemplateEntity findBy(String configCode, String templateCode) {
    return this.findSingle(
        "select s from SmsTemplateEntity s where s.configCode = ?1  and s.templateCode = ?2",
        configCode, templateCode);
  }


  /**
   * 查询记录数.
   */
  public Integer findCount(SmsTemplateReq req) {
    SqlBuilder sqlBuilder = createSql(req);
    String query = sqlBuilder.getQuerySql("select count(s) ");
    Object obj = this.findSingleResultByParams(query, sqlBuilder.getParams());
    return (obj == null) ? 0 : Integer.valueOf(obj.toString());
  }

  /**
   * 综合查询.
   */
  public List<SmsTemplateEntity> findListByReq(SmsTemplateReq req) {
    SqlBuilder sqlBuilder = createSql(req);
    String query = sqlBuilder.getQuerySql("select s ");
    return this.findListByParams(query, sqlBuilder.getParams());
  }

  /**
   * 综合查询Page.
   */
  public DtoPage<SmsTemplateEntity, SmsTemplateTotal> findPageByReq(SmsTemplateReq req) {
    SqlBuilder sqlBuilder = createSql(req);
    String query = sqlBuilder.getQuerySql("select s ");
    String totalQuery = sqlBuilder.getTotalSql("select count(s) as totalSize ");
    return this.findDtoPageByParams(req.getCurPage(), req.getPageSize(), query, totalQuery,
        SmsTemplateTotal.class, sqlBuilder.getParams());
  }

  /**
   * Create SQL By Req .
   */
  public SqlBuilder createSql(SmsTemplateReq req) {
    StringBuilder sql = new StringBuilder(" from SmsTemplateEntity s where 1=1  ");
    Map<String, Object> params = new HashMap<String, Object>();

    if (StringUtils.isNotBlank(req.getConfigCode())) {
      sql.append(" and s.configCode = :configCode");
      params.put("configCode", req.getConfigCode());
    }

    if (req.getHasTemplateType()) {
      sql.append(" and s.templateType = :templateType");
      params.put("templateType", req.getTemplateType());
    }

    String orderSql = " order by s.createTime desc ";
    if (StringUtils.isNotBlank(req.getOrderName())) {
      orderSql = String.format(" order by %s %s ", req.getOrderName(),
          req.getOrderAsc() ? " asc " : " desc ");
    }

    return SqlBuilder.build().setSql(sql).setParams(params).setOrderSql(orderSql);
  }


}
