
package com.gitee.jmash.sms.entity;


import com.gitee.jmash.core.orm.jpa.CryptoConverter;
import com.gitee.jmash.core.orm.jpa.entity.CreateEntity;
import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.persistence.Column;
import jakarta.persistence.Convert;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import jmash.sms.protobuf.ConfigType;
import jmash.sms.protobuf.SmsProvider;


/**
 * 短信模块 消息应用配置表 .
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Entity
@Table(name = "sms_app_config")
public class SmsAppConfigEntity extends CreateEntity<String> {

  private static final long serialVersionUID = 1L;

  /**
   * 应用配置Code.
   */
  private String configCode;
  /**
   * 配置名称.
   */
  private String configName;
  /**
   * 配置类型.
   */
  private ConfigType configType;
  /**
   * 服务厂商.
   */
  private SmsProvider provider;
  /**
   * 是否默认.
   */
  private Boolean isDefault = false;
  /**
   * 访问key.
   */
  private String accessKeyId;
  /**
   * 访问密钥.
   */
  private String accessKeySecret;
  /**
   * 邮件账户名.
   */
  private String accountName;

  /**
   * 默认构造函数.
   */
  public SmsAppConfigEntity() {
    super();
  }

  /**
   * 实体主键.
   */
  @Transient
  @JsonbTransient
  public String getEntityPk() {
    return this.configCode;
  }

  /**
   * 实体主键.
   */
  public void setEntityPk(String pk) {
    this.configCode = pk;
  }

  /**
   * 应用配置Code(ConfigCode).
   */
  @Id
  @Column(name = "config_code", nullable = false)
  public String getConfigCode() {
    return configCode;
  }

  /**
   * 应用配置Code(ConfigCode).
   */
  public void setConfigCode(String configCode) {
    this.configCode = configCode;
  }

  /**
   * 配置名称(ConfigName).
   */
  @Column(name = "config_name", nullable = false)
  public String getConfigName() {
    return configName;
  }

  /**
   * 配置名称(ConfigName).
   */
  public void setConfigName(String configName) {
    this.configName = configName;
  }

  /**
   * 配置类型(ConfigType).
   */
  @Column(name = "config_type", nullable = false)
  @Enumerated(EnumType.STRING)
  public ConfigType getConfigType() {
    return configType;
  }

  /**
   * 配置类型(ConfigType).
   */
  public void setConfigType(ConfigType configType) {
    this.configType = configType;
  }

  @Column(name = "provider_", nullable = false)
  @Enumerated(EnumType.STRING)
  public SmsProvider getProvider() {
    return provider;
  }

  public void setProvider(SmsProvider provider) {
    this.provider = provider;
  }

  /**
   * 是否默认(IsDefault).
   */
  @Column(name = "is_default", nullable = false)
  public Boolean getIsDefault() {
    return isDefault;
  }

  /**
   * 是否默认(IsDefault).
   */
  public void setIsDefault(Boolean isDefault) {
    this.isDefault = isDefault;
  }

  /**
   * 访问key(AccessKeyId).
   */
  @Column(name = "access_key_id")
  public String getAccessKeyId() {
    return accessKeyId;
  }

  /**
   * 访问key(AccessKeyId).
   */
  public void setAccessKeyId(String accessKeyId) {
    this.accessKeyId = accessKeyId;
  }

  /**
   * 访问密钥(AccessKeySecret).
   */
  @Column(name = "access_key_secret")
  @Convert(converter = CryptoConverter.class)
  public String getAccessKeySecret() {
    return accessKeySecret;
  }

  /**
   * 访问密钥(AccessKeySecret).
   */
  public void setAccessKeySecret(String accessKeySecret) {
    this.accessKeySecret = accessKeySecret;
  }

  /**
   * 邮件账户名(AccountName).
   */
  @Column(name = "account_name")
  public String getAccountName() {
    return accountName;
  }

  /**
   * 邮件账户名(AccountName).
   */
  public void setAccountName(String accountName) {
    this.accountName = accountName;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((configCode == null) ? 0 : configCode.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final SmsAppConfigEntity other = (SmsAppConfigEntity) obj;
    if (configCode == null) {
      if (other.configCode != null) {
        return false;
      }
    } else if (!configCode.equals(other.configCode)) {
      return false;
    }
    return true;
  }


}
