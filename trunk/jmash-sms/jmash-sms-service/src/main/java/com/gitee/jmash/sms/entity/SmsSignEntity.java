
package com.gitee.jmash.sms.entity;


import com.gitee.jmash.core.orm.jpa.entity.CreateEntity;
import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import java.util.UUID;
import jmash.sms.protobuf.AuditStatus;


/**
 * 短信模块 短信签名表 .
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Entity
@Table(name = "sms_sign")

public class SmsSignEntity extends CreateEntity<UUID> {

  private static final long serialVersionUID = 1L;

  /**
   * 签名ID.
   */
  private UUID signId = UUID.randomUUID();
  /**
   * 签名名称.
   */
  private String signName;
  /**
   * 应用配置code.
   */
  private String configCode;
  /**
   * 工单号.
   */
  private String orderId;
  /**
   * 验证码类型.
   */
  private String businessType;
  /**
   * 适用场景.
   */
  private String useScene;
  /**
   * 审核状态.
   */
  private AuditStatus auditStatus;

  /**
   * 默认构造函数.
   */
  public SmsSignEntity() {
    super();
  }

  /**
   * 实体主键.
   */
  @Transient
  @JsonbTransient
  public UUID getEntityPk() {
    return this.signId;
  }

  /**
   * 实体主键.
   */
  public void setEntityPk(UUID pk) {
    this.signId = pk;
  }

  /**
   * 签名ID(SignId).
   */
  @Id
  @Column(name = "sign_id", nullable = false, columnDefinition = "BINARY(16)")
  public UUID getSignId() {
    return signId;
  }

  /**
   * 签名ID(SignId).
   */
  public void setSignId(UUID signId) {
    this.signId = signId;
  }

  /**
   * 签名名称(SignName).
   */
  @Column(name = "sign_name", nullable = false)
  public String getSignName() {
    return signName;
  }

  /**
   * 签名名称(SignName).
   */
  public void setSignName(String signName) {
    this.signName = signName;
  }

  /**
   * 应用配置code(ConfigCode).
   */
  @Column(name = "config_code", nullable = false)
  public String getConfigCode() {
    return configCode;
  }

  /**
   * 应用配置code(ConfigCode).
   */
  public void setConfigCode(String configCode) {
    this.configCode = configCode;
  }

  /**
   * 工单号(OrderId).
   */
  @Column(name = "order_id")
  public String getOrderId() {
    return orderId;
  }

  /**
   * 工单号(OrderId).
   */
  public void setOrderId(String orderId) {
    this.orderId = orderId;
  }

  /**
   * 验证码类型(BusinessType).
   */
  @Column(name = "business_type", nullable = false)
  public String getBusinessType() {
    return businessType;
  }

  /**
   * 验证码类型(BusinessType).
   */
  public void setBusinessType(String businessType) {
    this.businessType = businessType;
  }

  /**
   * 适用场景(UseScene).
   */
  @Column(name = "use_scene")
  public String getUseScene() {
    return useScene;
  }

  /**
   * 适用场景(UseScene).
   */
  public void setUseScene(String useScene) {
    this.useScene = useScene;
  }

  /**
   * 审核状态(AuditStatus).
   */
  @Column(name = "audit_status", nullable = false)
  @Enumerated(EnumType.STRING)
  public AuditStatus getAuditStatus() {
    return auditStatus;
  }

  /**
   * 审核状态(AuditStatus).
   */
  public void setAuditStatus(AuditStatus auditStatus) {
    this.auditStatus = auditStatus;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((signId == null) ? 0 : signId.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final SmsSignEntity other = (SmsSignEntity) obj;
    if (signId == null) {
      if (other.signId != null) {
        return false;
      }
    } else if (!signId.equals(other.signId)) {
      return false;
    }
    return true;
  }


}
