
package com.gitee.jmash.sms.entity;


import com.gitee.jmash.core.orm.jpa.entity.CreateEntity;
import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import java.util.UUID;
import jmash.sms.protobuf.AuditStatus;
import jmash.sms.protobuf.TemplateType;


/**
 * 短信模块 短消息模板表 .
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Entity
@Table(name = "sms_template")

public class SmsTemplateEntity extends CreateEntity<UUID> {

  private static final long serialVersionUID = 1L;

  /**
   * 模板ID.
   */
  private UUID templateId = UUID.randomUUID();
  /**
   * 模板CODE.
   */
  private String templateCode;
  /**
   * 模板名称.
   */
  private String templateName;
  /**
   * 应用配置code.
   */
  private String configCode;
  /**
   * 工单号.
   */
  private String orderId;
  /**
   * 模板内容.
   */
  private String templateContent;
  /**
   * 模板类型.
   */
  private TemplateType templateType = TemplateType.Notify;
  /**
   * 审核状态.
   */
  private AuditStatus auditStatus;

  /**
   * 默认构造函数.
   */
  public SmsTemplateEntity() {
    super();
  }

  /**
   * 实体主键.
   */
  @Transient
  @JsonbTransient
  public UUID getEntityPk() {
    return this.templateId;
  }

  /**
   * 实体主键.
   */
  public void setEntityPk(UUID pk) {
    this.templateId = pk;
  }

  @Id
  @Column(name = "template_id", nullable = false, columnDefinition = "BINARY(16)")
  public UUID getTemplateId() {
    return templateId;
  }

  public void setTemplateId(UUID templateId) {
    this.templateId = templateId;
  }

  /**
   * 模板CODE(TemplateCode).
   */
  @Column(name = "template_code", nullable = false)
  public String getTemplateCode() {
    return templateCode;
  }

  /**
   * 模板CODE(TemplateCode).
   */
  public void setTemplateCode(String templateCode) {
    this.templateCode = templateCode;
  }

  /**
   * 模板名称(TemplateName).
   */
  @Column(name = "template_name", nullable = false)
  public String getTemplateName() {
    return templateName;
  }

  /**
   * 模板名称(TemplateName).
   */
  public void setTemplateName(String templateName) {
    this.templateName = templateName;
  }

  /**
   * 应用配置code(ConfigCode).
   */
  @Column(name = "config_code", nullable = false)
  public String getConfigCode() {
    return configCode;
  }

  /**
   * 应用配置code(ConfigCode).
   */
  public void setConfigCode(String configCode) {
    this.configCode = configCode;
  }

  /**
   * 工单号(OrderId).
   */
  @Column(name = "order_id")
  public String getOrderId() {
    return orderId;
  }

  /**
   * 工单号(OrderId).
   */
  public void setOrderId(String orderId) {
    this.orderId = orderId;
  }

  /**
   * 模板内容(TemplateContent).
   */
  @Column(name = "template_content")
  public String getTemplateContent() {
    return templateContent;
  }

  /**
   * 模板内容(TemplateContent).
   */
  public void setTemplateContent(String templateContent) {
    this.templateContent = templateContent;
  }

  /**
   * 模板类型(TemplateType).
   */
  @Column(name = "template_type", nullable = false)
  @Enumerated(EnumType.ORDINAL)
  public TemplateType getTemplateType() {
    return templateType;
  }

  /**
   * 模板类型(TemplateType).
   */
  public void setTemplateType(TemplateType templateType) {
    this.templateType = templateType;
  }

  /**
   * 审核状态(AuditStatus).
   */
  @Column(name = "audit_status", nullable = false)
  @Enumerated(EnumType.STRING)
  public AuditStatus getAuditStatus() {
    return auditStatus;
  }

  /**
   * 审核状态(AuditStatus).
   */
  public void setAuditStatus(AuditStatus auditStatus) {
    this.auditStatus = auditStatus;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((templateCode == null) ? 0 : templateCode.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final SmsTemplateEntity other = (SmsTemplateEntity) obj;
    if (templateCode == null) {
      if (other.templateCode != null) {
        return false;
      }
    } else if (!templateCode.equals(other.templateCode)) {
      return false;
    }
    return true;
  }


}
