
package com.gitee.jmash.sms.grpc;

import com.gitee.jmash.common.utils.UUIDUtil;
import com.gitee.jmash.common.utils.VersionUtil;
import com.gitee.jmash.core.grpc.cdi.GrpcService;
import com.gitee.jmash.core.lib.ProtoEnumUtil;
import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.sms.SmsFactory;
import com.gitee.jmash.sms.entity.SmsAppConfigEntity;
import com.gitee.jmash.sms.entity.SmsSignEntity;
import com.gitee.jmash.sms.entity.SmsTemplateEntity;
import com.gitee.jmash.sms.impl.EmailBase;
import com.gitee.jmash.sms.impl.SmsBase;
import com.gitee.jmash.sms.mapper.SmsAppConfigMapper;
import com.gitee.jmash.sms.mapper.SmsSignMapper;
import com.gitee.jmash.sms.mapper.SmsTemplateMapper;
import com.gitee.jmash.sms.model.SmsAppConfigTotal;
import com.gitee.jmash.sms.model.SmsSignTotal;
import com.gitee.jmash.sms.model.SmsTemplateTotal;
import com.gitee.jmash.sms.service.SmsAppConfigRead;
import com.gitee.jmash.sms.service.SmsAppConfigWrite;
import com.gitee.jmash.sms.service.SmsSignRead;
import com.gitee.jmash.sms.service.SmsSignWrite;
import com.gitee.jmash.sms.service.SmsTemplateRead;
import com.gitee.jmash.sms.service.SmsTemplateWrite;
import com.google.protobuf.BoolValue;
import com.google.protobuf.Empty;
import com.google.protobuf.EnumValue;
import com.google.protobuf.Int32Value;
import com.google.protobuf.StringValue;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import io.grpc.Status;
import io.grpc.stub.StreamObserver;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import jmash.protobuf.CustomEnumValue;
import jmash.protobuf.CustomEnumValueMap;
import jmash.protobuf.Entry;
import jmash.protobuf.EntryList;
import jmash.protobuf.EnumEntryReq;
import jmash.protobuf.EnumValueList;
import jmash.sms.protobuf.ConfigType;
import jmash.sms.protobuf.EmailBasicReq;
import jmash.sms.protobuf.EmailCaptchaReq;
import jmash.sms.protobuf.SmsAppConfigCreateReq;
import jmash.sms.protobuf.SmsAppConfigKey;
import jmash.sms.protobuf.SmsAppConfigKeyList;
import jmash.sms.protobuf.SmsAppConfigList;
import jmash.sms.protobuf.SmsAppConfigModel;
import jmash.sms.protobuf.SmsAppConfigPage;
import jmash.sms.protobuf.SmsAppConfigReq;
import jmash.sms.protobuf.SmsAppConfigUpdateReq;
import jmash.sms.protobuf.SmsBasicReq;
import jmash.sms.protobuf.SmsCaptchaReq;
import jmash.sms.protobuf.SmsResp;
import jmash.sms.protobuf.SmsSignKey;
import jmash.sms.protobuf.SmsSignKeyList;
import jmash.sms.protobuf.SmsSignList;
import jmash.sms.protobuf.SmsSignModel;
import jmash.sms.protobuf.SmsSignPage;
import jmash.sms.protobuf.SmsSignReq;
import jmash.sms.protobuf.SmsTemplateKey;
import jmash.sms.protobuf.SmsTemplateKeyList;
import jmash.sms.protobuf.SmsTemplateList;
import jmash.sms.protobuf.SmsTemplateModel;
import jmash.sms.protobuf.SmsTemplatePage;
import jmash.sms.protobuf.SmsTemplateReq;

/**
 * Grpc服务实现.
 */
@GrpcService
public class SmsImpl extends jmash.sms.SmsGrpc.SmsImplBase {

  private static Log log = LogFactory.getLog(SmsImpl.class);

  // 模块版本
  public static final String version = "v1.0.0";

  @Override
  public void version(Empty request, StreamObserver<StringValue> responseObserver) {
    responseObserver.onNext(StringValue.of(version + "-" + VersionUtil.snapshot(SmsImpl.class)));
    responseObserver.onCompleted();
  }

  @Override
  public void findEnumList(StringValue request, StreamObserver<EnumValueList> responseObserver) {
    try {
      List<EnumValue> list = ProtoEnumUtil.getEnumList(request.getValue());
      responseObserver.onNext(EnumValueList.newBuilder().addAllValues(list).build());
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  public void findEnumMap(StringValue request,
      StreamObserver<CustomEnumValueMap> responseObserver) {
    try {
      Map<Integer, CustomEnumValue> values = ProtoEnumUtil.getEnumMap(request.getValue());
      responseObserver.onNext(CustomEnumValueMap.newBuilder().putAllValues(values).build());
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  public void findEnumEntry(EnumEntryReq request, StreamObserver<EntryList> responseObserver) {
    try {
      List<Entry> entryList =
          ProtoEnumUtil.getEnumCodeList(request.getClassName(), request.getType());
      responseObserver.onNext(EntryList.newBuilder().addAllValues(entryList).build());
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresAuthentication
  public void sendSms(SmsBasicReq request, StreamObserver<SmsResp> responseObserver) {
    try (SmsAppConfigRead smsAppConfigRead = SmsFactory.getSmsAppConfigRead(request.getTenant())) {
      SmsAppConfigEntity smsAppConfig = new SmsAppConfigEntity();
      if(StringUtils.isNotBlank(request.getConfigCode())) {
        smsAppConfig = smsAppConfigRead.findById(request.getConfigCode());
      } else {
        SmsAppConfigReq.Builder config = SmsAppConfigReq.newBuilder().setTenant(request.getTenant());
        config.setHasConfigType(true).setConfigType(ConfigType.SMS);
        config.setHasProvider(request.getHasProvider()).setProvider(request.getProvider());
        List<SmsAppConfigEntity> list = smsAppConfigRead.findListByReq(config.build());
        if (!list.isEmpty()) {
          smsAppConfig = list.get(0);
        }
      }
      if (StringUtils.isEmpty(smsAppConfig.getConfigCode())) {
        responseObserver.onNext(
            SmsResp.newBuilder().setReason("没有可用的短信服务,请联系管理员配置！").build());
      } else {
        SmsBase sms = SmsFactory.getSmsImpl(smsAppConfig);
        SmsResp resp = sms.sendSms(request);
        responseObserver.onNext(resp);
      }
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresAuthentication
  public void sendSmsCaptcha(SmsCaptchaReq request, StreamObserver<SmsResp> responseObserver) {
    try (SmsAppConfigRead smsAppConfigRead = SmsFactory.getSmsAppConfigRead(request.getTenant())) {
      SmsAppConfigEntity smsAppConfig = new SmsAppConfigEntity();
      if(StringUtils.isNotBlank(request.getConfigCode())) {
        smsAppConfig = smsAppConfigRead.findById(request.getConfigCode());
      } else {
        SmsAppConfigReq.Builder config = SmsAppConfigReq.newBuilder()
            .setTenant(request.getTenant());
        config.setHasConfigType(true).setConfigType(ConfigType.SMS);
        config.setHasProvider(request.getHasProvider()).setProvider(request.getProvider());
        List<SmsAppConfigEntity> list = smsAppConfigRead.findListByReq(config.build());
        if (!list.isEmpty()) {
          smsAppConfig = list.get(0);
        }
      }
      if (StringUtils.isEmpty(smsAppConfig.getConfigCode())) {
        responseObserver.onNext(
            SmsResp.newBuilder().setReason("没有可用的短信服务,请联系管理员配置！").build());
      } else {
        SmsBase sms = SmsFactory.getSmsImpl(smsAppConfig);
        SmsResp resp = sms.sendCaptcha(request);
        responseObserver.onNext(resp);
      }
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  public void sendEmail(EmailBasicReq request, StreamObserver<SmsResp> responseObserver) {
    try (SmsAppConfigRead smsAppConfigRead = SmsFactory.getSmsAppConfigRead(request.getTenant())) {
      SmsAppConfigEntity smsAppConfig = new SmsAppConfigEntity();
      if(StringUtils.isNotBlank(request.getConfigCode())) {
        smsAppConfig = smsAppConfigRead.findById(request.getConfigCode());
      } else {
        SmsAppConfigReq.Builder config = SmsAppConfigReq.newBuilder()
            .setTenant(request.getTenant());
        config.setHasConfigType(true).setConfigType(ConfigType.EMAIL);
        config.setHasProvider(request.getHasProvider()).setProvider(request.getProvider());
        List<SmsAppConfigEntity> list = smsAppConfigRead.findListByReq(config.build());
        if (!list.isEmpty()) {
          smsAppConfig = list.get(0);
        }
      }
      if (StringUtils.isEmpty(smsAppConfig.getConfigCode())) {
        responseObserver.onNext(
            SmsResp.newBuilder().setReason("没有可用的邮件服务,请联系管理员配置！").build());
      } else {
        EmailBase sms = SmsFactory.getEmailImpl(smsAppConfig);
        SmsResp resp = sms.sendEmail(request);
        responseObserver.onNext(resp);
      }
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  public void sendEmailCaptcha(EmailCaptchaReq request, StreamObserver<SmsResp> responseObserver) {
    try (SmsAppConfigRead smsAppConfigRead = SmsFactory.getSmsAppConfigRead(request.getTenant())) {
      SmsAppConfigEntity smsAppConfig = new SmsAppConfigEntity();
      if(StringUtils.isNotBlank(request.getConfigCode())) {
        smsAppConfig = smsAppConfigRead.findById(request.getConfigCode());
      } else {
        SmsAppConfigReq.Builder config = SmsAppConfigReq.newBuilder()
            .setTenant(request.getTenant());
        config.setHasConfigType(true).setConfigType(ConfigType.EMAIL);
        config.setHasProvider(request.getHasProvider()).setProvider(request.getProvider());
        List<SmsAppConfigEntity> list = smsAppConfigRead.findListByReq(config.build());
        if (!list.isEmpty()) {
          smsAppConfig = list.get(0);
        }
      }
      if (StringUtils.isEmpty(smsAppConfig.getConfigCode())) {
        responseObserver.onNext(
            SmsResp.newBuilder().setReason("没有可用的邮件服务,请联系管理员配置！").build());
      } else {
        EmailBase sms = SmsFactory.getEmailImpl(smsAppConfig);
        SmsResp resp = sms.sendEmailCaptcha(request);
        responseObserver.onNext(resp);
      }
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresAuthentication
  public void syncSmsSign(SmsAppConfigKey request, StreamObserver<BoolValue> responseObserver) {
    try (SmsAppConfigRead smsAppConfigRead = SmsFactory.getSmsAppConfigRead(request.getTenant());
        SmsSignWrite smsSignWrite = SmsFactory.getSmsSignWrite(request.getTenant())) {
      SmsAppConfigEntity entity = smsAppConfigRead.findById(request.getConfigCode());
      boolean result = smsSignWrite.syncSmsSign(entity);
      responseObserver.onNext(BoolValue.of(result));
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresAuthentication
  public void syncSmsTemplate(SmsAppConfigKey request, StreamObserver<BoolValue> responseObserver) {
    try (SmsAppConfigRead smsAppConfigRead = SmsFactory.getSmsAppConfigRead(request.getTenant());
        SmsTemplateWrite smsTemplateWrite = SmsFactory.getSmsTemplateWrite(request.getTenant())) {
      SmsAppConfigEntity entity = smsAppConfigRead.findById(request.getConfigCode());
      boolean result = smsTemplateWrite.syncSmsTemplate(entity);
      responseObserver.onNext(BoolValue.of(result));
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("sms:sms_app_config:list")
  public void findSmsAppConfigPage(SmsAppConfigReq request,
      StreamObserver<SmsAppConfigPage> responseObserver) {
    try (SmsAppConfigRead smsAppConfigRead = SmsFactory.getSmsAppConfigRead(request.getTenant())) {
      DtoPage<SmsAppConfigEntity, SmsAppConfigTotal> page = smsAppConfigRead.findPageByReq(request);
      SmsAppConfigPage modelPage = SmsAppConfigMapper.INSTANCE.pageSmsAppConfig(page);
      responseObserver.onNext(modelPage);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }


  @Override
  @RequiresPermissions("sms:sms_app_config:list")
  public void findSmsAppConfigList(SmsAppConfigReq request,
      StreamObserver<SmsAppConfigList> responseObserver) {
    try (SmsAppConfigRead smsAppConfigRead = SmsFactory.getSmsAppConfigRead(request.getTenant())) {
      List<SmsAppConfigEntity> list = smsAppConfigRead.findListByReq(request);
      List<SmsAppConfigModel> modelList = SmsAppConfigMapper.INSTANCE.listSmsAppConfig(list);
      responseObserver.onNext(SmsAppConfigList.newBuilder().addAllResults(modelList).build());
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("sms:sms_app_config:view")
  public void findSmsAppConfigById(SmsAppConfigKey request,
      StreamObserver<SmsAppConfigModel> responseObserver) {
    try (SmsAppConfigRead smsAppConfigRead = SmsFactory.getSmsAppConfigRead(request.getTenant())) {
      SmsAppConfigEntity entity = smsAppConfigRead.findById(request.getConfigCode());
      SmsAppConfigModel model = SmsAppConfigMapper.INSTANCE.model(entity);
      responseObserver.onNext(model);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }


  @Override
  @RequiresPermissions("sms:sms_app_config:add")
  public void createSmsAppConfig(SmsAppConfigCreateReq request,
      StreamObserver<SmsAppConfigModel> responseObserver) {
    try (SmsAppConfigWrite smsAppConfigWrite =
        SmsFactory.getSmsAppConfigWrite(request.getTenant())) {
      SmsAppConfigEntity entity = smsAppConfigWrite.insert(request);
      SmsAppConfigModel model = SmsAppConfigMapper.INSTANCE.model(entity);
      responseObserver.onNext(model);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("sms:sms_app_config:update")
  public void updateSmsAppConfig(SmsAppConfigUpdateReq request,
      StreamObserver<SmsAppConfigModel> responseObserver) {
    try (SmsAppConfigWrite smsAppConfigWrite =
        SmsFactory.getSmsAppConfigWrite(request.getTenant())) {
      SmsAppConfigEntity entity = smsAppConfigWrite.update(request);
      SmsAppConfigModel model = SmsAppConfigMapper.INSTANCE.model(entity);
      responseObserver.onNext(model);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("sms:sms_app_config:delete")
  public void deleteSmsAppConfig(SmsAppConfigKey request,
      StreamObserver<SmsAppConfigModel> responseObserver) {
    try (SmsAppConfigWrite smsAppConfigWrite =
        SmsFactory.getSmsAppConfigWrite(request.getTenant())) {
      SmsAppConfigEntity entity = smsAppConfigWrite.delete(request.getConfigCode());
      SmsAppConfigModel model = SmsAppConfigMapper.INSTANCE.model(entity);
      responseObserver.onNext(model);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("sms:sms_app_config:delete")
  public void batchDeleteSmsAppConfig(SmsAppConfigKeyList request,
      StreamObserver<Int32Value> responseObserver) {
    try (SmsAppConfigWrite smsAppConfigWrite =
        SmsFactory.getSmsAppConfigWrite(request.getTenant())) {
      final Set<String> set = request.getConfigCodeList().stream().collect(Collectors.toSet());

      int r = smsAppConfigWrite.batchDelete(set);
      responseObserver.onNext(Int32Value.of(r));
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("sms:sms_sign:list")
  public void findSmsSignPage(SmsSignReq request, StreamObserver<SmsSignPage> responseObserver) {
    try (SmsSignRead smsSignRead = SmsFactory.getSmsSignRead(request.getTenant())) {
      DtoPage<SmsSignEntity, SmsSignTotal> page = smsSignRead.findPageByReq(request);
      SmsSignPage modelPage = SmsSignMapper.INSTANCE.pageSmsSign(page);
      responseObserver.onNext(modelPage);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }


  @Override
  @RequiresPermissions("sms:sms_sign:list")
  public void findSmsSignList(SmsSignReq request, StreamObserver<SmsSignList> responseObserver) {
    try (SmsSignRead smsSignRead = SmsFactory.getSmsSignRead(request.getTenant())) {
      List<SmsSignEntity> list = smsSignRead.findListByReq(request);
      List<SmsSignModel> modelList = SmsSignMapper.INSTANCE.listSmsSign(list);
      responseObserver.onNext(SmsSignList.newBuilder().addAllResults(modelList).build());
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("sms:sms_sign:view")
  public void findSmsSignById(SmsSignKey request, StreamObserver<SmsSignModel> responseObserver) {
    try (SmsSignRead smsSignRead = SmsFactory.getSmsSignRead(request.getTenant())) {
      SmsSignEntity entity = smsSignRead.findById(UUIDUtil.fromString(request.getSignId()));
      SmsSignModel model = SmsSignMapper.INSTANCE.model(entity);
      responseObserver.onNext(model);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("sms:sms_sign:delete")
  public void deleteSmsSign(SmsSignKey request, StreamObserver<SmsSignModel> responseObserver) {
    try (SmsSignWrite smsSignWrite = SmsFactory.getSmsSignWrite(request.getTenant())) {
      SmsSignEntity entity = smsSignWrite.delete(UUIDUtil.fromString(request.getSignId()));
      SmsSignModel model = SmsSignMapper.INSTANCE.model(entity);
      responseObserver.onNext(model);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("sms:sms_sign:delete")
  public void batchDeleteSmsSign(SmsSignKeyList request,
      StreamObserver<Int32Value> responseObserver) {
    try (SmsSignWrite smsSignWrite = SmsFactory.getSmsSignWrite(request.getTenant())) {
      final List<String> list = request.getSignIdList();
      final Set<UUID> set =
          list.stream().map(v -> UUIDUtil.fromString(v)).collect(Collectors.toSet());

      int r = smsSignWrite.batchDelete(set);
      responseObserver.onNext(Int32Value.of(r));
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("sms:sms_template:list")
  public void findSmsTemplatePage(SmsTemplateReq request,
      StreamObserver<SmsTemplatePage> responseObserver) {
    try (SmsTemplateRead smsTemplateRead = SmsFactory.getSmsTemplateRead(request.getTenant())) {
      DtoPage<SmsTemplateEntity, SmsTemplateTotal> page = smsTemplateRead.findPageByReq(request);
      SmsTemplatePage modelPage = SmsTemplateMapper.INSTANCE.pageSmsTemplate(page);
      responseObserver.onNext(modelPage);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }


  @Override
  @RequiresPermissions("sms:sms_template:list")
  public void findSmsTemplateList(SmsTemplateReq request,
      StreamObserver<SmsTemplateList> responseObserver) {
    try (SmsTemplateRead smsTemplateRead = SmsFactory.getSmsTemplateRead(request.getTenant())) {
      List<SmsTemplateEntity> list = smsTemplateRead.findListByReq(request);
      List<SmsTemplateModel> modelList = SmsTemplateMapper.INSTANCE.listSmsTemplate(list);
      responseObserver.onNext(SmsTemplateList.newBuilder().addAllResults(modelList).build());
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("sms:sms_template:view")
  public void findSmsTemplateById(SmsTemplateKey request,
      StreamObserver<SmsTemplateModel> responseObserver) {
    try (SmsTemplateRead smsTemplateRead = SmsFactory.getSmsTemplateRead(request.getTenant())) {
      SmsTemplateEntity entity =
          smsTemplateRead.findById(UUIDUtil.fromString(request.getTemplateId()));
      SmsTemplateModel model = SmsTemplateMapper.INSTANCE.templateModel(entity,true);
      responseObserver.onNext(model);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("sms:sms_template:delete")
  public void deleteSmsTemplate(SmsTemplateKey request,
      StreamObserver<SmsTemplateModel> responseObserver) {
    try (SmsTemplateWrite smsTemplateWrite = SmsFactory.getSmsTemplateWrite(request.getTenant())) {
      SmsTemplateEntity entity =
          smsTemplateWrite.delete(UUIDUtil.fromString(request.getTemplateId()));
      SmsTemplateModel model = SmsTemplateMapper.INSTANCE.model(entity);
      responseObserver.onNext(model);
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }

  @Override
  @RequiresPermissions("sms:sms_template:delete")
  public void batchDeleteSmsTemplate(SmsTemplateKeyList request,
      StreamObserver<Int32Value> responseObserver) {
    try (SmsTemplateWrite smsTemplateWrite = SmsFactory.getSmsTemplateWrite(request.getTenant())) {
      final Set<UUID> set = request.getTemplateIdList().stream().map(v -> UUIDUtil.fromString(v))
          .collect(Collectors.toSet());

      int r = smsTemplateWrite.batchDelete(set);
      responseObserver.onNext(Int32Value.of(r));
      responseObserver.onCompleted();
    } catch (Exception ex) {
      log.error("", ex);
      responseObserver.onError(Status.INTERNAL.withDescription(ex.getMessage()).asException());
    }
  }
}
