
package com.gitee.jmash.sms.impl;


import com.gitee.jmash.sms.entity.SmsAppConfigEntity;
import jmash.sms.protobuf.EmailBasicReq;
import jmash.sms.protobuf.EmailCaptchaReq;
import jmash.sms.protobuf.SmsProvider;
import jmash.sms.protobuf.SmsResp;

/**
 * Email实现接口.
 *
 * @author CGD
 */
public abstract class EmailBase {

  protected SmsAppConfigEntity appConfig;

  public EmailBase(SmsAppConfigEntity appConfig) {
    super();
    this.appConfig = appConfig;
  }

  /**
   * 获取服务厂商.
   */
  public abstract SmsProvider getProvider();

  /**
   * 发送邮件.
   */
  public abstract SmsResp sendEmail(EmailBasicReq req);

  /**
   * 发送验证码.
   */
  public SmsResp sendEmailCaptcha(EmailCaptchaReq req) {
    String subject = String.format("%s 您的验证码", req.getProduct());
    String content =
        "尊敬的用户，\n" + "  为了验证您的邮箱地址，请在我们的网站上输入以下验证码：\n" + "  验证码：%s \n"
            + "\n"
            + "  此验证码将在接下来的15分钟内有效。请尽快使用该验证码完成验证流程。\n"
            + "  如果这不是您本人的操作，请忽略本邮件并检查您的账户是否存在任何未经授权的活动。\n"
            + "谢谢，\n" + "  %s 团队";
    content = String.format(content, req.getCaptcha(), req.getProduct());
    EmailBasicReq smsReq = EmailBasicReq.newBuilder()
        .setEmail(req.getEmail()).setSubject(subject)
        .setTextBody(content).build();
    return sendEmail(smsReq);
  }

}
