
package com.gitee.jmash.sms.impl;


import com.gitee.jmash.sms.entity.SmsAppConfigEntity;
import jmash.sms.protobuf.SmsBasicReq;
import jmash.sms.protobuf.SmsCaptchaReq;
import jmash.sms.protobuf.SmsProvider;
import jmash.sms.protobuf.SmsResp;

/**
 * Sms 短信实现接口.
 *
 * @author CGD
 */
public abstract class SmsBase {

  protected SmsAppConfigEntity appConfig;

  public SmsBase(SmsAppConfigEntity appConfig) {
    super();
    this.appConfig = appConfig;
  }

  /**
   * 获取服务厂商.
   */
  public abstract SmsProvider getProvider();

  /**
   * 发送短信.
   */
  public abstract SmsResp sendSms(SmsBasicReq req);

  /**
   * 发送验证码.
   */
  public SmsResp sendCaptcha(SmsCaptchaReq req) {
    SmsBasicReq smsReq =
        SmsBasicReq.newBuilder().setMobilePhone(req.getMobilePhone()).setProvider(req.getProvider())
            .setSmsSign(req.getSmsSign()).putParams("code", req.getCaptcha())
            .putParams("product", req.getProduct()).setTemplateCode("SMS_12370167").build();
    return sendSms(smsReq);
  }

}
