
package com.gitee.jmash.sms.impl.aliyun;


import com.aliyun.dm20151123.Client;
import com.aliyun.dm20151123.models.SingleSendMailRequest;
import com.aliyun.dm20151123.models.SingleSendMailResponse;
import com.aliyun.teaopenapi.models.Config;
import com.gitee.jmash.sms.entity.SmsAppConfigEntity;
import com.gitee.jmash.sms.impl.EmailBase;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import jmash.sms.protobuf.EmailBasicReq;
import jmash.sms.protobuf.SmsProvider;
import jmash.sms.protobuf.SmsResp;

/**
 * Aliyun Sms .
 *
 * @author CGD
 */
public class EmailAliyunImpl extends EmailBase {

  private static Log log = LogFactory.getLog(EmailAliyunImpl.class);

  public EmailAliyunImpl(SmsAppConfigEntity appConfig) {
    super(appConfig);
  }

  @Override
  public SmsProvider getProvider() {
    return SmsProvider.Aliyun;
  }

  @Override
  public SmsResp sendEmail(EmailBasicReq req) {
    try {
      SingleSendMailRequest mailReq = new SingleSendMailRequest();
      if (StringUtils.isNotBlank(appConfig.getAccountName())) {
        mailReq.setAccountName(appConfig.getAccountName());
      } else {
        mailReq.setAccountName("service@mail.byqcby.com");
      }
      mailReq.setAddressType(0);
      mailReq.setSubject(req.getSubject());
      mailReq.setToAddress(req.getEmail());
      mailReq.setHtmlBody(req.getHtmlBody());
      mailReq.setTextBody(req.getTextBody());
      if (StringUtils.isNotBlank(req.getReplyAddress())) {
        mailReq.setReplyAddress(req.getReplyAddress());
        mailReq.setReplyToAddress(true);
      } else {
        mailReq.setReplyToAddress(false);
      }
      Client client = createClient(appConfig);
      // 调用邮件接口
      SingleSendMailResponse resp = client.singleSendMail(mailReq);
      if (resp.getStatusCode() == 200) {
        return SmsResp.newBuilder().setSuccess(true).build();
      } else {
        log.error("发送邮件异常：" + "InvalidMailAddress.NotFound");
        return SmsResp.newBuilder().setSuccess(false).setReason("InvalidMailAddress.NotFound")
            .build();
      }
    } catch (Exception ex) {
      log.error("发送失败：", ex);
      return SmsResp.newBuilder().setSuccess(true).setReason(ex.getMessage()).build();
    }
  }


  public Client createClient(SmsAppConfigEntity appConfig) throws Exception {
    Config config = new Config().setAccessKeyId(appConfig.getAccessKeyId())
        .setAccessKeySecret(appConfig.getAccessKeySecret());
    config.endpoint = "dm.aliyuncs.com";
    return new Client(config);
  }

}
