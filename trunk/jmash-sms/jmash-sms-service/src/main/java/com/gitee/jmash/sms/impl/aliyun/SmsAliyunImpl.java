
package com.gitee.jmash.sms.impl.aliyun;

import com.aliyun.dysmsapi20170525.Client;
import com.aliyun.dysmsapi20170525.models.QuerySmsSignListRequest;
import com.aliyun.dysmsapi20170525.models.QuerySmsSignListResponse;
import com.aliyun.dysmsapi20170525.models.QuerySmsSignListResponseBody;
import com.aliyun.dysmsapi20170525.models.QuerySmsTemplateListRequest;
import com.aliyun.dysmsapi20170525.models.QuerySmsTemplateListResponse;
import com.aliyun.dysmsapi20170525.models.QuerySmsTemplateListResponseBody;
import com.aliyun.dysmsapi20170525.models.SendSmsRequest;
import com.aliyun.dysmsapi20170525.models.SendSmsResponse;
import com.gitee.jmash.sms.entity.SmsAppConfigEntity;
import com.gitee.jmash.sms.impl.SmsBase;
import com.gitee.jmash.sms.impl.aliyun.enums.AliyunSmsResType;
import jakarta.json.bind.JsonbBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import jmash.sms.protobuf.SmsBasicReq;
import jmash.sms.protobuf.SmsProvider;
import jmash.sms.protobuf.SmsResp;

/**
 * Aliyun Sms .
 *
 * @author CGD
 */
public class SmsAliyunImpl extends SmsBase {

  private static Log log = LogFactory.getLog(SmsAliyunImpl.class);

  public SmsAliyunImpl(SmsAppConfigEntity appConfig) {
    super(appConfig);
  }

  @Override
  public SmsProvider getProvider() {
    return SmsProvider.Aliyun;
  }

  @Override
  public SmsResp sendSms(SmsBasicReq request) {
    try {
      SendSmsRequest sendSmsRequest = new SendSmsRequest();
      sendSmsRequest.setPhoneNumbers(request.getMobilePhone());
      sendSmsRequest.setSignName(request.getSmsSign());
      sendSmsRequest.setTemplateCode(request.getTemplateCode());
      String templateParam = JsonbBuilder.create().toJson(request.getParamsMap());
      sendSmsRequest.setTemplateParam(templateParam);
      Client client = createClient(appConfig);
      // 调用短信接口
      SendSmsResponse resp = client.sendSms(sendSmsRequest);

      String code = resp.getBody().code;
      if (AliyunSmsResType.SUCCESS.getStatus().equals(code)) {
        return SmsResp.newBuilder().setSuccess(true).build();
      } else {
        log.error("发送短信异常：" + resp.getBody().getMessage());
        return SmsResp.newBuilder().setSuccess(false).setReason(resp.getBody().getMessage())
            .build();
      }
    } catch (Exception ex) {
      log.error("发送失败：", ex);
      return SmsResp.newBuilder().setSuccess(true).setReason(ex.getMessage()).build();
    }
  }

  /**
   * 获取短信签名.
   */
  public QuerySmsSignListResponseBody syncSmsSign(int page, int pageSize) {
    try {
      Client client = createClient(appConfig);
      QuerySmsSignListRequest req = new QuerySmsSignListRequest();
      req.setPageIndex(page);
      req.setPageSize(pageSize);
      QuerySmsSignListResponse resp = client.querySmsSignList(req);
      String code = resp.getBody().code;
      if (AliyunSmsResType.SUCCESS.getStatus().equals(code)) {
        return resp.getBody();
      } else {
        log.error("同步短信签名异常：" + resp.getBody().getMessage());
      }
    } catch (Exception ex) {
      log.error("同步短信签名异常：", ex);
    }
    return null;
  }

  /**
   * 获取短信模板.
   */
  public QuerySmsTemplateListResponseBody syncSmsTemplate(int page, int pageSize) {
    try {
      Client client = createClient(appConfig);
      QuerySmsTemplateListRequest req = new QuerySmsTemplateListRequest();
      req.setPageIndex(page);
      req.setPageSize(pageSize);
      QuerySmsTemplateListResponse resp = client.querySmsTemplateList(req);
      String code = resp.getBody().code;
      if (AliyunSmsResType.SUCCESS.getStatus().equals(code)) {
        return resp.getBody();
      } else {
        log.error("同步短信模板异常：" + resp.getBody().getMessage());
      }
    } catch (Exception ex) {
      log.error("同步短信模板异常：", ex);
    }
    return null;
  }


  public Client createClient(SmsAppConfigEntity appConfig) throws Exception {
    com.aliyun.teaopenapi.models.Config config =
        new com.aliyun.teaopenapi.models.Config().setAccessKeyId(appConfig.getAccessKeyId())
            .setAccessKeySecret(appConfig.getAccessKeySecret());
    config.endpoint = "dysmsapi.aliyuncs.com";
    return new Client(config);
  }

}
