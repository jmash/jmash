
package com.gitee.jmash.sms.impl.aliyun.enums;

/**
 * 阿里云短信回执状态.
 */
public enum AliyunSmsResType {
  // 成功
  SUCCESS("OK", "成功"),
  // 账户不存在
  ACCOUNT_NOT_EXISTS("isv.ACCOUNT_NOT_EXISTS", "账户不存在"),
  // RAM权限DENY
  RAM_PERMISSION_DENY("isp.RAM_PERMISSION_DENY", "RAM权限DENY"),
  // 业务停机
  OUT_OF_SERVICE("isv.OUT_OF_SERVICE", "业务停机"),
  // 未开通云通信产品的阿里云客户
  PRODUCT_UN_SUBSCRIPT("isv.PRODUCT_UN_SUBSCRIPT", "未开通云通信产品的阿里云客户"),
  // 产品未开通
  PRODUCT_UNSUBSCRIBE("isv.PRODUCT_UNSUBSCRIBE", "产品未开通"),
  // 账户异常
  ACCOUNT_ABNORMAL("isv.ACCOUNT_ABNORMAL", "账户异常"),
  // 短信模板不合法
  SMS_TEMPLATE_ILLEGAL("isv.SMS_TEMPLATE_ILLEGAL", "短信模板不合法"),
  // 短信签名不合法
  SMS_SIGNATURE_ILLEGAL("isv.SMS_SIGNATURE_ILLEGAL", "短信签名不合法"),
  // 参数异常
  INVALID_PARAMETERS("isv.INVALID_PARAMETERS", "参数异常"),
  // 系统错误
  SYSTEM_ERROR("isp.SYSTEM_ERROR", "系统错误"),
  // 非法手机号
  MOBILE_NUMBER_ILLEGAL("isv.MOBILE_NUMBER_ILLEGAL", "非法手机号"),
  // 手机号码数量超过限制
  MOBILE_COUNT_OVER_LIMIT("isv.MOBILE_COUNT_OVER_LIMIT", "手机号码数量超过限制"),
  // 模板缺少变量
  TEMPLATE_MISSING_PARAMETERS("isv.TEMPLATE_MISSING_PARAMETERS", "模板缺少变量"),
  // 业务限流
  BUSINESS_LIMIT_CONTROL("isv.BUSINESS_LIMIT_CONTROL", "业务限流"),
  // JSON参数不合法，只接受字符串值
  INVALID_JSON_PARAM("isv.INVALID_JSON_PARAM", "JSON参数不合法，只接受字符串值"),
  // 黑名单管控
  BLACK_KEY_CONTROL_LIMIT("isv.BLACK_KEY_CONTROL_LIMIT", "黑名单管控"),
  // 参数超出长度限制
  PARAM_LENGTH_LIMIT("isv.PARAM_LENGTH_LIMIT", "参数超出长度限制"),
  // 不支持URL
  PARAM_NOT_SUPPORT_URL("isv.PARAM_NOT_SUPPORT_URL", "不支持URL"),
  // 账户余额不足
  AMOUNT_NOT_ENOUGH("isv.AMOUNT_NOT_ENOUGH", "账户余额不足");

  private String status;

  private String info;

  AliyunSmsResType(String status, String info) {
    this.status = status;
    this.info = info;
  }

  public String getStatus() {
    return this.status;
  }

  public String getInfo() {
    return info;
  }

  /**
   * 获取阿里Type.
   */
  public static AliyunSmsResType findByStatus(String status) {
    for (AliyunSmsResType type : AliyunSmsResType.values()) {
      if (type.status.equals(status)) {
        return type;
      }
    }
    return null;
  }

}
