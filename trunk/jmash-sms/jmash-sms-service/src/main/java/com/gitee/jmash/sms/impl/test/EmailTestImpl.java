
package com.gitee.jmash.sms.impl.test;

import com.gitee.jmash.sms.entity.SmsAppConfigEntity;
import com.gitee.jmash.sms.impl.EmailBase;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import jmash.sms.protobuf.EmailBasicReq;
import jmash.sms.protobuf.SmsProvider;
import jmash.sms.protobuf.SmsResp;

/**
 * Sms Test.
 *
 * @author CGD
 */
public class EmailTestImpl extends EmailBase {

  private static Log log = LogFactory.getLog(EmailTestImpl.class);

  public EmailTestImpl(SmsAppConfigEntity appConfig) {
    super(appConfig);
  }

  @Override
  public SmsProvider getProvider() {
    return SmsProvider.Test;
  }


  @Override
  public SmsResp sendEmail(EmailBasicReq req) {
    String body = StringUtils.isNotBlank(req.getHtmlBody()) ? req.getHtmlBody() : req.getTextBody();
    String content = String.format("标题：%s\n内容：%s\n", req.getSubject(), body);
    log.warn(req);
    return SmsResp.newBuilder().setSuccess(true).setReason(content).build();
  }

}
