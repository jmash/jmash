
package com.gitee.jmash.sms.impl.test;

import com.gitee.jmash.sms.entity.SmsAppConfigEntity;
import com.gitee.jmash.sms.impl.SmsBase;
import jakarta.json.bind.JsonbBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import jmash.sms.protobuf.SmsBasicReq;
import jmash.sms.protobuf.SmsProvider;
import jmash.sms.protobuf.SmsResp;

/**
 * Sms Test.
 *
 * @author CGD
 */
public class SmsTestImpl extends SmsBase {

  private static Log log = LogFactory.getLog(SmsTestImpl.class);

  public SmsTestImpl(SmsAppConfigEntity appConfig) {
    super(appConfig);
  }

  @Override
  public SmsProvider getProvider() {
    return SmsProvider.Test;
  }

  @Override
  public SmsResp sendSms(SmsBasicReq request) {
    String templateParam = JsonbBuilder.create().toJson(request.getParamsMap());
    String content =
        String.format("短信模板：%s\n短信参数如下：%s\n", request.getTemplateCode(), templateParam);
    log.warn(request);
    return SmsResp.newBuilder().setSuccess(true).setReason(content).build();
  }

}
