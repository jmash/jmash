
package com.gitee.jmash.sms.mapper;

import com.crenjoy.proto.mapper.BeanMapper;
import com.crenjoy.proto.mapper.ProtoMapper;
import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.sms.entity.SmsAppConfigEntity;
import com.gitee.jmash.sms.model.SmsAppConfigTotal;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import jmash.sms.protobuf.SmsAppConfigCreateReq;
import jmash.sms.protobuf.SmsAppConfigModel;
import jmash.sms.protobuf.SmsAppConfigPage;

/**
 * SmsAppConfigMapper.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Mapper
public interface SmsAppConfigMapper extends BeanMapper, ProtoMapper {

  SmsAppConfigMapper INSTANCE = Mappers.getMapper(SmsAppConfigMapper.class);

  List<SmsAppConfigModel> listSmsAppConfig(List<SmsAppConfigEntity> list);

  SmsAppConfigPage pageSmsAppConfig(DtoPage<SmsAppConfigEntity, SmsAppConfigTotal> page);

  SmsAppConfigModel model(SmsAppConfigEntity entity);

  SmsAppConfigEntity create(SmsAppConfigCreateReq req);

  SmsAppConfigEntity clone(SmsAppConfigEntity entity);


}
