
package com.gitee.jmash.sms.mapper;

import com.crenjoy.proto.mapper.BeanMapper;
import com.crenjoy.proto.mapper.ProtoMapper;
import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.sms.entity.SmsSignEntity;
import com.gitee.jmash.sms.model.SmsSignTotal;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import jmash.sms.protobuf.SmsSignModel;
import jmash.sms.protobuf.SmsSignPage;

/**
 * SmsSignMapper.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Mapper
public interface SmsSignMapper extends BeanMapper, ProtoMapper {

  SmsSignMapper INSTANCE = Mappers.getMapper(SmsSignMapper.class);

  List<SmsSignModel> listSmsSign(List<SmsSignEntity> list);

  SmsSignPage pageSmsSign(DtoPage<SmsSignEntity, SmsSignTotal> page);

  SmsSignModel model(SmsSignEntity entity);

  SmsSignEntity clone(SmsSignEntity entity);

}
