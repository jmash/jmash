
package com.gitee.jmash.sms.mapper;

import com.crenjoy.proto.mapper.BeanMapper;
import com.crenjoy.proto.mapper.ProtoMapper;
import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.sms.entity.SmsTemplateEntity;
import com.gitee.jmash.sms.model.SmsTemplateTotal;
import com.gitee.jmash.sms.utils.ExtractParametersUtil;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import jmash.sms.protobuf.SmsTemplateModel;
import jmash.sms.protobuf.SmsTemplatePage;

/**
 * SmsTemplateMapper.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Mapper
public interface SmsTemplateMapper extends BeanMapper, ProtoMapper {

  SmsTemplateMapper INSTANCE = Mappers.getMapper(SmsTemplateMapper.class);

  List<SmsTemplateModel> listSmsTemplate(List<SmsTemplateEntity> list);

  SmsTemplatePage pageSmsTemplate(DtoPage<SmsTemplateEntity, SmsTemplateTotal> page);

  SmsTemplateModel model(SmsTemplateEntity entity);

  default SmsTemplateModel templateModel(SmsTemplateEntity entity, boolean isParams) {
    SmsTemplateModel model = model(entity);
    if(model == null) return null;
    List<String> params = ExtractParametersUtil.extractParameters(model.getTemplateContent());
    if(params.size()>0) {
      model = model.toBuilder().addAllTemplateParams(params).build();
    }
    return model;
  }

  SmsTemplateEntity clone(SmsTemplateEntity entity);


}
