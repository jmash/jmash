package com.gitee.jmash.sms.service;

import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.sms.entity.SmsAppConfigEntity;
import com.gitee.jmash.sms.model.SmsAppConfigTotal;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.util.List;
import jmash.sms.protobuf.ConfigType;
import jmash.sms.protobuf.SmsAppConfigReq;
import jmash.sms.protobuf.SmsBasicReq;

/**
 * 消息应用配置 sms_app_config服务Read接口.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public interface SmsAppConfigRead extends TenantService {

  /**
   * 根据主键查询.
   */
  public SmsAppConfigEntity findById(@NotNull String entityId);

  /**
   * 查询页信息.
   */
  public DtoPage<SmsAppConfigEntity, SmsAppConfigTotal> findPageByReq(
      @NotNull @Valid SmsAppConfigReq req);

  /**
   * 综合查询.
   */
  public List<SmsAppConfigEntity> findListByReq(@NotNull @Valid SmsAppConfigReq req);
}
