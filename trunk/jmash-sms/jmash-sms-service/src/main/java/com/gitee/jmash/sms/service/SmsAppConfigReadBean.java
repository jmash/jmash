
package com.gitee.jmash.sms.service;

import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.orm.cdi.JpaTenantService;
import com.gitee.jmash.core.orm.jpa.TenantEntityManager;
import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.core.transaction.JakartaTransaction;
import com.gitee.jmash.sms.dao.SmsAppConfigDao;
import com.gitee.jmash.sms.entity.SmsAppConfigEntity;
import com.gitee.jmash.sms.model.SmsAppConfigTotal;
import jakarta.enterprise.inject.Typed;
import jakarta.enterprise.inject.spi.CDI;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import jakarta.transaction.Transactional.TxType;
import jakarta.validation.executable.ValidateOnExecution;
import java.util.List;
import jmash.sms.protobuf.ConfigType;
import jmash.sms.protobuf.SmsAppConfigReq;
import jmash.sms.protobuf.SmsBasicReq;
import org.apache.commons.lang3.StringUtils;

/**
 * 消息应用配置 sms_app_config读服务.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Typed(SmsAppConfigRead.class)
@Transactional(TxType.SUPPORTS)
@JpaTenantService
@ValidateOnExecution
public class SmsAppConfigReadBean implements SmsAppConfigRead, JakartaTransaction {

  protected TenantEntityManager tem = new TenantEntityManager();

  protected SmsAppConfigDao smsAppConfigDao = new SmsAppConfigDao(tem);

  @PersistenceContext(unitName = "ReadSms")
  public void setEntityManager(EntityManager entityManager) {
    this.tem.setEntityManager(entityManager, false);
  }

  @Override
  public EntityManager getEntityManager() {
    return this.tem.getEntityManager();
  }

  @Override
  @SuppressWarnings("unchecked")
  public <T extends TenantService> T setTenant(String tenant) {
    this.tem.setTenant(tenant);
    return (T) this;
  }

  @Override
  public void setTenantOnly(String tenant) {
    this.tem.setTenantOnly(tenant);
  }

  @Override
  public String getTenant() {
    return this.tem.getTenant();
  }

  @Override
  public SmsAppConfigEntity findById(String entityId) {
    return smsAppConfigDao.find(entityId);
  }


  @Override
  public DtoPage<SmsAppConfigEntity, SmsAppConfigTotal> findPageByReq(SmsAppConfigReq req) {
    return smsAppConfigDao.findPageByReq(req);
  }

  @Override
  public List<SmsAppConfigEntity> findListByReq(SmsAppConfigReq req) {
    return smsAppConfigDao.findListByReq(req);
  }

  @Override
  public void close() throws Exception {
    CDI.current().destroy(this);
  }
}
