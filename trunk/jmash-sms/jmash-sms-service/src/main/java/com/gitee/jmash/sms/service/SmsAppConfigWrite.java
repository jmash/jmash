package com.gitee.jmash.sms.service;

import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.sms.entity.SmsAppConfigEntity;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.util.Set;
import jmash.sms.protobuf.SmsAppConfigCreateReq;
import jmash.sms.protobuf.SmsAppConfigUpdateReq;

/**
 * 消息应用配置 sms_app_config服务Write接口.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public interface SmsAppConfigWrite extends TenantService {

  /**
   * 插入实体.
   */
  public SmsAppConfigEntity insert(@NotNull @Valid SmsAppConfigCreateReq smsAppConfig);

  /**
   * update 实体.
   */
  public SmsAppConfigEntity update(@NotNull @Valid SmsAppConfigUpdateReq smsAppConfig);

  /**
   * 根据主键删除.
   */
  public SmsAppConfigEntity delete(@NotNull String entityId);

  /**
   * 根据主键数组删除.
   */
  public Integer batchDelete(@NotNull @Valid Set<@NotNull String> entityIds);


}
