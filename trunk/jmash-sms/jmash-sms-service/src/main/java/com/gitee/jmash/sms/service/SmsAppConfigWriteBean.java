
package com.gitee.jmash.sms.service;

import com.gitee.jmash.common.lock.DistributedLock;
import com.gitee.jmash.core.jaxrs.ParamsValidationException;
import com.gitee.jmash.core.orm.cdi.JpaTenantService;
import com.gitee.jmash.core.transaction.JakartaTransaction;
import com.gitee.jmash.core.utils.FieldMaskUtil;
import com.gitee.jmash.sms.entity.SmsAppConfigEntity;
import com.gitee.jmash.sms.mapper.SmsAppConfigMapper;
import jakarta.enterprise.inject.Typed;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import jakarta.transaction.Transactional.TxType;
import jakarta.validation.ValidationException;
import jakarta.validation.executable.ValidateOnExecution;
import java.util.Set;
import jmash.sms.protobuf.SmsAppConfigCreateReq;
import jmash.sms.protobuf.SmsAppConfigUpdateReq;

/**
 * 消息应用配置 sms_app_config写服务.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Typed(SmsAppConfigWrite.class)
@Transactional(TxType.REQUIRED)
@JpaTenantService
@ValidateOnExecution
public class SmsAppConfigWriteBean extends SmsAppConfigReadBean
    implements SmsAppConfigWrite, JakartaTransaction {

  @Inject
  DistributedLock lock;

  @Override
  @PersistenceContext(unitName = "WriteSms")
  public void setEntityManager(EntityManager entityManager) {
    this.tem.setEntityManager(entityManager, true);
  }

  @Override
  public SmsAppConfigEntity insert(SmsAppConfigCreateReq smsAppConfig) {
    SmsAppConfigEntity entity = SmsAppConfigMapper.INSTANCE.create(smsAppConfig);
    // 1.业务校验.
    // 2.仅校验,不执行.
    if (smsAppConfig.getValidateOnly()) {
      return entity;
    }
    // 3.检查是否重复请求.
    if (!lock.lock(smsAppConfig.getRequestId(), 60)) {
      throw new ParamsValidationException("requestId", "客户端发起重复请求");
    }
    // 4.执行业务(创建人及时间内部处理.)
    smsAppConfigDao.persist(entity);
    return entity;
  }

  @Override
  public SmsAppConfigEntity update(SmsAppConfigUpdateReq req) {
    SmsAppConfigEntity entity = smsAppConfigDao.find(req.getConfigCode(), req.getValidateOnly());
    if (null == entity) {
      throw new ValidationException("找不到实体:" + req.getConfigCode());
    }
    // 无需更新,返回当前数据库数据.
    if (req.getUpdateMask().getPathsCount() == 0) {
      return entity;
    }
    // 更新掩码属性
    FieldMaskUtil.copyMask(entity, req, req.getUpdateMask());
    // 1.业务校验.
    // 2.仅校验,不执行.
    if (req.getValidateOnly()) {
      return entity;
    }

    // 3.检查是否重复请求.
    if (!lock.lock(req.getRequestId(), 60)) {
      throw new ParamsValidationException("requestId", "客户端发起重复请求");
    }

    // 4.执行业务
    smsAppConfigDao.merge(entity);
    return entity;
  }

  @Override
  public SmsAppConfigEntity delete(String entityId) {
    SmsAppConfigEntity entity = smsAppConfigDao.removeById(entityId);
    return entity;
  }

  @Override
  public Integer batchDelete(Set<String> entityIds) {
    int i = 0;
    for (String entityId : entityIds) {
      smsAppConfigDao.removeById(entityId);
      i++;
    }
    return i;
  }

}
