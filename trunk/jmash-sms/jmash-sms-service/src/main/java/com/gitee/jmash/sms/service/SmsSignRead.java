package com.gitee.jmash.sms.service;

import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.sms.entity.SmsSignEntity;
import com.gitee.jmash.sms.model.SmsSignTotal;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;
import jmash.sms.protobuf.SmsSignReq;

/**
 * 短信签名 sms_sign服务Read接口.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public interface SmsSignRead extends TenantService {

  /**
   * 根据主键查询.
   */
  public SmsSignEntity findById(@NotNull UUID entityId);

  /**
   * 查询页信息.
   */
  public DtoPage<SmsSignEntity, SmsSignTotal> findPageByReq(@NotNull @Valid SmsSignReq req);

  /**
   * 综合查询.
   */
  public List<SmsSignEntity> findListByReq(@NotNull @Valid SmsSignReq req);

}
