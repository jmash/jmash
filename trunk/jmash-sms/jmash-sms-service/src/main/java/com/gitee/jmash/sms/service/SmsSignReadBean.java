
package com.gitee.jmash.sms.service;

import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.orm.cdi.JpaTenantService;
import com.gitee.jmash.core.orm.jpa.TenantEntityManager;
import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.core.transaction.JakartaTransaction;
import com.gitee.jmash.sms.dao.SmsSignDao;
import com.gitee.jmash.sms.entity.SmsSignEntity;
import com.gitee.jmash.sms.model.SmsSignTotal;
import jakarta.enterprise.inject.Typed;
import jakarta.enterprise.inject.spi.CDI;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import jakarta.transaction.Transactional.TxType;
import jakarta.validation.executable.ValidateOnExecution;
import java.util.List;
import java.util.UUID;
import jmash.sms.protobuf.SmsSignReq;

/**
 * 短信签名 sms_sign读服务.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Typed(SmsSignRead.class)
@Transactional(TxType.SUPPORTS)
@JpaTenantService
@ValidateOnExecution
public class SmsSignReadBean implements SmsSignRead, JakartaTransaction {

  protected TenantEntityManager tem = new TenantEntityManager();

  protected SmsSignDao smsSignDao = new SmsSignDao(tem);

  @PersistenceContext(unitName = "ReadSms")
  public void setEntityManager(EntityManager entityManager) {
    this.tem.setEntityManager(entityManager, false);
  }

  @Override
  public EntityManager getEntityManager() {
    return this.tem.getEntityManager();
  }

  @Override
  @SuppressWarnings("unchecked")
  public <T extends TenantService> T setTenant(String tenant) {
    this.tem.setTenant(tenant);
    return (T) this;
  }

  @Override
  public void setTenantOnly(String tenant) {
    this.tem.setTenantOnly(tenant);
  }

  @Override
  public String getTenant() {
    return this.tem.getTenant();
  }

  @Override
  public SmsSignEntity findById(UUID entityId) {
    return smsSignDao.find(entityId);
  }


  @Override
  public DtoPage<SmsSignEntity, SmsSignTotal> findPageByReq(SmsSignReq req) {
    return smsSignDao.findPageByReq(req);
  }

  @Override
  public List<SmsSignEntity> findListByReq(SmsSignReq req) {
    return smsSignDao.findListByReq(req);
  }

  @Override
  public void close() throws Exception {
    CDI.current().destroy(this);
  }
}
