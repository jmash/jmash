package com.gitee.jmash.sms.service;

import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.sms.entity.SmsAppConfigEntity;
import com.gitee.jmash.sms.entity.SmsSignEntity;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.util.Set;
import java.util.UUID;

/**
 * 短信签名 sms_sign服务Write接口.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public interface SmsSignWrite extends TenantService {

  /**
   * 短信签名.
   */
  public boolean syncSmsSign(SmsAppConfigEntity entity);

  /**
   * 根据主键删除.
   */
  public SmsSignEntity delete(@NotNull UUID entityId);

  /**
   * 根据主键数组删除.
   */
  public Integer batchDelete(@NotNull @Valid Set<@NotNull UUID> entityIds);

}
