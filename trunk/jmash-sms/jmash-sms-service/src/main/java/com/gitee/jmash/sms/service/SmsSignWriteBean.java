
package com.gitee.jmash.sms.service;

import com.aliyun.dysmsapi20170525.models.QuerySmsSignListResponseBody;
import com.aliyun.dysmsapi20170525.models.QuerySmsSignListResponseBody.QuerySmsSignListResponseBodySmsSignList;
import com.gitee.jmash.common.lock.DistributedLock;
import com.gitee.jmash.core.orm.cdi.JpaTenantService;
import com.gitee.jmash.core.transaction.JakartaTransaction;
import com.gitee.jmash.sms.SmsFactory;
import com.gitee.jmash.sms.entity.SmsAppConfigEntity;
import com.gitee.jmash.sms.entity.SmsSignEntity;
import com.gitee.jmash.sms.impl.aliyun.SmsAliyunImpl;
import jakarta.enterprise.inject.Typed;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import jakarta.transaction.Transactional.TxType;
import jakarta.validation.executable.ValidateOnExecution;
import java.util.Set;
import java.util.UUID;
import jmash.sms.protobuf.AuditStatus;

/**
 * 短信签名 sms_sign写服务.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Typed(SmsSignWrite.class)
@Transactional(TxType.REQUIRED)
@JpaTenantService
@ValidateOnExecution
public class SmsSignWriteBean extends SmsSignReadBean implements SmsSignWrite, JakartaTransaction {

  @Inject
  DistributedLock lock;

  @Override
  @PersistenceContext(unitName = "WriteSms")
  public void setEntityManager(EntityManager entityManager) {
    this.tem.setEntityManager(entityManager, true);
  }

  @Override
  public boolean syncSmsSign(SmsAppConfigEntity config) {
    SmsAliyunImpl sms = (SmsAliyunImpl) SmsFactory.getSmsImpl(config);
    int page = 1;
    int pageSize = 50;
    QuerySmsSignListResponseBody body = sms.syncSmsSign(page, pageSize);
    addSmsSign(config, body);
    while (body.getTotalCount() > page * pageSize) {
      page++;
      body = sms.syncSmsSign(page, pageSize);
      addSmsSign(config, body);
    }
    return true;
  }

  /**
   * 添加短信签名.
   */
  protected void addSmsSign(SmsAppConfigEntity config, QuerySmsSignListResponseBody body) {
    for (QuerySmsSignListResponseBodySmsSignList sign : body.getSmsSignList()) {
      if (null == smsSignDao.findBy(config.getConfigCode(), sign.getSignName())) {
        SmsSignEntity entity = new SmsSignEntity();
        entity.setSignName(sign.getSignName());
        entity.setConfigCode(config.getConfigCode());
        entity.setOrderId(sign.getOrderId());
        entity.setBusinessType(sign.getBusinessType());
        entity.setAuditStatus(AuditStatus.valueOf(sign.getAuditStatus()));
        smsSignDao.persist(entity);
      }
    }
  }

  @Override
  public SmsSignEntity delete(UUID entityId) {
    SmsSignEntity entity = smsSignDao.removeById(entityId);
    return entity;
  }

  @Override
  public Integer batchDelete(Set<UUID> entityIds) {
    int i = 0;
    for (UUID entityId : entityIds) {
      smsSignDao.removeById(entityId);
      i++;
    }
    return i;
  }

}
