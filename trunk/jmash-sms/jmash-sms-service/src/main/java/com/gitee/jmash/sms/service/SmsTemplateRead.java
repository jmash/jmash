package com.gitee.jmash.sms.service;

import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.sms.entity.SmsTemplateEntity;
import com.gitee.jmash.sms.model.SmsTemplateTotal;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;
import jmash.sms.protobuf.SmsTemplateReq;

/**
 * 短消息模板 sms_template服务Read接口.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public interface SmsTemplateRead extends TenantService {

  /**
   * 根据主键查询.
   */
  public SmsTemplateEntity findById(@NotNull UUID entityId);

  /**
   * 查询页信息.
   */
  public DtoPage<SmsTemplateEntity, SmsTemplateTotal> findPageByReq(
      @NotNull @Valid SmsTemplateReq req);

  /**
   * 综合查询.
   */
  public List<SmsTemplateEntity> findListByReq(@NotNull @Valid SmsTemplateReq req);

}
