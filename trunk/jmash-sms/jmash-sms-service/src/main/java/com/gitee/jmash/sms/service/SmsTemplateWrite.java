package com.gitee.jmash.sms.service;

import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.sms.entity.SmsAppConfigEntity;
import com.gitee.jmash.sms.entity.SmsTemplateEntity;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.util.Set;
import java.util.UUID;

/**
 * 短消息模板 sms_template服务Write接口.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public interface SmsTemplateWrite extends TenantService {

  /**
   * 同步短信模板.
   */
  public boolean syncSmsTemplate(SmsAppConfigEntity config);

  /**
   * 根据主键删除.
   */
  public SmsTemplateEntity delete(@NotNull UUID entityId);

  /**
   * 根据主键数组删除.
   */
  public Integer batchDelete(@NotNull @Valid Set<@NotNull UUID> entityIds);

}
