
package com.gitee.jmash.sms.service;

import com.aliyun.dysmsapi20170525.models.QuerySmsTemplateListResponseBody;
import com.aliyun.dysmsapi20170525.models.QuerySmsTemplateListResponseBody.QuerySmsTemplateListResponseBodySmsTemplateList;
import com.gitee.jmash.common.lock.DistributedLock;
import com.gitee.jmash.core.orm.cdi.JpaTenantService;
import com.gitee.jmash.core.transaction.JakartaTransaction;
import com.gitee.jmash.sms.SmsFactory;
import com.gitee.jmash.sms.entity.SmsAppConfigEntity;
import com.gitee.jmash.sms.entity.SmsTemplateEntity;
import com.gitee.jmash.sms.impl.aliyun.SmsAliyunImpl;
import jakarta.enterprise.inject.Typed;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import jakarta.transaction.Transactional.TxType;
import jakarta.validation.executable.ValidateOnExecution;
import java.util.Set;
import java.util.UUID;
import jmash.sms.protobuf.AuditStatus;
import jmash.sms.protobuf.TemplateType;

/**
 * 短消息模板 sms_template写服务.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Typed(SmsTemplateWrite.class)
@Transactional(TxType.REQUIRED)
@JpaTenantService
@ValidateOnExecution
public class SmsTemplateWriteBean extends SmsTemplateReadBean
    implements SmsTemplateWrite, JakartaTransaction {

  @Inject
  DistributedLock lock;

  @Override
  @PersistenceContext(unitName = "WriteSms")
  public void setEntityManager(EntityManager entityManager) {
    this.tem.setEntityManager(entityManager, true);
  }

  @Override
  public boolean syncSmsTemplate(SmsAppConfigEntity config) {
    SmsAliyunImpl sms = (SmsAliyunImpl) SmsFactory.getSmsImpl(config);
    int page = 1;
    int pageSize = 50;
    QuerySmsTemplateListResponseBody body = sms.syncSmsTemplate(page, pageSize);
    addSmsTemplate(config, body);
    while (body.getTotalCount() > page * pageSize) {
      page++;
      body = sms.syncSmsTemplate(page, pageSize);
      addSmsTemplate(config, body);
    }
    return true;
  }

  /**
   * 添加短信模板.
   */
  protected void addSmsTemplate(SmsAppConfigEntity config, QuerySmsTemplateListResponseBody body) {
    for (QuerySmsTemplateListResponseBodySmsTemplateList template : body.getSmsTemplateList()) {
      if (null == smsTemplateDao.findBy(config.getConfigCode(), template.getTemplateCode())) {
        SmsTemplateEntity entity = new SmsTemplateEntity();
        entity.setTemplateCode(template.getTemplateCode());
        entity.setConfigCode(config.getConfigCode());
        entity.setTemplateName(template.getTemplateName());
        entity.setOrderId(template.getOrderId());
        entity.setTemplateContent(template.getTemplateContent());
        System.out.println(template.getTemplateType());
        entity.setTemplateType(TemplateType.forNumber(template.getTemplateType()));
        entity.setAuditStatus(AuditStatus.valueOf(template.getAuditStatus()));
        smsTemplateDao.persist(entity);
      }
    }
  }

  @Override
  public SmsTemplateEntity delete(UUID entityId) {
    SmsTemplateEntity entity = smsTemplateDao.removeById(entityId);
    return entity;
  }

  @Override
  public Integer batchDelete(Set<UUID> entityIds) {
    int i = 0;
    for (UUID entityId : entityIds) {
      smsTemplateDao.removeById(entityId);
      i++;
    }
    return i;
  }

}
