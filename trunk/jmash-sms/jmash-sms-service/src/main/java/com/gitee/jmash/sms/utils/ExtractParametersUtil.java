package com.gitee.jmash.sms.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExtractParametersUtil {
  public static List<String> extractParameters(String content) {
    List<String> parameters = new ArrayList<>();
    // 定义正则表达式，匹配 ${} 中的内容
    String regex = "\\$\\{([^}]+)\\}";
    Pattern pattern = Pattern.compile(regex);
    Matcher matcher = pattern.matcher(content);
    while (matcher.find()) {
      // 提取并添加参数到列表中
      parameters.add(matcher.group(1));
    }

    return parameters;
  }
}
