/*!40101 SET NAMES utf8 */;

INSERT INTO jmash_sms.sms_app_config (config_code, config_name, config_type, provider_, is_default,
                                      access_key_id, access_key_secret, account_name, create_by, create_time)
VALUES ('EmailTest', '邮件测试服务', 'EMAIL', 'Test', 0, '', '', 'service@mail.byqcby.com', 0xA12CB34C198549A69DA6F220EDF50E1F,
        '2024-09-06'),
       ('SmsTest', '短信测试服务', 'SMS', 'Test', 1, '', '', '', 0xA12CB34C198549A69DA6F220EDF50E1F,
        '2024-09-06');