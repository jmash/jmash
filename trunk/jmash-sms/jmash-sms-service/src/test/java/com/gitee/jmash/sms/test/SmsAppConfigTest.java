
package com.gitee.jmash.sms.test;


import com.google.protobuf.FieldMask;
import com.google.protobuf.Int32Value;
import java.util.UUID;
import io.grpc.StatusRuntimeException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import jmash.sms.SmsGrpc;
import jmash.sms.protobuf.ConfigType;
import jmash.sms.protobuf.SmsAppConfigCreateReq;
import jmash.sms.protobuf.SmsAppConfigKey;
import jmash.sms.protobuf.SmsAppConfigKeyList;
import jmash.sms.protobuf.SmsAppConfigModel;
import jmash.sms.protobuf.SmsAppConfigPage;
import jmash.sms.protobuf.SmsAppConfigReq;
import jmash.sms.protobuf.SmsAppConfigUpdateReq;
import jmash.sms.protobuf.SmsProvider;

/**
 * 测试.
 *
 * @author CGD
 */
@TestMethodOrder(OrderAnnotation.class)
public class SmsAppConfigTest extends SmsTest {

  private SmsGrpc.SmsBlockingStub smsStub = null;

  @Test
  @Order(1)
  public void validatorTest() {
    smsStub = SmsGrpc.newBlockingStub(channel);
    smsStub = smsStub.withCallCredentials(getAuthcCallCredentials());
    StatusRuntimeException exception = Assertions.assertThrows(StatusRuntimeException.class, () -> {
      SmsAppConfigCreateReq req = SmsAppConfigCreateReq.newBuilder().build();
      smsStub.createSmsAppConfig(req);
    });
    Assertions.assertEquals("INTERNAL", exception.getStatus().getCode().name());
    Assertions.assertNotNull(exception.getMessage());
  }

  @Test
  @Order(2)
  public void findPage() {
    smsStub = SmsGrpc.newBlockingStub(channel);
    smsStub = smsStub.withCallCredentials(getAuthcCallCredentials());
    final SmsAppConfigReq.Builder request = SmsAppConfigReq.newBuilder();
    request.setTenant(TENANT);
    final SmsAppConfigPage modelPage = smsStub.findSmsAppConfigPage(request.build());
    System.out.println(modelPage);
  }


  @Test
  @Order(3)
  public void batchDeleteTest() {
    smsStub = SmsGrpc.newBlockingStub(channel);
    smsStub = smsStub.withCallCredentials(getAuthcCallCredentials());
    SmsAppConfigKeyList.Builder request = SmsAppConfigKeyList.newBuilder();
    request.setTenant(TENANT);
    final Int32Value result = smsStub.batchDeleteSmsAppConfig(request.build());
    System.out.println(result);
  }

  @Test
  @Order(4)
  public void test() {
    smsStub = SmsGrpc.newBlockingStub(channel);
    smsStub = smsStub.withCallCredentials(getAuthcCallCredentials());

    // 创建
    SmsAppConfigCreateReq.Builder req = SmsAppConfigCreateReq.newBuilder();
    req.setRequestId(UUID.randomUUID().toString());
    req.setTenant(TENANT);
    req.setConfigCode("test");
    req.setConfigName("测试服务");
    req.setProvider(SmsProvider.Test);
    req.setConfigType(ConfigType.SMS);
    req.setIsDefault(false);
    SmsAppConfigModel entity = smsStub.createSmsAppConfig(req.build());

    // findById
    SmsAppConfigKey pk = SmsAppConfigKey.newBuilder().setConfigCode(entity.getConfigCode())
        .setTenant(TENANT).build();
    entity = smsStub.findSmsAppConfigById(pk);

    // Update
    SmsAppConfigUpdateReq.Builder updateReq = SmsAppConfigUpdateReq.newBuilder();
    updateReq.setRequestId(UUID.randomUUID().toString());
    updateReq.setTenant(TENANT);
    updateReq.setConfigCode("test");
    updateReq.setConfigType(ConfigType.EMAIL);
    FieldMask.Builder fieldMask = FieldMask.newBuilder();
    fieldMask.addPaths("config_type");

    updateReq.setUpdateMask(fieldMask.build());
    entity = smsStub.updateSmsAppConfig(updateReq.build());
    // findById
    pk = SmsAppConfigKey.newBuilder().setConfigCode(entity.getConfigCode()).setTenant(TENANT)
        .build();
    entity = smsStub.findSmsAppConfigById(pk);
    // Delete
    pk = SmsAppConfigKey.newBuilder().setConfigCode(entity.getConfigCode()).setTenant(TENANT)
        .build();
    entity = smsStub.deleteSmsAppConfig(pk);
  }

}
