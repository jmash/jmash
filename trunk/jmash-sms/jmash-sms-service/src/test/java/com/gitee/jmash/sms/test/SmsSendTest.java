package com.gitee.jmash.sms.test;

import com.google.protobuf.BoolValue;
import jmash.sms.protobuf.EmailBasicReq;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import jmash.sms.SmsGrpc;
import jmash.sms.protobuf.EmailCaptchaReq;
import jmash.sms.protobuf.SmsAppConfigKey;
import jmash.sms.protobuf.SmsBasicReq;
import jmash.sms.protobuf.SmsCaptchaReq;
import jmash.sms.protobuf.SmsProvider;
import jmash.sms.protobuf.SmsResp;

@TestMethodOrder(OrderAnnotation.class)
public class SmsSendTest extends SmsTest {

  private SmsGrpc.SmsBlockingStub smsStub = null;

  @Test
  @Order(1)
  public void testSmsBasic() {
    smsStub = SmsGrpc.newBlockingStub(channel);
    smsStub = smsStub.withCallCredentials(getAuthcCallCredentials());
    SmsBasicReq.Builder req =
        SmsBasicReq.newBuilder().setTenant(TENANT).setMobilePhone("18636635682")
            .setSmsSign("阿里云短信测试").setTemplateCode("SMS_154950909")
            .putParams("code", "7890");
    req.setHasProvider(true).setProvider(SmsProvider.Test);
    SmsResp resp = smsStub.sendSms(req.build());
    System.out.println(resp);
  }

  @Test
  @Order(2)
  public void testSms() {
    smsStub = SmsGrpc.newBlockingStub(channel);
    smsStub = smsStub.withCallCredentials(getAuthcCallCredentials());
    SmsCaptchaReq.Builder req = SmsCaptchaReq.newBuilder().setTenant(TENANT)
        .setMobilePhone("15103460848").setSmsSign("百圆养车").setCaptcha("6789")
        .setProduct("唐僧威卡");
    req.setHasProvider(true).setProvider(SmsProvider.Test);
    SmsResp resp = smsStub.sendSmsCaptcha(req.build());
    System.out.println(resp);
  }

  @Test
  @Order(3)
  public void testSmsSyncSign() {
    smsStub = SmsGrpc.newBlockingStub(channel);
    smsStub = smsStub.withCallCredentials(getAuthcCallCredentials());
    SmsAppConfigKey req = SmsAppConfigKey.newBuilder().setTenant(TENANT).setTenant(TENANT)
        .setConfigCode("SmsAliyun").build();
    BoolValue resp = smsStub.syncSmsSign(req);
    System.out.println(resp);
  }

  @Test
  @Order(4)
  public void testSmsSyncTemplate() {
    smsStub = SmsGrpc.newBlockingStub(channel);
    smsStub = smsStub.withCallCredentials(getAuthcCallCredentials());
    SmsAppConfigKey req = SmsAppConfigKey.newBuilder().setTenant(TENANT).setTenant(TENANT)
        .setConfigCode("SmsAliyun").build();
    BoolValue resp = smsStub.syncSmsTemplate(req);
    System.out.println(resp);
  }

  @Test
  @Order(5)
  public void testEmail() {
    smsStub = SmsGrpc.newBlockingStub(channel);
    smsStub = smsStub.withCallCredentials(getAuthcCallCredentials());
    EmailCaptchaReq.Builder req = EmailCaptchaReq.newBuilder().setTenant(TENANT)
        .setEmail("52398508@qq.com").setCaptcha("567890").setProduct("唐僧威卡");
    req.setHasProvider(true).setProvider(SmsProvider.Test);
    SmsResp resp = smsStub.sendEmailCaptcha(req.build());
    System.out.println(resp.getSuccess());
    System.out.println(resp.getReason());
  }

  @Test
  @Order(6)
  public void testSmsByConfigCode() {
    smsStub = SmsGrpc.newBlockingStub(channel);
    smsStub = smsStub.withCallCredentials(getAuthcCallCredentials());
    SmsBasicReq.Builder req =
        SmsBasicReq.newBuilder().setTenant(TENANT).setMobilePhone("18903413101")
            .setSmsSign("阿里云短信测试").setTemplateCode("SMS_154950909")
            .putParams("code", "7890");
    req.setConfigCode("SmsAliyun");
    SmsResp resp = smsStub.sendSms(req.build());
    System.out.println(resp);
  }

  @Test
  public void testEmailByConfigCode() {
    smsStub = SmsGrpc.newBlockingStub(channel);
    smsStub = smsStub.withCallCredentials(getAuthcCallCredentials());
    EmailBasicReq.Builder req = EmailBasicReq.newBuilder().setTenant(TENANT)
        .setEmail("52398508@qq.com").setSubject("测试邮件").setTextBody("这是一封测试邮件！");
        //.setConfigCode("EmailAliyun");
    SmsResp resp = smsStub.sendEmail(req.build());
    System.out.println(resp);
  }

}
