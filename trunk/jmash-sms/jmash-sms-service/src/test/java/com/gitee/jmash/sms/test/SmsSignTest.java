
package com.gitee.jmash.sms.test;


import com.google.protobuf.Int32Value;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import jmash.sms.SmsGrpc;
import jmash.sms.protobuf.SmsSignKeyList;
import jmash.sms.protobuf.SmsSignPage;
import jmash.sms.protobuf.SmsSignReq;

/**
 * 测试.
 *
 * @author CGD
 */
@TestMethodOrder(OrderAnnotation.class)
public class SmsSignTest extends SmsTest {

  private SmsGrpc.SmsBlockingStub smsStub = null;


  @Test
  @Order(2)
  public void findPage() {
    smsStub = SmsGrpc.newBlockingStub(channel);
    smsStub = smsStub.withCallCredentials(getAuthcCallCredentials());
    final SmsSignReq.Builder request = SmsSignReq.newBuilder();
    request.setTenant(TENANT);
    final SmsSignPage modelPage = smsStub.findSmsSignPage(request.build());
    System.out.println(modelPage);
  }


  @Test
  @Order(3)
  public void batchDeleteTest() {
    smsStub = SmsGrpc.newBlockingStub(channel);
    smsStub = smsStub.withCallCredentials(getAuthcCallCredentials());
    SmsSignKeyList.Builder request = SmsSignKeyList.newBuilder();
    request.setTenant(TENANT);
    final Int32Value result = smsStub.batchDeleteSmsSign(request.build());
    System.out.println(result);
  }

}
