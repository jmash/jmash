
package com.gitee.jmash.sms.test;


import com.google.protobuf.Int32Value;
import jmash.sms.protobuf.SmsTemplateKey;
import jmash.sms.protobuf.SmsTemplateModel;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import jmash.sms.SmsGrpc;
import jmash.sms.protobuf.SmsTemplateKeyList;
import jmash.sms.protobuf.SmsTemplatePage;
import jmash.sms.protobuf.SmsTemplateReq;

/**
 * 测试.
 *
 * @author CGD
 */
@TestMethodOrder(OrderAnnotation.class)
public class SmsTemplateTest extends SmsTest {

  private SmsGrpc.SmsBlockingStub smsStub = null;


  @Test
  @Order(2)
  public void findPage() {
    smsStub = SmsGrpc.newBlockingStub(channel);
    smsStub = smsStub.withCallCredentials(getAuthcCallCredentials());
    final SmsTemplateReq.Builder request = SmsTemplateReq.newBuilder();
    request.setTenant(TENANT);
    final SmsTemplatePage modelPage = smsStub.findSmsTemplatePage(request.build());
    System.out.println(modelPage);
  }


  @Test
  @Order(3)
  public void batchDeleteTest() {
    smsStub = SmsGrpc.newBlockingStub(channel);
    smsStub = smsStub.withCallCredentials(getAuthcCallCredentials());
    SmsTemplateKeyList.Builder request = SmsTemplateKeyList.newBuilder();
    request.setTenant(TENANT);
    final Int32Value result = smsStub.batchDeleteSmsTemplate(request.build());
    System.out.println(result);
  }

  @Test
  public void findTemplateByIdTest() {
    smsStub = SmsGrpc.newBlockingStub(channel);
    smsStub = smsStub.withCallCredentials(getAuthcCallCredentials());
    SmsTemplateKey.Builder request = SmsTemplateKey.newBuilder();
    request.setTenant(TENANT);
    request.setTemplateId("090b7458-7bf8-4aed-b313-c6eb18791ac7");
    final SmsTemplateModel model = smsStub.findSmsTemplateById(request.build());
    System.out.println(model);
  }

}
